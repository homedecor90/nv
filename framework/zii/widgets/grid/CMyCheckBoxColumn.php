<?php
/**
 * CCheckBoxColumn class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

Yii::import('zii.widgets.grid.CGridColumn');

/**
 * CCheckBoxColumn represents a grid view column of checkboxes.
 *
 * CCheckBoxColumn supports no selection (read-only), single selection and multiple selection.
 * The mode is determined according to {@link selectableRows}. When in multiple selection mode, the header cell will display
 * an additional checkbox, clicking on which will check or uncheck all of the checkboxes in the data cells.
 *
 * Additionally selecting a checkbox can select a grid view row (depending on {@link CGridView::selectableRows} value) if
 * {@link selectableRows} is null (default).
 *
 * By default, the checkboxes rendered in data cells will have the values that are the same as
 * the key values of the data model. One may change this by setting either {@link name} or
 * {@link value}.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id: CCheckBoxColumn.php 3437 2011-11-07 15:03:58Z mdomba $
 * @package zii.widgets.grid
 * @since 1.1
 */
class CMyCheckBoxColumn extends CCheckBoxColumn
{
	public $visible;
	protected function renderDataCellContent($row,$data)
	{
		$visible = false;
		if($this->visible!==null){
			$visible = $this->evaluateExpression($this->visible,array('data'=>$data,'row'=>$row));
		}
			
		if ($visible){
			parent::renderDataCellContent($row,$data);
		}
	}
}