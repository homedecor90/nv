<?php

class ItemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $itemImageFolderPath = "images/item_images/";
	
	public $to_order = null;
	public $not_ordered = null;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
    
	/**
	 * Specifies the access control rules.

	 * This method is used by the 'accessControl' filter.

	 * @return array access control rules

	 */

	public function accessRules()

	{

		return array(

			array(

				'allow',

				'actions'=>array(

					'index',

					'view',

					'iteminfo',

					'itemcountinstock',

					'itemcolorlist',

					'item_color_list_by_name',

					'item_eta_by_id_quantity',

					'add_new_item_ajax',

					'add_new_item_on_supplier_order_ajax',

					'create',

					'update',

					'qrcodes',

					'admin',

					'admin_waiting',

					'approve_items',

					'delete_waiting_items',

					'inventory',

					'count_pending_scan_string',

					'adjust_instock_qty_ajax',

					'delete_individual_items_ajax',

					'delete_item_full_ajax',

					'add_sale',

					'order_items_from_inventory_ajax',

					'add_related_items_ajax',

					'remove_related_items_ajax',

                    'retailerInventory',
                    'shoppingCart'

				),

				'users'=>array('*'),

				'expression' => 'UrlPermission::checkUrlPermission',

			),
//            array('allow',
//                'actions' => array(
//                    'retailerInventory',
//                ),
//                'users' => array('*')
//            ),
			array('deny',  // deny all users

				'users'=>array('*'),

			),

		);

	}



	/**

	 * Displays a particular model.

	 * @param integer $id the ID of the model to be displayed

	 */

	public function actionView($id)

	{

		$this->render('view',array(

			'model'=>$this->loadModel($id),

		));

	}

    

    public function actionQrcodes($id)

    {

        $model = $this->loadModel($id);

        $dataProvider=new CActiveDataProvider('Individualitem', array(

            'criteria'=>array(

                'condition'=>'item_id='.$id,

                'order'=>'id ASC',

            ),

            'pagination'=>array(

                'pageSize'=>5,

            ),

        ));

        $this->render('qr_codes',array(

			'model'=>$model,

			'dataProvider'=>$dataProvider

		));

    }

    

	/**

	 * Creates a new model.

	 * If creation is successful, the browser will be redirected to the 'view' page.

	 */

	public function actionCreate()

	{

		$model=new Item;

		

		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);



		if(isset($_POST['Item']))

		{

			$model->attributes=$_POST['Item'];

			$model->condition = "New";

			if (Yii::app()->user->isAdmin()){

				$model->status = Item::$STATUS_APPROVED;

			} else {

				$model->status = Item::$STATUS_WAITING_APPROVAL;

			}

			

			if($model->save()){

				$model->boxDimsFromRequest($_POST['ItemBox']);

				$image_url = $this->uploadItemImage($model);

				if ($image_url != ""){

					$model->image = $image_url;

					$model->save();

				}

				

				////////////////////////////////////////////////////////////

				// Added Activity

				Activity::model()->addActivity_CreateNewItem();

				

				$this->redirect(array('admin'));

			}

		}



		$model->box = new ItemBox();

		

		$this->render('create',array(

			'model'=>$model,

		));

	}

	

	public function uploadItemImage($model){

		$image_file = CUploadedFile::getInstance($model,'image_file');

		

		if ($image_file == null)

			return "";

		

		$file_name = $image_file->getName();

		$arrFileName = explode(".", $file_name);			

		$file_extension = "";

		if (isset($arrFileName[1])){

			$file_extension = $arrFileName[1];

		}

		$file_path = $this->itemImageFolderPath.$model->id.".".$file_extension;			

		$image_file->saveAs($file_path);

		return $file_path;

	}



	/**

	 * Updates a particular model.

	 * If update is successful, the browser will be redirected to the 'view' page.

	 * @param integer $id the ID of the model to be updated

	 */

	public function actionUpdate($id)

	{

		$model=$this->loadModel($id);

		

		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);



		if(isset($_POST['Item']))

		{

            $download_image = ($model->image_url != $_POST['Item']['image_url']);

            $model->attributes=$_POST['Item'];

			$image_url = $this->uploadItemImage($model);

			if ($image_url != ""){

				$model->image = $image_url;

			}



			$model->save();

            $model->boxDimsFromRequest($_POST['ItemBox']);

            $model->downloadImageByUrl();

            $this->redirect(array('view','id'=>$model->id));

		}

		

		if (!$model->box){

			$model->box = new ItemBox();

		}

		

		$related_items_manual 	= Item::getBoughtTogether($model->id, 'manual');

		$related_items_auto 	= Item::getBoughtTogether($model->id, 'auto');

		$criteria = new CDbCriteria();

		$criteria->addCondition('id!='.$model->id);

		$criteria->order = 'item_name';

		$all_items = Item::model()->findAll($criteria);

		

		$this->render('update',array(

			'model'=>$model,

			'related_items_manual'=>$related_items_manual,

			'related_items_auto'=>$related_items_auto,

			'all_items'=>$all_items,

		));

	}

	

	public function actionAdd_related_items_ajax(){

		$item_id = $_POST['item_id'];

		$related_id = $_POST['related_id'];

		foreach($related_id as $r_id){

			$id1 = $item_id < $r_id ? $item_id : $r_id;

			$id2 = $item_id > $r_id ? $item_id : $r_id;

			$criteria = new CDbCriteria();

			$criteria->compare('item1_id', $id1);

			$criteria->compare('item2_id', $id2);

			if (!ItemBoughtTogether::model()->find($criteria)){

				$row = new ItemBoughtTogether();

				$row->item1_id = $id1;

				$row->item2_id = $id2;

				$row->qty = 0;

				$row->save();

			}

		}

	}

	

	public function actionRemove_related_items_ajax(){

		$item_id = $_POST['item_id'];

		$related_id = $_POST['related_id'];

		foreach($related_id as $r_id){

			$id1 = $item_id < $r_id ? $item_id : $r_id;

			$id2 = $item_id > $r_id ? $item_id : $r_id;

			$criteria = new CDbCriteria();

			$criteria->compare('item1_id', $id1);

			$criteria->compare('item2_id', $id2);

			$criteria->compare('qty', 0); // only manually added items can be removed

			ItemBoughtTogether::model()->deleteAll($criteria);

		}

	}

	

	/**

	 * Deletes a particular model.

	 * If deletion is successful, the browser will be redirected to the 'admin' page.

	 * @param integer $id the ID of the model to be deleted

	 */

	public function actionDelete($id)

	{

		if(Yii::app()->request->isPostRequest)

		{

			// we only allow deletion via POST request

			$this->loadModel($id)->delete();



			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

			if(!isset($_GET['ajax']))

				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

		}

		else

			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

	}



	/**

	 * Lists all models.

	 */

	public function actionIndex()

	{

		$dataProvider=new CActiveDataProvider('Item');

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));

	}



	/**

	 * Manages all models.

	 */

	public function actionAdmin()

	{

		$model=new Item('search');

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Item']))

			$model->attributes=$_GET['Item'];



		$this->render('admin',array(

			'model'=>$model,

		));

	}

	

	public function actionAdmin_waiting()

	{

		$model=new Item('search');

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Item']))

			$model->attributes=$_GET['Item'];



		$this->render('admin_waiting',array(

			'model'=>$model,

		));

	}

	

	public function actionInventory()

	{

		$model = new Item('search');

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Item'])){

			if ($_GET['Item']['id'] == 0){

                unset($_GET['Item']['id']);

            }

            $model->attributes=$_GET['Item'];

        }

        

        $item_list = Item::model()->getAllItemIdNameList(true, true);

        

		$this->render('inventory',array(

			'model'=>$model,

            'item_list'=>$item_list

		));

	}

    public function actionRetailerInventory()

    {
        $this->layout = "column1";
        $model = new Item('search');

        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['Item'])){

            if ($_GET['Item']['id'] == 0){

                unset($_GET['Item']['id']);

            }

            $model->attributes=$_GET['Item'];

        }



        $item_list = Item::model()->getAllItemIdNameList(true, true);



        $this->render('retailerInventory',array(

            'model'=>$model,

            'item_list'=>$item_list

        ));

    }


    /**

	 * Get Item Color List by Item Code through Ajax

	 */

	public function actionItemcolorlist($item_code){

        $item_code = rawUrlDecode($item_code);

        $itemColorList = Item::model()->getColorList($item_code);		

		if ($itemColorList == null){

			$result['message'] = "failed";

		} else {

			$result['message'] = "success";

			$result['itemColorList'] = $itemColorList;

		}

		echo json_encode($result);

	}

	

    /*

     * Count in stock for the item, AJAX

     */

    public function actionItemcountinstock ($id){

        $count = Item::model()->getCountInStock($id);

        if ($count !== null){

            $result['message'] = "success";

			$result['count_in_stock'] = $count;

        } else {

            $result['message'] = "failed";

        }

        

        echo json_encode($result);

    }

    

	/**

	 * Get Item Color List by Item Code through Ajax

	 */

	public function actionItem_color_list_by_name($item_name){

		$itemColorList = Item::model()->getColorListByName(rawurldecode($item_name));		

		if ($itemColorList == null){

			$result['message'] = "failed";

		} else {

			$result['message'] = "success";

			$result['itemColorList'] = $itemColorList;

		}

		echo json_encode($result);

	}

	

	public function actionItem_eta_by_id_quantity($item_id, $quantity){

		$strEtaById = Item::getStatusString($item_id, $quantity);

		echo $strEtaById;

	}

	

	/**

	 * Get Item Info by Item id through Ajax

	 */

	public function actionIteminfo($id){

		$item_info = Item::model()->findByPK($id);

		

		if ($item_info == null){

			$result['message'] = "failed";

		} else {

			$result['message'] = "success";

			$result['itemInfo'] = $item_info->attributes;

		}

		echo json_encode($result);

	}



	/**

	 * Returns the data model based on the primary key given in the GET variable.

	 * If the data model is not found, an HTTP exception will be raised.

	 * @param integer the ID of the model to be loaded

	 */

	public function loadModel($id)

	{

		$model=Item::model()->findByPk($id);

		if($model===null)

			throw new CHttpException(404,'The requested page does not exist.');

		return $model;

	}



	/**

	 * Performs the AJAX validation.

	 * @param CModel the model to be validated

	 */

	protected function performAjaxValidation($model)

	{

		if(isset($_POST['ajax']) && $_POST['ajax']==='item-form')

		{

			echo CActiveForm::validate($model);

			Yii::app()->end();

		}

	}

	

	public function actionAdd_new_item_ajax(){

		$newItemModel = new Item;

		

		$result['message'] = "fail";

		if(count($_POST))

		{

			$attributes = $_POST;

			$quantity = $attributes['quantity'];

			unset($attributes['quantity']);

			$newItemModel->attributes = $attributes;

			

			$newItemModel->exw_cost_price = 0;

			$newItemModel->fob_cost_price = 0;			

			// $newItemModel->local_pickup = 0;

			// $newItemModel->la_oc_shipping = 0;

			// $newItemModel->canada_shipping = 0;

			$newItemModel->cbm = 0;

			$newItemModel->dimensions_length = "Dimensions Length";

			$newItemModel->dimensions_width = "Dimensions Width";

			$newItemModel->dimensions_height = "Dimensions Height";

			$newItemModel->weight = 0;

			$newItemModel->description = "Description";

			$newItemModel->condition = "New";

			

			// if (Yii::app()->user->isAdmin()){

				$newItemModel->status = Item::$STATUS_APPROVED;

			// } else {

				// $newItemModel->status = Item::$STATUS_WAITING_APPROVAL;

			// }

			

			if($newItemModel->save()){

				$result['message'] = "success";

				$item_data = array(

					"item_name"=>$newItemModel->item_name,

					"item_code"=>$newItemModel->item_code,

					"color"=>$newItemModel->color,

					"sale_price"=>$newItemModel->sale_price,

					"local_pickup"=>$newItemModel->local_pickup,

					"la_oc_shipping"=>$newItemModel->la_oc_shipping,

					"canada_shipping"=>$newItemModel->canada_shipping,

					"quantity"=>$quantity,

				);

				$result['data'] = $item_data;

				////////////////////////////////////////////////////////////

				// Added Activity

				Activity::model()->addActivity_CreateNewItem();

			} else {

				//$result['error'] = CHtml::errorSummary($newItemModel);

			}

		}



		echo(json_encode($result));

	}

	

	public function actionAdd_new_item_on_supplier_order_ajax(){

		$newItemModel = new Item;

		

		$result['message'] = "fail";

		if(count($_POST))

		{

			$attributes = $_POST;

			$quantity = $attributes['quantity'];

			unset($attributes['quantity']);

			$newItemModel->attributes = $attributes;

			

			// if (Yii::app()->user->isAdmin()){

				$model->status = Item::$STATUS_APPROVED;

			// } else {

				// $model->status = Item::$STATUS_WAITING_APPROVAL;

			// }

			

			if($newItemModel->save()){

				$result['message'] = "success";

				$item_data = array(

					"item_code"=>$newItemModel->item_code,

					"color"=>$newItemModel->color,

					"sale_price"=>$newItemModel->sale_price,

					"quantity"=>$quantity,

				);

				$result['data'] = $item_data;

				

				////////////////////////////////////////////////////////////

				// Added Activity

				Activity::model()->addActivity_CreateNewItem();

			} else {

				$result['error'] = CHtml::errorSummary($newItemModel);

			}

		}



		echo(json_encode($result));

	}

	

	public function actionCount_pending_scan_string($item_id)

	{

		$strResult = Item::model()->getCountPendingScanString($item_id);

		echo json_encode($strResult);

	}

	

	public function actionApprove_items($item_id_list){

		$arrItemId = explode("_", $item_id_list);

		if ($arrItemId == null)

			return;

		

		$criteriaItems = new CDbCriteria;

		$criteriaItems->addInCondition('id', $arrItemId);

		$attributesItems = array(

			'status'=>Item::$STATUS_APPROVED,

		);

		

		Item::model()->updateAll($attributesItems, $criteriaItems);

		$waitingItemCount = Item::model()->getWaitingItemCount();

		$result['waitingItemCount'] = $waitingItemCount;

		echo json_encode($result);

	}

    

    public function actionDelete_waiting_items($item_id_list){

		$arrItemId = explode("_", $item_id_list);

		if ($arrItemId == null)

			return;

		

		$criteriaItems = new CDbCriteria;

		$criteriaItems->addInCondition('id', $arrItemId);

				

		if (Item::model()->deleteAll($criteriaItems)){

            $result['status'] = 'success';

            $waitingItemCount = Item::model()->getWaitingItemCount();

            $result['waitingItemCount'] = $waitingItemCount;

        } else {

            $result['status'] = 'fail';

            $result['message'] = 'Error deleting items';

        }

		

		echo json_encode($result);

	}

    

    // act: add, remove

    public function actionAdjust_instock_qty_ajax($item_id, $act){

        if ($act == 'remove'){

            $criteria = new CDbCriteria();

            // Look for items that are not sold first

            $criteria->compare('item_id', $item_id);

            $criteria->compare('status', Individualitem::$STATUS_IN_STOCK);

            $criteria->order = 'customer_order_items_id';

            $item = Individualitem::model()->find($criteria);

            if (!$item){

            	return false;

            }

            if ($item->customer_order_items_id == 0){

                // we have at least one not sold in stock item and delete it

            	$item->delete();

                $result['status'] = 'success';

            } else {

            	// all in stock items are sold

                // look for another available, not sold item (incoming, waiting, etc)

                $criteria = new CDbCriteria();

                $criteria->compare('item_id', $item_id);

                $criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 

				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"

				AND status != "'.Individualitem::$STATUS_NOT_ORDERED.'"

                AND status != "'.Individualitem::$STATUS_IN_STOCK.'"');

                $criteria->order = Individualitem::getStatusOrderStatement();

                $criteria->compare('customer_order_items_id', 0);

                $subst_item = Individualitem::model()->find($criteria);

                

                if ($subst_item){

                	Individualitem::swapCustomerOrderInfo($item, $subst_item);

                	$subst_item->save();

                	$item->delete();

                } else {

	            	$item->status = Individualitem::$STATUS_NOT_ORDERED;

	                $item->supplier_order_id = 0;

	                $item->save();

                }



                $result['status'] = 'success';

            }

        } else if ($act == 'add'){

            // Look for "sold not ordered" items first

            

            $criteria = new CDbCriteria();

            // Look for items that are not sold first

            $criteria->compare('item_id', $item_id);

            $criteria->compare('status', Individualitem::$STATUS_NOT_ORDERED);

            $criteria->limit = 1;

            if ($item = Individualitem::model()->find($criteria)){

                $item->status = Individualitem::$STATUS_IN_STOCK;

                $item->save();

                $result['status'] = 'success';

            } else {

                $item = Item::model()->findByPk($item_id);

                $individual_item = new Individualitem();

                $individual_item->attributes = $item->attributes;

                $individual_item->item_id = $item->id;

                $individual_item->status = Individualitem::$STATUS_IN_STOCK;
                $individual_item->scanned_in_date = date('Y-m-d H:i');

                if ($individual_item->save()){

                    $result['status'] = 'success';

                } else {

                    $result['status'] = 'fail';

                }

            }

        }

        

        echo json_encode($result);

    }

    

    // disabled function

    public function actionDelete_individual_items_ajax($item_id){

        return false;

        $criteria = new CDbCriteria();

        $criteria->compare('item_id', $item_id);

        $criteria->compare('status', Individualitem::$STATUS_IN_STOCK);

        $criteria->compare('customer_order_items_id', 0);

        $free_items     = Individualitem::model()->count($criteria);

        $total_items    = Item::model()->getCountInStock($item_id);

        if ($free_items == $total_items){

            Individualitem::model()->deleteAll($criteria);

            // header('503 '.($total_items - $free_items).' items are already sold');

        } else {

            header('HTTP/1.1 400');

            echo ($total_items - $free_items).' item(s) are already sold';

        }

        

        // echo json_encode($result);

    }

    

    public function actionDelete_item_full_ajax($item_id){

        // check if there are sold, incoming, waiting items

        $status_criteria = new CDbCriteria();

        $status_criteria->compare('status', '<>'.Individualitem::$STATUS_IN_STOCK);

        $status_criteria->compare('status', '<>'.Individualitem::$STATUS_PENDING_SCAN);

        $status_criteria->compare('status', '<>'.Individualitem::$STATUS_SOLD);

        $status_criteria->compare('item_id', $item_id);

        

        $no_delete_count = Individualitem::model()->count($status_criteria);

        

        if ($no_delete_count)

        {

            header('HTTP/1.1 400');

            echo 'Some items are already sold, ordered to supplier or by customer';

            return;

        }

        

        // check if there are items in customer/supplier orders

        $items_criteria = new CDbCriteria();

        $items_criteria->compare('item_id', $item_id);

        

        $no_delete_count = CustomerOrderItems::model()->count($items_criteria) + SupplierOrderItems::model()->count($items_criteria);

        if ($no_delete_count)

        {

            header('HTTP/1.1 400');

            echo 'Some items are already sold, ordered to supplier or by customer';

            return;

        }

        

        // no items anywhere - delete

        Individualitem::model()->deleteAll($items_criteria);

        Item::model()->deleteByPk($item_id);

        

    }

    

    public function actionAdd_sale(){

        $model=new ItemPriorSale();



		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);



        $item_list = Item::model()->getAllItemIdNameList();

        

		if(isset($_POST['ItemPriorSale']))

		{

			$model->attributes=$_POST['ItemPriorSale'];

			if ($model->save()){

                Yii::app()->user->setFlash('success', "Record added");

                $this->redirect('add_sale');

            }

		}



		$this->render('add_sale',array(

			'model'=>$model,

			'item_list'=>$item_list,

		));

    }

    

    /**

     * Called when admin clicks "order" under the "to order" table on inventory page

     * Adds items to existing supplier order or creates new one if nothing's existing 

     */

    public function actionOrder_items_from_inventory_ajax($type){

    	if ($type == 'to_order'){

    		$this->inventoryOrderToOrder();

    	} else {

    		$this->inventoryOrderNotOrdered();

    	}

    	

    	return true;    	

    }

    

    private function inventoryOrderNotOrdered(){

    	// this code should be repeated

    	$items 		= $_POST['items'];

    	$suppliers 	= $_POST['suppliers'];

    	$by_supplier = array();

    	$missing_fields = array();

    	foreach ($items as $i=>$item_id){

    		$item = Item::model()->findByPk($item_id);

    		if (!$item->filledInfo()){

    			$missing_fields[] = $item->item_name;

    		}

    		$by_supplier[$suppliers[$i]][] = $item;

    	}

    	 

    	if (count($missing_fields)){

    		$result['status'] = 'fail';

    		$result['message'] = 'Some items are missing info: '.implode(',', $missing_fields);

    		echo json_encode($result);

    		return true;

    	}

    	 

    	

    	foreach ($by_supplier as $supplier => $items){

    		// Look if we have exitsting pending order

    		$order = SupplierOrder::getPendingOrder($supplier);

    		$new_order = false;

    		if (!$order){

    			$new_order = true;

    			$order = SupplierOrder::createEmptyOrder($supplier);

    			if (!$order){

    				$result['status'] = 'fail';

    				$result['message'] = 'Error. Not all orders were saved.';

    				echo json_encode($result);

    				return false;

    			}

    			AutoMail::sendMailNewSupplierOrderCreated($supplier);

    		}

    		 

    		// now we have order, and need to add items to it

    		foreach ($items as $item){

    			//$item = Item::model()->findByPk($item_id);

    			$order_items 					= new SupplierOrderItems();

    			$order_items->supplier_order_id	= $order->id;

    			//$order_items->quantity 			= $item->countItemsInventory(Individualitem::$STATUS_NOT_ORDERED, true);

    			$order_items->quantity			= 0;

    			$order_items->item_id 			= $item->id;

    			$order_items->exw_cost_price	= $item->exw_cost_price;

    			$order_items->fob_cost_price	= $item->fob_cost_price;

    			$order_items->sale_price		= $item->sale_price;

    			$order_items->added_date		= date("Y-m-d H:i:s");

    			$order_items->status			= SupplierOrder::$STATUS_PENDING;

    			$order_items->operator_id 		= Yii::app()->user->id;

    			if (!$order_items->save()){

    				$result['status'] = 'fail';

    				$result['message'] = 'Error. Not all orders were saved.';

    				echo json_encode($result);

    				return true;

    			}

   	

    			// TODO: this method should behave the same way as createIndividualitems() regarding quantity field

    			$order_items->addNotOrderedItems($item);

    		}

    		if(!$new_order){

    			AutoMail::sendMailNewItemsAddedToSupplierOrder($supplier);

    		}

    	}

    	

    	$result['status'] = 'success';

    	echo json_encode($result);

    }

    

    private function inventoryOrderToOrder(){

    	$items 		= $_POST['items'];

    	$suppliers 	= $_POST['suppliers'];

    	$by_supplier = array();

    	$missing_fields = array();

    	foreach ($items as $i=>$item_id){

    		$item = Item::model()->findByPk($item_id);

    		if (!$item->filledInfo()){

    			$missing_fields[] = $item->item_name;

    		}

    		$by_supplier[$suppliers[$i]][] = $item;

    	}

    	

    	if (count($missing_fields)){

    		$result['status'] = 'fail';

    		$result['message'] = 'Some items are missing info: '.implode(',', $missing_fields);

    		echo json_encode($result);

    		return true;

    	}

    	

    	 

    	foreach ($by_supplier as $supplier => $items){

    		// Look if we have exitsting pending order

    		$order = SupplierOrder::getPendingOrder($supplier);

    		$new_order = false;

    		if (!$order){

    			$new_order = true;

    			$order = SupplierOrder::createEmptyOrder($supplier);

    			if (!$order){

    				$result['status'] = 'fail';

    				$result['message'] = 'Error. Not all orders were saved.';

    				echo json_encode($result);

    				return false;

    			}

    			AutoMail::sendMailNewSupplierOrderCreated($supplier);

    		}

    	

    		// now we have order, and need to add items to it

    		foreach ($items as $item){

    			//$item = Item::model()->findByPk($item_id);

    			$order_items 					= new SupplierOrderItems();

    			$order_items->supplier_order_id	= $order->id;

    			$order_items->quantity 			= $item->toOrder(true);

    			$order_items->item_id 			= $item->id;

    			$order_items->exw_cost_price	= $item->exw_cost_price;

    			$order_items->fob_cost_price	= $item->fob_cost_price;

    			$order_items->sale_price		= $item->sale_price;

    			$order_items->added_date		= date("Y-m-d H:i:s");

    			$order_items->status			= SupplierOrder::$STATUS_PENDING;

    			$order_items->operator_id 		= Yii::app()->user->id;

    			if (!$order_items->save()){

    				$result['status'] = 'fail';

    				$result['message'] = 'Error. Not all orders were saved.';

    				echo json_encode($result);

    				return true;

    			}

   				$order_items->createIndividualitems();



    			//$order_items->addNotOrderedItems($item);

    		}

    		if(!$new_order){

    			AutoMail::sendMailNewItemsAddedToSupplierOrder($supplier);

    		}

    	}

    	 

    	$result['status'] = 'success';

    	echo json_encode($result);

    }

    /**
     *  Show shopping cart, when retailers click add to cart button
     */
    public function actionShoppingCart($id)
    {
        $item = $this->loadModel($id);
        $customerOrder = new CustomerOrder();

        $customerId = RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id);

        $customerOrder->customer_id = $customerId;
        //$salesPersonList = YumUser::model()->getSalesPersonList();
        $customerOrder->sales_person_id = 0;
        $customerOrder->grand_total_price = 0;
        $customerOrder->discounted_total = 0;

        $itemInfo = array(
            'price' => $item->sale_price,
            'shippingCost' => array(
                'Shipping' => $item->shipping_price,
                'Local Pickup' => $item->local_pickup,
                'Shipped from Los Angeles' => $item->la_oc_shipping,
                'Canada Shipping' => $item->canada_shipping,
            )
        );
        Yii::app()->clientScript->scriptMap=array(
            'jquery.js' => false,
            'jquery.min.js' => false,
        );
        Yii::app()->clientScript->registerScript('itemInfo',
            'shippingCosts ='.CJavaScript::encode(CustomerOrder::getShippingMethods()));
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('retailerShoppingCart', array(
                'item' => $item,
                'customerOrder' => $customerOrder,
                'itemInfo' => $itemInfo,
            ), false, true);
        } else {
            $this->render('retailerShoppingCart', array(
                'item' => $item,
                'customerOrder' => $customerOrder,
            ));
        }
    }
}

