<?php
/**
 * Created by PhpStorm.
 * User: Addicted
 * Date: 2/17/14
 * Time: 4:30 PM
 */

class CustomerOrderController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'approvePayment',
                ),
                'users' => array('admin')
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }


    /**
     * Approve payments
     * @param $id
     */
    public function actionApprovePayment($id)
    {
        $model = $this->loadModel('CustomerOrder', $id);
        if ($model->saveAttributes(array(
            'payment_approved' => '1'
        ))) {
            if (Yii::app()->request->isAjaxRequest) {
                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'Payment successfully approved'
                ));
            }
        }
    }
    /**
     * Loads the requested data model.
     * @param string $class the model class name
     * @param integer $id the model ID
     * @param array $criteria additional search criteria
     * @param boolean $exceptionOnNull whether to throw exception if the model is not found. Defaults to true.
     * @return CActiveRecord the model instance.
     * @throws CHttpException if the model cannot be found
     */
    protected function loadModel($class, $id, $criteria = array(), $exceptionOnNull = true)
    {
        if (empty($criteria))
            $model = CActiveRecord::model($class)->findByPk($id);
        else
        {
            $finder = CActiveRecord::model($class);
            $c = new CDbCriteria($criteria);
            $c->mergeWith(array(
                'condition' => $finder->tableSchema->primaryKey . '=:id',
                'params' => array(':id' => $id),
            ));
            $model = $finder->find($c);
        }
        if (isset($model))
            return $model;
        else if ($exceptionOnNull)
            throw new CHttpException(404, 'Unable to find the requested object.');
    }
} 