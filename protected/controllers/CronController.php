<?php

class CronController extends CController{

	public function actionIndex(){
		//AutoMail::sendMailSupplierOrderPendingItems();
		//AutoMail::sendMailSupplierOrderSetETL();
		//AutoMail::sendMailSupplierOrderLoading();
		//AutoMail::sendMailCustomerOrderProcessing();
		//AutoMail::sendMailSupplierOrderBeforeETL();
		//AutoMail::sendMailSupplierOrderContainerBeforeETASupplierShipper();
		//AutoMail::sendMailSupplierOrderContainerBeforeETASupplierShipper();
		// AutoMail::sendMailNotEnoughInStockToAdminOperator();
	}
	
	public function actionCron2hours(){
		AutoMail::sendMailSupplierOrderUploadingOfficialDocuments();
	}
    
	public function actionCron4hours(){
		AutoMail::sendSupplierAcceptItemsReminders();
		AutoMail::sendMailSupplierOrderSetETL();
	}
	
    public function actionCron30mins(){
		AutoMail::sendShipperBolReminders();
	}
    
	public function actionCron_daily(){
		AutoMail::sendMailSupplierOrderBeforeETL();
		AutoMail::sendMailSupplierOrderPendingItems();
		AutoMail::sendMailSupplierOrderContainerBeforeETASupplierShipper();
        AutoMail::sendCustomerReminders();
        AutoMail::sendMailNotEnoughInStockToAdminOperator();
        AutoMail::sendReviewNotifications();
        AutoMail::sendCSResolveClaim();
	}
	
    /*public function actionEtl_notice(){
        AutoMail::sendMailSupplierOrderSetETL();
    }
	
	public function actionSupplier_before_etl(){
        AutoMail::sendMailSupplierOrderBeforeETL();
    }*/
}
