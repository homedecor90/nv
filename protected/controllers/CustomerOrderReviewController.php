<?php
/**
 * CustomerOrderReviewController
 */
class CustomerOrderReviewController extends Controller
{
    public function actions()
    {
        return array(
            'uploadImage' => array(
                'formClass' => 'ReviewImageUploadForm',
                'class' => 'ext.xupload.actions.XUploadAction',
                'path' =>Yii::app() -> getBasePath() . "/../upload/review_images",
                'publicPath' => Yii::app() -> getBaseUrl() . "/upload/review_images",
                'subfolderVar' => 'order_id',
            )
        );
    }
} 