<?php

class IndividualitemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'view',
					'view_detail',
					'print_qrcode',
					'create',
					'update',
					'admin',
					'queue',
					'delete',
					'send_order_items_by_admin_ajax',
					'delete_not_ordered_items_by_admin_ajax',
					'sold_items_details_ajax',
					'cancel_from_customer_order_ajax',
					'scan_as_out_ajax',
					'move_item_to_shipment_ajax',
					'replace_order_item_ajax',
					'scan_out_qr_code',
					'select_shipment_to_scan_box_ajax'
				),
				'users'=>array('*'),
				'expression' => 'UrlPermission::checkUrlPermission',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id, $box)
	{
		
		if(isset($_POST['check_state']))
		{
			$model = $this->loadModel($id);
			// $model->status = $_POST['check_state'];
            if ($_POST['check_state'] == Individualitem::$STATUS_IN_STOCK){
                $model->scanAsIn('New');
				Yii::app()->user->setFlash('success', "Item has been checked in");
            } elseif($_POST['check_state'] == Individualitem::$STATUS_SOLD) {
                $model->scanAsOut();
				Yii::app()->user->setFlash('success', "Item has been checked out");
            }
			$model->save();
		}
	
		$this->render('view',array(
			'model'=>$model,
			'i_item_id'=>$id,
			'box'=>$box
		));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView_detail($id)
	{
		$model = $this->loadModel($id);
	
		$this->render('view_detail',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Individualitem;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$save_success = true;
		$crated_quantity = 0;
		$str_new_id_list = "";
		$print_qrcode = false;
		
		if(isset($_POST['Individualitem']))
		{
			$quantity = intval($_POST['quantity']);
			if (isset($_POST['print_qrcode'])){
				$print_qrcode = true;
			}
			$new_id_list = array();
			
			if ($_POST['Individualitem']['item_id'] != 0){
				$crated_quantity = $quantity;
				
				while ($quantity > 0){
					$model->attributes = $_POST['Individualitem'];
					$model->status = Individualitem::$STATUS_PENDING_SCAN;
					
					if($model->save()){
						Yii::import('ext.qrcode.QRCodeGenerator');
						$this->createIndividualitemQRCode($model->id);
						$new_id_list[] = $model->id;
						$model = new Individualitem;
						$quantity--;
					} else {					
						$save_success = false;
						break;					
					}
				}
			}
			
			$str_new_id_list = implode("_", $new_id_list);
		}
		
		$itemCodeList = Item::model()->getAllItemCodeList();

		$this->render('create',array(
			'save_success'=>$save_success,
			'crated_quantity'=>$crated_quantity,			
			'model'=>$model,
			'itemCodeList'=>$itemCodeList,
			'print_qrcode'=>$print_qrcode,
			'str_new_id_list'=>$str_new_id_list,
		));
	}
	
	private function createIndividualitemQRCode($individual_item_id, $box_i){
		Yii::import('ext.qrcode.QRCodeGenerator');
		$code = new QRCodeGenerator();
		$code->filePath = "images/qrcodes";
		$code->filename = "item_".$individual_item_id.".png";
		$code->data = Yii::app()->getBaseUrl(true)."/index.php/individualitem/view/".$individual_item_id.'?box='.$box_i;
		$code->matrixPointSize = 10;
		$code->subfolderVar = false;
		$code->create();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Individualitem']))
		{
			$model->attributes=$_POST['Individualitem'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		
		$itemCodeList = Item::model()->getAllItemCodeList();

		$this->render('update',array(
			'model'=>$model,
			'itemCodeList'=>$itemCodeList,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Individualitem');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Individualitem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Individualitem']))
			$model->attributes=$_GET['Individualitem'];
        
        $item_list = Item::model()->getAllItemIdNameList();

		$this->render('admin',array(
			'model'=>$model,
            'item_list'=>$item_list
		));
	}
	
	public function actionPrint_qrcode($id=""){
		$type = '';
		if(isset($_GET['type'])){
			$type = $_GET['type'];
		}
		
		$id_array = array();
		if($type == 'instock'){
			$criteria = new CDbCriteria();
			$criteria->compare('status', Individualitem::$STATUS_IN_STOCK);
			$items = Individualitem::model()->findAll($criteria);
			foreach ($items as $item){
				$id_array[] = $item->id;
			}
		}

		$this->renderPartial('print_qrcode',array(
			'id_list'=>$id,
			'id_array'=>$id_array,
			'type'=>$type
		));
	}
	
	public function actionQueue()
	{
		$model=new Individualitem('search');
		$supplierList = YumUser::model()->getSupplierList();
		
		$this->render('queue',array(
			'model'=>$model,
			'supplierList'=>$supplierList
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Individualitem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='individual-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionSend_order_items_by_admin_ajax($supplier_id, $order_id, $item_list, $item_quantity_list, $item_status_list, $item_custom_color_list){
		SupplierOrder::sendOrderExistingItems($supplier_id, $order_id, $item_list, $item_quantity_list, $item_status_list, $item_custom_color_list);
		
		$notOrderedItemCount = Individualitem::model()->getNotOrderedItemCount();
		$result['notOrderedItemCount'] = $notOrderedItemCount;
		echo json_encode($result);
	}
    
    public function actionDelete_not_ordered_items_by_admin_ajax($item_list){
        
        $items = explode('|', $item_list);
        foreach ($items as $item){
            $arr = explode('_', $item);
            $item_id = $arr[0];
            $item_sold = ($arr[1] == 'S');
            // echo intval($item_sold);
            // die();
            $criteria = new CDbCriteria();
            $criteria->compare('status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
            $criteria->compare('status', Individualitem::$STATUS_DENIED, true, "or");
            $criteria->compare('item_id', $item_id);
            if ($item_sold){
                // $criteria->addCondition('NOT ISNULL(sold_date)');
            } else {
                $criteria->addCondition('ISNULL(sold_date)');
            }
            
            $model = new Individualitem;
            // echo $model->count($criteria);
            // die();
            if ($model->deleteAll($criteria)){
                $result['status'] = 'success';
            } else {
                $result['status'] = 'fail';
                $result['message'] = 'Error deleting items';
            }
        }
        
        $notOrderedItemCount = Individualitem::model()->getNotOrderedItemCount();
		$result['notOrderedItemCount'] = $notOrderedItemCount;
		echo json_encode($result);
    }
    
    // returns HTML-code
    public function actionSold_items_details_ajax($item_id, $status){
        $item = Item::model()->findByPk($item_id);
    	$items = $item->getSoldItemsByShortStatus($status);
    	$can_scan_out = $item->canScanOutIndividual();
    	
        $result['status'] = 'success';
        $text  = '<table class="items">';
        $text .= '<tr><th></th><th>Customer</th><th>Billing Name</th><th>Sold On</th>';
        if ($can_scan_out){
            $text .= '<th style="text-align: center;" colspan="2">Action</th>';
        } else {
            $text .= '<th style="text-align: center;">Action</th>';
        }
        $text .= '</tr>';
        foreach($items as $i=>$row){
            $name = $row->customer_order_items->order->customer->first_name.' '.$row->customer_order_items->order->customer->last_name;
            $c_id = $row->customer_order_items->order->customer_id;
            $text .= '<tr>';
            $text .= '<td>'.($i+1).'</td>';
            $text .= '<td style="text-align: left;">';
            $text .= '<a href="'.Yii::app()->getBaseUrl(true).'/index.php/customer/'.$c_id.'">'.$name.'</a>';
            $text .= '</td>';
			$text .= '<td>'.$row->customer_order_items->order->billing_info->billing_name.' '.$row->customer_order_items->order->billing_info->billing_last_name.'</td>';
            $text .= '<td>';
            $text .= date("M. d, Y", strtotime($row->sold_date));
            $text .= '</td>';
            if ($can_scan_out){
                $text .= '<td>';
                $text .= '<a class="scan_item" rel="'.$row->id.'" href="#">Scan as out</a>';
                $text .= '</td>';
            }
            $text .= '<td>';
            // if ($row->shipping_canceled){
                // $text .= 'Shipping cancelled on '.date("M. d, Y", strtotime($row->shipping_canceled_date));
            // } else {
                $text .= '<a class="cancel_item" rel="'.$row->id.'" href="#">Cancel shipping</a>';
            // }
            $text .= '</td>';
            $text .= '</tr>';
        }
        $text .= '</table>';
        $result['text'] = $text;
        echo json_encode($result);
    }
    
    /**
     * We need to create "orphan" shipment first
     * @param int $ind_item_id
     */
    public function actionScan_as_out_ajax($ind_item_id){
    	$item = Individualitem::model()->with('shipment', 'items', 'customer_order_items', 'customer_order_items.order')->findByPk($ind_item_id);
    	if (!$item->items->canScanOutIndividual()){
    		$result['status'] = 'fail';
    		$result['message'] = 'No items available';
    		
    		echo json_encode($result);
    		
    		return true;
    	}
    	
    	if (isset($_POST['tracking_number'])){
    		if (count($item->shipment->active_ind_items) < 2){
    			$shipment = $item->shipment;
    		} else {
    			$shipment = $item->customer_order_items->order->createShipment(array($item->id)); 
    		}

            $shipping_company = $_POST['shipping_company'];
            $shipment_cost = isset($_POST['shipment_cost']) ? $_POST['shipment_cost'] : 0;

            $shipment->tracking_website             = $_POST['tracking_website'];
            $shipment->shipping_company             = $shipping_company;
            $shipment->phone_number 	            = $_POST['phone_number'];
            $shipment->third_party_shipment_cost 	= $shipment_cost;
            $shipment->create3rdPartyBid($shipping_company, $shipment_cost);
    		$res = $shipment->setTrackingNumber($_POST['tracking_number']);
    	} else {
    		$res = $item->scanAsOut();
    	}
    	
        // $item->status = Individualitem::$STATUS_SOLD;
        if ($res){
            $result['status'] = 'success';
            $result['s_id'] = $shipment->id;
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving data';
        }
        
        echo json_encode($result);
    }
    
    public function actionCancel_from_customer_order_ajax($ind_item_id){
        $item = Individualitem::model()->with('customer_order_items', 'customer_order_items.order', 'customer_order_items.order.customer', 'items')->findByPk($ind_item_id);

        $reason = isset($_POST['reason']) ? intval($_POST['reason']) : 0;
        
        if ($item->cancelShipping($reason)){
        	$note = 'Individual item shipping cancelled: '.$item->items->item_name;
        	if ($reason){
            	$reason = CancellationReason::model()->findByPk($reason);
            	$note .= ' ('.$reason->reason.')';
            }
             
        	$item->customer_order_items->order->customer->addNote($note);
        	$result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving data';
        }
        
        echo json_encode($result);
    }
	
	public function actionMove_item_to_shipment_ajax($item_id, $shipment_id){
		// TODO: shipment can already have bids
        $item = Individualitem::model()->findByPk($item_id);
		$old_shipment_id = $item->customer_order_shipment_id;
		$item->customer_order_shipment_id = $shipment_id;
		if ($item->save()){
            $old_shipment = CustomerOrderShipment::model()->with('ind_items')->findByPk($old_shipment_id);
			if (!count($old_shipment->ind_items) && $old_shipment->status == CustomerOrderShipment::$STATUS_PENDING){
				$old_shipment->delete();
			} else {
				$old_shipment->updateStatus();
			}
			$result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving data';
        }
        
        echo json_encode($result);
	}
	
	public function actionReplace_order_item_ajax($ind_item_id, $order_id){
		$ind_item = Individualitem::model()->findByPk($ind_item_id);
		$item_id = $ind_item->item_id;
		$criteria = new CDbCriteria();
		$criteria->compare('item_id', $item_id);
		$criteria->compare('customer_order_id', $order_id);
		$order_items = CustomerOrderItems::model()->findAll($criteria);
		$item_ids = array();
		foreach($order_items as $row){
			$item_ids[] = $row->id;
		}
		$criteria->condition = '';
		$criteria->params = array();
		$criteria->addInCondition('customer_order_items_id', $item_ids);
		$criteria->order = 'FIELD("status", "'.Individualitem::$STATUS_ORDERED_WITHOUT_ETL.'", "'.Individualitem::$STATUS_ORDERED_WITH_ETL.'", "'.Individualitem::$STATUS_ORDERED_WITH_ETA.'")';
		// fields we need to copy: sale price, shipping prices, shipping total, sale date, customer order items id, shipment id
		// TODO: check why we didn't actually copy all the fields mentioned above
		$old_item = Individualitem::model()->find($criteria);
		
		$ind_item->shipping_total 				= $old_item->shipping_total;
		$ind_item->customer_order_items_id 		= $old_item->customer_order_items_id;
		$ind_item->customer_order_shipment_id 	= $old_item->customer_order_shipment_id;
		$ind_item->sold_date 					= $old_item->sold_date;
		$ind_item->is_replacement				= $old_item->is_replacement;
		$ind_item->replacement_item				= $old_item->replacement_item;
		$ind_item->replacement_boxes			= $old_item->replacement_boxes;
		
		$old_item->sold_date 					= '0000-00-00 00:00:00';
		$old_item->customer_order_items_id 		= 0;
		$old_item->customer_order_shipment_id 	= 0;
		$old_item->shipping_total 				= 0;
		$old_item->is_replacement				= 0;
		$old_item->replacement_item				= 0;
		$old_item->replacement_boxes			= '';
		
		if ($ind_item->save() && $old_item->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data.';
		}
		
		echo json_encode($result);
	}
	
	public function actionScan_out_qr_code($i_item_id, $box) {
		// we can use item ID as a parameter instead of individual item ID but maybe it will be needed later when we add "condition" feature for ind. items
		$i_item = Individualitem::model()->with('items')->findByPk($i_item_id);
		$item = $i_item->items;
		// select all shipments containing item we have
		$criteria = new CDbCriteria();
		$criteria->with 	= array('shipment', 'shipment.customer_order', 'shipment.customer_order.customer');
		$criteria->together = true;
		$criteria->compare('t.item_id', $i_item->item_id);
		$criteria->compare('t.status', Individualitem::$STATUS_IN_STOCK);
		$criteria->compare('t.customer_order_shipment_id','>0');
		$criteria->addInCondition('shipment.status', array(CustomerOrderShipment::$STATUS_PENDING,
														CustomerOrderShipment::$STATUS_PICKUP,
														CustomerOrderShipment::$STATUS_SHIPPER_PICKUP));
		$i_items = Individualitem::model()->findAll($criteria);
		$shipments = array();
		$ids = array();
		foreach ($i_items as $row) {
			if (!in_array($row->shipment->id, $ids)){
				// let's check if we need the box we scanned
				$show = true;
				
				if (!$row->shipment->readyForShipping() || !$row->shipment->needToScanOutBox($item->id, $box)){
					$show = false;
				}				
				
				if ($show){
					$shipments[] = $row->shipment;
					$row->shipment->initSessionBoxScanOutInfo();
					$ids[] = $row->shipment->id;
				}
			}
		}
		
		$this->render('scan_out_qr_code',array(
				'shipments'=>$shipments,
				'item'=>$item,
				'box'=>$box,
		));
	}
	
	public function actionSelect_shipment_to_scan_box_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		if (!$shipment){
			return false;
		}
		
		$data = $_POST;
		$box_array = $shipment->getSessionBoxesScanOutArray();
		$box_array = $box_array[$shipment_id];
		$pending_scan = $shipment->getPendingScanOutBoxQty();
		if ($pending_scan == 1){
			// last box, need to set tracking number
			$shipment->tracking_website = $data['tracking_website'];
			$res = $shipment->setTrackingNumber($data['tracking_number']);
			$message = 'Shipment #'.$shipment_id.' scanned out successfully';
		} else {
			$res = $shipment->setSessionBoxScannedOut($data['item_id'], $data['box']);
			$message = 'Box scanned successfully, please scan other boxes to complete shipment';
		}
		
		if ($res){
			$result['status'] = 'success';
			$result['message'] = $message;
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}
		
		echo json_encode($result);
	}
}
