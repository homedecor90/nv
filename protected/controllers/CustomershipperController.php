<?php

class CustomershipperController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	public $stats = null;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>array(
								'pending_orders',
								'cancel_order_item_ajax',
								'cancel_order_ajax',
								'cancel_shipment_ajax',
								'order_chargeback_ajax',
								'order_send_for_bidding_ajax',
								'shipment_send_for_bidding_ajax',
								'order_schedule_pickup_ajax',
								'new_shipment_ajax',
								'shipment_details_ajax',
								'scan_shipment_out_ajax',
								'chargebacks',
								'cancelled',
								'bidding',
								'place_bids_ajax',
								'select_bids',
								'shipment_quotes_list_ajax',
								'select_shipment_bid_ajax',
                                'edit_shipment_bid_ajax',
								'send_shipments_for_shippers_pickup_ajax',
								'send_payment_notification_ajax',
								'bol_upload',
								'upload_bol_ajax',
								'set_bol_filename_ajax',
								'scheduled',
								'download_labels',
								'download_bols',
								'view_bol',
								'set_tracking_number_ajax',
								'requeue_shipment_ajax',
								'reset_bol_ajax',
								'reset_quote_ajax',
								'send_shipper_notifications_ajax',
								'get_edit_order_info_form_ajax',
								'edit_order_info_ajax',
								'approve_order_info_changes_ajax',
								'delete_order_info_changes_ajax',
								'edited_order_info',
								'completed',
								'cancel_shipment_bid_ajax',
								'generate_shipping_services_bids_ajax',
								'customer_pickup',
								'pickup_docs',
								'order_change_shipping_method_form_ajax',
								'order_change_shipping_method_ajax',
								'mark_shipment_as_damaged_ajax',
								'claims',
								'shipment_mark_as_damaged_form_ajax',
								'replace_damaged_items_ajax',
								'refund_damaged_items_ajax',
								'order_damaged_items_from_supplier_form_ajax',
								'order_damaged_items_from_supplier_ajax',
								'cs_claims',
								'cs_completed',
								'cs_invoices',
								'update_claim_amount_ajax',
								'invoices',
								'pay_shipper_invoices_ajax',
								'combine_customer_orders_ajax',
								'disable_additional_payment_ajax',
								'shipment_details_json_ajax',
								'combine_shipments_ajax',
								'upload_claim_doc',
								'attach_claim_doc_ajax',
								'authorize_return_ajax',
								'resolve_claim_ajax',
								'download_claim_doc',
								'claims_archive',
								'cs_claims_archive',
								'shipments',
								'chargebacks_claims',
								'authorize_return_form_ajax',
								'claim_notes_window_ajax',
								'add_claim_note_ajax'
						),
						'users'=>array('*'),
						'expression' => 'UrlPermission::checkUrlPermission',
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}

	public function actionPending_orders(){
		// $this->fixCancelledItems();
		$model = new CustomerOrder();
		if (isset($_GET['CustomerOrder']['_filter_billing_name'])){
			$model->_filter_billing_name = $_GET['CustomerOrder']['_filter_billing_name'];
		}

		$itemNameList = Item::model()->getAllItemNameList();

		$settings = Settings::model()->getSettingVariables();
		$sales_tax_percent = number_format($settings->tax_percent, 2);
		
		$criteria = new CDbCriteria();
		$criteria->compare('approved', 0);
		
		$this->render('pending_orders', array(
				'customerOrderModel'=>$model,
				'itemNameList'=>$itemNameList,
				'sales_tax_percent'=>$sales_tax_percent,
				'pending_approval_count'=>CustomerOrder::model()->count($criteria)
		));
	}

	public function actionChargebacks(){
		$this->render('chargebacks', array(
				'customerOrderModel'=>new CustomerOrder(),
		));
	}

	public function actionCancelled(){
		$this->render('cancelled', array(
				'customerOrderModel'=>new CustomerOrder(),
		));
	}

	/*private function fixCancelledItems(){
		$criteria = new CDbCriteria();
		$criteria->compare('shipping_canceled', 1);
		$ind_items = Individualitem::model()->findAll($criteria);
		foreach ($ind_items as $old_item){
			if ($old_item->status == Individualitem::$STATUS_NOT_ORDERED){
				$old_item->status = Individualitem::$STATUS_CANCELLED_SHIPPING;
				$old_item->save();
			} else {
				$new_item = new Individualitem();
				$new_item->attributes = $old_item->attributes;
				$new_item->status = Individualitem::$STATUS_CANCELLED_SHIPPING;
				$new_item->shipping_canceled_date = $old_item->shipping_canceled_date;
				$new_item->supplier_order_id = 0;
				$new_item->supplier_order_container_id = 0;
				if ($new_item->save()){
					$old_item->customer_order_items_id = 0;
					$old_item->customer_order_shipment_id = 0;
					$old_item->sold_date = '0000-00-00 00:00:00';
					$old_item->shipping_canceled = 0;
					$old_item->save();
				}
			}
		}
	}
*/
	public function actionCancel_order_item_ajax($item_id){
		$items = CustomerOrderItems::model()->findByPk($item_id);
		$items->canceled = 1;
		if ($items->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	} 
	
	public function actionCancel_order_ajax($order_id){
		$order = CustomerOrder::model()->with('customer')->findByPk($order_id);
		$reason = isset($_POST['reason']) ? intval($_POST['reason']) : 0;
		if ($order->cancelShipping($reason)){
			$note = 'Shipping cancelled for the order #'.$order->id;
			if ($reason){
				$reason = CancellationReason::model()->findByPk($reason);
				$note .= ' ('.$reason->reason.')';
			}
			$order->customer->addNote($note);
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionCancel_shipment_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->with('customer_order','customer_order.customer')->findByPk($shipment_id);
		$reason = isset($_POST['reason']) ? intval($_POST['reason']) : 0;
		if ($shipment->cancel($reason)){
			$note = 'Shipping cancelled for the shipment #'.$shipment->id;
			if ($reason){
				$reason = CancellationReason::model()->findByPk($reason);
				$note .= ' ('.$reason->reason.')';
			}
			$shipment->customer_order->customer->addNote($note);
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}
		echo json_encode($result);
	}

	public function actionOrder_chargeback_ajax($order_id){
		$order = CustomerOrder::model()->findByPk($order_id);
		$reason = isset($_POST['reason']) ? intval($_POST['reason']) : 0;
		if ($order->chargeback($reason)){
			$note = 'Chargeback for the order #'.$order->id;
			if ($reason){
				$reason = CancellationReason::model()->findByPk($reason);
				$note .= ' ('.$reason->reason.')';
			}
			$order->customer->addNote($note);
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionOrder_send_for_bidding_ajax($id){
		$order = CustomerOrder::model()->findByPk($id);

		if ($order->verificationRequired()){
			$result['status'] = 'fail';
			$result['message'] = 'Order shipping info verification required';
		} elseif (!$order->itemsHaveInfo()){
			$result['status'] = 'fail';
			$result['message'] = 'Some items are missing info';
		} elseif ($order->isInStock()){
			$result['status'] = 'success';
			$order->sendForBidding();
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Some items are not in stock';
		}

		echo json_encode($result);
	}

	public function actionShipment_send_for_bidding_ajax($id){
		$shipment = CustomerOrderShipment::model()->findByPk($id);
		if ($shipment->customer_order->verificationRequired()){
			$result['status'] = 'fail';
			$result['message'] = 'Order shipping info verification required';
		} elseif (!$shipment->itemsHaveInfo()){
			$result['status'] = 'fail';
			$result['message'] = 'Some items are missing info';
		} elseif ($shipment->isInStock()){
			$result['status'] = 'success';
			$shipment->sendForBidding();
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Some items are not in stock';
		}

		echo json_encode($result);
	}

	public function actionOrder_schedule_pickup_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		
		if ($shipment->scheduleCustomerPickup($_GET['option'] == 'customer', $_GET['date'])){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionNew_shipment_ajax($order_id, array $item_id){
		// TODO: data validation
		$order = CustomerOrder::model()->findByPk($order_id);
		if ($order->createShipment($item_id)){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		/*
		 $shipment = new CustomerOrderShipment();
		$shipment->customer_order_id = $order_id;
		$shipment->status = CustomerOrderShipment::$STATUS_PENDING;
		if ($shipment->save()){
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $item_id);
		Individualitem::model()->updateAll(array('customer_order_shipment_id'=>$shipment->id), $criteria);
		$result['status'] = 'success';
		} else {
		$result['status'] = 'fail';
		$result['message'] = 'Error saving data';
		}
		*/
		echo json_encode($result);
	}

	public function actionShipment_details_ajax($order_id){
		$order = CustomerOrder::model()->with('shipments')->findByPk($order_id);
		if ($order){
			$result['status'] = 'success';
			$result['data'] = $order->getShipmentTable();
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error retrieving order data';
		}

		echo json_encode($result);
	}

	public function actionScan_shipment_out_ajax($shipment_id){
		// TODO: do not let mark "orphan" and picked up locally shipments as damaged
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		if (isset($_POST['tracking_number'])){
            // TODO: avoid duplicating code here and in individualitem controller (when single items is scanned out and orphan shipment is created)
            $shipping_company = $_POST['shipping_company'];
            $shipment_cost = isset($_POST['shipment_cost']) ? $_POST['shipment_cost'] : 0;

            $shipment->tracking_website             = $_POST['tracking_website'];
            $shipment->shipping_company             = $shipping_company;
            $shipment->phone_number 	            = $_POST['phone_number'];
            $shipment->third_party_shipment_cost 	= $shipment_cost;
            $shipment->create3rdPartyBid($shipping_company, $shipment_cost);

			$res = $shipment->setTrackingNumber($_POST['tracking_number']);
		} else {
			$res = $shipment->scanAsOut();
		}
		if ($res){
			$result['status'] = 'success';
			//$result['items'] = $items;
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Not saved';
		}

		echo json_encode($result);
	}

	public function actionBidding(){
		$this->render('bidding', array(
				'shipmentModel'=>new CustomerOrderShipment(),
		));
	}

	public function actionCompleted(){
		$model = new CustomerOrderShipment();

		if ($_GET['CustomerOrderShipment']['_filter_customer_name']){
			$model->_filter_customer_name = $_GET['CustomerOrderShipment']['_filter_customer_name'];
		}
			
		$this->render('completed', array(
				'shipmentModel'=>$model,
		));
	}

	public function actionPlace_bids_ajax(){
		$ids                = $_POST['id'];
		$rate_quotes        = $_POST['rate_quote'];
		$rate_quote_numbers = $_POST['rate_quote_number'];
		$i = 0;
		foreach ($ids as $i=>$id){
			$criteria = new CDbCriteria();
			$criteria->compare('shipment_id', $id);
			$criteria->compare('customer_shipper_id', Yii::app()->user->id);
			// TODO: add editable feature for bids selected by admin
			// $bid = CustomerOrderShipmentBid::model()->find($criteria);
			if (!CustomerOrderShipmentBid::model()->count($criteria)){
				$bid = new CustomerOrderShipmentBid();
				$bid->shipment_id           = $id;
				$bid->customer_shipper_id   = Yii::app()->user->id;
				$bid->rate_quote            = $rate_quotes[$i];
				$bid->rate_quote_number     = $rate_quote_numbers[$i];
				$bid->date                  = date('Y-m-d H:i:s');
				$bid->status                = CustomerOrderShipmentBid::$STATUS_PENDING;

				$bid->save();
			}
		}

		$result['status'] = 'success';

		echo json_encode($result);
	}

	public function actionSelect_bids(){
		/* $criteria = new CDbCriteria();
		 $criteria->compare('status', CustomerOrderShipment::$STATUS_BIDDING);
		$criteria->compare('notification_sent', 0);
		$new_shipments = CustomerOrderShipment::model()->count($criteria); */
		$this->render('select_bids', array(
				'shipmentModel'=>new CustomerOrderShipment(),
				/* 'new_shipments'=>$new_shipments */
		));
	}

	public function actionShipment_quotes_list_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		$this->renderPartial('_shipment_quotes_list', array(
				'bids'=>$shipment->bids
		));
	}

	public function actionSelect_shipment_bid_ajax($bid_id){
		$bid = CustomerOrderShipmentBid::model()->findByPk($bid_id);
		$shipment_id = $bid->shipment_id;
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		$shipment->deselectAllBids();
		if ($bid->selectBid()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Failed to select bid';
		}

		echo json_encode($result);
	}

    public function actionEdit_shipment_bid_ajax($bid_id){
        $bid = CustomerOrderShipmentBid::model()->findByPk($bid_id);
        $bid->rate_quote = floatval($_POST['new_quote']);
        if ($bid->save()){
            $order = $bid->shipment->customer_order;
            $order->calculateAndSaveProfit();
            $result['status'] = 'success';
            $result['message'] = 'Quote updated successfully';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Failed to update the quote';
        }

        echo json_encode($result);
    }

	public function actionSend_shipments_for_shippers_pickup_ajax(array $shipment_id, $date){
		// TODO: do not let user send request if there are no shipments ready
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $shipment_id);
		$shipments = CustomerOrderShipment::model()->findAll($criteria);
		foreach ($shipments as $shipment){
			$shipment->sendForShipperPickup($date);
		}

		CustomerOrderShipment::sendShipperEmails($shipments, $date);
		//~ CustomerOrderShipment::sendShippingLabels($shipments);
	}

	public function actionSend_payment_notification_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		$shipment->additional_payment_required = 1;

		$amount = $shipment->additionalPaymentAmountToPay();
		$code = substr(md5(uniqid()), 0, 8);
		$shipment->additional_payment_amount = $amount;
		$shipment->additional_payment_done = 0;
		$shipment->additional_payment_code = $code;
		$shipment->additional_payment_bid = $shipment->selected_user_bid->id;
		$shipment->additional_payment_notification_date = date('Y-m-d H:i:s');

		//~ echo $shipment->customer_order->customer->email;
		//~ die();

		if ($shipment->save()){
            $link = Yii::app()->createAbsoluteUrl('/customer/additional_payment', array(
                'id' => $shipment_id,
                'code' => $code
            ), 'https');
			//$link = Yii::app()->getBaseUrl(true).'/index.php/customer/additional_payment/?id='.$shipment_id.'&code='.$code;

			$items_str = $shipment->getItemsListForEmail();

			Yii::import('application.extensions.phpmailer.JPhpMailer');

			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipment->customer_order->customer->email);
			$mail->SetFrom(Customer::NOTIFICATION_EMAIL, 'RegencyShop.com');
			$mail->Subject = AutoMailMessage::MS_CUSTOMER_ADDITIONAL_PAYMENT_REQUIRED;
			$body = str_replace('[link]', $link, AutoMailMessage::MC_CUSTOMER_ADDITIONAL_PAYMENT_REQUIRED);
			$body = str_replace('[name]', $shipment->customer_order->customer->first_name.' '.$shipment->customer_order->customer->last_name, $body);
			$body = str_replace('[items]', $items_str, $body);
			$mail->Body = str_replace('[amount]', $amount, $body);
			$mail->Send();
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionBol_upload(){
		$this->render('bol_upload', array(
				'shipmentModel'=>new CustomerOrderShipment(),
		));
	}

	public function actionUpload_bol_ajax(){
		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$folder='upload/bol/';// folder for uploaded files
		$allowedExtensions = array("*");//array("jpg","jpeg","gif","exe","mov" and etc...
		$sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder);
		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		$fileSize = filesize($folder.$result['filename']);//GETTING FILE SIZE
		$fileName = $result['filename'];//GETTING FILE NAME

		echo json_encode($fileSize);// it's array
	}

	public function actionSet_bol_filename_ajax($shipment_id, $filename, $website){
		$criteria = new CDbCriteria();
		$criteria->compare('id', $shipment_id);
		$criteria->compare('shipper_id', Yii::app()->user->id);
		$shipment = CustomerOrderShipment::model()->find($criteria);
		$company = $shipment->shipper->profile->company;
		if (!$company){
			$company = $shipment->shipper->username;
		}
		$company = str_replace(' ', '', $company);

		if (!$shipment){
			$result['status'] = 'fail';
			$result['message'] = 'Shipment Not Found';
		} else {
			$path = Yii::getPathOfAlias('webroot').'/upload/bol/'.$filename;
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			//~ $code = substr(md5(uniqid()), 0, 5);
			$new_name = $shipment_id.'_'.$company.'.'.$ext;
			$new_path = Yii::getPathOfAlias('webroot').'/upload/bol/'.$new_name;
			if (file_exists($new_path)){
				@unlink($new_path);
			}
			$shipment->bol_path = $new_name;
			$shipment->bol_upload_date = date('Y-m-d H:i:s');
			$shipment->tracking_website = base64_decode($website);
			$shipment->save();
			rename($path, $new_path);
			$result['status'] = 'success';
		}

		echo json_encode($result);
	}

	public function actionScheduled(){
		//~ CustomerOrderShipment::model()->findByPk(30)->requeue();
		$shipment = new CustomerOrderShipment();
		$date = isset($_GET['date']) ? $_GET['date'] : false;
		$data_w_bol = $shipment->searchScheduled($date, true);
		$data_wo_bol = $shipment->searchScheduled($date, false);

		// get dates for highlighting
		$sql = "SELECT date_format( pickup_date, '%m/%d/%Y' ) as `date`
		FROM customer_order_shipment
		WHERE STATUS = '".CustomerOrderShipment::$STATUS_SHIPPER_PICKUP."'
		GROUP BY date_format( pickup_date, '%j' )";
		$result = Yii::app()->db->createCommand($sql)->queryAll();

		$dates = array();
		foreach ($result as $row){
			$dates[] = '"'.$row['date'].'"';
		}

		$this->render('scheduled', array(
				'dataProviderWithBol' => $data_w_bol,
				'dataProviderWithoutBol' => $data_wo_bol,
				'highlightDates'=>$dates,
		));
	}

	public function actionDownload_labels(){
		$date = isset($_GET['date']) ? $_GET['date'] : false;
		$criteria = new CDbCriteria();
		$criteria->compare('status', CustomerOrderShipment::$STATUS_SHIPPER_PICKUP);
		$criteria->addCondition('bol_path != ""');
		if ($date){
			$date_mysql = date('Y-m-d', strtotime($date));
			$criteria->addCondition('t.pickup_date>="'.$date_mysql.'" AND t.pickup_date<ADDDATE(\''.$date_mysql.'\', 1)');
		}

		$shipments = CustomerOrderShipment::model()->findAll($criteria);
		CustomerOrderShipment::generateShippingLabels($shipments, $date);
	}

	public function actionDownload_bols(){
		$date = isset($_GET['date']) ? $_GET['date'] : false;
		$criteria = new CDbCriteria();
		$criteria->compare('status', CustomerOrderShipment::$STATUS_SHIPPER_PICKUP);
		$criteria->addCondition('bol_path != ""');
		if ($date){
			$date_mysql = date('Y-m-d', strtotime($date));
			$criteria->addCondition('t.pickup_date>="'.$date_mysql.'" AND t.pickup_date<ADDDATE(\''.$date_mysql.'\', 1)');
		}

		$shipments = CustomerOrderShipment::model()->findAll($criteria);

		// TODO: remove code duplicating with lables function
		$zip = new ZipArchive();
		$dir = Yii::getPathOfAlias('webroot') . '/upload/bol';
		$filename = 'bol';
		if ($date){
			$filename .= '_'.str_replace(array('. ', ', '), '_', $date);
		}
		$zip_path = $dir.'/'.$filename.'.zip';
		$zip->open($zip_path, file_exists($zip_path) ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE);
		foreach($shipments as $shipment){
			$zip->addFile($dir.'/'.$shipment->bol_path, $shipment->bol_path);
		}

		if ($zip->close()){
			header('Content-Type: application/zip');
			header('Content-Disposition: attachment; filename="'.$filename.'.zip"');
			flush();
			readfile($zip_path);
		} else {
			echo 'Error occured';
		}
	}

	public function actionView_bol($id){
		$criteria = new CDbCriteria();
		$criteria->compare('id', $id);
		//$criteria->compare('status', CustomerOrderShipment::$STATUS_SHIPPER_PICKUP);
		$criteria->addCondition('bol_path != ""');
		if (!YumUser::isOperator(true) && !YumUser::isWarehouse()){
			$criteria->compare('shipper_id', Yii::app()->user->id);
		}

		$shipment = CustomerOrderShipment::model()->find($criteria);
		if(!$shipment){
			echo 'Not Found';
			return false;
		}

		$path = Yii::getPathOfAlias('webroot').'/upload/bol/'.$shipment->bol_path;
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		switch (strtolower($ext)){
			case 'pdf':
				header('Content-Type: application/pdf');
				break;
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
		}

		flush();
		readfile($path);
	}

	public function actionSet_tracking_number_ajax($id, $tracking_number){
		$shipment = CustomerOrderShipment::model()->findByPk($id);
		if ($shipment && $shipment->setTrackingNumber($tracking_number)){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionRequeue_shipment_ajax($id){
		$shipment = CustomerOrderShipment::model()->findByPk($id);
		if ($shipment->requeue()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionReset_bol_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
		if ($shipment->resetBol()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionReset_quote_ajax($bid_id){

	}

	public function actionSend_shipper_notifications_ajax(){
		// TODO: use scopes here and everywhere
		$criteria = new CDbCriteria();
		$criteria->compare('status', CustomerOrderShipment::$STATUS_BIDDING);
		//$criteria->compare('notification_sent', 0);
		$count = CustomerOrderShipment::model()->count($criteria);
		$users = YumUser::getCustomerShipperUserList();

		Yii::import('application.extensions.phpmailer.JPhpMailer');

		foreach ($users as $user){
			$name = $user->profile->firstname.' '.$user->profile->lastname;
			$subject = AutoMailMessage::MS_SHIPPER_NOTIFICATION;
			$from_email = AutoMail::getAdminEmail();
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($user->profile->email, $name);
			$mail->SetFrom($from_email, AutoMail::EMAIL_FROM);
			$mail->Subject = $subject;
			$body = str_replace('[count]', $count, AutoMailMessage::MC_SHIPPER_NOTIFICATION);
			//$body = str_replace('[username]', $user->username, $body);
			$body = str_replace('[name]', $name, $body);
			$mail->Body = $body;
			$mail->Send();
			//TODO: create static email function somewhere
			if ($add_emails = $user->profile->getAdditionalEmails()){
				foreach ($add_emails as $email){
					$mail = new JPhpMailer;
					$mail->IsSendmail();
					$mail->AddAddress($email, $name);
					$mail->SetFrom($from_email, AutoMail::EMAIL_FROM);
					$mail->Subject = $subject;
					$mail->Body = $body;
					$mail->Send();
				}
			}
		}

		//CustomerOrderShipment::model()->updateAll(array('notification_sent'=>1), $criteria);
		$result['result'] = 'success';
		echo json_encode($result);
	}

	public function actionGet_edit_order_info_form_ajax($order_id){
	    $customerOrder = CustomerOrder::model()->findByPk($order_id);
        $billing_info = isset($customerOrder->billing_info) ?
            $customerOrder->billing_info : new CustomerOrderBillingInfo();
        $billing_info->customer_order_id = $order_id;
		$this->renderPartial('_edit_billing_info', array(
				'billing_info' => $billing_info,
		));
	}

	public function actionEdit_order_info_ajax(){
		$info = $_POST['CustomerOrderBillingInfo'];
		$order_id = $info['customer_order_id'];
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_id', $order_id);
		$admin = Yii::app()->user->isAdmin();
		if ($admin){
			$record = CustomerOrderBillingInfo::model()->find($criteria);
            if (!$record){
                $record = new CustomerOrderBillingInfo();
            }
		} else {
			$record = CustomerOrderBillingInfoUnapproved::model()->find($criteria);
			if (!$record){
				$record = new CustomerOrderBillingInfoUnapproved();
			}
		}

		$record->attributes = $info;
		if ($record->save()){
			$result['status'] = 'success';
			$result['message'] = $admin ? '' : 'Order info will be updated after Admin\'s approval';
		} else {
			$result['status'] = 'success';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionEdited_order_info(){
		$this->render('edited_order_info', array(
				'model' => new CustomerOrderBillingInfoUnapproved(),
		));
	}

	public function actionDelete_order_info_changes_ajax($id){
		if (CustomerOrderBillingInfoUnapproved::model()->deleteByPk($id)){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Failed to delete row';
		}

		echo json_encode($result);
	}

	public function actionApprove_order_info_changes_ajax($id){
		$new_data = CustomerOrderBillingInfoUnapproved::model()->findByPk($id);

		$old_data = isset($new_data->customer_order->billing_info) ?
            $new_data->customer_order->billing_info : new CustomerOrderBillingInfo();

		$old_data->attributes = $new_data->attributes;
		if ($old_data->save() && $new_data->delete()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Failed to save data';
		}

		echo json_encode($result);
	}

	function actionCancel_shipment_bid_ajax($bid_id){
		$bid = CustomerOrderShipmentBid::model()->findByPk($bid_id);
		if ($bid->cancel()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Failed to cancel bid';
		}

		echo json_encode($result);
	}

	/*
	 * Generates quotes for 3rd party shipping services (fedex, ups, usps)
	*/
	function actionGenerate_shipping_services_bids_ajax(){
		$criteria = new CDbCriteria();
		$criteria->compare('t.shipping_services_bids_generated', 0);
		$criteria->compare('t.status', CustomerOrderShipment::$STATUS_BIDDING);

		$shipments = CustomerOrderShipment::model()
		->with('customer_order', 'customer_order.billing_info', 'active_ind_items', 'active_ind_items.items')
		->findAll($criteria);

		$messages = array();
		foreach ($shipments as $shipment){
			$messages = array_merge($messages, ShippingCompany::generateQuotes($shipment));
		}

		$result['status'] = 'success';
		$result['messages'] = $messages;
		echo json_encode($result);
	}

	function actionCustomer_pickup(){
		$this->render('customer_pickup', array(
				'shipmentModel'=>new CustomerOrderShipment(),
				/* 'new_shipments'=>$new_shipments */
		));
	}

	function actionPickup_docs($id){
		$shipment = CustomerOrderShipment::model()->findByPk($id);
		if(!$shipment){
			echo 'Not Found';
			return false;
		}

		$path = Yii::getPathOfAlias('webroot').'/pdf/pickup/shipment_'.$shipment->id.'.pdf';
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		switch (strtolower($ext)){
			case 'pdf':
				header('Content-Type: application/pdf');
				break;
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
		}

		flush();
		readfile($path);
	}

	// TODO: change all string literals to constants for shipping methods
	function actionOrder_change_shipping_method_form_ajax($order_id){
		$order = CustomerOrder::model()->with('order_items', 'order_items.items')->findByPk($order_id);
		$current_total = $order->grand_total_price;
		$current_dsc_total = $order->discounted_total;

		$shipping_arr = array();
		$shipping_arr['shipping'] = array(
				'title' => CustomerOrder::SHIPPING_METHOD_SHIPPING,
				'total' => $order->calculateGrandTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_SHIPPING),
				'dsc_total'=> $order->calculateDiscountedTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_SHIPPING),
		);

		$shipping_arr['local_pickup'] = array(
				'title' => CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP,
				'total' => $order->calculateGrandTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP),
				'dsc_total'=> $order->calculateDiscountedTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP),
		);

		$shipping_arr['shipped_from_la'] = array(
				'title' => CustomerOrder::SHIPPING_METHOD_SHIPPED_FROM_LA,
				'total' => $order->calculateGrandTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_SHIPPED_FROM_LA),
				'dsc_total'=> $order->calculateDiscountedTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_SHIPPED_FROM_LA),
		);

		$shipping_arr['canada_shipping'] = array(
				'title' => CustomerOrder::SHIPPING_METHOD_CANADA_SHIPPING,
				'total' => $order->calculateGrandTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_CANADA_SHIPPING),
				'dsc_total'=> $order->calculateDiscountedTotalWithShippingMethod(CustomerOrder::SHIPPING_METHOD_CANADA_SHIPPING),
		);

		$this->renderPartial('_order_change_shipping_method_form', array(
				'shipping_arr' => $shipping_arr,
				'current_total' => $current_total,
				'current_dsc_total' => $current_dsc_total,
				'current_profit' => $order->profit,
				'order' => $order
		));
	}

	public function actionOrder_change_shipping_method_ajax($order_id){
		if (!isset($_POST['shipping_method']) || !isset($_POST['custom_discounted_total'])){
			return false;
		}

		$additional_payment 		= floatval($_POST['additional_payment']);
		$custom_discounted_total 	= $_POST['custom_discounted_total'] ? floatval($_POST['custom_discounted_total']) : false;
		$shipping_method 			= $_POST['shipping_method'];
		$order_items 				= $_POST['order_items'];
		$special_shipping 			= $_POST['special_shipping'];
		$send_notification			= $_POST['send_notification'];

		$by_items = array();
		foreach ($order_items as $i => $i_id) {
			$by_items[$i_id] = $special_shipping[$i];
		}

		$order = CustomerOrder::model()->with('order_items')->findByPk($order_id);
		if ($additional_payment < 0){
			$additional_payment = false;
			$send_notification = false;
		}
		if($order->editShippingInfoAfterSale($shipping_method, $by_items, $additional_payment, $send_notification, $custom_discounted_total)){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = CHtml::errorSummary($order);
		}
		// TODO: admin approve/delete

		echo json_encode($result);
	}

	public function actionShipment_mark_as_damaged_form_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->with('scanned_out_ind_items', 'scanned_out_ind_items.items')->findByPk($shipment_id);
		//$items = $shipment->active_ind_items;
		$items = $shipment->scanned_out_ind_items;

		$this->renderPartial('_shipment_mark_as_damaged', array(
				'items' => $items,
				'shipment' => $shipment,
		));
	}

	public function actionMark_shipment_as_damaged_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->with('customer_order','customer_order.customer')->findByPk($shipment_id);

		$boxes = $_POST['boxes'];
		$items = array();
		$item_ids = array();
		foreach ($boxes as $box){
			$val = explode('_', $box);
			$item_ids[] = $val[0];
			$items[$val[0]][] = $val[1];
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id', $item_ids);
		$i_items = Individualitem::model()->with('items')->findAll($criteria);
		$total = 0;
		
		foreach ($i_items as $i_item){
			$total += $i_item->sale_price;
		}
		
		$claim = new CustomerOrderShipmentClaim();
		$claim->shipment_id = $shipment->id;
		$claim->claim_date 	= date('Y-m-d H:i:s');
		$claim->amount = $total + $shipment->selected_user_bid->rate_quote;
		if (!$claim->save()){
			$result['status'] = 'fail';
			$result['status'] = 'Failed to save, try again';
			echo json_encode($result);
			return true;
		}
		
		foreach ($i_items as $i_item){
			$i_item->damaged = 1;
			if (count($items[$i_item->id]) != $i_item->items->box_qty){
				$i_item->damaged_boxes = implode(',', $items[$i_item->id]);
			}
			$i_item->claim_id = $claim->id;
			$i_item->save();
			$item_names[] = $i_item->items->item_name;
		}
		
		$result['status'] = 'success';
		$note  = 'Shipment #'.$shipment->id.' has been damaged during customer shipping.';
		$note .= '<br />Damaged items: ';
		$note .= implode(', ', $item_names);
		$shipment->customer_order->customer->addNote($note, $shipment);
		$shipment->sendClaimEmail();

		echo json_encode($result);
	}

	public function actionClaims(){
		$this->render('claims', array(
				'claimModel'=>new CustomerOrderShipmentClaim(),
		));
	}

	public function actionClaims_archive(){
		$this->render('claims_archive', array(
				'shipmentModel'=>new CustomerOrderShipment(),
		));
	}

	public function actionReplace_damaged_items_ajax($claim_id){
		if (!isset($_POST['item_ids']) || !count($_POST['item_ids'])){
			$result['status'] = 'fail';
			$result['message'] = 'No items given';

			echo json_encode($result);
			return false;
		}

		$items_criteria = new CDbCriteria();
		$items_criteria->addInCondition('t.id', $_POST['item_ids']);
		$items = Individualitem::model()->with('items')->findAll($items_criteria);
		$damaged_items = array();
		$replace_items = array();
		foreach ($items as $item){
			$damaged_items[$item->item_id][] = $item;
		}

		// create replacement order
		$claim = CustomerOrderShipmentClaim::model()->with('shipment', 'shipment.customer_order', 'shipment.customer_order.customer', 'shipment.customer_order.billing_info')->findByPk($claim_id);
		$order 						= new CustomerOrder();
		$order->customer_id 		= $claim->shipment->customer_order->customer_id;
		$order->sales_person_id		= Yii::app()->user->id;
		$order->shipping_method 	= $claim->shipment->customer_order->shipping_method;
		$order->status				= CustomerOrder::$STATUS_PENDING;
		$order->created_on			= date('Y-m-d H:i:s');
		$order->discounted 			= CustomerOrder::ORDER_NOT_DISCOUNTED;
		$order->grand_total_price 	= 0;
		$order->discounted_total	= 0;
		$order->approved			= 1;
		$order->shipping_status		= CustomerOrder::$SHIPPING_STATUS_PENDING;
		$order->is_replacement		= 1;
		$order->replacement_order	= $claim->shipment->customer_order_id;
		if (!$order->save()){
			$result['status'] = 'fail';
			$result['message'] = 'Error creating order';

			echo json_encode($result);
			return false;
		}

		$billing_info = new CustomerOrderBillingInfo();
		$billing_info->attributes = $claim->shipment->customer_order->billing_info->attributes;
		$billing_info->order_notes = '[Replacement]';
		$billing_info->customer_order_id = $order->id;
		$billing_info->save();

		$claim->shipment->customer_order->customer->addNote('Replacement order created (#'.$order->id.')');

		// fill order with order items
		$customer_order_items = array();
		// TODO: support custom colors
		foreach ($damaged_items as $item_id=>$ind_items){
			$order_items 							= new CustomerOrderItems();
			$order_items->customer_order_id			= $order->id;
			$order_items->item_id 					= $item_id;
			$order_items->quantity					= count($ind_items);
			$order_items->exw_cost_price			= $ind_items[0]->items->exw_cost_price;
			$order_items->fob_cost_price			= $ind_items[0]->items->fob_cost_price;
			$order_items->special_shipping_price	= 0;
			$order_items->sale_price 				= 0;
			if (!$order_items->save()){
				$result['status'] = 'fail';
				//$result['message'] = 'Error occured, order has been created but cannot be sold';
				$result['message'] = CHtml::errorSummary($order_items);

				echo json_encode($result);
				return false;
			}

			$customer_order_items[$item_id] = $order_items;
		}

		$order->sell();
		$items_criteria = new CDbCriteria();
		$items_criteria->addInCondition('id', $_POST['item_ids']);
		Individualitem::model()->updateAll(array(
				'replaced'=>1,
				'replaced_order_id'=>$order->id
		), $items_criteria);

		// after order is sold we have real individual items and should edit them (set prices to 0, add replacement box numbers etc)
		// TODO: check what prices should remain and what prices should be set to 0
		foreach ($damaged_items as $item_id=>$damaged_ind_items){
			foreach ($customer_order_items[$item_id]->ind_items as $i=>$new_item){
				$old_item = $damaged_ind_items[$i];
				$new_item->shipping_price 		= 0;
				$new_item->local_pickup 		= 0;
				$new_item->la_oc_shipping 		= 0;
				$new_item->canada_shipping 		= 0;
				$new_item->is_replacement 		= 1;
				//$new_item->exw_cost_price		= 0;
				//$new_item->fob_cost_price		= 0;
				$new_item->replacement_item		= $old_item->id;
				$new_item->replacement_boxes 	= $old_item->damaged_boxes;
				$new_item->save();
			}
		}

		$order->calculateAndSaveProfit();

		$result['status'] = 'success';
		echo json_encode($result);
	}

	public function actionOrder_damaged_items_from_supplier_form_ajax($claim_id){
		$claim = CustomerOrderShipmentClaim::model()->findByPk($claim_id);
		$items = $claim->ind_items;
		$supplierList = YumUser::model()->getSupplierList();

		$this->renderPartial('_shipment_order_damaged_items_from_supplier', array(
				'items' => $items,
				'claim' => $claim,
				'supplierList'=>$supplierList
		));
	}

	public function actionOrder_damaged_items_from_supplier_ajax($claim_id){
		// create order if needed or select existing one

		$boxes			= $_POST['boxes'];
		$order_id 		= intval($_POST['order_id']);
		$supplier_id 	= intval($_POST['supplier_id']);
		if ($order_id === 0){
			$order = new SupplierOrder();
			// 			if (Yii::app()->user->isAdmin()){
			//TODO: now order becomes accepted from the very beginning because denying it by supplier will cause problems
			$order->status = SupplierOrder::$STATUS_ACCEPTED;
			// 			} else {
			// 				$order->status = SupplierOrder::$STATUS_WAITING_APPROVAL;
			// 			}

			$order->supplier_id = $supplier_id;
			$order->created_date = date("Y-m-d H:i:s");
			$order->operator_id	= Yii::app()->user->id;
			if (!$order->save()){
				echo CHtml::errorSummary($order);
				return false;
			}
			Activity::model()->addActivity_CreateSupplierOrderByAdmin($supplier_id);

			AutoMail::sendMailNewSupplierOrderCreated($supplier_id);
		} else {
			$order = SupplierOrder::model()->findByPk($order_id);
		}
		// create order items
		$i_boxes = array();
		$item_ids = array();
		foreach ($boxes as $box){
			$val = explode('_', $box);
			$item_ids[] = $val[0];
			$i_boxes[$val[0]][] = $val[1];
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id', $item_ids);
		$i_items = Individualitem::model()->with('items', 'items.box')->findAll($criteria);
		foreach ($i_items as $i_item){
			// items grouped by item_id
			$old_items[$i_item->item_id][] = $i_item;
		}

		foreach($old_items as $item_id=>$i_items){
			$order_items 						= new SupplierOrderItems();
			$order_items->supplier_order_id 	= $order->id;
			$order_items->is_replacement 		= 1;
			$order_items->item_id				= $item_id;
			$order_items->quantity				= count($i_items);
			$order_items->exw_cost_price		= 0;
			$order_items->fob_cost_price		= 0;
			$order_items->sale_price			= $i_items[0]->items->sale_price;
			$order_items->added_date			= date('Y-m-d H:i:s');
			$order_items->status				= "Accepted";
			$order_items->processed_date		= date("Y-m-d H:i:s");
			$order_items->operator_id			= Yii::app()->user->id;
			$order_items->save();

			foreach ($i_items as $i_item){
				$new_item 								= new Individualitem();
				$new_item->attributes 					= $i_item->attributes;
				$new_item->status 						= Individualitem::$STATUS_ORDERED_WITHOUT_ETL;
				$new_item->item_id 						= $i_item->item_id;
				$new_item->supplier_order_id 			= $order->id;

				if (count($i_boxes[$i_item->id]) != $i_item->items->box_qty){
					$new_item->supplier_replacement_boxes = implode(',',$i_boxes[$i_item->id]);
				}
				$new_item->is_supplier_replacement		= 1;
				foreach ($i_boxes[$i_item->id] as $box){
					$new_item->supplier_replacement_cbm += $i_item->items->getBoxCBM($box);
				}
				//echo $new_item->supplier_replacement_cbm.'/';
				// TODO: copy just needed fields from the old item and leave other fields default
				$new_item->customer_order_items_id 		= 0;
				$new_item->customer_order_shipment_id 	= 0;
				$new_item->supplier_order_container_id	= 0;
				$new_item->eta 							= NULL;
				$new_item->sold_date 					= NULL;
				$new_item->shipping_total				= 0;
				$new_item->scanned_out_date 			= '0000-00-00 00:00:00';
				$new_item->scanned_in_date 				= '0000-00-00 00:00:00';
				$new_item->save();

				$i_item->reordered_from_supplier = 1;
				$i_item->save();

				if ($new_item->save()){
					$individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
					$individualItemSupplierOrderItems->individual_item_id = $new_item->id;
					$individualItemSupplierOrderItems->supplier_order_items_id = $order_items->id;
					$individualItemSupplierOrderItems->save();
				}
			}
				
			// TODO: this should probably be combined automatically without creating and deleting the row
			$order_items->combine();
		}

		$result['status'] = 'success';

		echo json_encode($result);
	}

	public function actionRefund_damaged_items_ajax($claim_id, $amount){
		if (!isset($_POST['item_ids']) || !count($_POST['item_ids'])){
			$result['status'] = 'fail';
			$result['message'] = 'No items given';

			echo json_encode($result);
			return false;
		}

		$claim = CustomerOrderShipmentClaim::model()->with('shipment','shipment.customer_order','shipment.customer_order.customer')->findByPk($claim_id);

		$refund = new CustomerOrderRefund();
		$refund->customer_order_id 	= $claim->shipment->customer_order_id;
		$refund->amount 			= round($amount,2);
		$refund->date 				= date('Y-m-d H:i:s');

		if ($refund->save()){
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $_POST['item_ids']);
			Individualitem::model()->updateAll(array('claim_refund_id'=>$refund->id), $criteria);
			$claim->shipment->customer_order->customer->addNote('Refund: $'.number_format($amount, 2));
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving refund info';
		}

		echo json_encode($result);
	}

	public function actionCs_claims(){
		$model = new CustomerOrderShipment();

		$claim = new CustomerOrderShipmentClaim();
		$claim->_shipper_id = Yii::app()->user->id;
		$dataProvider = $claim->search();

		$this->render('cs_claims', array(
				'dataProvider'=>$dataProvider,
		));
	}

	public function actionCs_claims_archive(){
		$model = new CustomerOrderShipment();

		$claim = new CustomerOrderShipmentClaim();
		$claim->_shipper_id = Yii::app()->user->id;
		$dataProvider = $claim->search(true);

		$this->render('cs_claims_archive', array(
				'dataProvider'=>$dataProvider,
		));
	}

	public function actionCs_completed(){
		$model = new CustomerOrderShipment();

		$shipmentModel = new CustomerOrderShipment();
		$shipmentModel->shipper_id = Yii::app()->user->id;
		$dataProvider = $shipmentModel->searchCompleted();

		$this->render('cs_completed', array(
				'dataProvider'=>$dataProvider,
		));
	}

	public function actionUpdate_claim_amount_ajax($claim_id, $amount){
		$claim = CustomerOrderShipmentClaim::model()->findByPk($claim_id);
		$claim->amount = $amount;
		if ($claim->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving refund info';
		}

		echo json_encode($result);
	}

	public function actionInvoices(){
		$list = YumUser::getCustomerShipperUserList(true);
		// $shipper_list = array_merge(array('0'=>''), $shipper_list);
		$shipper_list = array(0=>'');
		foreach($list as $i=>$user){
			$shipper_list[$user->id] = $user->profile->company;
		}
				
		$criteria = new CDbCriteria();
		$shipper_id = $_GET['shipper_id'] ? $_GET['shipper_id'] : '>0';

		$criteria->compare('t.status', CustomerOrderShipment::$STATUS_COMPLETED);
		$criteria->compare('t.paid', 0);
		$criteria->compare('t.shipper_id', $shipper_id);
		//$criteria->select = '*, SUM(selected_user_bid.rate_quote) as _total_unpaid';
		$criteria->with = 'selected_user_bid';
 		$criteria->together = true;
		$res = CustomerOrderShipment::model()->findAll($criteria);
		$total_unpaid = 0;
		$total_claims = NULL;
		if ($shipper_id != '>0'){
			$total_claims = CustomerOrderShipmentClaim::getOpenClaimsTotal($shipper_id);
		}

		foreach ($res as $row){
			$total_unpaid += $row->selected_user_bid->rate_quote;
		}
		$this->render('invoices', array(
				'shipment'=>new CustomerOrderShipment(),
				'shippers'=>$shipper_list,
				'shipper_id'=>$_GET['shipper_id'] ? $_GET['shipper_id']: 0,
				'total_unpaid'=>$total_unpaid,
				'total_claims'=>$total_claims,
		));
	}

	public function actionCs_invoices(){
		$list = YumUser::getCustomerShipperUserList();
		// $shipper_list = array_merge(array('0'=>''), $shipper_list);
		$this->render('cs_invoices', array(
				'shipment'=>new CustomerOrderShipment(),
		));
	}

	public function actionPay_shipper_invoices_ajax(){
		$shipment_id = $_POST['shipment_id'];
		$checknumber = $_POST['checknumber'];
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $shipment_id);
		$model = CustomerOrderShipment::model();
		$res = $model->updateAll(array(
				'paid'=>1,
				'paid_date'=>date('Y-m-d H:i:s'),
				'paid_checknumber'=>$checknumber,
		), $criteria);
		if ($res){
				
			// send CS notifications
			$criteria = new CDbCriteria();
			$criteria->addInCondition('t.id', $shipment_id);

			// admin/operator
			$a_o_emails = AutoMail::getAdminOperatorEmails();
			
			$shipments = CustomerOrderShipment::model()->with('shipper', 'shipper.profile', 'customer_order', 'customer_order.billing_info')->findAll($criteria);
			$by_cs = array();
			foreach ($shipments as $shipment){
				$by_cs[$shipment->shipper_id][] = $shipment;
			}
			
			foreach ($by_cs as $rows){
				$total = 0;
				// TODO: check what to do with "home decor" and payment date
				$subject = AutoMailMessage::MS_CS_INVOICE_PAID;
				
				$shipper = $rows[0]->shipper;
				if ($shipper){
                    $emails = array_merge($a_o_emails, $shipper->profile->getEmails(true));

                    $shipper_name = $shipper->profile->firstname.' '.$shipper->profile->lastname;
                    //$customer_name = $rows[0]->customer_order->billing_info->shipping_name.' '.$rows[0]->customer_order->billing_info->shipping_last_name;

                    $table  = '<table class="item_list">';
                    $table .= '<tr><th>Rate Quote Number</th><th>Rate Quote in $</th><th>Customer Name</th><th>Date of Pickup</th></tr>';
                    foreach ($rows as $shipment){
                        $customer_name = $shipment->customer_order->billing_info->shipping_name.' '.$shipment->customer_order->billing_info->shipping_last_name;
                        $total += $shipment->selected_user_bid->rate_quote;
                        $table .= '<tr>';
                        $table .= '<td>'.$shipment->selected_user_bid->rate_quote_number.'</td>';
                        $table .= '<td>$'.number_format($shipment->selected_user_bid->rate_quote).'</td>';
                        $table .= '<td>'.$customer_name.'</td>';
                        $table .= '<td>'.date('M. d, Y', strtotime($shipment->pickup_date)).'</td>';
                        $table .= '</tr>';
                    }
                    $table .= '</table>';

                    $search = array('[name]','[total]','[check_number]','[date]');
                    $replace = array($shipper_name, number_format($total, 2), $checknumber, date('M. d, Y'));
                    $body  = str_replace($search, $replace, AutoMailMessage::MC_CS_INVOICE_PAID);
                    $body .= $table;

                    foreach ($emails as $email){
                        $mailer = Yii::app()->email;
                        $mailer->to = $email;
                        $mailer->from = AutoMail::getAdminEmail();
                        $mailer->subject = $subject;
                        $mailer->message = $body;
                        $mailer->send();
                    }
                }
			}
				
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = Chtml::errorSummary($model);
		}

		echo json_encode($result);
	}

	public function actionCombine_customer_orders_ajax(){
		$main_order = CustomerOrder::model()->findByPk($_POST['main_order_id']);
		$order = CustomerOrder::model()->findByPk($_POST['order_id']);
		$main_shipment = $main_order->shipments[0];
		$old_shipments = array();
		$item_count = 0;
		foreach ($order->order_items as $items) {
			foreach ($items->ind_items as $item) {
				$item_count++;
				$old_shipments[] = $item->shipment;
				$item->customer_order_shipment_id = $main_shipment->id;
				$item->save();
			}
		}

		foreach ($old_shipments as $shipment) {
			if (!count($shipment->ind_items)){
				$shipment->delete();
			}
		}

		if ($_POST['copy_info'] == 1){
			$main_order->billing_info->shipping_name 			= $order->billing_info->shipping_name;
			$main_order->billing_info->shipping_last_name 		= $order->billing_info->shipping_last_name;
			$main_order->billing_info->shipping_street_address 	= $order->billing_info->shipping_street_address;
			$main_order->billing_info->shipping_city 			= $order->billing_info->shipping_city;
			$main_order->billing_info->shipping_state 			= $order->billing_info->shipping_state;
			$main_order->billing_info->shipping_zip_code 		= $order->billing_info->shipping_zip_code;
			$main_order->billing_info->shipping_phone_number 	= $order->billing_info->shipping_street_address;
			$main_order->billing_info->busrestype 				= $order->billing_info->busrestype;
			$main_order->billing_info->save();
		}

		$order->combined_with = $main_order->id;
		$order->save();
		$order->customer->addNote('Order #'.$order->id.' was combined with order #'.$main_order->id);

		$result['status'] = 'success';
		$result['items_moved'] = $item_count;

		echo json_encode($result);
	}

	public function actionDisable_additional_payment_ajax($id){
		$shipment = CustomerOrderShipment::model()->findByPk($id);
		$shipment->additional_payment_disabled = 1;
		if ($shipment->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionShipment_details_json_ajax($shipment_id){
		$shipment = CustomerOrderShipment::model()->with('customer_order', 'customer_order.billing_info', 'customer_order.customer')->findByPk($shipment_id);
		if ($shipment){
			$details['id'] 					= $shipment->id;
			$details['status'] 				= $shipment->status;
			$details['order_id'] 			= $shipment->customer_order_id;
			$details['customer_id'] 		= $shipment->customer_order->customer_id;
			$details['customer_name'] 		= $shipment->customer_order->customer->first_name.' '.$shipment->customer_order->customer->last_name;
			$details['shipping_name']		= $shipment->customer_order->billing_info->shipping_name.' '.$shipment->customer_order->billing_info->shipping_last_name;
			$details['shipping_address']	= $shipment->customer_order->getShippingAddressStr();
				
			$result['status'] = 'success';
			$result['shipment'] = $details;
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Shipment not found';
		}

		echo json_encode($result);
	}

	public function actionCombine_shipments_ajax(){
		$shipment = CustomerOrderShipment::model()->findByPk($_POST['attached_shipment_id']);
		if ($shipment->attachTo($_POST['main_shipment_id'])){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionAuthorize_return_ajax($shipment_id){
		$boxes = $_POST['boxes'];
		$i_boxes = array();
		$item_ids = array();
		foreach ($boxes as $box){
			$val = explode('_', $box);
			$item_ids[] = $val[0];
			$i_boxes[$val[0]][] = $val[1];
		}

		$items = Individualitem::model()->with('items')->findAllByPk($item_ids);
		$claim_shipment = CustomerOrderShipment::model()->with('customer_order','customer_order.billing_info','shipper','shipper.profile')->findByPk($shipment_id);
		// create return shipment
		$shipment = new CustomerOrderShipment();
		$shipment->return 				= 1;
		$shipment->return_date 			= date('Y-m-d H:i:s');
		$shipment->customer_order_id 	= $claim_shipment->customer_order_id;
		$shipment->shipper_id 			= $claim_shipment->shipper_id;
		$shipment->status				= CustomerOrderShipment::$STATUS_BIDDING;
		$shipment->total_shipping_price = 0;
		$fail = false;
		if ($shipment->save()){
			foreach ($items as $i_item){
				$return 				= new IndividualItemReturn();
				$return->shipment_id 	= $shipment->id;
				$return->item_id 		= $i_item->item_id;
				$return->claim_id 		= $i_item->customer_order_shipment_id;

				if (count($i_boxes[$i_item->id]) < $i_item->items->box_qty){
					$return->returned_boxes = implode(',', $i_boxes[$i_item->id]);
				}
				if ($return->save()){
					$i_item->customer_return_id = $return->id;
					$i_item->save();
				} else {
					$fail = true;
				}
			}
				
			// send CS email
			Yii::import('application.extensions.phpmailer.JPhpMailer');

			$shipper = $claim_shipment->shipper;
			$shipper_name = $shipper->profile->firstname.' '.$shipper->profile->lastname;
			$customer_name = $claim_shipment->customer_order->billing_info->shipping_name.' '.$claim_shipment->customer_order->billing_info->shipping_last_name;
				
			$subject = AutoMailMessage::MS_RETURN_AUTHORIZED;
			$search = array('[name]','[tracking_number]','[customer_name]');
			$replace = array($shipper_name, $claim_shipment->tracking_number, $customer_name);
			$body = str_replace($search, $replace, AutoMailMessage::MC_RETURN_AUTHORIZED);

			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->claim_email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();

			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
		} else {
			$fail = true;
		}

		if ($fail){
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		} else {
			$result['status'] = 'success';
		}

		echo json_encode($result);
	}

	public function actionUpload_claim_doc(){
		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$folder='upload/claims/';// folder for uploaded files
		$allowedExtensions = array("pdf","jpg","jpeg","png","rar","zip","doc","odt");//array("jpg","jpeg","gif","exe","mov" and etc...
		$sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder);
		//$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		if (isset($result['success']) && $result['success'] == true){
			$path = $folder.$result['filename'];
			$doc = new OfficialDoc();
			$doc->path = $path;
			$doc->date = date('Y-m-d H:i:s');
			$doc->type = OfficialDoc::$TYPE_CLAIM;
			$doc->save();
			$response['status'] = 'success';
			$response['doc_id'] = $doc->id;
		} else {
			$response['status'] = 'fail';
			$response['message'] = $result['error'];
		}

		// 		$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
		// 		$fileName=$result['filename'];//GETTING FILE NAME

		echo json_encode($response);// it's array
	}

	public function actionAttach_claim_doc_ajax($claim_id){
		$doc_ids = $_POST['doc_ids'];
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $doc_ids);
		$docs = OfficialDoc::model()->findAll($criteria);
		if (OfficialDoc::model()->updateAll(array('item_id'=>$claim_id), $criteria)){
			// send email to both CS emails
			Yii::import('application.extensions.phpmailer.JPhpMailer');
				
			$claim = CustomerOrderShipmentClaim::model()->findByPk($claim_id);
			$shipper = $claim->shipment->shipper;
			$shipper_name = $shipper->profile->firstname.' '.$shipper->profile->lastname;
			$customer_name = $claim->shipment->customer_order->billing_info->shipping_name.' '.$claim->shipment->customer_order->billing_info->shipping_last_name;
				
			$subject = str_replace('[tracking_number]', $claim->shipment->tracking_number, AutoMailMessage::MS_CLAIM_DOC_UPLOADED);
			$search = array('[name]','[customer_name]','[tracking_number]','[quote_number]');
			$replace = array($shipper_name, $customer_name, $claim->shipment->tracking_number, $claim->shipment->selected_user_bid->rate_quote_number);
			$body = str_replace($search, $replace, AutoMailMessage::MC_CLAIM_DOC_UPLOADED);
			// 			die($body);
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->email);
			foreach ($docs as $doc){
				$mail->AddAttachment(Yii::getPathOfAlias('webroot') .'/'.$doc->path);
			}
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
			// TODO: this is stupid, this BCC issue should be resolved
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->claim_email);
			foreach ($docs as $doc){
				$mail->AddAttachment(Yii::getPathOfAlias('webroot') .'/'.$doc->path);
			}
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Failed to save file info';
		}

		echo json_encode($result);
	}

	public function actionResolve_claim_ajax($claim_id){
		$claim = CustomerOrderShipmentClaim::model()->with('shipment', 'shipment.shipper','shipment.shipper.profile')->findByPk($claim_id);
		$claim->claim_resolved = 1;
		$claim->claim_resolved_date = date('Y-m-d H:i:s');
		if ($claim->save()){
			foreach ($claim->claim_docs as $doc){
				$doc->delete();
			}
			// send CS notification
			Yii::import('application.extensions.phpmailer.JPhpMailer');
				
			$shipper = $claim->shipment->shipper;
			$shipper_name = $shipper->profile->firstname.' '.$shipper->profile->lastname;
				
			$subject = AutoMailMessage::MS_CLAIM_RESOLVED;
			$search = array('[name]','[tracking_number]');
			$replace = array($shipper_name, $claim->shipment->tracking_number);
			$body = str_replace($search, $replace, AutoMailMessage::MC_CLAIM_RESOLVED);
				
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->claim_email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
				
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
				
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}

		echo json_encode($result);
	}

	public function actionDownload_claim_doc($id){
		$doc = OfficialDoc::model()->with('shipment')->findByPk($id);
		if ($doc->type != OfficialDoc::$TYPE_CLAIM){
			return false;
		}
		if (YumUser::isCustomerShipper(false) && $doc->shipment->shipper_id != Yii::app()->user->id){
			return false;
		}
		$doc->download();
	}

	public function actionAuthorize_return_form_ajax($shipment_id) {
		$shipment = CustomerOrderShipment::model()->findByPk($shipment_id);
// 		$items = $shipment->damaged_ind_items;
		$items = $shipment->scanned_out_ind_items;

		$this->renderPartial('_authorize_return_form', array(
				'items' => $items,
				'shipment' => $shipment,
		));
	}

	public function actionShipments(){
		// data for scheduled tab
		$shipment = new CustomerOrderShipment();
		$date = isset($_GET['date']) ? $_GET['date'] : false;
		$data_w_bol = $shipment->searchScheduled($date, true);
		$data_wo_bol = $shipment->searchScheduled($date, false);
		
		$model = new CustomerOrderShipment();
		if (isset($_GET['CustomerOrderShipment']['_filter_customer_name'])){
			$model->_filter_customer_name = $_GET['CustomerOrderShipment']['_filter_customer_name'];
		}
		
		// get dates for highlighting
		$sql = "SELECT date_format( pickup_date, '%m/%d/%Y' ) as `date`
		FROM customer_order_shipment
		WHERE STATUS = '".CustomerOrderShipment::$STATUS_SHIPPER_PICKUP."'
		GROUP BY date_format( pickup_date, '%j' )";
		$result = Yii::app()->db->createCommand($sql)->queryAll();

		$dates = array();
		foreach ($result as $row){
			$dates[] = '"'.$row['date'].'"';
		}

		$this->render('shipments', array(
				'dataProviderWithBol' => $data_w_bol,
				'dataProviderWithoutBol' => $data_wo_bol,
				'highlightDates'=>$dates,
				'shipmentModel'=>$model
		));
	}

	public function actionChargebacks_claims(){
        $customerOrderClaim = new CustomerOrderShipmentClaim('search');
        $customerOrderClaim->attributes = Yii::app()->request->getParam('CustomerOrderShipmentClaim');
		$this->render('chargebacks_claims', array(
				'customerOrderModel'=>new CustomerOrder(),
				'shipmentModel'=>new CustomerOrderShipment(),
				'claimModel'=> $customerOrderClaim,
		));
	}
	
	public function actionClaim_notes_window_ajax($claim_id){
		$claim = CustomerOrderShipment::model()->findByPk($claim_id);
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_shipment_id', $claim_id);
		$this->renderPartial('_claim_notes', array(
				'claim'=>$claim,
				'notes'=>CustomerNote::model()->findAll($criteria),
		));
	}
	
	public function actionAdd_claim_note_ajax($claim_id){
		$text = nl2br($_POST['text']);
		$text = '<strong>Claim #'.$claim_id.'</strong>: '.$text;
		$claim = CustomerOrderShipment::model()->with('customer_order', 'customer_order.customer')->findByPk($claim_id);
		if ($claim->customer_order->customer->addNote($text, $claim)){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['messages'] = 'Error saving a note';
		}
		
		echo json_encode($result);
	}
}