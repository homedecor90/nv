<?php

class ReportsController extends Controller
{

    public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'lostsale',
					'sales_profit',
					'customer_shipping',
					'supplier',
					'sales_items',
				),
				'users'=>array('*'),
				'expression' => 'UrlPermission::checkUrlPermission',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
    public function actionLostsale(){
        $model=new CustomerOrderItems();
		
		$this->render('lostsale',array(
			'model'=>$model
		));
    }
    
    public function actionSales_profit(){
    	$model=new CustomerOrder('search');
    	$date_from 	= isset($_GET['date_from']) && $_GET['date_from'] ? date('Y-m-d H:i:s', strtotime($_GET['date_from'])) : false;
    	$date_to 	= isset($_GET['date_to']) && $_GET['date_to'] ? date('Y-m-d H:i:s', strtotime($_GET['date_to'])) : false;
    	
    	$criteria = new CDbCriteria();
		if ($date_from && $date_to){
			$criteria->addCondition('sold_date >= "'.$date_from.'"');
			$criteria->addCondition('sold_date <= ADDDATE("'.$date_to.'", 1)');
		}
        $criteria->compare('status', CustomerOrder::$STATUS_SOLD);
        $criteria->compare('shipping_status', '<>'.CustomerOrder::$SHIPPING_STATUS_CANCELLED);
        $criteria->compare('shipping_status', '<>'.CustomerOrder::$SHIPPING_STATUS_CHARGEBACK);

        $model->attributes = $_GET['CustomerOrder'];

        $criteria->with = array('billing_info');
        $criteria->together = true;
        $criteria->compare('billing_info.billing_state', $model->state);
       // $criteria->compare('status1', CustomerOrder::$STATUS_SOLD);
        $row = $model->find($criteria);

        $total_profit = 0;
        $total_paid = 0;
        $orders = $model->findAll($criteria);
        foreach ($orders as $order){
            $total_profit   += $order->profit;
            $total_paid     += $order->getTotalPaidAmount();
        }

    	$this->render('sales_profit',array(
    			'model'=>$model,
    			'date_from'=>$date_from,
    			'date_to'=>$date_to,
    			'total_profit'=>$total_profit,
    			'total_paid'=>$total_paid
    	));
    }
    
    public function actionSales_items(){
    	$model=new Individualitem();

        $date_from 	= isset($_GET['date_from']) && $_GET['date_from'] ? date('Y-m-d H:i:s', strtotime($_GET['date_from'])) : false;
        $date_to 	= isset($_GET['date_to']) && $_GET['date_to'] ? date('Y-m-d H:i:s', strtotime($_GET['date_to'])) : false;

    	$this->render('sales_items',array(
    		'model'=>$model,
            'date_from'=>$date_from,
            'date_to'=>$date_to,
    	));
    }
    
    public function actionCustomer_shipping(){
    	$model = new CustomerOrderShipment();
    	
    	$list = YumUser::getCustomerShipperUserList(true);
    	$shipper_list = array(0=>'');
    	foreach($list as $user){
    		$shipper_list[$user->id] = $user->profile->company;
    	}
    	
    	$date_from 	= isset($_GET['date_from']) && $_GET['date_from'] ? date('Y-m-d H:i:s', strtotime($_GET['date_from'])) : false;
    	$date_to 	= isset($_GET['date_to']) && $_GET['date_to'] ? date('Y-m-d H:i:s', strtotime($_GET['date_to'])) : false;
    	$shipper_id	= isset($_GET['shipper_id']) ? $_GET['shipper_id'] : false;
    	$filter_by	= isset($_GET['filter_by']) ? $_GET['filter_by'] : false;
    	
    	$criteria = new CDbCriteria();
    	$criteria->addCondition('t.status = "'.CustomerOrderShipment::$STATUS_SCANNED_OUT.'" OR t.status="'.CustomerOrderShipment::$STATUS_COMPLETED.'"');
    	
    	if ($date_from && $date_to){
    		if ($filter_by == 'pickup'){
    			$criteria->addCondition('t.pickup_date >= "'.$date_from.'"');
    			$criteria->addCondition('t.pickup_date <= ADDDATE("'.$date_to.'", 1)');
    			$criteria->with = 'customer_order';
    			$criteria->addCondition('customer_order.shipping_method = "'.CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP.'"');
    		} elseif ($filter_by == 'payment'){
    			$criteria->addCondition('paid_date >= "'.$date_from.'"');
    			$criteria->addCondition('paid_date <= ADDDATE("'.$date_to.'", 1)');
    		}
    	}
    	
    	if ($shipper_id){
    		$criteria->compare('t.shipper_id', $shipper_id);
    	}
    	$rows = $model->findAll($criteria);
    	
    	$total_paid = 0; 
    	foreach ($rows as $row){
    		$total_paid += $row->selected_user_bid->rate_quote;
    	}

    	$this->render('customer_shipping',array(
    			'model'=>$model,
    			'date_from'=>$date_from,
    			'date_to'=>$date_to,
    			'shipper_id'=>$shipper_id,
    			'total_paid'=>$total_paid,
    			'shippers'=>$shipper_list
    	));
    }
    
    public function actionSupplier(){
    	$model = new SupplierOrder();
    	$date_from 	= isset($_GET['date_from']) && $_GET['date_from'] ? date('Y-m-d H:i:s', strtotime($_GET['date_from'])) : false;
    	$date_to 	= isset($_GET['date_to']) && $_GET['date_to'] ? date('Y-m-d H:i:s', strtotime($_GET['date_to'])) : false;
//
    	$criteria = new CDbCriteria();
    	if ($date_from && $date_to){
    		$criteria->addCondition('created_date >= "'.$date_from.'"');
    		$criteria->addCondition('created_date <= ADDDATE("'.$date_to.'", 1)');
    	}
    	$orders = $model->findAll($criteria);

        $total_fob = $total_exw = 0;


        foreach($orders as $order){
            $term = $order->getShippingTerms();
            if ($term === false){
                $total_fob += $order->getFOBTotal();
                $total_exw += $order->getEXWTotal();
            } elseif($term == 'FOB') {
                $total_fob += $order->getFOBTotal();
                $total_exw += $order->getFOBTotal();
            } elseif($term == 'EXW') {
                $total_fob += $order->getEXWTotal();
                $total_exw += $order->getEXWTotal();
            }
        }
    	 
    	$this->render('supplier',array(
    		'model'=>$model,
    		'date_from'=>$date_from,
    		'date_to'=>$date_to,
            'total_fob'=>$total_fob,
            'total_exw'=>$total_exw
    	));
    }
}