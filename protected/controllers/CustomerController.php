<?php
class CustomerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    
    public $stats = null;
	public $profit = null;
    
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

    public function actions(){
        return array(
            'captcha'=>array(
                //'class'=>'CCaptchaAction',
                'class'=>'application.extensions.CaptchaAction',
                'fontFile' => 'd:\OpenServer\domains\midmodfurnishings\framework\web\widgets\captcha\Duality.ttf'
            ),
        );
    }
    public function actionEcaptcha()
    {
        $this->renderPartial('ecaptcha');
    }
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(
					'index',
					'view',
					'checkout_order',
					'checkout_order_by_admin',
					'complete_order',
					'admin',
					'delete',
					'create',
					'update',
					'order_detail',
					'add_new_note_ajax',
					'add_new_followup_ajax',
					'add_new_order_ajax',
					'add_new_osc_order_ajax',
					'send_invoice_ajax',
					'send_customer_reminder_ajax',
					'sell_order_items',
					'sell_order_items_by_ajax',
					'import_osc_orders',
					'archived_osc_orders',
					'add_osc_order',
					'add_new_customer_ajax',
					'add_3rd_party_shipment_ajax',
					'check_existing_customer_ajax',
					'import_osc_data_ajax',
					'check_name_duplicated_ajax',
					'get_today_followup_list_ajax',
					'dismiss_cusomter_followup',
					'alert_cusomter_followup',
					'delete_order_items_ajax',
                    'update_order_items_ajax',
                    'add_order_items_ajax',
                    'set_order_add_tax_ajax',
                    'set_order_shipping_method_ajax',
                    'set_order_completed_ajax',
                    'apply_order_standard_discount_ajax',
                    'apply_order_custom_discount_ajax',
                    'set_order_lost_sale_ajax',
                    'set_order_dead_ajax',
                    'contact',
                    'pending',
                    'delete_pending_customer_ajax',
                    'sellallcompletedorders',
                    'sold_orders',
                    'fix_osc_orders',
					'edit_order_notes_ajax',
					'pending_approval_orders',
					'approve_order_ajax',
					'deny_order_ajax',
					'pickup_date',
					'additional_payment',
					'review',
					'order_shipping_info_verification_request_ajax',
					'verify_order_shipping_info_ajax',
					'find_customers_results',
					'get_customers_names_ajax',
					'order_details_json_ajax',
					'delete_osc_order_ajax',
					'reviews',
					'unsubscribe',
					'issue_refund_ajax',
					'create_newsletter_ajax',
                    'createRetailerOrder',
                    'checkout_order_by_retailer',
                    'retailer_new_order_ajax',
                    'checkoutOrderBraintree',
				),
				'users'=>array('*'),
				'expression' => 'UrlPermission::checkUrlPermission',
			),
            array('allow',
                'actions' => array(
                    'deleteReview',
                    'repost',
                    'editReview'
                ),
                'users' => array('admin')
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionAdd_new_note_ajax(){
		$newCustomerNoteModel = new CustomerNote;
		
		$result['message'] = "fail";
		if(isset($_POST['CustomerNote'])){
			$_POST['CustomerNote']["created_on"] = date("Y-m-d H:i:s", time());
						
			$newCustomerNoteModel->attributes = $_POST['CustomerNote'];
			if ($newCustomerNoteModel->save()){
				$result['message'] = "success";
			}
			
			$customer_id = $newCustomerNoteModel->customer_id;
			
			////////////////////////////////////////////////////////////
			// Added Activity
			Activity::model()->addActivity_CreateCustomerNote($customer_id);
		}		
		echo json_encode($result);
	}
	
	public function actionAdd_new_followup_ajax(){
		$newCustomerFollowupModel = new CustomerFollowup;
		
		$result['message'] = "fail";
		if(isset($_POST['CustomerFollowup'])){
			$_POST['CustomerFollowup']["created_on"] = date("Y-m-d H:i:s", time());
						
			$newCustomerFollowupModel->attributes = $_POST['CustomerFollowup'];
			$newCustomerFollowupModel->followup_time = date("Y-m-d H:i:s", strtotime($newCustomerFollowupModel->followup_time));
			$newCustomerFollowupModel->status = "Followed";
			if ($newCustomerFollowupModel->save()){
				$result['message'] = "success";
			}
			
			$customer_id = $newCustomerFollowupModel->customer_id;
			
			////////////////////////////////////////////////////////////
			// Added Activity
			Activity::model()->addActivity_CreateCustomerFollowup($customer_id);
		}		
		echo json_encode($result);
	}

    /**
     * Creates retailer order
     * @throws CException if order creation failed
     */
    public function actionRetailer_new_order_ajax() {
        //check if pending order for this user
        //with selected shipping method exists within session
        $orderIds = Yii::app()->session['order'];
        if (isset($orderIds[$_POST['CustomerOrder']['shipping_method']])) {
            $customerOrder = CustomerOrder::model()->findByPk(
                $orderIds[$_POST['CustomerOrder']['shipping_method']]
            );
            if ($customerOrder->status != CustomerOrder::STATUS_PENDING) {
                unset($customerOrder, $orderIds[$_POST['CustomerOrder[shipping_method]']]);
            }
        }
        //if order not exist create new
        if (empty($customerOrder) && isset($_POST['CustomerOrder'])) {
            $customerId = RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id);
            $customerOrder = new CustomerOrder();
            $customerOrder->setAttributes($_POST['CustomerOrder']);
            $customerOrder->setAttributes(array(
                'customer_id' => $customerId,
                'status' => CustomerOrder::STATUS_PENDING,
                'created_on' => date("Y-m-d H:i:s", time()),
                'shipping_status' => CustomerOrder::$SHIPPING_STATUS_PENDING,
                'customerId' => RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id),
            ));
            $customerOrder->approved = 1;
            if ($customerOrder->save()) {
                //add order id to session
                $orderIds[$customerOrder->shipping_method] = $customerOrder->id;
                Yii::app()->session->add('order', $orderIds);
            } else {
                throw new CException('Cannot save order');
            }
        }
        //add items to this order
        if (isset($_POST['item']['item_id'])) {
            foreach($_POST['item']['item_id'] as $key => $itemId) {
                $specialShippingPrice = isset($_POST['item']['special_shipping_price'][$key]) ?
                    $_POST['item']['special_shipping_price'][$key] : 0;
                $customerOrder->addItem($itemId, $_POST['item']['quantity'][$key],
                    $_POST['item']['custom_color'][$key], $specialShippingPrice);
            }
            if (!$customerOrder->save()) {
                throw new CException('Error processing order');
            }
        }
        echo CJSON::encode(array('message' => 'success'));
    }


	public function actionAdd_new_order_ajax(){
		$newCustomerOrderModel = new CustomerOrder;
		
		$result['message'] = "fail";
		if(isset($_POST['CustomerOrder'])){
			$_POST['CustomerOrder']["status"] = CustomerOrder::$STATUS_PENDING;
			$_POST['CustomerOrder']["created_on"] = date("Y-m-d H:i:s", time());
			
			// create new customer order
			$newCustomerOrderModel->attributes = $_POST['CustomerOrder'];
            $newCustomerOrderModel->approved = 1;
            $newCustomerOrderModel->shipping_status = CustomerOrder::$SHIPPING_STATUS_PENDING;

			if ($newCustomerOrderModel->save()){
				$result['message'] = "success";
				$customerOrderId = $newCustomerOrderModel->attributes['id'];

                if (isset($_POST['item']['item_id'])) {
                    $itemIdList = $_POST['item']['item_id'];
                    // add the items with quantity to the order
                    foreach ($itemIdList as $key => $itemId){
                        $item = Item::model()->findByPk($itemId);
                        $attributes = array(
                            "customer_order_id"			=>	$customerOrderId,
                            "item_id"					=>	$itemId,
                            "quantity"					=>	$_POST['item']['quantity'][$key],
                            "custom_color"				=>	$_POST['item']['custom_color'][$key],
                            "exw_cost_price"			=>	$item->exw_cost_price,
                            "fob_cost_price"			=>	$item->fob_cost_price,
                            "sale_price"				=>	$item->sale_price,
                        );
                        if (isset($_POST['item']['special_shipping_price'][$key])) {
                            $attributes["special_shipping_price"] = $_POST['item']['special_shipping_price'][$key];
                        } else $attributes["special_shipping_price"] = 0;

                        $newCustomterOrderItemsModel = new CustomerOrderItems;

                        $newCustomterOrderItemsModel->attributes = $attributes;
                        $newCustomterOrderItemsModel->save();

                    }

                    if ($_POST['CustomerOrder']['discounted'] == 'Discounted'){
                        if (!$_POST['CustomerOrder']['custom_discount']){
                            $newCustomerOrderModel->discounted_total = CustomerOrder::model()->getCustomerOrderDiscountedTotal($newCustomerOrderModel->id, $newCustomerOrderModel->grand_total_price);
                            $newCustomerOrderModel->save();
                        }
                    }

                    ////////////////////////////////////////////////////////////
                    // Added Activity
                    Activity::model()->addActivity_CreateCustomerOrder($customerOrderId);
                }
			}
		}

		echo json_encode($result);
	}
	
	public function actionAdd_3rd_party_shipment_ajax(){
		if (!$_POST['Customer']['email']){
			$result['status'] = "fail";
			$result['message'] = "Please enter customer email";
			echo json_encode($result);
			return;
		}
		
		$criteria = new CDbCriteria();
        $criteria->compare('email', $_POST['Customer']['email']);
        if (!($customer = Customer::model()->find($criteria))){
        	$customer = new Customer();
			$name = explode(" ", $_POST['Customer']['name']);
            $first_name = isset($name[0])?$name[0]:"";
            $last_name = isset($name[1])?$name[1]:"";
            $customer = new Customer();
            $customer->email = $_POST['Customer']['email'];
            $customer->heard_through = 'Third party / '.$_POST['sold_on'];
            $customer->first_name = $first_name;
            $customer->last_name = $last_name;
            $customer->phone = $_POST['Customer']['phone'];
            $customer->created_on = date("Y-m-d H:i:s");
            if ($customer->save()){
				$customer->addNote('Customer created for third-party shipment');
			} else {
				$result['status'] = "fail";
				$result['message'] = "Failed to save shipment";
				echo json_encode($result);
				return;
			}
		}
		
		$newCustomerOrderModel = new CustomerOrder;
		
		$result['status'] = "fail";
		if(isset($_POST['CustomerOrder'])){
			$_POST['CustomerOrder']["status"] = CustomerOrder::$STATUS_PENDING;
			$_POST['CustomerOrder']["created_on"] = date("Y-m-d H:i:s", time());
			
			// create new customer order
			$newCustomerOrderModel->attributes = $_POST['CustomerOrder'];
			$newCustomerOrderModel->approved = Yii::app()->user->isAdmin() ? 1 : 0;
			$newCustomerOrderModel->sales_person_id = Yii::app()->user->id;
            $newCustomerOrderModel->shipping_status = CustomerOrder::$SHIPPING_STATUS_PENDING;
            $newCustomerOrderModel->customer_id = $customer->id;
			
			if ($newCustomerOrderModel->save()){
				$result['status'] = "success";
				$customerOrderId = $newCustomerOrderModel->attributes['id'];
				$itemIdList = $_POST['item']['item_id'];
				
				// add the items with quantity to the order
				foreach ($itemIdList as $key => $itemId){
					$item = Item::model()->findByPk($itemId);
					$attributes = array(
						"customer_order_id"			=>	$customerOrderId,
						"item_id"					=>	$itemId,
						"quantity"					=>	$_POST['item']['quantity'][$key],
						"custom_color"				=>	$_POST['item']['custom_color'][$key],
						"exw_cost_price"			=>	$item->exw_cost_price,
						"fob_cost_price"			=>	$item->fob_cost_price,
						"sale_price"				=>	$item->sale_price,
						"special_shipping_price"	=>	$_POST['item']['special_shipping_price'][$key],
					);
					
					$newCustomterOrderItemsModel = new CustomerOrderItems;
					
					$newCustomterOrderItemsModel->attributes = $attributes;
					$newCustomterOrderItemsModel->save();
				}
				
                if ($_POST['CustomerOrder']['discounted'] == 'Discounted'){
                    if (!$_POST['CustomerOrder']['custom_discount']){
                            $newCustomerOrderModel->discounted_total = CustomerOrder::model()->getCustomerOrderDiscountedTotal($newCustomerOrderModel->id, $newCustomerOrderModel->grand_total_price);
                            $newCustomerOrderModel->save();
                        }
                }
				$customer->addNote('New order added as third party shipment');
                
				if (Yii::app()->user->isAdmin()){
					$this->sellOrder($newCustomerOrderModel->id);
				} else {
					$result['message'] = 'Shipment will be added after approval';
				}

				$billing_info = new CustomerOrderBillingInfo();
				$billing_info->customer_order_id = $newCustomerOrderModel->id;
				$name = explode(" ", $_POST['Customer']['name']);
				$first_name = isset($name[0])?$name[0]:"";
				$last_name = isset($name[1])?$name[1]:"";
				
				$billing_info->billing_name 			= $first_name;
				$billing_info->billing_last_name 		= $last_name;
				$billing_info->billing_street_address 	= $_POST['Customer']['street'];
				$billing_info->billing_city 			= $_POST['Customer']['city'];
				$billing_info->billing_state 			= $_POST['Customer']['state'];
				$billing_info->billing_zip_code			= $_POST['Customer']['zip_code'];
				$billing_info->billing_phone_number		= $_POST['Customer']['phone'];
				$billing_info->shipping_name 			= $first_name;
				$billing_info->shipping_last_name 		= $last_name;
				$billing_info->shipping_street_address 	= $_POST['Customer']['street'];
				$billing_info->shipping_city 			= $_POST['Customer']['city'];
				$billing_info->shipping_state 			= $_POST['Customer']['state'];
				$billing_info->shipping_zip_code		= $_POST['Customer']['zip_code'];
				$billing_info->shipping_phone_number	= $_POST['Customer']['phone'];
				$billing_info->order_notes 				= '(THIRD PARTY SHIPMENT)';
				$billing_info->save();
				////////////////////////////////////////////////////////////
				// Added Activity
				Activity::model()->addActivity_CreateCustomerOrder($customerOrderId);
				
				// send email notification to admin
				// TODO: rewrite all email code that requires BCC using Yii::app()->email instead of PHPMailer
				$email = Yii::app()->email;
				
				$admin_email = AutoMail::getAdminEmail();
				$email->to = $admin_email;
				$email->from = $admin_email;
				$email->subject = AutoMailMessage::MS_ADMIN_3RD_PARTY_ORDER_ADDED;
				
				$link = Yii::app()->getBaseUrl(true).'/index.php/customer/pending_approval_orders';
				$email->message = str_replace('[link]', $link, AutoMailMessage::MC_ADMIN_3RD_PARTY_ORDER_ADDED);
				$email->send();
			} else {
				print_r($newCustomerOrderModel->getErrors());
				die();
			}
		}
		
		echo json_encode($result);
	}
	
	public function actionAdd_new_osc_order_ajax($osc_order_id, $osc_site_id){
		$newCustomerOrderModel = new CustomerOrder('importOscOrder');
		
		$result['message'] = "fail";
		if(isset($_POST['CustomerOrder'])){
            $oscOrder = OscOrders::model()->findByPk(array(
                'orders_id' => $osc_order_id, 'site_id' => $osc_site_id
            ));
			$_POST['CustomerOrder']["status"] = CustomerOrder::$STATUS_SOLD;
			$_POST['CustomerOrder']["created_on"] = date("Y-m-d H:i:s", time());
			
			// create new customer order
			$newCustomerOrderModel->attributes = $_POST['CustomerOrder'];
			$newCustomerOrderModel->approved = 1;
            $newCustomerOrderModel->customer_id = $oscOrder->getCustomerID();
			if ($newCustomerOrderModel->save()){
				$result['message'] = "success";
				$customerOrderId = $newCustomerOrderModel->attributes['id'];
				$itemIdList = $_POST['item']['item_id'];
				
				// add the items with quantity to the order
				foreach ($itemIdList as $key => $itemId){
					$item = Item::model()->findByPk($itemId);
					$attributes = array(
						"customer_order_id"			=>	$customerOrderId,
						"item_id"					=>	$itemId,
						"quantity"					=>	$_POST['item']['quantity'][$key],
                        "custom_color"				=>	$_POST['item']['custom_color'][$key],
						"exw_cost_price"			=>	$item->exw_cost_price,
						"fob_cost_price"			=>	$item->fob_cost_price,
						"sale_price"				=>	$item->sale_price,
						"special_shipping_price"	=>	$_POST['item']['special_shipping_price'][$key],
					);
					
					$newCustomterOrderItemsModel = new CustomerOrderItems;
					
					$newCustomterOrderItemsModel->attributes = $attributes;
					$newCustomterOrderItemsModel->save();
                    
				}
				
				$newCustomerOrderModel->importOscOrderBillingInfo($osc_order_id, $osc_site_id);
                $newCustomerOrderModel->grand_total_price = CustomerOrder::getCustomerOrderTotalPrice($newCustomerOrderModel->id);
                $newCustomerOrderModel->setCorrectOrderDiscountedTotal();
                $newCustomerOrderModel->save();
                $this->sellOrder($newCustomerOrderModel->id, $oscOrder->date_purchased);
				
				////////////////////////////////////////////////////////////
				// Added Activity
				Activity::model()->addActivity_CreateCustomerOrder($customerOrderId);
                $newCustomerOrderModel->sendTransactionConfirmation();
			} else {
				$result["error"] = CHtml::errorSummary($newCustomerOrderModel);
			}
		}
		
		echo json_encode($result);
	}
	
	public function actionSend_invoice_ajax($order_id){
		$result['message'] = "fail";
		
		$customerOder =  CustomerOrder::model()->findByPk($order_id);
		if ($customerOder == null){
			$result['message'] = "fail";
			echo json_encode($result);
			return;
		}
		
		$customer = Customer::model()->findByPk($customerOder->customer_id);
		if ($customerOder == null){
			$result['message'] = "fail";
			echo json_encode($result);
			return;
		}
		
		////////////////////////////////////////////////////////////
		// Added Activity
		Activity::model()->addActivity_SendInvoiceForCustomerOrder($order_id);
		
		$total_price    = CustomerOrder::model()->getCustomerOrderTotalPrice($order_id);
        $dsc            = ($customerOder->discounted == 'Discounted');
        $dsc_total      = $customerOder->discounted_total;
        $dsc_amount     = $total_price - $dsc_total;
        
        $total_price    = number_format($total_price, 2);
        $dsc_total      = number_format($dsc_total, 2);
        $dsc_amount     = number_format($dsc_amount, 2);
        
		$user_id = Yii::app()->user->id;
		
		$adminModel = YumUser::model()->findByPk(1);
		$salesPersonModel = YumUser::model()->findByPk($customerOder->sales_person_id);
		$salesPersonMailAddress = $salesPersonModel->profile->email;
		$adminMailAddress = $adminModel->profile->email;
		
		$email = Yii::app()->email;
		
		$email->to = $customer->email;
		//~ $email->from = $salesPersonMailAddress;
		$email->from = Customer::NOTIFICATION_EMAIL;
		$email->bcc = "support@regencyshop.com,".$adminMailAddress.",".$salesPersonMailAddress;
		$email->subject = 'Order Information';
		
		$checkout_url = $customerOder->getPublicCheckoutURL();
		$content = 'Hello '. $customer->first_name." ".$customer->last_name.'<br/>'
			.'As discussed your total investment for all the items listed below is only $'.($dsc ? $dsc_total : $total_price).'<br/><br/>'
			.CustomerOrder::model()->getCustomerOrderItemsForInvoice($order_id).'<br/><br/>'
            .($dsc ? 'Regular price: $'.$total_price.'<br />Discount: $'.$dsc_amount.'<br /><br />Your investment: $'.$dsc_total : 'Your investment: $'.$total_price).'<br />'
			.'FREE SHIPPING!<br />I can assure you that you will love our furniture and will also refer us to your friends.<br/><br/>'
			.'You can securely make the payment at the link below:<br/><br/>'
			.'<a href="'.$checkout_url.'">'.$checkout_url.'</a><br/><br/>'
			.'We will process your order as soon as you make the payment. Feel free to call me anytime at '.Settings::getVar('sales_phone_number').'.<br/><br/>'
			.'Looking forward to serving all your furniture needs,<br />'	
			.'Lisa G.'
			.'<br /><br />'
			.'Note: Prices are subject to change without notice. If you do not purchase this item within 7 days of this invoice email, then price difference will have to be charged for the order to be processed.'
			;
		
		$email->message = $content;
		$email->send();
		
		$result['message'] = "success";
		echo json_encode($result);
	}
    
    public function actionSend_customer_reminder_ajax($order_id){
        $result['message'] = "fail";
		
		$customerOrder =  CustomerOrder::model()->findByPk($order_id);
		if ($customerOrder == null){
			$result['message'] = "fail";
			echo json_encode($result);
			return;
		}
		
		$customer = Customer::model()->findByPk($customerOrder->customer_id);
		if ($customerOrder == null){
			$result['message'] = "fail";
			echo json_encode($result);
			return;
		}
		
		////////////////////////////////////////////////////////////
		// Added Activity
		// Activity::model()->addActivity_SendInvoiceForCustomerOrder($order_id);
        
        if ($customerOrder->sendCustomerReminder()){
			$result['message'] = "success";
		} else {
			$result['message'] = "fail";
		}
		
		echo json_encode($result);
    }
    
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		/**
			Add Item
		*/
		$profit = array();
		$newItemModel = new Item;
		
		$newCustomerNoteModel = new CustomerNote;		
		$newCustomerFollowupModel = new CustomerFollowup;
		$newCustomerOrderModel = new CustomerOrder;
	
		/**
			Get the Sales Person List
		*/
		$salesPersonList = YumUser::model()->getSalesPersonList();
		
		/**
			Get the Note Data Provider for Search
		*/		
		$customerNoteModel = new CustomerNote('search');
		$customerNoteModel->unsetAttributes();  // clear any default values
		$customerNoteModel->setAttribute('customer_id', $id);
		
		/**
			Get the Followup Data Provider for Search
		*/
		$customerFollowupModel = new CustomerFollowup('search');
		$customerFollowupModel->unsetAttributes();  // clear any default values
		$customerFollowupModel->setAttribute('customer_id', $id);
		
		/**
			Get the Order Data Provider for Search
		*/
		$customerOrderModel = new CustomerOrder('search');
		$customerOrderModel->unsetAttributes();  // clear any default values
		$customerOrderModel->setAttribute('customer_id', $id);
		$itemCodeList = Item::model()->getAllItemCodeList();
		$itemNameList = Item::model()->getAllItemNameList();
        
        $settings = Settings::model()->getSettingVariables();
        $tax_percent = number_format($settings->tax_percent, 2);

		$profit = CustomerOrder::getCustomerProfit($id);
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'customerNoteModel'=>$customerNoteModel,
			'customerFollowupModel'=>$customerFollowupModel,
			'customerOrderModel'=>$customerOrderModel,
			'newCustomerNoteModel'=>$newCustomerNoteModel,
			'newCustomerFollowupModel'=>$newCustomerFollowupModel,
			'newCustomerOrderModel'=>$newCustomerOrderModel,
			'newItemModel'=>$newItemModel,
			'itemCodeList'=>$itemCodeList,
			'itemNameList'=>$itemNameList,
			'salesPersonList'=>$salesPersonList,
			'sales_tax_percent'=>$tax_percent,
			'profit'=>$profit,
		));
	}

    public function actionCreateRetailerOrder()
    {
        $this->layout = "column1";

        $id = RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id);
        $profit = array();
        $newItemModel = new Item;

        $newCustomerNoteModel = new CustomerNote;
        $newCustomerFollowupModel = new CustomerFollowup;
        $newCustomerOrderModel = new CustomerOrder;

        /**
        Get the Sales Person List
         */
        $salesPersonList = YumUser::model()->getSalesPersonList();

        /**
        Get the Note Data Provider for Search
         */
        $customerNoteModel = new CustomerNote('search');
        $customerNoteModel->unsetAttributes();  // clear any default values
        $customerNoteModel->setAttribute('customer_id', $id);

        /**
        Get the Followup Data Provider for Search
         */
        $customerFollowupModel = new CustomerFollowup('search');
        $customerFollowupModel->unsetAttributes();  // clear any default values
        $customerFollowupModel->setAttribute('customer_id', $id);

        /**
        Get the Order Data Provider for Search
         */
        $customerOrderModel = new CustomerOrder('search');
        $customerOrderModel->unsetAttributes();  // clear any default values
        $customerOrderModel->setAttribute('customer_id', $id);
        $itemCodeList = Item::model()->getAllItemCodeList();
        $itemNameList = Item::model()->getAllItemNameList();

        $settings = Settings::model()->getSettingVariables();
        $tax_percent = number_format($settings->tax_percent, 2);

        $profit = CustomerOrder::getCustomerProfit($id);

        $this->render('retailerOrder',array(
            'model'=>$this->loadModel($id),
            'customerNoteModel'=>$customerNoteModel,
            'customerFollowupModel'=>$customerFollowupModel,
            'customerOrderModel'=>$customerOrderModel,
            'newCustomerNoteModel'=>$newCustomerNoteModel,
            'newCustomerFollowupModel'=>$newCustomerFollowupModel,
            'newCustomerOrderModel'=>$newCustomerOrderModel,
            'newItemModel'=>$newItemModel,
            'itemCodeList'=>$itemCodeList,
            'itemNameList'=>$itemNameList,
            'salesPersonList'=>$salesPersonList,
            'sales_tax_percent'=>$tax_percent,
            'profit'=>$profit,
        ));
    }

    /**
     * Checkout the order
     */
    public function actionCheckout_order(){//$order_id
        $getParams = $this->getActionParams();

        $order_id = 0;

        if(isset($getParams['order_id']) && intval($getParams['order_id'])>0){
            $order_id = intval($getParams['order_id']);
            $this->redirect(Yii::app()->getBaseUrl(true)."/index.php/customer/checkout_order/".
                md5($order_id.CustomerOrder::$ORDER_SALT)."/".$order_id); //redirect old link to hash link
        } else {
            foreach ($getParams as $md5_sum=>$id){
                if(md5($id.CustomerOrder::$ORDER_SALT)==$md5_sum){ //hash link is true
                    $order_id = $id;
                }
            }
            if($order_id==0)
                throw new CHttpException(404,'The specified post cannot be found.');
        }

        $this->layout = "regencyshop";
        $customerOrder = $this->loadOrderModel($order_id);

        if ($customerOrder->discounted == "Discounted"){
            $amount = $customerOrder->discounted_total;
        } else {
            $amount = $customerOrder->grand_total_price;
        }

        $this->checkout($customerOrder, 'userCheckout');

    }

    /**
	 * Checkout the order
	 */
	public function _old_actionCheckout_order(){//$order_id
		$getParams = $this->getActionParams();
		
		$order_id = 0;
		
		if(isset($getParams['order_id']) && intval($getParams['order_id'])>0){
			$order_id = intval($getParams['order_id']);
			$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/customer/checkout_order/".
							md5($order_id.CustomerOrder::$ORDER_SALT)."/".$order_id); //redirect old link to hash link
		} else {
			foreach ($getParams as $md5_sum=>$id){
				if(md5($id.CustomerOrder::$ORDER_SALT)==$md5_sum){ //hash link is true
					$order_id = $id; 
				} 
			}
			if($order_id==0)
				throw new CHttpException(404,'The specified post cannot be found.');
		}
			
		$this->layout = "regencyshop";
		$customerOrderModel = CustomerOrder::model()->findByPk($order_id);
		$item_html = CustomerOrder::model()->getCustomerOrderItemsForInvoice($order_id);
		$customerOrderItemsModel = new CustomerOrderItems('search');
		$customerModel = Customer::model()->findByPk($customerOrderModel->customer_id);
        
		$pathinfo = pathinfo(__FILE__);
		$dir_path = $pathinfo['dirname'];
		
		require_once $dir_path.'/../extensions/anet_php_sdk/AuthorizeNet.php';
		
		$transaction_success = true;
		$error_message = "";
		
		$amount = 0;
		if ($customerOrderModel->discounted == "Discounted"){
	        $amount = $customerOrderModel->discounted_total;
		} else {
			$amount = $customerOrderModel->grand_total_price;
		}
		
		
		if (isset($_POST) && (count($_POST) > 0)){
			//~ $transaction = new AuthorizeNetAIM('5w376qdmRPLD', '46363rSbGmnEw28m');
			$transaction = new AuthorizeNetAIM(Customer::$authorize_api_login_id, Customer::$authorize_transaction_key);
			$transaction->setSandbox(false);
			//$transaction->amount = $_POST['amount'];
			$transaction->amount = $amount;
			$transaction->card_num = $_POST['authorizenet_cc_number'];
			$transaction->exp_date = $_POST['authorizenet_cc_expires_month']."/".$_POST['authorizenet_cc_expires_year'];
			// $transaction->setCustomField('Card name :',$_POST['authorizenet_cc_owner']);
            $transaction->setCustomField('Card name :',$_POST['fname'].' '.$_POST['lname']);
			$transaction->setCustomField('Card type :',$_POST['credit_card_type']);

			// $transaction->description = $_POST['desc'];
			// get plain text description
            $criteriaItems = new CDbCriteria;
            $criteriaItems->compare('customer_order_id',$order_id);
            $items = CustomerOrderItems::model()->findAll($criteriaItems);
            
            $items_text = array();
            foreach ($items as $item){
                $item_info = Item::model()->findByPk($item->item_id);
                $items_text[] = $item_info->item_name.': '.$item->quantity;
            }
            $transaction->description = implode(', ', $items_text);
			$transaction->first_name = $_POST['fname'];
            $transaction->last_name = $_POST['lname'];
			$transaction->address=$_POST['street_address'];
			$transaction->city=$_POST['city'];
			$transaction->state=$_POST['state'];
			$transaction->zip=$_POST['postcode'];
			$transaction->phone=$_POST['telephone'];
			$transaction->email=$_POST['email_address'];
			$transaction->setCustomField('The Shipment Adress :',$_POST['busrestype'].', '.$customerOrderModel->shipping_method);

			if ($_POST['sh_radio']!='0') {
				$transaction->ship_to_first_name = $_POST['ShipFirstName'];
				$transaction->ship_to_address=$_POST['ShipAddress'];
				$transaction->ship_to_city=$_POST['ShipCity'];
				$transaction->ship_to_state=$_POST['shipstate'];
				$transaction->ship_to_zip=$_POST['shippostcode'];
				$transaction->setCustomField('Shipment telephone:',$_POST['shiptelephone']);
			} else {
				$transaction->ship_to_first_name = $_POST['fname'];
				$transaction->ship_to_address=$_POST['street_address'];
				$transaction->ship_to_city=$_POST['city'];
				$transaction->ship_to_state=$_POST['state'];
				$transaction->ship_to_zip=$_POST['postcode'];
				$transaction->setCustomField('Shipment telephone:',$_POST['telephone']);
			}
			
			$response = $transaction->authorizeAndCapture();
			//if ($response->approved) {
			if (!$response->error && $response->approved == 1) {
			//if (true) {
				$customerOrderModel->status = CustomerOrder::$STATUS_COMPLETED;
				$customerOrderModel->save();
				$customerOrderModel->importAuthorizeNetTransctionBillingInfo($_POST);
				$customerOrderModel->addCustomerPayment(CustomerPayment::TYPE_ORDER_MAIN, $amount);
				////////////////////////////////////////////////////////////
				// Added Activity
				Activity::model()->addActivity_CompletePayCustomerOrder($order_id);
                
                $this->sellOrder($order_id);
			} else {
				if(isset($response->error_message)){
					$transaction_success = false;
					$error_message = $response->error_message;
					if (strpos($error_message, 'Response Reason Text:') !== false){
						$error_message = explode('Response Reason Text:', $error_message);
						$error_message = $error_message[1];
					}
				} else {
					if ($response->declined){
						$transaction_success = false;
						$error_message = $response->response_reason_text;
					}
				}

                $orderBillingInfoModel = CustomerOrderBillingInfo::getNewModelFromPost();
			}
		}

        if (!isset($orderBillingInfoModel)){
            $orderBillingInfoModel = new CustomerOrderBillingInfo();
        }

		$this->render('checkout_order',array(
			'customerOrderModel'=>$customerOrderModel,
			'customerOrderItemsModel'=>$customerOrderItemsModel,
            'customerModel'=>$customerModel,
			'transaction_success'=>$transaction_success,
			'error_message'=>$error_message,
            'orderBillingInfoModel'=>$orderBillingInfoModel,
			'response'=>$response,
		));
	}

    /**
     * @param $customerOrder
     * @param string $view
     */
    protected function checkout(CustomerOrder $customerOrder, $view = 'checkout') {
        $billingInfo = CustomerOrderBillingInfo::model()->with(array(
            'order' => array(
                'select' => false,
                'condition' => 'customer_id = :customerId',
                'params' => array(':customerId' => $customerOrder->customer_id),
                'order' => 'order.id DESC'
            )
        ))->find();
        if (empty($billingInfo)) $billingInfo = new CustomerOrderBillingInfo();
        $billingInfo->customer_order_id = $customerOrder->id;
        $billingInfo->scenario = 'checkout';

        if(isset($_POST['ajax']) && $_POST['ajax']==='checkout-form')
        {
            echo CActiveForm::validate(array($billingInfo, $customerOrder));
            Yii::app()->end();
        }

        if (isset($_POST['CustomerOrderBillingInfo'])) {
            $billingInfo->setAttributes($_POST['CustomerOrderBillingInfo']);
            if ($billingInfo->validate()) {
                $result = Yii::app()->paymentProcessor->processPayment($customerOrder, $billingInfo);
                if ($result['success']) {
                    Yii::app()->user->setFlash('success',
                        "Payment successfully processed. Transaction ID: {$result['transactionId']}"
                    );
                    $billingInfo->save(false);
                    $customerOrder->addCustomerPayment(
                        CustomerPayment::TYPE_ORDER_MAIN,
                        $customerOrder->discounted_total
                    );
                    $customerOrder->sell();
                    //save processor used
                    //bad solution - just to avoid errors in old method
                    $customerOrder->saveAttributes(array(
                        'payment_processor' => Settings::getVar('payment_processor')
                    ));
                    $customerOrder->sendTransactionConfirmation();
                    Activity::model()->addActivity_CompletePayCustomerOrder($customerOrder->id);

                } else {
                    Yii::app()->user->setFlash('error', 'Error with payment processor settings');
                    Yii::log('Error in payment processing: ' . $result['message'], 'error', 'Payment processing');
                }
            }
        }

        $itemsDataProvider = new CActiveDataProvider('CustomerOrderItems', array(
            'criteria' => array(
                'condition' => 'customer_order_id = :customerOrderId',
                'params' => array(':customerOrderId' => $customerOrder->id),
            ),
        ));

        $this->render($view, array(
            'order' => $customerOrder,
            'billingInfo' => $billingInfo,
            'itemsDataProvider' => $itemsDataProvider,
        ));
    }
    protected function loadOrderModel($orderId)
    {
        $customerOrder = CustomerOrder::model()->with('customer')->findByPk($orderId);
        if (empty($customerOrder))
            throw new CHttpException(404,'The requested page does not exist.');
        if ($customerOrder->status != CustomerOrder::$STATUS_PENDING) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $customerOrder;
    }

    protected function checkRetailerOrderAccess($customerOrder) {
        if (Yii::app()->user->isAdmin()) return true;
        $customerId = RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id);
        if ($customerOrder->customer_id != $customerId)
            throw new CHttpException(403, "You're not authorized to access");
    }
    /**
     * @param $order_id
     * @throws CHttpException
     */
    public function actionCheckout_order_by_admin($order_id)
    {
        $customerOrder = $this->loadOrderModel($order_id);
        $this->checkout($customerOrder);
    }


	public function actionCheckout_order_by_admin2($order_id){
		// TODO: checkout methods should not contain so much copy-paste
		$customerOrderModel = CustomerOrder::model()->findByPk($order_id);
		$customerOrderItemsModel = new CustomerOrderItems('search');
        $customerModel = Customer::model()->findByPk($customerOrderModel->customer_id);
        
		$pathinfo = pathinfo(__FILE__);
		$dir_path = $pathinfo['dirname'];
		
		require_once $dir_path.'/../extensions/anet_php_sdk/AuthorizeNet.php';
		
		$transaction_success = true;
		$error_message = "";
		
		if (isset($_POST) && (count($_POST) > 0)){
			//~ $transaction = new AuthorizeNetAIM('5w376qdmRPLD', '46363rSbGmnEw28m');
			$transaction = new AuthorizeNetAIM(Customer::$authorize_api_login_id, Customer::$authorize_transaction_key);
			$transaction->setSandbox(false);
			$transaction->amount = $_POST['amount'];
			$transaction->card_num = $_POST['authorizenet_cc_number'];
			$transaction->exp_date = $_POST['authorizenet_cc_expires_month']."/".$_POST['authorizenet_cc_expires_year'];
			$transaction->setCustomField('Card name :',$_POST['fname'].' '.$_POST['lname']);
			// $transaction->setCustomField('Card name :',$_POST['authorizenet_cc_owner']);
			$transaction->setCustomField('Card type :',$_POST['credit_card_type']);

			// $transaction->description = $_POST['desc'];
			// get plain text description
            $criteriaItems = new CDbCriteria;
            $criteriaItems->compare('customer_order_id',$order_id);
            $items = CustomerOrderItems::model()->findAll($criteriaItems);
            
            $items_text = array();
            foreach ($items as $item){
                $item_info = Item::model()->findByPk($item->item_id);
                $items_text[] = $item_info->item_name.': '.$item->quantity;
            }
            $transaction->description = implode(', ', $items_text);
			$transaction->first_name = $_POST['fname'];
			$transaction->last_name = $_POST['lname'];
			$transaction->address=$_POST['street_address'];
			$transaction->city=$_POST['city'];
			$transaction->state=$_POST['state'];
			$transaction->zip=$_POST['postcode'];
			$transaction->phone=$_POST['telephone'];
			$transaction->email=$_POST['email_address'];
			$shipping = $_POST['busrestype'].', shipping method: '.$customerOrderModel->shipping_method;
            if (isset($_POST['notes']) && trim($_POST['notes'])){
                $shipping .= ' Additional notes: '.$_POST['notes'];
            }
            // die ($shipping);
            $transaction->setCustomField('The Shipment Adress :', $shipping);
            // $transaction->setCustomField('The Shipment Adress :',$_POST['busrestype'].', '.$customerOrderModel->shipping_method);

			if ($_POST['sh_radio']!='0') {
				$transaction->ship_to_first_name = $_POST['ShipFirstName'];
				$transaction->ship_to_last_name = $_POST['ShipLastName'];
				$transaction->ship_to_address=$_POST['ShipAddress'];
				$transaction->ship_to_city=$_POST['ShipCity'];
				$transaction->ship_to_state=$_POST['shipstate'];
				$transaction->ship_to_zip=$_POST['shippostcode'];
				$transaction->setCustomField('Shipment telephone:',$_POST['shiptelephone']);
			} else {
				$transaction->ship_to_first_name = $_POST['fname'];
				$transaction->ship_to_last_name = $_POST['lname'];
				$transaction->ship_to_address=$_POST['street_address'];
				$transaction->ship_to_city=$_POST['city'];
				$transaction->ship_to_state=$_POST['state'];
				$transaction->ship_to_zip=$_POST['postcode'];
				$transaction->setCustomField('Shipment telephone:',$_POST['telephone']);
			}
			
			$response = $transaction->authorizeAndCapture();
						
			//if ($response->approved) {
			if (!$response->error && $response->approved == 1) {
			//if (true) {
				$customerOrderModel->status = CustomerOrder::$STATUS_COMPLETED;
				$customerOrderModel->save();
				$customerOrderModel->importAuthorizeNetTransctionBillingInfo($_POST);
				$customerOrderModel->addCustomerPayment(CustomerPayment::TYPE_ORDER_MAIN, $_POST['amount']);
				
				////////////////////////////////////////////////////////////
				// Added Activity
				Activity::model()->addActivity_CompletePayCustomerOrder($order_id);
                
                $this->sellOrder($order_id);
                
			} else {
				if(isset($response->error_message)){
					$transaction_success = false;
					$error_message = $response->error_message;
				} else {
					if ($response->declined){
						$transaction_success = false;
						$error_message = $response->response_reason_text;
					}
				}
				
				$orderBillingInfoModel = CustomerOrderBillingInfo::getNewModelFromPost();
			}
		}
		
		if (!isset($orderBillingInfoModel)){
			$strQuery = 'SELECT id FROM customer_order WHERE customer_id='.$customerModel->id.' AND status=\''.CustomerOrder::$STATUS_SOLD.'\'';
	        $result = Yii::app()->db->createCommand($strQuery)->queryAll();
	        if (count($result)){
	            $order_ids = array();
	            foreach($result as $row){
	                $order_ids[] = $row['id'];
	            }
	            $criteria = new CDbCriteria();
	            $criteria->addInCondition('customer_order_id', $order_ids);
	            $criteria->order = 'id DESC';
	            $orderBillingInfoModel = CustomerOrderBillingInfo::model()->find($criteria);
	            if (!$orderBillingInfoModel){
	                $orderBillingInfoModel = new CustomerOrderBillingInfo();
	            }
	        } else {
	            $orderBillingInfoModel = new CustomerOrderBillingInfo();
	        }
		}
		
		$this->render('checkout_order_by_admin',array(
			'customerOrderModel'=>$customerOrderModel,
			'customerOrderItemsModel'=>$customerOrderItemsModel,
			'orderBillingInfoModel'=>$orderBillingInfoModel,
			'customerModel'=>$customerModel,
			'transaction_success'=>$transaction_success,
			'error_message'=>$error_message,
			'response'=>$response,
		));
	}


    public function actionCheckout_order_by_retailer($order_id){

        $customerOrder = $this->loadOrderModel($order_id);
        $this->checkRetailerOrderAccess($customerOrder);

        if ($customerOrder->discounted == "Discounted"){
            $amount = $customerOrder->discounted_total;
        } else {
            $amount = $customerOrder->grand_total_price;
        }
        //if the amount of transaction is too large
        if ($amount >= CustomerOrder::CC_MAX_CHECKOUT) {
            //email admin notification
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->IsSendmail();
            $mail->AddAddress(Customer::NOTIFICATION_EMAIL);
            $mail->SetFrom(AutoMail::getAdminEmail(), 'RegencyShop.com');
            $mail->Subject = AutoMailMessage::MS_RETAILER_TRANSACTION_LARGE;
            $mail->Body = "Large transaction from {$customerOrder->first_name} {$customerOrder->last_name}";
            if ($mail->Send()){
                $result['status'] = 'success';
            } else {
                $result['status'] = 'fail';
            }
            $this->render('checkout_order_by_check');
            Yii::app()->end;
        }
        //check that order belong to this retailer
        $this->checkout($customerOrder);
        // Get retailer user customer id

    }

    public function actionComplete_order($order_id){
        $customerOrderModel = CustomerOrder::model()->findByPk($order_id);
		$customerOrderItemsModel = new CustomerOrderItems('search');
        $customerModel = Customer::model()->findByPk($customerOrderModel->customer_id);
        
        if (isset($_POST) && (count($_POST) > 0)){
            // print_r($_POST);
            // die();
            $customerOrderModel->status = CustomerOrder::$STATUS_COMPLETED;
            $customerOrderModel->save();
            $_POST['sh_radio'] = '1';
            $_POST['notes'] .= (($_POST['notes'] ? "\n\n" : '').'(MARK AS SOLD)');
            $customerOrderModel->importAuthorizeNetTransctionBillingInfo($_POST);
            
            ////////////////////////////////////////////////////////////
            // Added Activity
            Activity::model()->addActivity_CompletePayCustomerOrder($order_id);
            
            $this->sellOrder($order_id);
            $customerOrderModel->sendTransactionConfirmation();
        }
        
        $strQuery = 'SELECT id FROM customer_order WHERE customer_id='.$customerModel->id.' AND status=\''.CustomerOrder::$STATUS_SOLD.'\'';
        $result = Yii::app()->db->createCommand($strQuery)->queryAll();
        if (count($result)){
            $order_ids = array();
            foreach($result as $row){
                $order_ids[] = $row['id'];
            }
            $criteria = new CDbCriteria();
            $criteria->addInCondition('customer_order_id', $order_ids);
            $criteria->order = 'id DESC';
            $orderBillingInfoModel = CustomerOrderBillingInfo::model()->find($criteria);
            if (!$orderBillingInfoModel){
                $orderBillingInfoModel = new CustomerOrderBillingInfo();
            }
        } else {
            $orderBillingInfoModel = new CustomerOrderBillingInfo();
        }
        
		$this->render('complete_order',array(
			'customerOrderModel'=>$customerOrderModel,
			'customerOrderItemsModel'=>$customerOrderItemsModel,
			'orderBillingInfoModel'=>$orderBillingInfoModel,
			'customerModel'=>$customerModel,
		));
    }
    
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Customer;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        
		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
            $model->created_on = date("Y-m-d H:i:s", time());
			if($model->save()){
				$pending = $_POST['pending'];
                if ($pending){
                    $pending_customer = PendingCustomer::model()->findByPk($pending);
					$model->addNote('<strong>Enquiry: </strong>'.$pending_customer->enquiry);
					$pending_customer->delete();
                }
                $this->redirect(array('view','id'=>$model->id));
            }
		}
		
        $pending = 0;
        if (isset($_GET['pending'])){
            if ($p_customer = PendingCustomer::model()->findByPk($_GET['pending'])){
                $model->first_name  = $p_customer->name;
                $model->email       = $p_customer->email;
                $model->phone       = $p_customer->phone;
            }
            
            $pending = $_GET['pending'];
        }
        
		$heardThroughList = HeardThrough::model()->getList();

		$this->render('create',array(
			'model'=>$model,
			'pending'=>$pending,
			'heardThroughList'=>$heardThroughList,			
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		
		$heardThroughModel = HeardThrough::model()->findAll();
		$heardThroughList = array();
		foreach ($heardThroughModel as $heardThrough){
			$text = $heardThrough['heard_through'];
			$heardThroughList[$text] = $text;
		}

		$this->render('update',array(
			'model'=>$model,
			'heardThroughList'=>$heardThroughList,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Customer');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$status = ''; //Last Order
		if(isset($_GET['Customer']) && isset($_GET['Customer']["last_order"])){
			$status = $_GET['Customer']["last_order"];
			//unset($_GET['Customer']["last_order"]);
		}


		$model=new Customer('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Customer']))
			$model->attributes=$_GET['Customer'];
        
        $stats = array('all_time'=>array(), 'last_30_days'=>array());
        $stats['all_time']['customers'] = Customer::model()->count();
        $stats['all_time']['sold']      = CustomerOrder::getTotalSoldOrders();
        $stats['all_time']['pending']   = CustomerOrder::getTotalPendingOrders();
        $stats['all_time']['lost']      = CustomerOrder::getTotalLostOrders();
        
        $criteria = new CDbCriteria();
        $criteria->condition = 'created_on > date_sub(NOW(), INTERVAL 30 DAY)';
        $stats['last_30_days']['customers'] = Customer::model()->count($criteria);
        $stats['last_30_days']['sold']      = CustomerOrder::getTotalSoldOrders(30);
        $stats['last_30_days']['pending']   = CustomerOrder::getTotalPendingOrders(30);
        $stats['last_30_days']['lost']      = CustomerOrder::getTotalLostOrders(30);
        
		$this->render('admin',array(
			'model'=>$model,
			'stats'=>$stats,
			'order_status'=>$status
		));
	}
	
	public function actionSell_order_items($order_id){
		$customerOrderModel = CustomerOrder::model()->findByPk($order_id);
		
		//Get the order items info
		$customerOrderItemsModel = new CustomerOrderItems('search');
		
		//Get the customer info		
		$customerModel = Customer::model()->findByPk($customerOrderModel->customer_id);
		$customerName = $customerModel->first_name." ".$customerModel->last_name;
		
		//Get the sales person info
		$salesPersonModel = YumUser::model()->findByPk($customerOrderModel->sales_person_id);
		$salesPersonName = $salesPersonModel->profile->firstname." ".$salesPersonModel->profile->lastname;
		
		$this->render('sell_order_items',array(
			'customerOrderModel'=>$customerOrderModel,
			'customerOrderItemsModel'=>$customerOrderItemsModel,
			'salesPersonName'=>$salesPersonName,
			'customerName'=>$customerName,
		));
	}
	
    public function sellOrder($order_id, $date = false){
        $customerOrderModel = CustomerOrder::model()->findByPk($order_id);
        $customerOrderModel->sell($date);
    }
    
	public function actionSell_order_items_by_ajax($order_id){
		$this->sellOrder($order_id);
	}
	
	public function actionOrder_detail($id)
	{
		$customerOrderModel = CustomerOrder::model()->findByPk($id);
		
		//Get the order items info
		$customerOrderItemsModel = new CustomerOrderItems('search');
		
		//Get the customer info		
		$customerModel = Customer::model()->findByPk($customerOrderModel->customer_id);
		$customerName = $customerModel->first_name." ".$customerModel->last_name;
		
		//Get the sales person info		
		$salesPersonModel = YumUser::model()->findByPk($customerOrderModel->sales_person_id);
		$salesPersonName = $salesPersonModel->profile->firstname." ".$salesPersonModel->profile->lastname;		
		
		$this->render('order_detail',array(
			'customerOrderModel'=>$customerOrderModel,
			'customerOrderItemsModel'=>$customerOrderItemsModel,
			'salesPersonName'=>$salesPersonName,
			'customerName'=>$customerName,
		));
	}
	
	public function actionOrder_details_json_ajax($order_id){
		$details = array();
		$order = CustomerOrder::model()->with('customer', 'billing_info')->findByPk($order_id);
		$details['order_id'] 			= $order_id;
		$details['customer_id'] 		= $order->customer_id;
		$details['customer_name'] 		= $order->customer->first_name.' '.$order->customer->last_name;
		$details['shipping_name']		= $order->billing_info->shipping_name.' '.$order->billing_info->shipping_last_name;
		$details['shipping_address']	= $order->getShippingAddressStr();
		$details['status']				= 'success';
		
		echo json_encode($details);
	}
	
	public function actionImport_osc_orders(){
		$oscOrdersModel = new OscOrders('search');
		$oscOrdersModel->unsetAttributes();  // clear any default values
		
		$this->render('osc_import',array(
			'oscOrdersModel'=>$oscOrdersModel,
		));
	}
	
	public function actionArchived_osc_orders(){
		$oscOrdersModel = new OscOrders('search');
		$oscOrdersModel->unsetAttributes();  // clear any default values
		
		$this->render('archived_osc_orders',array(
			'oscOrdersModel'=>$oscOrdersModel,
		));
	}
	
	public function actionAdd_osc_order($order_id, $site_id){
		$oscOrderModel = OscOrders::model()->findByPk(array(
            'orders_id' => $order_id, 'site_id' => $site_id
        ));
        if (empty($oscOrderModel)) throw new CHttpException(404, 'Object not found');
		$oscOrderItemsModel = new OscOrdersProducts('search');
		$oscOrderItemsModel->unsetAttributes();  // clear any default values
		$oscOrderItemsModel->setAttribute('orders_id', $order_id);
		$oscOrderItemsModel->setAttribute('site_id', $site_id);
		$orderTotal = OscOrders::model()->getOscOrderTotal($order_id, $site_id);
		$orderTotalPrice = OscOrders::model()->getOscOrderTotalPrice($order_id, $site_id);
		
		$newCustomerModel = new Customer;
		$newCustomerOrderModel = new CustomerOrder;
		$customerList = Customer::model()->getCustomerList();
		
		$newItemModel = new Item;		
		$itemCodeList = Item::model()->getAllItemCodeList();
		$itemNameList = Item::model()->getAllItemNameList();
		
		$heardThroughList = HeardThrough::model()->getList();
		
		$this->render('add_osc_order',array(
			'oscOrderModel'=>$oscOrderModel,
			'oscOrderItemsModel'=>$oscOrderItemsModel,
			'newCustomerOrderModel'=>$newCustomerOrderModel,
			'newItemModel'=>$newItemModel,
			'itemCodeList'=>$itemCodeList,
			'itemNameList'=>$itemNameList,
			'customerList'=>$customerList,
			'newCustomerModel'=>$newCustomerModel,
			'heardThroughList'=>$heardThroughList,
			'orderTotal'=>$orderTotal,
			'orderTotalPrice'=>$orderTotalPrice,
		));
	}
	
	public function actionAdd_new_customer_ajax(){
		$newCustomerModel = new Customer;
		
		$result['message'] = "fail";
		if(count($_POST))
		{
			$newCustomerModel->attributes = $_POST['Customer'];
			
			if($newCustomerModel->save()){
				$result['message'] = "success";
				$item_data = array(
					"id"=>$newCustomerModel->id,
					"customer_name"=>$newCustomerModel->first_name." ".$newCustomerModel->last_name ,
				);
				$result['data'] = $item_data;
				
			} else {
				$result['error'] = CHtml::errorSummary($newCustomerModel);
			}
		}

		echo json_encode($result);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Customer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionImport_osc_data_ajax()
    {
        /**@todo This is left only to be sure nothing is broke for multisites
         * this should be certainly reworked
         */
        foreach (Yii::app()->params['oscImport'] as $siteId => $credentials) {
            $siteId++;
            $con = @mysql_connect(
                $credentials['host'], $credentials['user'], $credentials['pass']
            );
            if (!$con) die('Could not connect: ' . mysql_error());
            mysql_select_db($credentials['db'], $con)
                || die('DB select failed: ' . mysql_error());

            OscOrders::model()->importOscData($siteId);
            OscOrdersProducts::model()->importOscData($siteId);
            OscOrdersProductsAttributes::model()->importOscData($siteId);
            OscOrdersTotal::model()->importOscData($siteId);
        }
        /*
         * Commented as old method
		$con = @mysql_connect(OscOrders::$osc_db, OscOrders::$osc_db_user, OscOrders::$osc_db_pass);
		if (!$con) {
			die('Could not connect: ' . mysql_error());
			return;
		}
		
		mysql_select_db(OscOrders::$osc_db_name, $con);
		
		OscOrders::model()->importOscData();
		OscOrdersProducts::model()->importOscData();
		OscOrdersProductsAttributes::model()->importOscData();
		OscOrdersTotal::model()->importOscData();
        */
	}
	
	public function actionCheck_name_duplicated_ajax(){
		$result['message'] = "duplicated";
		$newCustomerModel = new Customer;
		
		$result['message'] = "not duplicated";
		if(count($_POST))
		{
			$newCustomerModel->attributes = $_POST['Customer'];
			if ($newCustomerModel->checkNameDuplicated() == true){
				$result['message'] = "duplicated";
				$result['id'] = $newCustomerModel->getDuplicatedCustomerId();
			}
		}

		echo json_encode($result);
	}
	
	public function actionGet_today_followup_list_ajax(){
		$customerFollowup = new CustomerFollowup('search');
		$user_id = Yii::app()->user->id;
		$todayFollowupList = $customerFollowup->todayFollowupList($user_id);
		
		echo json_encode($todayFollowupList);
	}
	
	public function actionDismiss_cusomter_followup($followup_id){
		$customrFollowupModel = CustomerFollowup::model()->findByPk($followup_id);
		$customrFollowupModel->status = "Dismissed";
		$customrFollowupModel->save();
	}
	
	public function actionAlert_cusomter_followup($followup_id){
		$customrFollowupModel = CustomerFollowup::model()->findByPk($followup_id);
		$customrFollowupModel->status = "Alerted";
		$customrFollowupModel->save();
	}
    
    public function actionDelete_order_items_ajax($items_id){
        // we need to return new order totals on success
        $orderItems = CustomerOrderItems::model()->findByPk($items_id);
        $order = CustomerOrder::model()->findByPk($orderItems->customer_order_id);
        if (YumUser::isRetailer() && !Yii::app()->user->isAdmin()) {
            //check the order belongs to this retailer
            $customerId = RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id);
            if ($order->customer_id != $customerId)
                throw new CHttpException('403', "You're not authorized to edit this order");
        }
        $orderItems->delete();
        $result['status'] = 'success';
        $result['order_id'] = $order->id;
		$result['profit'] = CustomerOrder::getOrderAntProfit($order->id);
        $grand_total = CustomerOrder::getCustomerOrderTotalPrice($order->id);
        $order->grand_total_price = $grand_total;
        $order->setCorrectOrderDiscountedTotal();

        $result['grand_total'] = number_format($grand_total, 2);
        $result['discounted_total'] = number_format($order->discounted_total, 2);
        if (!$order->save()){
            var_dump(CHtml::errorSummary($order));
            // var_dump($result);
            exit;
        }
        
        echo json_encode($result);
    }
    
    public function actionUpdate_order_items_ajax($items_id, $old_value, $new_value){
        $orderItems = CustomerOrderItems::model()->findByPk($items_id);
        $orderItems->quantity = $new_value;

        $order = CustomerOrder::model()->findByPk($orderItems->customer_order_id);

        if (YumUser::isRetailer()) {
            $this->checkRetailerOrderAccess($order);
            /*
            //check the order belongs to this retailer
            if (!RetailerUserCustomer::model()->exists(
                'user_id = :user_id and customer_id = :customer_id',
                array(
                    ':user_id' => Yii::app()->user->id,
                    ':customer_id' => $order->customer_id,
                )
            )) throw new CHttpException('403', "You're not authorized to edit this order");
            */
        }
        $orderItems->save();

        $grand_total = CustomerOrder::getCustomerOrderTotalPrice($order->id);
        $order->grand_total_price = $grand_total;
        $order->setCorrectOrderDiscountedTotal();
        
        $result['status'] = 'success';
        $result['order_id'] = $order->id;
        $result['grand_total'] = number_format($grand_total, 2);
        // $result['discounted_total'] = CustomerOrder::countCustomerOrderDiscountedTotal($order->id);
        $result['discounted_total'] = number_format($order->discounted_total, 2);
        $result['eta'] = Item::model()->getStatusString($orderItems->item_id, $new_value);
        $result['profit'] = CustomerOrder::getOrderAntProfit($order->id);
        // if ($order->shipping_method == "Shipping") {
            // $shipping_price = $orderItems->items->shipping_price;
        // } else if ($customerOrder->shipping_method == "Local Pickup") {
            // $shipping_price = $orderItems->items->local_pickup;
        // } else if ($customerOrder->shipping_method == "Shipped from Los Angeles"){
            // $shipping_price = $orderItems->items->la_oc_shipping;
        // } else if ($customerOrder->shipping_method == "Canada Shipping"){
            // $shipping_price = $orderItems->items->canada_shipping;
        // }
        
        // $item_total_price = ($orderItems->sale_price + $shipping_price + $orderItems->special_shipping_price) * $orderItems->quantity;
        $item_total_price = $orderItems->getTotalPrice($order);
        $result['item_total'] = $item_total_price;

        if (!$order->save()){
            // var_dump(CHtml::errorSummary($order));
            var_dump($result);
            exit;
        }
        
        echo json_encode($result);
    }
    
    public function actionAdd_order_items_ajax($customer_order_id, $item_id, $quantity, $special_shipping_price, $custom_color){
        $order = CustomerOrder::model()->findByPk($customer_order_id);
        $item = Item::model()->findByPk($item_id);

        if (YumUser::isRetailer() && !Yii::app()->user->isAdmin()) {
            //check the order belongs to this retailer
            $customerId = RetailerUserCustomer::getRetailerCustomerId(Yii::app()->user->id);
            if ($order->customer_id != $customerId)
                throw new CHttpException('403', "You're not authorized to edit this order");
        }

        $order_items = new CustomerOrderItems();
        $order_items->customer_order_id         = $customer_order_id;
        $order_items->item_id                   = $item_id;
        $order_items->quantity                  = $quantity;
        $order_items->special_shipping_price    = $special_shipping_price;
        $order_items->custom_color              = $custom_color;
        $order_items->exw_cost_price            = $item->exw_cost_price;
        $order_items->fob_cost_price            = $item->fob_cost_price;
        $order_items->sale_price                = $item->sale_price;
        
        if ($order_items->insert()){
            $order->grand_total_price = CustomerOrder::getCustomerOrderTotalPrice($order->id);
            $order->setCorrectOrderDiscountedTotal();
            $order->save();
            $result['status'] = 'success';
            $result['order_total']      = number_format($order->grand_total_price, 2);
            $result['discounted_total'] = number_format($order->discounted_total, 2);
            $result['items_id']         = $order_items->id;
			$result['order_id']         = $order->id;
			$result['profit'] = CustomerOrder::getOrderAntProfit($order->id);
        } else {
            $result['status'] = 'fail';
        }
        
		
        echo json_encode($result);
    }
    
    public function actionSet_order_add_tax_ajax($order_id, $state){
        $order = CustomerOrder::model()->findByPk($order_id);

        if (YumUser::isRetailer()) $this->checkRetailerOrderAccess($order);

        $order->add_sales_tax = $state;
        
		if ($order->updateTotals()){
			$result['status'] = 'success';
			$result['new_total'] = number_format($order->grand_total_price, 2);
			$result['new_discounted'] = number_format($order->discounted_total, 2);
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving order info';
		}
        echo json_encode($result);
    }
    
    public function actionSet_order_shipping_method_ajax($order_id, $method){
        $order = CustomerOrder::model()->findByPk($order_id);

        if (YumUser::isRetailer()) $this->checkRetailerOrderAccess($order);

        $order->shipping_method = $method;
        
		// need to save order first to get correct total as customerOrderItems object is used for calculation
		if ($order->save() && $order->updateTotals()){
			$result['status'] = 'success';
			$result['new_total'] = number_format($order->grand_total_price, 2);
			$result['new_discounted'] = number_format($order->discounted_total, 2);
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving order info';
		}
        
        echo json_encode($result);
    }
    
	
	// TODO: use $order->updateTotals() everywhere here
    public function actionApply_order_standard_discount_ajax($order_id, $discounted){
        $order = CustomerOrder::model()->findByPk($order_id);
        $order->discounted = ($discounted ? 'Discounted' : 'Not Discounted');
        if ($order->discounted == 'Discounted'){
            $order->discounted_total = CustomerOrder::getCustomerOrderDiscountedTotal($order_id, $order->grand_total_price);
        } else {
            $order->discounted_total = $order->grand_total_price;
        }
        
        $order->raw_discounted = 0;
        $order->custom_discount = 0;
        
        if ($order->save()){
            $result['status'] = 'success';
            // $result['discounted_total'] = $order->discounted_total;
            // $result['discounted'] = $order->discounted;
        } else {
            $result['status'] = 'fail';
        }
        
        echo json_encode($result);
    }
    
    public function actionApply_order_custom_discount_ajax($order_id, $discounted_total){
        $order = CustomerOrder::model()->findByPk($order_id);

        $order->custom_discount = 1;
        $order->discounted = 'Discounted';
        $order->discounted_total = $discounted_total;
        $order->raw_discounted = $discounted_total;
        
        if ($order->save()){
            $result['status'] = 'success';
            // $result['discounted_total'] = $order->discounted_total;
            // $result['discounted'] = $order->discounted;
        } else {
            $result['status'] = 'fail';
        }
        
        echo json_encode($result);
    }
    
    public function actionSet_order_completed_ajax($order_id){
        $order = CustomerOrder::model()->findByPk($order_id);
        $order->status = CustomerOrder::$STATUS_COMPLETED;
        if ($order->save()){
            $this->sellOrder($order_id);
            $order->sendTransactionConfirmation();
            // Activity::model()->addActivity_CompletePayCustomerOrder($order_id);
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving order info';
        }
        
        echo json_encode($result);
    }
    
    public function actionSet_order_lost_sale_ajax($order_id){
        $order = CustomerOrder::model()->findByPk($order_id);
        $order->status = CustomerOrder::$STATUS_LOST;
        $order->lost_sale_date = date("Y-m-d H:i:s", time());
        if ($order->save()){
            // Activity::model()->addActivity_CompletePayCustomerOrder($order_id);
            $criteria = new CDbCriteria();
            $criteria->compare('customer_order_id', $order_id);
            CustomerOrderItems::model()->updateAll(array('lost_sale'=>1, 'lost_sale_date'=>$order->lost_sale_date), $criteria);
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving order info';
        }
        
        echo json_encode($result);
    }
    
    public function actionSet_order_dead_ajax($order_id){
        $order = CustomerOrder::model()->findByPk($order_id);
        $order->status = CustomerOrder::$STATUS_DEAD;
        // $order->lost_sale_date = date("Y-m-d H:i:s", time());
        if ($order->save()){
            // Activity::model()->addActivity_CompletePayCustomerOrder($order_id);
            // $criteria = new CDbCriteria();
            // $criteria->compare('customer_order_id', $order_id);
            // $items = CustomerOrderItems::model()->updateAll(array('lost_sale'=>1), $criteria);
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving order info';
        }
        
        echo json_encode($result);
    }
    
    public function actionContact(){
        $model = new PendingCustomer('externalContact');

        if (isset($_POST['PendingCustomer'])) {
            $model->attributes = $_POST['PendingCustomer'];

            if (isset($_POST['ajax']) && $_POST['ajax'] == 'contact-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
            if ($model->save()) {
                //@todo it's AWFUL
                $email = Yii::app()->email;
                $email->to = 'support@regencyshop.com';
                $email->from = $model->email;
                $email->subject = 'Contact Form Message';
                $content = 'New message from contact form.<br />
                        <strong>Name: </strong>' . $model->name . '<br />
                        <strong>Email: </strong>' . $model->email . '<br />
                        <strong>Phone: <Attributes/strong>' . $model->phone . '<br />
                        <strong>Enquiry: </strong>'.$model->enquiry.'<br />';
                $email->message = $content;
                $email->send();
                $this->renderPartial('_contactSuccess');
                Yii::app()->end();
                //$this->redirect('https://www.regencyshop.com/contact_us_success.php');
            }
        }
        if (isset($_GET['remote'])) {
            $this->renderPartial('contact', array('model' => $model), false, true);
        } else
            $this->render('contact', array('model' => $model));
    }
    
    public function actionPending(){
        $model=new PendingCustomer();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PendingCustomer']))
			$model->attributes=$_GET['PendingCustomer'];

		$this->render('pending',array(
			'model'=>$model,
		));
    }
    
    public function actionDelete_pending_customer_ajax($customer_id){
        if (PendingCustomer::model()->deleteByPk($customer_id)){
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error deleting record';
        }
        
        echo json_encode($result);
    }
    
 	public function actionSellallcompletedorders(){
		return;
		// remove all item-order connections
		$criteria = new CDbCriteria();
		$criteria->addCondition('`status` != "'.Individualitem::$STATUS_SOLD.'" AND `sold_date`>"0000-00-00 00:00:00"');
		Individualitem::model()->updateAll(array('customer_order_items_id'=>0, 'sold_date'=>'0000-00-00 00:00:00'), $criteria);
		
		// get all sold orders ordered by sold_date
		$criteria = new CDbCriteria();
        $criteria->compare('status', CustomerOrder::$STATUS_SOLD);
		$criteria->addCondition('sold_date > \'0000-00-00 00:00:00\'');
		$criteria->order = 'sold_date ASC';
        $orders = CustomerOrder::model()->findAll($criteria);

		foreach ($orders as $order){
            // $items_criteria = new CDbCriteria();
            // $ind_items_criteria = new CDbCriteria();
            // $items_criteria->compare('customer_order_id', $order->id);
            // $items = CustomerOrderItems::model()->find($items_criteria);
            // $ind_items_criteria->compare('customer_order_items_id', $items->id);
            // $ind_item = Individualitem::model()->find($ind_items_criteria);
            // $order->sold_date = $ind_item->sold_date;
            // $order->save();
			$order->_sellOrderItems_2();
        }
	}
    
    public function actionSold_orders(){
        $this->render('sold_orders', array(
            'customerOrderModel'=>new CustomerOrder(),
        ));
    }
    
	public function actionFix_osc_orders(){

        $info = CustomerOrderBillingInfo::model()->findAll();
        foreach($info as $row){
            $row->billing_email_address = 'casualuser+mike+bi'.$row->customer_order_id.'@gmail.com';
            $row->save();
        }

        return;
        $items = Item::model()->findAll();
        foreach ($items as $item){
            if (file_exists($p = Yii::getPathOfAlias('webroot').'/images/item_images/'.$item->id.'.jpg')){
                $path = $p;
            } elseif (file_exists($p = Yii::getPathOfAlias('webroot').'/images/item_images/'.$item->id.'.gif')){
                $path = $p;
            } elseif (file_exists($p = Yii::getPathOfAlias('webroot').'/images/item_images/'.$item->id.'.png')){
                $path = $p;
            } else {
                echo 'Fail: '.$item->id.'<br />';
                continue;
            }
            try{
                $img = new Imagick($path);
                $img->thumbnailimage(150, 150, true);
                $img->writeimage($path);
            } catch(Exception $e){
                echo 'Problem: '.$path.'<br />';
            }
            echo $path.'<br />';
        }

	}
	
	public function actionEdit_order_notes_ajax($order_id){
		$text = $_POST['text'];
		$order = CustomerOrder::model()->findByPk($order_id);
		if ($order->billing_info){
			$billing_info = $order->billing_info;
		} else {
			$billing_info = new CustomerOrderBillingInfo();
			$billing_info->customer_order_id = $order->id;
		}
		$billing_info->order_notes = $text;
		if ($billing_info->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving data';
		}
		
		echo json_encode($result);
	}
	
		
	public function actionPending_approval_orders(){
        $this->render('pending_approval_orders', array(
            'customerOrderModel'=>new CustomerOrder(),
        ));
	}
	
	public function actionApprove_order_ajax($order_id){
		$order = CustomerOrder::model()->findByPk($order_id);
		if ($order->approve()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving order data';
		}
		
		echo json_encode($result);
	}
	
	public function actionDeny_order_ajax($order_id){
		$order = CustomerOrder::model()->with('customer', 'billing_info')->findByPk($order_id);
		if ($order->deny()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving order data';
		}
		
		echo json_encode($result);
	}
	
	public function actionPickup_date($id, $code){
		$criteria = new CDbCriteria();
		$criteria->compare('id', $id);
		$criteria->compare('pickup_code', $code);
		$shipment = CustomerOrderShipment::model()->find($criteria);
		$this->layout = 'regencyshop';
		$this->pageTitle = 'Pickup Date';
		if (!$shipment){
			throw new CHttpException(404, 'Shipment not found');
		}
		if (isset($_POST['CustomerOrderShipment']['pickup_date'])){
			$time = strtotime($_POST['CustomerOrderShipment']['pickup_date']);
			$valid = true;
			if ($time < time())
			{
				$valid = false;
			}
			$date = getdate($time);
			if ($date['wday'] < 1 || $date['wday'] > 5){
				// Mon-Fri
				$valid = false;
			}
			
			if ($date['hours'] > 16 || $date['hours'] < 9
					&& !($date['hours'] == 16 && $date['mins'] == 0)){
				// 9am - 4pm
				$valid = false;
			}
			
			if (!$valid){
				Yii::app()->user->setFlash('notice', "Please set correct date");
				$this->redirect('');
			} else {
				$shipment->pickup_date = date('Y-m-d H:i:s', $time);
				$shipment->save();
				$shipment->sendPickupEmail();
			}
		}
		$this->render('pickup_date', array(
			'shipment'=>$shipment,
		));
	}
	
	public function actionAdditional_payment($id, $code){
		$this->layout = "regencyshop";
		
		// "se" parameter indicates whether it is additional payment for shipment (beyond ppoint) or for the order updated shipping
		$se = isset($_GET['se']) && $_GET['se'] == 1;
		if ($se){
			$criteria = new CDbCriteria();
			$criteria->compare('id', $id);
			$criteria->compare('shipping_edited_payment_code', $code);
			$order = CustomerOrder::model()->find($criteria);
			$billing_info = $order->billing_info;
			$amount = $order->shipping_edited_payment_amount;
		} else {	 
			$criteria = new CDbCriteria();
			$criteria->compare('id', $id);
			$criteria->compare('additional_payment_code', $code);
			$shipment = CustomerOrderShipment::model()->find($criteria);
			$billing_info = $shipment->customer_order->billing_info;
			$amount = $shipment->additional_payment_amount;
		}
		
		$pathinfo = pathinfo(__FILE__);
		$dir_path = $pathinfo['dirname'];
		require_once $dir_path.'/../extensions/anet_php_sdk/AuthorizeNet.php';
		
		if (isset($_POST) && (count($_POST) > 0)){
			$transaction = new AuthorizeNetAIM(Customer::$authorize_api_login_id, Customer::$authorize_transaction_key);
			$transaction->setSandbox(false);
			$transaction->amount = $amount;
			$transaction->card_num = $_POST['authorizenet_cc_number'];
			$transaction->exp_date = $_POST['authorizenet_cc_expires_month']."/".$_POST['authorizenet_cc_expires_year'];
			$transaction->setCustomField('Card type :',$_POST['credit_card_type']);

            $transaction->description = 'Additional shipping payment, shipment #'.$id;
			
			if (!$billing_info->billing_name){
				$transaction->setCustomField('Card name :',$_POST['fname'].' '.$_POST['lname']);
				$transaction->first_name = $_POST['fname'];
				$transaction->last_name = $_POST['lname'];
				$transaction->address=$_POST['street_address'];
				$transaction->city=$_POST['city'];
				$transaction->state=$_POST['state'];
				$transaction->zip=$_POST['postcode'];
				$transaction->phone=$_POST['telephone'];
				$transaction->email=$_POST['email_address'];
			} else {
				$transaction->setCustomField('Card name :',$billing_info->billing_name.' '.$billing_info->billing_last_name);
				$transaction->first_name = $billing_info->billing_name;
				$transaction->last_name = $billing_info->billing_last_name;
				$transaction->address=$billing_info->billing_street_address;
				$transaction->city=$billing_info->billing_city;
				$transaction->state=$billing_info->billing_state;
				$transaction->zip=$billing_info->billing_zip_code;
				$transaction->phone=$billing_info->billing_phone_number;
				$transaction->email=$billing_info->billing_email_address;
			}
			//~ $transaction->setCustomField('The Shipment Adress :',$_POST['busrestype'].', '.$customerOrderModel->shipping_method);
			
			$response = $transaction->authorizeAndCapture();
			if (!$response->error && $response->approved == 1) {
				// TODO: activity and email notifications
				if ($se){
					$order->shipping_edited_payment_done = 1;
					$order->shipping_edited_payment_date = date('Y-m-d H:i:s');
					$order->save();
					$order->addCustomerPayment(CustomerPayment::TYPE_ORDER_SHIPPING_EDITED, $amount);
					$order->customer->addNote('Shipping edited payment done: $'.number_format($amount, 2));
				} else {
					$shipment->additional_payment_done = 1;
					$shipment->additional_payment_date = date('Y-m-d H:i:s');
					$shipment->save();
					$shipment->addCustomerPayment(CustomerPayment::TYPE_SHIPMENT_ADDITIONAL, $amount);
					$shipment->customer_order->customer->addNote('Shipping beyond point payment done: $'.number_format($amount, 2));
				}
				////////////////////////////////////////////////////////////
				// Added Activity
				//~ Activity::model()->addActivity_CompletePayCustomerOrder($order_id);

			} else {
				if(isset($response->error_message)){
					$transaction_success = false;
					$error_message = $response->error_message;
					if (strpos($error_message, 'Response Reason Text:') !== false){
						$error_message = explode('Response Reason Text:', $error_message);
						$error_message = $error_message[1];
					}
				} else {
					if ($response->declined){
						$transaction_success = false;
						$error_message = $response->response_reason_text;
					}
				}
			}
		}
		
		$this->render('additional_payment',array(
			'shipment'=>$shipment,
			'se'=>$se,
			'order'=>$order,
			'billing_info'=>$billing_info,
			'transaction_success'=>$transaction_success,
			'error_message'=>$error_message,
			'amount'=>$amount
		));
	}
	
	public function actionReview($id, $code){
		$criteria = new CDbCriteria();
		$criteria->compare('id', $id);
		$criteria->compare('review_code', $code);
		$order = CustomerOrder::model()->find($criteria);
		$this->layout = 'regencyshop';
		$this->pageTitle = 'Order Review';
		if (!$order){
			throw new CHttpException(404, 'Order not found');
		}
		
		$model = new CustomerOrderReview();
		
		// TODO: set correct rules for review model

		if (isset($_POST['CustomerOrderReview']) && !$order->review_placed){
			$model->attributes = $_POST['CustomerOrderReview'];
			$model->customer_order_id = $id;
			$model->date = date('Y-m-d H:i:s');
			if ($model->save()){

				$post_data = '';
				if ($model->overall > 3){
                    $model->publish();
                    $post_data = $model->getPostData();
//					$username = trim($order->customer->first_name.$order->customer->last_name);
//					$password = substr(md5(uniqid()), 0, 8);
//                    if (!$model->publish()) {
//                        $post_data = $model->getPostData();
//
//                    }
//					$model->rr_username = $username;
//					$model->rr_password = $password;
//					$model->posted_to_rr = 1;
//					$model->save();
				}
				$order->review_placed = 1;
				$order->save();

				// send admin notification
				$email = Yii::app()->email;

				$email_to = 'support@regencyshop.com';
				$admin_email = AutoMail::getAdminEmail();
				$email->to = $email_to;
				$email->from = $admin_email;
				$email->subject = AutoMailMessage::MS_ADMIN_NEW_CUSTOMER_REVIEW;
				$find = array(
					'[customer_name]',
					'[order_id]',
					'[overall]',
					'[cost]',
					'[future_shopping]',
					'[shipping]',
					'[customer_service]',
					'[return]',
					'[text]',
				);

				$replace = array(
					$model->customer_order->customer->first_name.' '.$model->customer_order->customer->last_name,
					$id,
					$model->overall.'/5 '.CustomerOrderReview::$overall_list[$model->overall],
					$model->cost.'/5 '.CustomerOrderReview::$cost_list[$model->cost],
					$model->future_shopping.'/5 '.CustomerOrderReview::$future_shopping_list[$model->future_shopping],
					$model->shipping.'/5 '.CustomerOrderReview::$shipping_list[$model->shipping],
					$model->customer_service.'/5 '.CustomerOrderReview::$customer_service_list[$model->customer_service],
					$model->return.'/5 '.CustomerOrderReview::$return_list[$model->return],
					$model->review_text
				);
				$body = str_replace($find, $replace, AutoMailMessage::MC_ADMIN_NEW_CUSTOMER_REVIEW);
				$email->message = $body;
				$email->send();
			}
		} else {
			$model->overall = 5;
		}
		
		$this->render('review', array(
			'order'=>$order,
			'model'=>$model,
			'post_data'=>$post_data,
			'order_items'=>new CustomerOrderItems()
		));
	}
	
	public function actionOrder_shipping_info_verification_request_ajax($order_id){
		$order = CustomerOrder::model()->with('customer', 'order_items', 'order_items.items')->findByPk($order_id);
		$name = $order->customer->first_name.' '.$order->customer->last_name;
		
		$items_arr = array();
		foreach ($order->order_items as $items){
			$items_arr[] = $items->items->item_name;
		}
		
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->AddAddress($order->customer->email);
		$mail->SetFrom(Customer::NOTIFICATION_EMAIL, 'RegencyShop.com');
		$mail->Subject = AutoMailMessage::MS_CUSTOMER_ORDER_VERIFICATION;
		$body = str_replace('[name]', $name, AutoMailMessage::MC_CUSTOMER_ORDER_VERIFICATION);
		$body = str_replace('[items]', implode(',', $items_arr), $body);
		$mail->Body = $body;
		if ($mail->Send()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
		}
		
		echo json_encode($result);
	}
	
	public function actionVerify_order_shipping_info_ajax($order_id){
		$order = CustomerOrder::model()->findByPk($order_id);
		$order->shipping_info_verified = true;
		if ($order->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error saving order';
            //var_dump($order->errors);
		}
		
		echo json_encode($result);
	}
	
	public function actionGet_customers_names_ajax($term=''){
		$names = array();
		$all_names = array();
		if(!empty($term)){
			$criteria = new CDbCriteria;
			$criteria->params = array(':query'=>$term.'%');
			
			$criteria->condition = 'first_name LIKE :query OR last_name LIKE :query';
			$customers = Customer::model()->findAll($criteria);
			// TODO: total trim! make it more readable
			foreach ($customers as $customer){
				$all_names[trim(trim(strtolower($customer->first_name)).' '.trim(strtolower($customer->last_name)))] = 
							trim(trim($customer->first_name).' '.trim($customer->last_name));
			}
			
			$criteria->condition = 'billing_name LIKE :query OR billing_last_name LIKE :query';
			$customer_bilings = CustomerOrderBillingInfo::model()->findAll($criteria);
			foreach ($customer_bilings as $biling){
				$all_names[trim(trim(strtolower($biling->billing_name)).' '.trim(strtolower($biling->billing_last_name)))] = 
							trim(trim($biling->billing_name).' '.trim($biling->billing_last_name));
			}
			
			$criteria->condition = 'shipping_name LIKE :query OR shipping_last_name LIKE :query';
			$customer_shippings = CustomerOrderBillingInfo::model()->findAll($criteria);
			foreach ($customer_shippings as $shipping){
				$all_names[trim(trim(strtolower($shipping->shipping_name)).' '.trim(strtolower($shipping->shipping_last_name)))] = 
							trim(trim($shipping->shipping_name).' '.trim($shipping->shipping_last_name));
			}
			
			$criteria->condition = 'email LIKE :query';
			$customers_email = Customer::model()->findAll($criteria);
			foreach ($customers_email as $customer){
				$all_names[strtolower($customer->email)] = $customer->email;
			}
			foreach ($all_names as $name){
				$names[]=$name;
			}
		
		}
		
		echo json_encode($names);
	}
	
	public function actionFind_customers_results($search_str=''){
		
		$search_str = trim($search_str);
		if(strlen($search_str)<3) throw new CHttpException(404,'Page not found.');
		$search_items = explode(" ", $search_str);
		
		$name = '';
		$first_name = '';
		$last_name = '';
		$email = '';
		
		$criteria = new CDbCriteria;
		$criteria_biling = new CDbCriteria;
		$criteria_shiping = new CDbCriteria;
		if(count($search_items)==2){
			$first_name = $search_items[0].'%';
			$last_name = $search_items[1].'%';
			$name = $search_str.'%';
			$criteria->condition = ' (first_name LIKE :first_name AND last_name LIKE :last_name) OR
									 first_name LIKE :name OR last_name LIKE :name';
			$criteria_biling->condition = ' (billing_name LIKE :first_name AND billing_last_name LIKE :last_name) OR
											billing_name LIKE :name OR billing_last_name LIKE :name';
			$criteria_shiping->condition =' (shipping_name LIKE :first_name AND shipping_last_name LIKE :last_name) OR
											 shipping_name LIKE :name OR shipping_last_name LIKE :name';
			$criteria_params = array(
					':first_name'=>$first_name, 
					':last_name'=>$last_name, 
					':name'=>$name
			);
			$criteria->params = $criteria_params;
			$criteria_biling->params = $criteria_params;
			$criteria_shiping->params = $criteria_params;
			
		} else {
			$emailValidator = new CEmailValidator();
			if($emailValidator->validateValue($search_str)){
				$email = $search_str.'%';
				$criteria->condition = 'email LIKE :email';
				$criteria->params = array(':email'=>$email);
			} else {
				$name = $search_str.'%';
				$criteria->condition = 'first_name LIKE :name OR last_name LIKE :name';
				$criteria->params = array(':name'=>$name);
				$criteria_biling->condition = 'billing_name LIKE :name OR billing_last_name LIKE :name';
				$criteria_shiping->condition = 'shipping_name LIKE :name OR shipping_last_name LIKE :name';
				$criteria_biling->params = array(':name'=>$name);
				$criteria_shiping->params = array(':name'=>$name);
			}	
		}
		
			$find_customers = array();
			$criteria_order = new CDbCriteria;
			$customers = Customer::model()->findAll($criteria);
			foreach ($customers as $customer){
				$find_customers[$customer->id] = array(
						'id'=>$customer->id,
						'customer_name'=>trim($customer->first_name).' '.trim($customer->last_name),
						'biling_name'=>array(),
						'shipping_name'=>array(),
						'email'=>$customer->email,
						'phone'=>$customer->phone,
				);
				$criteria_order->condition = 'customer_id = :customer_id';
				$criteria_order->params = array(':customer_id'=>$customer->id);
				$orders = CustomerOrder::model()->findAll($criteria_order);
				foreach ($orders as $order){
					if(!empty($order->billing_info->billing_name) || 
					   !empty($order->billing_info->billing_last_name)){
						$find_customers[$customer->id]['biling_name'][strtolower($order->billing_info->billing_name).'_'.strtolower($order->billing_info->billing_last_name)]=
								trim($order->billing_info->billing_name).' '.trim($order->billing_info->billing_last_name);
					}
					if(!empty($order->billing_info->shipping_name) || 
					   !empty($order->billing_info->shipping_last_name)){
						$find_customers[$customer->id]['shipping_name'][strtolower($order->billing_info->shipping_name).'_'.strtolower($order->billing_info->shipping_last_name)]=
								trim($order->billing_info->shipping_name).' '.trim($order->billing_info->shipping_last_name);
					}
 				}
			}
			if(empty($email)){
				
			
			$bilings = CustomerOrderBillingInfo::model()->findAll($criteria_biling);
			foreach ($bilings as $biling){
				$biling_order = CustomerOrder::model()->findByPk($biling->customer_order_id);
				if(isset($find_customers[$biling_order->customer->id])) continue;
				$find_customers[$biling_order->customer->id] = array(
						'id'=>$biling_order->customer->id,
						'customer_name'=>trim($biling_order->customer->first_name).' '.trim($biling_order->customer->last_name),
						'biling_name'=>array(),
						'shipping_name'=>array(),
						'email'=>$biling_order->customer->email,
						'phone'=>$biling_order->customer->phone,
				);
				$criteria_order->condition = 'customer_id = :customer_id';
				$criteria_order->params = array(':customer_id'=>$biling_order->customer->id);
				$orders = CustomerOrder::model()->findAll($criteria_order);
				foreach ($orders as $order){
					if(!empty($order->billing_info->billing_name) || 
					   !empty($order->billing_info->billing_last_name)){
							$find_customers[$biling_order->customer->id]['biling_name'][strtolower($order->billing_info->billing_name).'_'.strtolower($order->billing_info->billing_last_name)] = 
								trim($order->billing_info->billing_name).' '.trim($order->billing_info->billing_last_name);
					}
					
					if(!empty($order->billing_info->shipping_name) || 
					   !empty($order->billing_info->shipping_last_name)){
							$find_customers[$biling_order->customer->id]['shipping_name'][strtolower($order->billing_info->shipping_name).'_'.strtolower($order->billing_info->shipping_last_name)] = 
								trim($order->billing_info->shipping_name).' '.trim($order->billing_info->shipping_last_name);
					}
 				}
			}

			$bilings = CustomerOrderBillingInfo::model()->findAll($criteria_shiping);
			foreach ($bilings as $biling){
				$biling_order = CustomerOrder::model()->findByPk($biling->customer_order_id);
				if(isset($find_customers[$biling_order->customer->id])) continue;
				$find_customers[$biling_order->customer->id] = array(
						'id'=>$biling_order->customer->id,
						'customer_name'=>trim($biling_order->customer->first_name).' '.trim($biling_order->customer->last_name),
						'biling_name'=>array(),
						'shipping_name'=>array(),
						'email'=>$biling_order->customer->email,
						'phone'=>$biling_order->customer->phone,
				);
				$criteria_order->condition = 'customer_id = :customer_id';
				$criteria_order->params = array(':customer_id'=>$biling_order->customer->id);
				$orders = CustomerOrder::model()->findAll($criteria_order);
				foreach ($orders as $order){
					if(!empty($order->billing_info->billing_name) || 
					   !empty($order->billing_info->billing_last_name)){
							$find_customers[$biling_order->customer->id]['biling_name'][strtolower($order->billing_info->billing_name).'_'.strtolower($order->billing_info->billing_last_name)] = 
								trim($order->billing_info->billing_name).' '.trim($order->billing_info->billing_last_name);
					}
					
					if(!empty($order->billing_info->shipping_name) || 
					   !empty($order->billing_info->shipping_last_name)){
							$find_customers[$biling_order->customer->id]['shipping_name'][strtolower($order->billing_info->shipping_name).'_'.strtolower($order->billing_info->shipping_last_name)] = 
								trim($order->billing_info->shipping_name).' '.trim($order->billing_info->shipping_last_name);
					}
 				}
			}
			}
			$this->render('find_customers_results', array(
				'find_customers'=>$find_customers,
				'search_str'=>$search_str
			));
		
	}
	
	public function actionDelete_osc_order_ajax($order_id, $site_id){
        $model = OscOrders::model()->findByPk(array(
            'orders_id' => $order_id,
            'site_id' => $site_id,
        ));
        if (!isset($model))
            throw new CHttpException(404, 'Not found');
        $model->saveAttributes(array(
            'status' => OscOrders::STATUS_DELETED
        ));
        //this block is here, so it's possible to get back to 'super' algo
        /*
		$criteria = new CDbCriteria();
		$criteria->compare('orders_id', $order_id);
		$criteria->compare('site_id', $site_id);
		OscOrders::model()->deleteAll($criteria);
		OscOrdersProducts::model()->deleteAll($criteria);
		OscOrdersProductsAttributes::model()->deleteAll($criteria);
		OscOrdersTotal::model()->deleteAll($criteria);
        */
	}
	
	public function actionUnsubscribe($code){
		$this->layout = "regencyshop";
		$code = explode('|', $code);
		$email = base64_decode($code[0]);
		$code = $code[1];
		if ($code != Customer::getNewsletterUnsubscribeCode($email)){
			echo '<strong>Invalid request</strong>';
			return false;
		}
		
		$criteria = new CDbCriteria();
		$criteria->compare('email', $email);
		$customer = Customer::model()->find($criteria);
		if (!$customer){
			$newsletter_email = NewsletterEmail::model()->find($criteria);
		}
		if (!$customer && !$newsletter_email){
			echo '<strong>Invalid request</strong>';
			return false;
		}
		if ($customer){
            $customer->saveAttributes(array(
                'newsletter_unsubscribed' => 1
            ));
            Yii::app()->db->createCommand()->insert('customer_unsubscribe', array(
                'customer_id' => $customer->id,
            ));
		}
		
		if ($newsletter_email){
			$newsletter_email->unsubscribed = 1;
			$newsletter_email->save();
		}
		
		// it's possible that next email for this user is already in the queue so we should remove it
		$criteria->compare('sent',0);
		CustomerNewsletterLetter::model()->deleteAllByAttributes(array(
            'sent' => 0,
            'customer_id' => $customer->id,
        ));
		
		echo 'Your email <strong>'.$email.'</strong> has been successfully unsubscribed from our newsletter</h4>';
	}
	
	public function actionReviews(){
		$this->render('reviews', array(
				'model'=>new CustomerOrderReview(),
		));
	}

    public function actionRepost($id) {
        $model = CustomerOrderReview::model()->findByPk($id);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://www.regencyshop.com/ajax_reviewadd.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $customer = $model->customer_order->customer;
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'customers_name' => $customer->first_name . ' ' . substr($customer->last_name, 0, 1),
            'rating' => $model->overall,
            'review' => $model->review_text,
            'ApiKeyCode' => 'REGREVIEW',
            'products_id' => $_POST['products_id']
        ));
        $result = curl_exec ($ch);
        if ($result !== false) {
            echo $result;
            $status = json_decode($result, true);
            if ($status['status'] == 'success') {
                $model->saveAttributes(array(
                    'remote_product_id' => (int) $_POST['products_id'],
                ));
            }
        } else {
            echo json_encode(array(
                'status' => 'failure',
                'message' => curl_error($ch),
            ));
        }
    }

    public function actionEditReview($id) {
        $model = CustomerOrderReview::model()->findByPk($id);
        if (empty($model))
            throw new CHttpException(404, 'Object not found');
        if (isset($_POST['CustomerOrderReview'])) {
            $model->attributes = $_POST['CustomerOrderReview'];
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo json_encode(array(
                        'status' => 'success',
                        'message' => 'Review updated successfully'
                    ));
                    Yii::app()->end();
                }
            }
        }
        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('updateReview', array('model' => $model));
    }
    public function actionDeleteReview($id)
    {
        $model = CustomerOrderReview::model()->findByPk($id);
        if (isset($model)) $model->delete();
        else throw new CHttpException(404, 'Object not found');
    }

	public function actionIssue_refund_ajax($order_id, $amount){
		$order = CustomerOrder::model()->findByPk($order_id);
		$refund = new CustomerOrderRefund();
		$refund->amount = round($amount, 2);
		$refund->customer_order_id = $order->id;
		$refund->date = date('Y-m-d H:i:s');
		if ($refund->save()){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'fail';
			$result['message'] = 'Error adding refund';
		}
	
		echo json_encode($result);
	}
	
	public function actionCreate_newsletter_ajax(){
		$items = $_POST['items'];
		$row = new CustomerNewsletter();
        //add subscribed and unsubscribed count
        $row->setAttributes(array(
            'subscribed' => Customer::model()->count('newsletter_unsubscribed = 0'),
            'unsubscribed' => Customer::model()->count('newsletter_unsubscribed = 1'),
        ));
		$row->date_added 		= date('Y-m-d H:i:s');
		$row->last_customer_id 	= 0;
		$row->completed 		= 0;
		$row->recommended_items	= implode(',', $items);
        $time = time();
        $m = intval(date('m', $time));
        $d = intval(date('d', $time));
        $y = intval(date('Y', $time));
        if ($d > 20){
            $row->start_time = $y.'-'.($m+1).'-04';
        } else {
            $row->start_time = $y.'-'.$m.'-04';
        }

		$row->save();
	}
}
