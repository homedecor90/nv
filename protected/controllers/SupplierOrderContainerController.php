<?php
/**
 * SupplierOrderContainer Controller
 */
class SupplierOrderContainerController extends Controller
{
    public $layout = 'column2';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions'=>array(
                    'archive',
                    'archived',
                ),
                'users'=>array('admin'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
    /**
     * Sends container to archive
     * @param $id
     */
    public function actionArchive($id)
    {
        $model = $this->loadModel($id);
        $model->saveAttributes(array(
            'archived' => 1,
        ));
    }

    public function actionArchived()
    {
        $model = new SupplierOrderContainer('search');
        $model->archived = 1;
        $this->render('archived', array('model' => $model));
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SupplierOrderContainer
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=SupplierOrderContainer::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
} 