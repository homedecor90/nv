<?php

class SupplierorderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	private $supplier_order_upload_path = "upload/attach/supplier_order/";
	private $supplier_order_upload_temp_path = "upload/";

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'index',
					'view',
					'accept_order_by_supplier_ajax',
					'deny_order_by_supplier_ajax',
					'set_etl_by_supplier_ajax',
					'complete_order_by_supplier_ajax',
					'accept_item_by_supplier_ajax',
					'save_bank_info_by_supplier_ajax',
					'deny_item_by_supplier_ajax',
					'deny_qty_change_ajax',
					'accept_qty_change_ajax',
					'shipping',
					'bid_container_by_shipper_ajax',
					'update',
					'create',
					'add_items',
					'delete',
					'view_container',
					'container_detail',
					'container_detail_by_shipper',
					'order_detail',
					'create_container_for_orders_by_admin_ajax',
					'pay_advanced_by_admin_ajax',
					'pay_remaining_by_admin_ajax',
					'choose_bid_by_admin_ajax',
					'upload',
					'admin',
					'admin_waiting',
					'reset_all_bids',
					'upload_official_docs_by_supplier_ajax',
					'view_supplier_order_track',
					'admin_archived',
					'index_archived',
					'get_pending_order_for_supplier_ajax',
					'approve_orders',
					'delete_pending_approval_orders',
					'delete_denied_item',
					'load_order_items_by_supplier_ajax',
					'load_order_items_by_admin_ajax',
					'get_pending_loading_items_by_supplier_id',
					'delete_existing_items_ajax',
					'update_existing_items_ajax',
					'delete_order_by_admin_ajax',
					'delete_pending_order_by_admin_ajax',
					'resend_order_by_admin_ajax',
					'delete_container_bid_ajax',
                    'pending_payment',
                    'payment_history',
                    'pay_shipper_invoice_ajax',
                    'approve_supplier_bank_info_ajax',
                    'deny_supplier_bank_info_ajax',
                    'change_order_status_ajax',
                    'get_container_ind_items_ids_ajax',
                    'get_custom_color_list_ajax',
					'scan_container_items_in_stock_ajax',
					'scan_loaded_items_in_ajax',
					'full_paid_doc',
					'adv_paid_doc',
					'final_doc',
					'resend_shipper_requests_ajax',
					'add_new_note_ajax',
					'edit_note_ajax',
					'print',
					'order_details_csv',
					'bank_info',
					'set_etl_by_admin_ajax',
					'combine_supplier_orders_ajax',
					'get_orders_for_supplier_ajax'
				),
				'users'=>array('*'),
				'expression' => 'UrlPermission::checkUrlPermission',
			),
			array('allow',
                'actions' => array('scanItemsIn', 'addItemNotes', 'archive'),
                'users' => array('admin'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SupplierOrder;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$supplierList = YumUser::model()->getSupplierList();
		$itemCodeList = Item::model()->getAllItemCodeList(true);

		if (isset($_POST['SupplierOrder']))
		{
			if (Yii::app()->user->isAdmin()){
				$_POST['SupplierOrder']["status"] = SupplierOrder::$STATUS_PENDING;
			} else {
				$_POST['SupplierOrder']["status"] = SupplierOrder::$STATUS_WAITING_APPROVAL;
			}
			
			$_POST['SupplierOrder']["created_date"] = date("Y-m-d H:i:s", time());
			
			$model->attributes=$_POST['SupplierOrder'];
			$model->operator_id = Yii::app()->user->id;
			
			if($model->save()){
				$post_data = $_POST;
				$model->createSupplierOrderItems($post_data);
				$model->attachPendingLoadingItems();
				
				$supplier_id = $model->supplier_id;
				Activity::model()->addActivity_CreateSupplierOrderByAdmin($supplier_id);
				
				AutoMail::sendMailNewSupplierOrderCreated($supplier_id);
				
				$this->redirect(array('admin'));
			}
		}
		
		$newItemModel = new Item;

		$this->render('create',array(
			'model'=>$model,
			'itemCodeList'=>$itemCodeList,
			'supplierList'=>$supplierList,
			'newItemModel'=>$newItemModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SupplierOrder']))
		{
			$model->attributes=$_POST['SupplierOrder'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new SupplierOrder('search');
        // get supplier bank info
        $criteria = new CDbCriteria();
        $criteria->compare('supplier_id', Yii::app()->user->id);
        $criteria->compare('accepted', 1);
        
        $bank_info = SupplierBankInfo::model()->find($criteria);
        if (!$bank_info){
            $bank_info = new SupplierBankInfo();
        }
        
		$this->render('index',array(
			'model'=>$model,
			'bank_info'=>$bank_info,
		));
	}
	
	public function actionIndex_archived()
	{
		$model=new SupplierOrder('search');

		$this->render('index_archived',array(
			'model'=>$model,
		));
	}
	
	public function actionAdd_items($order_id)
	{
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$supplierList = YumUser::model()->getSupplierList();
		$itemCodeList = Item::model()->getAllItemCodeList(true);
		
		if (isset($_POST['SupplierOrder']))
		{
			if (isset($_POST['item']['item_id'])){
                $post_data = $_POST;
                $supplierOrderModel->createSupplierOrderItems($post_data);
                
                $supplier_id = $supplierOrderModel->supplier_id;			
                Activity::model()->addActivity_AddItemsToSupplierOrder($supplier_id);
                AutoMail::sendMailNewItemsAddedToSupplierOrder($supplier_id);
			}
            
            $supplierOrderModel->discount_amount = $_POST['SupplierOrder']['discount_amount'];
            $supplierOrderModel->save();
            
			$this->redirect(array('order_detail', 'order_id'=>$supplierOrderModel->id));
		}
		
		$supplierOrderItemsModel = new SupplierOrderItems('search');
		$newItemModel = new Item;

		$this->render('add_items',array(
			'supplierOrderModel'=>$supplierOrderModel,
			'supplierOrderItemsModel'=>$supplierOrderItemsModel,
			'itemCodeList'=>$itemCodeList,
			'supplierList'=>$supplierList,
			'newItemModel'=>$newItemModel,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SupplierOrder('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SupplierOrder']))
			$model->attributes=$_GET['SupplierOrder'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionAdmin_waiting()
	{
		$model=new SupplierOrder('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SupplierOrder']))
			$model->attributes=$_GET['SupplierOrder'];

		$this->render('admin_waiting',array(
			'model'=>$model,
		));
	}
	
	public function actionAdmin_archived()
	{
		$model=new SupplierOrder('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SupplierOrder']))
			$model->attributes=$_GET['SupplierOrder'];

		$this->render('admin_archived',array(
			'model'=>$model,
		));
	}
	
	public function actionView_supplier_order_track()
	{
		$model=new SupplierOrder('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SupplierOrder']))
			$model->attributes=$_GET['SupplierOrder'];

		$this->render('view_supplier_order_track',array(
			'model'=>$model,
		));
	}
	
	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SupplierOrder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='supplier-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionReset_all_bids($container_id){
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		SupplierOrderContainerBidding::model()->deleteAll($criteriaBid);
		Activity::model()->addActivity_ResetAllSupplierOrderContainerBids();
	}
	
	public function actionAccept_order_by_supplier_ajax($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplierOrderModel->accepted_date = date("Y-m-d H:i:s", time());
		$supplierOrderModel->status = SupplierOrder::$STATUS_ACCEPTED;
		if ($supplierOrderModel->save()){
			$supplierOrderModel->modifyChangeItmesStatusAsAccepted();
		}
		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_AcceptSupplierOrderBySupplier($supplier_id);
	}
	
	public function actionDeny_order_by_supplier_ajax($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplierOrderModel->accepted_date = date("Y-m-d H:i:s", time());
		$supplierOrderModel->status = SupplierOrder::$STATUS_DENIED;
		if ($supplierOrderModel->save()){
			$supplierOrderModel->modifyChangeItmesStatusAsDenied();
		}
		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_DenySupplierOrderItemsBySupplier($supplier_id);
        AutoMail::sendMailSupplierDenyOrder($supplier_id);
	}
	
	public function actionSet_etl_by_supplier_ajax($order_id, $etl_date){
		if ($etl_date == "")
			return;

		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplierOrderModel->setETL($etl_date);
		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_SetEtlSupplierOrderBySupplier($supplier_id);
	}
	
	public function actionSet_etl_by_admin_ajax($id, $etl_date, $container = 0){
		if ($etl_date == ""){
			return;
        }
		if ($container){
            $criteria = new CDbCriteria();
            $criteria->compare('container_id', $id);
            $orders = SupplierOrder::model()->findAll($criteria);
        } else {
            $order = SupplierOrder::model()->findByPk($id);
            $orders = array($order);
        }

        foreach ($orders as $order){
            $order->updateETLByAdmin($etl_date);
            $result['status'] = 'success';
			$result['message'] = 'ETL has been successfully saved';
        }

		echo json_encode($result);
	}
	
	public function actionComplete_order_by_supplier_ajax($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplierOrderModel->complete();
		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_CompleteSupplierOrderBySupplier($supplier_id);
		AutoMail::sendMailSupplierFinishOrder($supplier_id);
	}
	
	public function actionPay_advanced_by_admin_ajax($order_id, $file_name=""){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplierOrderModel->advanced_paid_date = date("Y-m-d H:i:s", time());
		$supplierOrderModel->status = SupplierOrder::$STATUS_ADVANCED_PAID;
		$total_price = SupplierOrder::model()->getSupplierOrderTotalPrice($order_id);
		$supplierOrderModel->advanced_paid_amount = round($total_price * 0.3, 2);
		
		if ($file_name != ""){
			$file_full_path = $this->supplier_order_upload_temp_path.$file_name;
			
			$upload_path = $this->supplier_order_upload_path;
			if (!is_dir($upload_path.$order_id)){
				mkdir($upload_path.$order_id);
			}
			
			if (!is_dir($upload_path.$order_id."/advanced_piad")){
				mkdir($upload_path.$order_id."/advanced_piad");
			}
			
			if (file_exists($file_full_path)){
				$file_dest_path = $upload_path.$order_id."/advanced_piad/".$file_name;
				copy($file_full_path, $file_dest_path);
				unlink($file_full_path);
				$file_url = "upload/attach/supplier_order/".$order_id."/advanced_piad/".$file_name;
				if (!$supplierOrderModel->advanced_paid_attach_file_path){
                    $supplierOrderModel->advanced_paid_attach_file_path = $file_url;
                } else {
                    $supplierOrderModel->advanced_paid_attach_file_path = $supplierOrderModel->advanced_paid_attach_file_path.'||'.$file_url;
                }
			}
		}
		
		if ($supplierOrderModel->save() == false){
			echo CHtml::errorSummary($supplierOrderModel);
			return;
		}
		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_PayAdvancedSupplierOrderByAdmin($supplier_id);
		AutoMail::sendMailAdvnacedPaymentFileUploaded($supplier_id);
	}
	
	public function actionPay_remaining_by_admin_ajax($order_id, $file_name=""){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplierOrderModel->full_paid_date = date("Y-m-d H:i:s", time());
		$supplierOrderModel->status = SupplierOrder::$STATUS_FULL_PAID;
		
		if ($file_name != ""){
			$file_full_path = $this->supplier_order_upload_temp_path.$file_name;
			
			$upload_path = $this->supplier_order_upload_path;			
			if (!is_dir($upload_path.$order_id)){
				mkdir($upload_path.$order_id);
			}
			
			if (!is_dir($upload_path.$order_id."/full_piad")){
				mkdir($upload_path.$order_id."/full_piad");
			}
			
			if (file_exists($file_full_path)){
				$file_dest_path = $upload_path.$order_id."/full_piad/".$file_name;			
				copy($file_full_path, $file_dest_path);
				unlink($file_full_path);
				$file_url = "upload/attach/supplier_order/".$order_id."/full_piad/".$file_name;
				// $supplierOrderModel->full_paid_attach_file_path = $file_url;
                if (!$supplierOrderModel->full_paid_attach_file_path){
                    $supplierOrderModel->full_paid_attach_file_path = $file_url;
                } else {
                    $supplierOrderModel->full_paid_attach_file_path = $supplierOrderModel->full_paid_attach_file_path.'||'.$file_url;
                }
			}
		}
		
		$supplierOrderModel->save();
		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_PayRemainingSupplierOrderByAdmin($supplier_id);
		AutoMail::sendMailAdvnacedPaymentFileUploaded($supplier_id);
	}
	
    public function actionDelete_order_by_admin_ajax($order_id){
        if (!Yii::app()->user->isAdmin()){
            $result['status'] = 'fail';
            echo json_encode($result);
            return;
        }
        
        $order = SupplierOrder::model()->findByPk($order_id);
        $criteria_order_items = new CDbCriteria();
        $criteria_order_items->compare('supplier_order_id', $order_id);
        
        $order_items = SupplierOrderItems::model()->findAll($criteria_order_items);
        
        foreach ($order_items as $row){
        	$row->deleteFromOrder();
        }
        
        $order->delete();
        
        $result['status'] = 'success';
        echo json_encode($result);
        
        /*$order = SupplierOrder::model()->findByPk($order_id);
        
        if ($order->status != SupplierOrder::$STATUS_DENIED){
            $result['status'] = 'fail';
            echo json_encode($result);
            return;
        }
        
        $order->deleted = 1;
        if ($order->save()){
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = CHtml::errorSummary($order);
        }
        
        echo json_encode($result);*/
    }
    
    public function actionResend_order_by_admin_ajax($order_id){
        if (!Yii::app()->user->isAdmin()){
            $result['status'] = 'fail';
            echo json_encode($result);
            return;
        }
        
        $order = SupplierOrder::model()->findByPk($order_id);
        
        if ($order->status != SupplierOrder::$STATUS_DENIED){
            $result['status'] = 'fail';
            echo json_encode($result);
            return;
        }
        
        // save order row
        $order->status = CustomerOrder::$STATUS_PENDING;
        if (!$order->save()){
            $result['status'] = 'fail';
            $result['message'] = CHtml::errorSummary($order);
        }
        
        // we need to get all individual items for this order
        // and all orderItems rows and set correct status for all of them
        
        $criteria_order_items = new CDbCriteria();
        $criteria_order_items->compare('supplier_order_id', $order_id);

        $order_items = SupplierOrderItems::model()->findAll($criteria_order_items);
        $order_items_ids = array();
        foreach ($order_items as $row){
            $order_items_ids[] = $row->id;
        }
        
        $criteria_ind_items_sup = new CDbCriteria();
        $criteria_ind_items_sup->addInCondition('supplier_order_items_id', $order_items_ids);
        $ind_items_sup_items = IndividualItemSupplierOrderItems::model()->findAll($criteria_ind_items_sup);
        
        $ind_items_ids = array();
        foreach ($ind_items_sup_items as $row){
            $ind_items_ids[] = $row->individual_item_id;
        }
        
        $criteria_ind_items = new CDbCriteria();
        $criteria_ind_items->addInCondition('id', $ind_items_ids);
        
       if (!SupplierOrderItems::model()->updateAll(array('status'=>SupplierOrder::$STATUS_PENDING), $criteria_order_items)){
            $result['status'] = 'fail';
            $result['message'] = 'Error updating data';
        } else if (!Individualitem::model()->updateAll(array('status'=>Individualitem::$STATUS_ORDERED_BUT_WAITING), $criteria_ind_items)){
            $result['status'] = 'fail';
            $result['message'] = 'Error updating data ii';
        } else {
            
            $supplier_id = $order->supplier_id;
            Activity::model()->addActivity_CreateSupplierOrderByAdmin($supplier_id);
            
            AutoMail::sendMailNewSupplierOrderCreated($supplier_id);
            
            $result['status'] = 'success';
        }
        
        echo json_encode($result);
    }
    
    public function actionDelete_pending_order_by_admin_ajax($order_id){
        
        $order = SupplierOrder::model()->findByPk($order_id);
        $criteria_order_items = new CDbCriteria();
        $criteria_order_items->compare('supplier_order_id', $order_id);

        $order_items = SupplierOrderItems::model()->findAll($criteria_order_items);
        
        foreach ($order_items as $row){
            $row->deleteFromOrder();
        }
        
        $order->delete();

        /*
        $order_items_ids = array();
        foreach ($order_items as $row){
            $order_items_ids[] = $row->id;
        }
        
        $criteria_ind_items_sup = new CDbCriteria();
        $criteria_ind_items_sup->addInCondition('supplier_order_items_id', $order_items_ids);
        $ind_items_sup_items = IndividualItemSupplierOrderItems::model()->findAll($criteria_ind_items_sup);
        
        $ind_items_ids = array();
        foreach ($ind_items_sup_items as $row){
            $ind_items_ids[] = $row->individual_item_id;
        }

        $criteria_ind_items = new CDbCriteria();
        $criteria_ind_items->addInCondition('id', $ind_items_ids);
        $criteria_ind_items->compare('customer_order_items_id', '<>0');
        $count_sold_items = Individualitem::model()->count($criteria_ind_items);
        
        if ($count_sold_items){
            $result['message'] = 'Order contains sold items';
            $result['status'] = 'fail';
            echo json_encode($result);
            return;
        }
        
        $criteria_ind_items = new CDbCriteria();
        $criteria_ind_items->addInCondition('id', $ind_items_ids);
        
        $order->delete();
        SupplierOrderItems::model()->deleteAll($criteria_order_items);
        IndividualItemSupplierOrderItems::model()->deleteAll($criteria_ind_items_sup);
        Individualitem::model()->deleteAll($criteria_ind_items);
        */
        $result['status'] = 'success';
        echo json_encode($result);
    }
    
	public function actionShipping(){
		$supplierOrderContainerModel = new SupplierOrderContainer('search');
		$supplierOrderContainerBiddingModel = new SupplierOrderContainerBidding();

		$this->render('shipping',array(
			'supplierOrderContainerModel'=>$supplierOrderContainerModel,
			'supplierOrderContainerBiddingModel'=>$supplierOrderContainerBiddingModel,
		));
	}
	
	public function actionAccept_item_by_supplier_ajax($item_id){
		$supplierOrderItemModel = SupplierOrderItems::model()->findByPk($item_id);
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplierOrderItemModel->supplier_order_id);
		$supplierOrderItemModel->status = "Accepted";
		$supplierOrderItemModel->processed_date = date("Y-m-d H:i:s", time());
		if ($supplierOrderItemModel->save()){
			$supplierOrderItemModel->acceptAddedItem();
			$supplierOrderItemModel->combine();
		} else {
			return false;
		}
				
		$supplier_id = $supplierOrderModel->supplier_id;		
		Activity::model()->addActivity_AcceptSupplierOrderItemsBySupplier($supplier_id);
	}
	
	public function actionDeny_item_by_supplier_ajax($item_id){
		$supplierOrderItemModel = SupplierOrderItems::model()->findByPk($item_id);
		$supplierOrderItemModel->status = SupplierOrder::$STATUS_DENIED;
		$supplierOrderItemModel->processed_date = date("Y-m-d H:i:s", time());
		if ($supplierOrderItemModel->save()){
			$supplierOrderItemModel->denyAddedItem();
		}
		
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplierOrderItemModel->supplier_order_id);		
		$supplier_id = $supplierOrderModel->supplier_id;
		Activity::model()->addActivity_DenySupplierOrderItemsBySupplier($supplier_id);
	}
	
	public function actionUpload()
	{
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder='upload/';// folder for uploaded files
        $allowedExtensions = array("*");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
 
        $fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
        $fileName=$result['filename'];//GETTING FILE NAME
 
        echo json_encode($fileSize);// it's array
	}
	
	private function addActivity($message){
		$acitivityModel = new Activity();
		$acitivityModel->message = $message;
		$acitivityModel->date = date("Y-m-d H:i:s", time());
		$acitivityModel->save();
	}
	
	public function actionCreate_container_for_orders_by_admin_ajax(){
		$order_id_list = $_POST['order_id_list'];
		if ($order_id_list == null)
			return;

		$destination_port = $_POST['destination_port'];
		$original_port = $_POST['original_port'];
		$user_id =  Yii::app()->user->id;
		
		$supplierOrderContainerModel = new SupplierOrderContainer();
		$supplierOrderContainerModel->created_date = date("Y-m-d H:i:s", time());
		$supplierOrderContainerModel->destination_port = $destination_port;
		$supplierOrderContainerModel->original_port = $original_port;
		$supplierOrderContainerModel->status = "Pending";
		$supplierOrderContainerModel->terms = SupplierOrderContainer::$TERMS_EXW;
		$supplierOrderContainerModel->operator_id = $user_id;
		
		if ($supplierOrderContainerModel->save()){
			$supplierOrderContainerModel->addSupplierOrders($order_id_list);
			$result['message'] = "success";
			
			Activity::model()->addActivity_CreateSupplierOrderContainerByAdmin();
			AutoMail::sendMailNewSupplierOrderContainerCreated($supplierOrderContainerModel);
		} else {
			$result['message'] = "fail";
			$result['error'] = CHtml::errorSummary($supplierOrderContainerModel);
		}
		
		echo json_encode($result);
	}
	
	public function actionView_container(){
		// CustomerOrder::addCustomerNotesEtaUpdated(3);
        $supplierOrderContainerModel = new SupplierOrderContainer('search');
        $supplierOrderContainerModel->archived = 0;
		$this->render('view_container',array(
			'supplierOrderContainerModel'=>$supplierOrderContainerModel,
		));
	}
	
	public function actionBid_container_by_shipper_ajax(){
		$supplierOrderContainerBiddingModel = new SupplierOrderContainerBidding();
		
		$supplier_shipper_id = $_POST['supplier_shipper_id'];
		$supplierOrderContainerBiddingModel->supplier_order_container_id 	= $_POST['supplier_order_container_id'];
		$supplierOrderContainerBiddingModel->cutoff_date 					= date("Y-m-d H:i:s", strtotime($_POST['cutoff_date']));
		$supplierOrderContainerBiddingModel->bid_eta					 	= date("Y-m-d H:i:s", strtotime($_POST['bid_eta']));
		$supplierOrderContainerBiddingModel->supplier_shipper_id 			= $supplier_shipper_id;
		$supplierOrderContainerBiddingModel->bid_date 						= date("Y-m-d H:i:s", time());
		$supplierOrderContainerBiddingModel->container_size 				= $_POST['container_size'];
		
		$criteriaSupplierOrder = new CDbCriteria;
		$criteriaSupplierOrder->compare('container_id', $supplierOrderContainerBiddingModel->supplier_order_container_id);
		$criteriaSupplierOrder->order = 'etl desc';
		$lastSupplierOrder = SupplierOrder::model()->find($criteriaSupplierOrder);
		$etl = $lastSupplierOrder->etl;
		
		$result = array();
		$result['message'] = "success";
		
		$cutoff_date = $supplierOrderContainerBiddingModel->cutoff_date;
		$eta = $supplierOrderContainerBiddingModel->bid_eta;
		
		if (strtotime($cutoff_date) < strtotime($etl)){
			$result['message'] = 'Cut Off Date is before the E.T.L';
		} else if (strtotime($eta) < strtotime($etl)){
			$result['message'] = 'E.T.A is before the E.T.L';
		} else if (strtotime($eta) < strtotime($cutoff_date)){
			$result['message'] = 'E.T.A is before the Cut Off Date';
		} else {
			if ($supplierOrderContainerBiddingModel->save()){
				$bid_id = $supplierOrderContainerBiddingModel->id;
				$optionNameList = $_POST['option_name'];
				$optionPriceList = $_POST['option_price'];
				
				foreach ($optionNameList as $key=>$option_name){
					$option_price = $optionPriceList[$key];
					$supplierOrderContainerBiddingOptionModel = new SupplierOrderContainerBiddingOptions();
					
					$supplierOrderContainerBiddingOptionModel->option_name = $option_name;
					$supplierOrderContainerBiddingOptionModel->price = $option_price;
					$supplierOrderContainerBiddingOptionModel->bidding_id = $bid_id;
					
					$supplierOrderContainerBiddingOptionModel->save();
				}
			}
			
			Activity::model()->addActivity_BidSupplierOrderContainerBySupplierShipper($supplier_shipper_id);
			AutoMail::sendMailSupplierShipperPlaceBid($supplier_shipper_id);
		}
		
		echo json_encode($result);
	}
	
	public function actionContainer_detail($container_id){
        $supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($container_id);
		$supplierOrderContainerBiddingModel = new SupplierOrderContainerBidding('search');
		
		$this->render('container_detail',array(
			'supplierOrderContainerBiddingModel'=>$supplierOrderContainerBiddingModel,
			'supplierOrderContainerModel'=>$supplierOrderContainerModel,
		));
	}
	
	public function actionContainer_detail_by_shipper($container_id){
		$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($container_id);
		$supplierOrderContainerBiddingModel = new SupplierOrderContainerBidding('search');
		$supplier_shipper_id =  Yii::app()->user->id;
		$bAlreadyBid = SupplierOrderContainerBidding::model()->isAlreadyBid($container_id, $supplier_shipper_id);
		
		$this->render('container_detail_by_shipper',array(
			'supplierOrderContainerBiddingModel'=>$supplierOrderContainerBiddingModel,
			'supplierOrderContainerModel'=>$supplierOrderContainerModel,
			'bAlreadyBid'=>$bAlreadyBid,
            'lowest_price'=>SupplierOrderContainerBidding::model()->getLowestPrice($container_id, true)
		));
	}
	
	public function actionOrder_detail($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);

        if (empty($supplierOrderModel))
            throw new CHttpException(404, 'Object not found');

		if (!YumUser::isOperator(true) && $supplierOrderModel->supplier_id != Yii::app()->user->id){
			return false;
		}
		$supplierOrderOfficialDocsModel = new SupplierOrderOfficialDocs();		
		$newSupplierOrderNoteModel = new SupplierOrderNote();		
		$supplierOrderOfficialDocsModel->supplier_order_id = $order_id;
		// $htmlPendingLoadingItems = SupplierOrder::model()->getPendingLoadingItemsBySupplierID($supplier_id);
		
		$supplierOrderNote = new SupplierOrderNote('search');
		
		$newItemModel = new Item;
		$itemCodeList = Item::model()->getAllItemCodeList(true);
		
		$this->render('order_detail',array(
			'supplierOrderModel'=>$supplierOrderModel,
			'newSupplierOrderNoteModel'=>$newSupplierOrderNoteModel,
			'supplierOrderNote'=>$supplierOrderNote,
			'supplierOrderOfficialDocsModel'=>$supplierOrderOfficialDocsModel,
			'newItemModel'=>$newItemModel,
			'itemCodeList'=>$itemCodeList,
			// 'htmlPendingLoadingItems'=>$htmlPendingLoadingItems,
		));
	}
	
	public function actionChoose_bid_by_admin_ajax($bid_id, $terms){
		$supplierOrderContainerBiddingModel = SupplierOrderContainerBidding::model()->findByPk($bid_id);
		$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk(
			$supplierOrderContainerBiddingModel->supplier_order_container_id);
			
		$supplierOrderContainerModel->status = SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER;
		$supplierOrderContainerModel->chosen_date = date("Y-m-d H:i:s", time());
		$supplierOrderContainerModel->terms = $terms;
		$supplierOrderContainerModel->supplier_shipper_id = $supplierOrderContainerBiddingModel->supplier_shipper_id;
		$supplierOrderContainerModel->save();
		
		$criteriaIndividualItems = new CDbCriteria;
		$criteriaIndividualItems->compare('supplier_order_container_id', $supplierOrderContainerModel->id);
		$attributesIndividualItems = array(
			'cutoff_date'=>$supplierOrderContainerBiddingModel->cutoff_date,
			'eta'=>$supplierOrderContainerBiddingModel->bid_eta,
			'status'=>Individualitem::$STATUS_ORDERED_WITH_ETA,
		);
		Individualitem::model()->updateAll($attributesIndividualItems, $criteriaIndividualItems);
		
        CustomerOrder::addCustomerNotesEtaUpdated($supplierOrderContainerModel->id);
        
		Activity::model()->addActivity_ChooseSupplierOrderContainerShipper();
		AutoMail::sendMailBidSelected($supplierOrderContainerModel->id);
	}
	
	public function actionUpload_official_docs_by_supplier_ajax($order_id, $file_name=""){
		$file_full_path = $this->supplier_order_upload_temp_path.$file_name;
			
		$upload_path = $this->supplier_order_upload_path;			
		if (!is_dir($upload_path.$order_id)){
			mkdir($upload_path.$order_id);
		}
		
		if (!is_dir($upload_path.$order_id."/official_docs")){
			mkdir($upload_path.$order_id."/official_docs");
		}
		
		echo $file_full_path;
		
		if (file_exists($file_full_path)){
			$file_dest_path = $upload_path.$order_id."/official_docs/".$file_name;			
			copy($file_full_path, $file_dest_path);
			unlink($file_full_path);
			$file_url = "upload/attach/supplier_order/".$order_id."/official_docs/".$file_name;

            $supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
            $supplierOrderModel->advanced_paid_attach_file_path = $file_url;
			$supplierOrderOfficialDocsModel = new SupplierOrderOfficialDocs();
			$supplierOrderOfficialDocsModel->supplier_order_id = $order_id; 
			$supplierOrderOfficialDocsModel->url = $file_url;
			$supplierOrderOfficialDocsModel->file_name = $file_name;
			$supplierOrderOfficialDocsModel->created_on = date("Y-m-d H:i:s", time());
			$supplierOrderOfficialDocsModel->save();
			

			$supplierOrderModel->status = SupplierOrder::$STATUS_ARCHIVED;
			$supplierOrderModel->save();
			AutoMail::sendMailSupplierOrderUploadedOfficialDocumentsBySupplier($order_id, $file_dest_path);

		}
	}
	
	/**
	 * Returns all orders except 
	 * @param int $supplier_id
	 * @param array $exclude
	 * @param string $no_container if set to 1 - only orders that are not combined are returned
	 */
	public function actionGet_orders_for_supplier_ajax($supplier_id, $no_container = '0', array $exclude = array()){
		$criteria=new CDbCriteria;
		$criteria->compare('supplier_id',$supplier_id);
		if (count($exclude)){
			$criteria->addNotInCondition('id', $exclude);
		}
		
		if ($no_container == '1'){
			$criteria->compare('container_id', '0');
		}
		
		$criteria->addCondition('status != "'.SupplierOrder::$STATUS_ARCHIVED.'"');
		$orders = SupplierOrder::model()->findAll($criteria);
		
		$this->layout = false;
		$this->render('_order_list_ajax',array('orders'=>$orders));
	}
	
	public function actionGet_pending_order_for_supplier_ajax($supplier_id, array $exclude = array()){
		$criteria=new CDbCriteria;
		$criteria->compare('status', SupplierOrder::$STATUS_PENDING, true, "OR");
		$criteria->compare('status', SupplierOrder::$STATUS_ACCEPTED, true, "OR");
		$criteria->compare('status', SupplierOrder::$STATUS_ADVANCED_PAID, true, "OR");
		$criteria->compare('status', SupplierOrder::$STATUS_MAKING, true, "OR");
		$criteria->compare('supplier_id',$supplier_id);
		if (count($exclude)){
			$criteria->addNotInCondition('id', $exclude);
		}
		$orders = SupplierOrder::model()->findAll($criteria);

		$this->layout = false;
		$this->render('_order_list_ajax',array('orders'=>$orders));
	}
	
	/**
	 * 
	 * @param int $order_id ID for order that is attached
	 * @param int $recipient_order_id ID for order that another one is attached to
	 */
	public function actionCombine_supplier_orders_ajax($order_id, $recipient_order_id){
		$order = SupplierOrder::model()->with('order_items')->findByPk($order_id);
		$recipient_order = SupplierOrder::model()->findByPk($recipient_order_id);
		foreach ($order->order_items as $items){
			$items->supplier_order_id = $recipient_order_id;
			$items->save();
			
			$ind_item_ids = array();
			foreach ($items->ind_items as $ind_item){
				$ind_item_ids[] = $ind_item->id;
			}
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $ind_item_ids);
 			$criteria2 = clone($criteria);
			//$criteria->compare('supplier_order_id', '>0');
			Individualitem::model()->updateAll(array('supplier_order_id'=>$recipient_order_id), $criteria);
			
// 			$criteria2->compare('supplier_order_container_id', '>0');
 			Individualitem::model()->updateAll(array('supplier_order_container_id'=>$recipient_order->container_id), $criteria2);
			$items->combine();
		}
		
		$order->delete();
		
		$result['status'] = 'success';
		
		echo json_encode($result);
	}
	
	public function actionApprove_orders($order_id_list){
		$arrOrderId = explode("_", $order_id_list);
		if ($arrOrderId == null)
			return;
		
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->addInCondition('id', $arrOrderId);
		$attributesOrders = array(
			'status'=>SupplierOrder::$STATUS_PENDING,
		);
		
		SupplierOrder::model()->updateAll($attributesOrders, $criteriaOrders);
		$waitingSupplierOrderCount = SupplierOrder::model()->getWaitingOrderCount();
		$result['waitingSupplierOrderCount'] = $waitingSupplierOrderCount;
		echo json_encode($result);
	}
	
    public function actionDelete_pending_approval_orders($order_id_list){
		$arrOrderId = explode("_", $order_id_list);
		if ($arrOrderId == null)
			return;
        
        // $order = SupplierOrder::model()->findByPk($order_id);
        $criteria_order_items = new CDbCriteria();
        $criteria_order_items->addInCondition('supplier_order_id', $arrOrderId);

        $order_items = SupplierOrderItems::model()->findAll($criteria_order_items);
        $order_items_ids = array();
        foreach ($order_items as $row){
            $order_items_ids[] = $row->id;
        }
        
        $criteria_ind_items_sup = new CDbCriteria();
        $criteria_ind_items_sup->addInCondition('supplier_order_items_id', $order_items_ids);
        $ind_items_sup_items = IndividualItemSupplierOrderItems::model()->findAll($criteria_ind_items_sup);
        
        $ind_items_ids = array();
        foreach ($ind_items_sup_items as $row){
            $ind_items_ids[] = $row->individual_item_id;
        }

        $criteria_ind_items = new CDbCriteria();
        $criteria_ind_items->addInCondition('id', $ind_items_ids);
        $criteria_ind_items->compare('customer_order_items_id', '<>0');
        $count_sold_items = Individualitem::model()->count($criteria_ind_items);
        
        if ($count_sold_items){
            $result['message'] = 'Some items are already sold';
            $result['status'] = 'fail';
            echo json_encode($result);
            return;
        }
        
        $criteria_ind_items = new CDbCriteria();
        $criteria_ind_items->addInCondition('id', $ind_items_ids);
        
        // $order->delete();
        $criteria_order = new CDbCriteria();
        $criteria_order->addInCondition('id', $arrOrderId);
        
        SupplierOrder::model()->deleteAll($criteria_order);
        SupplierOrderItems::model()->deleteAll($criteria_order_items);
        IndividualItemSupplierOrderItems::model()->deleteAll($criteria_ind_items_sup);
        Individualitem::model()->deleteAll($criteria_ind_items);
        
        $result['status'] = 'success';
        echo json_encode($result);
	}
    
	// public function actionDelete_denied_item($item_id){
		// $criteria = new CDbCriteria;
		
		// $criteria->compare('status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
		// $criteria->compare('status', Individualitem::$STATUS_DENIED, true, "or");
		// $criteria->compare('item_id', $item_id);
		// Individualitem::model()->deleteAll($criteria);
	// }
	
	public function actionLoad_order_items_by_supplier_ajax($supplier_order_id, $item_id_list, $quantity_list){
		SupplierOrder::loadOrderItems($supplier_order_id, $item_id_list, $quantity_list);
	}
	
	public function actionLoad_order_items_by_admin_ajax($supplier_order_id, $item_id_list, $quantity_list){
		SupplierOrder::loadOrderItems($supplier_order_id, $item_id_list, $quantity_list);
		$order = SupplierOrder::model()->findByPk($supplier_order_id);
		$order->loaded_by_admin = 1;
		if ($order->save()){
			//attach pending loaded items to the existing pending order if we have any for this supplier
			$p_order = SupplierOrder::getPendingOrder($order->supplier_id);
			// TODO: check what to do when there's no pending order
			if ($p_order){
				$p_order->attachPendingLoadingItems();
			}
			
			// scan items as IN
			$criteria = new CDbCriteria();
			$criteria->compare('supplier_order_id', $supplier_order_id);
			$criteria->compare('loaded', Individualitem::$LS_LOADED);
			echo Individualitem::model()->updateAll(array(
                'status'=>Individualitem::$STATUS_IN_STOCK,
                'scanned_in_date' => date('Y-m-d H:i:s'),
            ), $criteria);
			die();
			
		} else {
			echo 'Error saving order';
		}
	}
	
	public function actionScan_loaded_items_in_ajax($order_id){
		$order = SupplierOrder::model()->findByPk($order_id);
		if ($order->loaded_by_admin){
			$criteria = new CDbCriteria();
			$criteria->compare('supplier_order_id', $order_id);
			$criteria->compare('loaded', Individualitem::$LS_LOADED);
			Individualitem::model()->updateAll(array('status'=>Individualitem::$STATUS_IN_STOCK), $criteria);
			$result['status'] = 'success';
			$result['items'] = Individualitem::model()->count($criteria);
		} else {
			$result['status'] 	= 'fail';
			$result['message'] 	= 'Loaded items are not confirmed by admin yet';
		}
		
		echo json_encode($result);
	}
	
	public function actionGet_pending_loading_items_by_supplier_id($supplier_id){
		$htmlPendingLoadingItems = SupplierOrder::getPendingLoadingItemsBySupplierIDStr($supplier_id);
		echo $htmlPendingLoadingItems;
	}
    
    public function actionDelete_existing_items_ajax($items_id)
    {
        $orderItemsModel = SupplierOrderItems::model()->findByPk($items_id);
        // $order_id = $orderItemsModel->supplier_order_id;
        // if items are already accepted changes should be verified and accepted by supplier
        /*if ($orderItemsModel->status == 'Accepted'){
            $orderItemsModel->new_quantity = 0;
            $orderItemsModel->quantity_updated = 1;
            if ($orderItemsModel->save()){
                //send email notification to supplier
                AutoMail::sendMailQuantityUpdatedSupplierOrder($orderItemsModel->order->supplier_id);
                $res['status'] = 'success';
            } else {
                $res['status'] = 'fail';
                $res['message'] = 'Error saving data';
            }
        } else {*/
        	$orderItemsModel->deleteFromOrder();
        	AutoMail::sendMailItemRemovedSupplierOrder($orderItemsModel->order->supplier_id, $orderItemsModel->item_id);
        	
            $res['status'] = 'success';
        //}
        echo json_encode($res);
    }
    
    public function actionUpdate_existing_items_ajax($items_id, $new_value)
    {
        $orderItemsModel = SupplierOrderItems::model()->findByPk($items_id);

        // if items are already accepted changes should be verified and accepted by supplier
        if ($orderItemsModel->status == 'Accepted'){
            $orderItemsModel->new_quantity = $new_value;
            $orderItemsModel->quantity_updated = 1;
            if ($orderItemsModel->save()){
                //send email notification to supplier
                AutoMail::sendMailQuantityUpdatedSupplierOrder($orderItemsModel->order->supplier_id);
                $res['status'] = 'success';
                $res['qty'] = $orderItemsModel->quantity.' <span>('.$new_value.')</span>';
            } else {
                $res['status'] = 'fail';
                $res['message'] = 'Error saving data';
            }
        } else {
            if ($orderItemsModel->updateQty($new_value)){
                $res['status'] = 'success';
                $res['qty'] = $new_value;
            } else {
                $res['status'] = 'fail';
                $res['message'] = 'Error saving data';
            }
        }
        
        echo json_encode($res);
    }
    
    public function actionDelete_container_bid_ajax($bid_id){
        $bid = SupplierOrderContainerBidding::model()->findByPk($bid_id);
        
        if ($bid->delete()){
            $criteria = new CDbCriteria();
            $criteria->compare('bidding_id', $bid_id);
            SupplierOrderContainerBiddingOptions::model()->deleteAll($criteria);
            
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error deleting bid';
        }
        
        echo json_encode($result);
    }
    
    public function actionPending_payment(){
        $container_model = new SupplierOrderContainer();
        $container_model->paid = 0;
        $list = YumUser::getSupplierShipperList();
        // $shipper_list = array_merge(array('0'=>''), $shipper_list);
        $shipper_list = array(0=>'');
        foreach($list as $i=>$val){
            $shipper_list[$i] = $val;
        }
        if ($_GET['shipper_id']){
            $container_model->supplier_shipper_id = $_GET['shipper_id'];
        }
        
        $this->render('pending_payment', array(
            'container_model'=>$container_model,
            'shipper_list'=>$shipper_list
        ));
    }
    
    public function actionPayment_history(){
        $container_model = new SupplierOrderContainer();
        $container_model->paid = 1;
        
        $list = YumUser::getSupplierShipperList();
        // $shipper_list = array_merge(array('0'=>''), $shipper_list);
        $shipper_list = array(0=>'');
        foreach($list as $i=>$val){
            $shipper_list[$i] = $val;
        }
        if ($_GET['shipper_id']){
            $container_model->supplier_shipper_id = $_GET['shipper_id'];
        }
        
        $this->render('payment_history', array(
            'container_model'=>$container_model,
            'shipper_list'=>$shipper_list
        ));
    }
    
    public function actionPay_shipper_invoice_ajax($container_id, $check_number){
        $container = SupplierOrderContainer::model()->findByPk($container_id);
        $container->paid = 1;
        $container->paid_date = date("Y-m-d H:i:s", time());
        $container->paid_check = $check_number;
        if ($container->save()){
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving data';
        }
        
        echo json_encode($result);
    }
    
    public function actionAccept_qty_change_ajax($items_id){
        $itemsModel = SupplierOrderItems::model()->findByPk($items_id);
        if ($itemsModel->new_quantity != 0){
            if ($itemsModel->updateQty($itemsModel->new_quantity)){
                $itemsModel->new_quantity = 0;
                $itemsModel->quantity_updated = 0;
                $itemsModel->save();
                $result['status'] = 'success';
            } else {
                $result['status'] = 'fail';
                $result['message'] = 'Error saving new data';
            }
        } else {
            if ($itemsModel->deleteFromOrder()){
                $result['status'] = 'success';
            } else {
                $result['status'] = 'fail';
                $result['message'] = 'Error saving new data';
            }
        }
        
        echo json_encode($result);
    }
    
    public function actionDeny_qty_change_ajax($items_id){
        $itemsModel = SupplierOrderItems::model()->findByPk($items_id);
        $itemsModel->new_quantity = 0;
        $itemsModel->quantity_updated = 0;
        $itemsModel->save();
        $result['status'] = 'success';
    }
    
    public function actionSave_bank_info_by_supplier_ajax(){
        $criteria = new CDbCriteria();
        $criteria->compare('supplier_id', Yii::app()->user->id);
        $criteria->compare('accepted', 0);
        
        $bank_info = SupplierBankInfo::model()->find($criteria);
        if (!$bank_info){
            $bank_info = new SupplierBankInfo();
            $bank_info->accepted = 0;
        }
        
        $bank_info->attributes = $_POST;
        $bank_info->supplier_id = Yii::app()->user->id;
        $bank_info->save();
    }
    
    /**
     * Supplier's page
     */
    public function actionBank_info(){
    	$criteria = new CDbCriteria();
    	$criteria->compare('supplier_id', Yii::app()->user->id);
    	$criteria->order = 'accepted DESC';
    	$model = SupplierBankInfo::model()->find($criteria);
    	if (!$model){
    		$model = new SupplierBankInfo();
    		$model->supplier_id = Yii::app()->user->id;
    	}
    	
    	// Uncomment the following line if AJAX validation is needed
    	// $this->performAjaxValidation($model);
    	
    	if(isset($_POST['SupplierBankInfo']))
    	{
    		$model->attributes=$_POST['SupplierBankInfo'];
    		$model->accepted = 1;
    		if($model->save()){
    			//$this->redirect(array('bank_info');
    			$criteria = new CDbCriteria();
    			$criteria->compare('supplier_id', Yii::app()->user->id);
    			$criteria->compare('accepted', 0);
    			SupplierBankInfo::model()->deleteAll($criteria);
    		}
    	}
    	
    	$this->render('bank_info',array(
    			'model'=>$model,
    	));
    }
    
    public function actionApprove_supplier_bank_info_ajax($row_id){
        $row_new = SupplierBankInfo::model()->findByPk($row_id);
        $criteria = new CDbCriteria();
        $criteria->compare('supplier_id', $row_new->supplier_id);
        $criteria->compare('accepted', 1);
        SupplierBankInfo::model()->deleteAll($criteria);
        $row_new->accepted = 1;
        $row_new->save();
        AutoMail::sendMailBankInfoAccepted($row_new->supplier_id);
    }
    
    public function actionDeny_supplier_bank_info_ajax($row_id){
        SupplierBankInfo::model()->deleteByPk($row_id);
    }
    
    public function actionChange_order_status_ajax($order_id){
        // $result['status'] = 'fail';
        // $result['message'] = $order_id.' '.$_POST['status'];
        
        $order = SupplierOrder::model()->findByPk($order_id);
        if ($order->setStatus($_POST['status'], $_POST['etl'])){
            $result['status'] = 'success';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Error saving order data';
        }
        
        echo json_encode($result);
    }
    
    public function actionGet_container_ind_items_ids_ajax($container_id){
        $container = SupplierOrderContainer::model()->findByPk($container_id);
        $sql = "select id from individual_item where supplier_order_container_id = ".$container_id;
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        $result['status'] = 'success';
        $result['ids'] = array();
        foreach ($rows as $row){
            $result['ids'][] = $row['id'];
        }
        
        echo json_encode($result);
    }
	
	public function actionGet_custom_color_list_ajax(array $cid){
		$result['colors'] = array();
		foreach ($cid as $c_id){
			$id = explode('_', $c_id);
			$item_id = $id[0];
			$sold 	 = $id[1] == 'S';
			$criteria = new CDbCriteria;
			$criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED);
			//$criteria->compare('t.status', Individualitem::$STATUS_DENIED, true, "or");
			if ($sold){
				$criteria->addCondition('t.sold_date>"0000-00-00 00:00:00"');
			} else {
				$criteria->addCondition('NOT t.sold_date>"0000-00-00 00:00:00"');
			}
			$criteria->compare('t.item_id', $item_id);
			$criteria->with = 'customer_order_items';
			$criteria->together = true;
			
			$items = Individualitem::model()->findAll($criteria);
			$colors = array();
			foreach ($items as $item){
				if ($item->customer_order_items->custom_color){
					$colors[$item->customer_order_items->custom_color]++;
				}
			}
			
			$colors2 = array();
			foreach ($colors as $color=>$count){
				$colors2[] = $count.'x"'.$color.'"';
			}
			
			$result['colors'][] = implode(', ', $colors2);
			// $result['colors'][] = count($items);
		}
		$result['status'] = 'success';
		echo json_encode($result);
	}
	
	public function actionScan_container_items_in_stock_ajax($container_id){
		$container = SupplierOrderContainer::model()->findByPk($container_id);
		$result['status'] = 'success';
		$result['items'] = $container->scanAllItemsIn();
		echo json_encode($result);
	}
	
	public function actionAdv_paid_doc($order_id, $doc){
		$order = SupplierOrder::model()->findByPk($order_id);
		if (!YumUser::isOperator(true) && Yii::app()->user->id != $order->supplier_id){
			return false;
		}
		
		$docs = explode('||',$order->advanced_paid_attach_file_path);
		$filename = $docs[$doc-1];
		$path = Yii::getPathOfAlias('webroot').'/'.$filename;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		// echo $path;
		// die();
		switch (strtolower($ext)){
			case 'pdf':
				header('Content-Type: application/pdf');
				break;
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
		}
		
		// header('Content-Description: File Transfer');
		// header('Content-Type: application/octet-stream');
		// header('Content-Disposition: attachment; filename='.basename($path));
		
		flush();
		readfile($path);
	}
	
	public function actionFull_paid_doc($order_id, $doc){
		$order = SupplierOrder::model()->findByPk($order_id);
		if (!YumUser::isOperator(true) && Yii::app()->user->id != $order->supplier_id){
			return false;
		}
		
		$docs = explode('||',$order->full_paid_attach_file_path);
		$filename = $docs[$doc-1];
		$path = Yii::getPathOfAlias('webroot').'/'.$filename;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		// echo $path;
		// die();
		switch (strtolower($ext)){
			case 'pdf':
				header('Content-Type: application/pdf');
				break;
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
		}
		
		// header('Content-Description: File Transfer');
		// header('Content-Type: application/octet-stream');
		// header('Content-Disposition: attachment; filename='.basename($path));
		
		flush();
		readfile($path);
	}
	
	public function actionFinal_doc($order_id, $doc){
		$order = SupplierOrder::model()->findByPk($order_id);
		if (!YumUser::isOperator(true) 
			&& Yii::app()->user->id != $order->supplier_id
			&& Yii::app()->user->id != $order->getShipperID()){
			return false;
		}
		
		$criteria = new CDbCriteria;
		$criteria->compare('supplier_order_id', $order_id, true);
		$docs = SupplierOrderOfficialDocs::model()->findAll($criteria);
		
		$filename = $docs[$doc-1]->url;
		$path = Yii::getPathOfAlias('webroot').'/'.$filename;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		switch (strtolower($ext)){
			case 'pdf':
				header('Content-Type: application/pdf');
				break;
			case 'jpg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
		}

		flush();
		readfile($path);
	}
	
	public function actionResend_shipper_requests_ajax(){
		$criteria = new CDbCriteria();
		$criteria->compare('status', SupplierOrderContainer::$STATUS_PENDING);
		$containers = SupplierOrderContainer::model()->findAll($criteria);
		foreach ($containers as $cont){
			AutoMail::sendMailNewSupplierOrderContainerCreated($cont);
		}
		
		$result['status'] = 'success';
		
		echo json_encode($result);
	}
	
	public function actionAdd_new_note_ajax(){
	
		$newSupplierOrderNoteModel = new SupplierOrderNote();
		
		$result['message'] = "fail";
		if(isset($_POST['SupplierOrderNote'])){
			$_POST['SupplierOrderNote']["created_on"] = date("Y-m-d H:i:s", time());
			$_POST['SupplierOrderNote']["modified"] = $_POST['SupplierOrderNote']["created_on"];
			
			$newSupplierOrderNoteModel->attributes = $_POST['SupplierOrderNote'];
			if ($newSupplierOrderNoteModel->save()){
				$result['message'] = "success";
			}
		
		}		
		echo json_encode($result);
	}
	
	public function actionEdit_note_ajax(){
	
		$newSupplierOrderNoteModel = new SupplierOrderNote();
		
		$result['message'] = "fail";
		
		if(isset($_POST['SupplierOrderNote']) &&
		   isset($_POST['SupplierOrderNote']['id']) &&
		   intval($_POST['SupplierOrderNote']['id']) > 0){
			$user_id = Yii::app()->user->id;
			$isAdmin = Yii::app()->user->isAdmin();
			$note_id = intval($_POST['SupplierOrderNote']['id']);
			$note = $newSupplierOrderNoteModel->findByPk($note_id);
			if(!empty($note) && ($isAdmin==1 || $user_id == $note->operator_id)){
				
				$note->content = $_POST['SupplierOrderNote']['content'];
				$note->modified = date("Y-m-d H:i:s", time());
				
				if ($note->save()){
					$result['message'] = "success";
				}
			}
		}		
		echo json_encode($result);
	}
	
	public function actionPrint($order_id){
		$order = SupplierOrder::model()->with('order_items', 'order_items.items')->findByPk($order_id);
		if (!YumUser::isOperator(true) && $order->supplier_id != Yii::app()->user->id){
			return false;
		}
		$rows = array();
		$this->layout = 'print';
		$this->render('print', array(
            'order'=>$order,
        ));
	}
	
	public function actionOrder_details_csv($order_id){
		$order = SupplierOrder::model()->findByPk($order_id);
		if (!YumUser::isOperator(true) && $order->supplier_id != Yii::app()->user->id){
			return false;
		}
		header('Content-type: text/csv');
		header('Content-disposition: attachment;filename=order_'.$order->id.'.csv');
		if (YumUser::isOperator(true)){
			$header = 'Item name,';
		} 
		echo $header."Item code,Item color,Custom color,EXW Price,FOB Price,CBM,Qty,Total EXW Price,Total FOB Price,Total CBM\n";
		foreach ($order->order_items as $items){
			if ($items->status == SupplierOrderItems::$STATUS_ACCEPTED){
				if (YumUser::isOperator(true)){
					$line = $items->items->item_name.',';
				}
				
				$line .= $items->items->item_code.',';
				$line .= $items->items->color.',';
	            $line .= $items->custom_color.',';
	           	$line .= $items->exw_cost_price.',';
	            $line .= $items->fob_cost_price.',';
	            $line .= $items->items->cbm.',';
	            $line .= $items->quantity.',';
	            $line .= (floatval($items->exw_cost_price) * floatval($items->quantity)).',';
	            $line .= (floatval($items->fob_cost_price) * floatval($items->quantity)).',';
	            $line .= $items->getTotalCBM().",\n";

	            echo $line;
			}
		}
	}

    /**
     * Scan items in
     * @param $itemId
     * @throws CHttpException
     */
    public function actionScanItemsIn($itemId)
    {
        $supplierOrderItems = SupplierOrderItems::model()->findByPk($itemId);
        if (empty($supplierOrderItems))
            throw new CHttpException(404, 'Object not found');
        $quantity = Yii::app()->request->getParam('quantity');
        if (!filter_var($quantity, FILTER_VALIDATE_INT, array(
            'options' => array('min' => 1)
        )) || $quantity > $supplierOrderItems->quantity) {
            throw new CHttpException(400, 'Invalid request');
        }
        $movedCount = $supplierOrderItems->moveToInitialOrder($quantity);
        echo CJSON::encode(array(
            'status' => 'success',
            'message' => "Scanned in {$movedCount} items",
            'movedCount' => $movedCount
        ));
    }

    public function actionAddItemNotes($id)
    {
        $item = SupplierOrderItems::model()->findByPk($id);
        if (empty($item))
            throw new CHttpException(404, 'The item is not found');
        if (isset($_POST['SupplierOrderItems'])) {
            $item->attributes = $_POST['SupplierOrderItems'];
            if ($item->save()) {
                echo CJSON::encode(array(
                    'status' => 'success',
                    'message' => 'Notes added',
                    'item_id' => $item->id,
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => 'error',
                    'message' => 'Note cannot be saved'
                ));
            }
            Yii::app()->end();
        }
        $this->renderPartial('itemNotes', array('item' => $item));
    }

    public function actionArchive()
    {
        $supplierOrder = $this->loadModel(Yii::app()->request->getParam('id'));
        $supplierOrder->saveAttributes(array(
            'status' => SupplierOrder::$STATUS_ARCHIVED
        ));
    }
}
