<?php
/**
 * Created by PhpStorm.
 * User: Addicted
 * Date: 2/7/14
 * Time: 2:04 PM
 */

class CustomerNewsletterController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * @return array access rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array(
                    'index',
                ),
                'users' => array('admin')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $model = new CustomerNewsletter('search');
        $model->unsetAttributes();
        $model->attributes = Yii::app()->request->getParam('CustomerNewsletter');
        $this->render('index', array('model' => $model));
    }
} 