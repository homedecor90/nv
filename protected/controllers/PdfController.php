<?php

class PdfController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/pdf';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'customer_order',
					'inventory',
					'container',
					'supplier_order_details',
					'shipping_manifest'
				),
				'users'=>array('*'),
				'expression' => 'UrlPermission::checkUrlPermission',
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionCustomer_order($order_id)
	{
		$customerOrderModel = CustomerOrder::model()->findByPk($order_id);
		
		$settings = Settings::model()->getSettingVariables();
		$tax_percent = number_format($settings->tax_percent, 2);
	
		$fileName = 'customer_invoice_'.$order_id; // without the extension, I add it later. The idea is to get the fileName from the db for instance or generate it dynamically
		$file = Yii::getPathOfAlias('webroot') . '/customer_invoice/' . $fileName . '.pdf';
		
		$html2pdf = Yii::app()->ePdf->HTML2PDF();
		$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot') . '/css/invoice_pdf.css');
		$html2pdf->WriteHTML($this->render('admin', array(
				'customerOrderModel'=>$customerOrderModel,
				'tax_percent'=>$tax_percent,
			), true));
		$html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
		
		
		header('Content-Type: application/pdf');
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		
		flush();
		readfile($file);
		
		/*
		$this->render('admin', array(
			'customerOrderModel'=>$customerOrderModel,
		));*/
	}
    
    public function actionInventory(){
        $criteria = new CDbCriteria();
        $criteria->order = 'item_name';
        $data = Item::model()->findAll($criteria);
        // $this->render('inventory', array('data'=>$data));
        
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($this->render('inventory', array(
				'data'=>$data,
			), true));
            
        $file = Yii::getPathOfAlias('webroot') . '/pdf/inventory.pdf';
		$html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
		
		header('Content-Type: application/pdf');
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		
		flush();
		readfile($file);
    }
    
    public function actionContainer($container_id){
        $container = SupplierOrderContainer::model()->findByPk($container_id);
        
        // $this->render('inventory', array('data'=>$data));
        
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($this->render('container', array(
				'model'=>$container,
			), true));
            
        $file = Yii::getPathOfAlias('webroot') . '/pdf/container_'.$container_id.'.pdf';
		$html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
		
		header('Content-Type: application/pdf');
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		
		flush();
		readfile($file);
    }
    
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SupplierOrder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionSupplier_order_details($order_id){
		$order = SupplierOrder::model()->findByPk($order_id);
		if (!YumUser::isOperator(true) && $order->supplier_id != Yii::app()->user->id){
			return false;
		}
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('t.supplier_order_id',$order->id);
		$criteriaItems->compare('t.status', SupplierOrderItems::$STATUS_ACCEPTED);
		$criteriaItems->with = 'items';
		$criteriaItems->together = true;
		$criteriaItems->order = 'not_loaded_reordered, is_replacement, items.item_name';
		$order_items = SupplierOrderItems::model()->findAll($criteriaItems);
		
		$html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($this->render('supplier_order_details', array(
				'order'=>$order,
				'order_items'=>$order_items,
		), true));
			
		// using time of sending instead of pickup time to avoid duplicating
//		$file = Yii::getPathOfAlias('webroot') . '/pdf/shipping_labels/labels_'.time().'.pdf';
		//~ $html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
		$filename = 'order_details_'.$order->id;
// 		if ($date){
// 			$filename .= '_'.str_replace(array('. ', ', '), '_', $date);
// 		}
		$html2pdf->Output($filename.'.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
	}

	public function actionShipping_manifest($type){
		$criteria = new CDbCriteria();
		if ($type == 'cs'){
			$criteria->compare('t.status', CustomerOrderShipment::$STATUS_SHIPPER_PICKUP);
			if (isset($_GET['id'])){
				$ids = $_GET['id'];
				$criteria->addInCondition('t.id', $ids);
			}
		} elseif($type='customer'){
			$criteria->compare('t.status', CustomerOrderShipment::$STATUS_PICKUP);
		} else {
			return false;
		}
		
		$shipments = CustomerOrderShipment::model()->with('customer_order','customer_order.customer','customer_order.billing_info')->findAll($criteria);
		
		$html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($this->render('shipments_manifest', array(
			'shipments'=>$shipments
		), true));

		$html2pdf->Output('manifest.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
		
	}

}
