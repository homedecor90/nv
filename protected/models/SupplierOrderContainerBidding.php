<?php

/**
 * This is the model class for table "supplier_order_container_bidding".
 *
 * The followings are the available columns in table 'supplier_order_container_bidding':
 * @property string $id
 * @property string $supplier_order_container_id
 * @property string $supplier_shipper_id
 * @property string $bid_date
 * @property string $cutoff_date
 * @property string $bid_eta
 */
class SupplierOrderContainerBidding extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierOrderContainerBidding the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_order_container_bidding';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_order_container_id, supplier_shipper_id, bid_date', 'required'),
			array('supplier_order_container_id, supplier_shipper_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, supplier_order_container_id, supplier_shipper_id, bid_date, cutoff_date, bid_eta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'supplier_order_container_id' => 'Supplier Order Container',
			'supplier_shipper_id' => 'Supplier Shipper',
			'bid_date' => 'Bid Date',
			'cutoff_date' => 'Cut-Off Date',
			'bid_eta' => 'Bid E.T.A',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($container_id = 0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		if ($container_id != 0){
			$this->supplier_order_container_id = $container_id;
		}

		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_order_container_id',$this->supplier_order_container_id,true);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id,true);
		$criteria->compare('bid_date',$this->bid_date,true);
		$criteria->compare('cutoff_date',$this->cutoff_date,true);
		$criteria->compare('bid_eta',$this->bid_eta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getBiddingEXWOptions($bid_id){
		// $arrayFields = array_merge(SupplierOrderContainer::$FOB_FIELD_LIST,
							// SupplierOrderContainer::$COMMON_FIELD_LIST);
		
        $arrayFields = SupplierOrderContainer::getEXWFields();
        
		$criteriaItems = new CDbCriteria;
		$criteriaItems->addInCondition('option_name', $arrayFields);
		$criteriaItems->compare('bidding_id',$bid_id);
		
		$bidding_options = SupplierOrderContainerBiddingOptions::model()->findAll($criteriaItems);		
		
        $total_exw = SupplierOrderContainerBidding::getBiddingOptionsTotalEXWPrice($bid_id);
        $total_fob = SupplierOrderContainerBidding::getBiddingOptionsTotalFOBPrice($bid_id);
        
        $strOptions = '<h3>Step 1: China Charges</h3>';
		$strOptions .= '<table class="item_list">'
			.'<thead>'			
			.'<th class="option_name">Option Name</td>'
			.'<th class="option_price">Option Price</td>'			
			.'</thead>';
			
		foreach ($bidding_options as $bidding_option){
			$strOptions .= "<tr>";
			$strOptions .= "<td class='option_name'>".$bidding_option->option_name."</td>";
			$strOptions .= "<td class='option_price'>".$bidding_option->price."</td>";			
			$strOptions .= "</tr>";
		}
		
		$strOptions .= "<tr>";
		$strOptions .= "<td><b>Total</b></td>";
		$strOptions .= '<td><b>'.($total_exw - $total_fob).'</b></td>';			
		$strOptions .= "</tr>";

		$strOptions .= "</table>";
        // TODO: remove copy-paste
        $arrayFields = SupplierOrderContainer::getCommonFields();
        
		$criteriaItems = new CDbCriteria;
		$criteriaItems->addInCondition('option_name', $arrayFields);
		$criteriaItems->compare('bidding_id',$bid_id);
		
		$bidding_options = SupplierOrderContainerBiddingOptions::model()->findAll($criteriaItems);		
		
        $strOptions .= '<h3>Step 2: USA Charges</h3>';
		$strOptions .= '<table class="item_list">'
			.'<thead>'			
			.'<th class="option_name">Option Name</td>'
			.'<th class="option_price">Option Price</td>'			
			.'</thead>';
			
		foreach ($bidding_options as $bidding_option){
			$strOptions .= "<tr>";
			$strOptions .= "<td class='option_name'>".$bidding_option->option_name."</td>";
			$strOptions .= "<td class='option_price'>".$bidding_option->price."</td>";			
			$strOptions .= "</tr>";
		}
		
		$strOptions .= "<tr>";
		$strOptions .= "<td><b>Total</b></td>";
		$strOptions .= '<td><b>'.$total_fob.'</b></td>';			
		$strOptions .= "</tr>";
        $strOptions .= "<tr>";
		$strOptions .= "<td><b>Total EXW</b></td>";
		$strOptions .= '<td class="exw_total"><b>'.$total_exw.'</b></td>';			
		$strOptions .= "</tr>";
		$strOptions .= "</table>";

        
		return $strOptions;
	}
	
	public static function getBiddingFOBOptions($bid_id){
		$arrayFields = array_merge(SupplierOrderContainer::$FOB_FIELD_LIST,
							SupplierOrderContainer::getCommonFields());
		
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('bidding_id',$bid_id);
		$criteriaItems->addInCondition('option_name', $arrayFields);
		$bidding_options = SupplierOrderContainerBiddingOptions::model()->findAll($criteriaItems);		
		
		$strOptions = '<table class="item_list">'
			.'<thead>'			
			.'<th class="option_name">Option Name</td>'
			.'<th class="option_price">Option Price</td>'			
			.'</thead>';
			
		foreach ($bidding_options as $bidding_option){
			$strOptions .= "<tr>";
			$strOptions .= "<td class='option_name'>".$bidding_option->option_name."</td>";
			$strOptions .= "<td class='option_price'>".$bidding_option->price."</td>";			
			$strOptions .= "</tr>";
		}
		
		$strOptions .= "<tr>";
		$strOptions .= "<td><b>Total FOB</b></td>";
		$strOptions .= '<td class="fob_total"><b>'.SupplierOrderContainerBidding::getBiddingOptionsTotalFOBPrice($bid_id).'</b></td>';			
		$strOptions .= "</tr>";

		$strOptions .= "</table>";

		return $strOptions;
	}
	
	public static function getBiddingOptionsTotalEXWPrice($bid_id){
		$arrayFields = array_merge(SupplierOrderContainer::getEXWFields(),
							SupplierOrderContainer::getCommonFields());
		
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('bidding_id',$bid_id);
		$criteriaItems->addInCondition('option_name', $arrayFields);
		$bidding_options = SupplierOrderContainerBiddingOptions::model()->findAll($criteriaItems);		
		
		$price = 0;
		
		foreach ($bidding_options as $bidding_option){
			$price += $bidding_option->price;
		}

		return $price;
	}
	
	public static function getBiddingOptionsTotalFOBPrice($bid_id){
		$arrayFields = array_merge(SupplierOrderContainer::$FOB_FIELD_LIST,
							SupplierOrderContainer::getCommonFields());
		
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('bidding_id',$bid_id);
		$criteriaItems->addInCondition('option_name', $arrayFields);
		$bidding_options = SupplierOrderContainerBiddingOptions::model()->findAll($criteriaItems);		
		
		$price = 0;
		
		foreach ($bidding_options as $bidding_option){
			$price += $bidding_option->price;
		}

		return $price;
	}
	
	public static function getBiddingOptionsEXWTotalPriceWithShipping($bid_id){
		$supplierOrderContainerBiddingModel = SupplierOrderContainerBidding::model()->findByPk($bid_id);
		$container_id = $supplierOrderContainerBiddingModel->supplier_order_container_id;
		$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($container_id);
		
		$containerTotalEXWPrice = SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($container_id);
		$shipperTotalEXWPrice = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalEXWPrice($bid_id);
		$result = $containerTotalEXWPrice + $shipperTotalEXWPrice;
		
		$lowest_price = SupplierOrderContainerBidding::model()->getLowestPrice($container_id);
		$strAction = $result;
		if ($lowest_price == $result){
			$strAction = '<div class="lowest_price_in_container">'.$strAction.'</div>';
		} else {
			$strAction = '<div>'.$strAction.'</div>';
		}
		
		if ($supplierOrderContainerModel->supplier_shipper_id == 0){
			if (YumUser::model()->isOperator()){
				$strAction .= '<input class="choose_exw" type="button" value="Choose" rel="'.$bid_id.'"/>';		
			}
		}

		$choosen_supplier_shipper_id = $supplierOrderContainerModel->supplier_shipper_id;
		$supplier_shipper_id = $supplierOrderContainerBiddingModel->supplier_shipper_id;
		if ($supplier_shipper_id == $choosen_supplier_shipper_id
			&& $supplierOrderContainerModel->terms == "EXW"){
			$strAction = '<div class="choosen_shipper">'.$strAction.'</div>';
		}
		
		return $strAction;
	}
	
	public static function getBiddingOptionsFOBTotalPriceWithShipping($bid_id){
		$supplierOrderContainerBiddingModel = SupplierOrderContainerBidding::model()->findByPk($bid_id);
		$container_id = $supplierOrderContainerBiddingModel->supplier_order_container_id;
		$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($container_id);
		
		$containerTotalFOBPrice = SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($container_id);
		$shipperTotalFOBPrice = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalFOBPrice($bid_id);
		$result = $containerTotalFOBPrice + $shipperTotalFOBPrice;
		
		$lowest_price = SupplierOrderContainerBidding::model()->getLowestPrice($container_id);
		$strAction = $result;
		if ($lowest_price == $result){
			$strAction = '<div class="lowest_price_in_container">'.$strAction.'</div>';
		} else {
			$strAction = '<div>'.$strAction.'</div>';
		}
		
		if ($supplierOrderContainerModel->supplier_shipper_id == 0){
			if (YumUser::model()->isOperator()){
				$strAction .= '<input class="choose_fob" type="button" value="Choose" rel="'.$bid_id.'"/>';
			}
		}
		
		$choosen_supplier_shipper_id = $supplierOrderContainerModel->supplier_shipper_id;
		$supplier_shipper_id = $supplierOrderContainerBiddingModel->supplier_shipper_id;
		if ($supplier_shipper_id == $choosen_supplier_shipper_id
			&& $supplierOrderContainerModel->terms == "FOB"){
			$strAction = '<div class="choosen_shipper">'.$strAction.'</div>';
		}
		
		return $strAction;
	}
	
	public static function getShipperName($bid_id){
		$supplierOrderContainerBiddingModel = SupplierOrderContainerBidding::model()->findByPk($bid_id);
		$supplier_shipper_id = $supplierOrderContainerBiddingModel->supplier_shipper_id;
		
		if ($supplier_shipper_id == 0){
			return "";
		}
		
		$supplierShipper = YumUser::model()->findByPk($supplier_shipper_id);
		$name = $supplierShipper->profile->firstname." ".$supplierShipper->profile->lastname;
		
		return $name;
	}
    
	public static function getLowestPrice($container_id, $only_quote = false){
		$criteria=new CDbCriteria;
		$criteria->compare('supplier_order_container_id', $container_id, true);
		
		$lowest_price = 0;
		$bid_list = SupplierOrderContainerBidding::model()->findAll($criteria);
		$totalEXWPrice = SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($container_id);
		$totalFOBPrice = SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($container_id);
		
		foreach ($bid_list as $bid){
			$bid_id = $bid->id;
			
			$shipperTotalEXWPrice = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalEXWPrice($bid_id);
            if ($only_quote){
                $resultEXWPrice = $shipperTotalEXWPrice;
            } else {
                $resultEXWPrice = $totalEXWPrice + $shipperTotalEXWPrice;
            }
			
			$shipperTotalFOBPrice = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalFOBPrice($bid_id);
			if ($only_quote){
                $resultFOBPrice = $shipperTotalFOBPrice;
            } else {
                $resultFOBPrice = $totalFOBPrice + $shipperTotalFOBPrice;
            }
			
			if ($lowest_price == 0){
				$lowest_price = $resultEXWPrice;
			}
			
			$lowest_price = min($lowest_price, $resultEXWPrice, $resultFOBPrice);
		}
		
		return $lowest_price;
	}
	
	public static function isAlreadyBid($supplier_order_container_id, $supplier_shipper_id){
		$criteria = new CDbCriteria;
		$criteria->compare('supplier_order_container_id', $supplier_order_container_id, true);
		$criteria->compare('supplier_shipper_id', $supplier_shipper_id, true);
		
		$count = SupplierOrderContainerBidding::model()->count($criteria);
		
		if ($count == 0)
			return false;
		return true;
	}
}