<?php

/**
 * This is the model class for table "cancellation_reason".
 *
 * The followings are the available columns in table 'cancellation_reason':
 * @property string $id
 * @property string $reason
 */
class CancellationReason extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CancellationReason the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cancellation_reason';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reason', 'required'),
			array('reason', 'length', 'max'=>300),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, reason', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reason' => 'Reason',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('reason',$this->reason,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getCancellationReasonCode(){
		Yii::import('application.controllers.CancelreasonController');

		$controller = new CancelreasonController('CancellationReasonFancy');
		$controller->widget('application.extensions.fancybox.EFancyBox', array(
				'target'=>'#cancellation_reason_button',
				'config'=>array(
						'width'=>350,
						'height'=>400,
						'autoDimensions'=>false,
				),
		), true);
		
		$reasons = CancellationReason::model()->findAll();
		$select_reason_form = $controller->renderPartial('/cancelreason/_select_cancellation_reason_form', array('reasons'=>$reasons), true);
		
		$str  = '<input type="button" value="Cancellation Reason" href="#cancellation_reason_div" id="cancellation_reason_button" style="display:none;"/>';
		$str .= '<div class="fancy_box_div_wrapper" style="display:none;">
					<div id="cancellation_reason_div" class="fancy_box_div">';
		$str .= $select_reason_form;
		$str .= '</div></div>';
		
		return $str;
	}
}