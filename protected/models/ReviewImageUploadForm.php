<?php
Yii::import('xupload.models.XUploadForm');
class ReviewImageUploadForm extends XUploadForm
{
        /**
         * Declares the validation rules.
         * The rules state that username and password are required,
         * and password needs to be authenticated.
         */
        public function rules()
        {
                return array(
                    array(
                        'file', 'file', 'minSize' => 10 * 1024, 'maxSize' => 1024 * 2048,
                        'types' => 'gif, jpeg, jpg, png',
                    ),
                );
        }
        /**
         * Declares attribute labels.
         */
        public function attributeLabels()
        {
                return array(
                        'file'=>'Upload files',
                );
        }
        /**
         * A stub to allow overrides of thumbnails returned
         * @since 0.5
         * @author acorncom
         * @param string $publicPath
         * @return string thumbnail name (if blank, thumbnail won't display)
         */
        public function getThumbnailUrl($publicPath) {
            return Yii::app()->easyImage->thumbSrcOf($publicPath.$this->filename, array(
                'resize' => array(
                    'width' => Yii::app()->params['CustomerOrderReview']['thumbWidth'],
                    'height' => Yii::app()->params['CustomerOrderReview']['thumbHeight'],
                )
            ));
        }

}
