<?php

/**
 * This is the model class for table "item".
 *
 * The followings are the available columns in table 'item':
 * @property string $id
 * @property string $item_name
 * @property string $item_main_name
 * @property string $item_code
 * @property string $color
 * @property double $sale_price
 * @property double $exw_cost_price
 * @property double $fob_cost_price
 * @property double $shipping_price
 * @property double $local_pickup
 * @property double $la_oc_shipping
 * @property double $canada_shipping
 * @property double $cbm
 * @property string $dimensions_length
 * @property string $dimensions_width
 * @property string $dimensions_height
 * @property double $weight
 * @property string $description
 * @property string $condition
 * @property string $image
 * @property string $url
 * @property string $image_url
 * @property string $status
 */
class Item extends CActiveRecord
{
    const IMAGE_PATH = '/images/item_images/';
    const NO_PIC_IMAGE = 'no-image.png';
    const NO_PIC_THUMB = 'no-image-thumb.png';
	public $image_file;
	public $item_sold_count;
	public $item_instock_count;
	public $item_incoming_count;
	public $item_pendingscan_count;
    
	private $stats;
	private $to_order;
	
	/**
	 * private var to contain cached quantities for different items
	 * qty in this array shows how many ind. items for this item are available for the shipping (can be sent for bidding or scheduled for customer pickup
	 * @var array
	 */
	private static $available_for_shipping_qty = array();
	
	public static $STATUS_WAITING_APPROVAL 	= "Waiting Approval";
	public static $STATUS_APPROVED			= "Approved";
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Item the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_name, item_code, color, sale_price, exw_cost_price, fob_cost_price, shipping_price, local_pickup, la_oc_shipping, canada_shipping, cbm, condition, box_qty', 'required'),
			array('sale_price, exw_cost_price, fob_cost_price, shipping_price, local_pickup, la_oc_shipping, canada_shipping, cbm, weight', 'numerical'),
			array('item_name, item_code, item_main_name, color, dimensions_length, dimensions_width, dimensions_height, condition, status', 'length', 'max'=>100),
			array('url, image_url', 'length', 'max'=>150),
			array('image', 'length', 'max'=>1000),
			array('image_thumb', 'length', 'max'=>100),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_name, item_code, color, sale_price, exw_cost_price, fob_cost_price, shipping_price, local_pickup, la_oc_shipping, canada_shipping, cbm, dimensions_length, dimensions_width, dimensions_height, weight, description, condition, image, status', 'safe', 'on'=>'search'),
			array('image', 'file', 'types'=>'jpg, jpeg, gif, png', 'allowEmpty'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'individual' => array(self::HAS_MANY, 'Individualitem', 'item_id',
				'order'=>'individual.id ASC'),
			'box' => array(self::HAS_ONE, 'ItemBox', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_name' => 'Item Name',
			'item_main_name' => 'Item Main Name',
			'item_code' => 'Item Code',
			'color' => 'Color',
			'sale_price' => 'Sale Price',
			'exw_cost_price' => 'EXW Cost Price',
			'fob_cost_price' => 'FOB Cost Price',
			'shipping_price' => 'Shipping Price',
			'local_pickup' => 'Local Pickup',
			'la_oc_shipping' => 'LA.OC Shipping',
			'canada_shipping' => 'Canada Shipping',
			'priceWithShipping' => 'Price',
			'cbm' => 'Cbm',
			'dimensions_length' => 'Dimensions Length',
			'dimensions_width' => 'Dimensions Width',
			'dimensions_height' => 'Dimensions Height',
			'weight' => 'Weight',
			'box_qty'=>'Box Qty',
			'description' => 'Description',
			'condition' => 'Condition',
			'image' => 'Image',
			'url' => 'URL',
			'image_url' => 'Image URL',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

        // $no_zeros means that we show only items that have something in stock, sold or incoming
        
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('item_code',$this->item_code,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('sale_price',$this->sale_price);
		$criteria->compare('exw_cost_price',$this->exw_cost_price);
		$criteria->compare('fob_cost_price',$this->fob_cost_price);
		$criteria->compare('shipping_price',$this->shipping_price);
		$criteria->compare('local_pickup',$this->local_pickup);
		$criteria->compare('la_oc_shipping',$this->la_oc_shipping);
		$criteria->compare('canada_shipping',$this->canada_shipping);
		$criteria->compare('cbm',$this->cbm);
		$criteria->compare('dimensions_length',$this->dimensions_length,true);
		$criteria->compare('dimensions_width',$this->dimensions_width,true);
		$criteria->compare('dimensions_height',$this->dimensions_height,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('condition',$this->condition,true);
		$criteria->compare('image',$this->image,true);
		//$criteria->compare('status',$this->status,true);
		$criteria->compare('status','<>'.Item::$STATUS_WAITING_APPROVAL);
        
        $criteria->order = 'item_name';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>40, 
			),
		));
	}
	
    public function searchInventory(){
		$criteria=new CDbCriteria;
        
        if (!$this->id){
            // let's find ids of items that have individual items we need
            $strQuery = "SELECT DISTINCT item_id FROM `individual_item` WHERE status!='".Individualitem::$STATUS_DENIED."'
                        AND status!='".Individualitem::$STATUS_ORDERED_BUT_WAITING."'";
            
            $result = Yii::app()->db->createCommand($strQuery)->queryAll();
            $id_list = array();
            
            foreach ($result as $row){
                $id_list[] = $row["item_id"];
            }
            
            $criteria->addInCondition('id',$id_list,false);
            
        } else {
            $criteria->compare('id',$this->id,false);
        }

		$criteria->compare('status','<>'.Item::$STATUS_WAITING_APPROVAL);
        $criteria->order = 'item_name';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>40, 
			),
		));
    }
    
	public function search_waiting()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		// $criteria->compare('id',$this->id,true);
		// $criteria->compare('item_name',$this->item_name,true);
		// $criteria->compare('item_code',$this->item_code,true);
		// $criteria->compare('color',$this->color,true);
		// $criteria->compare('sale_price',$this->sale_price);
		// $criteria->compare('exw_cost_price',$this->exw_cost_price);
		// $criteria->compare('fob_cost_price',$this->fob_cost_price);
		// $criteria->compare('shipping_price',$this->shipping_price);
		// $criteria->compare('local_pickup',$this->local_pickup);
		// $criteria->compare('la_oc_shipping',$this->la_oc_shipping);
		// $criteria->compare('canada_shipping',$this->canada_shipping);
		// $criteria->compare('cbm',$this->cbm);
		// $criteria->compare('dimensions_length',$this->dimensions_length,true);
		// $criteria->compare('dimensions_width',$this->dimensions_width,true);
		// $criteria->compare('dimensions_height',$this->dimensions_height,true);
		// $criteria->compare('weight',$this->weight);
		// $criteria->compare('description',$this->description,true);
		// $criteria->compare('condition',$this->condition,true);
		// $criteria->compare('image',$this->image,true);
		// $criteria->compare('status',, false);
        $criteria->condition = 'ISNULL(status) OR status=\''.Item::$STATUS_WAITING_APPROVAL.'\'';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>40, 
			),
		));
	}
	
	public static function getAvailableForShippingQty($item_id){
		if (!isset(self::$available_for_shipping_qty[$item_id])){
			$criteria = new CDbCriteria();
			$criteria->compare('shipping_started', 0);
			$criteria->compare('item_id', $item_id);
			$criteria->compare('status', Individualitem::$STATUS_IN_STOCK);
			self::$available_for_shipping_qty[$item_id] = intval(Individualitem::model()->count($criteria));
		}

		return self::$available_for_shipping_qty[$item_id];
	}
	
	public static function getAllItemNameList(){
		$sql = "select distinct item_name from item order by item_name";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$itemNameList = array();
		
		$itemColorList = null;
		foreach ($result as $item){
			$item_name = $item['item_name'];
			$itemNameList[$item_name] = $item_name;
			
			if ($itemColorList == null){
				$itemColorList = Item::model()->getColorList($item_name);
			}
		}
		
		if ($itemColorList == null){
			$itemColorList = array();
		}
				
		$list['itemNameList'] = $itemNameList;
		$list['itemColorList'] = $itemColorList;
		return $list;
	}
	
    // returns id=>name list
    public static function getAllItemIdNameList($only_approved = false, $with_all_items = false){
        $sql = "select id, item_name from item";
        if ($only_approved){
            $sql .= ' where status = \''.Item::$STATUS_APPROVED.'\'';
        }
        
        $sql .= ' order by item_name';
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$itemNameList = array();
		if ($with_all_items){
            $itemNameList[0] = '- All -';
        }
		foreach ($result as $item){
            $itemNameList[$item['id']] = $item['item_name'];
        }
				
		// $list['itemNameList'] = $itemNameList;

		return $itemNameList;
    }
    
    // this function differs with previous one, it shows only items that have some individual items in stock, sold or incoming
    public static function getAllItemIdNameListInventory(){
        $sql = "SELECT DISTINCT individual_item.item_id, item.item_name FROM `individual_item` 
                        LEFT JOIN item on individual_item.item_id = item.id
                        WHERE individual_item.status!='".Individualitem::$STATUS_DENIED."'
                        AND individual_item.status!='".Individualitem::$STATUS_ORDERED_BUT_WAITING."'
                        AND item.status ='".Item::$STATUS_APPROVED."'
                        order by item.item_name";
        // $sql = "select id, item_name from item where status = '".Item::$STATUS_APPROVED."' order by item_name";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$itemNameList = array();
        $itemNameList[0] = '- All -';

		foreach ($result as $item){
            $itemNameList[$item['item_id']] = $item['item_name'];
        }
				
		// $list['itemNameList'] = $itemNameList;

		return $itemNameList;
    }
    
	public static function getColorListByName($item_name){
		$sql = "select distinct color, id from item where item_name = '".$item_name."'";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$itemColorList = array();
		
		foreach ($result as $item){
			$itemColorList[$item['id']] = $item['color'];
		}

		return $itemColorList;
	}
	
	public static function getAllItemCodeList($show_item_name = false){
		$sql = "select distinct item_code, item_name from item where status<>'".Item::$STATUS_WAITING_APPROVAL."'";
		
        if ($show_item_name){
            $sql .= ' order by item_name';
        } else {
            $sql .= ' order by item_code';
        }
        
        $result = Yii::app()->db->createCommand($sql)->queryAll();
		$itemCodeList = array();
		
		$itemColorList = null;
		foreach ($result as $item){
			$item_code = $item['item_code'];
			if ($show_item_name){
                $itemCodeList[$item_code] = $item['item_name'];
            } else {
                $itemCodeList[$item_code] = $item_code;
            }
			
			if ($itemColorList == null){
				$itemColorList = Item::model()->getColorList($item_code);
			}
		}
		
		if ($itemColorList == null){
			$itemColorList = array();
		}
				
		$list['itemCodeList'] = $itemCodeList;
		$list['itemColorList'] = $itemColorList;
		return $list;
	}	
	
	public static function getColorList($item_code){
		$sql = "select distinct color, id from item where item_code = '".$item_code."' and (status<>'".Item::$STATUS_WAITING_APPROVAL."' OR isnull(status))";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$itemColorList = array();

		foreach ($result as $item){
			$itemColorList[$item['id']] = $item['color'];
		}

		return $itemColorList;
	}
	
	public static function getAvailability($order_item_id, $count){
		$suppplierOrderItemsModel = CustomerOrderItems::model()->findByPk($order_item_id);
		
		$arrIndividualitemStatus = array(
			Individualitem::$STATUS_IN_STOCK,
			Individualitem::$STATUS_ORDERED_WITHOUT_ETL,
			Individualitem::$STATUS_ORDERED_WITH_ETL,
			Individualitem::$STATUS_DENIED,
			Individualitem::$STATUS_ORDERED_BUT_WAITING,
		);

		$strAvailability = "<table style='margin:0px'>";
		$strAvailability .= "<thead>"
					."<th>Status</th>"
					."<th>Count</th>"
					."</thead>";
					
		foreach ($arrIndividualitemStatus as $itemStatus){
			$criteriaItemStatus = new CDbCriteria();
			$criteriaItemStatus->compare('item_id', $suppplierOrderItemsModel->item_id);
			$criteriaItemStatus->compare('status', $itemStatus);
			$criteriaItemStatus->compare('customer_order_items_id', 0);
			$count_in_status = Individualitem::model()->count($criteriaItemStatus);
			$strAvailability .= "<tr>"
					."<td>".$itemStatus."</td>"
					."<td>".$count_in_status."</td>"
					."</tr>";
		}
		$strAvailability .= "</table>";
		
		return $strAvailability;
	}
	
	/**
	 * How many items are on the "bidding" step or scheduled for pickup 
	 */
	public function countShippingStarted(){
		$criteria = new CDbCriteria();
		$criteria->compare('item_id', $this->id);
		$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
		$criteria->compare('shipping_started', 1);
		return Individualitem::model()->count($criteria);
	}
	
	/**
	 * Returns array of Individualitem ready to sell. Which means the top of unsold ind. items list according to the status 
	 * IMPORTANT: if there are not enough items in the DB function returns all available at the moment, "not ordered" items are not created
	 * @param int $item_id
	 * @param int $qty
	 */
	public static function getItemsToSell($item_id, $qty){
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_items_id', '>0');
		$criteria->compare('item_id', $item_id);
		$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
		$offset = Individualitem::model()->count($criteria);
		
		$criteria = new CDbCriteria();
		$criteria->compare('item_id', $item_id);
		$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'"
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
		$criteria->order = Individualitem::getStatusOrderStatement();
		$criteria->limit = $qty;
		$criteria->offset = $offset;
		return Individualitem::model()->findAll($criteria);
	}
	
	public static function getStatusString($item_id, $qty){
		$items = self::getItemsToSell($item_id, $qty);
		$str = CustomerOrderItems::getStatusStringForItems($items);
		$count_not_ordered = $qty - count($items);
		if ($count_not_ordered){
			// not enough items in the DB
			$str = ($str == "")?'':$str.'<br/>';
			$str .= '<span style="color:#C00000">'.$count_not_ordered.' - Not Ordered</span>';
		}
		
		return $str;
	}
	/*
	public static function getEtaStringById($item_id, $quantity){
		$count_in_stock     = Item::getCountInStock($item_id);
		$count_pending_scan = Item::getCountPendingScan($item_id);
		$count_with_eta     = Item::getCountOrderedWithEta($item_id);
		$count_pending_eta  = Item::getCountPedningEta($item_id);
		$count_waiting		= Item::getCountWaiting($item_id);
		
		$cur_quantity = $quantity - $count_in_stock;
		$strEtaById = "";
		
		if ($cur_quantity > 0){
			if ($count_in_stock != 0){
				$strEtaById .= '<span style="color:#000000">'.$count_in_stock.' - In Stock</span>';
			}
			
            if ($cur_quantity - $count_pending_scan > 0){
                if ($count_pending_scan != 0){
                    $strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
                    $strEtaById .= '<span style="color:#666">'.$count_pending_scan.' - Pending Scan</span>';
                    $cur_quantity = $cur_quantity - $count_pending_scan;
                }
                
                if ($cur_quantity - $count_with_eta > 0){
                    if ($count_with_eta != 0){
						$strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
						$strEtaById .= Item::model()->getStringCountOrderedWithEta($item_id, $count_with_eta);
						$cur_quantity = $cur_quantity - $count_with_eta;
                    }
					
                    if ($cur_quantity - $count_pending_eta > 0){
                        if ($count_pending_eta != 0){
                            $strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
                            $strEtaById .= '<span style="color:#548AC4">'.$count_pending_eta.' - Pending E.T.A</span>';
							$cur_quantity = $cur_quantity - $count_pending_eta;
                        }
                        
						if ($cur_quantity - $count_waiting > 0){
							if ($count_waiting != 0){
								$strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
								$strEtaById .= '<span style="color:#ba9f07">'.$count_waiting.' - Waiting</span>';
								$cur_quantity = $cur_quantity - $count_waiting;
							}
							
							if ($cur_quantity > 0){
								$strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
								$strEtaById .= '<span style="color:#C00000">'.$cur_quantity.' - Not Ordered</span>';
							}
						} else {
							$strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
							$strEtaById .= '<span style="color:#ba9f07">'.$cur_quantity.' - Waiting</span>';
						}						
                    } else {
                        if ($cur_quantity > 0){
                            $strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
                            $strEtaById .= '<span style="color:#548AC4">'.$cur_quantity.' - Pending E.T.A</span>';
                        }
                    }
                } else {
                    $strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
                    $strEtaById .= Item::model()->getStringCountOrderedWithEta($item_id, $cur_quantity);
                }
            
            } else {
                $strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
                $strEtaById .= '<span style="color:#666">'.$cur_quantity.' - Pending Scan</span>';
            }
            
		} else {
			if ($quantity > 0){
				$strEtaById = ($strEtaById=="")?"":$strEtaById."<br/>";
				$strEtaById .= '<span style="color:#000000">'.$quantity.' - In Stock</span>';
			}
		}
		
		return $strEtaById;
	}
	*/
	public static function getCountInStock($item_id){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_IN_STOCK);
        $criteriaItemStatus->compare('customer_order_items_id', 0);
		$count_in_stock = Individualitem::model()->count($criteriaItemStatus);
		
		return $count_in_stock;
	}
	
	public static function getCountInStockAll($item_id){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_IN_STOCK);
        // $criteriaItemStatus->compare('customer_order_items_id', 0);
		$count_in_stock = Individualitem::model()->count($criteriaItemStatus);
		
		return $count_in_stock;
	}
	
	public static function getCountPendingScan($item_id){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_PENDING_SCAN);
        $criteriaItemStatus->compare('customer_order_items_id', 0);
		//$criteriaItemStatus->addCondition('customer_order_items_id <> 0');
		$count_pending_scan = Individualitem::model()->count($criteriaItemStatus);
		
		// if ($count_pending_scan > 0){
			//$strResult = '<a href="#" class="pending_scan_count" rel="'.$item_id.'">'.$count_pending_scan.'</a>';
			// $strResult = $count_pending_scan;
		// } else {
			$strResult = $count_pending_scan;
		// }
		
		return $strResult;
	}
	
	public static function getCountPendingScanString($item_id){
		$connection=Yii::app()->db;
		$commandPendingScan = $connection->createCommand();		
		
		$commandPendingScan
			->select(array(
				'c.id as customer_id',
				'COUNT(*) as count_by_customers',
				'CONCAT(c.first_name, " ", c.last_name) as customer_name'))
			
			->join('customer_order_items coi', 'ii.customer_order_items_id = coi.id')
			->join('customer_order co', 'coi.customer_order_id = co.id')
			->join('customer c', 'c.id = co.customer_id')

			->where('ii.status=:status and ii.item_id=:item_id',
				array(
					':status'=>Individualitem::$STATUS_PENDING_SCAN,
					':item_id'=>$item_id,
				)
			)
			
			->group('c.id')
			
			->from('individual_item ii');
		
		
		$customer_list = $commandPendingScan->queryAll();
		
		$strResult = '<table id="pending_scan_item"><thead>'
				.'<th class="customer_name">Customer Name</th>'
				.'<th class="quantity">Qty</th>'
				.'<th class="last_comment">Last Comment</th>'
		;
		
		$strItem = "";
		$customer_count = 0;
		foreach ($customer_list as $customer){
			if ($customer['count_by_customers'] > 0){
				$customer_id = $customer['customer_id'];
				$item_count = $customer['count_by_customers'];
				$customer_name = $customer['customer_name'];
				
				$criteriaNote = new CDbCriteria();
				$criteriaNote->compare('customer_id', $customer_id);
				$criteriaNote->order = 'created_on desc';
				
				$customerNoteModel = CustomerNote::model()->find($criteriaNote);
				
				$last_comment = "";
				if ($customerNoteModel != NULL){
					$last_comment = $customerNoteModel->content;
				}
				
				$strItem .= '<tr>'
					.'<td class="customer_name">'.$customer_name.'</td>'
					.'<td class="quantity">'.$item_count.'</td>'
					.'<td class="last_comment">'.$last_comment.'</td></tr>';
				$customer_count++;
			}
		}
		
		if ($customer_count == 0){
			$strItem = '<tr><td colspan="3">No Customers</td></tr>';
		}
		
		$strResult .= '<tbody>'.$strItem.'</tbody>';
		
		return $strResult;
	}

	public static function getCountIncoming($item_id, $sold = NULL){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_WITH_ETA, false);
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_WITH_ETL, false, 'or');
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_WITHOUT_ETL, false, 'or');
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_BUT_WAITING, false, 'or');
		$criteriaItemStatus->compare('item_id', $item_id);
		if ($sold === true){
            $criteriaItemStatus->compare('customer_order_items_id', '<>0');
        } elseif ($sold === false){
            $criteriaItemStatus->compare('customer_order_items_id', 0);
        }
		// $criteriaItemStatus->compare('customer_order_items_id', 0);
		$count_incoming = Individualitem::model()->count($criteriaItemStatus);
		
		return $count_incoming;
	}
	
    public static function getCountNotOrdered($item_id){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_NOT_ORDERED);
		// $criteriaItemStatus->compare('customer_order_items_id', 0);
		$count_not_ordered = Individualitem::model()->count($criteriaItemStatus);
		
		return $count_not_ordered;
	}
    
	public static function getCountSold($item_id){
		$criteriaItemStatus = new CDbCriteria();
		// $criteriaItemStatus->compare('item_id', $item_id);
		// $criteriaItemStatus->compare('status', Individualitem::$STATUS_SOLD);
		$criteriaItemStatus->condition = 'item_id = '.$item_id.' AND NOT ISNULL(sold_date)';
		// $criteriaItemStatus->compare('customer_order_items_id', 0);
		$count = Individualitem::model()->count($criteriaItemStatus);
		
		return $count;
	}
	
    // need to duplicate functions with non-static versions
    public function countItemSold(){
        if (!$this->item_sold_count){
            $this->item_sold_count = Item::getCountSold($this->id);
        }
        
        return $this->item_sold_count;
    }
    
    public function countItemInStock(){
        if (!$this->item_instock_count){
            $this->item_instock_count = $this->countItemsInventory(Individualitem::$STATUS_IN_STOCK);
        }
        
        return $this->item_instock_count;
    }
    
    public function countItemIncoming(){
        if (!$this->item_incoming_count){
            $this->item_incoming_count = Item::getCountIncoming($this->id);
        }
        
        return $this->item_incoming_count;
    }
    
    public function countPendingScan(){
        if (!$this->item_pendingscan_count){
            $this->item_pendingscan_count = $this->countItemsInventory(Individualitem::$STATUS_PENDING_SCAN);
        }
        
        return $this->item_pendingscan_count;
    }
    
    public function finalCount(){
        return $this->countItemsInventory(Individualitem::$STATUS_IN_STOCK, false) 
                + $this->countItemsInventory(Individualitem::$STATUS_PENDING_SCAN, false)
                + $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL, false)
                + $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITHOUT_ETL, false) // + $this->countPendingScan() + $this->countItemIncoming() - $this->countItemSold();
                + $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETA, false);
    }
    
    /**
     * 
     * @param string $status
     * @param bool $sold if NULL passed - total is returned, if true - only sold, if false - only not sold
     */
    public function countItemsInventory($status, $sold = null){
		$this->fetchStats();

		$status = str_replace(' ', '_', strtolower($status));
		if (!isset($this->stats[$status])){
			return 0;
		}
		if ($sold === true){
			return $this->stats[$status]['sold'];
		}
		if ($sold === false){
			return $this->stats[$status]['total'] - $this->stats[$status]['sold'];
		}
		if ($sold === null){
			return $this->stats[$status]['total'];
		}
    }
    
    /**
     * 
     * @param string $status short status as sent in AJAX-request, not full 
     */
    public function getSoldItemsByShortStatus($status){
    	// statuses ordered by priority
    	$statuses = array(
    			Individualitem::$STATUS_IN_STOCK,
    			Individualitem::$STATUS_ORDERED_WITH_ETA,
    			Individualitem::$STATUS_ORDERED_WITH_ETL,
    			Individualitem::$STATUS_ORDERED_WITHOUT_ETL,
    			Individualitem::$STATUS_ORDERED_BUT_WAITING,
    			//Individualitem::$STATUS_DENIED,
    			Individualitem::$STATUS_NOT_ORDERED
    	);
    	 
    	$criteria = new CDbCriteria();
    	
    	switch ($status){
    		case 'in_stock':            $status = Individualitem::$STATUS_IN_STOCK;
    		break;
    		case 'sold_not_ordered':    $status = Individualitem::$STATUS_NOT_ORDERED;
    		break;
    		case 'without_eta':         $status = Individualitem::$STATUS_ORDERED_WITH_ETL;
    		//$criteria->compare('t.status', Individualitem::$STATUS_ORDERED_WITH_ETL, false, 'or');
    		break;
    		case 'with_eta':            $status = Individualitem::$STATUS_ORDERED_WITH_ETA;
    		break;
    		case 'waiting':            	$status = Individualitem::$STATUS_ORDERED_BUT_WAITING;
    		break;
    	}
    	 
    	$limit = $this->countItemsInventory($status, true);
    	
    	if ($status == Individualitem::$STATUS_ORDERED_WITHOUT_ETL){
    		$limit += $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL, true);
    	}
    	$i = 0;
    	$offset = 0;
    	while($statuses[$i] != $status){
    		$offset += $this->countItemsInventory($statuses[$i], true);
    		$i++;
    	}
    	
    	$criteria = new CDbCriteria();
    	$criteria->compare('t.item_id', $this->id);
    	$criteria->compare('t.customer_order_items_id', '>0');
    	$criteria->addCondition('t.status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'"
    			AND t.status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
    	$criteria->order = 't.shipping_started DESC, t.sold_date';
    	$criteria->limit = $limit;
    	$criteria->offset = $offset;
    	$criteria->with = array('customer_order_items', 'customer_order_items.order', 'customer_order_items.order.customer');
    	$criteria->together = true;
    	 
    	return Individualitem::model()->findAll($criteria);
    }
    
    /**
     * Tell whether we have anough "free" in stock items and can scan out random individual item 
     */
    public function canScanOutIndividual(){
    	return $this->countItemsInventory(Individualitem::$STATUS_IN_STOCK) > $this->countShippingStarted();
    }
    
	// TODO: finally make it as method with value caching
    public function toOrder($number_only = false){
        if ($number_only && $this->to_order){
        	return $this->to_order;
        }
    	// last 30 days sales
        
    	$days_interval = 30;
        $lead_time = 45;
        $possible_delay = 15;
        $criteria = new CDbCriteria();
        $criteria->condition = 'item_id = '.$this->id.' AND sold_date >= date_sub(NOW(), INTERVAL '.$days_interval.' DAY)';
        $last_period_sales = Individualitem::model()->count($criteria);
        
        // check prior sales info
        $strQuery = 'SELECT SUM(qty) as `sum` FROM item_prior_sale WHERE item_id='.$this->id.' AND date >= date_sub(NOW(), INTERVAL '.$days_interval.' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $last_period_sales += $result[0]['sum'];
        
        $strQuery = 'SELECT SUM(quantity) as `sum` FROM customer_order_items WHERE item_id='.$this->id.' AND lost_sale_date >= date_sub(NOW(), INTERVAL '.$days_interval.' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $last_period_sales += $result[0]['sum'];
        
        $count_per_day = $last_period_sales/$days_interval;
        
        $criteria->condition = 'item_id = '.$this->id.' AND sold_date < date_sub(NOW(), INTERVAL '.($days_interval).' DAY) AND sold_date >= date_sub(NOW(), INTERVAL '.($days_interval*2).' DAY)';
        $prev_period1_sales = Individualitem::model()->count($criteria);
        
        
        // check prior sales info
        $strQuery = 'SELECT SUM(qty) as `sum` FROM item_prior_sale WHERE item_id='.$this->id.' AND date < date_sub(NOW(), INTERVAL '.($days_interval).' DAY) AND date >= date_sub(NOW(), INTERVAL '.($days_interval*2).' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $prev_period1_sales += $result[0]['sum'];
        
        $strQuery = 'SELECT SUM(quantity) as `sum` FROM customer_order_items WHERE item_id='.$this->id.' AND lost_sale_date < date_sub(NOW(), INTERVAL '.($days_interval).' DAY) AND lost_sale_date >= date_sub(NOW(), INTERVAL '.($days_interval*2).' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $prev_period1_sales += $result[0]['sum'];
        
        $criteria->condition = 'item_id = '.$this->id.' AND sold_date < date_sub(NOW(), INTERVAL '.($days_interval*2).' DAY) AND sold_date >= date_sub(NOW(), INTERVAL '.($days_interval*3).' DAY)';
        $prev_period2_sales = Individualitem::model()->count($criteria);
        
        // check prior sales info
        $strQuery = 'SELECT SUM(qty) as `sum` FROM item_prior_sale WHERE item_id='.$this->id.' AND date < date_sub(NOW(), INTERVAL '.($days_interval*2).' DAY) AND date >= date_sub(NOW(), INTERVAL '.($days_interval*3).' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $prev_period2_sales += $result[0]['sum'];
        
        $strQuery = 'SELECT SUM(quantity) as `sum` FROM customer_order_items WHERE item_id='.$this->id.' AND lost_sale_date < date_sub(NOW(), INTERVAL '.($days_interval*2).' DAY) AND lost_sale_date >= date_sub(NOW(), INTERVAL '.($days_interval*3).' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $prev_period2_sales += $result[0]['sum'];
        
        $criteria->condition = 'item_id = '.$this->id.' AND sold_date < date_sub(NOW(), INTERVAL '.($days_interval*3).' DAY) AND sold_date >= date_sub(NOW(), INTERVAL '.($days_interval*4).' DAY)';
        $prev_period3_sales = Individualitem::model()->count($criteria);
        
        // check prior sales info
        $strQuery = 'SELECT SUM(qty) as `sum` FROM item_prior_sale WHERE item_id='.$this->id.' AND date < date_sub(NOW(), INTERVAL '.($days_interval*3).' DAY) AND date >= date_sub(NOW(), INTERVAL '.($days_interval*4).' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $prev_period3_sales += $result[0]['sum'];
        
        $strQuery = 'SELECT SUM(quantity) as `sum` FROM customer_order_items WHERE item_id='.$this->id.' AND lost_sale_date < date_sub(NOW(), INTERVAL '.($days_interval*3).' DAY) AND lost_sale_date >= date_sub(NOW(), INTERVAL '.($days_interval*4).' DAY)';
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        $prev_period3_sales += $result[0]['sum'];
        
        if ($last_period_sales - $prev_period1_sales == 0)
        {
            $growth_rate_1 = 0;
        } else {
            $growth_rate_1 = $prev_period1_sales ? ($last_period_sales - $prev_period1_sales) / $prev_period1_sales : 1;
        }
        if ($prev_period1_sales - $prev_period2_sales == 0)
        {
            $growth_rate_2 = 0;
        } else {
            $growth_rate_2 = $prev_period2_sales ? ($prev_period1_sales - $prev_period2_sales) / $prev_period2_sales : 1;
        }
        if ($prev_period2_sales - $prev_period3_sales == 0)
        {
            $growth_rate_3 = 0;
        } else {
            $growth_rate_3 = $prev_period3_sales ? ($prev_period2_sales - $prev_period3_sales) / $prev_period3_sales : 1;
        }
        
        $avg_rate = 0.5*$growth_rate_1 + 0.3*$growth_rate_2 + 0.2*$growth_rate_3;
        
        $needed_for_period = $lead_time * $count_per_day;
        $count_in_stock = $this->countItemsInventory(Individualitem::$STATUS_IN_STOCK, false);
        $count_incoming = $this->countItemsInventory(Individualitem::$STATUS_ORDERED_BUT_WAITING, false)
        					+ $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITHOUT_ETL, false)
        					+ $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL, false)
        					+ $this->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETA, false);
		// $count_in_stock = Item::getCountInStockAll($item_id);
        // $count_incoming = Item::getCountIncoming($item_id);
		// return $count_in_stock + $count_incoming;
        $safety_stock = $count_per_day * $possible_delay;
        
        $to_order = $needed_for_period - $count_in_stock - $count_incoming + $safety_stock;
        $this->to_order = $to_order;
        //$to_order = $needed_for_period.' - '.$count_in_stock.' - '.$count_incoming.' + '.$safety_stock.' = '.$to_order;
        //return $to_order;
        // return $growth_rate_1.' '.$growth_rate_2.' '.$growth_rate_3.'<br />'.$last_period_sales.' '.$prev_period1_sales.' '.$prev_period2_sales.' '.$prev_period3_sales;
        // return max($to_order, 0).'<br />('.round(max($to_order, 0) * (1 + $avg_rate), 2).')';
        if ($number_only){
        	return $to_order;
        } else {
        	return $to_order.'<br />('.round($to_order * (1 + $avg_rate), 2).')';
        }
    }
    
	public static function getStringCountOrderedWithEta($item_id, $qunatity){
		$criteriaItemStatus = new CDbCriteria();
		
		$criteriaItemStatus->select = array('eta', 'count(supplier_order_container_id) as count_eta_grou_by_container_id');
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_WITH_ETA);
		$criteriaItemStatus->compare('customer_order_items_id', 0);
		$criteriaItemStatus->group = 'supplier_order_container_id';
		
		$containerList = Individualitem::model()->findAll($criteriaItemStatus);
		
		$strResult = "";
		foreach ($containerList as $container){
			$eta = $container->eta;
			$count_eta = $container->count_eta_grou_by_container_id;
			
			$strResult = ($strResult=="")?"":$strResult."<br/>";
			if ($qunatity > $count_eta){
				$strResult .= '<span style="color:#00A050">'.$count_eta.' Arriving on '.date("M. d, Y", strtotime($eta)).'</span>';
				$qunatity = $qunatity - $count_eta;
			} else {
				$strResult .= '<span style="color:#00A050">'.$qunatity.' Arriving on '.date("M. d, Y", strtotime($eta)).'</span>';
				break;
			}
		}
		
		$container_count = count($containerList);
		return $strResult ;
	}
	
	public static function getCountPedningEta($item_id){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_WITHOUT_ETL, true, "or");
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_WITH_ETL, true, "or");
		//$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_BUT_WAITING, true, "or");
		
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('customer_order_items_id', 0);
		$count_pending_eta = Individualitem::model()->count($criteriaItemStatus);
		
		return $count_pending_eta;
	}
	
	public static function getCountWaiting($item_id){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_ORDERED_BUT_WAITING);
		
		$criteriaItemStatus->compare('item_id', $item_id);
		$criteriaItemStatus->compare('customer_order_items_id', 0);
		$count_waiting = Individualitem::model()->count($criteriaItemStatus);
		
		return $count_waiting;
	}
	
	public static function getWaitingItemCount(){
		$criteria=new CDbCriteria;
		$criteria->compare('status',Item::$STATUS_WAITING_APPROVAL);
		$count = Item::model()->count($criteria);
		return $count;
	}
    
    public function filledInfo(){
        // TODO: why does it return int instead of bool?
    	return intval($this->cbm != 0
                    && $this->exw_cost_price != 0 
                    && $this->exw_cost_price != 0
        			&& $this->box);
    }
	
	public function boxDimsFromRequest($box_info){
		if ($this->box){
			$box = $this->box;
		} else {
			$box = new ItemBox();
			$box->item_id = $this->id;
		}
		$box->attributes = $box_info;
		$box->save();
	}
	
	public function getBoxDimsStr(){
		$boxes = array();
		for ($i=1; $i<=$this->box_qty; $i++){
			$width = 'box_'.$i.'_width';
			$height = 'box_'.$i.'_height';
			$length = 'box_'.$i.'_length';
			$weight = 'box_'.$i.'_weight';
			$boxes[] = $this->box->$width.' x '.$this->box->$height.' x '.$this->box->$length.' - '.$this->box->$weight.' lbs';
		}
		return implode('<br />', $boxes);
	}
	
	public function getSingleBoxDimsStr($box_i){
		$width = 'box_'.$box_i.'_width';
		$height = 'box_'.$box_i.'_height';
		$length = 'box_'.$box_i.'_length';
		$weight = 'box_'.$box_i.'_weight';
		return $this->box->$width.' x '.$this->box->$height.' x '.$this->box->$length.' - '.$this->box->$weight.' lbs';
	}
	
	/**
	 * if $not_ordered == true - render table for not ordered items, if false - for "to order" count 
	 * @param bool $not_ordered
	 * @return Ambigous <string, mixed>
	 */
	public static function getToOrderTable($not_ordered = false){
		$controller = new ItemController('');
		
		//$suppliers = array_merge(array(0=>'- Do Not Order -'), YumUser::model()->getSupplierList());
		$suppliers = YumUser::model()->getSupplierList();
		$suppliers[0] = '- Do Not Order -';
		ksort($suppliers);
		
		return $controller->renderPartial('item/_to_order_table', array(
			'items'=>Item::model()->findAll(),
			'supplierList'=>$suppliers,
			'not_ordered'=>$not_ordered,
		), true);
	}
	
	/**
	 * selects and saves stats info to the private field  
	 */
	private function fetchStats(){
		if (!$this->stats){
			$criteria = new CDbCriteria();
			$criteria->compare('customer_order_items_id', '>0');
			$criteria->compare('item_id', $this->id);
			$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
			
			$total_sold = Individualitem::model()->count($criteria);

// 			$strQuery = 'SELECT id FROM customer_order WHERE customer_id='.$customerModel->id.' AND status=\''.CustomerOrder::$STATUS_SOLD.'\'';
// 			$result = Yii::app()->db->createCommand($strQuery)->queryAll();
			$criteria = new CDbCriteria();
			$criteria->compare('item_id', $this->id);
			$criteria->select = 'COUNT(*) as num, status';
			$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
			$criteria->order = Individualitem::getStatusOrderStatement();
			$criteria->group = 'status';
			$rows = Individualitem::model()->findAll($criteria);
			$stats = array();
			foreach ($rows as $row){
				$status = str_replace(' ', '_', strtolower($row->status));
				$stats[$status]['total'] = $row->num;
				$stats[$status]['sold'] = min($row->num, $total_sold);
				$total_sold = max($total_sold - $row->num, 0);
			}
			return $this->stats = $stats;
		}
	}

    /**
     * @param $item_id
     * @param $manual_auto
     * @return array array of Item objects that were bought with this one
     */
    public static function getBoughtTogether($item_id, $manual_auto = ''){
        $criteria = new CDbCriteria();
        $criteria->addCondition('item1_id='.$item_id.' OR item2_id='.$item_id);
        if ($manual_auto == 'manual'){
            $criteria->addCondition('qty=0');
        } elseif($manual_auto == 'auto'){
            $criteria->addCondition('qty>0');
        }
        $criteria->order = 'qty DESC';
        $rows = ItemBoughtTogether::model()->findAll($criteria);
        $ids = array();
        foreach ($rows as $row){
            if ($row->item1_id == $item_id){
                $ids[] = $row->item2_id;
            } else {
                $ids[] = $row->item1_id;
            }
        }
// 		print_r($ids);
// 		die();
        if (count($ids)){
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $ids);
// 			$criteria->group = 'item_main_name';
            //$criteria->order = 'FIELD(id,'.implode(',', $ids).')';
            $criteria->order = 'item_name';
            return Item::model()->findAll($criteria);
        } else {
            return array();
        }
    }

    public function getBoxCBM($box_i){
		$width = 'box_'.$box_i.'_width';
		$height = 'box_'.$box_i.'_height';
		$length = 'box_'.$box_i.'_length';
		return round($this->box->$width * $this->box->$height * $this->box->$length * 0.000016387064, 4);
	}

    /**
     * Image is downloaded from by image_url field and stored as id.jpg in images/item_images
     */
    public function downloadImageByUrl(){
        if ($this->image_url){
            $headers = get_headers($this->image_url, 1);
            $type = $headers["Content-Type"];
            switch ($type){
                case 'image/jpeg':   $ext = 'jpg';
                                    break;
                case 'image/gif':   $ext = 'gif';
                                    break;
                case 'image/png':   $ext = 'png';
                                    break;
                default: return '';
            }

            $path = Yii::getPathOfAlias('webroot').'/images/item_images/'.$this->id.'.'.$ext;
            if(file_put_contents($path, file_get_contents($this->image_url))){
                $img = new Imagick($path);
                $img->thumbnailimage(150, 150, true);
                $img->writeimage($path);
            }
        }

        return '';
    }

    public function getStoredImageUrl(){
        if (file_exists(Yii::getPathOfAlias('webroot').'/images/item_images/'.$this->id.'.jpg')){
            return Yii::app()->getBaseUrl(true).'/images/item_images/'.$this->id.'.jpg';
        }

        if (file_exists(Yii::getPathOfAlias('webroot').'/images/item_images/'.$this->id.'.gif')){
            return Yii::app()->getBaseUrl(true).'/images/item_images/'.$this->id.'.gif';
        }

        if (file_exists(Yii::getPathOfAlias('webroot').'/images/item_images/'.$this->id.'.png')){
            return Yii::app()->getBaseUrl(true).'/images/item_images/'.$this->id.'.png';
        }

        return '';
    }

    public function getThumbnailUrl() {
        if (isset($this->image_thumb)) {
            return self::IMAGE_PATH.$this->image_thumb;
        } else {
            return self::IMAGE_PATH.self::NO_PIC_THUMB;
        }
    }
    /**
     *  @return float
     */
    public function getPriceWithShipping()
    {
        return $this->sale_price + $this->shipping_price;
    }

    public function getShippingCost($shippingMethod)
    {
        switch ($shippingMethod) {
            case CustomerOrder::SHIPPING_METHOD_SHIPPING:
                return $this->shipping_price;
            case CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP:
                return $this->local_pickup;
            case CustomerOrder::SHIPPING_METHOD_SHIPPED_FROM_LA:
                return $this->la_oc_shipping;
            case CustomerOrder::SHIPPING_METHOD_CANADA_SHIPPING:
                return $this->canada_shipping;
        }
    }

    /**
     *  Shipping cost for product from Chinese port to US port
     */

}
