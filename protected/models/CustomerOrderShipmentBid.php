<?php

/**
 * This is the model class for table "customer_order_shipment_bid".
 *
 * The followings are the available columns in table 'customer_order_shipment_bid':
 * @property string $id
 * @property string $shipment_id
 * @property string $customer_shipper_id
 * @property integer $auto_generated
 * @property string $delivery_service
 * @property double $rate_quote
 * @property string $rate_quote_number
 * @property string $status
 * @property string $date
 * @property integer $selected
 * @property string $selection_date
 */
class CustomerOrderShipmentBid extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerOrder the static model class
	 */
	
	public static $STATUS_PENDING      	= "Pending";
    public static $STATUS_SELECTED   	= "Selected";
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_shipment_bid';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shipment_id, rate_quote, status, date', 'required'),
			// array('status', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('shipment_id, customer_shipper_id, delivery_service, rate_quote, rate_quote_number, status, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            // 'ind_items'=>array(self::HAS_MANY, 'Individualitem', 'customer_order_shipment_id', 'order'=>'ind_items.item_id'),
            // 'ind_items'=>array(self::HAS_MANY, 'Individualitem', 'customer_order_shipment_id', 'order'=>'ind_items.item_id'),
            // 'customer_order'=>array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
			'shipment'=>array(self::BELONGS_TO, 'CustomerOrderShipment', 'shipment_id'),
			'shipper'=>array(self::BELONGS_TO, 'YumUser', 'customer_shipper_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shipment_id' => 'Shipment ID',
			'customer_shipper_id' => 'Customer Shipper ID',
			'rate_quote' => 'Rate Quote',
			'rate_quote_number' => 'Rate Quote Number',
			'date' => 'Date',
			'delivery_service' => 'Delivary Service',
			'auto_generated' => 'Auto Generated',
			'status' => 'Status',
		);
	}
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 /*
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('customer_order_id',$this->customer_order_id,false);
		$criteria->compare('status',$this->status,false);
		$criteria->compare('pickup_date',$this->pickup_date,false);
        
        $criteria->order = 'id ASC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	*/
	
	public function getShipperName(){
		if ($this->auto_generated){
			return $this->delivery_service;
		} else {
			return $this->shipper->profile->firstname.' '.$this->shipper->profile->lastname;
		}
	}
	
	public function selectBid(){
		$this->selected = 1;
		$this->selection_date = date('Y-m-d H:i:s');
		return $this->save();
	}
	
	public function needsAdditionalPayment(){
		if ($this->shipment->customer_order->is_replacement){
			return false;
		}
		return $this->rate_quote > round($this->shipment->total_shipping_price * CustomerOrderShipment::QUOTE_LIMIT, 2);
	}
	
	public function getCompany(){
		if ($this->auto_generated){
			return $this->delivery_service;
		} else {
			return $this->shipper->profile->company;
		}
	}
	
	// cancel shippers bid and send notification
	public function cancel(){
		if (!$this->auto_generated){
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			
			$name = $this->shipper->profile->firstname.' '.$this->shipper->profile->lastname;
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($this->shipper->profile->email, $name);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Subject = AutoMailMessage::MS_SHIPPER_BID_CANCELLED;
			$body = str_replace('[name]', $name, AutoMailMessage::MC_SHIPPER_BID_CANCELLED);
			$body = str_replace('[quote]', '$'.number_format($this->rate_quote, 2), $body);
			$body = str_replace('[quote_number]', $this->rate_quote_number, $body);
			$mail->Body = $body;
			$mail->Send();
		}
		
		return $this->delete();
	}
}
