<?php
/**
 * This is the model class for table "customer_order_shipment".
 *
 * The followings are the available columns in table 'customer_order_shipment':
 * @property string $id
 * @property string $customer_order_id
 * @property string $status
 * @property string $pickup_date
 * @property string $shipper_id
 * @property string $pickup_code
 * @property double $total_shipping_price
 * @property integer $additional_payment_required
 * @property double $additional_payment_amount
 * @property string $additional_payment_notification_date
 * @property integer $additional_payment_done
 * @property string $additional_payment_date
 * @property string $additional_payment_code
 * @property string $additional_payment_bid
 * @property integer $additional_payment_disabled
 * @property string $bol_path
 * @property string $bol_upload_date
 * @property string $tracking_number
 * @property string $tracking_website
 * @property string $tracking_number_date
 * @property integer $notification_sent
 * @property integer $shipping_services_bids_generated
 * @property integer $claim
 * @property integer $claim_resolved
 * @property string $claim_date
 * @property string $claim_resolved_date
 * @property double $claim_amount
 * @property integer $cancellation_reason
 * @property integer $paid
 * @property string $paid_date
 * @property string $paid_checknumber
 * @property string $fedex_shipment_id
 * @property integer $combo
 * @property integer $attached_to
 * @property integer $return
 * @property string $return_date
 * @property string $modified_on
 * @property string $shipping_company
 * @property string $phone_number
 */
class CustomerOrderShipment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerOrder the static model class
	 */
	
	public static $STATUS_PENDING      		= "Pending";
	public static $STATUS_BIDDING   		= "Bidding";
	public static $STATUS_PICKUP   			= "Pickup";
	public static $STATUS_SHIPPER_PICKUP	= "Shipper Pickup";
	public static $STATUS_SCANNED_OUT		= "Scanned OUT";
	public static $STATUS_COMPLETED			= "Completed";
	public static $STATUS_CANCELLED			= "Cancelled";
	// used when current shipment's items were attached to a different shipment
	public static $STATUS_ATTACHED			= "Attached";
	
	public $_filter_customer_name = '';
	public $_total_unpaid = 0;
	public $_total_claims = 0;
	public $_total = null;
	
	private $boxes = null;
	
    const QUOTE_LIMIT = 0.95;
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_shipment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_order_id, status', 'required'),
			array('status', 'length', 'max'=>20),
			array('tracking_number', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_order_id, status, pickup_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            // 'ind_items'=>array(self::HAS_MANY, 'Individualitem', 'customer_order_shipment_id', 'order'=>'ind_items.item_id'),
            'ind_items'=>array(self::HAS_MANY, 'Individualitem', 'customer_order_shipment_id', 'order'=>'ind_items.item_id', 'with'=>'customer_order_items', 'together'=>true),
			'returns'=>array(self::HAS_MANY, 'IndividualItemReturn', 'shipment_id'),
            'active_ind_items'=>array(
            		self::HAS_MANY, 
            		'Individualitem', 
            		'customer_order_shipment_id', 
            		'order'=>'active_ind_items.item_id', 
//             		'with'=>'customer_order_items', 
//             		'together'=>true, 
            		'condition'=>'active_ind_items.status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'"
            			AND active_ind_items.status != "'.Individualitem::$STATUS_SOLD.'"'
            ),
			'scanned_out_ind_items'=>array(
					self::HAS_MANY,
					'Individualitem',
					'customer_order_shipment_id',
					'order'=>'scanned_out_ind_items.item_id',
// 					'with'=>'customer_order_items',
// 					'together'=>true,
					'condition'=>'scanned_out_ind_items.status = "'.Individualitem::$STATUS_SCANNED_OUT.'"'
			),
			'claim_docs'=>array(
					self::HAS_MANY,
					'OfficialDoc',
					'item_id',
					'condition'=>'type = "'.OfficialDoc::$TYPE_CLAIM.'"'
			),
			'claims'=>array(
					self::HAS_MANY,
					'CustomerOrderShipmentClaim',
					'shipment_id',
			),
			'damaged_ind_items'=>array(self::HAS_MANY, 'Individualitem', 'customer_order_shipment_id', 'order'=>'damaged_ind_items.item_id', 'with'=>'customer_order_items', 'together'=>true, 'condition'=>'damaged_ind_items.damaged = 1'),
			'customer_order'=>array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
            'current_user_bid'=>array(self::HAS_ONE, 'CustomerOrderShipmentBid', 'shipment_id', 'condition'=>'customer_shipper_id='.Yii::app()->user->id),
            'lowest_user_bid'=>array(self::HAS_ONE, 'CustomerOrderShipmentBid', 'shipment_id', 'order'=>'rate_quote'),
            //returns bid model which is selected by admin or the lowest one if nothing is selected manually
			'selected_user_bid'=>array(self::HAS_ONE, 'CustomerOrderShipmentBid', 'shipment_id', 'order'=>'selected DESC, rate_quote'),
			'bids'=>array(self::HAS_MANY, 'CustomerOrderShipmentBid', 'shipment_id'),
			'total_bids'=>array(
					self::STAT, 
					'CustomerOrderShipmentBid', 
					'shipment_id'
			),
			'total_ind_items'=>array(
					self::STAT,
					'Individualitem',
					'customer_order_shipment_id'
			),
			'total_scanned_out_ind_items'=>array(
					self::STAT,
					'Individualitem',
					'customer_order_shipment_id',
					'condition'=>'status = "'.Individualitem::$STATUS_SCANNED_OUT.'"',
			),
			'total_damaged_ind_items'=>array(
					self::STAT,
					'Individualitem',
					'customer_order_shipment_id',
					'condition'=>'damaged = 1'
			),
			'shipper'=>array(
                self::BELONGS_TO,
                'YumUser',
                'shipper_id',
                'with'=>'profile'
            ),
			'total_additional_paid'=>array(
                self::STAT,
                'CustomerPayment',
                'item_id',
                'condition'=>'type='.CustomerPayment::TYPE_SHIPMENT_ADDITIONAL,
                'select'=>'ROUND(SUM(amount),2)'
            ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_order_id' => 'Customer Order ID',
			'status' => 'Status',
			'pickup_date' => 'Pickup Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('customer_order_id',$this->customer_order_id,false);
		$criteria->compare('status',$this->status,false);
		$criteria->compare('pickup_date',$this->pickup_date,false);
        
        $criteria->order = 'id ASC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// search function for select bids page
	public function searchBidding()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('t.status', self::$STATUS_BIDDING);
		$criteria->with = array('customer_order', 'customer_order.billing_info');
		$criteria->together = true;
        $criteria->order = 't.id DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'Pagination' => array (
				'PageSize' => 50
			),
		));
	}
	
	public function searchCompleted()
	{
		$criteria=new CDbCriteria;

        if ($this->_filter_customer_name){
            $criteria->compare('billing_info.billing_name', $this->_filter_customer_name, true);
            $criteria->compare('billing_info.billing_last_name', $this->_filter_customer_name, true, 'or');
            $criteria->compare('billing_info.shipping_name', $this->_filter_customer_name, true, 'or');
            $criteria->compare('billing_info.shipping_last_name', $this->_filter_customer_name, true, 'or');
            $criteria->compare('customer.first_name', $this->_filter_customer_name, true, 'or');
            $criteria->compare('customer.last_name', $this->_filter_customer_name, true, 'or');
            $criteria->with = array('customer_order', 'customer_order.billing_info', 'customer_order.customer');
        }

        $criteria->compare('shipper_id', $this->shipper_id);

        $criteria->compare('t.status', self::$STATUS_COMPLETED);
// 		$criteria->together = true;
        $criteria->order = 't.pickup_date DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchClaims($resolved = false)
	{
		$criteria=new CDbCriteria;
		
		$criteria->compare('shipper_id', $this->shipper_id);
		
		$criteria->compare('claim', 1);
		$criteria->compare('claim_resolved', intval($resolved));
		$criteria->with = array('damaged_ind_items','damaged_ind_items.items', 'customer_order', 'customer_order.billing_info', 'customer_order.customer');
		$criteria->together = true;
		$criteria->order = 't.claim_date DESC';
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}
	
	public function searchScheduled($date = false, $with_bol)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('t.status', self::$STATUS_SHIPPER_PICKUP);
		$criteria->compare('t.status', self::$STATUS_SCANNED_OUT, false, 'or');
		$criteria->compare('t.shipper_id', '>0');
		if ($date){
			$date = date('Y-m-d', strtotime($date));
			$criteria->addCondition('t.pickup_date>="'.$date.'" AND t.pickup_date<ADDDATE(\''.$date.'\', 1)');
		}
		if ($with_bol){
			$criteria->addCondition('bol_path != ""');
		} else {
			$criteria->addCondition('bol_path = ""');
		}

		$criteria->with = array('customer_order', 'customer_order.billing_info');
		$criteria->together = true;
        $criteria->order = 't.id DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchShipperPickup()
	{
		$criteria=new CDbCriteria;
		
		$criteria->compare('shipper_id',$this->shipper_id,false);
		$criteria->compare('t.status', self::$STATUS_SHIPPER_PICKUP);
		$criteria->with = array('customer_order', 'customer_order.billing_info');
		$criteria->together = true;
        $criteria->order = 't.id DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchCustomerPickup()
	{
		$criteria=new CDbCriteria;
	
		$criteria->compare('shipper_id',$this->shipper_id,false);
		$criteria->compare('t.status', self::$STATUS_PICKUP);
		$criteria->with = array('customer_order', 'customer_order.billing_info');
		$criteria->together = true;
		$criteria->order = 't.id DESC';
	
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}
	
	// search function for shipper bidding page
	public function searchBiddingShipper()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;
        
		$criteria->compare('t.status', self::$STATUS_BIDDING);
		$criteria->with = array('customer_order', 'customer_order.billing_info');
		$criteria->together = true;
		$criteria->join = 'LEFT JOIN customer_order_shipment_bid as b 
							ON t.id=b.shipment_id AND customer_shipper_id='.Yii::app()->user->id;
        $criteria->order = 't.id DESC';
        
        $criteria->addCondition('ISNULL(b.id)');
        // return shipment is only visible for the shipper that shipped original shipment
        $criteria->addCondition('t.return = 0 OR (t.return = 1 AND t.shipper_id = '.Yii::app()->user->id.')');
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchInvoices($paid = false, $shipper_id = false){
		$criteria=new CDbCriteria;
		
		if (!$shipper_id){
			$shipper_id = $_GET['shipper_id'] ? $_GET['shipper_id'] : false;
		}

        if ($shipper_id){
            $criteria->compare('t.shipper_id', $shipper_id);
        } else {
            $criteria->addCondition('t.shipper_id > 0 OR third_party_shipment_cost>0');
        }

		$criteria->compare('t.status', self::$STATUS_COMPLETED);
		$criteria->compare('t.paid', intval($paid));

		$criteria->with = array('customer_order', 'customer_order.billing_info');
		$criteria->together = true;
		$criteria->order = 't.pickup_date DESC';
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'Pagination' => array(
						'PageSize' => 50
				),
		));
	}
	
	public function searchCSReport(){
		$date_from 	= isset($_GET['date_from']) && $_GET['date_from'] ? date('Y-m-d H:i:s', strtotime($_GET['date_from'])) : false;
    	$date_to 	= isset($_GET['date_to']) && $_GET['date_to'] ? date('Y-m-d H:i:s', strtotime($_GET['date_to'])) : false;
		$shipper_id	= isset($_GET['shipper_id']) ? $_GET['shipper_id'] : false;
		$filter_by	= isset($_GET['filter_by']) ? $_GET['filter_by'] : false;
		 
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.status = "'.CustomerOrderShipment::$STATUS_SCANNED_OUT.'" OR t.status="'.CustomerOrderShipment::$STATUS_COMPLETED.'"');
		$criteria->order = 't.id DESC';
		
		if ($date_from && $date_to){
			if ($filter_by == 'pickup'){
				$criteria->addCondition('t.pickup_date >= "'.$date_from.'"');
				$criteria->addCondition('t.pickup_date <= ADDDATE("'.$date_to.'", 1)');
				$criteria->with = 'customer_order';
				$criteria->addCondition('customer_order.shipping_method = "'.CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP.'"');
			} elseif ($filter_by == 'payment'){
				$criteria->addCondition('paid_date >= "'.$date_from.'"');
				$criteria->addCondition('paid_date <= ADDDATE("'.$date_to.'", 1)');
			}
		}
		 
		if ($shipper_id){
			$criteria->compare('shipper_id', $shipper_id);
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'Pagination' => array(
				'PageSize' => 10
			),
		));
	}
	
	public function countTotal(){
		return count($this->ind_items);
	}
	
	public function countInStock(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if ($item->status == Individualitem::$STATUS_IN_STOCK){
				$count++;
			}
		}
		
		return $count;
	}
	
	public function countIncoming(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if ($item->status == Individualitem::$STATUS_ORDERED_WITHOUT_ETL
				||$item->status == Individualitem::$STATUS_ORDERED_WITH_ETL
				||$item->status == Individualitem::$STATUS_ORDERED_WITH_ETA){
				$count++;
			}
		}
		
		return $count;
	}
	
	public function countNotOrdered(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if ($item->status == Individualitem::$STATUS_NOT_ORDERED){
				$count++;
			}
		}
		
		return $count;
	}
	
	public function countCancelled(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if ($item->status == Individualitem::$STATUS_CANCELLED_SHIPPING){
				$count++;
			}
		}
		
		return $count;
	}
	
	public function scanAsOut(){
		// TODO: don't forget about customer notes
		if($this->customer_order->shipping_method == 'Local Pickup'){
			$this->status = self::$STATUS_COMPLETED;
		} else {
			$this->status = self::$STATUS_SCANNED_OUT;
		}
		if ($this->save()){
			$this->attachItemsForShipping();
			// TODO: think about using before/aftersave for order updating
			$this->customer_order->updateShippingStatus();
			// active items array has been probably modified by attach items function
			$items = $this->getRelated('active_ind_items', true);
			foreach ($items as $item){
				$item->scanAsOut(false);
			}
			return true;
		} else {
			return false;
		}
	}
	
	// no_update indicates if we should update parent customer order shipping status
	public function cancel($reason, $chargeback = false, $update_order = true){
		if ($this->return){
			$this->cancelReturn();
			return true;
		}
		foreach ($this->active_ind_items as $item){
			$item->cancelShipping($reason, $chargeback, $update_order);
		}
		
		$this->status = self::$STATUS_CANCELLED;
		$this->cancellation_reason = $reason;
		$this->save();
		
		if ($update_order && $this->customer_order){
			$this->customer_order->updateShippingStatus();
			$this->customer_order->calculateAndSaveProfit();
		}
		
		return true;
	}
	
	private function cancelReturn(){
		$bol_path = $this->bol_path;
		$shipper_id = $this->shipper_id;
		$this->customer_order->updateShippingStatus();
		if ($bol_path){
			@unlink(Yii::getPathOfAlias('webroot').'/upload/bol/'.$bol_path);
		}
		$criteria = new CDbCriteria();
		$criteria->compare('shipment_id', $this->id);

		if ($this->selected_user_bid){
			// send notification
			Yii::import('application.extensions.phpmailer.JPhpMailer');
	
			$shipper = $this->shipper;
			$shipper_name = $shipper->profile->firstname.' '.$shipper->profile->lastname;
				
			$subject = AutoMailMessage::MS_RETURN_CANCELLED;
			$search = array('[name]','[quote_number]');
			$replace = array($shipper_name, $this->selected_user_bid->rate_quote_number);
			$body = str_replace($search, $replace, AutoMailMessage::MC_RETURN_CANCELLED);
			
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->claim_email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
			
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Body = $body;
			$mail->Subject = $subject;
			$mail->Send();
		}
        CustomerOrderShipmentBid::model()->deleteAll($criteria);

		$return_ids = array();
		foreach ($this->returns as $return){
			$return_ids[] = $return->id;
			$return->delete();
		}
		
		$criteria = new CDbCriteria();
		$criteria->addInCondition('customer_return_id', $return_ids);
		Individualitem::model()->updateAll(array('customer_return_id'=>0), $criteria);
		$this->delete();
	}
	
	public function getItemTable($scanned_out = false){
		if ($this->return){
			$criteria = new CDbCriteria();
			$criteria->compare('shipment_id', $this->id);
			$criteria->with = array('items', 'items.box');
			$criteria->together = true;
			$i_items = IndividualItemReturn::model()->findAll($criteria);
		} else {
			$criteria = new CDbCriteria();
			$criteria->compare('customer_order_shipment_id', $this->id);
			if ($scanned_out){
				$criteria->addCondition('t.status = "'.Individualitem::$STATUS_SCANNED_OUT.'"');
			} else {
				$criteria->addCondition('t.status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'"
										AND t.status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
			}
			
			$criteria->with = array('items', 'items.box');
			$criteria->together = true;
			$i_items = Individualitem::model()->findAll($criteria);
		}
		$items = array();
		foreach ($i_items as $i_item){
			$items[$i_item->item_id][] = $i_item;
		}
		
		Yii::import('application.controllers.CustomershipperController');
		
		$controller = new CustomershipperController('');
		return $controller->renderPartial('customershipper/_shipment_item_table', array(
			'items'=>$items,
			'shipment'=>$this,
		), true);
	}
    
    public function getBoxQty(){
        $qty = 0;
        
        if ($this->return){
        	foreach ($this->returns as $row){
        		if (!$row->returned_boxes){
        			// all boxes are replaced
        			$qty += $row->items->box_qty;
        		} else {
        			$qty += count(explode(',', $row->returned_boxes));
        		}
        	}
        	
        	return $qty;
        } 
        
        if ($this->status == self::$STATUS_COMPLETED || $this->status == self::$STATUS_SCANNED_OUT){
        	$items = $this->scanned_out_ind_items;
        } else {
        	$items = $this->active_ind_items;
        }
        foreach ($items as $item){
          	if ($item->is_replacement){
          		if (!$item->replacement_boxes){
          			// all boxes are replaced
          			$qty += $item->items->box_qty;
          		} else {
          			$qty += count(explode(',', $item->replacement_boxes));
          		}
           	} else {
        		$qty += $item->items->box_qty;
           	}
        }
        
        return $qty;
    }
    
    public function getBoxDimsStr(){
        $rows = array();
        
        if ($this->return){
        	foreach ($this->returns as $row){
        		if (!$row->returned_boxes){
        			$rows[] = $row->items->getBoxDimsStr();
        		} else {
        			$boxes = explode(',',$row->returned_boxes);
        			foreach ($boxes as $box){
        				$rows[] = $row->items->getSingleBoxDimsStr($box);
        			}
        		}
        	}
        } else {
	        if ($this->status == self::$STATUS_COMPLETED || $this->status == self::$STATUS_SCANNED_OUT){
	        	$items = $this->scanned_out_ind_items;
	        } else {
	        	$items = $this->active_ind_items;
	        }
	        foreach ($items as $item){
	        	if (!$item->is_replacement || !$item->replacement_boxes){
	        		$rows[] = $item->items->getBoxDimsStr();
	        	} else {
	        		$boxes = explode(',',$item->replacement_boxes);
	        		foreach ($boxes as $box){
	        			$rows[] = $item->items->getSingleBoxDimsStr($box);
	        		}
	        	}
	        }
        }
        
        return implode('<br />', $rows);
    }
    
    /**
     * returns array containing box dimensions and weight
     * array of associative arrays 
     */
    public function getBoxes(){
    	if (!($this->boxes)){
	    	$this->boxes = array();
	    	if ($this->status == self::$STATUS_COMPLETED || $this->status == self::$STATUS_SCANNED_OUT){
	    		$items = $this->scanned_out_ind_items;
	    	} else {
	    		$items = $this->active_ind_items;
	    	}
	    	foreach ($items as $item){
	    		for ($i = 1; $i <= $item->items->box_qty; $i++){
	    			$width = 'box_'.$i.'_width';
					$height = 'box_'.$i.'_height';
					$length = 'box_'.$i.'_length';
					$weight = 'box_'.$i.'_weight';
					$this->boxes[] = array(
						'width' => $item->items->box->$width,
						'height' => $item->items->box->$height,
						'length' => $item->items->box->$length,
						'weight' => $item->items->box->$weight,
					);
	    		}
	    	}

    	}
    	
    	return $this->boxes;
    }
    
    // this function stores correct shipping price to the table row
    public function updateShippingPrice(){
        $this->total_shipping_price = $this->calculateShippingPrice();
        return $this->save();
    }
    
	public function sendPickupEmail(){
		$customer = Customer::model()->findByPk($this->customer_order->customer_id);
// 		$adminModel = YumUser::model()->findByPk(1);
// 		$adminMailAddress = $adminModel->profile->email;
		
		// send to customer
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->AddAddress($customer->email);
		$mail->AddAttachment(Yii::getPathOfAlias('webroot') . '/pdf/pickup/shipment_'.$this->id.'.pdf');
		//~ $mail->SetFrom($adminMailAddress, 'Admin');		
		$mail->SetFrom(Customer::NOTIFICATION_EMAIL);
		$mail->Subject = AutoMailMessage::MS_CUSTOMER_PICKUP_SCHEDULED;
		$body = str_replace('[date]', date('M d, Y \a\t H:i A', strtotime($this->pickup_date)), AutoMailMessage::MC_CUSTOMER_PICKUP_SCHEDULED);
        $warehouse_address = Settings::getVar('company_address_street').' '.Settings::getVar('company_address_city').', '.Settings::getVar('company_address_state').' '.Settings::getVar('company_address_zip');
        $body = str_replace('[warehouse_address]', $warehouse_address, $body);
        $mail->Body = $body;

		$mail->Send();

		// now send the copy to aperators, admin, and warehouse
		$user_ids = AutoMail::getAdminOperatorWarehouseUserIdList();
		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id', $user_ids);
		$users = YumUser::model()->with('profile')->findAll($criteria);
		
		foreach ($users as $user){
			if ($user->profile){
				$mail = new JPhpMailer;
				$mail->IsSendmail();
				$mail->AddAddress($user->profile->email, $user->profile->firstname.' '.$user->profile->lastname);
				$mail->AddAttachment(Yii::getPathOfAlias('webroot') . '/pdf/pickup/shipment_'.$this->id.'.pdf');
				$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
				$mail->Body = $body;
				$mail->Subject = 'Customer Pickup Scheduled';
				$mail->Send();
			}
		}

	}
	
	public function sendClaimEmail(){
		if (!$this->shipper_id){
			return false;
		}
		
		$shipper = YumUser::model()->with('profile')->findByPk($this->shipper_id);
		$bid = $this->selected_user_bid;
		
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->AddAddress($shipper->profile->claim_email);
		$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
		$mail->Subject = str_replace('[quote_number]', $bid->rate_quote_number, AutoMailMessage::MS_CUSTOMER_SHIPPER_CLAIM);
		$find = array('[name]', '[zip]', '[customer_name]', '[quote_number]');
		$replace = array(
			$shipper->profile->firstname.' '.$shipper->profile->lastname,
			$this->customer_order->billing_info->shipping_zip_code,
			$this->customer_order->billing_info->shipping_name.' '.$this->customer_order->billing_info->shipping_last_name,
			$bid->rate_quote_number,
		);
		$body = str_replace($find, $replace, AutoMailMessage::MC_CUSTOMER_SHIPPER_CLAIM);
		$mail->Body = $body;
		
		$mail->Send();
	}
	
    public function getShippingPrice(){
        return $this->total_shipping_price;
    }   
    
    // returns current shipping price
    public function calculateShippingPrice(){
        $total = 0;
        
		foreach ($this->active_ind_items as $item){
			$total += $item->shipping_total;
		}
		
        return round($total, 2);
    }
    
	public function calculateTotalPrice($with_tax = true){
		$total = 0;
		foreach ($this->active_ind_items as $item){
			$total += $item->shipping_total + $item->sale_price;
		}
		
		if ($with_tax && $this->customer_order->add_sales_tax){
            $total = $total * (100 + $this->customer_order->getTaxPercent()) / 100;
        }
		
        return round($total, 2);
	}
	
	public function generatePickupPDF(){
		$controller = new CustomershipperController('');
		$controller->layout = '//layouts/pdf';
		
		$items = array();
		foreach ($this->ind_items as $item){
			if (isset($items[$item->item_id])){
				$items[$item->item_id]['qty']++;
			} else {
				$items[$item->item_id] = array('qty'=>1, 'name' => $item->items->item_name);
			}
		}
		
        $html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($controller->render('pdf/shipment', array(
				'shipment'=>$this,
				'order'=>$this->customer_order,
				'items'=>$items,
                'company_name' => $this->getCompanyName(),
			), true));
        $file = Yii::getPathOfAlias('webroot') . '/pdf/pickup/shipment_'.$this->id.'.pdf';
		$html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
		return $file;
	}

    public function getCompanyName()
    {
        return isset($this->customer_order->payment_processor) ?
            $this->customer_order->payment_processor
            : Settings::getVar('company_name');
    }
    public function getCurrentUserBidRateQuote(){
        return $this->current_user_bid ? $this->current_user_bid->rate_quote : false;
    }
    
    public function getCurrentUserBidRateQuoteNumber(){
        return $this->current_user_bid ? $this->current_user_bid->rate_quote_number : false;
    }

    public function getSelectedUserBidRateQuote() {
        return $this->selected_user_bid ? $this->selected_user_bid->rate_quote : false;
    }
    // returns 90% of shipping price or -1 if order is replacement
    public function getQuoteLimit(){
        if ($this->customer_order->is_replacement){
        	return -1;
        }
    	return round($this->getShippingPrice() * self::QUOTE_LIMIT, 2);
    }
    
    public function isCurrentBidTooHigh(){
        $lowest_quote = $this->getLowestQuote();
        return $this->current_user_bid->rate_quote > $this->getQuoteLimit() 
			|| ($lowest_quote && $this->current_user_bid->rate_quote>$lowest_quote);
    }
    
    public function getLowestQuote(){
        return $this->lowest_user_bid ? $this->lowest_user_bid->rate_quote : false;
    }
	
	public function getCustomerBillingName(){
		return $this->customer_order->getBillingName();
	}
	
	public function getCustomerShippingName(){
		return $this->customer_order->getShippingName();
	}
	
	public function getShipperName(){
		return $this->selected_user_bid ? $this->selected_user_bid->getShipperName() : false;
	}
	
	public function deselectAllBids(){
		$criteria = new CDbCriteria();
		$criteria->compare('shipment_id', $this->id);
		CustomerOrderShipmentBid::model()->updateAll(array('selected'=>0, 'selection_date'=>'0000-00-00 00:00:00'), $criteria);
	}
	
	// sets correct status depending on individual items statuses
	public function updateStatus($update_order = true){
		$cancelled = 0;
		$scanned_out = 0;
		foreach ($this->ind_items as $item){
			if ($item->status == Individualitem::$STATUS_CANCELLED_SHIPPING){
				$cancelled++;
			} else if ($item->status == Individualitem::$STATUS_SOLD) {
				$scanned_out++;
			}
		}
		
		if ($cancelled == count($this->ind_items)){
			// all items are cancelled -> shipment is cancelled
			$this->status = self::$STATUS_CANCELLED;
			// echo '1';
			$this->save();
		} else if ($cancelled + $scanned_out == count($this->ind_items)){
			// TODO: add scanned out date
			// all items are cancelled or scanned out -> shipment is scanned out
			$this->status = self::$STATUS_SCANNED_OUT;
			// echo '2';
			$this->save();
		} else {
			// echo '3';
		}

		if ($update_order){
			$this->customer_order->updateShippingStatus();
		}
		
		$this->customer_order->calculateAndSaveProfit();
	}
	
	/**
	 *
	 * @param bool $by_customer if false - date is set immediately, if true - date is set by customer and $date parameter is ignored
	 * @param string $date	 
	 */
	public function scheduleCustomerPickup($by_customer, $date = ''){
		$this->status = CustomerOrderShipment::$STATUS_PICKUP;
		if (!$by_customer){
			$this->pickup_date = date('Y-m-d H:i:s', strtotime($date));
			$this->pickup_code = '';
		} else {
			$this->pickup_code = substr(md5(uniqid()), 0, 8);
		}
		
		// next line also saves the model so we do not save it again
		if (!$this->updateShippingPrice()){
			return false;
		}
		
		$this->attachItemsForShipping();
		
		$path = $this->generatePickupPDF();
		$this->customer_order->updateShippingStatus();
		
		if (!$by_customer){
			$this->sendPickupEmail();
		} else {
			$link = Yii::app()->getBaseUrl(true).'/index.php/customer/pickup_date/?id='.$this->id.'&code='.$this->pickup_code;
			$customer = Customer::model()->findByPk($this->customer_order->customer_id);
			$adminModel = YumUser::model()->findByPk(1);
			$adminMailAddress = $adminModel->profile->email;
		
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($customer->email);
			//~ $mail->SetFrom($adminMailAddress, 'Admin');
			$mail->SetFrom(Customer::NOTIFICATION_EMAIL);
			$mail->Subject = AutoMailMessage::MS_CUSTOMER_PICKUP_SCHEDULED_BY_CUSTOMER;
			$mail->Body = str_replace('[link]', $link, AutoMailMessage::MC_CUSTOMER_PICKUP_SCHEDULED_BY_CUSTOMER);
			$mail->Send();
		}
		
		return true;
	}
	
	private function attachItemsForShipping(){
		// select in stock items to attach to the shipment before shipping
		$items = array();
		foreach ($this->active_ind_items as $item){
			if ($item->status != Individualitem::$STATUS_IN_STOCK){
				$items[$item->item_id][] = $item;
			}
		}
			
		// TODO: write many comments about how item/shipment/order connections work
		foreach ($items as $item_id => $rows){
			$criteria = new CDbCriteria();
			$criteria->compare('shipping_started', 0);
			$criteria->compare('item_id', $item_id);
			$criteria->compare('status', Individualitem::$STATUS_IN_STOCK);
			$criteria->compare('customer_order_shipment_id', '<>'.$this->id);
			$criteria->limit = count($rows);
			$new_items = Individualitem::model()->findAll($criteria);
			foreach ($rows as $i => $old_item) {
				// we need to swap some fields that contain customer order info
				Individualitem::swapCustomerOrderInfo($old_item, $new_items[$i]);
				$old_item->save();
				$new_items[$i]->save();
			}
		}
			
			
		// update ind. items, stick them to the shipment
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_shipment_id', $this->id);
		Individualitem::model()->updateAll(array('shipping_started'=>1), $criteria);
		
		return true;
	}
	
	public function sendForBidding($update_order = true){
		$this->status = CustomerOrderShipment::$STATUS_BIDDING;
        if ($this->updateShippingPrice()){
        	if ($update_order){
				$this->customer_order->updateShippingStatus();
			}
			
			$this->attachItemsForShipping();
			
			$note = 'Shipment #'.$this->id.' sent for bidding';
			$this->customer_order->customer->addNote($note, $this);
			
			// send shipper notifications
			$users = YumUser::getCustomerShipperUserList();
			
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			
			foreach ($users as $user){
				$table  = '<table>';
				$table .= '<tr><th>Number of Boxes</th><th>Destination City, State</th><th>Destination ZIP Code</th></tr>';
				$table .= '<tr><td>'.$this->getBoxQty().'</td>';
				$table .= '<td>'.$this->customer_order->billing_info->shipping_city.', '.$this->customer_order->billing_info->shipping_state.'</td>';
				$table .= '<td>'.$this->customer_order->billing_info->shipping_zip_code.'</td></tr>';
				$table .= '</table>';
                $name = $user->profile->firstname.' '.$user->profile->lastname;
                $subject = AutoMailMessage::MS_SHIPPER_NEW_SHIPMENT_AUTO;
                $body = str_replace('[name]', $name, AutoMailMessage::MC_SHIPPER_NEW_SHIPMENT_AUTO);
                $body = str_replace('[shipment_table]', $table, $body);
                $from_email = AutoMail::getAdminEmail();

                $mail = new JPhpMailer;
                $mail->IsSendmail();
                $mail->IsHTML();
                $mail->AddAddress($user->profile->email, $name);
                $mail->SetFrom($from_email, AutoMail::EMAIL_FROM);
                $mail->Subject = $subject;
				$mail->Body = $body;
				$mail->Send();
                if ($add_emails = $user->profile->getAdditionalEmails()){
                    foreach ($add_emails as $email){
                        $mail = new JPhpMailer;
                        $mail->IsSendmail();
                        $mail->IsHTML();
                        $mail->AddAddress($email, $name);
                        $mail->SetFrom($from_email, AutoMail::EMAIL_FROM);
                        $mail->Subject = $subject;
                        $mail->Body = $body;
                        $mail->Send();
                    }
                }
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public function sendForShipperPickup($date){
		$this->pickup_date = date('Y-m-d H:i:s', strtotime($date));
		$this->status = self::$STATUS_SHIPPER_PICKUP;
		$this->shipper_id = $this->selected_user_bid->customer_shipper_id;
		$this->selected_user_bid->selected = 1;
		$this->selected_user_bid->selection_date = date('Y-m-d H:i:s');
		$this->selected_user_bid->save();
		
		//$items_str = nl2br($this->getItemsListForEmail());
		$note = 'Customer shipper pickup scheduled for shipment #'.$this->id.': '.date('M. d, Y \a\t H:i A', strtotime($date));
		//$note .= '<br /><strong>Items to be picked up:</strong><br />'.$items_str;

		$this->customer_order->customer->addNote($note, $this);
		
		return $this->save();
	}
	
	// manifest emails
	public static function sendShipperEmails($shipments, $date){
		// we need to create manifest for each shipper containing all his shipments for the date 
		$by_shipper = array();
		foreach ($shipments as $shipment){
			$by_shipper[$shipment->selected_user_bid->customer_shipper_id][] = $shipment;
		}
							
		$adminModel = YumUser::model()->findByPk(1);
		$adminMailAddress = $adminModel->profile->email;
		
		$time = strtotime($date);
		
		foreach ($by_shipper as $shipper_id=>$shipments){
			$shipper = YumUser::model()->findByPk($shipper_id);
			
			$controller = new CustomershipperController('');
			$controller->layout = '//layouts/pdf';			
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($controller->render('pdf/shipper_manifest', array(
					'shipments'=>$shipments,
					'shipper'=>$shipper,
					'date'=>$date,
				), true));
				
			// using time of sending instead of pickup time to avoid duplicating
			$file = Yii::getPathOfAlias('webroot') . '/pdf/shipper_manifest/shipper_'.$shipper->id.'_'.time().'.pdf';
			$html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
			
			$body = AutoMailMessage::MC_CUSTOMER_SHIPPER_MANIFEST_EMAIL;
			$body = str_replace('[name]', $shipper->profile->firstname.' '.$shipper->profile->lastname, $body);
			$body = str_replace('[date]', date('M d, Y \a\t H:i A', $time), $body);
			$body = str_replace('[company_city]', Settings::getVar('company_address_city'), $body);
			$count = count($shipments);
			$body = str_replace('[shipments]', $count.' shipment'.($count>1 ? 's' : ''), $body);
			$subject  = AutoMailMessage::MS_CUSTOMER_SHIPPER_MANIFEST_EMAIL;
			
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($shipper->profile->email);
			$mail->SetFrom($adminMailAddress, AutoMail::EMAIL_FROM);
			$mail->Subject = $subject;
			$mail->AddAttachment($file);
			$mail->Body = $body;
			$mail->Send();
			if ($add_emails = $shipper->profile->getAdditionalEmails()){
				foreach ($add_emails as $email){
					$mail = new JPhpMailer;
					$mail->IsSendmail();
//					$mail->AddAddress($email, $name);
					$mail->AddAddress($email);
					$mail->SetFrom($adminMailAddress, AutoMail::EMAIL_FROM);
					$mail->Subject = $subject;
					$mail->AddAttachment($file);
					$mail->Body = $body;
					$mail->Send();
				}
			}
		}
	}
	
	public static function generateShippingLabels($shipments, $date = false){
		// TODO: don't forget to add all needed emails (admin, operator, warehouse)
		$controller = new CustomershipperController('');
		$controller->layout = '//layouts/pdf';			
		$html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($controller->render('pdf/shipping_labels', array(
				'shipments'=>$shipments,
				'date'=>$date,
			), true));
			
		// using time of sending instead of pickup time to avoid duplicating
		$file = Yii::getPathOfAlias('webroot') . '/pdf/shipping_labels/labels_'.time().'.pdf';
		//~ $html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
		$filename = 'labels';
		if ($date){
			$filename .= '_'.str_replace(array('. ', ', '), '_', $date);
		}
		$html2pdf->Output($filename.'.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
		
		//~ $body = AutoMailMessage::MC_CUSTOMER_SHIPPER_MANIFEST_EMAIL;
		//~ $body = str_replace('[name]', $shipper->profile->firstname.' '.$shipper->profile->lastname, $body);
		//~ $body = str_replace('[date]', date('M d, Y \a\t H:i A', $time), $body);
		//~ $count = count($shipments);
		//~ $body = str_replace('[shipments]', $count.' shipment'.($count>0 ? 's' : ''), $body);
		//~ 
		//~ Yii::import('application.extensions.phpmailer.JPhpMailer');
		//~ $mail = new JPhpMailer;
		//~ $mail->IsSendmail();
		//~ $mail->AddAddress($shipper->profile->email);
		//~ $mail->SetFrom($adminMailAddress, 'Admin');
		//~ $mail->Subject = AutoMailMessage::MS_CUSTOMER_SHIPPER_MANIFEST_EMAIL;
		//~ $mail->AddAttachment($file);
		//~ $mail->Body = $body;
		//~ $mail->Send();
	}
	
	public function isInStock(){
		$items = array();
		foreach ($this->active_ind_items as $item){
			$items[$item->item_id][] = $item;
		}
		
		foreach ($items as $item_id => $rows){
			$criteria = new CDbCriteria();
			$criteria->compare('shipping_started', 0);
			$criteria->compare('item_id', $item_id);
			$criteria->compare('status', Individualitem::$STATUS_IN_STOCK);
			if (Individualitem::model()->count($criteria) < count($rows)){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * returns the difference between current ayment amount according to the bid selected and already paid amount
	 */
	public function additionalPaymentAmountToPay(){
		return $this->additionalPaymentAmount() - $this->total_additional_paid;
	}
	
	public function getAdditionalPaymentColumn(){
		if ($this->return){
			return '';
		}
		
		$strings = array();
		
		if ($this->additional_payment_disabled){
			$strings[] = '[Disabled]';
		}
		
		$to_pay = $this->additionalPaymentAmountToPay();
		$str = '';
		if($this->additional_payment_required && !$this->additional_payment_done) {
			// means that payment notification has been sent to the customer 
			$str .= '<a href="'.Yii::app()->getBaseUrl(true).'/index.php/customer/additional_payment/?id='.$this->id.'&code='.$this->additional_payment_code.'">Notification sent ($'.$this->additional_payment_amount;
			$time = strtotime($this->additional_payment_notification_date);
			if ($time > 0){
				$str .= ' on '.date('M d', $time);
			}
			$str .= ')</a>';
			$strings[] = $str;
		}
		
		if ($this->selected_user_bid){
			if (!$this->additional_payment_disabled 
					&& $this->selected_user_bid->needsAdditionalPayment()
					&& $this->selected_user_bid->id != $this->additional_payment_bid){
				$strings[] = 'To pay: $'.number_format($to_pay, 2);
// 				$strings[] = 'Current amount: $'.number_format($this->additionalPaymentAmount(), 2);
				if ($to_pay > 0){
					$strings[] .= '<input type="button" value="Send Notification" class="send_payment_notification" rel="'.$this->id.'" />';
				}
			}
			
// 			if(!$this->additional_payment_disabled
// 					&& $this->selected_user_bid->needsAdditionalPayment()
// 					&& $this->selected_user_bid->id != $this->additional_payment_bid){
// 				$strings[] .= '<input type="button" value="Send Notification" class="send_payment_notification" rel="'.$this->id.'" />';
// 			}
		}
		
		
		
		if ($this->total_additional_paid){
			$strings[] .= 'Total Paid: $'.number_format($this->total_additional_paid, 2);
		}
		
		if (Yii::app()->user->isAdmin() && !$this->additional_payment_disabled){
			$strings[] .= '<input type="button" value="Disable" class="disable_additional_payment" rel="'.$this->id.'" />';
		}
		
		return implode('<br />', $strings);
	}
	
	public function readyForPickupScheduling(){
		if (!$this->total_bids){
			return false;
		}
		
		if ($this->selected_user_bid->auto_generated){
			return false;
		}
		
		if ($this->additionalPaymentAmountToPay() > 0 
				&& !$this->additional_payment_disabled 
				&& !$this->return){
			return false;
		}
		
// 		if ($this->additional_payment_done || $this->additional_payment_disabled){
// 			return true;
// 		}
		
// 		// TODO: check these conditions, they look wrong
// 		if ($this->additional_payment_required && !$this->additional_payment_done){
// 			if (!$this->selected_user_bid->needsAdditionalPayment()){
// 				return true;
// 			}
// 			return false;
// 		}
		
// 		if ($this->selected_user_bid->needsAdditionalPayment()){
// 			return false;
// 		}
		
		return true;
	}
	
	/**
	 * Shows whether items in the shipment have fields filled up (cbm, fob/exw, box dims)
	 */
	public function itemsHaveInfo(){
		$items = $this->getRelated('active_ind_items', false, array('with'=>'items', 'together'=>true));
		foreach($items as $item){
			if (!$item->items->filledInfo()){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Indicated whether shipping is ready for shipping process (bidding of customer pickup)
	 */
	public function readyForShipping(){
// 		if ($this->customer_order->shipping_method == CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP){
// 			return false;
// 		}
		if (!$this->isInStock()){
			return false;
		}
		if ($this->customer_order->verificationRequired()){
			return false;
		}
		
		if ($this->customer_order->shipping_edited_payment_amount>0 && !$this->customer_order->shipping_edited_payment_done){
			return false;
		}
		
		return true;
	}
	
	// this function doesn't only set the number field but also set's status to "completed"
	public function setTrackingNumber($number){
		$this->tracking_number = $number;
		if ($this->pickup_date == '0000-00-00 00:00:00' || !$this->pickup_date){
			// if pickup was not scheduled before - we set current date as a pickup date
			$this->pickup_date = date('Y-m-d H:i:s');
		}
		$this->scanAsOut();
		// TODO: check why STATUS_COMPLETED is used instead of STATUS_SCANNED_OUT
		$this->status = self::$STATUS_COMPLETED;
		$this->tracking_number_date = date('Y-m-d H:i:s');
		if ($this->save()){
			if ($this->customer_order->first_shipment_completed == '0000-00-00 00:00:00'){
				$this->customer_order->first_shipment_completed = date('Y-m-d H:i:s');
				$this->customer_order->save();
			}
			$this->customer_order->calculateAndSaveProfit();
			$this->sendTrackingNumberToCustomer();
			
			//$items_str = nl2br($this->getItemsListForEmail(true));
			$note  = 'Tracking number set for shipment #'.$this->id;
			$note .= '<br /><strong>Tracking number: </strong>'.$number;
			$note .= '<br /><strong>Website: </strong>'.$this->tracking_website;
			//$note .= '<br /><strong>Shipments contents: </strong><br />'.$items_str;
			$this->customer_order->customer->addNote($note, $this);
			/*$items = $this->ind_items;
			foreach ($items as $item){
				$this->customer_order->addCustomerOrderItemShippedNote($item->id);
			}*/
			return true;
		} else {
			return false;
		}
	}
	
	public function getShippingCompany(){
		if ($this->shipper_id){
			return $this->shipper->profile->company;
		} else {
			return $this->shipping_company;
		}
	}
	
	public function getShipperPhoneNumber(){
		if ($this->shipper_id){
			return $this->shipper->profile->phone;
		} else {
			return $this->phone_number;
		}
	}
	
	public function sendTrackingNumberToCustomer(){
		$items_str = $this->getItemsListForEmail(true);
			
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->AddAddress($this->customer_order->customer->email);
		$mail->SetFrom(Customer::NOTIFICATION_EMAIL, 'RegencyShop.com');
		$mail->Subject = AutoMailMessage::MS_CUSTOMER_TRACKING_NUMBER_SET;
		$search = array(
			'[website]',
			'[name]',
			'[items]',
			'[number]',
			'[company]',
			'[phone_number]'
		);
		$replace = array(
			$this->tracking_website,
			$this->customer_order->billing_info->billing_name.' '.$this->customer_order->billing_info->billing_last_name,
			$items_str,
			$this->tracking_number,
			$this->getShippingCompany(),
			$this->getShipperPhoneNumber()
		);
		$body = str_replace($search, $replace, AutoMailMessage::MC_CUSTOMER_TRACKING_NUMBER_SET);
		$mail->Body = $body;
		$mail->Send();
	}
	
	public function getItemsListForEmail($scanned_out = false){
		$items = array();
		if ($scanned_out){
			$ind_items = $this->scanned_out_ind_items;
		} else {
			$ind_items = $this->active_ind_items;
		}
		foreach ($ind_items as $item){
			if (isset($items[$item->item_id])){
				$items[$item->item_id][qty]++;
			} else {
				$items[$item->item_id] = array('qty'=>1, 'item'=>$item->items->item_name);
			}
		}
		
		$items_str = '';
		foreach ($items as $row){
			$items_str .= $row['item'].': '.$row['qty'].' item'.($row['qty']>1 ? 's' : '')."\n";
		}
		
		return $items_str;
	}
	
	/**
	 * returns amount according to the currently selected bid
	 */
	public function additionalPaymentAmount(){
		return round(($this->selected_user_bid->rate_quote - $this->total_shipping_price * self::QUOTE_LIMIT) * 1.15, 2);
	}
	
	// bringing item back to pending orders page
	public function requeue(){
		// we need to: 
		// - set status
		// - remove bids
		// - remove BOL
		// - clean up shipper, website, pickup date and all other fields
		// - send shipper notification
		$bol_path = $this->bol_path;		
		$shipper_id = $this->shipper_id;
		$old_status = $this->status;
		
		$this->status = self::$STATUS_PENDING;
		$this->pickup_date 							= '0000-00-00 00:00:00';
		$this->shipper_id 							= 0;
		$this->shipping_services_bids_generated		= 0;
		$this->total_shipping_price 				= 0;
		$this->additional_payment_required 			= 0;
		$this->additional_payment_disabled 			= 0;
// 		$this->additional_payment_amount 			= 0;
// 		$this->additional_payment_done 				= 0;
// 		$this->additional_payment_date 				= '0000-00-00 00:00:00';
// 		$this->additional_payment_code 				= '';
		$this->additional_payment_bid 				= 0;
		$this->bol_path 							= '';
		$this->bol_upload_date 						= '0000-00-00 00:00:00';
		$this->tracking_website 					= '';
		if ($this->save()){
			$this->customer_order->updateShippingStatus();
			$this->customer_order->customer->addNote('Shipment #'.$this->id.' requeued');
			if ($bol_path){
				@unlink(Yii::getPathOfAlias('webroot').'/upload/bol/'.$bol_path);
			}
			$criteria = new CDbCriteria();
			$criteria->compare('shipment_id', $this->id);
			CustomerOrderShipmentBid::model()->deleteAll($criteria);
			
			// update ind. items, detach them from the shipment
			$criteria = new CDbCriteria();
			$criteria->compare('customer_order_shipment_id', $this->id);
			Individualitem::model()->updateAll(array('shipping_started'=>0), $criteria);
				
			
			if ($shipper_id){
				// send notification
				Yii::import('application.extensions.phpmailer.JPhpMailer');
				
				$adminModel = YumUser::model()->findByPk(1);
				$adminMailAddress = $adminModel->profile->email;
				
				$mail = new JPhpMailer;
				$mail->IsSendmail();
				$mail->AddAddress(YumUser::model()->findByPk($shipper_id)->profile->email);
				$mail->SetFrom($adminMailAddress, 'Furniture Store');
				$mail->Subject = AutoMailMessage::MS_SHIPPER_SHIPMENT_REQUEUED;
				$body = str_replace('[id]', $this->id, AutoMailMessage::MC_SHIPPER_SHIPMENT_REQUEUED);
				$mail->Body = str_replace('[zip]', $this->customer_order->billing_info->shipping_zip_code, $body);
				$mail->Send();
			} elseif($this->customer_order->shipping_method == CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP
					&& $old_status == self::$STATUS_PICKUP) {
				// send customer notification if it's local pickup
				Yii::import('application.extensions.phpmailer.JPhpMailer');

				$mail = new JPhpMailer;
				$mail->IsSendmail();
				$mail->AddAddress($this->customer_order->customer->email);
				$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
				$mail->Subject = AutoMailMessage::MS_CUSTOMER_SHIPMENT_REQUEUED;
				$body = str_replace('[name]', $this->getCustomerName(), AutoMailMessage::MC_CUSTOMER_SHIPMENT_REQUEUED);
				$mail->Body = $body;
				$mail->Send();
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public function getCustomerName(){
		return $this->customer_order->customer->first_name.' '.$this->customer_order->customer->last_name;
	}
	
	public function resetBol(){
		$this->bol_path = '';
		$this->bol_upload_date = '0000-00-00 00:00:00';
		if ($this->save()){
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			$name = $this->shipper->profile->firstname.' '.$this->shipper->profile->lastname;
			
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($this->shipper->profile->email);
			$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
			$mail->Subject = AutoMailMessage::MS_SHIPPER_BOL_RESET;
			$body = str_replace('[name]', $name, AutoMailMessage::MC_SHIPPER_BOL_RESET);
			$mail->Body = str_replace('[zip]', $this->customer_order->billing_info->shipping_zip_code, $body);
			$mail->Send();
			return true;
		} else {
			return false;
		}
	}
	
	public function notReplacedDamagedItemsCount(){
		$count = 0;
		foreach ($this->damaged_ind_items as $item){
			if (!$item->replaced){
				$count++;
			}
		}
		
		return $count;
	}
	
	
	public function notReturnedDamagedItemsCount(){
		$count = 0;
		foreach ($this->damaged_ind_items as $item){
			if (!$item->customer_return_id){
				$count++;
			}
		}
	
		return $count;
	}
	
	
	public function notReorderedDamagedItemsCount(){
		$count = 0;
		foreach ($this->damaged_ind_items as $item){
			if (!$item->reordered_from_supplier){
				$count++;
			}
		}
	
		return $count;
	}
	

	/**
	 * returns items table used for customer notes section
	 * items are combine according to their status 
	 */
	public function getItemStatsTableNotes(){
		$items = array();

		$this->setItemsFakeStatus(true);
		
		if ($this->return){
			foreach ($this->returns as $item){
				$items[$item->item_id.$item->getStatus()][] = $item;
			}
		} else {
			foreach ($this->ind_items as $item){
				$items[$item->item_id.$item->getStatus()][] = $item;
			}
		}
		$controller = new CustomerController('');
		return $controller->renderPartial('customer/_shipment_item_stats_table_notes', array(
				'shipment'=>$this,
				//'s_actions'=>$s_actions,
				'items'=>$items,
		), true);
	}
	
	/**
	 * 
	 * @param bool $all if true - use all items, if false - only active ones
	 */
	public function setItemsFakeStatus($all = false) {
		$items = $all ? $this->ind_items : $this->active_ind_items;
		$all_items = array();
		foreach ($items as $item) {
			$all_items[$item->customer_order_items_id][] = $item;
		};
		
		foreach ($all_items as $rows) {
			Individualitem::setItemsFakeStatus($rows);
		}
		
		$occurs = array();
		foreach ($items as $item){
			if (!isset($occurs[$item->item_id])){
				$occurs[$item->item_id] = 1;
			} else {
				$occurs[$item->item_id]++;
			}
			if ($occurs[$item->item_id] <= Item::getAvailableForShippingQty($item->item_id)){
				$item->_available_for_shipping = true;
			}
		}
	}
	
	public function getActionButtonsStr(){
		// all available statuses with actions they should be displayed with
		$local_pickup = $this->customer_order->shipping_method == CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP;
		if ($this->readyForShipping()){
			if ($local_pickup){
				$send_for_shipping = 'schedule_pickup';
			} else {
				$send_for_shipping = 'send_for_bidding';
			}
		} else {
			$send_for_shipping = null;
		}  
		$statuses = array(
			CustomerOrderShipment::$STATUS_PENDING => array(
					$send_for_shipping,
					'cancel',
					'scan_as_out',
					'edit_shipping_info'
			),
			CustomerOrderShipment::$STATUS_BIDDING => array(
					'requeue',
			),
			CustomerOrderShipment::$STATUS_SHIPPER_PICKUP => array(
					'requeue',
					'set_tracking_number',
			),
			CustomerOrderShipment::$STATUS_COMPLETED => array(
					$local_pickup || $this->claim ? null : 'mark_as_damaged',
			),
			CustomerOrderShipment::$STATUS_PICKUP => array(
					'requeue',
					'scan_as_out_customer_pickup'
			),
		);
		
		$actions = isset($statuses[$this->status]) ? $statuses[$this->status] : array();
		$actions[] = 'expand_order_details';
		$buttons = array();
		foreach ($actions as $action){
			switch ($action){
				case 'scan_as_out':
                    if ($this->readyForShipping()){
                        $buttons[] = '<input type="button" class="scan_out_shipment" rel="'.$this->id.'" value="Scan As Out" />';
                    }
				break;
			
				case 'send_for_bidding': $buttons[] = '<input type="button" class="send_for_bidding_shipment" rel="'.$this->id.'" value="Send For Bidding" />';
				break;
			
				case 'schedule_pickup': $buttons[] = '<input type="button" class="schedule_pickup" rel="'.$this->id.'" value="Schedule Customer Pickup" />';
				break;
			
				case 'mark_as_damaged': (!$this->return ? $buttons[] = '<input type="button" class="damage" rel="'.$this->id.'" value="Mark As Damaged" />' : '');
				break;
			
				case 'requeue': $buttons[] = (!$this->return ? '<input type="button" class="requeue" rel="'.$this->id.'" value="Requeue" />' 
						: '<input type="button" class="cancel_return" rel="'.$this->id.'" value="Cancel Return" />');
				break;
			
				case 'cancel': $buttons[] = '<input type="button" class="cancel_shipment" rel="'.$this->id.'" value="Cancel" />';
				break;
				
				case 'expand_order_details': $buttons[] = '<input type="button" class="order_expand_shipment_details" rel="'.$this->customer_order_id.'" value="Expand Order Details" />';
				break;
				
				case 'edit_shipping_info': $buttons[] = '<br />'.$this->customer_order->getEditShippingButtonCode();
				break;

				case 'scan_as_out_customer_pickup':
                    if ($this->readyForShipping()){
                        $buttons[] = '<input type="button" class="scan_out_customer_pickup" rel="'.$this->id.'" value="Scan As Out" />';
                    }
                break;
			}
		}

        if ($this->customer_order->verificationRequired()){
            $buttons[] = '<input type="button" class="order_verification_request" rel="'.$this->customer_order_id.'" value="Verification Request" />';
            $buttons[] = '<input type="button" rel="'.$this->customer_order_id.'" class="order_verify" value="Verified" />';
        }

		return implode('', $buttons);
	}
	
	public function getCompletedShipmentActionsStr(){
		$str = '';
		$lines = array();
		foreach ($this->claims as $claim){
			$lines[] ='<span class="shipment_damaged">Damaged on '.date('M. d, Y', strtotime($claim->claim_date)).'</span>';
		}
		$str .= implode('<br />', $lines);
		if($this->total_scanned_out_ind_items > $this->total_damaged_ind_items) {
			$str .= '<input type="button" class="damage" rel="'.$this->id.'" value="Mark as Damaged" />';
		}
		
		if (Yii::app()->user->isAdmin()){
			$str .= '<br /><input type="button" class="authorize_return" rel="'.$this->id.'" value="Authorize Return" />';
		}
		
		return $str;
	}
	
	/**
	 * returns status with additional info like tracking number and website or date when shipment was damaged
	 */
	public function getStatusExt(){
		$str = $this->status;
		$p_time = strtotime($this->pickup_date);
		$add = array();
		if ($p_time>0){
			$add[] = 'Pickup Date: '.date('M. d, Y', $p_time);
		}
		if ($this->tracking_number){
			$add[] = 'Tracking Number: '.$this->tracking_number;
			$add[] = 'Tracking Website: '.$this->tracking_website;
		}
		
		if ($this->claim){
			$add[] = 'Damaged on: '.date('M. d, Y', strtotime($this->claim_date));
		}

		if (count($add)){
			$str .= '<br /><br />'.implode('<br />', $add);
		}
		
		return $str;
	}
	
	public function getClaimActionsStr(){
		$str = '';
		if (Yii::app()->user->isAdmin()){
			if ($this->notReplacedDamagedItemsCount()){
				$str .= '<input type="button" id="replace_'.$this->id.'" value="Replace items" class="replace" rel="'.$this->id.'"/><br />
				<input type="button" value="Save" id="replace_save_'.$this->id.'" class="replace_save" rel="'.$this->id.'"/>
				<input type="button" value="Cancel" id="replace_cancel_'.$this->id.'" class="replace_cancel" rel="'.$this->id.'"/>';
			}
			
			if ($this->notReorderedDamagedItemsCount()){
				$str .= '<input type="button" id="supplier_order_'.$this->id.'" value="Order from Supplier" class="supplier_order" rel="'.$this->id.'"/><br />';
			}
			$str .= '<input type="button" id="refund_'.$this->id.'" value="Refund" class="refund" rel="'.$this->id.'"/><br />
				<input type="button" value="Save" id="refund_save_'.$this->id.'" class="refund_save" rel="'.$this->id.'"/>
				<input type="button" value="Cancel" id="refund_cancel_'.$this->id.'" class="refund_cancel" rel="'.$this->id.'"/>';
			
			if ($this->notReturnedDamagedItemsCount()){
				$str .= '<input type="button" id="authorize_return_'.$this->id.'" value="Authorize return" class="authorize_return" rel="'.$this->id.'"/><br />
				<input type="button" value="Save" id="authorize_return_save_'.$this->id.'" class="authorize_return_save" rel="'.$this->id.'"/>
				<input type="button" value="Cancel" id="authorize_return_cancel_'.$this->id.'" class="authorize_return_cancel" rel="'.$this->id.'"/>';
			}
			
			if (!$this->claim_resolved){
				$str .= '<input type="button" class="resolve" value="Resolve" rel="'.$this->id.'" /><br />';
			}
		}
		
		if (!$this->claim_resolved){
			$str .= '<input type="button" class="upload_doc" value="Upload Doc" rel="'.$this->id.'" /><br />';
		}
		
		$str .= '<input type="button" class="claim_notes" value="Notes" rel="'.$this->id.'" />';
		
		return $str;
	}
	
	public function attachTo($main_shipment_id){
		$main_shipment = CustomerOrderShipment::model()->findByPk($main_shipment_id);
		if (!$main_shipment){
			return false;
		}
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_shipment_id', $this->id);
		Individualitem::model()->updateAll(array(
				'customer_order_shipment_id'=>$main_shipment_id, 
				'prev_shipment_id'=>$this->id
		), $criteria);
		
		$this->attached_to = $main_shipment_id;
		$this->status = self::$STATUS_ATTACHED;

		$main_shipment->combo = 1;
		
		if ($this->save() && $main_shipment->save()){
			$this->customer_order->updateShippingStatus();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return html list of download links
	 */
	public function getClaimDocsListStr(){
		$links = array();
		foreach ($this->claim_docs as $doc){
			$links[] = '<a target="_blank" href="'.Yii::app()->getBaseUrl(true).'/index.php/customershipper/download_claim_doc/?id='.$doc->id.'">'.$doc->getFilename().'</a>';
		}
		
		return implode('<br />', $links);
	}
	
	/**
	 * This function and few next ones are about saving scanned boxes info in the session.
	 * When user scanned some boxes but not all of them we store pending scan boxes in the session in multi-level array
	 * $boxes_to_scan_out[shipment_id][item_id][box_i][qty]
	 * Another session array var is used to store timestamp when the shipment was added to the session
	 * $shipment_scan_out_timestamp[shipment_id] = $timestamp
	 */
	public function initSessionBoxScanOutInfo(){
		$box_array = $this->getSessionBoxesScanOutArray();
		$shipment_timestamp = $this->getSessionShipmentScanOutTimestampArray();
		if (isset($box_array[$this->id])){
			// already initialized
			if ($shipment_timestamp[$this->id] < strtotime($this->modified_on)){
				unset($box_array[$this->id]);
				unset($shipment_timestamp[$this->id]);
			} else {
				return true;
			}
		}
		$items = $this->getRelated('active_ind_items', false, array('with'=>'items', 'together'=>true));
		
		foreach ($items as $item){
			if ($item->is_replacement && $item->replacement_boxes){
				$boxes = explode(',', $item->replacement_boxes);
			} else {
				$boxes = array();
				for ($i=1; $i<=$item->items->box_qty; $i++){
					$boxes[] = $i;
				}
			}
			foreach ($boxes as $box){
				if (!isset($box_array[$this->id][$item->item_id][$box])){
					$box_array[$this->id][$item->item_id][$box] = 0;
				}
				
				$box_array[$this->id][$item->item_id][$box]++;
			}
		}
		
		$shipment_timestamp[$this->id] = time();
		
		$this->setSessionBoxesScanOutArray($box_array);
		$this->setSessionShipmentScanOutTimestampArray($shipment_timestamp);
		return true;
	}
	
	public function getSessionBoxesScanOutArray(){
		return Yii::app()->user->getState('boxes_to_scan_out', array());
	}

	private function setSessionBoxesScanOutArray($box_array){
		return Yii::app()->user->setState('boxes_to_scan_out', $box_array);
	}
	
	private function getSessionShipmentScanOutTimestampArray(){
		return Yii::app()->user->getState('shipment_scan_out_timestamp', array());
	}
	
	private function setSessionShipmentScanOutTimestampArray($arr){
		return Yii::app()->user->setState('shipment_scan_out_timestamp', $arr);
	}
	
	/**
	 * Returns true if we have the box given in the pending scan out boxes list, false otherwise
	 * @param int $item_id
	 * @param int $box_i
	 */
	public function needToScanOutBox($item_id, $box_i){
		$this->initSessionBoxScanOutInfo();
		$box_array = $this->getSessionBoxesScanOutArray();
		if (isset($box_array[$this->id][$item_id][$box_i]) && $box_array[$this->id][$item_id][$box_i]>0){
			return true;
		}
		
		return false;
	}
	
	public function setSessionBoxScannedOut($item_id, $box){
		$box_array = $this->getSessionBoxesScanOutArray();
		if (!isset($box_array[$this->id][$item_id][$box])){
			return false;
		}
		
		$box_array[$this->id][$item_id][$box]--;
		if ($box_array[$this->id][$item_id][$box] == 0){
			unset($box_array[$this->id][$item_id][$box]);
		}
		
		if (!count($box_array[$this->id][$item_id])){
			unset($box_array[$this->id][$item_id]);
		}
		
// 		if (!count($box_array[$this->id])){
// 			unset($box_array[$this->id]);
// 		}
		
		$this->setSessionBoxesScanOutArray($box_array);
		
		return true;
	}
	
	public function getPendingScanOutBoxQty(){
		$box_array = $this->getSessionBoxesScanOutArray();
		$box_array = $box_array[$this->id];
		$res = 0;
		foreach ($box_array as $item){
			foreach ($item as $box=>$qty){
				$res += $qty;
			}
		}
		
		return $res;
	}
	
	public function beforeSave(){
		$this->modified_on = date('Y-m-d H:i:s');
		return parent::beforeSave();
	}
	
	public function updateModifiedTime(){
		$this->modified_on = date('Y-m-d H:i:s');
		$this->save();
	}

	/**
	 *
	 * @param int $type
	 * @param float $amount
	 * @param string $date current date is used if nothing given
	 */
	public function addCustomerPayment($type, $amount, $date = null){
		$time = $date ? strtotime($date) : time();
	
		$payment 				= new CustomerPayment();
		$payment->customer_id 	= $this->customer_order->customer_id;
		$payment->type 			= $type;
		$payment->item_id 		= $this->id;
		$payment->amount 		= $amount;
		$payment->date 			= date('Y-m-d H:i:s', $time);
	
		return $payment->save();
	}

    /**
     * Returns total amount paid by customer as additional shipping payment (beyond point)
     */
    public function getTotalAdditionalPaymentAmount(){
        // The problem is that some shipments were paid using CustomerPayment and some not
        // so if we have payments in CustomerPayment - return them. If not - return additional_payment_amount field

        if ($this->total_additional_paid){
            return $this->total_additional_paid;
        } else {
            return $this->additional_payment_amount && $this->additional_payment_done ? $this->additional_payment_amount : 0;
        }
    }

    public function create3rdPartyBid($shipment_company, $rate_quote){
        $bid = new CustomerOrderShipmentBid();
        $bid->shipment_id       = $this->id;
        $bid->auto_generated    = 1;
        $bid->delivery_service  = $shipment_company;
        $bid->rate_quote        = $rate_quote;
        $bid->status            = CustomerOrderShipmentBid::$STATUS_PENDING;
        $bid->selected          = 1;
        $bid->date              = date('Y-m-d H:i:s');
        $bid->save();
    }


    public function getClaimsListStr(){
        $arr = array();
        foreach ($this->claims as $claim){
            $arr[] = 'Damaged on '.date('M. d, Y', strtotime($claim->claim_date));
        }

        return implode('<br />', $arr);
    }
}