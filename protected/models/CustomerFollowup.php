<?php

/**
 * This is the model class for table "customer_followup".
 *
 * The followings are the available columns in table 'customer_followup':
 * @property string $id
 * @property string $sales_person_id
 * @property string $customer_id
 * @property string $followup_time
 * @property string $created_on
 * @property string $status
 */
class CustomerFollowup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerFollowup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_followup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sales_person_id, customer_id, followup_time, created_on', 'required'),
			array('sales_person_id, customer_id', 'length', 'max'=>10),
			array('status', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sales_person_id, customer_id, followup_time, created_on, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer'=>array(self::BELONGS_TO, 'Customer', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sales_person_id' => 'Sales Person',
			'customer_id' => 'Customer',
			'followup_time' => 'Followup Time',
			'created_on' => 'Created On',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($customer_id = 0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sales_person_id',$this->sales_person_id,true);
		
		if ($customer_id != 0){
			$this->customer_id = $customer_id;
		}
		
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('followup_time',$this->followup_time,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchToday()
	{
		$startTime = strtotime(date(("Y-m-d"), time()));
		$endTime = $startTime + 24 * 3600;
		
		$criteria=new CDbCriteria;
		//$criteria->compare('sales_person_id', $sales_person_id);
		$criteria->compare('followup_time', '>'.date(("Y-m-d H:i:s"), $startTime));
		$criteria->compare('followup_time', '<'.date(("Y-m-d H:i:s"), $endTime));
		// $criteria->compare('status', 'Followed');
		// $criteria->compare('status', 'Alerted', true, "OR");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function hasTodayFollowup($sales_person_id){
		$startTime = strtotime(date(("Y-m-d"), time()));
		$endTime = $startTime + 24 * 3600;
		
		$criteria=new CDbCriteria;
		$criteria->compare('sales_person_id', $sales_person_id);
		$criteria->compare('followup_time', '>'.date(("Y-m-d H:i:s"), $startTime));
		$criteria->compare('followup_time', '<'.date(("Y-m-d H:i:s"), $endTime));
		
		$followup = CustomerFollowup::model()->findAll($criteria);
		return (count($followup) > 0);
	}
	
	public function todayFollowupList($sales_person_id){
		$startTime = strtotime(date(("Y-m-d"), time()));
		$endTime = $startTime + 24 * 3600;
		
		$criteria=new CDbCriteria;
		$criteria->compare('sales_person_id', $sales_person_id);
		$criteria->compare('followup_time', '>'.date(("Y-m-d H:i:s"), $startTime));
		$criteria->compare('followup_time', '<'.date(("Y-m-d H:i:s"), $endTime));
		$criteria->compare('status', 'Followed');
		$criteria->compare('status', 'Alerted', true, "OR");
		$followupList = CustomerFollowup::model()->findAll($criteria);
		
		$result = array();
		foreach ($followupList as $followupModel){
			$customer_id = $followupModel->customer_id;
			$customerModel = Customer::model()->findByPk($customer_id);
			$customer_name = $customerModel->first_name." ".$customerModel->last_name;
			
			$followup = array(
				'customer_id' 	=>	$customer_id,
				'customer_name'	=>	$customer_name,
				'followup_time' =>	strtotime($followupModel->followup_time),
				'status' =>	$followupModel->status,
			);
			$result[$followupModel->id] = $followup;
		}
		
		return $result;
	}
	
	public function search_sales_followup($sales_person_id){
		$startTime = strtotime(date(("Y-m-d"), time()));
		$endTime = $startTime + 24 * 3600;
		
		$criteria=new CDbCriteria;
		$criteria->compare('sales_person_id', $sales_person_id);
		$criteria->compare('followup_time', '>'.date(("Y-m-d H:i:s"), $startTime));
		$criteria->compare('followup_time', '<'.date(("Y-m-d H:i:s"), $endTime));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getCustomerName(){
		$customerInfo = Customer::model()->findByPk($this->customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		return $customerName;
	}
}