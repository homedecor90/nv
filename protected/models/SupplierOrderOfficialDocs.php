<?php

/**
 * This is the model class for table "supplier_order_official_docs".
 *
 * The followings are the available columns in table 'supplier_order_official_docs':
 * @property string $id
 * @property string $supplier_order_id
 * @property string $url
 */
class SupplierOrderOfficialDocs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierOrderOfficialDocs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_order_official_docs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_order_id, url', 'required'),
			array('supplier_order_id', 'length', 'max'=>10),
			array('url', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, supplier_order_id, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'supplier_order_id' => 'Supplier Order',
			'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_order_id',$this->supplier_order_id,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
		
	public function getOfficialDocumentsTable(){
		$i = 0;
		
		$strDocuments = '<table id="official_documents" class="items">'
						.'<thead>'
						.'<th>No.</th>'
						.'<th>Amount Paid</th>'
						.'<th>Name</th>'
						.'<th>Uploaded Date</th>'
						.'<th>Action</th>'
						.'</thead>';
						
		$criteriaDocuments = new CDbCriteria;
		$criteriaDocuments->compare('supplier_order_id', $this->supplier_order_id, true);
		$arrDocuments = SupplierOrderOfficialDocs::model()->findAll($criteriaDocuments);
		
		$supplierOrderModel = SupplierOrder::model()->findByPk($this->supplier_order_id);
		if ($supplierOrderModel->advanced_paid_attach_file_path != ""){
            $files = explode('||', $supplierOrderModel->advanced_paid_attach_file_path);
            $strButton = '';
			$i++;
			foreach($files as $j=>$file){
                // $strButton = '<a target="blank" href="'.Yii::app()->getBaseUrl(true)."/".$file.'">View</a>';
                $strButton .= '<a target="blank" href="'.Yii::app()->getBaseUrl(true)."/supplierorder/adv_paid_doc/?order_id=".$this->supplier_order_id.'&doc='.($j+1).'">View</a>';
			}
			$strDocuments .= '<tr class="row">'
							.'<td>'.$i.'</td>'
							.'<td>$'.$supplierOrderModel->advanced_paid_amount.'</td>'
							.'<td>30% advance payment</td>'
							.'<td>'.date("M. d, Y", strtotime($supplierOrderModel->advanced_paid_date)).'</td>'
							.'<td>'.$strButton.'</td>'
							.'</tr>';
		}
		
		if ($supplierOrderModel->full_paid_attach_file_path != ""){
			$files = explode('||', $supplierOrderModel->full_paid_attach_file_path);
            $strButton = '';
			$i++;
			foreach($files as $j=>$file){
                // $strButton .= '<a target="blank" href="'.Yii::app()->getBaseUrl(true)."/".$file.'">View</a><br />';
                $strButton .= '<a target="blank" href="'.Yii::app()->getBaseUrl(true)."/supplierorder/full_paid_doc/?order_id=".$this->supplier_order_id.'&doc='.($j+1).'">View</a>';
			}            
			$total = $supplierOrderModel->supplier_order_container->terms == 'FOB' 
						? SupplierOrder::getSupplierOrderTotalFobPrice($supplierOrderModel->id)
						: SupplierOrder::getSupplierOrderTotalExwPrice($supplierOrderModel->id);
			$paid = $total - $supplierOrderModel->advanced_paid_amount;
			$strDocuments .= '<tr class="row">'
							.'<td>'.$i.'</td>'
							.'<td>$'.$paid.'</td>'
							.'<td>Final payment</td>'
							.'<td>'.date("M. d, Y", strtotime($supplierOrderModel->full_paid_date)).'</td>'
							.'<td>'.$strButton.'</td>'
							.'</tr>';
            
		}
		
		foreach ($arrDocuments as $j=>$document){
			$i++;
			$strButton = '<a target="blank" href="'.Yii::app()->getBaseUrl(true).'/index.php/supplierorder/final_doc/?order_id='.$supplierOrderModel->id.'&doc='.($j+1).'">View</a>';
			// $strButton = '<a target="blank" href="'.Yii::app()->getBaseUrl(true)."/".$document->url.'">View</a>';
			$strDocuments .= '<tr class="row">'
							.'<td>'.$i.'</td>'
							.'<td></td>'
							.'<td>Official PI/PL</td>'
							.'<td>'.date("M. d, Y", strtotime($document->created_on)).'</td>'
							.'<td>'.$strButton.'</td>'
							.'</tr>';
		}
		
		if ($i == 0){
			$strDocuments .= '<tr class="no_result"><td colspan="4">No Result</td></tr>';
		}
		
		$strDocuments .= '</table>';
		
		return $strDocuments;
	}
}