<?php

/**
 * This is the model class for table "customer_newsletter_letter".
 *
 * The followings are the available columns in table 'customer_newsletter_letter':
 * @property integer $id
 * @property integer $newsletter_id
 * @property integer $customer_id
 * @property string $email
 * @property string $subject
 * @property string $text
 * @property integer $sent
 * @property string $date_sent
 */
class CustomerNewsletterLetter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerNewsletterLetter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_newsletter_letter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('newsletter_id, customer_id, email', 'required'),
			array('newsletter_id, customer_id, sent', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>100),
			array('subject', 'length', 'max'=>200),
			array('date_sent', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, newsletter_id, customer_id, email, subject, text, sent, date_sent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'newsletter_id' => 'Newsletter',
			'customer_id' => 'Customer',
			'email' => 'Email',
			'subject' => 'Subject',
			'text' => 'Text',
			'sent' => 'Sent',
			'date_sent' => 'Date Sent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('newsletter_id',$this->newsletter_id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('sent',$this->sent);
		$criteria->compare('date_sent',$this->date_sent,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}