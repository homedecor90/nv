<?php
class SupplierBankInfo extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_bank_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_id, account_name, account_address, account_number, routing_number, bank_name, bank_address', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, supplier_id, account_name, account_address, account_number, routing_number, bank_name, bank_address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user'=>array(self::BELONGS_TO, 'YumUser', 'supplier_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_name' => 'Account Name',
			'account_address' => 'Account Address',
			'account_number' => 'Account Number',
			'routing_number' => 'Routing Number',
			'bank_name' => 'Bank Name',
			'bank_address' => 'Bank Address',
		);
	}
    
    public function getCountNotAccepted(){
        $criteria = new CDbCriteria();
        $criteria->compare('accepted', 0);
        return SupplierBankInfo::model()->count($criteria);
    }
    
    public function searchNotApproved(){
		$criteria=new CDbCriteria;

		$criteria->compare('accepted',0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }
}