<?php

/**
 * This is the model class for table "customer_order".
 *
 * The followings are the available columns in table 'customer_order':
 * @property string $id
 * @property string $sales_person_id
 * @property string $customer_id
 * @property string $shipping_method
 * @property string $status
 * @property string $created_on
 * @property string $discounted
 * @property double $grand_total_price
 * @property double $discounted_total
 * @property double $profit
 * @property int $add_sales_tax
 * @property string payment_processor
 * @property boolean payment_approved
 *
 * Getters/setters:
 * @property string $description
 *
 * Relations:
 * @property CustomerOrderBillingInfo $billing_info
 * @property Customer $customer
 * @property CustomerOrderShipment[] $shipments
 * @property CustomerOrderItems[] $order_items
 * @property Individualitem[] $individual_items
 * @property CustomerOrder[] $replacements
 * @property CustomerOrderRefund[] $refunds
 *
 * Stat Relations:
 * @property double $total_refunded
 * @property double $total_shipping_edited_paid
 */
class CustomerOrder extends CActiveRecord
{

    public static $STATUS_PENDING	= "Pending";

    const STATUS_PENDING = 'Pending';
    const STATUS_COMPLETED = "Completed";
    const STATUS_SOLD = "Sold";
    const STATUS_LOST = "Lost Sale";
    const STATUS_DEAD = "Dead";
    /**
     * @var string
     * @deprecated
     */
    public static $STATUS_COMPLETED = "Completed";
	public static $STATUS_SOLD 		= "Sold";
	public static $STATUS_LOST 		= "Lost Sale";
	public static $STATUS_DEAD 		= "Dead";

    public static $SHIPPING_STATUS_PENDING      = "Pending";
    public static $SHIPPING_STATUS_CANCELLED    = "Cancelled";
    public static $SHIPPING_STATUS_CHARGEBACK   = "Chargeback";
    public static $SHIPPING_STATUS_BIDDING   	= "Bidding";
    public static $SHIPPING_STATUS_PICKUP   	= "Pickup";
    public static $SHIPPING_STATUS_SCANNED_OUT 	= "Scanned OUT";
    public static $SHIPPING_STATUS_ATTACHED	 	= "Attached";
    public static $SHIPPING_STATUS_COMPLETED 	= "Completed";
    
    const SHIPPING_METHOD_LOCAL_PICKUP 		= 'Local Pickup';
    const SHIPPING_METHOD_SHIPPING 			= 'Shipping';
    const SHIPPING_METHOD_SHIPPED_FROM_LA 	= 'Shipped from Los Angeles';
    const SHIPPING_METHOD_CANADA_SHIPPING 	= 'Canada Shipping';
    
    const ORDER_DISCOUNTED = 'Discounted';
    const ORDER_NOT_DISCOUNTED = 'Not Discounted';

    const CC_MAX_CHECKOUT = 3000;

    const SHIPPING_PROFIT_RATE = 0.15;

	public static $ORDER_SALT = "asdf793khksad98"; //salt for PublicCheckoutURL() if change, than old checkout url not working
	
	public $_total = null;
	public $state;
	private $_tax_percent = null;
    private $_total_paid_amount = null;
	public $_filter_billing_name = '';
    private $_description;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CustomerOrder the static model class
     */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order';
	}

	/**
	tio * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sales_person_id, customer_id, shipping_method, status, created_on, discounted, grand_total_price, discounted_total', 'required'),
            array('payment_processor', 'required', 'on' => 'importOscOrder'),
			array('grand_total_price, discounted_total, add_sales_tax, custom_discount, raw_discounted, profit', 'numerical'),
			array('sales_person_id, customer_id', 'length', 'max'=>10),
			array('payment_processor', 'length', 'max'=>100),
			array('shipping_method, status, discounted', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sales_person_id, customer_id, shipping_method, status, created_on, state, discounted, grand_total_price, discounted_total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'billing_info'=>array(self::HAS_ONE, 'CustomerOrderBillingInfo', 'customer_order_id'),
            'customer'=>array(self::BELONGS_TO, 'Customer', 'customer_id'),
			'shipments'=>array(self::HAS_MANY, 'CustomerOrderShipment', 'customer_order_id', 'order'=>'shipments.id'),
			'order_items'=>array(self::HAS_MANY, 'CustomerOrderItems', 'customer_order_id'),
            'individual_items' => array(
                self::HAS_MANY,
                'Individualitem',
                array('id' => 'customer_order_items_id'),
                'through' => 'order_items',
            ),
			'replacements'=>array(self::HAS_MANY, 'CustomerOrder', 'replacement_order'),
			'refunds'=>array(self::HAS_MANY, 'CustomerOrderRefund', 'customer_order_id'),
            'total_refunded'=>array(
                self::STAT,
                'CustomerOrderRefund',
                'customer_order_id',
                'select'=>'ROUND(SUM(amount),2)'
            ),
            'total_shipping_edited_paid'=>array(
                self::STAT,
                'CustomerPayment',
                'item_id',
                'condition'=>'type='.CustomerPayment::TYPE_ORDER_SHIPPING_EDITED,
                'select'=>'ROUND(SUM(amount),2)'
            ),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sales_person_id' => 'Sales Person',
			'state' => 'State',
			'customer_id' => 'Customer',
			'shipping_method' => 'Shipping Method',
			'status' => 'Status',
			'created_on' => 'Created On',
			'discounted' => 'Discounted',
			'grand_total_price' => 'Grand Total Price',
			'discounted_total' => 'Discounted Total',
			'add_sales_tax' => 'Add Sales Tax',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sales_person_id',$this->sales_person_id,true);
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('shipping_method',$this->shipping_method,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('discounted',$this->discounted,true);
		$criteria->compare('grand_total_price',$this->grand_total_price);
		$criteria->compare('discounted_total',$this->discounted_total);
        
        $criteria->order = 'id DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchByDates($date_from, $date_to){
		$criteria=new CDbCriteria;
		$criteria->order = 't.id DESC';
		$criteria->compare('status', self::$STATUS_SOLD);
        $criteria->compare('shipping_status', '<>'.CustomerOrder::$SHIPPING_STATUS_CANCELLED);
        $criteria->compare('shipping_status', '<>'.CustomerOrder::$SHIPPING_STATUS_CHARGEBACK);

		if ($date_from && $date_to){
			$criteria->addCondition('sold_date >= "'.$date_from.'"');
			$criteria->addCondition('sold_date <= ADDDATE("'.$date_to.'", 1)');			
		}

        $criteria->with = array('billing_info');
        $criteria->together = true;
        $criteria->compare('billing_info.billing_state', $this->state, false);

        $per_page   = isset($_GET['per_page']) ? $_GET['per_page'] : 25;
        if ($per_page == 0){
            $pagination = false;
        } else {
            $pagination = array(
                'PageSize' => $per_page
            );
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'Pagination' => $pagination
		));
	}
	
	public function searchPendingApproval()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sales_person_id',$this->sales_person_id,true);
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('shipping_method',$this->shipping_method,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('discounted',$this->discounted,true);
		$criteria->compare('grand_total_price',$this->grand_total_price);
		$criteria->compare('discounted_total',$this->discounted_total);
		
		$criteria->compare('approved', 0);
		
        $criteria->order = 'id DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
    public function searchSold()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sales_person_id',$this->sales_person_id,true);
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('shipping_method',$this->shipping_method,true);
		$criteria->compare('status',CustomerOrder::$STATUS_SOLD,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('discounted',$this->discounted,true);
		$criteria->compare('grand_total_price',$this->grand_total_price);
		$criteria->compare('discounted_total',$this->discounted_total);
//         $criteria->addCondition('UNIX_TIMESTAMP(sold_date)>0', 'and');
//         $criteria->order = 'sold_date DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
	public function searchChargebacks()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('sales_person_id',$this->sales_person_id,false);
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('shipping_method',$this->shipping_method,false);
		$criteria->compare('status',$this->status,false);
		$criteria->compare('created_on',$this->created_on,false);
		$criteria->compare('discounted',$this->discounted,false);
		$criteria->compare('grand_total_price',$this->grand_total_price);
		$criteria->compare('discounted_total',$this->discounted_total);
        $criteria->addCondition('UNIX_TIMESTAMP(sold_date)>0', 'and');
        $criteria->order = 'chargeback_date DESC';
		
		$criteria->compare('shipping_status', self::$SHIPPING_STATUS_CHARGEBACK);
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchCancelled()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('sales_person_id',$this->sales_person_id,false);
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('shipping_method',$this->shipping_method,false);
		$criteria->compare('status',$this->status,false);
		$criteria->compare('created_on',$this->created_on,false);
		$criteria->compare('discounted',$this->discounted,false);
		$criteria->compare('grand_total_price',$this->grand_total_price);
		$criteria->compare('discounted_total',$this->discounted_total);
        $criteria->addCondition('UNIX_TIMESTAMP(sold_date)>0', 'and');
        $criteria->order = 'cancellation_date DESC';
		
		$criteria->compare('shipping_status', self::$SHIPPING_STATUS_CANCELLED);
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
    public function searchSoldPendingShipping()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        // echo '<!--search '.$this->customer_id.'-->';
		$criteria=new CDbCriteria;
        $criteria->compare('approved', 1);
/*
        die($this->_filter_billing_name);
*/
        $criteria->compare('billing_info.billing_name', $this->_filter_billing_name, true);
        $criteria->compare('billing_info.billing_last_name', $this->_filter_billing_name, true, 'or');
        $criteria->compare('billing_info.shipping_name', $this->_filter_billing_name, true, 'or');
        $criteria->compare('billing_info.shipping_last_name', $this->_filter_billing_name, true, 'or');
        $criteria->compare('shipping_status',CustomerOrder::$SHIPPING_STATUS_PENDING);
        $criteria->compare('combined_with', 0);
        $criteria->addCondition('UNIX_TIMESTAMP(t.sold_date)>0', 'and');
        $criteria->order = 't.sold_date DESC';
/*
        $criteria->with = array('shipments', 'shipments.ind_items');
        
*/
		$criteria->with = array('billing_info');
		$criteria->together = true;
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
	public static function getCustomerOrderItems($customer_order_id){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$strItems = '<table class="item_list">'
			.'<thead>'
			.'<th class="item_code">Item Code</td>'
			.'<th class="color">Color</td>'
			.'<th class="price">Price</td>'
			.'<th class="shipping_price">Shipping</td>'
			.'<th class="eta">E.T.A</td>'
			.'<th class="quantity">Qty</td>'
			.'</thead>';
			
		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item_attributes['item_id'];
			$item_info = Item::model()->findByPk($item_id);
			$item_eta = Item::getStatusString($item_id, $item_attributes['quantity']);
			
			$strItems .= "<tr>";
			$strItems .= "<td class='item_code'>".$item_info->item_code."</td>";
			$strItems .= "<td class='color'>".$item_info->color."</td>";
			$strItems .= "<td class='price'>".$item_attributes['sale_price']."</td>";
			
			if ($customerOrder->shipping_method == "Shipping") {
				$shipping_price = $item_info->shipping_price;
			} else if ($customerOrder->shipping_method == "Local Pickup") {
				$shipping_price = $item_info->local_pickup;
			} else if ($customerOrder->shipping_method == "Shipped from Los Angeles") {
				$shipping_price = $item_info->la_oc_shipping;
			} else if ($customerOrder->shipping_method == "Canada Shipping") {
				$shipping_price = $item_info->canada_shipping;
			}
			
			$strItems .= "<td class='shipping_price'>".$shipping_price."</td>";
			$strItems .= "<td class='eta'>".$item_eta."</td>";
			$strItems .= "<td class='quantity'>".$item_attributes['quantity']."</td>";			
			$strItems .= "</tr>";
		}

		$strItems .= "</table>";

		return $strItems;
	}
	
	// TODO: make all hese static methods non-static
	public static function getCustomerOrderItemsWithItemName($customer_order_id){
        $criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$strItems = '<table class="item_list" id="item_table_'.$customer_order_id.'">'
			.'<thead>'
			.'<th class="item_name">Item Name</td>'
			.'<th class="color">Color</td>'
			.'<th class="price">Price</td>'
			.'<th class="shipping_price">Shipping Price</td>'
			.'<th class="special_shipping_price">Special Shipping Price</td>'
			.'<th class="eta">E.T.A</td>'
			.'<th class="quantity">Qty</td>'
			.'<th class="custom_color">Custom Color</td>'
			.'<th class="item_total_price">Item Total Price</td>';
        if ($customerOrder->status == CustomerOrder::$STATUS_PENDING && $customerOrder->approved){
            $strItems.='<th class="item_edit">Edit</th>'
            .'<th class="item_delete">Delete</th>';
        }
		
        $strItems .= '</thead>';
			
		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item->item_id;
			$item_info = Item::model()->findByPk($item_id);
			
			$sale_price = $item->sale_price;
			$special_shipping_price = $item->special_shipping_price;
			if ($customerOrder->status != CustomerOrder::$STATUS_SOLD){
				$item_eta = Item::getStatusString($item_id, $item_attributes['quantity']);
			} else {
//				$item_eta = CustomerOrderItems::model()->getEtaStringById($item->id);
				$item_eta = $item->getItemStatusList();
			}
			$quantity = $item->quantity;
			
			if ($customerOrder->shipping_method == "Shipping") {
				$shipping_price = $item_info->shipping_price;
			} else if ($customerOrder->shipping_method == "Local Pickup") {
				$shipping_price = $item_info->local_pickup;
			} else if ($customerOrder->shipping_method == "Shipped from Los Angeles"){
				$shipping_price = $item_info->la_oc_shipping;
			} else if ($customerOrder->shipping_method == "Canada Shipping"){
				$shipping_price = $item_info->canada_shipping;
			}
			
			// $item_total_price = ($sale_price + $shipping_price) * $quantity + $special_shipping_price;
			$item_total_price = $item->getTotalPrice($customerOrder);
            
			$strItems .= "<tr>";
			$strItems .= "<td class='item_name'>".$item_info->item_name."</td>";
			$strItems .= "<td class='color'>".$item_info->color."</td>";
			$strItems .= "<td class='price'>".$sale_price."</td>";
			$strItems .= "<td class='shipping_price'>".$shipping_price."</td>";
			$strItems .= "<td class='special_shipping_price'>".$special_shipping_price."</td>";
			$strItems .= "<td class='eta' id='eta_".$item->id."'>".$item_eta."</td>";
			$strItems .= "<td class='quantity'><span id='qty_".$item->id."'>".$quantity."</span><input type='text' class='new_qty_input' id='new_qty_".$item->id."' value='".$quantity."' /></td>";
			$strItems .= "<td class='custom_color'>".$item->custom_color."</td>";
			$strItems .= "<td class='item_total_price' id='item_total_".$item->id."'>".$item_total_price."</td>";
            if ($customerOrder->status == CustomerOrder::$STATUS_PENDING && $customerOrder->approved){
                $strItems .= '<td class="item_edit"><a href="#" data-itemid="'.$item->id.'" class="edit_old_item" id="edit_'.$item->id.'">Edit</a>
                                    <a href="#" data-itemid="'.$item->id.'" class="edit_save_old_item" id="edit_save_'.$item->id.'">Save</a>
                                    <a href="#" data-itemid="'.$item->id.'" class="edit_cancel_old_item" id="edit_cancel_'.$item->id.'">Cancel</a>
                                </td>';
                $strItems .= '<td class="item_delete"><a href="#" data-itemid="'.$item->id.'" class="delete_old_item" id="delete_'.$item->id.'">Delete</a></td>';
			}
            $strItems .= "</tr>";
		}

		$strItems .= "</table>";
		
		
		if (Yii::app()->user->isAdmin()){

//            foreach ($customerOrder->order_items as $orderItems) {
//                foreach ($customerOrder->individual_items as $item) {
//                    $strItems .= $item->id;
//                    if ($item->scanned_out_date == '0000-00-00 00:00:00')
//                        $scannedOut = $item->scanned_out_date;
//                    else $scannedOut = $item->scanned_out_date;
//                    $strItems .= "<br>Item (#{$item->id}). Scanned In: {$item->scanned_in_date}.
//                        Scanned Out: {$item->scanned_out_date}.
//                        Cbm: {$item->cbm}.
//                        Length: {$item->dimensions_length}.
//                        Width: {$item->dimensions_width}.
//                        Supply Shipping cost: {$item->supplyShippingCost}";
//                }
//                $strItems .= "<br>Flat shipping fee: {$orderItems->flatShippingFee}";
//            }

            //$strItems .= '<br>Profit by formula: ' . round($customerOrder->getProfit(), 2);
            //$strItems .= '<br>Shipping quote: ' . $customerOrder->getTotalShippingQuote();
            //$strItems .= '<br>Profit by formula: ' . round($customerOrder->getProfit(), 2);
            $strItems .= '<br>Profit by formula: ' . round($customerOrder->getProfit(), 2);
            $strItems .= self::getOrderProfitAfterItemsInGrid($customer_order_id);
		}
		
		return $strItems;
	}
	
	public function getProfitWithReplacementsAndRefunds(){
		$profit = $this->profit;
		// TODO: this also can be done with STAT relation like refunds
		foreach ($this->replacements as $replacement){
			$profit += $replacement->profit;
		}
		
		return $profit - $this->total_refunded;
	} 
	
	// TODO: make this function and the one above not static
	public static function getOrderProfitAfterItemsInGrid($customer_order_id){
		$strReturn = "<br />";
		$profit_title = 'Profit';
		$profit_val = 0;
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		if($customerOrder->status == self::$STATUS_SOLD || 
		   $customerOrder->status == self::$STATUS_PENDING){
			if($customerOrder->status == self::$STATUS_SOLD){
				$profit_title = 'Profit';
				$profit_val = $customerOrder->getProfitWithReplacementsAndRefunds();
			}
			if($customerOrder->status == self::$STATUS_PENDING){
				$profit_title = 'Anticipated Profit';
				$profit_val = self::getOrderAntProfit($customer_order_id);
			}
		} else {
			return '';
		}
		
		$replacement = $customerOrder->is_replacement ? ' (Replacement)' : '';
		$refund = $customerOrder->total_refunded ? ' (Refunded: $'.number_format($customerOrder->total_refunded, 2).')' : '';
		
		$strReturn .= '<div class="order_ant_profit" id="ant_profit_'.$customer_order_id.'">'.$profit_title.': $<span>'.number_format($profit_val, 2).$replacement.$refund.'</span></div>';
		$strReturn .= "<br />";

		return $strReturn;
	}
	
	public static function getOrderAntProfit($customer_order_id){
		$profit_val = 0;
		$ant_profit_wod = 0; //anticipated profit without Discounts
		//ANT PROFIT = (Sale Price)*.97 - (Cost Price + 100*item CBM) 
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		foreach ($items as $item){
			$item_info = Item::model()->findByPk($item->item_id);
			$ant_profit_wod += ($item->sale_price*0.97 - $item->fob_cost_price - 100*$item_info->cbm)*$item->quantity;
		}
		$discount = $customerOrder->calculateDiscountAmount(false);
		$profit_val = $ant_profit_wod-$discount;
			
		return $profit_val;
	}
	
	public static function getCustomerOrderItemsForInvoice($customer_order_id){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$strItems = '<table class="item_list">'
			.'<thead>'
			.'<th class="item_name">Item Name</td>'
			.'<th class="color">Color</td>'
			.'<th class="quantity">Qty</td>'
			.'<th class="special_shipping">Special Shipping</td>'
			.'<th class="item_total_price">Total Price</td>'
			.'</thead>';
			
		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item->item_id;
			$item_info = Item::model()->findByPk($item_id);
			
			$sale_price = $item->sale_price;
			$special_shipping_price = $item->special_shipping_price;
			if ($customerOrder->status != CustomerOrder::$STATUS_SOLD){
				$item_eta = Item::model()->getStatusString($item_id, $item_attributes['quantity']);
			} else {
				$item_eta = $item->getItemStatusList();
			}
			$quantity = $item->quantity;
			
			$item_total_price = $item->getTotalPrice($customerOrder);
			
			$strItems .= "<tr>";
			$strItems .= "<td class='item_name'>".$item_info->item_name."</td>";
			$strItems .= "<td class='color'>".$item_info->color.($item->custom_color ? ' ('.$item->custom_color.')' : '')."</td>";
			// $strItems .= "<td class='price'>".$sale_price."</td>";
			$strItems .= "<td class='quantity'>".$quantity."</td>";
			$strItems .= "<td class='special_shipping'>".($special_shipping_price ? '$'.number_format($special_shipping_price, 2) : '')."</td>";
			$strItems .= "<td class='item_total_price'>$".number_format($item_total_price, 2)."</td>";
			$strItems .= "</tr>";
		}

		$strItems .= "</table>";

		return $strItems;
	}
	
    public static function getCustomerOrderItemsShort($customer_order_id){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$strItems = '<table class="item_list">'
			.'<thead>'
			.'<th class="item_name">Item Name</td>'
			.'<th class="color">Color</td>'
			.'<th class="custom_color">Custom Color</td>'
			.'<th class="quantity">Qty</td>'
			.'<th class="item_total_price">Total Price</td>'
			//.'<th></td>'
			.'</thead>';
			
		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item->item_id;
			$item_info = Item::model()->findByPk($item_id);
			
			$sale_price = $item->sale_price;
			$special_shipping_price = $item->special_shipping_price;
			$quantity = $item->quantity;

			$item_total_price = $item->getTotalPrice($customerOrder);
			
			$strItems .= "<tr>";
			$strItems .= "<td class='item_name'>".$item_info->item_name."</td>";
			$strItems .= "<td class='color'>".$item_info->color."</td>";
			$strItems .= "<td class='custom_color'>".$item->custom_color."</td>";
			// $strItems .= "<td class='price'>".$sale_price."</td>";
			$strItems .= "<td class='quantity'>".$quantity."</td>";
			$strItems .= "<td class='item_total_price'>$".number_format($item_total_price, 2)."</td>";
			//$strItems .= "<td>".($item->canceled ? '<span class="item_canceled">Canceled</span>' : '<a href="#" class="cancel_items" rel="'.$item->id.'">Cancel</a>')."</td>";
			$strItems .= "</tr>";
		}

		$strItems .= "</table>";

		return $strItems;
	}
    
	public static function getCustomerOrderItemsForInvoicePdf($customer_order_id){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$strItems = "";

		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item->item_id;
			$item_info = Item::model()->findByPk($item_id);
			
			$sale_price = $item->sale_price;
			$special_shipping_price = $item->special_shipping_price;
			$quantity = $item->quantity;

            
            $product_total = $item->getTotalPrice($customerOrder);
            $item_price = $item->getItemPrice($customerOrder);
            
			$strItems .= 
				'<tr>'
					.'<td class="item_name">'.$item_info->item_name.' � '.$item_info->color.($item->custom_color ? '/'.$item->custom_color : '').' ('.$quantity.' @ $'.number_format($item_price, 2).')'
                    //.($special_shipping_price ? ' (special shipping: $'.number_format($special_shipping_price, 2).')' : '')
                    .'</td>'
                    .'<td>'.($special_shipping_price ? '$'.number_format($special_shipping_price, 2) : '').'</td>'
					// .'<td class="special_shipping">$'.number_format($special_shipping_price, 2).'</td>'
					.'<td class="item_price">$'.number_format($product_total, 2).'</td>'
				.'</tr>';
				
		}

		return $strItems;
	}
	
	public static function getCustomerOrderTotalPrice($customer_order_id, $with_and_without_tax = false){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$price = 0;
		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item_attributes['item_id'];
			
			$sale_price = $item_attributes['sale_price'];
			$special_shipping_price = $item->special_shipping_price;

			$price += $item->getTotalPrice($customerOrder);
		}
		

        $old_price = $price;
        if ($customerOrder->add_sales_tax){
            $settings = Settings::model()->getSettingVariables();
            $tax_percent = number_format($settings->tax_percent, 2);
            $price = $price * (100 + $tax_percent) / 100;
        }

		if ($with_and_without_tax){

            return array('total'=>$old_price, 'total_with_tax'=>$price, 'tax'=>($price - $old_price));
        }
		
		return round($price, 2);
	}
	
	// this function only calculates total not saving it anywhere
	public function calculateGrandTotal($with_tax = true){		
		// TODO: cache values where it is possible
		if($this->is_replacement){
			return false;
		}
		$price = 0;
		foreach ($this->order_items as $item){
			$price += $item->getTotalPrice($this);
		}

        if ($with_tax && $this->add_sales_tax){
            $price = $price * (100 + $this->getTaxPercent()) / 100;
        }

		return $price;
	}
	
	public function calculateDiscountedTotal($with_tax = true){
		if ($this->is_replacement){
			return 0;
		}
		$grand_total = $this->calculateGrandTotal($with_tax);
        if ($this->discounted == 'Not Discounted'){
			return $grand_total;
		}

		if ($this->custom_discount){
			if ($with_tax && $this->add_sales_tax){
				return $this->raw_discounted * (100 + $this->getTaxPercent()) / 100;
			} else {
				return $this->raw_discounted;
			}
		}

        return $grand_total - $this->calculateStandardDiscount($with_tax);
	}

    /**
     * Calculate standard discount for order
     * @param bool $withTax
     * @return float|int
     * @throws CException
     */
    public function calculateStandardDiscount($withTax = true)
    {
        switch ($this->shipping_method){
            case 'Shipping':
                $shipping_field = 'shipping_price';
                break;
            case 'Local Pickup':
                $shipping_field = 'local_pickup';
                break;
            case 'Shipped from Los Angeles':
                $shipping_field = 'la_oc_shipping';
                break;
            case 'Canada Shipping':
                $shipping_field = 'canada_shipping';
                break;
        }
        if (!isset($shipping_field))
            throw new CException('Unknow shipping method');

        $subtract_value = 0;
        foreach($this->order_items as $item){
            // TODO: investigate and write comments about how this works
            $shipping_price = $item->items->$shipping_field;
            $quantity = $item->quantity;
            // calculate how many sale price percents should we distract
            if ($quantity > 10){
                $percents = 55 + 10*($quantity-10);
            } else {
                $percents = (1+$quantity)*$quantity/2;
            }
            $subtract_value += $item->sale_price*0.01*$percents;

            // same thing for shipping percents
            if ($quantity > 15){
                $percents = 120 + 15*($quantity-15);
            } else {
                $percents = (1+$quantity)*$quantity/2;
            }
            $subtract_value += $shipping_price*0.01*$percents;
        }

        if ($withTax && $this->add_sales_tax){
            $subtract_value = $subtract_value * (100 + $this->getTaxPercent()) / 100;
        }
        return $subtract_value;
    }
	public function calculateDiscountAmount($with_tax = true){
		return $this->calculateGrandTotal($with_tax) - $this->calculateDiscountedTotal($with_tax);
	}
	
	public static function getCustomerOrderTotalPriceWithoutShipping($customer_order_id){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('customer_order_id',$customer_order_id);
		$items = CustomerOrderItems::model()->findAll($criteriaItems);
		$customerOrder = CustomerOrder::model()->findByPk($customer_order_id);
		
		$price = 0;
		foreach ($items as $item){
			$item_attributes = $item->attributes;
			$item_id = $item_attributes['item_id'];
			$item_info = Item::model()->findByPk($item_id);
			$sale_price = $item_attributes['sale_price'];

			$price += floatval($sale_price) * intval($item_attributes['quantity']);
		}
		
        if ($customerOrder->add_sales_tax){
            $settings = Settings::model()->getSettingVariables();
            $tax_percent = number_format($settings->tax_percent, 2);
            $price = $price * (100 + $tax_percent) / 100;
        }

		
		return number_format($price, 2);
	}
	
	public static function getCustomerOrderDiscountedTotal($customer_order_id, $grand_total = false){
		$customerOrderModel = CustomerOrder::model()->findByPk($customer_order_id);
        // we can provide grand total for the case when order is not yet saved
        if ($grand_total === false){
            $grand_total = $customerOrderModel->grand_total_price;
        }
        
        // if ($customerOrderModel->discounted == 'Not Discounted'){
            // return $grand_total;
        // }
		// $price = $customerOrderModel->discounted_total;
        $criteria = new CDbCriteria();
        $criteria->compare('customer_order_id', $customer_order_id);
        $items = CustomerOrderItems::model()->findAll($criteria);

		$distract_value = 0;
        foreach($items as $item){
            if ($customerOrderModel->shipping_method == "Shipping") {
				$shipping_price = $item->items->shipping_price;
			} else if ($customerOrderModel->shipping_method == "Local Pickup") {
				$shipping_price = $item->items->local_pickup;
			} else if ($customerOrderModel->shipping_method == "Shipped from Los Angeles") {
				$shipping_price = $item->items->la_oc_shipping;
			} else if ($customerOrderModel->shipping_method == "Canada Shipping") {
				$shipping_price = $item->items->canada_shipping;
			}
            
            $quantity = $item->quantity;
            // calsulate how many sale price percents should we distract
            if ($quantity > 10){
                // 55 is for Arithmetic progression total for 10 members
                $percents = 55 + 10*($quantity-10);
            } else {
                // Arithmetic progression total
                $percents = (1+$quantity)*$quantity/2;
            }
            // echo $percents.'/';
            $distract_value += $item->sale_price*0.01*$percents;
            
            // same thing for shipping percents
            if ($quantity > 15){
                $percents = 120 + 15*($quantity-15);
            } else {
                $percents = (1+$quantity)*$quantity/2;
            }
            $distract_value += $shipping_price*0.01*$percents;
        }
        if ($customerOrderModel->add_sales_tax){
            $settings = Settings::model()->getSettingVariables();
            $tax_percent = number_format($settings->tax_percent, 2);
            $distract_value = $distract_value * (100 + $tax_percent) / 100;
        }
        
        return round($grand_total - $distract_value, 2);
	}

    /**
     * this function only sets correct value for discounted_total field, nothing more
     * @deprecated
     */
    public function setCorrectOrderDiscountedTotal(){
        if ($this->discounted == 'Not Discounted'){
            $this->discounted_total = $this->grand_total_price;
        } else {
            if ($this->custom_discount){
                $this->discounted_total = $this->raw_discounted;
                if ($this->add_sales_tax){
                    $this->discounted_total = $this->discounted_total * (100 + $this->getTaxPercent()) / 100;
                }
            } else {
                $this->discounted_total = CustomerOrder::model()->getCustomerOrderDiscountedTotal($this->id, $this->grand_total_price);
            }
        }
    }
    
	// sets correct total price, discounted total and shipping price for the order and all individual items
	public function updateTotals(){
		$this->grand_total_price = round($this->calculateGrandTotal(), 2);
		$this->discounted_total = round($this->calculateDiscountedTotal(), 2);
		if ($this->custom_discount){
			$this->raw_discounted = round($this->calculateDiscountedTotal(false), 2);
		}
		return $this->save();
	}
	
	public function updateIndividualItemsTotals(){
		switch ($this->shipping_method){
			case 'Shipping':
				$shipping_field = 'shipping_price';
				break;
			case 'Local Pickup':
				$shipping_field = 'local_pickup';
				break;
			case 'Shipped from Los Angeles':
				$shipping_field = 'la_oc_shipping';
				break;
			case 'Canada Shipping':
				$shipping_field = 'canada_shipping';
				break;
		}
		
		// calculate order shipping total without discount
		$order_shipping_total 	= $this->calculateShippingTotal(false);
		$discount 				= $this->calculateDiscountAmount(false);
		
		$order_items = array();
		$order_items_ids = array();
		foreach ($this->order_items as $o_items){
			$order_items[$o_items->id] = $o_items;
			$order_items_ids[] = $o_items->id;
		}
		
		$criteria = new CDbCriteria();
		$criteria->addInCondition('customer_order_items_id', $order_items_ids);
		$ind_items = Individualitem::model()->findAll($criteria);
		
		foreach ($ind_items as $item){
			// don't forget about custom, standard discount and sales tax
			// item price should be counted without tax but order/shipment total - with it
			$o_items = $order_items[$item->customer_order_items_id];
			if ($order_shipping_total && !$item->is_replacement){
				$item_shipping 	= $item->items->$shipping_field + $o_items->special_shipping_price / $o_items->quantity;
				$item_weight 	= $item_shipping / $order_shipping_total;
				$item_discount 	= $discount * $item_weight;
				$item->shipping_total = $item_shipping - $item_discount;
			} else {
				$item->shipping_total = 0;
			}
			$item->save();
		}
	}
	
	public function getTaxPercent(){
		if (!$this->_tax_percent){
			$settings = Settings::model()->getSettingVariables();
			$this->_tax_percent = $settings->tax_percent;
		}
		return $this->_tax_percent;
	}
	
	public function calculateShippingTotal($with_tax = true){
		if ($this->is_replacement){
			return 0;
		}
		$total = 0;
		switch ($this->shipping_method){
			case 'Shipping':
				$shipping_field = 'shipping_price';
				break;
			case 'Local Pickup':
				$shipping_field = 'local_pickup';
				break;
			case 'Shipped from Los Angeles':
				$shipping_field = 'la_oc_shipping';
				break;
			case 'Canada Shipping':
				$shipping_field = 'canada_shipping';
				break;
		}
		foreach ($this->order_items as $c_items){
			$total += $c_items->items->$shipping_field * $c_items->quantity + $c_items->special_shipping_price;
		}
		
		if ($with_tax && $this->add_sales_tax){
            $total = $total * (100 + $this->getTaxPercent()) / 100;
        }
		
		return $total;
	}
    public static function getRetailerActionButtonString($customer_order_id){
        $customerOrderModel = CustomerOrder::model()->findByPk($customer_order_id);
        $strButton = "";

        if ($customerOrderModel->status == CustomerOrder::STATUS_PENDING)
            $strButton = '<input type="button" rel="'.$customer_order_id.'" class="add_order_items" value="Add Items" />'.
                '<input class="payment" rel="'.$customer_order_id.'" type="button" value="Purchase"/><br/>';
        return $strButton;
    }

	public static function getActionButtonString($customer_order_id){
		$customerOrderModel = CustomerOrder::model()->findByPk($customer_order_id);
		$strButton = "";
		
		if (!$customerOrderModel->approved){
			if (Yii::app()->user->isAdmin()){
				$strButton  = '<input type="button" value="Approve" rel="'.$customerOrderModel->id.'" class="approve_order" /><br />';
				$strButton .= '<input type="button" value="Deny" rel="'.$customerOrderModel->id.'" class="deny_order" />';
			} else {
				$strButton = '';
			}
			
			return $strButton;
		}
		
		if ($customerOrderModel->status == CustomerOrder::$STATUS_PENDING){
			$strButton = '<input class="payment" rel="'.$customer_order_id.'" type="button" value="Process"/><br/>'
				.'<input type="button" rel="'.$customer_order_id.'" class="invoice" value="Send Invoice"/>'
				.'<input type="button" rel="'.$customer_order_id.'" class="send_reminder" value="Send Reminder"/>'
				.'<input type="button" rel="'.$customer_order_id.'" class="print_invoice" value="Print Invoice"/>'
                .'<input type="button" rel="'.$customer_order_id.'" class="add_order_items" value="Add Items" />'
                .'<input type="button" rel="'.$customer_order_id.'" class="complete_order" value="Complete" />';
//		} else if ($customerOrderModel->status == CustomerOrder::$STATUS_COMPLETED){
//			$strButton = '<input class="sell_items" rel="'.$customer_order_id.'" type="button" value="Sell Items"/>'
//					.'<input type="button" rel="'.$customer_order_id.'" class="print_invoice" value="Print Invoice"/>';
		} else {
			$strButton = ""
				.'<input type="button" rel="'.$customer_order_id.'" class="print_invoice" value="Print Invoice"/>';
			if (Yii::app()->user->isAdmin()){
				$strButton .= '<input type="button" rel="'.$customer_order_id.'" class="issue_refund" value="Issue Refund"/>';
			}
		}
		
        if ($customerOrderModel->status == CustomerOrder::$STATUS_PENDING){
            $strButton .= '<input type="button" rel="'.$customer_order_id.'" class="lost_sale" value="Lost sale"/>';
            $strButton .= '<input type="button" rel="'.$customer_order_id.'" class="dead" value="Dead"/>';
            $strButton .= '<br /><br />';
            
            if ($customerOrderModel->discounted == 'Discounted'){
                $strButton .= '<input type="button" rel="'.$customer_order_id.'" class="remove_discount" value="Remove Discount"/>';
            }
            
            if ($customerOrderModel->discounted == 'Not Discounted' || $customerOrderModel->custom_discount){
                $strButton .= '<input type="button" rel="'.$customer_order_id.'" class="apply_standard_discount" value="Standard Discount"/>';
            }
            
            
            $strButton .= '<input type="button" rel="'.$customer_order_id.'" class="apply_custom_discount" value="Custom Discount"/>';
        }
        
		return $strButton;
	}

    /**
     * @return array shipping methods
     */
    public static function getShippingMethods()
    {
        return array(
            'Shipping' => 'Shipping',
            'Local Pickup' => 'Local Pickup',
            'Shipped from Los Angeles' => 'Shipped from Los Angeles',
            'Canada Shipping' => 'Canada Shipping',
        );
    }

	public function sellOrderItems($date = false){
		foreach ($this->order_items as $items){
			$criteria = new CDbCriteria();
			$criteria->limit = $items->quantity;
			$criteria->compare('customer_order_items_id', 0);
			$criteria->compare('item_id', $items->item_id);
			$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"');
			$qty = Individualitem::model()->count($criteria);
			$diff = $items->quantity - $qty;
			$criteria->order = Individualitem::getStatusOrderStatement('');
			Individualitem::model()->updateAll(array(
				'customer_order_items_id'   => $items->id,
				'sold_date'                 => $date ? $date : date("Y-m-d H:i:s", time()),
				'shipping_price'            => $items->items->shipping_price,
				'local_pickup'              => $items->items->local_pickup,
				'la_oc_shipping'            => $items->items->la_oc_shipping,
				'canada_shipping'           => $items->items->canada_shipping,
				'sale_price'                => $items->items->sale_price,
			), $criteria);
			
			if ($diff > 0){
				// not enough items available, create "not ordered"
				for ($i = 0; $i<$diff; $i++){
					$individual_item = new Individualitem();
					$individual_item->attributes                = $items->items->attributes;
					$individual_item->item_id                   = $items->items->id;
					$individual_item->customer_order_items_id   = $items->id;
					$individual_item->status                    = Individualitem::$STATUS_NOT_ORDERED;
					$individual_item->sold_date                 = $date ? $date : date("Y-m-d H:i:s", time());
						
					if ($individual_item->save() == false){
						CHtml::errorSummary($individual_item);
					}
				}
			}
		}

		return true;
	}
		
	public static function getSalesPersonName($order_id){
		$customerOrderModel = CustomerOrder::model()->findByPk($order_id);	
		$sales_person_id = $customerOrderModel->sales_person_id;
		$strResult = "";
		
		if ($sales_person_id == 0){
			$strResult = "Website";
		} else {
			$strResult = YumUser::model()->getUserName($sales_person_id);
		}
		
		return $strResult;
	}
	
	public function importOscOrderBillingInfo($osc_order_id, $osc_site_id){
		$oscOrderModel = OscOrders::model()->findByPk(array(
            'orders_id' => $osc_order_id, 'site_id' => $osc_site_id
        ));
		$oscOrderModel->status = OscOrders::$STATUS_IMPORTED;
		
		$customerOrderBillingInfoModel = new CustomerOrderBillingInfo();
		$attributes = array(
			'customer_order_id'			=> $this->id,
			'cc_number'					=> $oscOrderModel->cc_number,
			'cc_expiry_date'			=> $oscOrderModel->cc_expires,
			'cc_name_on_card'			=> "",
			'cc_type'					=> $oscOrderModel->cc_type,

			'billing_name'				=> $oscOrderModel->billing_name,
			'billing_street_address'	=> $oscOrderModel->billing_street_address,
			'billing_city'				=> $oscOrderModel->billing_city,
			'billing_state'				=> $oscOrderModel->billing_state,
			'billing_zip_code'			=> $oscOrderModel->billing_postcode,
			'billing_phone_number'		=> $oscOrderModel->customers_telephone,
			'billing_email_address'		=> "",

			'shipping_type'				=> "",
			'shipping_name'				=> $oscOrderModel->delivery_name,
			'shipping_street_address'	=> $oscOrderModel->delivery_street_address,
			'shipping_city'				=> $oscOrderModel->delivery_city,
			'shipping_state'			=> $oscOrderModel->delivery_state,
			'shipping_zip_code'			=> $oscOrderModel->delivery_postcode,
			'shipping_phone_number'		=> $oscOrderModel->customers_telephone,
			'busrestype'				=> $oscOrderModel->busrestype,
            'order_notes'               => "(OSC ORDER)",
		);
		$customerOrderBillingInfoModel->attributes = $attributes;
		$customerOrderBillingInfoModel->save();
		
		$oscOrderModel->save();
	}
	
	public function importAuthorizeNetTransctionBillingInfo($post_data){
		$model = CustomerOrderBillingInfo::getNewModelFromPost($post_data);
		$model->customer_order_id = $this->id;
		if (!$model->save()) {
            Yii::log("Error saving CustomerBillingInfo:" .PHP_EOL. print_r($model->errors, true));
        };
		// if customer doesn't have an email - take it from billing info
		if ($model->billing_email_address && !$this->customer->email){
			$this->customer->email = $model->billing_email_address;
			$this->customer->save();
		}
	}
	
	public function getBillingInfo(){
		$criteriaBillingInfo = new CDbCriteria;
		$criteriaBillingInfo->compare('customer_order_id',$this->id);
		$customerOrderBillingInfoModel = CustomerOrderBillingInfo::model()->find($criteriaBillingInfo);
		
		if ($customerOrderBillingInfoModel == null){
			$customerModel = Customer::model()->findByPk($this->customer_id);
			$strResult = '<p>Name:  '.$customerModel->first_name.' '.$customerModel->last_name.'</p>'
				.'<p>Phone: '.$customerModel->phone.'</p>'
				.'<p>Email: '.$customerModel->email.'</p>';
		} else {
			$strResult = '<p>Name:  '.$customerOrderBillingInfoModel->billing_name.'</p>'
				.'<p>Address:  '.$customerOrderBillingInfoModel->billing_street_address.'</p>'
				.'<p>City:  '.$customerOrderBillingInfoModel->billing_city.'</p>'
				.'<p>State/Province/Country: '.$customerOrderBillingInfoModel->billing_state.'</p>'
				.'<p>Zip Code: '.$customerOrderBillingInfoModel->billing_zip_code.'</p>';
		}
		
		return $strResult;
	}
	
	public static function getCustomerEmailByOrderId($order_id){
		$customerOrderModel = CustomerOrder::model()->findByPk($order_id);
		$customer_id = $customerOrderModel->customer_id;
		$customerModel = Customer::model()->findByPk($customer_id);
		return $customerModel->email;
	}
	
	public static function getCustomerOrderNeedProcessCustomerEmailList(){
		$strQuery = 'SELECT '
			.'DISTINCT co.customer_id, '
			.'c.email as customer_email '
			.'FROM '
			.'customer_order co '
			.'LEFT JOIN customer c on co.customer_id = c.id '
			.'WHERE '
			.'DATEDIFF(now(), co.created_on)=2 '
			.'OR DATEDIFF(now(), co.created_on)=3 '
			.'OR DATEDIFF(now(), co.created_on)=6 '
			.'OR DATEDIFF(now(), co.created_on)=10 '
			.'OR DATEDIFF(now(), co.created_on)=15 '
			;
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		$id_list = array();
		
		foreach ($result as $row){
			$email_list[] = $row["customer_email"];
		}
		
		return $email_list;
	}
    
    // next few funstions return array with fields "number" and "total" for orders number and money total from them
    // days is used when we need to get result for last period
    public static function getTotalSoldOrders($days = 0){
        $strQuery = 'SELECT COUNT(*) as `number`, SUM(grand_total_price) as `total` FROM customer_order WHERE status=\''.CustomerOrder::$STATUS_SOLD.'\'';
        if ($days){
            $strQuery .= ' AND sold_date >= date_sub(NOW(), INTERVAL '.$days.' DAY)';
        }
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        return $result[0];
    }
    
    public static function getTotalLostOrders($days = 0){
        $strQuery = 'SELECT COUNT(*) as `number`, SUM(grand_total_price) as `total` FROM customer_order WHERE status=\''.CustomerOrder::$STATUS_LOST.'\'';
        if ($days){
            $strQuery .= ' AND lost_sale_date >= date_sub(NOW(), INTERVAL '.$days.' DAY)';
        }
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        return $result[0];
    }
    
    public static function getTotalPendingOrders($days = 0){
        $strQuery = 'SELECT COUNT(*) as `number`, SUM(grand_total_price) as `total` FROM customer_order WHERE status=\''.CustomerOrder::$STATUS_PENDING.'\'';
        if ($days){
            $strQuery .= ' AND created_on >= date_sub(NOW(), INTERVAL '.$days.' DAY)';
        }
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
        return $result[0];
    }
    
    public static function addCustomerNotesEtaUpdated($container_id){
        // $strQuery = 'SELECT customer_order_items_id, COUNT(*) as `items`, eta FROM individual_item WHERE supplier_order_container_id >0 GROUP BY customer_order_items_id';
        $strQuery = 'SELECT customer_order_items_id, COUNT(*) as `items`, item.item_name, item.item_code, eta, customer_order_items.customer_order_id
                        FROM individual_item 
                        LEFT JOIN customer_order_items ON customer_order_items.id = individual_item.customer_order_items_id
                        LEFT JOIN item ON item.id = individual_item.item_id
                        WHERE supplier_order_container_id = '.$container_id.' 
                        GROUP BY customer_order_items_id';
        $result = Yii::app()->db->createCommand($strQuery)->queryAll();
        foreach ($result as $row){
			$order = CustomerOrder::model()->findByPk($row['customer_order_id']);
			
			$note = new CustomerNote();
            $note->sales_person_id = Yii::app()->user->id;
            $note->customer_id = $order->customer_id;
            $note->created_on = date('Y-m-d H:i:s', time());
            $note->content = '<strong>E.T.A. set,</strong> '.$row['items'].' x '.$row['item_code'].' ('.$row['item_name'].').<br /><strong> E.T.A.:</strong> '.date('Y-m-d', strtotime($row['eta']));
            
            $note->save();
        }
    }
    
    public function getShippingAddressStr($address_type = true){
        if ($this->billing_info){
            $str  = $this->billing_info->shipping_street_address;
            $str .= '<br />'.$this->billing_info->shipping_city;
            $str .= '<br />'.$this->billing_info->shipping_state.', ';
            $str .= $this->billing_info->shipping_zip_code;
            $str .= '<br />Phone: '.$this->billing_info->shipping_phone_number;
            if ($address_type){
				$str .= '<br /><br />'.$this->billing_info->busrestype;
			}
            return $str;
        } else {
            return '';
        }
    }
	
	public function getBillingAddressStr(){
        if ($this->billing_info){
            $str  = $this->billing_info->billing_street_address;
            $str .= '<br />'.$this->billing_info->billing_city;
            $str .= '<br />'.$this->billing_info->billing_state.', ';
            $str .= $this->billing_info->billing_zip_code;
            $str .= '<br />Phone: '.$this->billing_info->billing_phone_number;
            // $str .= '<br /><br />Address Type: '.$this->billing_info->busrestype;
            return $str;
        } else {
            return '';
        }
    }
	
	public function getShippingAddressStrPlain(){
        if ($this->billing_info){
            $str  = 'Name: '.$this->billing_info->shipping_name.' '.$this->billing_info->shipping_last_name.'; ';
            $str .= 'Street address: '.$this->billing_info->shipping_street_address.'; ';
            $str .= 'City: '.$this->billing_info->shipping_city.'; ';
            $str .= 'State: '.$this->billing_info->shipping_state.'; ';
            $str .= 'ZIP: '.$this->billing_info->shipping_zip_code.'; ';
            $str .= 'Phone: '.$this->billing_info->shipping_phone_number.'; ';
            $str .= 'Address Type: '.$this->billing_info->busrestype;
            return $str;
        } else {
            return '';
        }
    }
	
	public function getBillingAddressStrPlain(){
        if ($this->billing_info){
            $str  = 'Street address: '.$this->billing_info->billing_street_address.', ';
            $str .= 'City: '.$this->billing_info->billing_city.', ';
            $str .= 'State: '.$this->billing_info->billing_state.', ';
            $str .= 'ZIP: '.$this->billing_info->billing_zip_code.', ';
            $str .= 'Phone: '.$this->billing_info->billing_phone_number.', ';
            // $str .= '<br /><br />Address Type: '.$this->billing_info->busrestype;
            return $str;
        } else {
            return '';
        }
    }
	
	public function getPublicCheckoutURL(){
        return Yii::app()->createAbsoluteUrl(Yii::app()->getBaseUrl(), array(), 'https')
            . "/customer/checkout_order/".md5($this->id.self::$ORDER_SALT)."/".$this->id;
		//return Yii::app()->getBaseUrl(true)."/index.php/customer/checkout_order/".md5($this->id.self::$ORDER_SALT)."/".$this->id;
		//return Yii::app()->getBaseUrl(true)."/index.php/customer/checkout_order/?order_id=".$this->id; //old not secure
		// return Yii::app()->params['secureURL']."index.php/customer/checkout_order/?order_id=".$this->id;
	}
	
	public function getShipmentTableShort(){
		if (!$this->shipments){
			return '';
		}
		$this->setShipmentItemsFakeStatus();
		$controller = new CustomershipperController('');
		return $controller->renderPartial('customershipper/_order_shipments_short', array(
			'shipments'=>$this->shipments,
			'order'=>$this,
		), true);
	}
	
	public function getShipmentTable(){
		// TODO: move this crap to the view
		if (!$this->shipments){
			return '';
		}
		
		$this->setShipmentItemsFakeStatus(true);
		
		$divs  = array();
		$head  = '';
		if ($this->shipping_status == self::$SHIPPING_STATUS_PENDING){
			$head .= '<input type="button" class="new_shipment" id="new_shipment_'.$this->id.'" rel="'.$this->id.'" value="New Shipment" />
						<input type="button" class="save_shipment" id="save_shipment_'.$this->id.'" rel="'.$this->id.'" value="Save" />
						<input type="button" class="cancel_new_shipment" id="cancel_new_shipment_'.$this->id.'" rel="'.$this->id.'" value="Cancel" />';
			//if (Yii::app()->user->isAdmin()){
			//$head .= '<input type="button" class="combine_orders" id="combine_orders_'.$this->id.'" rel="'.$this->id.'" value="Combine with another order" />';
	//		}
		}
		$shipment_ids = array();
		foreach ($this->shipments as $row){
			$shipment_ids[$row->id] = 'ID: '.$row->id;
		}
		
		foreach ($this->shipments as $row){
			$str  = '<div class="customer_order_shipment_data">';
			$str .= '<strong>ID: '.$row->id.'</strong>';
			if ($row->return){
				$str .= ' [Return]';
			}
			$str .= '<br />';
			$str .= 'Status: '.$row->status;
			if ($row->status == CustomerOrderShipment::$STATUS_PICKUP){
				$str .= ' ('.date('M. d, Y', strtotime($row->pickup_date)).')';
			}
			$str .= '<br />';
			$str .= '<a href="#" class="toggle_shipment_table" rel="'.$row->id.'">Collapse</a><br />';
			$str .= '<table class="items" id="shipment_table_'.$row->id.'">';
			$str .= '<tr><th>Item Name</th><th>Color</th><th>Status</th><th>Cancel</th><th>Shipment</th></tr>';
			foreach ($row->ind_items as $item){
				$str .= '<tr class="row">';
				$str .= '<td>';
				$str .= '<input type="checkbox" value="'.$item->id.'" class="shipment_item_checkbox order_'.$this->id.'" />';
				$str .= $item->items->item_name;
				if ($item->prev_shipment){
					$str .= '<br /><span class="imported_item_info">';
					$str .= 'Imported from <a href="'.Yii::app()->getBaseUrl(true).'/index.php/customer/'.$item->prev_shipment->customer_order->customer_id.'#note">';
					$str .= 'Order #'.$item->prev_shipment->customer_order_id.'</a>';
					$str .= '</span>';
				}
				$str .= '</td>';
				$str .= '<td>'.$item->items->color.'</td>';
				if ($item->getStatus() == Individualitem::$STATUS_ORDERED_WITH_ETA){
					$str .= '<td>Arriving on '.date("M. d, Y", strtotime($item->getEta())).'</td>';
				} else {
					$str .= '<td>'.$item->getStatus().'</td>';
				}
				if ($item->status == Individualitem::$STATUS_CANCELLED_SHIPPING){
					$str .= '<td title="'.date("M. d, Y", strtotime($item->shipping_canceled_date)).'" class="item_canceled">Cancelled</td>';
				} elseif ($row->status == CustomerOrderShipment::$STATUS_PENDING) {
					$str .= '<td><a href="#" class="cancel_item" rel="'.$item->id.'">Cancel</a></td>';
				} else {
					$str .= '<td></td>';
				}
				
				$str .= '<td>';
				if ($item->status != Individualitem::$STATUS_CANCELLED_SHIPPING && $row->status == CustomerOrderShipment::$STATUS_PENDING){
					$str .= CHtml::dropDownList('shipment_id', $row->id, $shipment_ids, array('class'=>'select_shipment_for_ind_item', 'rel'=>$item->id));
				}
				$str .= '</td>';
				$str .= '</tr>';
			}
			
			foreach ($row->returns as $return){
				$str .= '<tr>';
				$str .= '<td>'.$return->items->item_name.'</td>';
				$str .= '<td>'.$return->items->color.'</td>';
				$str .= '<td>'.$return->status.'</td>';
				$str .= '<td></td>';
				$str .= '<td></td>';
				$str .= '</tr>';
			}
			
			$str .= '</table>';
			//$str .= ($this->shipping_method == 'Local Pickup') ? '<input type="button" rel="'.$row->id.'" value="Schedule Pickup" class="schedule_pickup" />' : '<input type="button" rel="'.$row->id.'" value="Send for Bidding" class="send_for_bidding" />';
			$str .= '</div>';
			$divs[] = $str;
		}
		
		return $head.implode('<hr />', $divs);
	}
	
	public function getOrderContentsStr(){
		$str = array();
		foreach ($this->order_items as $items){
			$i_str = $items->quantity.' x '.$items->items->item_code. '('.$items->items->item_name.($items->custom_color ? ', custom color: '.$items->custom_color : '').'), status: '.str_replace('<br/>', '; ', $items->getItemStatusList());
			$str[] = $i_str;
		}
		
		return implode('<br />', $str);
	}
	
	public function addCustomerSoldNote(){
		$content  = '<strong>Order #'.$this->id.' sold</strong><br />';
		//$content .= '<strong>Order contents</strong>:<br />'.$this->getOrderContentsStr().'<br />';
		$content .= '<strong>Billing Name: </strong>'.$this->billing_info->billing_name.' '.$this->billing_info->billing_last_name.'<br />';
		$content .= '<strong>Shipping Address: </strong>'.$this->getShippingAddressStrPlain().'<br />';
		$content .= '<strong>Billing Adress: </strong>'.$this->getBillingAddressStrPlain();
		
		$this->customer->addNote($content, $this);
	}
	
	public function addCustomerOrderItemShippedNote($individualItemId = 0){
		if($individualItemId == 0 )	return false; 
		$ind_item = Individualitem::model()->findByPk($individualItemId);
		$content  = '<strong>Item #'.$ind_item->id.' Shipped (order #'.$this->id.') </strong><br />';
		$content .= '<strong>Shipment Completed:</strong>'.date('Y-m-d H:i:s', strtotime($ind_item->shipment->tracking_number_date));
		$shiper_profile = YumProfile::model()->findAllByPk($ind_item->shipment->shipper->id);
		$content .= '<strong>Shipping company:</strong>'.( empty($shiper_profile->company) ? $shiper_profile->firstname.' '.$shiper_profile->lastname : $shiper_profile->company).' ('.$shiper_profile->website.')';
		$content .= '<strong>Tracking number:</strong>'.$ind_item->shipment->tracking_number;
		$content .= '<strong>Item:</strong>'.$ind_item->items->item_code.'('.$ind_item->items->item_name.
					($ind_item->customer_order_items->custom_color ? ', custom color: '.$ind_item->customer_order_items->custom_color : '').'),<br />';
		$content .= '<strong>Order contents</strong>:<br />'.$this->getOrderContentsStr().'<br />';
		$this->customer->addNote($content);
	}
	
	public function sendCustomerReminder(){
		$total_price    = CustomerOrder::getCustomerOrderTotalPrice($this->id);
        $dsc            = ($this->discounted == 'Discounted');
        $dsc_total      = $this->discounted_total;
        $dsc_amount     = $total_price - $dsc_total;
        
        $total_price    = number_format($total_price, 2);
        $dsc_total      = number_format($dsc_total, 2);
        $dsc_amount     = number_format($dsc_amount, 2);
        
		$user_id = Yii::app()->user->id;
		
		//~ $adminModel = YumUser::model()->findByPk(1);
		//~ $salesPersonModel = YumUser::model()->findByPk($this->sales_person_id);
		//~ $salesPersonMailAddress = $salesPersonModel->profile->email;
		//~ $adminMailAddress = $adminModel->profile->email;
		
		$email = Yii::app()->email;
		$email->to = $this->customer->email;
		$email->from = Customer::NOTIFICATION_EMAIL;
		// $email->bcc = "support@regencyshop.com,".$adminMailAddress.",".$salesPersonMailAddress;
		$email->subject = 'Order Reminder';
		
		$checkout_url = $this->getPublicCheckoutURL();
		$content = 'Hello '. $this->customer->first_name." ".$this->customer->last_name.'<br />'
			.'Just wondering if you are still interested in getting your items <br/><br/>'
			.CustomerOrder::getCustomerOrderItemsForInvoice($this->id).'<br/><br/>'
            .($dsc ? 'Regular price: $'.$total_price.'<br />Discount: $'.$dsc_amount.'<br /><br />Your investment: $'.$dsc_total : 'Your investment: $'.$total_price).'<br />'
			.'<br /><br/>FREE SHIPPING!<br /><br/>'
			.'You can securely make the payment at the link below:<br/><br/>'
			.'<a href="'.$checkout_url.'">'.$checkout_url.'</a><br/><br/>'
			// .'We will process your order as soon as you make the payment. Feel free to call me anytime at 1-866-776-2680.<br/><br/>'
			.'Looking forward to serving all your furniture needs,<br />'
			.'Lisa G.'
			.'<br /><br />'
			.'Note: Prices are subject to change without notice. If you do not purchase this item within 7 days of this invoice email, then price difference will have to be charged for the order to be processed.'
			;
		
		$email->message = $content;
		return $email->send();
	}

    public function sendCustomerDiscountedReminder(){
        $total_price    = number_format(
            CustomerOrder::getCustomerOrderTotalPrice($this->id), 2
        );

        $email = Yii::app()->email;
        $email->to = $this->customer->email;
        $email->to = 'addicted2sounds@gmail.com';
        $email->from = Customer::NOTIFICATION_EMAIL;
        $email->subject = AutoMailMessage::MS_ORDER_DISCOUNTED_REMINDER;
        $email->message = strtr(AutoMailMessage::MC_ORDER_DISCOUNTED_REMINDER, array(
            '{name}' => $this->customer->fullName,
            '{items}' => CustomerOrder::getCustomerOrderItemsForInvoice($this->id),
            '{standard_price}' => $total_price,
            '{discounted_price}' => number_format(
                $this->discounted_total, 2
            ),
            '{savings}' => number_format(
                $total_price - $this->discounted_total, 2
            ),
            '{checkout_url}' => $this->getPublicCheckoutURL()
        ));
        return $email->send();
    }
	// creates new shipment for the order. 
	// $items contains individual item ids to include into new shipment
	// if no ids given - all items will be used
	public function createShipment($items = NULL){
		$shipment = new CustomerOrderShipment();
		$shipment->customer_order_id = $this->id;
		$shipment->status = CustomerOrderShipment::$STATUS_PENDING;
		if ($shipment->save()){
			$criteria = new CDbCriteria();
			if ($items){
				$criteria->addInCondition('id', $items);
			} else {
				$order_items = array();
				foreach ($this->order_items as $row){
					$order_items[] = $row->id;
				}
				$criteria->addInCondition('customer_order_items_id', $order_items);
			}
			
			Individualitem::model()->updateAll(array('customer_order_shipment_id'=>$shipment->id), $criteria);
			return $shipment;
		} else {
			return false;
		}
	}
	
	public function sell($date = false){
		$this->sellOrderItems($date);   // attach ind. items to the order
		$this->sold_date = $date ? $date : date("Y-m-d H:i:s", time());
		$this->shipping_status = self::$SHIPPING_STATUS_PENDING;    // initial status for CS process
		$this->status = CustomerOrder::$STATUS_SOLD;
        $this->save();
		$this->updateIndividualItemsTotals();    // calculate and save ind. items shipping prices and other info needed for CS process
		$this->addCustomerSoldNote();
		$this->createShipment();        // first shipment containing all ind. items from the order
		$this->calculateAndSaveProfit();
		////////////////////////////////////////////////////////////
		// Added Activity
		Activity::model()->addActivity_SellItemsCustomerOrder($this->id);
	}
	
	public function approve(){
		// TODO: don't forget about returning shippings
		if ($this->approved){
			return true;
		}
		
		$this->sell();
		$this->approved = 1;
		return $this->save();
	}
	
	public function deny(){
		if ($this->approved != 1){
			$criteria = new CDbCriteria();
			$criteria->compare('customer_order_id', $this->id);
			CustomerOrderItems::model()->deleteAll($criteria);
			if ($this->billing_info){
				$this->billing_info->delete();
			}
// 			if ($this->customer->total_orders == 1){
// 				$this->customer->delete();
// 			}
			
			return $this->delete();
		}
		
		return false;
	}
	
	public function cancelShipping($reason){
		foreach ($this->shipments as $shipment){
			if ($shipment->status != CustomerOrderShipment::$STATUS_SCANNED_OUT &&
						$shipment->status != CustomerOrderShipment::$STATUS_CANCELLED){
				$shipment->cancel($reason, false, false);
			}
		}
		$this->shipping_status = self::$SHIPPING_STATUS_CANCELLED;
		$this->cancellation_reason = $reason;
		$this->cancellation_date = date("Y-m-d H:i:s");
		return $this->save();
	}
	
	public function chargeback($reason){
		foreach ($this->shipments as $shipment){
			if ($shipment->status != CustomerOrderShipment::$STATUS_SCANNED_OUT &&
						$shipment->status != CustomerOrderShipment::$STATUS_CANCELLED){
				$shipment->cancel($reason, true, false);
			}
		}
		$this->shipping_status = self::$SHIPPING_STATUS_CHARGEBACK;
		$this->cancellation_reason = $reason;
		$this->chargeback_date = date("Y-m-d H:i:s");
		return $this->save();
	}
	
	public function updateShippingStatus(){
		$bidding 		= 0;
		$cancelled 		= 0;
		$scanned_out 	= 0;
		$pickup 		= 0;
		$pending 		= 0;
		$completed 		= 0;
		$attached 		= 0;
		foreach ($this->shipments as $shipment){
			switch ($shipment->status){
				case CustomerOrderShipment::$STATUS_PENDING:
					$pending++;
					break;
				case CustomerOrderShipment::$STATUS_PICKUP:
					$pickup++;
					break;
				case CustomerOrderShipment::$STATUS_BIDDING:
					$bidding++;
					break;
				case CustomerOrderShipment::$STATUS_SCANNED_OUT:
					$scanned_out++;
					break;
				case CustomerOrderShipment::$STATUS_CANCELLED:
					$cancelled++;
				case CustomerOrderShipment::$STATUS_COMPLETED:
					$completed++;
					break;
				case CustomerOrderShipment::$STATUS_ATTACHED:
					$attached++;
					break;
			}
		}
		
		if ($pending > 0){
			// something remains pending
			$this->shipping_status = self::$SHIPPING_STATUS_PENDING;
		} elseif ($attached == count($this->shipments)) {
			// everything is cancelled
			$this->shipping_status = self::$SHIPPING_STATUS_ATTACHED;
		} elseif ($cancelled == count($this->shipments)) {
			// everything is cancelled
			$this->shipping_status = self::$SHIPPING_STATUS_CANCELLED;
		} elseif ($completed == count($this->shipments)) {
			// everything is completed
			$this->shipping_status = self::$SHIPPING_STATUS_COMPLETED;
		} elseif ($cancelled + $scanned_out + $completed == count($this->shipments)){
			// something is scanned out and something is cancelled
			$this->shipping_status = self::$SHIPPING_STATUS_SCANNED_OUT;
		} elseif ($bidding > 0){
			$this->shipping_status = self::$SHIPPING_STATUS_BIDDING;
		} elseif ($pickup > 0){
			$this->shipping_status = self::$SHIPPING_STATUS_PICKUP;
		}
		
		return $this->save();
	}
	
	public function getBillingName(){
		return $this->billing_info->billing_name.' '.$this->billing_info->billing_last_name;
	}
	
	public function getShippingName(){
		return $this->billing_info->shipping_name.' '.$this->billing_info->shipping_last_name;
	}
	
	public function sendForBidding(){
		foreach ($this->shipments as $shipment){
			if ($shipment->status == CustomerOrderShipment::$STATUS_PENDING){
				$shipment->sendForBidding(false);
			}
		}
		
		$this->shipping_status = self::$SHIPPING_STATUS_BIDDING;
		return $this->save();
	}
	
	public function isInStock(){
		foreach ($this->shipments as $shipment){
			if (!$shipment->isInStock()){
				return false;
			}
		}
		
		return true;
	}
	
	public function itemsHaveInfo(){
		foreach ($this->shipments as $shipment){
			if (!$shipment->itemsHaveInfo()){
				return false;
			}
		}
	
		return true;
	}
	
	public function readyForShipping(){
		foreach ($this->shipments as $shipment){
			if (!$shipment->readyForShipping()){
				return false;
			}
		}
		
		return true;
	}
	
	public function sendReviewNotification(){
		if (!$this->review_code){
			$this->review_code = substr(md5(mt_rand().uniqid()), 0, 20);
		}
		 	
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		
		$url = Yii::app()->getBaseUrl(true).'/index.php/customer/review/?id='.$this->id.'&code='.$this->review_code;
		$name = $this->customer->first_name.' '.$this->customer->last_name;
		$items = array();
		foreach ($this->order_items as $item){
			$items[] = $item->items->item_name;
		}
		
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->AddAddress($this->customer->email);
		$mail->SetFrom(Customer::NOTIFICATION_EMAIL, 'RegencyShop.com');
		$mail->Subject = AutoMailMessage::MS_CUSTOMER_REVIEW;
		$body = AutoMailMessage::MC_CUSTOMER_REVIEW;
		$body = str_replace('[items]', implode(',', $items), $body);
		$body = str_replace('[name]', $name, $body);
		$mail->Body = str_replace('[url]', $url, $body);
		if($mail->Send()){
            //echo "notification sent. Order id: {$this->id}<br>";
			$this->review_notification_sent = 1;
			$this->save();
		}
	}

	//array for filter Last Order in /customer/admin
	public static function getOrderStatus(){ 
		return array(
			''=>'All',
			self::$STATUS_PENDING=>'Рending',
			self::$STATUS_SOLD=>'Sold',
			self::$STATUS_DEAD=>'Dead',
			self::$STATUS_LOST=>'Lost Sale',
			'no_order'=>'No Order',
		);
	}

	public function calculateAndSaveProfit($debug = false){
		//TODO: rewrite this using relations
        //TODO: check how multiple additional payment works and update this method if needed
		$profit = 0;
		if($debug) echo 'order_id='.$this->id.'<br />';
		$is_discount = false;
		if($this->discounted == 'Discounted'){
			$is_discount = true;
			if($debug) echo 'discount='.$is_discount.'<br />';
		}
		$criteria = new CDbCriteria;
		$criteria->compare('customer_order_id',$this->id);
		$items = CustomerOrderItems::model()->findAll($criteria);
		$shipments = CustomerOrderShipment::model()->findAll($criteria);
		$shipments_arr = array();
		foreach ($shipments as $shipment){
			if($shipment->status == CustomerOrderShipment::$STATUS_COMPLETED){
                $additional_payment = $shipment->getTotalAdditionalPaymentAmount();
//				if($shipment->additional_payment_required == 1 &&
//				   $shipment->additional_payment_amount > 0 &&
//				   $shipment->additional_payment_done ==1){
//						$additional_payment += $shipment->additional_payment_amount;
//				}

				$criteriaShipmentBid = new CDbCriteria;
				$criteriaShipmentBid->compare('shipment_id',$shipment->id);
				$criteriaShipmentBid->compare('selected',1);
				$selectedShipmentBid = CustomerOrderShipmentBid::model()->find($criteriaShipmentBid);
				if($debug) echo 'shipment_id='.$shipment->id.'<br />';
				if($debug) echo 'additional_payment='.$additional_payment.'<br />';
				if($debug) echo 'rate_quote='.$selectedShipmentBid->rate_quote.'<br />';
				$shipments_arr[$shipment->id] = array('add_pay'=>$additional_payment,
													  'rate_quote'=>$selectedShipmentBid->rate_quote);
			}
		}
		
		foreach ($items as $item){
			$criteriaItems = new CDbCriteria;
			$criteriaItems->compare('customer_order_items_id',$item->id);
			$individual_items = Individualitem::model()->findAll($criteriaItems);
			if($debug) echo 'item_id='.$item->id.'<br />';
			$special_shipping_price = 0;
			if($item->special_shipping_price > 0){
				$special_shipping_price = $item->special_shipping_price/$item->quantity;
			}
			if($debug) echo 'special_shipping_price='.$special_shipping_price.'<br />';
			foreach ($individual_items as $individual){
				if($individual->status == Individualitem::$STATUS_CANCELLED_SHIPPING) continue;
				
				$shipping_price = 0;
				if ($this->shipping_method == "Shipping") {
					$shipping_price = $individual->shipping_price;
					$customer_shipping_cost = $individual->items->shipping_price;
				} else if ($this->shipping_method == "Local Pickup") {
					$shipping_price = $individual->local_pickup;
					$customer_shipping_cost = $individual->items->local_pickup;
				} else if ($this->shipping_method == "Shipped from Los Angeles") {
					$shipping_price = $individual->la_oc_shipping;
					$customer_shipping_cost = $individual->items->la_oc_shipping;
				} else if ($this->shipping_method == "Canada Shipping") {
					$shipping_price = $individual->canada_shipping;
					$customer_shipping_cost = $individual->items->canada_shipping;
				}
				
				// shipping price and customer shipping cost can be different when order is replacement
				// in this case customer doesn't pay anything and shipping prise is $0
				if($debug) echo 'shipping_price='.$shipping_price.'<br />';
				// TODO: use correct calculation methods for discount
				if($is_discount){
					$discount = ($shipping_price + $special_shipping_price - $individual->shipping_total);
					if($discount < 0) $discount = 0;
				} else {
					$discount = 0;
				}
				if($debug) echo 'discount='.$discount.'<br />';
				$profit += (($item->sale_price*0.97 - $individual->fob_cost_price) + 
						  0.97*($special_shipping_price + $shipping_price) - $discount - 
						  100*$individual->getRealCBMForCustomerShipping());
				if($debug) echo 'sale_price='.$item->sale_price.'<br />';
				if($debug) echo 'fob_cost_price='.$individual->fob_cost_price.'<br />';
				if($debug) echo 'special_shipping_price='.$special_shipping_price.'<br />';
				if($debug) echo 'shipping_price='.$shipping_price.'<br />';
				if($debug) echo 'discount='.$discount.'<br />';
				if($debug) echo 'cbm='.$individual->getRealCBMForCustomerShipping().'<br />';
				if(!isset($shipments_arr[$individual->customer_order_shipment_id])){
					$profit -= 	$customer_shipping_cost;
					if($debug) echo 'shipping_price='.$shipping_price.'<br />';
				}
					
			}
			
		}
		
		foreach ($shipments_arr as $shipment){
			$profit += (0.97*$shipment["add_pay"] - $shipment["rate_quote"]);
			if($debug) echo 'add_pay='.$shipment["add_pay"].'<br />';
			if($debug) echo 'rate_quote='.$shipment["rate_quote"].'<br />';
		}
		if($debug){ 
			echo 'profit='.$profit.'<br /><br /><br />';
		} else {
			$this->profit = $profit;
			$this->save();
		}
		return true;
	}
	
	static public function calculateAndSaveProfitForAllSold(){
		$criteria = new CDbCriteria;
		$criteria->compare('status',self::$STATUS_SOLD);
		$orders = self::model()->findAll($criteria);
		foreach ($orders as $order){
			$order->calculateAndSaveProfit();
		}
	}
	
	static public function calculateAntProfitForCustomer($customer_id = 0){
		if($customer_id == 0) return 0;
		$total_ant_profit = 0;
		$criteria = new CDbCriteria;
		$criteria->compare('status',self::$STATUS_PENDING);
		$criteria->compare('customer_id', $customer_id);
		$orders = self::model()->findAll($criteria);
		foreach ($orders as $order){
			$total_ant_profit += self::getOrderAntProfit($order->id);
		}
		return $total_ant_profit;
	}
	
	static public function getCustomerProfit($customer_id = 0){
		$profit = array(
			'ant_profit'=>0,
			'last_profit'=>0,
			'total_profit'=>0	
		);
		
		if($customer_id > 0){
			$profit['ant_profit'] = self::calculateAntProfitForCustomer($customer_id);
			$criteria = new CDbCriteria;
			$criteria->compare('status',self::$STATUS_SOLD);
			$criteria->compare('customer_id', $customer_id);
			$criteria->order = 'sold_date ASC';
			$orders = self::model()->findAll($criteria);
			foreach ($orders as $order){
				if($order->profit > 0 ){
					$profit['last_profit'] = $order->profit;
					$profit['total_profit'] += $profit['last_profit'];
				}
			}
		}
		return $profit;
	}

	public function verificationRequired(){
		return $this->billing_info->shipping_zip_code != $this->billing_info->billing_zip_code
					&& !$this->shipping_info_verified; 
	}
	
	public function calculateGrandTotalWithShippingMethod($shipping_method){
		$old_method = $this->shipping_method;
		$this->shipping_method = $shipping_method;
		$total = $this->calculateGrandTotal();
		$this->shipping_method = $old_method;
		return $total;
	}
	
	public function calculateDiscountedTotalWithShippingMethod($shipping_method){
		$old_method = $this->shipping_method;
		$this->shipping_method = $shipping_method;
		$total = $this->calculateDiscountedTotal();
		$this->shipping_method = $old_method;
		return $total;
	}
	
	/**
	 * if order has a standard discount we convert it to the custom keeping totals the same
	 * it's usable for switching shipping method
	 * also works for not discounted orders
	 */
	public function convertToCustomDiscount($new_discounted_total = false){
        $modified = false;

		if (!$this->custom_discount){
			$this->raw_discounted 	= $this->calculateDiscountedTotal(false);
			$this->discounted_total = $this->calculateDiscountedTotal();
			$this->custom_discount 	= 1;
			$this->discounted 		= self::ORDER_DISCOUNTED;
			$modified = true;
		}
		
		if ($new_discounted_total){
			$this->raw_discounted = round($new_discounted_total, 2);
			$modified = true;
		}
		
		return $modified ? $this->save() : true;
	}
	
	/**
	 * Switches shipping method and sets correct totals, 
	 * converts discount to custom if additional discount given
	 * sends email notification to the customer
	 * @param string $shipping_method new shipping method
	 * @param array $special_shipping array of special shiping values for customer order tiems, items IDs are used as keys
	 * @param float $custom_discounted_total new custom discounted total 
	 * @return additional payment amount 
	 */
	public function editShippingInfoAfterSale($shipping_method, $special_shipping, $additional_payment = false, $send_notification = false, $custom_discounted_total = false){
		if ($custom_discounted_total){
			$this->convertToCustomDiscount($custom_discounted_total);
		}
		
		foreach ($this->order_items as $items) {
			$items->special_shipping_price = round($special_shipping[$items->id], 2);
			$items->save();
		}
		
		$this->shipping_method = $shipping_method;
		if ($this->updateTotals()){
			$this->updateIndividualItemsTotals();
		} else {
			return false;
		}
		
		if ($additional_payment){
			// take care of additional payment
			$this->shipping_edited = 1;
			$this->shipping_edited_payment_amount = round($additional_payment, 2);
			$this->shipping_edited_payment_code = substr(md5(uniqid()), 0, 8);
			if ($this->save()){
				if ($send_notification){
					$this->sendShippingEditedNotification();
				}
				return true;
			} else {
				return false;
			}
		} else {
			$this->shipping_edited = 1;
			return $this->save();
		}

		return true;
		// TODO: email notification
		// TODO: add customer note
	}
	
	public function sendShippingEditedNotification() {
        $link = Yii::app()->createAbsoluteUrl('/customer/additional_payment', array(
            'se' => 1,
            'id' => $this->id,
            'code' => $this->shipping_edited_payment_code
        ), 'https');
		//$link = Yii::app()->getBaseUrl(true).'/index.php/customer/additional_payment/?se=1&id='.$this->id.'&code='.$this->shipping_edited_payment_code;
		
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->AddAddress($this->customer->email);
		$mail->SetFrom(Customer::NOTIFICATION_EMAIL, 'RegencyShop.com');
		$mail->Subject = AutoMailMessage::MS_CUSTOMER_SHIPPING_UPDATED;
		$body = str_replace('[link]', $link, AutoMailMessage::MC_CUSTOMER_SHIPPING_UPDATED);
		$body = str_replace('[name]', $this->customer->first_name.' '.$this->customer->last_name, $body);
		$mail->Body = str_replace('[amount]', number_format($this->shipping_edited_payment_amount, 2), $body);
		$mail->Send();
	}
	
	/**
	 * For pending orders page
	 * @return string Button code, payment link or "payment done" message
	 */
	public function getEditShippingButtonCode() {
		if (!$this->shipping_edited){
			return '<input type="button" rel="'.$this->id.'" class="switch_shipping_method" value="Edit shipping info" />';
		}
		
		if ($this->shipping_edited_payment_done){
			return 'Shipping edited, payment done: $'.number_format($this->shipping_edited_payment_amount, 2);
		} else {
			if ($this->shipping_edited_payment_amount > 0){
				$link = Yii::app()->getBaseUrl(true).'/index.php/customer/additional_payment/?se=1&id='.$this->id.'&code='.$this->shipping_edited_payment_code;
				$str  = '<a href="'.$link.'">';
				$str .= 'Shipping edited, payment required: $'.number_format($this->shipping_edited_payment_amount);
				$str .= '</a>';
				return $str;
			} else {
				return 'Shipping edited, no payment required';
			}
		}
	}
	
	/**
	 * Returns the form code for scanning items/shipments/ out
	 * Form contains website and tracking number fields
	 */
	public static function getScanOutFormCode(){
		Yii::import('application.controllers.CustomerController');
		
		$controller = new CustomerController('ScanOutFancy');
		$controller->widget('application.extensions.fancybox.EFancyBox', array(
				'target'=>'#scan_out_form_button',
				'config'=>array(
						'width'=>270,
						'height'=>400,
						'autoDimensions'=>false,
				),
		), true);
		
		$scan_out_form  = '<h3>Enter Shipping Info</h3>';
		$scan_out_form .= '<label for="scan_out_tracking_number">Tracking Number: </label><br />';
		$scan_out_form .= '<input type="text" id="scan_out_tracking_number" /><br /><br />';
		
		$scan_out_form .= '<label for="scan_out_website">Tracking Website: </label><br />';
		$scan_out_form .= '<input type="text" id="scan_out_website" /><br /><br />';
		
		$scan_out_form .= '<label for="scan_out_website">Shipping Company: </label><br />';
		$scan_out_form .= '<input type="text" id="shipping_company" /><br /><br />';
		
		$scan_out_form .= '<label for="scan_out_website">Phone number: </label><br />';
		$scan_out_form .= '<input type="text" id="phone_number" /><br /><br />';

        $scan_out_form .= '<label for="shipment_cost">Shipment cost: </label><br />';
        $scan_out_form .= '<input type="text" id="shipment_cost" name="shipment_cost" /><br /><br />';

		$scan_out_form .= '<input type="button" value="Save" id="scan_out_form_submit" /><br />';
		
		$str  = '<input type="button" value="Scan Out Form" href="#scan_out_form_div" id="scan_out_form_button" style="display:none;"/>';
		$str .= '<div class="fancy_box_div_wrapper" style="display:none;">
		<div id="scan_out_form_div" class="fancy_box_div">';
		$str .= $scan_out_form;
		$str .= '</div></div>';
		
		return $str;
	}

	public function getItemStatsTableNotes(){
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_id', $this->id);
		$criteria->order = 't.id';
		$shipments = CustomerOrderShipment::model()->findAll($criteria);

		$s_actions = array();
		foreach ($shipments as $shipment){
			switch ($shipment->status){
				case CustomerOrderShipment::$STATUS_PENDING: 
					$s_actions[$shipment->id] = array('cancel', 'scan_as_out');
					if ($shipment->readyForShipping()){
						if ($this->shipping_method == self::SHIPPING_METHOD_LOCAL_PICKUP){
							$s_actions[] = 'schedule_pickup';
						} else {
							$s_actions[] = 'send_for_bidding';
						}
					}
					break;
				case CustomerOrderShipment::$STATUS_COMPLETED:
					$s_actions[$shipment->id] = array();
					if (!$shipment->claim){
							$s_actions[$shipment->id][] = 'mark_as_damaged';
					}
					break;
				case CustomerOrderShipment::$STATUS_BIDDING:
					$s_actions[$shipment->id] = array('requeue');
					break;
				case CustomerOrderShipment::$STATUS_SHIPPER_PICKUP:
					$s_actions[$shipment->id] = array('requeue', 'enter_tracking_number');
					break;
					
				default: $s_actions[$shipment->id] = array();
			}
		}
		//echo '<!-- order ID '.$this->id.'-->';
		$criteria = new CDbCriteria();
		$criteria->compare('customer_order_id', $this->id);
		$order_items = CustomerOrderItems::model()->findAll($criteria);
		
		foreach ($order_items as $items){
			$items_ids[] = $items->id;
		}
		
		$criteria = new CDbCriteria();
		$criteria->addInCondition('customer_order_items_id', $items_ids);
		$criteria->order = 't.customer_order_shipment_id';
		$criteria->with = 'items';
		$criteria->together = true;
		
		$i_items = Individualitem::model()->findAll($criteria);
		if (!$i_items){
			$i_items = array();
		}
		$controller = new CustomerController('');
		return $controller->renderPartial('customer/_order_item_stats_table_notes', array(
				'shipments'=>$this->shipments,
				'order'=>$this,
				'i_items'=>$i_items,
				's_actions'=>$s_actions,
		), true);
	}

	/**
	 *
	 * @param int $type
	 * @param float $amount
	 * @param string $date current date is used if nothing given
	 */
	public function addCustomerPayment($type, $amount, $date = null){
		$time = $date ? strtotime($date) : time();

		$payment 				= new CustomerPayment();
		$payment->customer_id 	= $this->customer_id;
		$payment->type 			= $type;
		$payment->item_id 		= $this->id;
		$payment->amount 		= $amount;
		$payment->date 			= date('Y-m-d H:i:s', $time);
		
		return $payment->save();
	}

	/**
	 * sets fake statuses for all items in all shipments
	 * 
	 * @param bool $all if true - use all items, if false - only active ones
	 */
	public function setShipmentItemsFakeStatus($all = false){
		foreach ($this->shipments as $shipment) {
			$shipment->setItemsFakeStatus($all);
		}
	}

    /**
     * Returns total amount of all payments made by customer:
     * main order payment, additional (beyond point) minus refunds
     */
    public function getTotalPaidAmount(){
        if (!$this->_total_paid_amount){
            $shipping_edited        = $this->total_shipping_edited_paid;
            $refunds                = $this->total_refunded;
            $main_payment           = $this->discounted_total;
            $additional_payments    = 0;
            foreach($this->shipments as $shipment){
                $additional_payments += $shipment->getTotalAdditionalPaymentAmount();
            }

            $this->_total_paid_amount = $main_payment + $shipping_edited + $additional_payments - $refunds;
        }

        return $this->_total_paid_amount;
    }

    /**
     * Get order profit
     * @return float
     */
    public function getProfit()
    {
        $calcLog = "Calculating profit for Order#{$this->id}\n";
        if ($this->status !== 'Sold') {
            echo nl2br($calcLog."Profit can be calculated only for Sold orders");
            return false;
        }
        $profit = 0;
        $totalFlatShipping = 0;
        foreach ($this->order_items as $orderItem) {
            $calcLog .= "Order item #{$orderItem->id}\n";
            $profit += $orderItem->itemsProfit;
            $calcLog .= "Item Profit: " . Yii::app()->user->getState('calcLog') . "\n";
            $totalFlatShipping += $orderItem->flatShippingFee;
            $calcLog .= "Item TotalFlatShipping: " . $orderItem->flatShippingFee . "\n";
        }
        $calcLog .= "ItemsProfit: $profit\n";
        $calcLog .= "TotalFlatShipping: $totalFlatShipping\n";
        $totalShippingQuote = $this->getTotalShippingQuote();
        $calcLog .= "TotalShippingQuote: " . $this->getTotalShippingQuote()
            . "\n" . Yii::app()->user->getState('shippingQuoteLog');
        $discount = $this->grand_total_price - $this->discounted_total;
        $calcLog .= "Discount: $discount\n";
        if ($totalShippingQuote == 0) {
            $profit += $totalFlatShipping * self::SHIPPING_PROFIT_RATE - $discount;
            $calcLog .= "Profit = ItemsProfit + TotalFlatShipping * SHIPPING_PROFIT_RATE - Discount\n";
        } else {
            if (($totalFlatShipping/$totalShippingQuote - 1) >= self::SHIPPING_PROFIT_RATE) {
                $profit += $totalFlatShipping - $discount - $totalShippingQuote;
                $calcLog .= "Profit = ItemsProfit + TotalFlatShipping - Discount - TotalShippingQuote\n";
            } else {
                $profit += $totalShippingQuote * self::SHIPPING_PROFIT_RATE - $discount;
                $calcLog .= "Profit = ItemsProfit + TotalShippingQuote *  SHIPPING_PROFIT_RATE - Discount\n";
            }
        }
        $calcLog .= "Order profit: $profit";
        echo nl2br($calcLog);
        return $profit;
    }

    /**
     * Get total shipping quote
     * @return float
     */
    public function getTotalShippingQuote()
    {
        $totalShippingQuote = 0;
        $log = '';
        foreach ($this->shipments as $shipment) {
            //var_dump($shipment->id);
            $totalShippingQuote += $shipment->getSelectedUserBidRateQuote();
            $log .= "[Shipment#{$shipment->id} Quote: {$shipment->selectedUserBidRateQuote}] ";
        }
        Yii::app()->user->setState('shippingQuoteLog', $log);
        return $totalShippingQuote;
    }


    public function addItem($itemId, $quantity, $customColor, $specialShippingPrice = 0)
    {
        $item = Item::model()->findByPk($itemId);
        if (empty($item)) return false;

        $customerOrderItem = new CustomerOrderItems();
        $customerOrderItem->setAttributes(array(
            "customer_order_id"			=>	$this->id,
            "item_id"					=>	$itemId,
            "quantity"					=>	$quantity,
            "custom_color"				=>	$customColor,
            "exw_cost_price"			=>	$item->exw_cost_price,
            "fob_cost_price"			=>	$item->fob_cost_price,
            "sale_price"				=>	$item->sale_price,
            "special_shipping_price"    =>  $specialShippingPrice,
        ));
        return $customerOrderItem->save();
    }

    public function getDescription()
    {
        if (!isset($this->_description)) {
            foreach ($this->order_items as $item) {
                $itemInfo = Item::model()->findByPk($item->item_id);
                $items_text[] = $itemInfo->item_name.': '.$item->quantity;
            }
            $this->_description = isset($items_text) ? implode(',', $items_text) : '';
        }

        return $this->_description;
    }

    /**
     * Sends confirmation of success transaction to customer
     */
    public function sendTransactionConfirmation()
    {
        $itemDescriptions = array();
        foreach ($this->order_items as $item) {
            $itemInfo = Item::model()->findByPk($item->item_id);
            $itemDescriptions[] = "{$item->quantity} x {$itemInfo->item_code} ({$itemInfo->item_name})";
        }
        AutoMail::sendMail($this->customer->email, AutoMailMessage::MS_TRANSACTION_CONFIRMATION,
            strtr(AutoMailMessage::MC_TRANSACTION_CONFIRMATION, array(
                '[name]' => $this->customer->fullname,
                '[items]' => implode("<br>", $itemDescriptions),
                //'[company]' => Settings::getVar('company_name') . ', ' . Settings::getVar('company_name_2'),
                '[company]' => isset($this->payment_processor) ? $this->payment_processor : Settings::getVar('payment_processor'),
            ))
        );
    }

    public function getItemNames()
    {
        foreach ($this->individual_items as $item) {
            $itemIds[] = $item->item_id;
        }
        if (isset($itemIds)) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $itemIds);
            $criteria->select = 'item_name';
            $items = Item::model()->findAll($criteria);
            foreach ($items as $item) $result[] = $item->item_name;
            return isset($result) ? $result : array();
        } else return array();
    }
}