<?php



/**

 * This is the model class for table "retailer_user_customer".

 *

 * The followings are the available columns in table 'retailer_user_customer':


 * @property integer $id


 * @property integer $user_id


 * @property integer $customer_id



 */

class RetailerUserCustomer extends CActiveRecord

{

	/**

	 * Returns the static model of the specified AR class.

	 * @param string $className active record class name.

	 * @return RetailerUserCustomer the static model class

	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	/**

	 * @return string the associated database table name

	 */

	public function tableName()

	{

		return 'retailer_user_customer';

	}



	/**

	 * @return array validation rules for model attributes.

	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(


			array('user_id, customer_id', 'required'),


			array('user_id, customer_id', 'numerical', 'integerOnly'=>true),


			// The following rule is used by search().

			// Please remove those attributes that should not be searched.

			array('id, user_id, customer_id', 'safe', 'on'=>'search'),

		);

	}



	/**

	 * @return array relational rules.

	 */

	public function relations()

	{

		// NOTE: you may need to adjust the relation name and the related

		// class name for the relations automatically generated below.

		return array(


		);

	}



	/**

	 * @return array customized attribute labels (name=>label)

	 */

	public function attributeLabels()

	{

		return array(


			'id' => 'ID',


			'user_id' => 'User',


			'customer_id' => 'Customer',


		);

	}



	/**

	 * Retrieves a list of models based on the current search/filter conditions.

	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.

	 */

	public function search()

	{

		// Warning: Please modify the following code to remove attributes that

		// should not be searched.



		$criteria=new CDbCriteria;



		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('customer_id',$this->customer_id);



		return new CActiveDataProvider($this, array(

			'criteria'=>$criteria,

		));

	}

    public static function getRetailerCustomerId($userId) {
        $retailerCustomerRel = self::model()->findByAttributes(array('user_id' => $userId));
        if (isset($retailerCustomerRel)) return $retailerCustomerRel->customer_id;
        $profile = YumProfile::model()->findByAttributes(array('user_id' => $userId));
        //check if customer with this email exist
        $customer = Customer::model()->findByAttributes(array('email' => $profile->email));
        if (empty($customer)) {
            $customer = new Customer();
            $customer->first_name = $profile->firstname;
            $customer->last_name = $profile->lastname;
            $customer->email = $profile->email;
            if (isset($profile->phone)) $customer->phone = $profile->phone;
            $customer->heard_through = 'Website';
            if (!$customer->save())
                Yii::log('Error saving customer model:'.json_encode($customer->errors));
        }

        $relModel = new RetailerUserCustomer();
        $relModel->setAttributes(array(
            'user_id' => $userId,
            'customer_id' => $customer->id,
        ));
        $relModel->save();
        return $customer->id;
    }
}