<?php

/**
 * This is the model class for table "supplier_order".
 *
 * The followings are the available columns in table 'supplier_order':
 * @property string $id
 * @property string $supplier_id
 * @property string $created_date
 * @property string $accepted_date
 * @property string $advanced_paid_date
 * @property string $completed_date
 * @property string $status
 * @property string $etl
 * @property string $etl_set_date
 * @property string $full_paid_date
 * @property string $supplier_shipper_id
 * @property string $choose_shipper_date
 * @property double $advanced_paid_amount
 * @property string $advanced_paid_attach_file_path
 * @property string $full_paid_attach_file_path
 * @property string $container_id
 * @property string $operator_id
 * @property string $loaded
 */
class SupplierOrder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierOrder the static model class
	 */
	 
	public static $STATUS_WAITING_APPROVAL  = "Waiting Approval";
	public static $STATUS_PENDING		    = "Pending";
	public static $STATUS_DENIED		    = "Denied";
	public static $STATUS_ACCEPTED		    = "Accepted / 30% notice sent";
	public static $STATUS_ADVANCED_PAID	    = "Advanced Paid";
	public static $STATUS_MAKING		    = "Making";
	public static $STATUS_COMPLETED		    = "Completed";
	public static $STATUS_FULL_PAID		    = "Full Paid";
	public static $STATUS_ARCHIVED		    = "Archived";

	public static $LS_LOADED			    = "Loaded";
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_id, created_date, operator_id', 'required'),
			array('advanced_paid_amount, discount_amount', 'numerical'),
			array('supplier_id, supplier_shipper_id, container_id, operator_id', 'length', 'max'=>10),
			array('status', 'length', 'max'=>100),
			array('advanced_paid_attach_file_path, full_paid_attach_file_path', 'length', 'max'=>1000),
			array('loaded', 'length', 'max'=>45),
			array('accepted_date, advanced_paid_date, completed_date, eta, etl_set_date, full_paid_date, choose_shipper_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, supplier_id, created_date, accepted_date, advanced_paid_date, completed_date, status, eta, etl_set_date, full_paid_date, supplier_shipper_id, choose_shipper_date, advanced_paid_amount, advanced_paid_attach_file_path, full_paid_attach_file_path, container_id, operator_id, loaded', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'supplier_order_container'=>array(self::BELONGS_TO, 'SupplierOrderContainer', 'container_id'),
            'supplier'=>array(self::BELONGS_TO, 'YumUser', 'supplier_id'),
			'order_items'=>array(self::HAS_MANY, 'SupplierOrderItems', 'supplier_order_id'),
			'individual_items'=>array(self::HAS_MANY, 'Individualitem', 'supplier_order_id'),
			'official_docs'=>array(self::HAS_MANY, 'SupplierOrderOfficialDocs', 'supplier_order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'supplier_id' => 'Supplier',
			'created_date' => 'Created Date',
			'accepted_date' => 'Accepted Date',
			'advanced_paid_date' => 'Advanced Paid Date',
			'completed_date' => 'Completed Date',
			'status' => 'Status',
			'discount_amount' => 'Discount Amount',
			'etl' => 'E.T.L',
			'etl_set_date' => 'E.T.L Set Date',
			'full_paid_date' => 'Full Paid Date',
			'supplier_shipper_id' => 'Supplier Shipper',
			'choose_shipper_date' => 'Choose Shipper Date',
			'advanced_paid_amount' => 'Advanced Paid Amount',
			'advanced_paid_attach_file_path' => 'Advanced Paid Attach File Path',
			'full_paid_attach_file_path' => 'Full Paid Attach File Path',
			'container_id' => 'Container',
			'operator_id' => 'Operator',
			'loaded' => 'Loaded',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($supplier_id = 0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		if ($supplier_id > 0){
			$this->supplier_id = $supplier_id;
            $criteria->compare('status', '<>'.SupplierOrder::$STATUS_DENIED);
		}
        
        $criteria->compare('status', '<>'.SupplierOrder::$STATUS_DENIED);
        
        $criteria->compare('deleted', '<>1');
        
		//$criteria->with = array('supplier_order_container');
		$criteria->compare('id',$this->id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('created_date',$this->created_date);
		$criteria->compare('accepted_date',$this->accepted_date);
		$criteria->compare('advanced_paid_date',$this->advanced_paid_date);
		$criteria->compare('completed_date',$this->completed_date);
		$criteria->compare('etl',$this->etl);
		$criteria->compare('etl_set_date',$this->etl_set_date);
		$criteria->compare('full_paid_date',$this->full_paid_date);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id);
		$criteria->compare('choose_shipper_date',$this->choose_shipper_date);
		$criteria->compare('advanced_paid_amount',$this->advanced_paid_amount);
		$criteria->compare('advanced_paid_attach_file_path',$this->advanced_paid_attach_file_path);
		$criteria->compare('full_paid_attach_file_path',$this->full_paid_attach_file_path);
		//$criteria->compare('container_id',$this->container_id,true);
		$criteria->compare('operator_id',$this->operator_id);
		$criteria->compare('status', '<>'.SupplierOrder::$STATUS_ARCHIVED);
        $criteria->compare('status', '<>'.SupplierOrder::$STATUS_WAITING_APPROVAL);
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'created_date DESC',
            )
		));
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
     * @param int
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search_archived($supplier_id = 0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		if ($supplier_id > 0){
			$this->supplier_id = $supplier_id;
		}

		//$criteria->with = array('supplier_order_container');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_id',$this->supplier_id,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('accepted_date',$this->accepted_date,true);
		$criteria->compare('advanced_paid_date',$this->advanced_paid_date,true);
		$criteria->compare('completed_date',$this->completed_date,true);
		$criteria->compare('etl',$this->etl,true);
		$criteria->compare('etl_set_date',$this->etl_set_date,true);
		$criteria->compare('full_paid_date',$this->full_paid_date,true);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id,true);
		$criteria->compare('choose_shipper_date',$this->choose_shipper_date,true);
		$criteria->compare('advanced_paid_amount',$this->advanced_paid_amount);
		$criteria->compare('advanced_paid_attach_file_path',$this->advanced_paid_attach_file_path,true);
		$criteria->compare('full_paid_attach_file_path',$this->full_paid_attach_file_path,true);
		//$criteria->compare('container_id',$this->container_id,true);
		$criteria->compare('operator_id',$this->operator_id,true);
		$criteria->compare('status', SupplierOrder::$STATUS_ARCHIVED);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'created_date DESC',
            )
		));
	}
	
	public function search_waiting($supplier_id = 0){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		if ($supplier_id > 0){
			$this->supplier_id = $supplier_id;
		}

		//$criteria->with = array('supplier_order_container');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_id',$this->supplier_id,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('accepted_date',$this->accepted_date,true);
		$criteria->compare('advanced_paid_date',$this->advanced_paid_date,true);
		$criteria->compare('completed_date',$this->completed_date,true);
		$criteria->compare('etl',$this->etl,true);
		$criteria->compare('etl_set_date',$this->etl_set_date,true);
		$criteria->compare('full_paid_date',$this->full_paid_date,true);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id,true);
		$criteria->compare('choose_shipper_date',$this->choose_shipper_date,true);
		$criteria->compare('advanced_paid_amount',$this->advanced_paid_amount);
		$criteria->compare('advanced_paid_attach_file_path',$this->advanced_paid_attach_file_path,true);
		$criteria->compare('full_paid_attach_file_path',$this->full_paid_attach_file_path,true);
		//$criteria->compare('container_id',$this->container_id,true);
		$criteria->compare('operator_id',$this->operator_id,true);
		$criteria->compare('status', SupplierOrder::$STATUS_WAITING_APPROVAL);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchByDates($date_from, $date_to){
        $criteria=new CDbCriteria;
        $criteria->order = 'id DESC';

        if ($date_from && $date_to){
            $criteria->addCondition('created_date >= "'.$date_from.'"');
            $criteria->addCondition('created_date <= ADDDATE("'.$date_to.'", 1)');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            ),
        ));
    }

	public static function getSupplierOrderTotalPrice($supplier_order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplier_order_id);		
		
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('status', "Accepted");
		$criteriaItems->compare('supplier_order_id', $supplier_order_id);
		$items = SupplierOrderItems::model()->findAll($criteriaItems);
		
		$price = 0;
		foreach ($items as $item){
			$cost_price = $item->exw_cost_price;
			$container_id = $supplierOrderModel->container_id;
			if ($container_id != 0){
				$conatinerModel = SupplierOrderContainer::model()->findByPk($container_id);
				if ($conatinerModel != NULL){
					if ($conatinerModel->terms == SupplierOrderContainer::$TERMS_FOB){
						$cost_price = $item->fob_cost_price;
					}
				}
			}
			
			$price += floatval($cost_price) * intval($item->quantity);			
		}
		return $price - intval($supplierOrderModel->discount_amount);
	}
	
	public static function getSupplierOrderTotalExwPrice($supplier_order_id){
		$order = SupplierOrder::model()->findByPk($supplier_order_id);
		$criteriaItems = new CDbCriteria;
		if ($order->status != SupplierOrder::$STATUS_PENDING){
			$criteriaItems->compare('status', "Accepted");
		}
		$criteriaItems->compare('supplier_order_id',$supplier_order_id);
		$items = SupplierOrderItems::model()->findAll($criteriaItems);
		
		$price = 0;
		foreach ($items as $item){
			$price += floatval($item->exw_cost_price) * intval($item->quantity);			
		}
		return $price;
	}

    public function getEXWTotal(){
        $criteriaItems = new CDbCriteria;
        if ($this->status != SupplierOrder::$STATUS_PENDING){
            $criteriaItems->compare('status', "Accepted");
        }
        $criteriaItems->compare('supplier_order_id',$this->id);
        $items = SupplierOrderItems::model()->findAll($criteriaItems);

        $price = 0;
        foreach ($items as $item){
            $price += floatval($item->exw_cost_price) * intval($item->quantity);
        }
        return $price;
    }

	public static function getSupplierOrderTotalFobPrice($supplier_order_id){
		$order = SupplierOrder::model()->findByPk($supplier_order_id);
		$criteriaItems = new CDbCriteria;
		if ($order->status != SupplierOrder::$STATUS_PENDING){
			$criteriaItems->compare('status', "Accepted");
		}
		$criteriaItems->compare('supplier_order_id',$supplier_order_id);
		$items = SupplierOrderItems::model()->findAll($criteriaItems);
		
		$price = 0;
		foreach ($items as $item){
			$price += floatval($item->fob_cost_price) * intval($item->quantity);			
		}
		return $price;
	}

    public function getFOBTotal(){
        // TODO: use this function instead of static crap everywhere
        // TODO: rewrite these functions using relations and single private function to avoid copy-paste
        $criteriaItems = new CDbCriteria;
        if ($this->status != SupplierOrder::$STATUS_PENDING){
            $criteriaItems->compare('status', "Accepted");
        }
        $criteriaItems->compare('supplier_order_id',$this->id);
        $items = SupplierOrderItems::model()->findAll($criteriaItems);

        $price = 0;
        foreach ($items as $item){
            $price += floatval($item->fob_cost_price) * intval($item->quantity);
        }
        return $price;
    }

    public function getSupplierOrderFobBalanceString(){
        if ($this->full_paid_date != NULL){
            return '$0';
        }
        $total = SupplierOrder::model()->getSupplierOrderTotalFobPrice($this->id);
        $res = '$'.number_format($total, 2);
        if (!$this->advanced_paid_amount && !$this->discount_amount){
            return $res;
        }
        
        if ($this->discount_amount){
            $res = '('.$res.' - $'.number_format($this->discount_amount, 2).')';
            $total = $total - $this->discount_amount;
        }
        
        if ($this->advanced_paid_amount){
            $res = $res.' - $'.number_format($this->advanced_paid_amount, 2);
            $total = $total - $this->advanced_paid_amount;
        }
        
        $res = $res.' = $'.number_format($total, 2);
        return $res;
    }
	
    public function getSupplierOrderExwBalanceString(){
        if ($this->full_paid_date != NULL){
            return '$0';
        }
        $total = SupplierOrder::model()->getSupplierOrderTotalExwPrice($this->id);
        $res = '$'.number_format($total, 2);
        if (!$this->advanced_paid_amount && !$this->discount_amount){
            return $res;
        }
        
        if ($this->discount_amount){
            $res = '('.$res.' - $'.number_format($this->discount_amount, 2).')';
            $total = $total - $this->discount_amount;
        }
        
        if ($this->advanced_paid_amount){
            $res = $res.' - $'.number_format($this->advanced_paid_amount, 2);
            $total = $total - $this->advanced_paid_amount;
        }
        
        $res = $res.' = $'.number_format($total, 2);
        return $res;
    }
    
    public function getSupplierOrderBalanceString(){
        return ($this->getShippingTerms() == 'FOB' ? '<span class="balance_highlight">' : '')
                .'<strong>FOB:</strong><br />'.$this->getSupplierOrderFobBalanceString()
                .($this->getShippingTerms() == 'FOB' ? '</span>' : '')
                .'<br /><br />'
                .($this->getShippingTerms() == 'EXW' ? '<span class="balance_highlight">' : '')
                .'<strong>EXW:</strong><br />'.$this->getSupplierOrderExwBalanceString()
                .($this->getShippingTerms() == 'EXW' ? '</span>' : '');
    }
    
	public static function getSupplierOrderTotalCBM($supplier_order_id){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('status', "<>Denied");
		$criteriaItems->compare('supplier_order_id',$supplier_order_id);
		$items = SupplierOrderItems::model()->findAll($criteriaItems);
		
		$cbm = 0;
		foreach ($items as $item){
//			$item_id = $item->item_id;
//			$item_info = Item::model()->findByPk($item_id);
			
			$cbm += $item->getTotalCBM();
		}
		return $cbm;
	}
	
    public static function getItemsPopupCode($order_id, $controller = null){
        if (!$controller){
            $controller = new CController('SupplierOrderFancy');
        }
        $controller->widget('application.extensions.fancybox.EFancyBox', array(
            'target'=>'#open_order_contents_'.$order_id,
            'config'=>array(
                'width'=>900,
                'height'=>400,
                'autoDimensions'=>false,
            ),
        ), true);

        $str  = '<div class="fancy_box_div_wrapper" style="display:none;">';
        $str .= '<div id="order_contents_'.$order_id.'" class="fancy_box_div">';
        
        $criteriaItems = new CDbCriteria;
        $criteriaItems->compare('supplier_order_id',$order_id);
        $criteriaItems->compare('status','<>'.SupplierOrder::$STATUS_DENIED);
        $items = SupplierOrderItems::model()->findAll($criteriaItems);
        
        $strItems = '<table class="item_list">'
            .'<thead>'
            .'<th class="item_code">Item Code</td>'
            .'<th class="color">Color</td>'
            .'<th class="exw_price">EXW Price</td>'
            .'<th class="fob_price">Fob Price</td>'
            .'<th class="cbm">CBM</td>'
            .'<th class="quantity">Qty</td>'
            .'<th class="custom_color">Custom Color</td>'
            .'<th class="added_date">Added Date</td>'
            .'<th class="total_exw_price">Total EXW Price</td>'
            .'<th class="total_fob_price">Total Fob Price</td>'
            .'<th class="total_cbm">Total CBM</td>'
            .'<th class="status">Status</td>'				
            .'</thead>';
        
        foreach ($items as $item){			
            $item_id = $item->item_id;
            $item_info = Item::model()->findByPk($item_id);
            
            $item_toal_exw_price = floatval($item->exw_cost_price) * floatval($item->quantity);
            $item_toal_fob_price = floatval($item->fob_cost_price) * floatval($item->quantity);
            $item_toal_cbm = floatval($item_info->cbm) * floatval($item->quantity);
            
            $strStatus = "<b>".$item->status."</b><br/>".date("M. d, Y", strtotime($item->processed_date));
            $strItems .= "<tr>";
            $strItems .= "<td class='item_code'>".$item_info->item_code."</td>";
            $strItems .= "<td class='color'>".$item_info->color."</td>";
            $strItems .= "<td class='exw_price'>".$item->exw_cost_price."</td>";
            $strItems .= "<td class='fob_price'>".$item->fob_cost_price."</td>";
            $strItems .= "<td class='cbm'>".$item_info->cbm."</td>";
            $strItems .= "<td class='quantity'>".$item->quantity."</td>";
            $strItems .= "<td class='custom_color'>".$item->custom_color."</td>";
            $strItems .= "<td class='added_date'>".date("M. d, Y", strtotime($item->added_date))."</td>";
            $strItems .= "<td class='total_exw_price'>".$item_toal_exw_price."</td>";
            $strItems .= "<td class='total_fob_price'>".$item_toal_fob_price."</td>";
            $strItems .= "<td class='total_cbm'>".$item_toal_cbm."</td>";
            $strItems .= "<td class='status'>".$strStatus."</td>";
            $strItems .= "</tr>";
        }
        $strItems .= "</table>";
        
        $str .= $strItems;
        
        $str .= '</div>';
        $str .= '</div>';
        
        return $str;
    }
    
    public static function getItemsPopupCodeAdmin($order_id, $controller = null){
        if (!$controller){
            $controller = new CController('SupplierOrderFancy');
        }
        $controller->widget('application.extensions.fancybox.EFancyBox', array(
            'target'=>'#open_order_contents_'.$order_id,
            'config'=>array(
                'width'=>900,
                'height'=>400,
                'autoDimensions'=>false,
            ),
        ), true);
        
        $str  = '<div class="fancy_box_div_wrapper" style="display:none;">';
        $str .= '<div id="order_contents_'.$order_id.'" class="fancy_box_div">';
        $str .= SupplierOrder::getSupplierOrderItems($order_id, true);
        $str .= '</div>';
        $str .= '</div>';
        
        return $str;
    }
    
    
    public static function getItemsPopupCodeSupplier($order_id, $controller = null){
        if (!$controller){
            $controller = new CController('SupplierOrderFancy');
        }
        $controller->widget('application.extensions.fancybox.EFancyBox', array(
            'target'=>'#open_order_contents_'.$order_id,
            'config'=>array(
                'width'=>850,
                'height'=>500,
                'autoDimensions'=>false,
            ),
        ), true);
        
        $str  = '<div class="fancy_box_div_wrapper" style="display:none;">';
        $str .= '<div id="order_contents_'.$order_id.'" class="fancy_box_div">';
        $str .= SupplierOrder::getSupplierOrderItems($order_id);
        $str .= '</div>';
        $str .= '</div>';
        
        return $str;
    }
    
	public static function getSupplierOrderStatus($supplier_order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplier_order_id);
		
		if (($supplierOrderModel->status == SupplierOrder::$STATUS_PENDING)
				|| ($supplierOrderModel->status == SupplierOrder::$STATUS_ACCEPTED)){
			return '<b>'.$supplierOrderModel->status.'</b>';
		}
		
		$result = '<b>'.$supplierOrderModel->status.'</b>';
		if ($supplierOrderModel->advanced_paid_attach_file_path != ""){
			$files = explode('||',$supplierOrderModel->advanced_paid_attach_file_path);
            $result .= '<br/>Advanced Paid On <br/><a target="blank" href="'.Yii::app()->getBaseUrl(true)."/"
				.$files[0].'">'
				.date("M. d, Y", strtotime($supplierOrderModel->advanced_paid_date)).'</a>'
				.'<br />($'.$supplierOrderModel->advanced_paid_amount.')<br />';
		} else if ($supplierOrderModel->advanced_paid_date != NULL ){
			$result .= '<br/>Advanced Paid On <br/>'.date("M. d, Y", strtotime($supplierOrderModel->advanced_paid_date))
                        .'<br />($'.$supplierOrderModel->advanced_paid_amount.')<br />';
		}
		
		if ($supplierOrderModel->full_paid_attach_file_path != ""){
			$result .= '<br/>Full Paid On <br/><a target="blank" href="'.Yii::app()->getBaseUrl(true)."/"
				.$supplierOrderModel->full_paid_attach_file_path.'">'
				.date("M. d, Y", strtotime($supplierOrderModel->full_paid_date)).'</a>';
		} else if ($supplierOrderModel->full_paid_date != NULL ){
			$result .= '<br/>Full Paid On <br/>'.date("M. d, Y", strtotime($supplierOrderModel->full_paid_date));
		}
		
		// if ($supplierOrderModel->status != SupplierOrder::$STATUS_FULL_PAID){
			// $total_price = SupplierOrder::model()->getSupplierOrderTotalPrice($supplier_order_id);
			// $balance = $total_price - $supplierOrderModel->advanced_paid_amount;
			// $result .= "<br/>Balance:".$balance;
		// }
		
		return $result;
	}
	
	public function getCurrentAdvancePaymentAmount(){
        $total_price = SupplierOrder::model()->getSupplierOrderTotalExwPrice($this->id);
		return round($total_price * 0.3, 2);
    }
    
	public static function getSupplierOrderItems($supplier_order_id, $with_delete = false){
		$order = SupplierOrder::model()->findByPk($supplier_order_id);
		
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.status !="'.SupplierOrderItems::$STATUS_DENIED.'"');
		$criteria->compare('supplier_order_id', $order->id);
		$criteria->with = 'items';
		$criteria->together = true;
		$criteria->order = 'not_loaded_reordered, is_replacement, items.item_name';
		
		Yii::import('application.controllers.SupplierorderController');
        $controller = new SupplierorderController('');
		return $controller->renderPartial('supplierorder/_order_items_list', array(
				'order'=>$order,
				'order_items'=>SupplierOrderItems::model()->findAll($criteria),
		), true);
	}
	
	public static function getActionButtonString($supplier_order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplier_order_id);
		$strButton = "";
		
		$isSupplier = YumUser::model()->isSupplier(false);
		$isAdmin    = Yii::app()->user->isAdmin();
		     
		if ($supplierOrderModel->status == SupplierOrder::$STATUS_PENDING){
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>'
						.'<input class="accept_order" rel="'.$supplier_order_id.'" type="button" value="Accept Order"/><br/>'
						.'<input class="deny_order" rel="'.$supplier_order_id.'" type="button" value="Deny Order"/>';
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/>';
                        
                if ($isAdmin){
                    $strButton .= '<br /><input class="delete_pending_order" rel="'.$supplier_order_id.'" type="button" value="Delete"/>';
                }
			}
		} else if ($supplierOrderModel->status == SupplierOrder::$STATUS_ACCEPTED){
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>';
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>'
						.'<input class="pay_advanced" rel="'.$supplier_order_id.'" type="button" value="Pay Advanced"/>';
			}
		} else if ($supplierOrderModel->status == SupplierOrder::$STATUS_ADVANCED_PAID){
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>'
						.'<input class="set_eta" rel="'.$supplier_order_id.'" type="button" value="Set E.T.L"/>';
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>';
                $strButton .= '<br/><input class="change_status" data-status="advanced_paid" rel="'.$supplier_order_id.'" type="button" value="Change Status"/>';
            }
		} else if ($supplierOrderModel->status == SupplierOrder::$STATUS_MAKING){
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>'
						.'<input class="complete" rel="'.$supplier_order_id.'" type="button" value="Complete"/>';
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>';
                $strButton .= '<br/><input class="change_status" data-status="making" rel="'.$supplier_order_id.'" type="button" value="Change Status"/>';
			}
		} else if ($supplierOrderModel->status == SupplierOrder::$STATUS_COMPLETED){
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>';
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/>';
				
				if ($supplierOrderModel->container_id != 0){
					$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($supplierOrderModel->container_id);
					if ($supplierOrderContainerModel != null){
						if( $supplierOrderContainerModel->status == SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER){
							$strButton .= '<br/><input class="pay_remaining" rel="'.$supplier_order_id.'" type="button" value="Pay Remaining"/>';
						}
					}
				}
                $strButton .= '<br/><input class="change_status" data-status="completed" rel="'.$supplier_order_id.'" type="button" value="Change Status"/>';
			}
		} else if ($supplierOrderModel->status == SupplierOrder::$STATUS_FULL_PAID){
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/>';
				if ($supplierOrderModel->loaded == SupplierOrder::$LS_LOADED){
					$strButton .= '<br/><input class="upload_documents" rel="'.$supplier_order_id.'" type="button" value="Upload Official Documents"/>';
				}
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/>';
				$strButton .= '<input class="order_archive" rel="'.$supplier_order_id.'" type="button" value="Archive"/>';
			}
		} else if ($supplierOrderModel->status == SupplierOrder::$STATUS_ARCHIVED) {
			if ($isSupplier){
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/><br/>';
			} else {
				$strButton = '<input class="order_detail" rel="'.$supplier_order_id.'" type="button" value="Detail"/>';
			}
		}  else if ($supplierOrderModel->status == SupplierOrder::$STATUS_DENIED){
            if ($isAdmin){
                $strButton = '<input class="order_resend" rel="'.$supplier_order_id.'" type="button" value="Resend"/><br/>';
                //$strButton .= '<input class="order_delete" rel="'.$supplier_order_id.'" type="button" value="Delete"/>';
            }
        }
		
        if (!$isSupplier && $supplierOrderModel->status != SupplierOrder::$STATUS_FULL_PAID && $supplierOrderModel->status != SupplierOrder::$STATUS_ARCHIVED){
            $strButton .= '<input class="add_items" rel="'.$supplier_order_id.'" type="button" value="Edit Order"/><br />';
        }
        
        if ($isAdmin && $supplierOrderModel->status != SupplierOrder::$STATUS_PENDING){
        	$strButton .= '<br /><input class="order_delete" rel="'.$supplier_order_id.'" type="button" value="Delete"/>';
        }
        
        if ($isAdmin 
        	&& $supplierOrderModel->container_id == 0){
        	       	
        	$strButton .= '<br /><input class="order_combine" rel="'.$supplier_order_id.'" data-supplier="'.$supplierOrderModel->supplier_id.'" type="button" value="Combine"/>';
        }
        
		return $strButton;
	}
	
	public function search_shipping(){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_id',$this->supplier_id,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('accepted_date',$this->accepted_date,true);
		$criteria->compare('advanced_paid_date',$this->advanced_paid_date,true);
		$criteria->compare('completed_date',$this->completed_date,true);
		$criteria->compare('status','Full Paid',true);
		$criteria->compare('etl',$this->etl,true);
		$criteria->compare('etl_set_date',$this->etl_set_date,true);
		$criteria->compare('full_paid_date',$this->full_paid_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function isCombinable($supplier_order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplier_order_id);
		
		if ($supplierOrderModel->container_id != 0){
			return false;
		}
		
		if (($supplierOrderModel->status == SupplierOrder::$STATUS_PENDING)
			|| ($supplierOrderModel->status == SupplierOrder::$STATUS_DENIED)
			|| ($supplierOrderModel->status == SupplierOrder::$STATUS_ACCEPTED)
			|| ($supplierOrderModel->status == SupplierOrder::$STATUS_ADVANCED_PAID)
			){
			return false;
		}
		
		return true;
	}
	
	public static function getSupplierName($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$supplier_profile = YumUser::model()->findByPk($supplierOrderModel->supplier_id)->profile;
		$strName = $supplier_profile->firstname." ".$supplier_profile->lastname
				.'<input type="hidden" class="supplier_original_port" id="supplier_original_port_'.$order_id.'" value="'.$supplier_profile->port_box.'"/>';
		
		return $strName;
	}
	
	public function createSupplierOrderItems($post_data){
		$supplierOrderId = $this->id;
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplierOrderId);
		$supplier_id = $supplierOrderModel->supplier_id;
				
		$itemIdList = $post_data['item']['item_id'];
		$new_quantity = 0;

		// add the items with quantity to the order
		foreach ($itemIdList as $key => $itemId){
			$item = Item::model()->findByPk($itemId);
			$quantity = intval($post_data['item']['quantity'][$key]);
			$custom_color = $post_data['item']['custom_color'][$key];
			
			$attributes = array(
				"supplier_order_id"	=>	$supplierOrderId,
				"item_id"			=>	$itemId,
				"quantity"			=>	$quantity,
				"custom_color"		=>	$custom_color,
				"exw_cost_price"	=>	$item->exw_cost_price,
				"fob_cost_price"	=>	$item->fob_cost_price,
				"sale_price"		=>	$item->sale_price,
				"added_date"		=>	date("Y-m-d H:i:s"),
				"status"			=>	SupplierOrder::$STATUS_PENDING,
				"notes"			=>	$post_data['item']['notes'][$key],
			);
			
			$newSupplierOrderItemsModel = new SupplierOrderItems;
			$newSupplierOrderItemsModel->attributes = $attributes;
			$newSupplierOrderItemsModel->operator_id = Yii::app()->user->id;
			$newSupplierOrderItemsModel->save();
			
			$new_quantity += $quantity;
			// TODO: remove this and use SupplierOrderItems::createIndividualItems()
			while ($quantity > 0){
				$individual_item = new Individualitem();
				$individual_item->attributes = $item->attributes;
				$individual_item->item_id = $item->id;
				$individual_item->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;				
				
				if ($individual_item->save()){
					$individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
					$individualItemSupplierOrderItems->individual_item_id = $individual_item->id;
					$individualItemSupplierOrderItems->supplier_order_items_id = $newSupplierOrderItemsModel->id;
					
					if ($individualItemSupplierOrderItems->save() == false){
						var_dump(CHtml::errorSummary($individualItemSupplierOrderItems));
						exit;
					}
				} else {
					var_dump(CHtml::errorSummary($individual_item));
					exit;
				}
				$quantity--;
			}
			
		}
		/*
		if (isset($post_data["add_pending_loading_quantity"])){
			$arrAddPendingLoadingQuantity = $post_data["add_pending_loading_quantity"];
			if (count($arrAddPendingLoadingQuantity) > 0){
				foreach ($arrAddPendingLoadingQuantity as $item_id => $addPendingLoadingQuantity){
					if ($addPendingLoadingQuantity == 0)
						continue;
						
					$attributes = array(
						"supplier_order_id"	=>	$supplierOrderId,
						"item_id"			=>	$item_id,
						"quantity"			=>	$addPendingLoadingQuantity,
						"exw_cost_price"	=>	0,
						"fob_cost_price"	=>	0,
						"sale_price"		=>	0,
						"added_date"		=>	date("Y-m-d H:i:s"),
						"status"			=>	SupplierOrder::$STATUS_PENDING,
					);
					
					$newSupplierOrderItemsModel = new SupplierOrderItems;
					$newSupplierOrderItemsModel->attributes = $attributes;
					$newSupplierOrderItemsModel->operator_id = Yii::app()->user->id;
					$newSupplierOrderItemsModel->save();
					
					$criteriaOrder = new CDbCriteria;
					$criteriaOrder->compare('supplier_id', $supplier_id);
					$supplierOrderList = SupplierOrder::model()->findAll($criteriaOrder);
					
					$arrSupplierOrderId = array();
					foreach ($supplierOrderList as $supplierOrder){
						$arrSupplierOrderId[] = $supplierOrder->id;
					}
					
					$criteriaInidividualItems = new CDbCriteria;
					$criteriaInidividualItems->compare('loaded', Individualitem::$LS_PENDING);
					$criteriaInidividualItems->compare('item_id', $item_id);
					$criteriaInidividualItems->addInCondition('supplier_order_id', $arrSupplierOrderId);
					
					$individualItemList = Individualitem::model()->findAll($criteriaInidividualItems);
					
					foreach ($individualItemList as $individualItemModel){
						$addPendingLoadingQuantity--;
						if ($addPendingLoadingQuantity < 0){
							break;
						}
						
						$individualItemModel->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;
						
						if ($individualItemModel->save()){
							$individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
							$individualItemSupplierOrderItems->individual_item_id = $individualItemModel->id;
							$individualItemSupplierOrderItems->supplier_order_items_id = $newSupplierOrderItemsModel->id;
							
							if ($individualItemSupplierOrderItems->save() == false){
								var_dump(CHtml::errorSummary($individualItemSupplierOrderItems));
								exit;
							}
						} else {
							var_dump(CHtml::errorSummary($individualItemModel));
							exit;
						}
						$quantity--;
					}
				}
			}
		}
		*/
		if ($new_quantity > 0){
			////////////////////////////////////////////////////////////
			// Added Activity
			Activity::model()->addActivity_CreateNewIndItemsForSupplierOrder($this->id, $new_quantity);
		}
	}
	
	public function modifyChangeItmesStatusAsAccepted(){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('supplier_order_id',$this->id);
		
		$attributes = array(
			'status'=>"Accepted",
			'processed_date'=>date("Y-m-d H:i:s", time())
			);
		
		SupplierOrderItems::model()->updateAll($attributes, $criteriaItems);
		$orderItems = SupplierOrderItems::model()->findAll($criteriaItems);
		
		foreach ($orderItems as $item){
			$criteriaOrderItems = new CDbCriteria;
			$criteriaOrderItems->compare('supplier_order_items_id',$item->id);
			/*$attributeItem = array(
				'status'=>Individualitem::$STATUS_ORDERED_WITHOUT_ETL,
				'supplier_id'=>$this->id
			);*/
			
			//IndividualItemSupplierOrderItems::model()->updateAll($attributeItem, $criteriaOrderItems);
			$individual_item_supplier_order_items = IndividualItemSupplierOrderItems::model()->findAll($criteriaOrderItems);
			foreach ($individual_item_supplier_order_items as $ind_item){
				$ind_item_info = Individualitem::model()->findByPk($ind_item->individual_item_id);				
				$ind_item_info->status = Individualitem::$STATUS_ORDERED_WITHOUT_ETL;
				$ind_item_info->supplier_order_id = $this->id;
				$ind_item_info->save();
			}
		}

        for ($i = 0; $i < count($orderItems); $i++){
            if (!$orderItems[$i]){
                continue;
            }

            $orderItems[$i]->combine();
        }

	}
	
	public function modifyChangeItmesStatusAsSetETA(){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('supplier_order_id', $this->id);
		$criteriaItems->compare('status', 'Accepted');
		
		/*$attributes = array(
			'status'=>"Accepted",
			'processed_date'=>date("Y-m-d H:i:s", time())
			);
		
		SupplierOrderItems::model()->updateAll($attributes, $criteriaItems);*/
		$orderItems = SupplierOrderItems::model()->findAll($criteriaItems);
		
		foreach ($orderItems as $item){
			$criteriaOrderItems = new CDbCriteria;
			$criteriaOrderItems->compare('supplier_order_items_id',$item->id);
			
			$individual_item_supplier_order_items = IndividualItemSupplierOrderItems::model()->findAll($criteriaOrderItems);
			foreach ($individual_item_supplier_order_items as $ind_item){
				$ind_item_info = Individualitem::model()->findByPk($ind_item->individual_item_id);				
				$ind_item_info->status = Individualitem::$STATUS_ORDERED_WITH_ETL;
				$ind_item_info->save();
			}
		}
	}
	
	public function modifyChangeItmesStatusAsDenied(){
		$criteriaItems = new CDbCriteria;
		$criteriaItems->compare('supplier_order_id',$this->id);
		
		$attributes = array(
			'status'=>"Denied",
			'processed_date'=>date("Y-m-d H:i:s", time())
			);
		
		SupplierOrderItems::model()->updateAll($attributes, $criteriaItems);
		$orderItems = SupplierOrderItems::model()->findAll($criteriaItems);
		
		foreach ($orderItems as $item){
			
			$criteriaOrderItems = new CDbCriteria;
			$criteriaOrderItems->compare('supplier_order_items_id',$item->id);
			
			$individual_item_supplier_order_items = IndividualItemSupplierOrderItems::model()->findAll($criteriaOrderItems);
			foreach ($individual_item_supplier_order_items as $ind_item){
				$ind_item_info = Individualitem::model()->findByPk($ind_item->individual_item_id);				
				$ind_item_info->status = Individualitem::$STATUS_NOT_ORDERED;
				$ind_item_info->denied_by_supplier = 1;
				$ind_item_info->save();
			}
		}
	}
	
	public function sendOrderExistingItems($supplier_id, $order_id, $item_list, $item_quantity_list, $item_status_list, $item_custom_color_list){
		$supplierOrder = SupplierOrder::model()->findByPk($order_id);
		if ($supplierOrder == NULL){
			$supplierOrder = new SupplierOrder();
            $supplierOrder->status = SupplierOrder::$STATUS_PENDING;
			$new_order = true;
		}
		
		$operator_id = Yii::app()->user->id;
		$supplierOrder->supplier_id = $supplier_id;
		$supplierOrder->created_date = date("Y-m-d H:i:s", time());
		$supplierOrder->operator_id = $operator_id;
		
		if ($supplierOrder->save()){
			$criteriaItems = new CDbCriteria;
			
			$arrItemList 		= explode("_", $item_list);
			$arrItemQuantity 	= explode("_", $item_quantity_list);
			$arrItemCustomColor = explode("_", $item_custom_color_list);
			$arrItemStatus 		= explode("_", $item_status_list);
			
			foreach ($arrItemQuantity as $index => $item_count){
				$item_id 			= $arrItemList[$index];
				$item_custom_color 	= $arrItemCustomColor[$index];
				$item_model 		= Item::model()->findByPk($item_id);
				$item_sold 			= $arrItemStatus[$index] == 'S';
                
				$supplierOrderItemsModel = new SupplierOrderItems();
				$supplierOrderItemsModel->operator_id = $operator_id;
				
				$attributes = array(
					"supplier_order_id"	=>	$supplierOrder->id,
					"item_id"			=>	$item_id,
					"quantity"			=>	$item_count,
					"custom_color"		=>	$item_custom_color,
					"exw_cost_price"	=>	$item_model->exw_cost_price,
					"fob_cost_price"	=>	$item_model->fob_cost_price,
					"sale_price"		=>	$item_model->sale_price,
					"added_date"		=>	date("Y-m-d H:i:s"),
					"status"			=>	SupplierOrder::$STATUS_PENDING
				);
				
				$supplierOrderItemsModel->attributes = $attributes;
				$supplierOrderItemsModel->save();
				
				$criteria = new CDbCriteria;
				// $criteria->compare('t.status', $item_status, true, "or");
				$criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
				$criteria->compare('t.status', Individualitem::$STATUS_DENIED, true, "or");
                if ($item_sold){
                    $criteria->addCondition('t.sold_date>"0000-00-00 00:00:00"');
                } else {
                    $criteria->addCondition('NOT t.sold_date>"0000-00-00 00:00:00"');
                }
				$criteria->compare('t.item_id', $item_id);
				
				$individualItemList = Individualitem::model()->findAll($criteria);

				foreach ($individualItemList as $individual_item){
					$item_count--;
					
					if ($item_count < 0)
						break;
						
					$individual_item->supplier_order_id = $supplierOrder->id;
					$individual_item->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;
					$individual_item->save();

					$newIndividualItemSupplierOrderItemsModel = new IndividualItemSupplierOrderItems();
					$newIndividualItemSupplierOrderItemsModel->attributes = array(
						'individual_item_id' => $individual_item->id,
						'supplier_order_items_id' => $supplierOrderItemsModel->id,
					);
					
					$newIndividualItemSupplierOrderItemsModel->save();
				}
				
				while ($item_count > 0){
					$individual_item = new Individualitem();
					$individual_item->attributes = $item_model->attributes;
					$individual_item->item_id = $item_model->id;
					$individual_item->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;				
					
					if ($individual_item->save()){
						$individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
						$individualItemSupplierOrderItems->individual_item_id = $individual_item->id;
						$individualItemSupplierOrderItems->supplier_order_items_id = $supplierOrderItemsModel->id;
						
						if ($individualItemSupplierOrderItems->save() == false){
							var_dump(CHtml::errorSummary($individualItemSupplierOrderItems));
							exit;
						}
					} else {
						var_dump(CHtml::errorSummary($individual_item));
						exit;
					}
					
					$item_count--;
				}
				
				/*$arrIndItemInSupplierOrderItems = $arrIndItemIDList[$item_id];
				
				foreach ($arrIndItemInSupplierOrderItems as $ind_item){
					$newIndividualItemSupplierOrderItemsModel = new IndividualItemSupplierOrderItems();
					$newIndividualItemSupplierOrderItemsModel->attributes = array(
						'individual_item_id' => $ind_item,
						'supplier_order_items_id' => $supplierOrderItemsModel->id,
					);
					
					$newIndividualItemSupplierOrderItemsModel->save();
				}*/
			}
			if ($new_order){
				$supplierOrder->attachPendingLoadingItems();
			}
		}
		
		Activity::model()->addActivity_CreateSupplierOrderByAdmin($supplier_id);
	}
	
	public static function getContainerDetailLink($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$container_id = $supplierOrderModel->container_id;
		
		if ($container_id == 0){
			return "";
		}
		
		$containerDetailUrl = Yii::app()->getBaseUrl(true)."/index.php/supplierorder/container_detail/?container_id=".$container_id;
		
		$strContainerDetailLink = 'ID:'.$container_id
				.'<br><a href="'.$containerDetailUrl.'">Detail View</a>';
		
		return $strContainerDetailLink;
	}
	
	public static function getAdvancedDate($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$strDate = "";
				
		if (($supplierOrderModel->status == SupplierOrder::$STATUS_ADVANCED_PAID)
			|| ($supplierOrderModel->status == SupplierOrder::$STATUS_MAKING)
			|| ($supplierOrderModel->status == SupplierOrder::$STATUS_COMPLETED)
			|| ($supplierOrderModel->status == SupplierOrder::$STATUS_FULL_PAID)){
				$strDate = date("M. d, Y", strtotime($supplierOrderModel->advanced_paid_date));
		}
		
		return $strDate;
	}
	
	public static function getFullPaidDate($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$strDate = "";
				
		if ($supplierOrderModel->status == SupplierOrder::$STATUS_FULL_PAID){
			$strDate = date("M. d, Y", strtotime($supplierOrderModel->full_paid_date));
		}
		
		return $strDate;
	}
	
	public static function getTotalPaidForShipDate($order_id){
		$supplierOrderModel = SupplierOrder::model()->findByPk($order_id);
		$container_id = $supplierOrderModel->container_id;
		$strDate = "";
		
		if ($container_id != 0){
			$conatinerModel = SupplierOrderContainer::model()->findByPk($container_id);			
			if ($conatinerModel != NULL){
				if ($conatinerModel->status == SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER){
					$strDate = date("M. d, Y", strtotime($conatinerModel->chosen_date));
				}
			}
		}
		
		return $strDate;
	}
	
	public static function getPendingOrdersTable($supplier_id, $exclude = array()){
		$strResult = '<table class="items">'
			.'<thead>'
			.'<th class="items">Items</th>'
			.'<th class="total_exw_price">Total EXW Price</th>'
			.'<th class="total_fob_price">Total Fob Price</th>'
			.'<th class="total_cbm">Total CBM</th>'
			.'<th class="action">Action</th>'			
			.'</thead>';
			
		$criteria=new CDbCriteria;
		$criteria->compare('status', SupplierOrder::$STATUS_PENDING, true, "OR");
		$criteria->compare('status', SupplierOrder::$STATUS_ACCEPTED, true, "OR");
		$criteria->compare('status', SupplierOrder::$STATUS_ADVANCED_PAID, true, "OR");
		$criteria->compare('status', SupplierOrder::$STATUS_MAKING, true, "OR");
		$criteria->compare('supplier_id',$supplier_id);
		if (count($exclude)){
			$criteria->addNotInCondition('id', $exclude);
		}
		$orderList = SupplierOrder::model()->findAll($criteria);
		
		$i = 0;
		foreach ($orderList as $order){
			$order_id = $order->id;
			$strItems = SupplierOrder::model()->getSupplierOrderItems($order_id);
			$total_exw_price = SupplierOrder::model()->getSupplierOrderTotalExwPrice($order_id);
			$total_fob_price = SupplierOrder::model()->getSupplierOrderTotalFobPrice($order_id);
			$total_cbm= SupplierOrder::model()->getSupplierOrderTotalCBM($order_id);
			$strAction = '<button class="choose_order" rel="'.$order_id.'">Choose</a>';
			
			$strResult .= '<tr>'
				.'<td class="items">'.$strItems.'</td>'
				.'<td class="total_exw_price">'.$total_exw_price.'</td>'
				.'<td class="total_fob_price">'.$total_fob_price.'</td>'
				.'<td class="total_cbm">'.$total_cbm.'</td>'
				.'<td class="action">'.$strAction.'</td>'
				.'</tr>';
				;
			$i++;
		}
		
		if ($i == 0){
			$strResult .= '<tr>'
				.'<td colspan="5" style="font-style:italic; text-align:center">No Result</td>'
				.'</tr>';
		}
	
		return $strResult;
	}
	
	public static function getWaitingOrderCount(){
		$criteria=new CDbCriteria;
		$criteria->compare('status',SupplierOrder::$STATUS_WAITING_APPROVAL);
		$count = SupplierOrder::model()->count($criteria);
		return $count;
	}
	
	public function getLoadingWaitingItems(){
		$criteriaInidividualItems = new CDbCriteria;
		$criteriaInidividualItems->compare('supplier_order_id', $this->id);
		$criteriaInidividualItems->addCondition('status !="'.Individualitem::$STATUS_NOT_ORDERED.'"');
		$criteriaInidividualItems->group = "item_id";
		
		$order_item_list = Individualitem::model()->findAll($criteriaInidividualItems);
		$operator = YumUser::isOperator(true);
		
		$strResult = '<table class="items"><thead>'
			.'<th>ITEM CODE</th>'
			.'<th>COLOR</th>'
			.'<th>QTY TO BE LOADED</th>'
			.($operator ? '<th>LOADED BY SUPPLIER</th>' : '')
			.'<th>ACTUALLY LOADED</th>'
			.'</thead>';
		
		$strItems = "";
		foreach ($order_item_list as $order_item){
			$item_id = $order_item->item_id;
			$criteriaInidividualItems = new CDbCriteria;
			$criteriaInidividualItems->compare('supplier_order_id', $this->id);
			$criteriaInidividualItems->compare('item_id', $item_id);
			$criteriaInidividualItems->addCondition('status !="'.Individualitem::$STATUS_NOT_ORDERED.'"');
			
			$count = Individualitem::model()->count($criteriaInidividualItems);
			$criteriaInidividualItems->compare('loaded', Individualitem::$LS_LOADED);
			$count_loaded = Individualitem::model()->count($criteriaInidividualItems);
			$itemModel = Item::model()->findByPk($item_id);
			$count_waiting = $operator ? $count_loaded : $count;

			$strItem = '<tr>'
				.'<td>'.$itemModel->item_code.'</td>'
				.'<td>'.$itemModel->color.'</td>'
				.'<td>'.$count.'</td>'
				.($operator ? '<td>'.$count_loaded.'</td>' : '' )
				.'<td><input type="text" id="loading_quantity_'.$item_id.'" class="loading_quantity" value="'.$count_waiting.'" rel="'.$item_id.'">'
				.'<input type="hidden" id="waiting_quantity_'.$item_id.'" class="waiting_quantity" value="'.$count_waiting.'" rel="'.$item_id.'"></td>'
				.'</tr>';
			$strItems .= $strItem;
		}
		
		if ($strItems == ""){
			$strItems = '<tbody><tr><td colspan="3" style="text-align:center; font-style:italic">No Result</td></tr></tbody>';
		}
		
		$strResult .= $strItems."</table>";
		
		return $strResult;
	}
	
	public static function loadOrderItems($supplier_order_id, $item_id_list, $quantity_list){
		$arrItemIdList = explode("_", $item_id_list);
		$arrQuantityList = explode("_", $quantity_list);
		
		foreach ($arrQuantityList as $index=>$quantity){
			$item_id = $arrItemIdList[$index];
			
			$criteria = new CDbCriteria;
			
			$criteriaInidividualItems = new CDbCriteria;
			$criteriaInidividualItems->compare('supplier_order_id', $supplier_order_id);
			$criteriaInidividualItems->compare('item_id', $item_id);
			$individualItemList = Individualitem::model()->findAll($criteriaInidividualItems);
			
			foreach ($individualItemList as $individualItem){
				$individualItem->loaded = ($quantity > 0)?Individualitem::$LS_LOADED:Individualitem::$LS_PENDING;
				$individualItem->save();
// 				echo $individualItem->id.' '.$individualItem->loaded;
// 				echo ' '.(int)$individualItem->save().';';
				$quantity--;
			}
		}
		
		$supplierOrderModel = SupplierOrder::model()->findByPk($supplier_order_id);
		$supplierOrderModel->loaded = SupplierOrder::$LS_LOADED;
		$supplierOrderModel->save();
	}
	
	
	public static function getPendingLoadingItemsBySupplierIDStr($supplier_id){
		$criteriaOrder = new CDbCriteria;
		$criteriaOrder->compare('supplier_id', $supplier_id);
		$supplierOrderList = SupplierOrder::model()->findAll($criteriaOrder);
		
		$arrSupplierOrderId = array();
		foreach ($supplierOrderList as $supplierOrder){
			$arrSupplierOrderId[] = $supplierOrder->id;
		}
		
		$criteriaInidividualItems = new CDbCriteria;
		$criteriaInidividualItems->compare('loaded', Individualitem::$LS_PENDING);
		$criteriaInidividualItems->addInCondition('supplier_order_id', $arrSupplierOrderId);
		$criteriaInidividualItems->group = "item_id";
		
		$order_item_list = Individualitem::model()->findAll($criteriaInidividualItems);
		
		$strResult = '<table class="items"><thead>'
			.'<th>Item Code</th>'
			.'<th>Color</th>'
			.'<th>Pending Items Quantity</th>'
			//.'<th>Quantity to Attach</th>'
			.'</thead>';
		
		$strItems = "";
        
		foreach ($order_item_list as $order_item){
			$item_id = $order_item->item_id;
			$criteriaInidividualItems = new CDbCriteria;
			$criteriaInidividualItems->compare('loaded', Individualitem::$LS_PENDING);
			$criteriaInidividualItems->addInCondition('supplier_order_id', $arrSupplierOrderId);
			$criteriaInidividualItems->compare('item_id', $item_id);
			
			$count = Individualitem::model()->count($criteriaInidividualItems);
			$itemModel = Item::model()->findByPk($item_id);
			$strItem = '<tr>'
				.'<td>'.$itemModel->item_code.'</td>'
				.'<td>'.$itemModel->color.'</td>'
				.'<td>'.$count.'</td>'
				//.'<td><input type="text" id="new_quantity_'.$item_id.'" class="new_quantity" name="add_pending_loading_quantity['.$item_id.']" value="0" rel="'.$item_id.'">'
				//.'<input type="hidden" id="pending_quantity_'.$item_id.'" class="pending_quantity" value="'.$count.'" rel="'.$item_id.'"></td>'
				.'</tr>';
			$strItems .= $strItem;
		}
		
		if ($strItems == ""){
			$strItems = '<tbody><tr><td colspan="4" style="text-align:center; font-style:italic">No Result</td></tr></tbody>';
            return '';
		}
		
		$strResult .= $strItems."</table>";
		
		return $strResult;
	}
	
	public static function getPendingLoadingItemsBySupplierID($supplier_id){
		$criteria = new CDbCriteria();
		$criteria->compare('supplier_id', $supplier_id);
		$orders = SupplierOrder::model()->findAll($criteria);
		$order_ids = array();
		foreach ($orders as $order){
			$order_ids[] = $order->id;
		}
		
		$ind_item_criteria = new CDbCriteria;
		$ind_item_criteria->compare('loaded', Individualitem::$LS_PENDING);
		$ind_item_criteria->addInCondition('supplier_order_id', $order_ids);
		$ind_item_criteria->group = "item_id";
		
		$ind_item_list = Individualitem::model()->findAll($ind_item_criteria);

		$result = array();
		foreach ($ind_item_list as $ind_item){
			$item_id = $ind_item->item_id;
			$criteria = new CDbCriteria;
			$criteria->compare('loaded', Individualitem::$LS_PENDING);
			$criteria->addInCondition('supplier_order_id', $order_ids);
			$criteria->compare('item_id', $item_id);
			$count = Individualitem::model()->count($criteria);
			if ($count){
				$result[$item_id] = $count;
			}
		}
		
		return count($result) ? $result : NULL;
	}
	
	// returns array, $array['item_id'] = $count;
	public function getPendingLoadingItems(){
		$result = array();
		foreach ($this->order_items as $order_item){
			$item_id = $order_item->item_id;
			$criteria = new CDbCriteria;
			$criteria->compare('loaded', Individualitem::$LS_PENDING);
			// $criteria->addInCondition('supplier_order_id', $arrSupplierOrderId);
			$criteria->compare('supplier_order_id', $this->id);
			$criteria->compare('item_id', $item_id);
			$count = Individualitem::model()->count($criteria);
			if ($count){
				$result[$item_id] = $count;
			}
		}
		
		return count($result) ? $result : NULL;
	}
	
	public static function getPendingSupplierOrderItemsSupplierIdList(){
		$strQuery = 'SELECT '
			.'DISTINCT u.id '
			.'FROM '
			.'supplier_order_items soi '
			.'LEFT JOIN supplier_order so ON soi.supplier_order_id = so.id '
			.'LEFT JOIN user u ON so.supplier_id = u.id '
			.'WHERE '
			.'soi.status="Pending"';
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		$id_list = array();
		
		foreach ($result as $row){
			$id_list[] = $row["id"];
		}
		
		return $id_list;
	}
	
	public static function getSupplierOrderSetETLSupplierIdList(){
		$strQuery = 'SELECT '
			.'DISTINCT u.id '
			.'FROM '
			.'supplier_order so '
			.'LEFT JOIN user u ON so.supplier_id = u.id '
			.'WHERE '
			.'so.status="Advanced Paid"';
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		$id_list = array();
		
		foreach ($result as $row){
			$id_list[] = $row["id"];
		}
		
		return $id_list;
	}
	
	public static function getSupplierOrderNeedLoadingSupplierIdList(){
		$strQuery = 'SELECT '
			.'DISTINCT u.id '
			.'FROM '
			.'supplier_order so '
			.'LEFT JOIN user u ON so.supplier_id = u.id '
			.'WHERE '
			.'so.status="FULL Paid"'
			.'AND ISNULL(so.loaded)'
			;
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		$id_list = array();
		
		foreach ($result as $row){
			$id_list[] = $row["id"];
		}
		
		return $id_list;
	}
	
	public static function getSupplierOrderNeedUploadingOfficialDocumentsSupplierIdList(){
		$strQuery = 'SELECT '
			.'DISTINCT u.id '
			.'FROM '
			.'supplier_order so '
			.'LEFT JOIN user u ON so.supplier_id = u.id '
			.'WHERE '
			.'so.status="Full Paid"'
			.'AND so.loaded="Loaded"'
			;
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		$id_list = array();
		
		foreach ($result as $row){
			$id_list[] = $row["id"];
		}
		
		return $id_list;
	}
    
	public static function getSupplierOrderBeforeETLSupplierList(){
		$strQuery = 'SELECT '
			.'u.id, '
			.'p.email as email_address, '
			.'DATEDIFF(so.etl, now()) as diff_days '
			.'FROM '
			.'supplier_order so '
			.'LEFT JOIN user u ON so.supplier_id = u.id '
			.'LEFT JOIN profile p ON p.user_id = u.id '
			.'WHERE '
			.'(DATEDIFF(so.etl, now())=4 '
			.'OR DATEDIFF(so.etl, now())=3  '
			.'OR DATEDIFF(so.etl, now())=1 ) ';
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		return $result;
	}
	
	public static function getSupplierOrderContainerBiddingBeforeETLSupplierList(){
		$strQuery = 'SELECT '
			.'u.id, '
			.'p.email as email_address, '
			.'DATEDIFF(so.etl, now()) as diff_days '
			.'FROM '
			.'supplier_order so '
			.'LEFT JOIN user u ON so.supplier_id = u.id '
			.'LEFT JOIN profile p ON p.user_id = u.id '
			.'WHERE '
			.'(DATEDIFF(so.etl, now())=4 '
			.'OR DATEDIFF(so.etl, now())=3  '
			.'OR DATEDIFF(so.etl, now())=1 ) ';
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		return $result;
	}
	
	public static function getSupplierOrderBeforeETASupplierShipperList(){
		$strQuery = 'SELECT '
			.'u.id, '
			.'p.email as email_address, '
			.'DATEDIFF(socb.bid_eta, now()) as diff_days '
			.'FROM  '
			.'supplier_order_container soc '
			.'LEFT JOIN supplier_order_container_bidding socb  '
			.'on (soc.id = socb.supplier_order_container_id and soc.supplier_shipper_id = socb.supplier_shipper_id) '
			.'LEFT JOIN user u ON socb.supplier_shipper_id = u.id '
			.'LEFT JOIN profile p ON p.user_id = u.id '
			.'WHERE '
			.'soc.status="Chosen Shipper" '
			.'AND (DATEDIFF(socb.bid_eta, now())=5 '
			.'OR DATEDIFF(socb.bid_eta, now())=3  '
			.'OR DATEDIFF(socb.bid_eta, now())=2 '
			.'OR DATEDIFF(socb.bid_eta, now())=1 '
			.')';
		
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		return $result;
	}
    
    // setting status manually
    public function setStatus($new_status, $etl = null){
        if ($this->status == self::$STATUS_COMPLETED){
            if ($new_status == self::$STATUS_MAKING){
                if ($this->rollBackCompleted()){
                    AutoMail::sendMailAdminResetOrderMaking(Yii::app()->user->id, $this->supplier_id);
                    Activity::addActivity_ResetSupplierOrderMakingByAdmin($this->supplier_id);
                    return true;
                } else {
                    return false;
                }
            } elseif($new_status == self::$STATUS_ADVANCED_PAID){
                if ($this->setStatus(self::$STATUS_MAKING) 
                        && $this->setStatus(self::$STATUS_ADVANCED_PAID)){
                    return true;
                } else {
                    return false;
                }
            } elseif($new_status == self::$STATUS_COMPLETED){
                return true;
            } else {
                return false;
            }
        } elseif($this->status == self::$STATUS_MAKING){
            if ($new_status == self::$STATUS_ADVANCED_PAID){
                if($this->rollBackMaking()){
                    Activity::addActivity_ResetSupplierOrderAdvancedPaidByAdmin($this->supplier_id);
                    AutoMail::sendMailAdminResetOrderAdvancedPaid(Yii::app()->user->id, $this->supplier_id);
                    return true;
                } else {
                    return false;
                }
            } elseif($new_status == self::$STATUS_COMPLETED){
                if ($this->complete()){
                    Activity::addActivity_CompleteSupplierOrderByAdmin($this->supplier_id);
                    AutoMail::sendMailAdminFinishOrder(Yii::app()->user->id, $this->supplier_id);
                    return true;
                } else {
                    return false;
                }
            } elseif($new_status == self::$STATUS_MAKING){
                return true;
            } else {
                return false;
            }
        } elseif($this->status == self::$STATUS_ADVANCED_PAID){
            if($new_status == self::$STATUS_MAKING){
                if($this->setETL($etl)){
                    AutoMail::sendMailAdminSetOrderETL(Yii::app()->user->id, $this->supplier_id);
                    Activity::addActivity_SetETLSupplierOrderByAdmin($this->supplier_id);
                    return true;
                } else {
                    return false;
                }
            } elseif($new_status == self::$STATUS_COMPLETED){
                if ($this->setETL($etl) 
                        && $this->complete()){
                    Activity::addActivity_CompleteSupplierOrderByAdmin($this->supplier_id);
                    AutoMail::sendMailAdminFinishOrder(Yii::app()->user->id, $this->supplier_id);
                    return true;
                } else {
                    return false;
                }
            } elseif($new_status == self::$STATUS_ADVANCED_PAID){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public function complete(){
        $this->completed_date = date("Y-m-d H:i:s", time());
		$this->status = SupplierOrder::$STATUS_COMPLETED;
		return $this->save();
    }
    
    public function setETL($etl){
        $this->etl = date("Y-m-d H:i:s", strtotime($etl));
		$this->etl_set_date = date("Y-m-d H:i:s", time());
		$this->status = SupplierOrder::$STATUS_MAKING;
		if ($this->save()){
			$this->modifyChangeItmesStatusAsSetETA();
            return true;
		}
    }
    
    // turns completed order into making
    private function rollBackCompleted(){
        $this->status = self::$STATUS_MAKING;
        $this->completed_date = null;
        return $this->save();
    }
    
    // turns making order into advanced paid
    private function rollBackMaking(){
        $this->status = self::$STATUS_ADVANCED_PAID;
        $this->etl = null;
        $this->etl_set_date = null;
        if (!$this->save()){
            return false;
        }
        
        $criteriaItems = new CDbCriteria;
		$criteriaItems->compare('supplier_order_id',$this->id);
		$orderItems = SupplierOrderItems::model()->findAll($criteriaItems);
		
		foreach ($orderItems as $item){
			$criteriaOrderItems = new CDbCriteria;
			$criteriaOrderItems->compare('supplier_order_items_id',$item->id);
			$individual_item_supplier_order_items = IndividualItemSupplierOrderItems::model()->findAll($criteriaOrderItems);
			foreach ($individual_item_supplier_order_items as $ind_item){
				$ind_item_info = Individualitem::model()->findByPk($ind_item->individual_item_id);				
				$ind_item_info->status = Individualitem::$STATUS_ORDERED_WITHOUT_ETL;
				$ind_item_info->supplier_order_id = $this->id;
				$ind_item_info->save();
			}
		}
        
        return true;
    }
    
    public function getShippingTerms(){
        if ($this->supplier_order_container->status == SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER){
            // $criteria = new CDbCriteria();
            // $criteria->compare('supplier_shipper_id', $this->supplier_order_container->supplier_shipper_id);
            // $criteria->compare('supplier_order_container_id', $this->supplier_order_container->id);
            // $bid = SupplierOrderContainerBidding::model()->find($criteria);
            return $this->supplier_order_container->terms;
        } else {
            return false;
        }
    }
	
	public function getETA(){
		if ($this->supplier_order_container){
			return $this->supplier_order_container->getETA();
		} else {
			return false;
		}
	}
	
	public function getShipperID(){
		return $this->supplier_order_container ? $this->supplier_order_container->supplier_shipper_id : NULL;
	}
	
	public function getETAString(){
		if ($eta = $this->getETA()){
			return date("M, d. Y", strtotime($eta));
		} else {
			return '';
		}
	}
	
	// looks for pending loading items for current supplier and attaches them all to the order
	public function attachPendingLoadingItems(){
        $itemIdsSupplierOrderItems = array();
        foreach (Individualitem::getPendingLoadingItems($this->supplier_id) as $individualItem) {
            if (array_key_exists($individualItem->item_id, $itemIdsSupplierOrderItems)) {
                $supplierOrderItemsId = $itemIdsSupplierOrderItems[$individualItem->item_id];
            } else {
                //create new supplier_order_items
                $supplierOrderItems = new SupplierOrderItems();
                $supplierOrderItems->setAttributes(array(
                    'supplier_order_id'	=>	$this->id,
                    'item_id'			=>	$individualItem->item_id,
                    'quantity'			=>	Individualitem::model()->countByAttributes(array(
                            'item_id' => $individualItem->item_id,
                            'loaded' => Individualitem::$LS_PENDING,
                        )),
                    'exw_cost_price'	=>	0,
                    'fob_cost_price'	=>	0,
                    'sale_price'		=>	0,
                    'added_date'		=>	date('Y-m-d H:i:s'),
                    'not_loaded_reordered' =>	1,
                    'status'			=>	SupplierOrder::$STATUS_PENDING,
                    'initial_order_id'	=>	$individualItem->supplier_order_id,
                    'operator_id' => Yii::app()->user->id,
                ));
                if ($supplierOrderItems->save()) {
                    $itemIdsSupplierOrderItems[$individualItem->item_id] = $supplierOrderItems->id;
                    $supplierOrderItemsId = $supplierOrderItems->id;
                } else {
                    Yii::log('Error saving SupplierOrderItems: '
                        . CVarDumper::dumpAsString($supplierOrderItems->errors),
                        'error');
                    throw new CDbException('Error saving SupplierOrderItems');
                }
            }
            //update individual items
            $individualItem->saveAttributes(array(
                'status' => Individualitem::$STATUS_ORDERED_BUT_WAITING,
                'loaded' => null,
                'supplier_order_id' => $this->id,
            ));
            //add pivot records
            $individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
            $individualItemSupplierOrderItems->setAttributes(array(
                'individual_item_id' => $individualItem->id,
                'supplier_order_items_id' => $supplierOrderItemsId,
            ));
            $individualItemSupplierOrderItems->save();
        }
        /*
		if ($pending_loading_items = self::getPendingLoadingItemsBySupplierID($this->supplier_id)){
			foreach ($pending_loading_items as $item => $qty){
				$attributes = array(
					"supplier_order_id"	=>	$this->id,
					"item_id"			=>	$item,
					"quantity"			=>	$qty,
					"exw_cost_price"	=>	0,
					"fob_cost_price"	=>	0,
					"sale_price"		=>	0,
					"added_date"		=>	date("Y-m-d H:i:s"),
					"not_loaded_reordered" =>	1,
					"status"			=>	SupplierOrder::$STATUS_PENDING,
				);
				
				$new_items = new SupplierOrderItems;
				$new_items->attributes = $attributes;
				$new_items->operator_id = Yii::app()->user->id;
				$new_items->save();

                //get current supplier orders
				$criteriaOrder = new CDbCriteria;
				$criteriaOrder->compare('supplier_id', $this->supplier_id);
				$supplierOrderList = SupplierOrder::model()->findAll($criteriaOrder);
				
				$arrSupplierOrderId = array();
				foreach ($supplierOrderList as $supplierOrder){
					$arrSupplierOrderId[] = $supplierOrder->id;
				}
				
				$criteriaInidividualItems = new CDbCriteria;
				$criteriaInidividualItems->compare('loaded', Individualitem::$LS_PENDING);
				$criteriaInidividualItems->compare('item_id', $item);
				$criteriaInidividualItems->addInCondition('supplier_order_id', $arrSupplierOrderId);
				
				$individualItemList = Individualitem::model()->findAll($criteriaInidividualItems);
				
				foreach ($individualItemList as $individualItemModel){
					
					$individualItemModel->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;
					$individualItemModel->loaded = NULL;
					$individualItemModel->supplier_order_id = $this->id;
					
					if ($individualItemModel->save()){
						$individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
						$individualItemSupplierOrderItems->individual_item_id = $individualItemModel->id;
						$individualItemSupplierOrderItems->supplier_order_items_id = $new_items->id;
						
						if ($individualItemSupplierOrderItems->save() == false){
							var_dump(CHtml::errorSummary($individualItemSupplierOrderItems));
							exit;
						}
					} else {
						var_dump(CHtml::errorSummary($individualItemModel));
						exit;
					}
				}
			}
		}
        */
	}
	
	/**
	 * Returns suplier order that still can be updated (i.e. item added)
	 * @param int $supplier_id
	 * @return SupplierOrder model or false if nothing found
	 */
	public static function getPendingOrder($supplier_id){
		$criteria = new CDbCriteria();
		$criteria->compare('status', SupplierOrder::$STATUS_PENDING);
		$criteria->compare('status', SupplierOrder::$STATUS_ACCEPTED, false, 'or');
		$criteria->compare('status', SupplierOrder::$STATUS_ADVANCED_PAID, false, 'or');
		$criteria->compare('status', SupplierOrder::$STATUS_MAKING, false, 'or');
		$criteria->compare('deleted', 0);
		$criteria->compare('supplier_id', $supplier_id);
		$order = SupplierOrder::model()->find($criteria);
		return $order;
	}
	
	/**
	 * Creates new Pending order
	 * @param int $supplier_id
	 * @return SupplierOrder or false if not saved
	 */
	public static function createEmptyOrder($supplier_id){
		$order = new SupplierOrder();
    	$order->supplier_id = $supplier_id;
    	$order->created_date = date('Y-m-d H:i:s');
    	$order->status = SupplierOrder::$STATUS_PENDING;
    	$order->operator_id = Yii::app()->user->id;
    	if ($order->save()){
    		return $order;
    	} else {
    		return false;
    	}
	}
	
	public function getSupplierInfoHtml(){
		$criteria = new CDbCriteria();
		$criteria->compare('supplier_id', $this->supplier_id);
		$criteria->compare('accepted', 1);
		$bank_info = SupplierBankInfo::model()->find($criteria);
		$str  = '<strong>Name: </strong>'.$this->supplier->profile->firstname.' '.$this->supplier->profile->lastname.'<br />';
		$str .= '<strong>Email: </strong>'.$this->supplier->profile->email.'<br />';
		$str .= '<br /><h4>Bank Info:</h4>';
		$str .= '<strong>Account Name: </strong>'.$bank_info->account_name.'<br />';
		$str .= '<strong>Account Address: </strong>'.$bank_info->account_address.'<br />';
		$str .= '<strong>Account Number: </strong>'.$bank_info->account_number.'<br />';
		$str .= '<strong>Routing Number: </strong>'.$bank_info->routing_number.'<br />';
		$str .= '<strong>Bank Name: </strong>'.$bank_info->bank_name.'<br />';
		$str .= '<strong>Bank Address: </strong>'.$bank_info->bank_address.'<br />';
		
		return $str;
	}

    public function updateETLByAdmin($date){
        $old_etl = $this->etl;
        $this->etl = date("Y-m-d H:i:s", strtotime($date));
        $this->etl_set_date = date("Y-m-d H:i:s", time());
        if ($this->save()){

            Yii::import('application.extensions.phpmailer.JPhpMailer');

            $name = $this->supplier->profile->firstname.' '.$this->supplier->profile->lastname;

            $mail = new JPhpMailer;
            $mail->IsSendmail();
            $mail->AddAddress($this->supplier->profile->email);
            $mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
            $mail->Subject = AutoMailMessage::MS_SUPPLIER_ETL_CHANGED_BY_ADMIN;
            $body = AutoMailMessage::MC_SUPPLIER_ETL_CHANGED_BY_ADMIN;

            $find = array('[name]', '[id]', '[old_etl]', '[new_etl]');
            $replace = array($name, $this->id, date('M d', strtotime($old_etl)), date('M d', strtotime($date)));
            $mail->Body = str_replace($find, $replace, $body);
            $mail->Send();
            return true;
        } else {
            return false;
        }
    }
}