<?php
/*
 * static class for 3rd party shipping companies operations (fedex/ups/usps)
 */
class ShippingCompany{
	/*
	 * @param CustomerOrderShipment $shipment - AR object
	 */
	
	public static $companies = array('Fedex Home', 'Fedex Ground', 'USPS');
	public static $sender_address = array(
		'street' 	=> '1625 W 144St #2',
		'city' 		=> 'Gardena', 
		'state' 	=> 'CA',
		'zip' 		=> '90247',
		'phone' 	=> '323 592 9090',
		'name' 		=> 'Designs Unlimited',
		'company' 	=> 'Designs Unlimited Inc'
	);
	
	public static $state_list = array('ALABAMA'=>"AL", 'ALASKA'=>"AK", 'AMERICAN SAMOA'=>"AS", 'ARIZONA'=>"AZ", 'ARKANSAS'=>"AR", 'CALIFORNIA'=>"CA", 'COLORADO'=>"CO", 'CONNECTICUT'=>"CT", 'DELAWARE'=>"DE", 'DISTRICT OF COLUMBIA'=>"DC", "FEDERATED STATES OF MICRONESIA"=>"FM", 'FLORIDA'=>"FL", 'GEORGIA'=>"GA", 'GUAM' => "GU", 'HAWAII'=>"HI", 'IDAHO'=>"ID", 'ILLINOIS'=>"IL", 'INDIANA'=>"IN", 'IOWA'=>"IA", 'KANSAS'=>"KS", 'KENTUCKY'=>"KY", 'LOUISIANA'=>"LA", 'MAINE'=>"ME", 'MARSHALL ISLANDS'=>"MH", 'MARYLAND'=>"MD", 'MASSACHUSETTS'=>"MA", 'MICHIGAN'=>"MI", 'MINNESOTA'=>"MN", 'MISSISSIPPI'=>"MS", 'MISSOURI'=>"MO", 'MONTANA'=>"MT", 'NEBRASKA'=>"NE", 'NEVADA'=>"NV", 'NEW HAMPSHIRE'=>"NH", 'NEW JERSEY'=>"NJ", 'NEW MEXICO'=>"NM", 'NEW YORK'=>"NY", 'NORTH CAROLINA'=>"NC", 'NORTH DAKOTA'=>"ND", "NORTHERN MARIANA ISLANDS"=>"MP", 'OHIO'=>"OH", 'OKLAHOMA'=>"OK", 'OREGON'=>"OR", "PALAU"=>"PW", 'PENNSYLVANIA'=>"PA", 'RHODE ISLAND'=>"RI", 'SOUTH CAROLINA'=>"SC", 'SOUTH DAKOTA'=>"SD", 'TENNESSEE'=>"TN", 'TEXAS'=>"TX", 'UTAH'=>"UT", 'VERMONT'=>"VT", 'VIRGIN ISLANDS' => "VI", 'VIRGINIA'=>"VA", 'WASHINGTON'=>"WA", 'WEST VIRGINIA'=>"WV", 'WISCONSIN'=>"WI", 'WYOMING'=>"WY");
	
	public static function generateQuotes($shipment){
		$messages = array();
		foreach (self::$companies as $company){
			$bid = new CustomerOrderShipmentBid();
			$bid->auto_generated = 1;
			$bid->delivery_service = $company;
			$rate = self::getShippingRateQuote($company, $shipment);
			$bid->rate_quote = $rate['rate'];
			$messages[] = $shipment->id.'/'.$company.': '.$rate['message']; 
			$bid->status = CustomerOrderShipmentBid::$STATUS_PENDING;
			$bid->date = date('Y-m-d H:i:s');
			$bid->shipment_id = $shipment->id;
			
			if (!$bid->rate_quote){
				//echo 'fail '.$company;
			} 
			
			$bid->save();
		}
		
		//$shipment->shipping_services_bids_generated = 1;
		//$shipment->save();
		
		return $messages;
	}
	
	public static function getShippingRateQuote($company, $shipment){
		if ($company == 'Fedex Home'){
			return self::getShippingRateQuoteFedex($shipment, 'GROUND_HOME_DELIVERY');
		}
		
		if ($company == 'Fedex Ground'){
			return self::getShippingRateQuoteFedex($shipment, 'FEDEX_GROUND');
		}
		
		if ($company == 'USPS'){
			return self::getShippingRateQuoteUsps($shipment);
		}
	}
	
	public static function getShippingRateQuoteFedex($shipment, $service){
		Yii::import('application.extensions.shipping_services.fedex.*');
		require_once('Fedex_Rate.php');
		$info = $shipment->customer_order->billing_info;
		
		// TODO: try not to duplicate code for different services
		// TODO: create log file for debug
		// TODO: different services
		
		$address = array(
			'street' 	=> $info->shipping_street_address,
			'city' 		=> $info->shipping_city,
			'state' 	=> $info->shipping_state,
			'zip' 		=> $info->shipping_zip_code,
			'phone' 	=> $info->shipping_phone_number,
			'name' 		=> $info->shipping_name.' '.$info->shipping_last_name,
			'residental'=> strtolower($info->busrestype) == 'residental',
		);
		
		if (strlen($address['state']) > 2){
			// need short state
			$address['state'] = self::$state_list[strtoupper($address['state'])];
		}
		
		$boxes_weight = array();
		$boxes_dims = array();
		$boxes = $shipment->getBoxes();
		foreach ($boxes as $box){
			$boxes_weight[] = $box['weight'];
			$boxes_dims[] = $box['length'].'x'.$box['width'].'x'.$box['height'];
		}
		
		//print_r($boxes);
		
		return getFedexShippingRate($address, count($boxes), $boxes_weight, $boxes_dims, $service);
	}
	
	public static function getShippingRateQuoteUsps($shipment){
		Yii::import('application.extensions.shipping_services.usps.*');
		require_once('USPS_Rate.php');
		
		$info = $shipment->customer_order->billing_info;
		
		// TODO: try not to duplicate code for different services
		$address = array(
				'street' 	=> $info->shipping_street_address,
				'city' 		=> $info->shipping_city,
				'state' 	=> $info->shipping_state,
				'zip' 		=> $info->shipping_zip_code,
				'phone' 	=> $info->shipping_phone_number,
				'name' 		=> $info->shipping_name.' '.$info->shipping_last_name,
				'residental'=> strtolower($info->busrestype) == 'residental',
		);
		
		if (strlen($address['state']) > 2){
			// need short state
			$address['state'] = self::$state_list[strtoupper($address['state'])];
		}
		
		$boxes_weight = array();
		$boxes_dims = array();
		$boxes = $shipment->getBoxes();
		foreach ($boxes as $box){
			$boxes_weight[] = $box['weight'];
			$boxes_dims[] = $box['length'].'x'.$box['width'].'x'.$box['height'];
		}
		
		return USPSParcelRate($address, count($boxes), $boxes_weight, $boxes_dims);
	}
}
?>