<?php

/**
 * This is the model class for table "customer_order_shipment_claim".
 *
 * The followings are the available columns in table 'customer_order_shipment_claim':
 * @property integer $id
 * @property integer $shipment_id
 * @property double $amount
 * @property string $claim_date
 * @property integer $claim_resolved
 * @property string $claim_resolved_date
 */
Yii::import('application.modules.profile.models.YumProfile');
class CustomerOrderShipmentClaim extends CActiveRecord
{
	public $_shipper_id;
	public $_total_claims = 0;
    public $shipperId;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerOrderShipmentClaim the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_shipment_claim';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shipment_id, amount, claim_date', 'required'),
			array('shipment_id, claim_resolved', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, shipment_id, amount, claim_date, claim_resolved, claim_resolved_date, shipperId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'shipment'	=>array(
					self::HAS_ONE,
					'CustomerOrderShipment', 
					array('id' => 'shipment_id')
			),
//			'shipment'	=>array(
//					self::BELONGS_TO,
//					'CustomerOrderShipment',
//					'shipment_id'
//			),
            'shipper' => array(
                self::HAS_ONE, 'YumUser', array('shipper_id' => 'id'), 'through' => 'shipment',
            ),
            'shipperProfile' => array(
                self::HAS_ONE, 'YumProfile', array('id' => 'user_id'),  'through' => 'shipper'
            ),
			'ind_items'	=>array(
					self::HAS_MANY, 
					'Individualitem', 
					'claim_id', 
					'order'=>'ind_items.item_id', 
					'with'=>'customer_order_items', 
					'together'=>true
			),
			'claim_docs'=>array(
					self::HAS_MANY,
					'OfficialDoc',
					'item_id',
					'condition'=>'type = "'.OfficialDoc::$TYPE_CLAIM.'"'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shipment_id' => 'Shipment',
			'amount' => 'Amount',
			'claim_date' => 'Claim Date',
			'claim_resolved' => 'Claim Resolved',
			'claim_resolved_date' => 'Claim Resolved Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($resolved = false)
	{
		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.shipment_id',$this->shipment_id);
		$criteria->compare('t.amount',$this->amount);
		$criteria->compare('t.claim_date',$this->claim_date);
		$criteria->compare('t.claim_resolved', intval($resolved));
		$criteria->compare('t.claim_resolved_date',$this->claim_resolved_date);
		$criteria->order = 't.claim_date DESC';
		$criteria->with = array('shipment');

        if ($this->shipperId) {
            $criteria->with['shipment'] = array(
                    'select' => false,
                    'joinType' => 'INNER JOIN',
                    'condition' => 'shipment.shipper_id = :shipperId',
                    'params' => array(':shipperId' => $this->shipperId)
            );
            $criteria->together = true;
        } else $criteria->with = array('shipment');
		if ($this->_shipper_id){
			$criteria->compare('shipment.shipper_id', $this->_shipper_id);
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Customer Shipper view
	 */
	public function getDamagedBoxesListCS(){
		$controller = new CustomershipperController('');
	
		return $controller->renderPartial('customershipper/_damaged_items_list_cs', array(
				'claim'=>$this
		), true);
	}
	
	public function getDamagedBoxesList(){
		$controller = new CustomershipperController('');
		switch ($this->shipment->customer_order->shipping_method){
			case 'Shipping':
				$shipping_field = 'shipping_price';
				break;
			case 'Local Pickup':
				$shipping_field = 'local_pickup';
				break;
			case 'Shipped from Los Angeles':
				$shipping_field = 'la_oc_shipping';
				break;
			case 'Canada Shipping':
				$shipping_field = 'canada_shipping';
				break;
		}
		return $controller->renderPartial('customershipper/_damaged_items_list', array(
				'claim'=>$this,
				'shipment'=>$this->shipment,
				'shipping_field'=>$shipping_field,
		), true);
	}
	
	/**
	 * @return html list of download links
	 */
	public function getClaimDocsListStr(){
		$links = array();
		foreach ($this->claim_docs as $doc){
			$links[] = '<a target="_blank" href="'.Yii::app()->getBaseUrl(true).'/index.php/customershipper/download_claim_doc/?id='.$doc->id.'">'.$doc->getFilename().'</a>';
		}
	
		return implode('<br />', $links);
	}
	
	public function notReplacedItemsCount(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if (!$item->replaced){
				$count++;
			}
		}
	
		return $count;
	}
	
	public function notReturnedItemsCount(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if (!$item->customer_return_id){
				$count++;
			}
		}
	
		return $count;
	}
	
	
	public function notReorderedItemsCount(){
		$count = 0;
		foreach ($this->ind_items as $item){
			if (!$item->reordered_from_supplier){
				$count++;
			}
		}
	
		return $count;
	}
	
	public function getActionsStr(){
		$str = '';
		if (Yii::app()->user->isAdmin()){
			if ($this->notReplacedItemsCount()){
				$str .= '<input type="button" id="replace_'.$this->id.'" value="Replace items" class="replace" rel="'.$this->id.'"/><br />
				<input type="button" value="Save" id="replace_save_'.$this->id.'" class="replace_save" rel="'.$this->id.'"/>
				<input type="button" value="Cancel" id="replace_cancel_'.$this->id.'" class="replace_cancel" rel="'.$this->id.'"/>';
			}
				
			if ($this->notReorderedItemsCount()){
				$str .= '<input type="button" id="supplier_order_'.$this->id.'" value="Order from Supplier" class="supplier_order" rel="'.$this->id.'"/><br />';
			}
			$str .= '<input type="button" id="refund_'.$this->id.'" value="Refund" class="refund" rel="'.$this->id.'"/><br />
			<input type="button" value="Save" id="refund_save_'.$this->id.'" class="refund_save" rel="'.$this->id.'"/>
			<input type="button" value="Cancel" id="refund_cancel_'.$this->id.'" class="refund_cancel" rel="'.$this->id.'"/>';
				
			if ($this->notReturnedItemsCount()){
				$str .= '<input type="button" id="authorize_return_'.$this->id.'" value="Authorize return" class="authorize_return" rel="'.$this->shipment_id.'"/><br />';
			}
				
			if (!$this->claim_resolved){
				$str .= '<input type="button" class="resolve" value="Resolve" rel="'.$this->id.'" /><br />';
			}
		}
	
		if (!$this->claim_resolved){
			$str .= '<input type="button" class="upload_doc" value="Upload Doc" rel="'.$this->id.'" /><br />';
		}
	
		$str .= '<input type="button" class="claim_notes" value="Notes" rel="'.$this->shipment_id.'" />';
	
		return $str;
	}
	
	public function getOpenClaimsTotal($shipper_id){
		$criteria = new CDbCriteria();
		$criteria->select = 'SUM(amount) as _total_claims';
		$criteria->compare('t.claim_resolved', 0);
		$criteria->compare('shipment.shipper_id', $shipper_id);
		$criteria->with = 'shipment';
		$row = self::model()->find($criteria);
		return $row->_total_claims;
	}
}