<?php

/**
 * This is the model class for table "customer_order_items".
 *
 * The followings are the available columns in table 'customer_order_items':
 * @property string $id
 * @property string $customer_order_id
 * @property string $item_id
 * @property string $quantity
 * @property double $exw_cost_price
 * @property double $fob_cost_price
 * @property double $sale_price
 * @property double $special_shipping_price
 */
class CustomerOrderItems extends CActiveRecord
{

	public $item_code;
	public $color;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerOrderItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_order_id, item_id, quantity, exw_cost_price, fob_cost_price, sale_price, special_shipping_price', 'required'),
			array('exw_cost_price, fob_cost_price, sale_price, special_shipping_price', 'numerical'),
			array('customer_order_id, item_id, quantity', 'length', 'max'=>10),
			array('custom_color', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_order_id, item_id, quantity, exw_cost_price, fob_cost_price, sale_price, special_shipping_price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items'=>array(self::BELONGS_TO, 'Item', 'item_id'),
			'order'=>array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
			'ind_items'=>array(self::HAS_MANY, 'Individualitem', 'customer_order_items_id'),
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_order_id' => 'Customer Order',
			'item_id' => 'Item',
			'quantity' => 'Quantity',
			'exw_cost_price' => 'EXW Cost Price',
			'fob_cost_price' => 'FOB Cost Price',
			'sale_price' => 'Sale Price',
			'special_shipping_price' => 'Special Shipping Price',
			'custom_color' => 'Custom Color',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($customer_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array(
			"items",			
		);
		
		$criteria->compare('customer_order_id',$customer_id,false);
		if ($this->item_code != NULL){
			$criteria->compare('items.item_code', $this->item_code, false);
		}
		
		if ($this->color != NULL){
			$criteria->compare('items.color', $this->color, false);
		}
		
		$criteria->compare('id',$this->id,false);
		$criteria->compare('item_id',$this->item_id,false);
		$criteria->compare('quantity',$this->quantity,false);
		$criteria->compare('exw_cost_price',$this->exw_cost_price);
		$criteria->compare('fob_cost_price',$this->fob_cost_price);
		$criteria->compare('sale_price',$this->sale_price);
		$criteria->compare('special_shipping_price',$this->special_shipping_price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
    public function searchLostSale(){
        $criteria = new CDbCriteria();
        $criteria->compare('lost_sale', 1);
        $criteria->join = 'LEFT JOIN `customer_order` ON `customer_order`.`id`=`t`.`customer_order_id`';
        $criteria->select = '`t`.*, `customer_order`.`lost_sale_date`';
        $criteria->order = '`customer_order`.`lost_sale_date`';
        
        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }
    
	public static function getItemShippingPrice($customer_order_item_id){
		$customerOrderItem = CustomerOrderItems::model()->findByPk($customer_order_item_id);
		$customerOrder = CustomerOrder::model()->findByPk($customerOrderItem->customer_order_id);
		
		$item_id = $customerOrderItem->item_id;
		$item_info = Item::model()->findByPk($item_id);
		
		if ($customerOrder->shipping_method == "Shipping") {
			$shipping_price = $item_info->shipping_price;
		} else if ($customerOrder->shipping_method == "Local Pickup") {
			$shipping_price = $item_info->local_pickup;
		} else if ($customerOrder->shipping_method == "Shipped from Los Angeles") {
			$shipping_price = $item_info->la_oc_shipping;
		} else if ($customerOrder->shipping_method == "Canada Shipping") {
			$shipping_price = $item_info->canada_shipping;
		}
		
		return $shipping_price;
	}
	
	public static function getItemTotalSalePrice($customer_order_item_id){
		$customerOrderItem = CustomerOrderItems::model()->findByPk($customer_order_item_id);
		$shipping_price = CustomerOrderItems::model()->getItemShippingPrice($customer_order_item_id);
		$total = ($customerOrderItem->sale_price + $shipping_price) * $customerOrderItem->quantity;
		return $total;
	}

	/**
	 * Returns items list with statuses they have according to adjusted item status (_fake_status)
	 */
	public function getItemStatusList(){
		$items = $this->ind_items;
		Individualitem::setItemsFakeStatus($items);
		
		return self::getStatusStringForItems($items);
	}
	
	/**
	 * 
	 * @param array $items array of Individualitem
	 */
	public static function getStatusStringForItems($items) {
		$count_in_stock 	= 0;
		$count_pending_scan = 0;
		$count_with_eta 	= array();
		$count_pending_eta 	= 0;
		$count_not_ordered 	= 0;
		$count_waiting 		= 0;
		$string_with_eta 	= "";
		$count_cancelled 	= 0;
		$count_sold			= 0;
		
		// TODO: don't foget to rewrite it if it works fine
		
		foreach ($items as $item){
			$status = $item->getStatus();
			if ($status == Individualitem::$STATUS_SOLD){
				$count_sold++;
			} else if ($status == Individualitem::$STATUS_IN_STOCK){
				$count_in_stock++;
			} else if($status == Individualitem::$STATUS_PENDING_SCAN){
				$count_pending_scan++;
			} else if ($status == Individualitem::$STATUS_ORDERED_WITH_ETA){
				$eta = $item->getEta();
				$count_with_eta[$eta]++;
			} else if ($status == Individualitem::$STATUS_ORDERED_WITHOUT_ETL
					|| $status == Individualitem::$STATUS_ORDERED_WITH_ETL){
				$count_pending_eta++;
			} else if ($status == Individualitem::$STATUS_CANCELLED_SHIPPING){
				$count_cancelled++;
			} else if ($status == Individualitem::$STATUS_ORDERED_BUT_WAITING){
				$count_waiting++;
			}else if ($status == Individualitem::$STATUS_NOT_ORDERED){
				// TODO: add a condition instead of "default"
				$count_not_ordered++;
			}
		}
		
		
		$strResult = "";
		
		if ($count_sold > 0){
			$strResult .= '<span style="color:#000; text-decoration: underline;">'.$count_sold.' - Scanned Out</span>';
		}
		
		if ($count_in_stock > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#000000">'.$count_in_stock.' - In Stock</span>';
		}
		
		if ($count_pending_scan > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#666">'.$count_pending_scan.' - Pending Scan</span>';
		}
		
		if (count($count_with_eta) > 0){
			foreach ($count_with_eta as $eta => $count) {
				$strResult = ($strResult == "")?'':$strResult.'<br/>';
				$strResult .= '<span style="color:#00A050">'.$count.' Arriving on '.date("M. d, Y", strtotime($eta)).'</span>';
			}
		}
		
		if ($count_pending_eta > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#548AC4">'.$count_pending_eta.' - Pending E.T.A</span>';
		}
		
		if ($count_waiting){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#ba9f07">'.$count_waiting.' - Waiting</span>';
		}
		
		if ($count_not_ordered > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#C00000">'.$count_not_ordered.' - Not Ordered</span>';
		}
		
		if ($count_cancelled){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#aaa">'.$count_cancelled.' - Cancelled Shipping</span>';
		}
		
		return $strResult;
	}
	/*
	public static function getEtaStringById($customer_order_items_id){
		$criteriaItemStatus = new CDbCriteria();
		
		$criteriaItemStatus->select = array('status', 'eta', 'count(*) as count_eta_grou_by_container_id');
		$criteriaItemStatus->compare('customer_order_items_id', $customer_order_items_id);
		$criteriaItemStatus->group = 'status, supplier_order_container_id';
		
		$containerList = Individualitem::model()->findAll($criteriaItemStatus);
		
		$count_in_stock 	= 0;
		$count_pending_scan = 0;
		$count_with_eta 	= 0;
		$count_pending_eta 	= 0;
		$count_not_ordered 	= 0;
		$count_waiting 		= 0;
		$string_with_eta 	= "";
		$count_cancelled 	= "";
		
		
		foreach ($containerList as $container){
			$eta = $container->eta;
			$count_item = $container->count_eta_grou_by_container_id;
			$status = $container->status;
			
			if ($status == Individualitem::$STATUS_SOLD){
				$count_sold += $count_item;
			} else if ($status == Individualitem::$STATUS_IN_STOCK){
				$count_in_stock += $count_item;
			} else if($status == Individualitem::$STATUS_PENDING_SCAN){
                $count_pending_scan += $count_item;
            } else if ($status == Individualitem::$STATUS_ORDERED_WITH_ETA){
				$count_with_eta += $count_item;
				$string_with_eta = ($string_with_eta == "")?'':$string_with_eta.'<br/>';
				$string_with_eta .= '<span style="color:#00A050">'.$count_item.' Arriving on '.date("M. d, Y", strtotime($eta)).'</span>';
			} else if ($status == Individualitem::$STATUS_ORDERED_WITHOUT_ETL 
				|| $status == Individualitem::$STATUS_ORDERED_WITH_ETL){
				$count_pending_eta += $count_item;
			} else if ($status == Individualitem::$STATUS_CANCELLED_SHIPPING){
				$count_cancelled += $count_item;
			} else if ($status == Individualitem::$STATUS_ORDERED_BUT_WAITING){
				$count_waiting += $count_item;
			}else {
				$count_not_ordered += $count_item;
			}
		}
		
		
		$strResult = "";
		
		if ($count_sold > 0){
			$strResult .= '<span style="color:#000; text-decoration: underline;">'.$count_sold.' - Scanned Out</span>';
		}
		
		if ($count_in_stock > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#000000">'.$count_in_stock.' - In Stock</span>';
		}
		
        if ($count_pending_scan > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
            $strResult .= '<span style="color:#666">'.$count_pending_scan.' - Pending Scan</span>';
		}
        
		if ($count_with_eta > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= $string_with_eta;
		}
		
		if ($count_pending_eta > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#548AC4">'.$count_pending_eta.' - Pending E.T.A</span>';
		}
		
		if ($count_waiting){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#ba9f07">'.$count_waiting.' - Waiting</span>';
		}
		
		if ($count_not_ordered > 0){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#C00000">'.$count_not_ordered.' - Not Ordered</span>';
		}
		
		if ($count_cancelled){
			$strResult = ($strResult == "")?'':$strResult.'<br/>';
			$strResult .= '<span style="color:#aaa">'.$count_cancelled.' - Cancelled Shipping</span>';
		}
		
		return $strResult;
	}
    */
    // return total price for the single item, e.g. for invoice
    public function getItemPrice($order = null){
        if (!$order){
            $order = $this->order;
        }
        
        if ($order->shipping_method == "Shipping") {
            $shipping_price = $this->items->shipping_price;
        } else if ($order->shipping_method == "Local Pickup") {
            $shipping_price = $this->items->local_pickup;
        } else if ($order->shipping_method == "Shipped from Los Angeles"){
            $shipping_price = $this->items->la_oc_shipping;
        } else if ($order->shipping_method == "Canada Shipping"){
            $shipping_price = $this->items->canada_shipping;
        }
        
        // echo $shipping_price;
        // die();
        
        return $this->sale_price + $shipping_price;
    }
    
    // returns total price for the row, all items together
    public function getTotalPrice($order = null){
        if (!$order){
            $order = $this->order;
        }
        
        if ($order->shipping_method == "Shipping") {
            $shipping_price = $this->items->shipping_price;
        } else if ($order->shipping_method == "Local Pickup") {
            $shipping_price = $this->items->local_pickup;
        } else if ($order->shipping_method == "Shipped from Los Angeles"){
            $shipping_price = $this->items->la_oc_shipping;
        } else if ($order->shipping_method == "Canada Shipping"){
            $shipping_price = $this->items->canada_shipping;
        }
        
        // echo $shipping_price;
        // die();
        
        return ($this->sale_price + $shipping_price) * $this->quantity + $this->special_shipping_price;
    }

    /**
     * Calculate average product supply shipping cost
     * @return float
     */
    public function getAverageItemSupplyShippingCost() {
        $itemsSupplyShippingCost = 0;
        foreach ($this->ind_items as $item)
            $itemsSupplyShippingCost += $item->getSupplyShippingCost();
        return ($this->quantity > 0)?
            $itemsSupplyShippingCost/$this->quantity : 0;
    }
    /**
     * Calculate average product holding cost
     * @return float
     */
    public function getHoldingCost()
    {
        $holdingCost = 0;
        foreach ($this->ind_items as $item) {
            $holdingCost += $item->getHoldingCostPerMonth() * $item->getHoldingPeriod();
        }
        return $holdingCost;
    }

    /**
     * Calculate Flat shipping fee for items
     * @return float
     */
    public function getFlatShippingFee()
    {
        return $this->items->getShippingCost($this->order->shipping_method) * $this->quantity;
    }
    /**
     * Get order items profit
     * @return float
     */
    public function getItemsProfit()
    {
        $item = $this->items;
        $profit = $this->quantity * $item->sale_price -
            ($item->fob_cost_price + $this->averageItemSupplyShippingCost)
            * $this->quantity - $this->getHoldingCost();
            //+ $this->getFlatShippingFee();
        //@todo remove this hacks
        $log = "Quantity[{$this->quantity}] * SalePrice[{$item->sale_price}] - "
            . "(FobCostPrice[{$item->fob_cost_price}] + AverageItemSupplyShippingCost[{$this->averageItemSupplyShippingCost}])"
            . "* Quantity[{$this->quantity}] - HoldingCost[{$this->holdingCost}] = $profit";
        Yii::app()->user->setState('calcLog', nl2br($log));
        return $profit;
    }
}