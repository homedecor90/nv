<?php

/**
 * This is the model class for table "customer_order_refund".
 *
 * The followings are the available columns in table 'customer_order_refund':
 * @property integer $id
 * @property integer $customer_order_id
 * @property double $amount
 * @property string $date
 */
class CustomerOrderRefund extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CustomerOrderRefund the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'customer_order_refund';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('customer_order_id, amount, date', 'required'),
            array('customer_order_id', 'numerical', 'integerOnly'=>true),
            array('amount', 'numerical'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, customer_order_id, amount, date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        	'order'=>array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'customer_order_id' => 'Customer Order',
            'amount' => 'Amount',
            'date' => 'Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('customer_order_id',$this->customer_order_id);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('date',$this->date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}