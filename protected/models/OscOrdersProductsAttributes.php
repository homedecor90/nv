<?php

/**
 * This is the model class for table "osc_orders_products_attributes".
 *
 * The followings are the available columns in table 'osc_orders_products_attributes':
 * @property integer $orders_products_attributes_id
 * @property integer $orders_id
 * @property integer $orders_products_id
 * @property string $products_options
 * @property string $products_options_values
 * @property string $options_values_price
 * @property string $price_prefix
 * @property integer $products_options_id
 * @property integer $products_options_values_id
 */
class OscOrdersProductsAttributes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OscOrdersProductsAttributes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'osc_orders_products_attributes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orders_id, orders_products_id, products_options_id, products_options_values_id', 'numerical', 'integerOnly'=>true),
			array('products_options', 'length', 'max'=>32),
			array('products_options_values', 'length', 'max'=>64),
			array('options_values_price', 'length', 'max'=>15),
			array('price_prefix', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('orders_products_attributes_id, orders_id, orders_products_id, products_options, products_options_values, options_values_price, price_prefix, products_options_id, products_options_values_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orders_products_attributes_id' => 'Orders Products Attributes',
			'orders_id' => 'Orders',
			'orders_products_id' => 'Orders Products',
			'products_options' => 'Products Options',
			'products_options_values' => 'Products Options Values',
			'options_values_price' => 'Options Values Price',
			'price_prefix' => 'Price Prefix',
			'products_options_id' => 'Products Options',
			'products_options_values_id' => 'Products Options Values',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('orders_products_attributes_id',$this->orders_products_attributes_id);
		$criteria->compare('orders_id',$this->orders_id);
		$criteria->compare('orders_products_id',$this->orders_products_id);
		$criteria->compare('products_options',$this->products_options,true);
		$criteria->compare('products_options_values',$this->products_options_values,true);
		$criteria->compare('options_values_price',$this->options_values_price,true);
		$criteria->compare('price_prefix',$this->price_prefix,true);
		$criteria->compare('products_options_id',$this->products_options_id);
		$criteria->compare('products_options_values_id',$this->products_options_values_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function importOscData($siteId){
        $last_order_id = Yii::app()->db->createCommand()
            ->select('orders_id')
            ->from('osc_orders_products_attributes')
            ->where('site_id=:siteId', array(':siteId'=>$siteId))
            ->order('orders_products_attributes_id desc')
            ->queryScalar();
		
		$sqlSelect = 'SELECT * FROM orders_products_attributes' . ($last_order_id ?
            " WHERE orders_id > $last_order_id" : '');
		$resource = mysql_query($sqlSelect);
		
		while ($row = mysql_fetch_array($resource)){
			$field_count = count(array_keys($row)) / 2;
			
			while ($field_count > 0){
				$field_count--;
				
				if (isset($row[$field_count])){
					unset($row[$field_count]);
				}
			}
			
			$newOscOrderProductsAttributesModel = new OscOrdersProductsAttributes();
			$newOscOrderProductsAttributesModel->orders_products_attributes_id = $row['orders_products_attributes_id'];
			$newOscOrderProductsAttributesModel->attributes = $row;
			$newOscOrderProductsAttributesModel->site_id = $siteId;
			$newOscOrderProductsAttributesModel->save();
		}
	}
}