<?php

/**
 * This is the model class for table "customer_order_billing_info".
 *
 * The followings are the available columns in table 'customer_order_billing_info':
 * @property string $id
 * @property string $customer_order_id
 * @property string $cc_number
 * @property string $cc_expiry_date
 * @property string $cc_name_on_card
 * @property string $cc_type
 * @property string $billing_name
 * @property string $billing_last_name
 * @property string $billing_street_address
 * @property string $billing_city
 * @property string $billing_state
 * @property string $billing_zip_code
 * @property string $billing_phone_number
 * @property string $billing_email_address
 * @property string $shipping_type
 * @property string $shipping_name
 * @property string $shipping_last_name
 * @property string $shipping_street_address
 * @property string $shipping_city
 * @property string $shipping_state
 * @property string $shipping_zip_code
 * @property string $shipping_phone_number
 * @property string $order_notes
 */
class CustomerOrderBillingInfo extends CActiveRecord
{
    public $cc_exp_month;
    public $cc_exp_year;
    public $ship_to_billing;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerOrderBillingInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_billing_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_order_id', 'required'),
			array(
                'cc_name_on_card, cc_number, cc_exp_month, cc_exp_year, cc_type,
                billing_name, billing_last_name, billing_street_address, billing_city,
                billing_state, billing_zip_code, billing_phone_number, billing_email_address,
                shipping_name, shipping_last_name,  shipping_street_address, shipping_city,
                shipping_state, shipping_zip_code, shipping_phone_number',
                'required',
                'on' => 'checkout'
            ),
            array('ship_to_billing', 'default', 'value' => 1, 'on' => 'checkout'),
			array('customer_order_id', 'length', 'max'=>10),
			array('cc_number, cc_expiry_date, cc_name_on_card, cc_type, billing_name, billing_last_name, billing_street_address, billing_city, billing_state, billing_zip_code, billing_phone_number, billing_email_address, shipping_type, shipping_name, shipping_last_name, shipping_street_address, shipping_city, shipping_state, shipping_zip_code, shipping_phone_number, busrestype', 'length', 'max'=>200),
			array('order_notes,shipping_notes', 'length', 'max'=>512),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_order_id, cc_number, cc_expiry_date, cc_name_on_card, cc_type, billing_name, billing_last_name, billing_street_address, billing_city, billing_state, billing_zip_code, billing_phone_number, billing_email_address, shipping_type, shipping_street_address, shipping_city, shipping_state, shipping_zip_code, shipping_phone_number, order_notes,shipping_notes', 'safe', 'on'=>'search'),
		);
	}

    public function beforeSave() {
        if (parent::beforeSave()) {
            $this->cc_number = '';
            return true;
        }
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'order' => array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        switch ($this->scenario) {
            case 'checkout':
                return array(
                    'id' => 'ID',
                    'customer_order_id' => 'Customer Order',
                    'cc_number' => 'Credit Card',
                    'cc_expiry_date' => 'Expire Date',
                    'cc_name_on_card' => 'Name On Card',
                    'cc_type' => 'Card Type',
                    'billing_name' => 'Firstname',
                    'billing_last_name' => 'Lastname',
                    'billing_street_address' => 'Street Address',
                    'billing_city' => 'City',
                    'billing_state' => 'State',
                    'billing_zip_code' => 'Zip Code',
                    'billing_phone_number' => 'Phone Number',
                    'billing_email_address' => 'Email Address',
                    'shipping_type' => 'Shipping Type',
                    'shipping_name' => 'Firstname',
                    'shipping_last_name' => 'Lastname',
                    'shipping_city' => 'City',
                    'shipping_state' => 'State',
                    'shipping_zip_code' => 'Zip Code',
                    'shipping_phone_number' => 'Phone Number',
                    'order_notes' => 'Order Notes',
                    'shipping_notes' => 'Shipping Notes',
                    'busrestype'=>'Address Type',
                    'ship_to_billing'=>'Ship to Billing',
                );
                break;
            default:
                return array(
                    'id' => 'ID',
                    'customer_order_id' => 'Customer Order',
                    'cc_number' => 'Credit Card',
                    'cc_expiry_date' => 'Expire Date',
                    'cc_name_on_card' => 'Name On Card',
                    'cc_type' => 'Card Type',
                    'billing_name' => 'Billing Name',
                    'billing_last_name' => 'Billing Last Name',
                    'billing_street_address' => 'Billing Street Address',
                    'billing_city' => 'Billing City',
                    'billing_state' => 'Billing State',
                    'billing_zip_code' => 'Billing Zip Code',
                    'billing_phone_number' => 'Billing Phone Number',
                    'billing_email_address' => 'Billing Email Address',
                    'shipping_type' => 'Shipping Type',
                    'shipping_name' => 'Shipping Name',
                    'shipping_last' => 'Shipping Last Name',
                    'shipping_city' => 'Shipping City',
                    'shipping_state' => 'Shipping State',
                    'shipping_zip_code' => 'Shipping Zip Code',
                    'shipping_phone_number' => 'Shipping Phone Number',
                    'order_notes' => 'Order Notes',
                    'shipping_notes' => 'Shipping Notes',
                    'busrestype'=>'Address Type',
                );
        }

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('customer_order_id',$this->customer_order_id,true);
		$criteria->compare('cc_number',$this->cc_number,true);
		$criteria->compare('cc_expiry_date',$this->cc_expiry_date,true);
		$criteria->compare('cc_name_on_card',$this->cc_name_on_card,true);
		$criteria->compare('cc_type',$this->cc_type,true);
		$criteria->compare('billing_name',$this->billing_name,true);
		$criteria->compare('billing_last_name',$this->billing_last_name,true);
		$criteria->compare('billing_street_address',$this->billing_street_address,true);
		$criteria->compare('billing_city',$this->billing_city,true);
		$criteria->compare('billing_state',$this->billing_state,true);
		$criteria->compare('billing_zip_code',$this->billing_zip_code,true);
		$criteria->compare('billing_phone_number',$this->billing_phone_number,true);
		$criteria->compare('billing_email_address',$this->billing_email_address,true);
		$criteria->compare('shipping_type',$this->shipping_type,true);
		$criteria->compare('shipping_street_address',$this->shipping_street_address,true);
		$criteria->compare('shipping_city',$this->shipping_city,true);
		$criteria->compare('shipping_state',$this->shipping_state,true);
		$criteria->compare('shipping_zip_code',$this->shipping_zip_code,true);
		$criteria->compare('shipping_phone_number',$this->shipping_phone_number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    static public function monthList()
    {
        return array(
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        );
    }

    static public function yearsList()
    {
        $year = date('Y');
        for ($i=0; $i<=10; $i++) {
            $years[$year + $i] = $year + $i;
        }
        return $years;
    }

    static public function cardTypes()
    {
        return array(
            'visa' => 'Visa',
            'mastercard' => 'Mastercard',
            'amex' => 'American Express',
            'discover' => 'Discover',
        );
    }

    static public function ListAddressTypes()
    {
        return array(
            'Residential' => 'Residential',
            'Business' => 'Business',
        );
    }
	/**
	 * Creates new billing info model and fills it with given data of $_POST if nothing given
	 * Model is not saved
	 * @param array $post_data
	 * @return CustomerOrderBillingInfo
	 */
	public static function getNewModelFromPost($post_data = null){
		if ($post_data == null){
			$post_data = $_POST;
		}
		
		$customerOrderBillingInfoModel = new CustomerOrderBillingInfo();
		
		if ($_POST['sh_radio']!='0') {
			$ship_to_firstname = $post_data['ShipFirstName'];
			$ship_to_lastname = $post_data['ShipLastName'];
			$ship_to_address=$post_data['ShipAddress'];
			$ship_to_city=$post_data['ShipCity'];
			$ship_to_state=$post_data['shipstate'];
			$ship_to_zip=$post_data['shippostcode'];
			$ship_to_phone=$post_data['shiptelephone'];
		} else {
			$ship_to_firstname = $post_data['fname'];
			$ship_to_lastname = $post_data['lname'];
			$ship_to_address=$post_data['street_address'];
			$ship_to_city=$post_data['city'];
			$ship_to_state=$post_data['state'];
			$ship_to_zip=$post_data['postcode'];
			$ship_to_phone=$post_data['telephone'];
		}
		
		$attributes = array(
				'cc_number'					=> '',
				'cc_expiry_date'			=> '',
				'cc_name_on_card'			=> isset($post_data['authorizenet_cc_owner']) ?
                        $post_data['authorizenet_cc_owner'] : '',
				'cc_type'					=> isset($post_data['credit_card_type']) ?
                        $post_data['credit_card_type'] : '',
		
				'billing_name'				=> $post_data['fname'],
				'billing_last_name'			=> $post_data['lname'],
				'billing_street_address'	=> $post_data['street_address'],
				'billing_city'				=> $post_data['city'],
				'billing_state'				=> $post_data['state'],
				'billing_zip_code'			=> $post_data['postcode'],
				'billing_phone_number'		=> $post_data['telephone'],
				'billing_email_address'		=> $post_data['email_address'],
		
				'shipping_type'				=> "",
				'shipping_name'	            => $ship_to_firstname,
				'shipping_last_name'	    => $ship_to_lastname,
				'shipping_street_address'	=> $ship_to_address,
				'shipping_street_address'	=> $ship_to_address,
				'shipping_city'				=> $ship_to_city,
				'shipping_state'			=> $ship_to_state,
				'shipping_zip_code'			=> $ship_to_zip,
				'shipping_phone_number'		=> $ship_to_phone,
		
				'order_notes'               => $post_data['notes'],
				'shipping_notes'            => $post_data['shipping_notes'],
				'busrestype'                => $post_data['busrestype'],
		);
		
		$customerOrderBillingInfoModel->setAttributes($attributes, false);
		return $customerOrderBillingInfoModel;
	}
}
