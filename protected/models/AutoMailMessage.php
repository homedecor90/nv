<?php
class AutoMailMessage{

	const MS_NEW_ORDER_CREATED = "New Order Created";

	const MC_NEW_ORDER_CREATED = "New order is created.";



	const MS_NEW_ITEMS_ADDED_TO_SUPPLIER_ORDER = "New Items Added";

	const MC_NEW_ITEMS_ADDED_TO_SUPPLIER_ORDER = "New items are added to your order.";



	const MS_QUANTITY_UPDATED_SUPPLIER_ORDER = "Supplier Order Updated";

	const MC_QUANTITY_UPDATED_SUPPLIER_ORDER = "Supplier order items quantity was updated by administrator.";



	const MS_ITEM_REMOVED_SUPPLIER_ORDER = "Supplier Order Updated";

	const MC_ITEM_REMOVED_SUPPLIER_ORDER = "Item \"[item_name]\" was removed from your order by administrator.";



	const MS_ADVANCED_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER = "Advanced Payment File Uploaded";

	const MC_ADVANCED_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER = "Advanced payment file is uploaded to you order.";



	const MS_FINAL_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER = "Final Payment File Uploaded";

	const MC_FINAL_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER = "Fianl payment file is uploaded to you order.";



	const MS_SUPPLIER_FINISH_ORDER = "Finish Supplier Order";

	const MC_SUPPLIER_FINISH_ORDER = " finishes his supplier order.";



	const MS_ADMIN_FINISH_ORDER = "Finish Supplier Order";

	const MC_ADMIN_FINISH_ORDER = " finishes supplier order for ";



	const MS_ADMIN_RESET_ORDER_MAKING = "Reset Supplier Order Status";

	const MC_ADMIN_RESET_ORDER_MAKING = " resets supplier order status to \"Making\" for ";



	const MS_ADMIN_RESET_ORDER_ADVANCED_PAID= "Reset Supplier Order Status";

	const MC_ADMIN_RESET_ORDER_ADVANCED_PAID = " resets supplier order status to \"Advanced Paid\" for ";



	const MS_ADMIN_SET_ORDER_ETL= "Set Supplier Order ETL";

	const MC_ADMIN_SET_ORDER_ETL = " sets supplier order ETL for ";



	const MS_SUPPLIER_DENY_ORDER = "Deny Supplier Order";

	const MC_SUPPLIER_DENY_ORDER = " denied his supplier order.";



	const MS_NEW_SUPPLIER_ORDER_CONTAINER_CREATED = "New Container Created";

	const MC_NEW_SUPPLIER_ORDER_CONTAINER_CREATED = "New supplier order container is created.";



	const MS_SUPPLIER_SHIPPER_PLACE_BID = "Supplier Shipper Place Bid";

	const MC_SUPPLIER_SHIPPER_PLACE_BID = " places the bid";



	const MS_SUPPLIER_SHIPPER_BID_SELECTED = "Supplier Shipper Selected";

	const MC_SUPPLIER_SHIPPER_BID_SELECTED = "Supplier shipper bid is selected.";



	const MS_SUPPLIER_ORDER_PENDING_ITEMS = "Pending Items";

	const MC_SUPPLIER_ORDER_PENDING_ITEMS = "You have pending order items.";



	const MS_SUPPLIER_ORDER_SET_ETL = "Set loading date";

	const MC_SUPPLIER_ORDER_SET_ETL = "[name], You must set loading date (ETL) for the pending order on NV

	You will continue to get reminder email until the loading date is set for the pending order after the advance has been paid.

	Set loading date on NV immediately,



	Furniture Store";



	const MS_SUPPLIER_ORDER_NEED_LOADING = "Loading Items";

	const MC_SUPPLIER_ORDER_NEED_LOADING = "You should load the item(s) for your order(s).";



	const MS_SUPPLIER_ORDER_NEED_UPLOADING_OFFICIAL_DOCUMENTS = "Uploading Official Documents";

	const MC_SUPPLIER_ORDER_NEED_UPLOADING_OFFICIAL_DOCUMENTS = "You should upload the official documents for your order(s).";



	const MS_CUSTOMER_ORDER_PROCESSING = "Process Order";

	const MC_CUSTOMER_ORDER_PROCESSING = "You should process your order(s).";



	const MS_SUPPLIER_ORDER_BOFORE_ETL = "Before E.T.L";

	const MC_SUPPLIER_ORDER_BOFORE_ETL = "Your order E.T.L is left ";



	const MS_SUPPLIER_ORDER_BOFORE_ETA_TO_SUPPLIER_SHIPPER = "Before E.T.A";

	const MC_SUPPLIER_ORDER_BOFORE_ETA_TO_SUPPLIER_SHIPPER = "Your container E.T.A is left ";



	const MS_NOT_ENOUGH_IN_STOCK_TO_ADMIN_OPERATOR = "Item Not Enough In Stock";

	const MC_NOT_ENOUGH_IN_STOCK_TO_ADMIN_OPERATOR = "Following items need to be ordered: ";



	const MS_SOLD_ITEM_NOT_IN_STOCK = "Item Not In Stock Sold";

	const MC_SOLD_ITEM_NOT_IN_STOCK = " is sold with items not in stock.";



	const MS_CUSTOMER_PICKUP_SCHEDULED = 'Your order pickup is scheduled';

	const MC_CUSTOMER_PICKUP_SCHEDULED = 'Hello,



	You can pickup your item on [date].

	Please bring the attached agreement signed along with your credit card that you paid with to the warehouse when you make your pickup. No inspections or pickups can be made without the signed copy of the agreement. The address to our warehouse is [warehouse_address].



	Cash you need to bring: $0

	Additional courtesy charges may apply if you miss your pickup appointment.



	Thank you,



	Regency Shop';



	const MS_CUSTOMER_PICKUP_SCHEDULED_BY_CUSTOMER = 'Order Pickup';

	const MC_CUSTOMER_PICKUP_SCHEDULED_BY_CUSTOMER = 'Hello,



	Your item is ready for pickup from our warehouse. Please schedule the date and time for pickup by clicking the link below:

	[link]



	Thank you,

	Regency Shop';



	const MS_CUSTOMER_SHIPPER_MANIFEST_EMAIL = 'Shipments are ready for pickup';

	const MC_CUSTOMER_SHIPPER_MANIFEST_EMAIL = 'Hello [name]



	You have [shipments] to be picked up from our warehouse in [company_city] on [date].

	The manifest attached to this email.



	Thank you,



	Furniture Store

	';



	const MS_CUSTOMER_ADDITIONAL_PAYMENT_REQUIRED = 'Beyond point shipping address';

	const MC_CUSTOMER_ADDITIONAL_PAYMENT_REQUIRED = 'Dear [name]

	Congratulations on the purchase on the items below:



	[items]



	As listed on our website, some US addresses come-up as “beyond point” areas and require a minor shipping charge*

	Your shipping address is coming up as beyond point.

	Please make a payment of $[amount] at the link below so that we can ship your furniture as soon as possible.

	[link]



	Thank you,

	Jenny

	RegencyShop.com



	*For beyond point details please visit any product page on our website or Terms of Service.

	';



	const MS_CUSTOMER_TRACKING_NUMBER_SET = 'Tracking Number';

	const MC_CUSTOMER_TRACKING_NUMBER_SET = 'Dear [name]

	Your items below have shipped:



	[items]



	*************************************************

	PLEASE READ CAREFULLY

	Once you receive the item, please be sure to carefully inspect everything. Any item signed-off as clear upon delivery cannot be exchanged, replaced or refunded.

	Please be sure to carefully inspect not only the box but also each piece for potential damage. We package all of our items extremely well, but in rare cases in-transit damages do occur.

	Just like you would not want to pay for a damaged item, please do not expect us to replace the item if you do not inspect your item carefully upon delivery.

	The driver is REQUIRED to wait until you inspect the merchandise and sign-off on the receipt.



    CURBSIDE DELIVERY ONLY

    Please remember that the basic free shipping for all furniture includes delivery to your curbside, not inside your home.  We advise scheduling someone to help you inspect and carry your new furniture into your home.



	*************************************************

	And Congratulations! You have ordered beautiful furniture that will last you for many years to come. You can track your shipment at [website] with tracking number [number]



	QUESTIONS ABOUT YOUR SHIPMENT:

	If you have any question about your shipment, then please call the shipper [company] directly at [phone_number]. Please have your tracking number handy, when you call.



	Be sure to check our website at www.regencyshop.com for all your furniture needs.

	Thank you

	Furniture Store';



	const MS_SHIPPER_SHIPMENT_REQUEUED = 'Shipment Requeued';

	const MC_SHIPPER_SHIPMENT_REQUEUED = 'Shipment has been requeued. Your rate quote and BOL have been cancelled. This shipment may ship out in the future and you can submit your quote at that time.

	Shipment ID: [id]

	Destination zip code: [zip]';



	const MS_SHIPPER_BOL_REMINDER = 'BOL upload reminder';

	const MC_SHIPPER_BOL_REMINDER = 'Dear [name]

	Please submit your BOL for shipments listed below:

	[shipments]



	Please upload your BOL’s immediately to avoid delays.

	Thank you

	Furniture Store';



	const MS_SHIPPER_BOL_RESET = 'BOL deleted';

	const MC_SHIPPER_BOL_RESET = 'Dear [name]

	BOL for shipping QUOTE number has been deleted, please reupload your BOL



	Thank you

	Furniture Store';



	const MS_SHIPPER_NOTIFICATION = 'Shipment Quotes Reminder';

	const MC_SHIPPER_NOTIFICATION = 'Dear [name]

	Please ensure that you have reviewed and submitted quotes on the [count] shipments listed on NV.



	Thank you,

	Furniture Store';



	const MS_SHIPPER_NEW_SHIPMENT_AUTO = 'New Shipment Added';

	const MC_SHIPPER_NEW_SHIPMENT_AUTO = 'Dear [name]

	A new shipment has been added to NV

	Please login and submit your quote



	[shipment_table]

	

	Thank you,

	Furniture Store';





	const MS_CUSTOMER_REVIEW = 'How did we do?';

	const MC_CUSTOMER_REVIEW = 'Hello [name],



	We are sure that you are enjoying your [items].

	We work very hard to bring you the best service and product possible. Please let us know about your experience to share with our managers.



	Employee promotions, raises and ..ahem .. terminations, yikes! depend on customer experience, so please be kind and honest  :)



	[url]





	Thank you and I will be looking forward to serving you again



	RegencyShop Customer Experience Team';





	const MS_SHIPPER_BID_CANCELLED = 'Bid cancelled';

	const MC_SHIPPER_BID_CANCELLED = 'Dear [name]

	Your quote [quote] for [quote_number] has been cancelled.

	Please resubmit your quote on NV.



	Thank you,

	Furniture Store';



	const MS_CUSTOMER_ORDER_VERIFICATION = 'Order Verification';

	const MC_CUSTOMER_ORDER_VERIFICATION = 'Hi [name],



	Thank you for your order of [items].

	We need further verification for your order since the billing address and shipping address do not match.

	Please send any one of the following:



	1. Front and back of credit card used or

	2. Copy of Driver’s License displaying billing address or

	3. Utility Bill displaying billing address



	Please note this is a necessary practice to prevent online fraudulent transactions. You may black out the first 12 digits of your credit card as well as information you do not wish to display to us on any of your documents. As soon your addresses are confirmed, we’ll be able to ship your items and send tracking information.



	Thank you,



	Regency Shop';



	const MS_CUSTOMER_SHIPPER_CLAIM = 'Shipment Damage #[quote_number]';

	const MC_CUSTOMER_SHIPPER_CLAIM = 'Dear [name]



	Shipment going to [zip] for [customer_name], [quote_number] has been delivered damaged.



	Please open a claim for this shipment.



	Thank you,



	Furniture Store';

	const MS_CUSTOMER_SHIPPING_UPDATED = 'Shipping Updated';

	const MC_CUSTOMER_SHIPPING_UPDATED = 'Hello, [name]

	Please make the balance payment of $[amount] based on your updated shipping.

	You can securely make the payment at the link below:

	[link]



	Thank you,

	Furniture Store';



	const MS_SUPPLIER_ETL_CHANGED_BY_ADMIN = 'Shipping Updated';

	const MC_SUPPLIER_ETL_CHANGED_BY_ADMIN = 'Dear [name],



	The loading date for Order #[id] has been changed from [old_etl] to [new_etl]

	Make sure you finish this order by [new_etl]



	Thank you,

	Furniture Store';



	const MS_CLAIM_DOC_UPLOADED = 'Claim for [tracking_number]';

	const MC_CLAIM_DOC_UPLOADED = 'Dear [name]

	Attached are the file/s for Claim related to shipment going to [customer_name], with tracking number:  [tracking_number]. The quote number for this shipment is: [quote_number]



	Thank you,

	Home Decor';



	const MS_CS_CLAIM_REMINDER = 'Claim for [tracking_number]';

	const MC_CS_CLAIM_REMINDER = 'Dear [name]

	We had filed a claim for Shipment number [tracking_number] for [customer_name] over 30 days ago. This claim needs to be resolved immediately.



	Thank you,

	Home Decor';



	const MS_CLAIM_RESOLVED = 'Claim resolved';

	const MC_CLAIM_RESOLVED = 'Hello [name]

	Claim for shipment [tracking_number] has been marked as resolved



	Thank you,

	Home Decor';



	const MS_RETURN_AUTHORIZED = 'Shipment Return';

	const MC_RETURN_AUTHORIZED = 'Dear [name]

	A return for shipment number [tracking_number] for [customer_name] has been authorized. Please submit your quote for this return shipment.



	Thank you,

	Home Decor';



	const MS_CS_INVOICE_PAID = 'Invoice Payment Made';

	const MC_CS_INVOICE_PAID = 'Dear [name]<br />

	A payment of $[total] has been made for the invoices listed below using Check #[check_number] on [date].<br />

	<br />

	Thank you,<br />

	Furniture Store<br />';



	const MS_RETURN_CANCELLED = 'Shipment Return Cancelled';

	const MC_RETURN_CANCELLED = 'Dear [name]

	Return Shipment number [quote_number] has been canceled. Please be advised that this shipment has NOT been authorized for return.



	Thank you,

	Home Decor';



	const MS_CUSTOMER_SHIPMENT_REQUEUED = 'Pickup Rescheduled';

	const MC_CUSTOMER_SHIPMENT_REQUEUED = 'Hello [name]

	Your pickup has been rescheduled. You will be receiving another email wth your new pickup date and time.



	Thank you,

	Furniture Store';



	const MS_ADMIN_3RD_PARTY_ORDER_ADDED = 'Third party order added';

	const MC_ADMIN_3RD_PARTY_ORDER_ADDED = 'Third party order added. You can approve or deny it by the link: [link]';



	const MS_ADMIN_NEW_CUSTOMER_REVIEW = 'New Customer Review';

	const MC_ADMIN_NEW_CUSTOMER_REVIEW = 'New customer review received.<br /><br />

	<strong>Customer name: </strong>[customer_name]<br />

	<strong>Order ID: </strong>[order_id]<br />

	<strong>Overall: </strong>[overall]<br />

	<strong>Cost: </strong>[cost]<br />

	<strong>Future Shopping: </strong>[future_shopping]<br />

	<strong>Shipping: </strong>[shipping]<br />

	<strong>Customer Service: </strong>[customer_service]<br />

	<strong>Return: </strong>[return]<br /><br />

	<strong>Review Text: </strong>[text]

	';

    const MS_RETAILER_TRANSACTION_LARGE = 'Large Transaction from Retailer';

    const MS_RETAILER_INVENTORY = 'Inventory - Regency Shop';
    const MC_RETAILER_INVENTORY = 'Hello!

Attached is the copy of our current inventory this week.
If you have problem viewing this email or need something not listed in our warehouse, feel free to contact us at toll-free 1 866 776 2680


As always RegencyShop.com is always here to serve your needs.


Regards,
RegencyShop.com Team
';

    const MS_TRANSACTION_CONFIRMATION = 'Order Confirmation - Furniture Store';
    const MC_TRANSACTION_CONFIRMATION = "
    Thank you [name] for your purchase at the modern furniture store.<br><br>

    PLEASE READ THIS EMAIL CAREFULLY AS IT CONTAINS IMPORTANT INFORMATION
    ABOUT YOUR ORDER<br>
    =========================================================<br><br>

    1. CURBSIDE DELIVERY ONLY: Please remember that the basic free shipping
    for all furniture includes delivery to your curbside, not inside your
    home.  We advise scheduling someone to help you inspect the item and
    carry your new furniture into your home.<br><br>

    2. DAMAGE INSPECTION: Once you receive the item, please be sure to
    carefully inspect everything. Any item signed-off as clear upon delivery
    cannot be exchanged, replaced or refunded. Please be sure to carefully
    inspect not only the box but also each piece for potential damage. We
    package all of our items extremely well, but in rare cases in-transit
    damages do occur. We cannot replace, refund or exchange any items that
    were marked as clear upon delivery.  The driver is REQUIRED to wait
    until you inspect the merchandise and sign-off on the receipt.<br><br>

    3. NAME ON THE CREDIT CARD STATEMENT: Your credit card statement will
    show company name depending on the website that you purchased the
    furniture from. Your statement may show: [company] as the
    seller. IMPORTANT: Please ensure that you do not dispute this charge.
    Disputing a charge immediately sends your account into collections and
    credit bureaus.<br><br>

    4. SHIPMENT TRACKING: Once your item leaves our warehouse, you will be
    receiving tracking number by email. In-stock items will typically ship
    within 2 business days of payment and arriving in 5-7 business days to
    your curb. Lead time on items not in stock can be a bit longer.<br><br>


    You have purchased great quality furniture that will last you for years
    to come. We want to make this experience memorable and wonderful for you.<br><br>


    If you have any questions, feel free to call customer service at (323)
    747 5378, Monday - Friday from 9am to 4pm PST.<br><br>


    Thank you and we look forward to serving you.<br><br>

    Modern Furniture Shop<br><br>


    YOUR ORDER DETAILS<br>
    ===================<br><br>

    [items]
    ";

    const MS_SUPPLIER_ORDER_NOTE = 'Note added to Order#{order_id}';
    const MC_SUPPLIER_ORDER_NOTE = '{note}';
/*
    const MS_ORDER_REMINDER = 'Order Reminder';
    const MC_ORDER_REMINDER = "
		Hello [name]<br>

		Just wondering if you are still interested in getting your items <br><br>
			.CustomerOrder::getCustomerOrderItemsForInvoice($this->id).'<br/><br/>'
            .([description] ? 'Regular price: $'.$total_price.'<br />Discount: $'.$dsc_amount.'<br /><br />Your investment: $'.$dsc_total : 'Your investment: $'.$total_price).'<br />'
			.'<br /><br/>'
			.'You can securely make the payment at the link below:<br/><br/>'
			.'<a href="'.$checkout_url.'">'.$checkout_url.'</a><br/><br/>'
			// .'We will process your order as soon as you make the payment. Feel free to call me anytime at 1-866-776-2680.<br/><br/>'
			.'Looking forward to serving all your furniture needs,<br />'
			.'Lisa G.'
			.'<br /><br />'
			.'Note: Prices are subject to change without notice. If you do not purchase this item within 7 days of this invoice email, then price difference will have to be charged for the order to be processed.'
			;

    ";
*/
    const MS_ORDER_DISCOUNTED_REMINDER = 'Order Reminder';
    const MC_ORDER_DISCOUNTED_REMINDER = '
    Hello {name},

    Good news! We have decided to give you a discount for the items below:

    {items}

    Standard Price: ${standard_price}
    Discounted Price: ${discounted_price}
    Savings: ${savings}

    It gets better, all items come with FREE SHIPPING!

    You can securely make the payment at the link below:
    {checkout_url}

    Feel free to call us anytime at 1-866-776-2680
    We are looking forward to serving all your furniture needs.

    Regards,
    RegencyShop Team
    ';
}
