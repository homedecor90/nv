<?php

/**
 * Class CustomerOrderReview
 *
 * Relations:
 * @property CustomerOrderReviewImage[] $images
 */
class CustomerOrderReview extends CActiveRecord
{
    const RESELLERRATINGS_URL = 'http://www.resellerratings.com/store/survey/new/Test_com';
    //const RESELLERRATINGS_URL = 'http://www.resellerratings.com/store/survey/new/RegencyShop';

	public static $overall_list = array(1=>'Very Dissatisfied', 2=>'Somewhat Dissatisfied', 3=>'Neither Satisfied nor Dissatisfied', 4=>'Somewhat Satisfied', 5=>'Very Satisfied');
	public static $cost_list = array('N/A', 'Very High Priced', 'High priced', 'Moderately priced', 'Low priced', 'Very low priced');
	public static $future_shopping_list = array('N/A', 'Not at all likely', 'Not very likely', 'Somewhat likely', 'Very likely', 'Extremely likely');
	public static $shipping_list = array('N/A', 'Poor', 'Fair', 'Good', 'Very Good', 'Excellent');
	public static $customer_service_list = array('N/A', 'Poor', 'Fair', 'Good', 'Very Good', 'Excellent');
	public static $return_list = array('N/A', 'Poor', 'Fair', 'Good', 'Very Good', 'Excellent');

    protected $postData;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CustomerOrder the static model class
     */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_order_id, date, overall, cost, future_shopping, shipping, customer_service, return, review_text', 'required'),
			//array('id, sales_person_id, customer_id, shipping_method, status, created_on, discounted, grand_total_price, discounted_total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer_order'=>array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
            'images' => array(self::HAS_MANY, 'CustomerOrderReviewImage', 'review_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'customer_order_id'=>'Order ID',
			'date'=>'Review Date'
		);
	}
	
	public function search($order_status = '')
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
	
		$criteria=new CDbCriteria;
		$criteria->order = 'id DESC';
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'=>array(
						'pageSize'=>20,
				),
		));
	}

    /**
     * Prepares post data for ResellerRatings
     * @return string
     */
    public function getPostData(){
        if (!isset($this->postData)) {
            $customer = $this->customer_order->customer;
            $username = strtolower(strtr(
                $customer->first_name . $customer->last_name, array(
                    ' ' => '',
                )
            ));
            $password = substr(md5(uniqid()), 0, 8);

            $this->rr_username = $username;
            $this->rr_password = $password;

            $data = 'comment='.urlencode($this->review_text);
            $data .= '&invoice_number='.$this->customer_order_id;
            $data .= '&transaction_date='.urlencode(date('m/d/Y', strtotime($this->customer_order->sold_date)));
            $data .= '&question[]=2';
            $data .= '&rating[2]='.urlencode(self::$overall_list[$this->overall]);
            $data .= '&question[]=3';
            $data .= '&rating[3]='.urlencode(self::$cost_list[$this->cost]);
            $data .= '&question[]=10';
            $data .= '&rating[10]='.urlencode(self::$future_shopping_list[$this->future_shopping]);
            $data .= '&question[]=18';
            $data .= '&rating[18]='.urlencode(self::$shipping_list[$this->shipping]);
            $data .= '&question[]=33';
            $data .= '&rating[33]='.urlencode(self::$customer_service_list[$this->customer_service]);
            $data .= '&question[]=41';
            $data .= '&rating[41]='.urlencode(self::$return_list[$this->return]);
            $data .= '&login[username]=';
            $data .= '&login[password]=';
            $data .= '&signup[username]='.urlencode($username);
            $data .= '&signup[email]='.urlencode($this->customer_order->customer->email);
            $data .= '&signup[password]='.urlencode($password);
            $data .= '&signup[password_confirm]='.urlencode($password);
            $data .= '&signup[age]=yes&signup[hide_my_email]=yes';
            $this->postData = $data;
        }
		return $this->postData;
	}

    public function publish()
    {
        $ch = curl_init(self::RESELLERRATINGS_URL);
        curl_setopt_array($ch, array(
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS => $this->getPostData(),
        ));
        $result = curl_exec($ch);
        if (strpos($result, 'we\'ve got your review') !== false) {
            $this->posted_to_rr = 1;
            $this->save();
            return true;
        } else {
            Yii::log(
                'Error publishing review' . CVarDumper::dumpAsString($this->postData),
                'error'
            );
            echo $result;
            return false;
        }
    }

    public function afterSave() {
        parent::afterSave();
        $this->addImages();
    }

    /**
     * Add images saved from user session
     * @throws Exception
     */
    public function addImages()
    {
        if( Yii::app( )->user->hasState( 'xuploadFiles' ) ) {
            $userImages = Yii::app( )->user->getState( 'xuploadFiles' );
            foreach( $userImages as $image ) {

                if( is_file( $image["path"] ) ) {
                    $img = new CustomerOrderReviewImage( );
                    $img->setAttributes(array(
                        'size' => $image["size"],
                        'mime' => $image["mime"],
                        'name' => $image["name"],
                        'filename' => $image["url"],
                        'review_id' => $this->id,
                    ));
                    if(!$img->save()) {
                        //Its always good to log something
                        Yii::log( "Could not save Image:\n".CVarDumper::dumpAsString(
                                $img->getErrors( ) ), CLogger::LEVEL_ERROR );
                        //this exception will rollback the transaction
                        throw new Exception( 'Could not save Image');
                    }
                } else {
                    var_dump($image);
                    //You can also throw an exception here to rollback the transaction
                    Yii::log( $image["path"]." is not a file", CLogger::LEVEL_WARNING );
                }
            }
            //Clear the user's session
            Yii::app( )->user->setState( 'xuploadFiles', null );
        }
    }

}
?>
