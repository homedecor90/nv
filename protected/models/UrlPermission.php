<?php



/**
 * This is the model class for table "url_permission".
 *
 * The followings are the available columns in table 'url_permission':
 * @property string $id

 * @property string $title

 * @property string $url

 */

class UrlPermission extends CActiveRecord

{



	/**

	 * Returns the static model of the specified AR class.

	 * @param string $className active record class name.

	 * @return UrlPermission the static model class

	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	/**

	 * @return string the associated database table name

	 */

	public function tableName()

	{

		return 'url_permission';

	}



	/**

	 * @return array validation rules for model attributes.

	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(

			array('title, url', 'required'),

			array('title, url', 'length', 'max'=>500),

			// The following rule is used by search().

			// Please remove those attributes that should not be searched.

			array('id, title, url', 'safe', 'on'=>'search'),

		);

	}



	/**

	 * @return array relational rules.

	 */

	public function relations()

	{

		// NOTE: you may need to adjust the relation name and the related

		// class name for the relations automatically generated below.

		return array(

		);

	}



	/**

	 * @return array customized attribute labels (name=>label)

	 */

	public function attributeLabels()

	{

		return array(

			'id' => 'ID',

			'title' => 'Title',

			'url' => 'Url',

		);

	}



	/**

	 * Retrieves a list of models based on the current search/filter conditions.

	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.

	 */

	public function search()

	{

		// Warning: Please modify the following code to remove attributes that

		// should not be searched.



		$criteria=new CDbCriteria;



		$criteria->compare('id',$this->id,true);

		$criteria->compare('title',$this->title,true);

		$criteria->compare('url',$this->url,true);



		return new CActiveDataProvider($this, array(

			'criteria'=>$criteria,

			'pagination'=>array( 

				'pageSize'=>100, 

			),

		));

	}

	

	public static function getPermissionGroupString($url_id){

		$urlPermissionModel = UrlPermission::model()->findByPk($url_id);

		$arrUrlGroupIDList = explode("_", $urlPermissionModel->url_group);

		$criteriaGroup = new CDbCriteria;

		$criteriaGroup->addInCondition('id', $arrUrlGroupIDList);

		

		$arrUrlGroupList = UrlGroup::model()->findAll($criteriaGroup);

		

		$strResult = "";

		$i = 1;

		foreach ($arrUrlGroupList as $urlGroup){

			$strGroup = "- ".$urlGroup->title;

			if ($strResult == ""){

				$strResult = $strGroup;

			} else {

				$strResult .= "<br/>".$strGroup;

			}

			$i++;

		}

		

		return $strResult;

	}

	

	public static function updateUrlPermissionGroup($url_id_list, $permission_group_id_list){

		$arrUrlIdList = explode("_", $url_id_list);

		

		$criteriaPermission = new CDbCriteria;

		$criteriaPermission->addInCondition('id', $arrUrlIdList);

		$attributesPermission = array(

			'url_group'=>$permission_group_id_list,

		);

		

		UrlPermission::model()->updateAll($attributesPermission, $criteriaPermission);

	}

	

	public static function getPermissionRoleString($url_id){

		$urlPermissionModel = UrlPermission::model()->findByPk($url_id);

		$arrUrlGroupIdList = explode("_", $urlPermissionModel->url_group);

		

		$criteriaPermission = new CDbCriteria;

		$criteriaPermission->addInCondition('id', $arrUrlGroupIdList);

		$urlGroupList = UrlPermission::model()->findAll($criteriaPermission);

		

		$groupStringList = array();

		foreach ($urlGroupList as $urlGroup){

			$url_group_id = $urlGroup->id;

			

			$criteriaUrl = new CDbCriteria;

			$criteriaUrl->compare('url_group_id', $url_group_id);

			$urlGroupRoleList = UrlGroupRole::model()->findAll($criteriaUrl);

			

			foreach ($urlGroupRoleList as $urlGroupRole){

				if (!in_array($urlGroupRole->role, $groupStringList)){

					$groupStringList[] = $urlGroupRole->role;

				}

			}

		}

		

		$result = implode(", ", $groupStringList);

		return $result;

	}

	

	public static function checkUrlPermission($data){

		$objAction = $data['action'];

		$controller_name = $objAction->getController()->getId();

		$action_name = $objAction->getId();

		$action_url = $controller_name."/".$action_name;

		$criteriaUrl = new CDbCriteria;

		$criteriaUrl->compare('url', $action_url);

		$urlPermissionModel = UrlPermission::model()->find($criteriaUrl);

		$arrUrlGroupIdList = explode("_", $urlPermissionModel->url_group);

		

		$criteriaPermission = new CDbCriteria;

		$criteriaPermission->addInCondition('id', $arrUrlGroupIdList);

		$urlGroupList = UrlPermission::model()->findAll($criteriaPermission);

		

		$groupStringList = array();

		foreach ($urlGroupList as $urlGroup){

			$url_group_id = $urlGroup->id;

			

			$criteriaUrl = new CDbCriteria;

			$criteriaUrl->compare('url_group_id', $url_group_id);

			$urlGroupRoleList = UrlGroupRole::model()->findAll($criteriaUrl);

			

			foreach ($urlGroupRoleList as $urlGroupRole){

				if (!in_array($urlGroupRole->role, $groupStringList)){

					$roleList[] = $urlGroupRole->role;

				}

			}

		}

		// print_r($roleList);

		// die();

		if (in_array(UrlGroup::$ROLE_ALL_VISITORS, $roleList)){

			return true;

		}

		

		if (in_array(UrlGroup::$ROLE_ALL_AUTHENTICATED_USERS, $roleList)){

			$user_id = Yii::app()->user->id;

			if ($user_id != 0){

				return true;

			}

		}

		

		if (Yii::app()->user->isAdmin()){

			if (in_array(UrlGroup::$ROLE_ADMIN, $roleList)){

				return true;

			}

		} 

        if (YumUser::isSalesPerson()){

			if (in_array(UrlGroup::$ROLE_SALES_PERSON, $roleList)){

				return true;

			}

		} 

        if (YumUser::isOperator()){

			if (in_array(UrlGroup::$ROLE_OPERATOR, $roleList)){

				return true;

			}

		} 

        if (YumUser::isSupplier(false)){

			if (in_array(UrlGroup::$ROLE_SUPPLIER, $roleList)){

				return true;

			}

		}

		if (YumUser::isCustomerShipper(false)){

			if (in_array(UrlGroup::$ROLE_CUSTOMER_SHIPPER, $roleList)){

				return true;

			}

		}

		if (YumUser::isWarehouse(false)){

			if (in_array(UrlGroup::$ROLE_WAREHOUSE, $roleList)){

				return true;

			}

		}

        if (YumUser::isSupplierShipper(false)){

			if (in_array(UrlGroup::$ROLE_SUPPLIER_SHIPPER, $roleList)){

				return true;

			}

		}

		

		if (YumUser::isCustomerService(false)){

			if (in_array(UrlGroup::$ROLE_CUSTOMER_SERVICE, $roleList)){

				return true;

			}

		}

		

		if (YumUser::isDataEntry(false)){

			

			if (in_array(UrlGroup::$ROLE_DATA_ENTRY, $roleList)){

				return true;

			}

		}

		if (YumUser::isRetailer(false)){

			

			if (in_array(UrlGroup::$ROLE_RETAILER, $roleList)){

				return true;

			}

		}
		

		return false;

	}

}