<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property string $id
 * @property double $tax_percent
 */
class Settings extends CActiveRecord
{
    public static $row;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
                'tax_percent, company_name, company_name_2, company_address_street,
			    company_address_city, company_address_state, company_address_zip,
			    sales_phone_number, customer_service_phone_number, payment_processor',
                'required'
            ),
			array('tax_percent', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tax_percent, company_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tax_percent' => 'Tax Percent',
			'company_name' => 'Company Name',
			'company_address_street' => 'Street Address',
			'company_address_city' => 'City',
			'company_address_state' => 'State',
			'company_address_zip' => 'Zip',
			'sales_phone_number' => 'Sales Phone Number',
			'customer_service_phone_number' => 'Customer Service Phone Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tax_percent',$this->tax_percent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getSettingVariables(){
		$settingModel = Settings::model()->findByPk(1);
		return $settingModel;
	}

    public static function getVar($var_name){
        if (!self::$row){
            self::$row = self::getSettingVariables();
        }
        return self::$row->$var_name;
    }
}