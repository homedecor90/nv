<?php

/**
 * This is the model class for table "customer_payment".
 *
 * The followings are the available columns in table 'customer_payment':
 * @property integer $id
 * @property integer $customer_id
 * @property string $type
 * @property integer $item_id
 * @property double $amount
 * @property string $date
 */
class CustomerPayment extends CActiveRecord
{
	const TYPE_ORDER_MAIN 				= 1;
	const TYPE_ORDER_SHIPPING_EDITED 	= 2;
	const TYPE_SHIPMENT_ADDITIONAL 		= 3;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, type, item_id, amount, date', 'required'),
			array('customer_id, item_id', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('type', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_id, type, item_id, amount, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Customer',
			'type' => 'Type',
			'item_id' => 'Item',
			'amount' => 'Amount',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}