<?php

class AutoMail{
	
	const EMAIL_FROM = 'Furniture Store';
	
	public static function sendAutoMail(){
		
	}
	
	public static function sendMail($to, $subject, $message, $from_current_user = false){		
        $user_id = $from_current_user ? Yii::app()->user->id : 1;
        $adminModel = YumUser::model()->findByPk($user_id);
		$adminMailAddress = $adminModel->profile->email;
	
		$email = Yii::app()->email;
		
		$email->to = $to;
		$email->from = $adminMailAddress;
		$email->subject = $subject;
		$content = $message;
		
		$email->message = $content;
		$email->send();
		
		//$filename = Yii::getPathOfAlias("application")."/timestamp.txt";
		$strResult = "To:".$to."\n"
			."Subject:".$subject."\n"
			."Message:".$message."\n"
			."Time:".date("Y-m-d H:i:s", time())."\n\n";
		
		//file_put_contents($filename, $strResult);
		
		$filename = Yii::getPathOfAlias("application")."/mail_log.txt";
		$mail_log_file = @fopen($filename, 'a');
		@fputs($mail_log_file, $strResult);
		@fclose($mail_log_file);
	}
	
	public static function getOperatorUserIdList(){
		$criteriaUser = new CDbCriteria;
		$criteriaUser->with = "roles";
		$criteriaUser->compare("role_id", 6);
		$criteriaUser->compare("status", '<>'.YumUser::STATUS_REMOVED);
		$userList = YumUser::model()->findAll($criteriaUser);
		$arrUserId = array();
		
		foreach ($userList as $userModel){
			$arrUserId[] = $userModel->id;
		}
		return $arrUserId;
	}
	
	/**
	 * @return array array of strings
	 */
	public static function getAdminOperatorEmails(){
		$ids = self::getOperatorUserIdList();
		$ids[] = 1;
		$emails = array();
		$criteria = new CDbCriteria();
		$criteria->addInCondition('user_id', $ids);
		$profiles = YumProfile::model()->findAll($criteria);
		foreach ($profiles as $profile){
			$emails[] = $profile->email;
		}
		
		return $emails;
	}
	
	public static function getAdminOperatorWarehouseUserIdList(){
		$criteriaUser = new CDbCriteria;
		$criteriaUser->with = "roles";
		$criteriaUser->compare("role_id", 6);
		$criteriaUser->compare("role_id", 11, false, 'or');
		$criteriaUser->compare("status", '<>'.YumUser::STATUS_REMOVED);
		$userList = YumUser::model()->findAll($criteriaUser);
		$arrUserId = array(1);
		
		foreach ($userList as $userModel){
			$arrUserId[] = $userModel->id;
		}
		return $arrUserId;	
	}
	
	public static function getSupplierShipperUserIdList(){
		$criteriaUser = new CDbCriteria;
		$criteriaUser->with = "roles";
		$criteriaUser->compare("role_id", 7);
        $criteriaUser->compare("status", '<>'.YumUser::STATUS_REMOVED);
		$userList = YumUser::model()->findAll($criteriaUser);
		$arrUserId = array();
		
		foreach ($userList as $userModel){
			$arrUserId[] = $userModel->id;
		}
		return $arrUserId;
	}
	
	public static function sendMailNewSupplierOrderCreated($supplier_id){
		$supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
	
		$subject = AutoMailMessage::MS_NEW_ORDER_CREATED;
		$message = AutoMailMessage::MC_NEW_ORDER_CREATED;
		AutoMail::sendMail($supplier_mail_address, $subject, $message, false); //true for send from user who create order
	}
	
    public static function sendMailBankInfoAccepted($supplier_id){
		$supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
	
		$subject = 'Bank Info Accepted';
		$message = 'Your bank info was accepted by admin.';
		AutoMail::sendMail($supplier_mail_address, $subject, $message, true);
	}
    
	public static function sendMailItemRemovedSupplierOrder($supplier_id, $item_id){
		$supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
		$item = Item::model()->findByPk($item_id);
		
		$subject = AutoMailMessage::MS_ITEM_REMOVED_SUPPLIER_ORDER;
		$message = str_replace('[item_name]', $item->item_name, AutoMailMessage::MC_ITEM_REMOVED_SUPPLIER_ORDER);
		AutoMail::sendMail($supplier_mail_address, $subject, $message, true);
	}
	
    public static function sendMailQuantityUpdatedSupplierOrder($supplier_id){
        $supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
		
		$subject = AutoMailMessage::MS_QUANTITY_UPDATED_SUPPLIER_ORDER;
		$message = AutoMailMessage::MC_QUANTITY_UPDATED_SUPPLIER_ORDER;
		AutoMail::sendMail($supplier_mail_address, $subject, $message, true);
    }
    
	public static function sendMailNewItemsAddedToSupplierOrder($supplier_id){
		$supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
	
		$subject = AutoMailMessage::MS_NEW_ITEMS_ADDED_TO_SUPPLIER_ORDER;
		$message = AutoMailMessage::MC_NEW_ITEMS_ADDED_TO_SUPPLIER_ORDER;
		AutoMail::sendMail($supplier_mail_address, $subject, $message, false);
	}
	
	public static function sendSupplierAcceptItemsReminders(){
		$criteria = new CDbCriteria();
		$criteria->compare('t.status', SupplierOrderItems::$STATUS_PENDING);
		$criteria->with = 'order';
		$criteria->together = true;
		$items = SupplierOrderItems::model()->findAll($criteria);
		$suppliers = array();
		foreach ($items as $item){
			if ($item->order->status != SupplierOrder::$STATUS_PENDING){
				$suppliers[] = $item->order->supplier_id;
			}
		}
		
		$suppliers = array_unique($suppliers);
		foreach ($suppliers as $supplier){
			self::sendMailNewItemsAddedToSupplierOrder($supplier);
		}
	}
	
	public static function sendMailAdvnacedPaymentFileUploaded($supplier_id){
		$supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
	
		$subject = AutoMailMessage::MS_ADVANCED_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER;
		$message = AutoMailMessage::MC_ADVANCED_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER;
		AutoMail::sendMail($supplier_mail_address, $subject, $message, false);
	}
	
	public static function sendMailFinalPaymentFileUploaded($supplier_id){
		$supplierModel = YumUser::model()->findByPk($supplier_id);
		$supplier_mail_address = $supplierModel->profile->email;
	
		$subject = AutoMailMessage::MS_FINAL_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER;
		$message = AutoMailMessage::MC_FINAL_PAYMENT_FILE_UPLOADED_TO_SUPPLIER_ORDER;
		AutoMail::sendMail($supplier_mail_address, $subject, $message, false);
	}
	
	public static function sendMailSupplierFinishOrder($supplier_id){
		$supplierName = YumUser::model()->getUserName($supplier_id);
		$subject = AutoMailMessage::MS_SUPPLIER_FINISH_ORDER;
		$message = AutoMailMessage::MC_SUPPLIER_FINISH_ORDER;
		$message = $supplierName.$message;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
		
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
    public static function sendMailAdminFinishOrder($admin_id, $supplier_id){
		$adminName = YumUser::model()->getUserName($admin_id);
        $supplierName = YumUser::model()->getUserName($supplier_id);
		$subject = AutoMailMessage::MS_ADMIN_FINISH_ORDER;
		$message = AutoMailMessage::MC_ADMIN_FINISH_ORDER;
		$message = $adminName.$message.$supplierName;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
        $arrUserIdList[] = $supplier_id;
		
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
    
    public static function sendMailAdminSetOrderETL($admin_id, $supplier_id){
		$adminName = YumUser::model()->getUserName($admin_id);
        $supplierName = YumUser::model()->getUserName($supplier_id);
		$subject = AutoMailMessage::MS_ADMIN_SET_ORDER_ETL;
		$message = AutoMailMessage::MC_ADMIN_SET_ORDER_ETL;
		$message = $adminName.$message.$supplierName;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
        $arrUserIdList[] = $supplier_id;
		
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
    
    public static function sendMailAdminResetOrderMaking($admin_id, $supplier_id){
		$adminName = YumUser::model()->getUserName($admin_id);
        $supplierName = YumUser::model()->getUserName($supplier_id);
		$subject = AutoMailMessage::MS_ADMIN_RESET_ORDER_MAKING;
		$message = AutoMailMessage::MC_ADMIN_RESET_ORDER_MAKING;
		$message = $adminName.$message.$supplierName;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
		$arrUserIdList[] = $supplier_id;
        
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
    
    public static function sendMailAdminResetOrderAdvancedPaid($admin_id, $supplier_id){
		$adminName = YumUser::model()->getUserName($admin_id);
        $supplierName = YumUser::model()->getUserName($supplier_id);
		$subject = AutoMailMessage::MS_ADMIN_RESET_ORDER_ADVANCED_PAID;
		$message = AutoMailMessage::MC_ADMIN_RESET_ORDER_ADVANCED_PAID;
		$message = $adminName.$message.$supplierName;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
		$arrUserIdList[] = $supplier_id;
        
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
    
    public static function sendMailSupplierDenyOrder($supplier_id){
		$supplierName = YumUser::model()->getUserName($supplier_id);
		$subject = AutoMailMessage::MS_SUPPLIER_DENY_ORDER;
		$message = AutoMailMessage::MC_SUPPLIER_DENY_ORDER;
		$message = $supplierName.$message;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
		
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
    
	public static function sendMailNewSupplierOrderContainerCreated($container){		
		$subject = AutoMailMessage::MS_NEW_SUPPLIER_ORDER_CONTAINER_CREATED;

		$supplierShipperUserList = AutoMail::getSupplierShipperUserIdList();
		
		foreach ($supplierShipperUserList as $user_id){
			$criteria = new CDbCriteria();
			$criteria->compare('supplier_order_container_id', $container->id);
			$criteria->compare('supplier_shipper_id', $user_id);
			$count = SupplierOrderContainerBidding::model()->count($criteria);
			if (!$count){
				$userModel = YumUser::model()->findByPk($user_id);
				$userMailAddress = $userModel->profile->email;
				
				$message = 'A new container has been added to the furniture store. Please submit your rate quote at the link below:<br />
					'.Yii::app()->getBaseURL(true).'/supplierorder/container_detail_by_shipper/?container_id='.$container->id.'<br />
					Username: '.$userModel->username.'<br />
					Pass: **hidden for security** You can reset your password if you don\'t remember it.<br />
					Thank you<br />
					Furniture Store';
				
				AutoMail::sendMail($userMailAddress, $subject, $message, true);
			}
		}
	}
	
	public static function sendMailSupplierShipperPlaceBid($supplier_shipper_id){
		$supplierName = YumUser::model()->getUserName($supplier_shipper_id);
		$subject = AutoMailMessage::MS_SUPPLIER_SHIPPER_PLACE_BID;
		$message = AutoMailMessage::MC_SUPPLIER_SHIPPER_PLACE_BID;
		$message = $supplierName.$message;
		
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
		
		foreach ($arrUserIdList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
	public static function sendMailBidSelected($container_id){		
		$subject = AutoMailMessage::MS_SUPPLIER_SHIPPER_BID_SELECTED;
		$message = AutoMailMessage::MC_SUPPLIER_SHIPPER_BID_SELECTED;		
		
		$message .= Yii::app()->getBaseUrl(true).'/index.php/supplierorder/container_detail_by_shipper/?container_id='.$container_id;
		
		$supplierShipperUserList = AutoMail::getSupplierShipperUserIdList();
		
		foreach ($supplierShipperUserList as $user_id){
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
	/////////////////////////////////////////////////////////////////////
	//  Auto E-mail
	
	public static function sendMailSupplierOrderPendingItems(){
		$subject = AutoMailMessage::MS_SUPPLIER_ORDER_PENDING_ITEMS;
		$message = AutoMailMessage::MC_SUPPLIER_ORDER_PENDING_ITEMS;
		$supplierIdList = SupplierOrder::model()->getPendingSupplierOrderItemsSupplierIdList();
		
		foreach ($supplierIdList as $supplier_id){
			$userModel = YumUser::model()->findByPk($supplier_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
	public static function sendMailSupplierOrderSetETL(){
		$subject = AutoMailMessage::MS_SUPPLIER_ORDER_SET_ETL;
		$message = AutoMailMessage::MC_SUPPLIER_ORDER_SET_ETL;
		$supplierIdList = SupplierOrder::model()->getSupplierOrderSetETLSupplierIdList();
		
		foreach ($supplierIdList as $supplier_id){
			$userModel = YumUser::model()->with('profile')->findByPk($supplier_id);
			$userMailAddress = $userModel->profile->email;
			$name = $userModel->profile->firstname.' '.$userModel->profile->lastname;
			$message = str_replace('[name]', $name, $message);
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
	public static function sendMailSupplierOrderLoading(){
		$subject = AutoMailMessage::MS_SUPPLIER_ORDER_NEED_LOADING;
		$message = AutoMailMessage::MC_SUPPLIER_ORDER_NEED_LOADING;
		$supplierIdList = SupplierOrder::model()->getSupplierOrderNeedLoadingSupplierIdList();
		
		foreach ($supplierIdList as $supplier_id){
			$userModel = YumUser::model()->findByPk($supplier_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
	public static function sendMailSupplierOrderUploadingOfficialDocuments(){
		$subject = AutoMailMessage::MS_SUPPLIER_ORDER_NEED_UPLOADING_OFFICIAL_DOCUMENTS;
		$message = AutoMailMessage::MC_SUPPLIER_ORDER_NEED_UPLOADING_OFFICIAL_DOCUMENTS;
		$supplierIdList = SupplierOrder::model()->getSupplierOrderNeedUploadingOfficialDocumentsSupplierIdList();
		
		foreach ($supplierIdList as $supplier_id){
			$userModel = YumUser::model()->findByPk($supplier_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, false);
		}
	}
	
	public static function sendMailSupplierOrderUploadedOfficialDocumentsBySupplier($order_id, $docs_path){
		$order = SupplierOrder::model()->findByPk($order_id);
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		
		$user_id = $from_current_user ? Yii::app()->user->id : 1;
        $adminModel = YumUser::model()->findByPk($user_id);
		$adminMailAddress = $adminModel->profile->email;
		if ($shipper_id = $order->getShipperID()){
			$shipper_user  	= $adminModel = YumUser::model()->findByPk($shipper_id);
			$shipper_name  	= $shipper_user->profile->firstname.' '.$shipper_user->profile->lastname;
			$shipper_email 	= $shipper_user->profile->email;
		}

		$subject 	= 'Official docs uploaded by supplier';
		$body 		= 'Official docs uploaded for the order #'.$order_id.'.';
		
		$mail = new JPhpMailer;
		$mail->IsSendmail();
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->AddAddress($adminMailAddress, 'Admin');
		$mail->AddAttachment($docs_path);
		$mail->SetFrom($adminMailAddress, 'Admin');
		
		$mail->Send();
		if ($shipper_id){
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->Subject = $subject;
			$mail->Body = $body;
			$mail->AddAddress($shipper_email, $shipper_name);
			$mail->AddAttachment($docs_path);
			$mail->SetFrom($adminMailAddress, 'Admin');
			
			$mail->Send();
		}
		
		$strResult = "To:".$adminMailAddress.($shipper_id ? ', '.$shipper_email : '')."\n"
			."Subject:".$subject."\n"
			."Message:".$body."\n"
			."Time:".date("Y-m-d H:i:s", time())."\n\n";

		$filename = Yii::getPathOfAlias("application")."/mail_log.txt";
		$mail_log_file = @fopen($filename, 'a');
		@fputs($mail_log_file, $strResult);
		@fclose($mail_log_file);
	}
	/*
    public static function sendMailSupplierOrderSetETL(){
        $subject = AutoMailMessage::MS_SUPPLIER_ORDER_NEED_ETL;
		$message = AutoMailMessage::MC_SUPPLIER_ORDER_NEED_ETL;
		$supplierIdList = SupplierOrder::model()->getSupplierOrderNeedETLSupplierIdList();
		// print_r($supplierIdList);
        // die();
		foreach ($supplierIdList as $supplier_id){
			$userModel = YumUser::model()->findByPk($supplier_id);
			$userMailAddress = $userModel->profile->email;
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
    }
    */
	public static function sendMailCustomerOrderProcessing(){
		$subject = AutoMailMessage::MS_CUSTOMER_ORDER_PROCESSING;
		$message = AutoMailMessage::MC_CUSTOMER_ORDER_PROCESSING;
		$customerEmailList = CustomerOrder::model()->getCustomerOrderNeedProcessCustomerEmailList();
		
		foreach ($customerEmailList as $cusomter_email){
			AutoMail::sendMail($cusomter_email, $subject, $message);
		}
	}
	
	public static function sendMailSupplierOrderBeforeETL(){
		$supplierList = SupplierOrder::model()->getSupplierOrderBeforeETLSupplierList();
		
		foreach ($supplierList as $supplier){
			$subject = AutoMailMessage::MS_SUPPLIER_ORDER_BOFORE_ETL;
			$message = AutoMailMessage::MC_SUPPLIER_ORDER_BOFORE_ETL.$supplier['diff_days']." day(s).";
			
			AutoMail::sendMail($supplier['email_address'], $subject, $message, true);
		}
	}
	
	public static function sendMailSupplierOrderContainerBeforeETASupplierShipper(){
		$supplierShipperList = SupplierOrder::model()->getSupplierOrderBeforeETASupplierShipperList();
		
		foreach ($supplierShipperList as $supplier_shipper){
			$subject = AutoMailMessage::MS_SUPPLIER_ORDER_BOFORE_ETA_TO_SUPPLIER_SHIPPER;
			$message = AutoMailMessage::MC_SUPPLIER_ORDER_BOFORE_ETA_TO_SUPPLIER_SHIPPER.$supplier_shipper['diff_days']." day(s).";
			
			AutoMail::sendMail($supplier_shipper['email_address'], $subject, $message, true);
		}
	}
	
	public static function sendMailNotEnoughInStockToAdminOperator(){
		$criteria = new CDbCriteria();
		$criteria->order = 'item_name';
		$items = Item::model()->findAll($criteria);
		$item_list = array();

		foreach($items as $item){
			if ($item->toOrder(true) > 0){
				$item_list[] = '- '.$item->item_name.' ('.$item->item_code.')';
			}
		}

		if (count($item_list)){
			$operatorUserList = AutoMail::getOperatorUserIdList();
			$arrUserIdList = array_merge(array(1), $operatorUserList);
			$subject = AutoMailMessage::MS_NOT_ENOUGH_IN_STOCK_TO_ADMIN_OPERATOR;
			$message = AutoMailMessage::MC_NOT_ENOUGH_IN_STOCK_TO_ADMIN_OPERATOR.'<br />'.implode('<br />', $item_list);
			
			foreach ($arrUserIdList as $user_id){
				$userModel = YumUser::model()->findByPk($user_id);
				$userMailAddress = $userModel->profile->email;
				AutoMail::sendMail($userMailAddress, $subject, $message, true);
			}			
		}
	}
	
	public static function sendMailNotInStockItemSold($item_id){
		$operatorUserList = AutoMail::getOperatorUserIdList();
		$arrUserIdList = array_merge(array(1), $operatorUserList);
		
		foreach ($arrUserIdList as $user_id){
			$subject = AutoMailMessage::MS_SOLD_ITEM_NOT_IN_STOCK;
			$message = AutoMailMessage::MC_SOLD_ITEM_NOT_IN_STOCK;
			$itemModel = Item::model()->findByPk($item_id);
			$message = $itemModel->item_code.$message;
		
			$userModel = YumUser::model()->findByPk($user_id);
			$userMailAddress = $userModel->profile->email;			
			AutoMail::sendMail($userMailAddress, $subject, $message, true);
		}
	}
	
	public static function sendCustomerReminders(){
		// we need to send reminder if customer's last order is pending for 1, 3, 6, 10 or 15 days
		$criteria = new CDbCriteria();
		$criteria->with = 'last_order';
		$criteria->together = true;
		$customers = Customer::model()->findAll($criteria);
		$now = time();
		foreach ($customers as $customer){
			if ($customer->last_order->status == CustomerOrder::$STATUS_PENDING){
				$days = floor(($now - strtotime($customer->last_order->created_on))/86400);
				if ($days == 0 
					|| $days == 3
					|| $days == 6
					|| $days == 10
					|| $days == 15){
                        $order = $customer->last_order;
                        if ($days == 3 && $order->discounted_total == 0) {
                            $order->updateTotals();
                            $order->sendCustomerDiscountedReminder();
                        } else $order->sendCustomerReminder();
				}
			}
		}
	}
	
	public static function getAdminEmail(){
        $adminModel = YumUser::model()->with('profile')->findByPk(1);
		return $adminModel->profile->email;
	}
	
	public static function sendShipperBolReminders(){
		$criteria = new CDbCriteria();
		$criteria->compare('t.status', CustomerOrderShipment::$STATUS_SHIPPER_PICKUP);
		$criteria->addCondition('t.bol_path = ""');
		$criteria->with = array('shipper', 'customer_order', 'customer_order.billing_info');
		$criteria->together = true;
		$shipments = CustomerOrderShipment::model()->findAll($criteria);
		$by_shipper = array();
		foreach ($shipments as $shipment){
			$by_shipper[$shipment->shipper_id][] = $shipment;
		}
		
		foreach ($by_shipper as $shipper_id => $shipments){
			$user = YumUser::model()->findByPk($shipper_id);
			$name = $user->profile->firstname.' '.$user->profile->lastname;
			
			$shipment_str = array();
			foreach ($shipments as $shipment){
				$shipment_str[] = '#'.$shipment->id
					.' - '.$shipment->customer_order->billing_info->shipping_zip_code
					.' - '.$shipment->customer_order->billing_info->busrestype;
			}
			
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			
			$from_email = AutoMail::getAdminEmail();
			$subject = AutoMailMessage::MS_SHIPPER_BOL_REMINDER;
			
			$mail = new JPhpMailer;
			$mail->IsSendmail();
			$mail->AddAddress($user->profile->email, $name);
			$mail->SetFrom($from_email, AutoMail::EMAIL_FROM);
			$mail->Subject = $subject;
			$body = str_replace('[name]', $name, AutoMailMessage::MC_SHIPPER_BOL_REMINDER);
			$body = str_replace('[shipments]', implode("\n", $shipment_str), $body);
			$mail->Body = $body;
			$mail->Send();
			if ($add_emails = $user->profile->getAdditionalEmails()){
				foreach ($add_emails as $email){
					$mail = new JPhpMailer;
					$mail->IsSendmail();
					$mail->AddAddress($email, $name);
					$mail->SetFrom($from_email, AutoMail::EMAIL_FROM);
					$mail->Subject = $subject;
					$mail->Body = $body;
					$mail->Send();
				}
			}
		}
	}
	
	public static function sendReviewNotifications(){
		$criteria = new CDbCriteria();
		$criteria->compare('t.status', CustomerOrder::$STATUS_SOLD);
		$criteria->compare('t.review_placed', 0);
		$criteria->addCondition('t.first_shipment_completed > DATE_SUB(NOW(), INTERVAL 26 DAY)');
		$criteria->with = array('customer', 'order_items', 'order_items.items');
		$criteria->together = true;
		$orders = CustomerOrder::model()->findAll($criteria);
		$now = time();
	
		foreach ($orders as $order){
            $days = floor(($now - strtotime($order->first_shipment_completed))/86400);
            if ($days == 15 || $days == 18 || $days == 25){
                // check if customer did not post review yet or did not get notification at least
                $criteria = new CDbCriteria();
                $criteria->compare('customer_id', $order->customer_id);
                $criteria->compare('review_notification_sent', 1);
                $criteria->addCondition('id != '.$order->id);
                if (!CustomerOrder::model()->count($criteria)){
                    //echo 'notification';
                    $order->sendReviewNotification();
                }
            }
        }
	}
	
	public static function sendCSResolveClaim(){
		$criteria = new CDbCriteria();
		$criteria->compare('claim', 1);
		$criteria->compare('claim_resolved', 0);
		$claims = CustomerOrderShipment::model()->with('shipper', 'shipper.profile', 'customer_order', 'customer_order.billing_info')->findAll($criteria);
		$claims_to_send = array();
		$now = time();
		foreach ($claims as $claim){
			$days = floor(($now - strtotime($order->claim_date))/86400);
			if ($days == 30){
				$claims_to_send[] = $claim;
			}
		}
		
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		
		foreach($claims_to_send as $claim){
			$emails 	= $claim->shipper->profile ? $claim->shipper->profile->getAdditionalEmails() : array();
			$emails[] 	= $claim->shipper->profile->email;
			$emails[] 	= $claim->shipper->profile->claim_email;
			
			$shipper = $claim->shipper;
			$shipper_name = $shipper->profile->firstname.' '.$shipper->profile->lastname;
			$customer_name = $claim->customer_order->billing_info->shipping_name.' '.$claim->customer_order->billing_info->shipping_last_name;
			
			$subject = str_replace('[tracking_number]', $claim->tracking_number, AutoMailMessage::MS_CS_CLAIM_REMINDER);
			$search = array('[name]','[tracking_number]','[customer_name]');
			$replace = array($shipper_name, $claim->tracking_number, $customer_name);
			$body = str_replace($search, $replace, AutoMailMessage::MC_CS_CLAIM_REMINDER);
			
			foreach ($emails as $email){
				if ($email){
					$mail = new JPhpMailer;
					$mail->IsSendmail();
					$mail->AddAddress($email);
					$mail->SetFrom(AutoMail::getAdminEmail(), AutoMail::EMAIL_FROM);
					$mail->Body = $body;
					$mail->Subject = $subject;
					$mail->Send();
				}
			}
		}
	}
}
