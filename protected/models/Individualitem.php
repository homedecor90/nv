<?php

/**
 * This is the model class for table "individual_item".
 *
 * The followings are the available columns in table 'individual_item':
 * @property string $id
 * @property string $item_id
 * @property double $sale_price
 * @property double $exw_cost_price
 * @property double $fob_cost_price
 * @property double $shipping_price
 * @property double $local_pickup
 * @property double $la_oc_shipping
 * @property double $canada_shipping
 * @property double $cbm
 * @property string $dimensions_length
 * @property string $dimensions_width
 * @property string $dimensions_height
 * @property double $weight
 * @property string $description
 * @property string $condition
 * @property string $status
 * @property string $customer_order_items_id
 * @property string $customer_order_shipment_id
 * @property integer $prev_shipment_id
 * @property integer $supplier_order_id
 * @property integer $supplier_order_container_id
 * @property string $cutoff_date
 * @property string $eta
 * @property string $loaded
 * @property integer $denied_by_supplier
 * @property string $sold_date
 * @property double $shipping_total
 * @property integer $shipping_canceled
 * @property string $shipping_canceled_date
 * @property string $shipping_canceled_item_id
 * @property integer $chargeback
 * @property string $scanned_out_date
 * @property string $scanned_in_date
 * @property integer $damaged
 * @property string $damaged_boxes
 * @property integer $replaced
 * @property string $replacement_order_id
 * @property integer $is_replacement
 * @property string $replacement_item
 * @property string $replacement_boxes
 * @property integer $reordered_from_supplier
 * @property integer $is_supplier_replacement
 * @property string $supplier_replacement_boxes
 * @property double $supplier_replacement_cbm
 * @property integer $claim_refund_id
 * @property integer $cancellation_reason
 * @property integer $shipping_started
 * @property integer $customer_return_id
 */ 
class Individualitem extends CActiveRecord
{
    const COST_OF_CAPITAL_OPPORTUNITY = 0.0058;
    const WAREHOUSE_RENTAL_PER_MONTH = 1900;
    const WAREHOUSE_AREA = 576000;//meters squared
    const WAREHOUSE_PRODUCT_LEVELS = 3;
    const DEFAULT_SUPPLY_COST_PER_CBM = 100;

    public $item_code;
	public $color;
	public $status_list;
	public $num;
	public $sold_state;
    public $filled;
    
    public $_fake_status 			= null; 
    public $_fake_eta 				= null;
    public $_available_for_shipping	= false;

    /**
     * @var string
     * @deprecated
     */
    public static $STATUS_SOLD 					= "Scanned OUT";
	public static $STATUS_SCANNED_OUT			= "Scanned OUT";
	public static $STATUS_NOT_ORDERED 			= "Not Ordered";
	public static $STATUS_ORDERED_WITHOUT_ETL 	= "Ordered Without E.T.L";
	public static $STATUS_ORDERED_WITH_ETL 		= "Ordered With E.T.L";
	public static $STATUS_ORDERED_WITH_ETA 		= "Ordered With E.T.A";
    /**
     * @var string
     * @deprecated
     */
    public static $STATUS_PENDING_SCAN			= "Pending Scan";
	public static $STATUS_IN_STOCK				= "In Stock";
// 	public static $STATUS_DENIED				= "Denied";
    /**
     * @var string
     * @deprecated
     */
    public static $STATUS_DENIED				= "Not Ordered";
	public static $STATUS_CANCELLED_SHIPPING	= "Cancelled Shipping";
	public static $STATUS_ORDERED_BUT_WAITING	= "Ordered But Waiting Accept/Deny";
	
	public static $LS_PENDING			= "Pending Loaded";
	public static $LS_LOADED			= "Loaded";
	
	public $count_eta_grou_by_container_id;
	public $count_by_customer_id;
	public $qty;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Individualitem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
        return 'individual_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, sale_price, exw_cost_price, fob_cost_price, shipping_price, local_pickup, la_oc_shipping, canada_shipping, cbm, condition', 'required'),
			array('prev_shipment_id, supplier_order_id, supplier_order_container_id, denied_by_supplier, shipping_canceled, chargeback, damaged, replaced, is_replacement, reordered_from_supplier, is_supplier_replacement, claim_refund_id, cancellation_reason, shipping_started, customer_return_id', 'numerical', 'integerOnly'=>true),
            array('sale_price, exw_cost_price, fob_cost_price, shipping_price, local_pickup, la_oc_shipping, canada_shipping, cbm, weight, shipping_total, supplier_replacement_cbm', 'numerical'),
			array('item_id, customer_order_items_id, customer_order_shipment_id', 'length', 'max'=>10),
			array('dimensions_length, dimensions_width, dimensions_height, condition, status', 'length', 'max'=>100),
			array('loaded', 'length', 'max'=>45),
			array('description, cutoff_date, eta, sold_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_id, sale_price, exw_cost_price, fob_cost_price, shipping_price, local_pickup, la_oc_shipping, canada_shipping, cbm, dimensions_length, dimensions_width, dimensions_height, weight, description, condition, status, customer_order_items_id, customer_order_shipment_id, prev_shipment_id, supplier_order_id, supplier_order_container_id, cutoff_date, eta, loaded, sold_date, shipping_total, shipping_canceled, shipping_canceled_date, shipping_canceled_item_id, chargeback, scanned_out_date, scanned_in_date, damaged, damaged_boxes, replaced, replaced_order_id, is_replacement, replacement_item, replacement_boxes, reordered_from_supplier, is_supplier_replacement, supplier_replacement_boxes, supplier_replacement_cbm, claim_refund_id, cancellation_reason, shipping_started', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items'=>array(self::BELONGS_TO, 'Item', 'item_id'),
			'customer_order_items'=>array(self::BELONGS_TO, 'CustomerOrderItems', 'customer_order_items_id'),
			'shipment'=>array(
					self::BELONGS_TO, 
					'CustomerOrderShipment', 
					'customer_order_shipment_id',
			),
			'prev_shipment'=>array(
					self::BELONGS_TO, 
					'CustomerOrderShipment', 
					'prev_shipment_id',
					'with' => 'customer_order'
			),
			'return'=>array(
					self::BELONGS_TO,
					'IndividualItemReturn',
					'return_id',
			),
            'supplier_order'=>array(self::BELONGS_TO, 'SupplierOrder', 'supplier_order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => 'Item',
			'sale_price' => 'Sale Price',
			'exw_cost_price' => 'EXW Cost Price',
			'fob_cost_price' => 'FPB Cost Price',
			'shipping_price' => 'Shipping Price',
			'local_pickup' => 'Local Pickup',
			'la_oc_shipping' => 'La Oc Shipping',
			'canada_shipping' => 'Canada Shipping',
			'cbm' => 'Cbm',
			'dimensions_length' => 'Dimensions Length',
			'dimensions_width' => 'Dimensions Width',
			'dimensions_height' => 'Dimensions Height',
			'weight' => 'Weight',
			'description' => 'Description',
			'condition' => 'Condition',
			'status' => 'Status',
			'status list' => 'Status List',
			'customer_order_items_id' => 'Customer Order Items',
			'supplier_order_id' => 'Supplier Order',
			'supplier_order_container_id' => 'Supplier Order Container',
			'cutoff_date' => 'Cut-Off Date',
			'eta' => 'E.T.A',
			'loaded' => 'Loaded',
			'sold_date' => 'Sold Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		if ($this->status_list != ""){
			$arrStatusList = explode("_", $this->status_list);
			
			if ($arrStatusList[0] == "1"){
				$criteria->compare('status', Individualitem::$STATUS_SOLD, true, "or");
			}
			
			if ($arrStatusList[1] == 1){
				$criteria->compare('status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
			}
			
			if ($arrStatusList[2] == 1){
				$criteria->compare('status', Individualitem::$STATUS_ORDERED_WITHOUT_ETL, true, "or");
			}
			
			if ($arrStatusList[3] == 1){
				$criteria->compare('status', Individualitem::$STATUS_ORDERED_WITH_ETL, true, "or");
			}
			
			if ($arrStatusList[4] == 1){
				$criteria->compare('status', Individualitem::$STATUS_IN_STOCK, true, "or");
			}

			if ($arrStatusList[5] == 1){
				$criteria->compare('status', Individualitem::$STATUS_DENIED, true, "or");
			}
			
			if ($arrStatusList[6] == 1){
				$criteria->compare('status', Individualitem::$STATUS_ORDERED_BUT_WAITING, true, "or");
			}
		} else {
			$criteria->compare('status', $this->status,true);
		}

		$criteria->with = array('items', 'supplier_order', 'supplier_order.supplier', 'supplier_order.supplier.profile');
        $criteria->together = true;
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.item_id',$this->item_id,false);
		$criteria->compare('t.sale_price',$this->sale_price);
		$criteria->compare('t.exw_cost_price',$this->exw_cost_price);
		$criteria->compare('t.fob_cost_price',$this->fob_cost_price);
		$criteria->compare('t.shipping_price',$this->shipping_price);
		$criteria->compare('t.local_pickup',$this->local_pickup);
		$criteria->compare('t.la_oc_shipping',$this->la_oc_shipping);
		$criteria->compare('t.canada_shipping',$this->canada_shipping);
		$criteria->compare('t.cbm',$this->cbm);
		$criteria->compare('t.dimensions_length',$this->dimensions_length,true);
		$criteria->compare('t.dimensions_width',$this->dimensions_width,true);
		$criteria->compare('t.dimensions_height',$this->dimensions_height,true);
		$criteria->compare('t.weight',$this->weight);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.condition',$this->condition,true);
		$criteria->compare('customer_order_items_id',$this->customer_order_items_id,true);
		
		if ($this->item_code != NULL){
			$criteria->compare('items.item_code', $this->item_code, true);
		}
		
		if ($this->color != NULL){
			$criteria->compare('items.color', $this->color, true);
		}
		
		$criteria->together = true;

		$result = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>50, 
			),
		));
		
		return $result;
	}
	
	public function searchQueue()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		if ($this->status_list != ""){
			$arrStatusList = explode("_", $this->status_list);
			
			if ($arrStatusList[1] == 1){
				$criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
			}
			

			if ($arrStatusList[5] == 1){
				$criteria->compare('t.status', Individualitem::$STATUS_DENIED, true, "or");
			}
		} else {
			if ($this->status == ""){
				$criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
				$criteria->compare('t.status', Individualitem::$STATUS_DENIED, true, "or");
			} else {
				$criteria->compare('t.status', $this->status,true);
			}
		}

		$criteria->with = array('items');
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.item_id',$this->item_id,true);
		$criteria->compare('t.sale_price',$this->sale_price);
		$criteria->compare('t.exw_cost_price',$this->exw_cost_price);
		$criteria->compare('t.fob_cost_price',$this->fob_cost_price);
		
		if ($this->item_code != NULL){
			$criteria->compare('items.item_code', $this->item_code, true);
		}
		
		if ($this->color != NULL){
			$criteria->compare('items.color', $this->color, true);
		}
		
		$criteria->together = true;

		$result = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>300, 
			),
		));
		
		return $result;
	}
	
	public function searchQueueInDash()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
		$criteria->compare('t.status', Individualitem::$STATUS_DENIED, true, "or");
		$criteria->with = array('items');
		$criteria->together = true;

		$result = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>300, 
			),
		));
		
		return $result;
	}
	
	public function searchByDates($date_from, $date_to){
		$criteria = new CDbCriteria();
		$criteria->with = 'items';
        $criteria->together = true;
		$criteria->group = 'item_id';
		$criteria->select = 'COUNT(t.id) as qty, item_id';
		$criteria->addCondition('NOT ISNULL(items.item_name)');

		if ($date_from && $date_to){
			$criteria->addCondition('sold_date >= "'.$date_from.'"');
			$criteria->addCondition('sold_date <= ADDDATE("'.$date_to.'", 1)');
		}

        $sort = new CSort();
        $sort->attributes = array(
            'items.item_name'=>array(
                'asc'=>'items.item_name ASC',
                'desc'=>'items.item_name DESC',
                'default'=>'desc'
            ),
            'qty'=>array(
                'asc'=>'qty ASC',
                'desc'=>'qty DESC',
                'default'=>'desc'
            ),
        );
        $sort->defaultOrder = 'items.item_name ASC';

        $per_page   = isset($_GET['per_page']) ? $_GET['per_page'] : 25;
        if ($per_page == 0){
            $pagination = false;
        } else {
            $pagination = new CPagination();
            $pagination->pageSize = $per_page;
            $pagination->itemCount = 100;
        }

		$data_provider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$sort,
			'pagination'=>$pagination
		));
//        $data_provider->setTotalItemCount();
        return $data_provider;
	}
	
	public static function customerOrdered($individualitem_id){
		$individualItemModel = Individualitem::model()->findByPk($individualitem_id);
		
		$strOrdered = "";
		if ($individualItemModel->customer_order_items_id == 0){
			$strOrdered = "No";
		} else {
			$strOrdered = "Yes";
		}
		
		return $strOrdered;
	}
	
	public static function getNotOrderedItemCount(){
		$criteriaItemStatus = new CDbCriteria();
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_DENIED, true, "OR");
		$criteriaItemStatus->compare('status', Individualitem::$STATUS_NOT_ORDERED, true, "OR");
		$count_not_ordered = Individualitem::model()->count($criteriaItemStatus);
		return $count_not_ordered;
	}
	
	public function searchDeniedItemList()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		
		$criteria->select = 't.*, count(t.id) as num, t.sold_date>"0000-00-00 00:00:00" as sold_state';
        $criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
		$criteria->compare('t.status', Individualitem::$STATUS_DENIED, true, "or");
		$criteria->group = 't.item_id, IF(t.sold_date>"0000-00-00 00:00:00", "S", "N")';
		$criteria->order = 'item_name';
		$criteria->with = array('items');
		$criteria->together = true;

		$result = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>300, 
			),
		));
		
		return $result;
	}
	
	public function searchPendingLoadingItemList(){
		$criteria = new CDbCriteria;
		
		$criteria->compare('loaded', Individualitem::$LS_PENDING, true, "or");
		$criteria->group = "t.item_id";
		$criteria->with = array('items');
		$criteria->together = true;

		$result = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>300, 
			),
		));
		
		return $result;
	}
	
	public static function getDeniedCount($individual_item_id){
		$individualItemModel = Individualitem::model()->findByPk($individual_item_id);
		$item_id = $individualItemModel->item_id;
		$itemModel = Item::model()->findByPk($item_id);

		$criteria = new CDbCriteria;
		
		$criteria->compare('status', Individualitem::$STATUS_NOT_ORDERED, true, "or");
		$criteria->compare('status', Individualitem::$STATUS_DENIED, true, "or");
		$criteria->compare('item_id', $itemModel->id);
		$count = Individualitem::model()->count($criteria);
		
		return $count;
	}
	
	public static function getLoadingPendingItemsCount(){
		$criteria = new CDbCriteria;
		
		$criteria->compare('loaded', Individualitem::$LS_PENDING, true, "or");
		$count = Individualitem::model()->count($criteria);
		
		return $count;
	}
	
	public static function getLoadingPendingCount($individual_item_id){
		$individualItemModel = Individualitem::model()->findByPk($individual_item_id);
		$item_id = $individualItemModel->item_id;
		$itemModel = Item::model()->findByPk($item_id);

		$criteria = new CDbCriteria;
		
		$criteria->compare('loaded', Individualitem::$LS_PENDING, true, "or");
		$criteria->compare('item_id', $itemModel->id);
		$count = Individualitem::model()->count($criteria);
		
		return $count;
	}
	
	public static function getItemMinList(){
		$strQuery = 'SELECT '
			.'ii.item_id as item_id, '
			.'count(ii.item_id) as min_sold, '
			.'i.item_name as item_code, '
			.'i.item_name as item_name, '
			.'i.color as item_color '
			.'FROM '
			.'individual_item ii '
			.'LEFT JOIN item i ON ii.item_id = i.id '
			.'WHERE '
			.'ii.customer_order_items_id != 0 '
			.'and DATEDIFF(now(), ii.sold_date) < 60 '
			.'GROUP BY '
			.'ii.item_id';
				
			
		$result = Yii::app()->db->createCommand($strQuery)->queryAll();
		
		return $result;
	}
    
    // deletes order info for this item, item is moved to not ordered section if it sold or deleted if it's not
    public function deleteFromOrder(){
        $criteria = new CDbCriteria();
        $criteria->compare('individual_item_id', $this->id);
        IndividualItemSupplierOrderItems::model()->deleteAll($criteria);
        if ($this->customer_order_items_id > 0){
            $this->supplier_order_id = null;
            $this->supplier_order_container_id = null;
            $this->status = Individualitem::$STATUS_NOT_ORDERED;
            return $this->save();
        } else {
            return $this->delete();
        }
    }
    
    public static function addToOrder($orderItemsModel){
        $item = Item::model()->findByPk($orderItemsModel->item_id);
        $individual_item = new Individualitem();
        $individual_item->attributes = $item->attributes;
        $individual_item->item_id = $item->id;
        $individual_item->status = $orderItemsModel->status;
        $individual_item->supplier_order_id = $orderItemsModel->supplier_order_id;
        
        if ($individual_item->save()){
            $individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
            $individualItemSupplierOrderItems->individual_item_id = $individual_item->id;
            $individualItemSupplierOrderItems->supplier_order_items_id = $orderItemsModel->id;
            
            if ($individualItemSupplierOrderItems->save() == false){
                var_dump(CHtml::errorSummary($individualItemSupplierOrderItems));
                exit;
            }
            
            return $individual_item->id;
        } else {
            var_dump(CHtml::errorSummary($individual_item));
            exit;
        }
    }
	
	// item cancellation means detaching it from customer order and creating a new one with "cancelled shipping" status
	// returns new item id
	public function cancelShipping($reason = 0, $chargeback = false, $update_shipment = true){
		if ($this->status == self::$STATUS_CANCELLED_SHIPPING){
			return true;
		}
		
		if ($this->status == self::$STATUS_NOT_ORDERED){
			$this->status = self::$STATUS_CANCELLED_SHIPPING;
			if ($this->save()){
				if ($update_shipment && $this->shipment){
					$this->shipment->updateStatus();
				}
				return $this->id;
			} else {
				return false;
			}
		} else {
			// TODO: check unsafe attributes
			$new_item 								= new Individualitem();
			$new_item->attributes 					= $this->attributes;
			$new_item->status 						= self::$STATUS_CANCELLED_SHIPPING;
			$new_item->shipping_canceled_date 		= date('Y-m-d H:i:s');
			$new_item->shipping_canceled_item_id 	= $this->id;
			$new_item->supplier_order_id 			= 0;
			$new_item->supplier_order_container_id 	= 0;
			$new_item->cancellation_reason 			= $reason;
			if ($chargeback){
				$new_item->chargeback = 1;
			}
			if ($new_item->save()){
				$this->customer_order_items_id 		= 0;
				$this->customer_order_shipment_id 	= 0;
				$this->sold_date 					= '0000-00-00 00:00:00';
				$this->prev_shipment_id 			= 0;
				$this->shipping_total 				= 0;
				$this->is_replacement 				= 0;
				$this->replacement_item 			= 0;
				$this->replacement_boxes 			= '';
				$this->save();
				if ($update_shipment && $this->shipment){
					$this->shipment->updateStatus();
				}
				return $new_item->id;
			} else {
				return false;
			}
		}
	}
	
	public function scanAsOut($update_shipment = true){
		$this->status = self::$STATUS_SOLD;
		$this->scanned_out_date = date('Y-m-d H:i:s');
		if ($this->save()){
			if ($update_shipment && $this->shipment){
				$this->shipment->updateStatus();
			}
			return true;
		} else {
			return false;
		}
	}
	
	public function scanAsIn($condition){
		$this->status = self::$STATUS_IN_STOCK;
		$this->condition = $condition;
		$this->scanned_in_date = date('Y-m-d H:i:s');
		if ($this->save()){
			// TODO: what about shipment updating? Checkk all possible scenarios
			// if ($update_shipment && $this->shipment){
				// $this->shipment->updateStatus();
			// }
			return true;
		} else {
			return false;
		}
	}
	
	public function getShortStatus(){
		switch ($this->getStatus()){
			case self::$STATUS_IN_STOCK: 			return 'in_stock';
			case self::$STATUS_NOT_ORDERED: 		return 'not_ordered';
			case self::$STATUS_ORDERED_BUT_WAITING: return 'ordered_but_waiting';
			case self::$STATUS_ORDERED_WITHOUT_ETL:
			case self::$STATUS_ORDERED_WITH_ETA:
			case self::$STATUS_ORDERED_WITH_ETL:	return 'incoming';
			case self::$STATUS_PENDING_SCAN:		return 'pending_scan';
			case self::$STATUS_SOLD:				return 'scanned_out';
		}
	}
	
	/**
	 * Generates QR codes for all boxes, storing them on the server
	 */
	public function generateQrCodes(){
		// TODO: think about dynamic images
		for ($i=1; $i<=$this->items->box_qty; $i++){
			Yii::import('ext.qrcode.QRCodeGenerator');
			$code = new QRCodeGenerator();
			$code->filePath = "images/qrcodes";
			$code->filename = "item_".$this->id.'_'.$i.".png";
			$code->data = Yii::app()->getBaseUrl(true)."/index.php/individualitem/view/".$this->id.'?box='.$i;
			$code->matrixPointSize = 10;
			$code->subfolderVar = false;
			$code->create();
		}
	}
	
	public function afterSave(){
		if ($this->isNewRecord){
			$this->generateQRCodes();
		}
		
		return parent::afterSave();
	}
	
	public function hasQrCodes(){
		$filename = Yii::getPathOfAlias('webroot')."/images/qrcodes/item_".$this->id.'_1.png';
		return file_exists($filename);
	}
	
	public function getQrCodeUrl($box_i){
		return Yii::app()->getBaseUrl(true).'/images/qrcodes/item_'.$model->id.'_'.$box_i.'.png';
	}
	
	public function getRealCBMForCustomerShipping(){
		if ($this->is_replacement && $this->replacement_boxes){
			$cbm = 0;
			$boxes = explode(',', $this->replacement_boxes);
			foreach ($boxes as $box){
				$cbm += $this->items->getBoxCBM($box);
			}
			
			return $cbm;
		} else {
			return $this->cbm;
		}
	}
	
	/**
	 * we assume items is damaged and do not check it
	 */
	public function getDamagedBoxesCBM(){
		if ($this->damaged_boxes){
			$cbm = 0;
			$boxes = explode(',', $this->damaged_boxes);
			foreach ($boxes as $box){
				$cbm += $this->items->getBoxCBM($box);
			}
			
			return $cbm;
		} else {
			return $this->cbm;
		}
	}
	
	public static function getStatusOrderStatement($t = 't.'){
		return 'FIELD('.$t.'status,
					"'.Individualitem::$STATUS_IN_STOCK.'",
					"'.Individualitem::$STATUS_ORDERED_WITH_ETA.'",
					"'.Individualitem::$STATUS_ORDERED_WITH_ETL.'",
					"'.Individualitem::$STATUS_ORDERED_WITHOUT_ETL.'",
					"'.Individualitem::$STATUS_ORDERED_BUT_WAITING.'",
					"'.Individualitem::$STATUS_NOT_ORDERED.'"), eta';
	}
	
	public function getStatus(){
		return $this->_fake_status ? $this->_fake_status : $this->status;
	}
	
	public function getEta(){
		return $this->_fake_eta ? $this->_fake_eta : $this->eta;		
	}
	
	/**
	 * $items should be sold together with the same order
	 * @param array $items
	 */
	public static function setItemsFakeStatus(array $items){
		$rows = array();
		$shipment_id = $items[0]->customer_order_shipment_id;
		foreach ($items as $i => $item) {
			if ($item->status != Individualitem::$STATUS_CANCELLED_SHIPPING
					&& $item->status != Individualitem::$STATUS_SCANNED_OUT
					&& $item->shipping_started == 0){
				$rows[] = $item;
				$shipment_id = min($shipment_id, $item->customer_order_shipment_id);
			}
		}

		if (!count($rows)){
			return;
		}
		
		$criteria = new CDbCriteria();
		$criteria->compare('item_id', $rows[0]->item_id);
		$criteria->addCondition('customer_order_items_id > 0');
		$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"
				AND shipping_started = 0');
		$date = date('Y-m-d H:i:s', strtotime($rows[0]->sold_date));
		$criteria->addCondition('sold_date < "'.$date.'"
						OR (sold_date = "'.$date.'" AND customer_order_items_id < '.$rows[0]->customer_order_items_id.')
						OR (customer_order_items_id = '.$rows[0]->customer_order_items_id.'	AND customer_order_shipment_id < '.$shipment_id.')');
		$offset = Individualitem::model()->count($criteria);
		
		$criteria = new CDbCriteria();
		$criteria->order = self::getStatusOrderStatement();
		
		$criteria->limit = count($rows);
		$criteria->offset = $offset;
		$criteria->compare('item_id', $rows[0]->item_id);
		$criteria->addCondition('status != "'.Individualitem::$STATUS_CANCELLED_SHIPPING.'" 
				AND status != "'.Individualitem::$STATUS_SCANNED_OUT.'"
				AND shipping_started = 0');
		$s_items = Individualitem::model()->findAll($criteria);
		foreach ($rows as $i=>$item){
			$item->_fake_status	= $s_items[$i]->status;
			$item->_fake_eta 	= $s_items[$i]->eta;
		}
	}
	
	/**
	 * swaps the fields that refer to customer order 
	 * items are not getting saved
	 * @param Individualitem $item_1
	 * @param Individualitem $item_2
	 */
	public static function swapCustomerOrderInfo($item_1, $item_2){
		$attr_1['customer_order_items_id'] 		= $item_1->customer_order_items_id;
		$attr_1['customer_order_shipment_id'] 	= $item_1->customer_order_shipment_id;
		$attr_1['sale_price'] 					= $item_1->sale_price;
		$attr_1['shipping_price']				= $item_1->shipping_price;
		$attr_1['local_pickup']					= $item_1->local_pickup;
		$attr_1['la_oc_shipping']				= $item_1->la_oc_shipping;
		$attr_1['canada_shipping']				= $item_1->canada_shipping;
		$attr_1['prev_shipment_id']				= $item_1->prev_shipment_id;
		$attr_1['sold_date']					= $item_1->sold_date;
		$attr_1['shipping_total']				= $item_1->shipping_total;
		$attr_1['is_replacement']				= $item_1->is_replacement;
		$attr_1['replacement_item']				= $item_1->replacement_item;
		$attr_1['replacement_boxes']			= $item_1->replacement_boxes;
		
		$attr_2['customer_order_items_id'] 		= $item_2->customer_order_items_id;
		$attr_2['customer_order_shipment_id'] 	= $item_2->customer_order_shipment_id;
		$attr_2['sale_price'] 					= $item_2->sale_price;
		$attr_2['shipping_price']				= $item_2->shipping_price;
		$attr_2['local_pickup']					= $item_2->local_pickup;
		$attr_2['la_oc_shipping']				= $item_2->la_oc_shipping;
		$attr_2['canada_shipping']				= $item_2->canada_shipping;
		$attr_2['prev_shipment_id']				= $item_2->prev_shipment_id;
		$attr_2['sold_date']					= $item_2->sold_date;
		$attr_2['shipping_total']				= $item_2->shipping_total;
		$attr_2['is_replacement']				= $item_2->is_replacement;
		$attr_2['replacement_item']				= $item_2->replacement_item;
		$attr_2['replacement_boxes']			= $item_2->replacement_boxes;
		
		$item_1->setAttributes($attr_2, false);
		$item_2->setAttributes($attr_1, false);
		return true;
	}

	/**
	 * @return array Array of damaged boxes indexes
	 */
	public function getDamagedBoxes(){
		if ($this->damaged_boxes){
			return explode(',', $this->damaged_boxes);
		}
	
		if ($this->damaged){
			$arr = array();
			for ($i=1; $i<=$this->items->box_qty; $i++){
				$arr[] = $i;
			}
				
			return $arr;
		}
	
		return array();
	}

    /**
     * Shipping cost for product i from Chinese port to US port
     * @return float
     */
    public function getSupplyShippingCost()
    {
        if (isset($this->supplier_order_container_id, $this->supplier_order_id)) {
            //get container volume
            $totalCbm = SupplierOrder::getSupplierOrderTotalCBM($this->supplier_order_id);
            if (!$totalCbm) return 0;
            $supplierOrderContainer = SupplierOrderContainer::model()->findByPk($this->supplier_order_container_id);
            if (isset($supplierOrderContainer)) {
            //get container total FOB price
                $containerBid = SupplierOrderContainerBidding::model()->findByAttributes(array(
                    'supplier_order_container_id' => $this->supplier_order_container_id,
                    'supplier_shipper_id' => $supplierOrderContainer->supplier_shipper_id,
                ));
                if (isset($containerBid)) {
                    $totalFOB = SupplierOrderContainerBidding::getBiddingOptionsTotalFOBPrice($containerBid->id);
                    $costPerCbm = $totalFOB / $totalCbm;
                }
            }
        }
        if (!isset($costPerCbm)) $costPerCbm = self::DEFAULT_SUPPLY_COST_PER_CBM;
        return $costPerCbm * $this->cbm;
    }

    /**
     * Holding cost for product in warehouse
     * From time that product scanned into our warehouse, to shipping out to the customer
     * @return float
     */
    public function getHoldingCostPerMonth()
    {
        $holdingCost = self::COST_OF_CAPITAL_OPPORTUNITY *
            ($this->fob_cost_price + $this->supplyShippingCost) +
            $this->dimensions_length * $this->dimensions_width *
            self::WAREHOUSE_RENTAL_PER_MONTH / (self::WAREHOUSE_AREA * self::WAREHOUSE_PRODUCT_LEVELS);
        return $holdingCost;
    }

    /**
     * Get item holding in warehouse period
     * Return false if item is not scanned in and out yet
     * @return int days item hold
     */
    public function getHoldingPeriod()
    {
        if (empty($this->scanned_in_date) or $this->scanned_in_date =='0000-00-00 00:00:00')
            return 1; //return 1 month
        if (empty($this->scanned_out_date) or $this->scanned_out_date =='0000-00-00 00:00:00') {
            //$scannedOutDate = $this->shipment->pickup_date;
            $scannedOutDate = date('Y-m-d H:i:s',
                strtotime($this->sold_date) + 60 * 60 * 24 * 7); //+7 days
        } else $scannedOutDate = $this->scanned_out_date;
        //var_dump($this->scanned_in_date, $scannedOutDate);
        $dateScannedIn = strtotime($this->scanned_in_date);
        $dateScannedOut = strtotime($scannedOutDate);
        return ($dateScannedOut - $dateScannedIn)/(60 * 60 * 24 * 30);
    }

    /**
     * @param int|null $supplierId
     * @return Individualitem[]
     */
    public static function getPendingLoadingItems($supplierId = null)
    {
        $withCondition = isset($supplierId) ? array(
            'supplier_order' => array(
                'select' => false,
                'joinType' => 'INNER JOIN',
                'condition' => 'supplier_order.supplier_id = :supplierId',
                'params' => array(':supplierId' => $supplierId),
            ),
        ) : array();
        return self::model()->with($withCondition)->findAllByAttributes(array(
                'loaded' => self::$LS_PENDING
        ));
    }
}
