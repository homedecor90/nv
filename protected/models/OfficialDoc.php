<?php

/**
 * This is the model class for table "official_doc".
 *
 * The followings are the available columns in table 'official_doc':
 * @property string $id
 * @property string $type
 * @property integer $item_id
 * @property string $path
 * @property string $date
 */
class OfficialDoc extends CActiveRecord
{
	
	public static $TYPE_CLAIM = "Claim"; 
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OfficialDoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'official_doc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, path, date', 'required'),
			array('item_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>20),
			array('path', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, item_id, path, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'shipment'=>array(
				self::BELONGS_TO, 
				'CustomerOrderShipment', 
				'item_id'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'item_id' => 'Item',
			'path' => 'Path',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeDelete(){
		parent::beforeDelete();
		if (!@unlink(Yii::getPathOfAlias('webroot') .'/'.$this->path)){
			return false;
		}
		return true;
	}
	
	public function getFilename(){
		return pathinfo($this->path, PATHINFO_BASENAME);
	}
	
	public function download(){
		$path = Yii::getPathOfAlias('webroot').'/'.$this->path;
		$ext = pathinfo($this->path, PATHINFO_EXTENSION);
		
		switch (strtolower($ext)){
			case 'pdf':
				header('Content-Type: application/pdf');
				break;
			case 'jpg':
			case 'jpeg':
				header('Content-Type: image/jpeg');
				break;
			case 'png':
				header('Content-Type: image/png');
				break;
			case 'zip':
				header('Content-Type: application/zip');
				break;
			case 'rar':
				header('Content-Type: application/x-rar-compressed');
				break;

			case 'doc':
				header('Content-Type: application/msword');
				break;
			case 'odt':
				header('Content-Type: application/vnd.oasis.opendocument.text');
				break;
		}
		
		flush();
		readfile($path);
	}
}