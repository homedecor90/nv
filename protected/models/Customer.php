<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property string $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $heard_through
 */
class Customer extends CActiveRecord
{
	var $last_folloup_time = false;
	const NOTIFICATION_EMAIL = 'support@regencyshop.com';
	
	public static $authorize_api_login_id = '6hVTd8QYd56D';
	public static $authorize_transaction_key = '2pw94FN395X9vW9w';

    private $_fullname;
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, heard_through', 'required'),
			array('first_name, last_name, phone, email, heard_through', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, first_name, last_name, phone, email, heard_through', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'last_order'=>array(self::HAS_ONE, 'CustomerOrder', 'customer_id', 'order'=>'last_order.id DESC'),
			'total_orders'=>array(self::STAT, 'CustomerOrder', 'customer_id'),
			'sold_orders'=>array(self::HAS_MANY, 'CustomerOrder', 'customer_id', 'condition'=>'status="'.CustomerOrder::$STATUS_SOLD.'"'),
			'orders'=>array(self::HAS_MANY, 'CustomerOrder', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'phone' => 'Phone',
			'email' => 'Email',
			'heard_through' => 'Heard Through',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 * $order_status - Last Order filter in /customer/admin
	 */
	public function search($order_status = '')
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		if(!empty($order_status)){
			if ($order_status == 'no_order'){
				$criteria->select = 't.*, "" as last_order_status';
				$criteria->addCondition('ISNULL((
				SELECT co.status AS
				STATUS FROM customer_order co
				WHERE co.customer_id = t.id
				ORDER BY co.id DESC
				LIMIT 1
				))');
			} else {
				$criteria->select = 't.*, "'.$order_status.'" as last_order_status';
				$criteria->addCondition('(
					SELECT co.status AS
					STATUS FROM customer_order co
					WHERE co.customer_id = t.id
					ORDER BY co.id DESC
					LIMIT 1
				) = :sold');
				$criteria->params = array(':sold'=>$order_status);
			}
		}
		
		$criteria->compare('t.id',$this->id,false);
		$criteria->compare('t.first_name',$this->first_name,true);
		$criteria->compare('t.last_name',$this->last_name,true);
		$criteria->compare('t.phone',$this->phone,true);
		$criteria->compare('t.email',$this->email,true);
		$criteria->compare('t.heard_through',$this->heard_through,true);
	        $criteria->order = 't.id desc';        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>100, 
			),
		));
	}
	
	public static function getCustomerList(){
		$customerList = Customer::model()->findAll();
		$customerIDList = array();
		
		foreach ($customerList as $customer){
			$customer_id = $customer->id;
			$customer_name = $customer->first_name." ".$customer->last_name;
			
			$customerIDList[$customer_id] = $customer_name;
		}
		
		return $customerIDList;
	}
	
	public function checkNameDuplicated(){
		$criteria=new CDbCriteria;
		$criteria->compare('first_name',$this->first_name,false);
		$criteria->compare('last_name',$this->last_name,false);
		$duplicatedCustomerCount = Customer::model()->count($criteria);
		
		if ($duplicatedCustomerCount > 0){
			return true;
		} else {
			return false;
		}
	}
	
	public function getDuplicatedCustomerId(){
		$criteria=new CDbCriteria;
		$criteria->compare('first_name',$this->first_name,false);
		$criteria->compare('last_name',$this->last_name,false);
		$duplicatedCustomerModel = Customer::model()->find($criteria);
		
		if ($duplicatedCustomerModel == NULL){
			return 0;
		} else {
			return $duplicatedCustomerModel->id;
		}
	}
    
    public function getLastOrder(){
        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->limit = 1;
        $criteria->compare('customer_id', $this->id);
        
        return CustomerOrder::model()->find($criteria);
    }
    
    public function getLastFollowupTime(){
        if ($this->last_folloup_time === false){
            $criteria = new CDbCriteria();
            $criteria->order = 'id desc';
            $criteria->limit = 1;
            $criteria->compare('customer_id', $this->id);
            $customerFollowup = CustomerFollowup::model()->find($criteria);
            $this->last_folloup_time = isset($customerFollowup) ?
                strtotime($customerFollowup->followup_time)
                : null;
        }
        
        return $this->last_folloup_time;
    }
	
	public function addNote($content, $model = NULL, $attributes = null){
		$note = new CustomerNote();
		$note->customer_id = $this->id;
		$note->content = $content;
		$note->sales_person_id = Yii::app()->user->id;
		$note->created_on = date("Y-m-d H:i:s");
		if ($model){
			if (is_a($model, 'CustomerOrder')){
				$note->customer_order_id = $model->id;
			} else if (is_a($model, 'CustomerOrderShipment')){
				$note->customer_order_shipment_id = $model->id;
			} else if (is_a($model, 'Individualitem')){
				$note->individual_item_id = $model->id;
			}
		} else if ($attributes){
			$note->attributes = $attributes;
		}
		return $note->save();
	}

	public function searchShipmentsForNotes(){
		$order_ids = array();
		foreach ($this->sold_orders as $order){
			// check if customer info need to be verified first
//			if (!$order->verificationRequired()){
				$order_ids[] = $order->id;
//			}
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.customer_order_id', $order_ids);
		$criteria->addCondition('status != "'.CustomerOrderShipment::$STATUS_ATTACHED.'"');
		$criteria->order = 't.customer_order_id desc';
		return new CActiveDataProvider(new CustomerOrderShipment(), array(
				'criteria'=>$criteria,
				'pagination'=>array(
                    'pageSize'=>30,
				),
		));
	}
	
	public static function getNewsletterUnsubscribeCode($email){
		$salt = '4DC5tZSB';
		return md5($email.$salt.$salt);
	}
	
	public function beforeSave(){
		if ($this->isNewRecord){
			$criteria = new CDbCriteria();
			$criteria->compare('email', $this->email);
			if (self::model()->count($criteria)){
				$this->addError('email', 'Customer with this email already exists');
				return false;
			}
		}
		return parent::beforeSave();
	}

    public function getFullname()
    {
        if (!isset($this->_fullname)) {
            $this->_fullname = $this->first_name . ' ' . $this->last_name;
        }
        return $this->_fullname;
    }
}
