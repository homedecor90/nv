<?php

/**
 * This is the model class for table "customer_newsletter".
 *
 * The followings are the available columns in table 'customer_newsletter':
 * @property integer $id
 * @property string $date_added
 * @property integer $completed
 * @property string $date_completed
 * @property integer $last_customer_id
 */
class CustomerNewsletter extends CActiveRecord
{
    private $_unsubscribed;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerNewsletter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_newsletter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_added, last_customer_id', 'required'),
			array('completed, last_customer_id, subscribed', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date_added, completed, date_completed, last_customer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'letters' => array(self::HAS_MANY, 'CustomerNewsletterLetter', 'newsletter_id'),
            'sent' => array(self::STAT, 'CustomerNewsletterLetter', 'newsletter_id', 'condition' => 'sent=1'),
            'pending' => array(self::STAT, 'CustomerNewsletterLetter', 'newsletter_id', 'condition' => 'sent=0'),
		);
	}



	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_added' => 'Date Added',
			'completed' => 'Completed',
			'date_completed' => 'Date Completed',
			'last_customer_id' => 'Last Customer',
		);
	}

    public function defaultScope()
    {
        return array(
            'order' => 'date_added DESC'
        );
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('completed',$this->completed);
		$criteria->compare('date_completed',$this->date_completed,true);
		$criteria->compare('last_customer_id',$this->last_customer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getUnsubscribed()
    {
        if (!isset($this->_unsubscribed)) {
            $prevNewsletter = self::model()->find(array(
                'condition' => 'completed = 1',
                'order' => 'date_completed DESC'
            ));
            if (isset($prevNewsletter)) {
                $this->_unsubscribed = CustomerUnsubscribe::model()->count(array(
                    'condition' => 't.date > :prevNewsletterDate',
                    'params' => array(':prevNewsletterDate' => $prevNewsletter->date_completed),
                ));
            } else $this->_unsubscribed = CustomerUnsubscribe::model()->count();
        }
        return $this->_unsubscribed;
    }
}