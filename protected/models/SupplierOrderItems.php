<?php

/**
 * This is the model class for table "supplier_order_items".
 *
 * The followings are the available columns in table 'supplier_order_items':
 * @property string $id
 * @property string $supplier_order_id
 * @property string $item_id
 * @property string $quantity
 * @property double $exw_cost_price
 * @property double $fob_cost_price
 * @property double $sale_price
 * @property string $added_date
 * @property string $status
 * @property string $processed_date
 * @property string $operator_id
 * @property string $notes
 */
class SupplierOrderItems extends CActiveRecord
{
	public $item_code;
	public $color;
	
	public static $STATUS_ACCEPTED 	= "Accepted";
	public static $STATUS_PENDING	= "Pending";
	public static $STATUS_DENIED	= "Denied";
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierOrderItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_order_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_order_id, item_id, quantity, exw_cost_price, fob_cost_price, sale_price, added_date, status, operator_id', 'required'),
			array('exw_cost_price, fob_cost_price, sale_price, initial_order_id', 'numerical'),
			array('supplier_order_id, item_id, quantity, operator_id', 'length', 'max'=>10),
			array('custom_color', 'length', 'max'=>30),
			array('status', 'length', 'max'=>45),
			array('not_loaded_reordered', 'boolean'),
			array('processed_date', 'safe'),
			array('notes', 'length', 'max' => 255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, supplier_order_id, item_id, quantity, exw_cost_price, fob_cost_price, sale_price, added_date, status, processed_date, operator_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items'=>array(self::BELONGS_TO, 'Item', 'item_id'),
            'order'=>array(self::BELONGS_TO, 'SupplierOrder', 'supplier_order_id'),
			'ind_items'=>array(self::MANY_MANY, 'Individualitem', 'individual_item_supplier_order_items(supplier_order_items_id, individual_item_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'supplier_order_id' => 'Supplier Order',
			'item_id' => 'Item',
			'quantity' => 'Quantity',
			'exw_cost_price' => 'EXW Cost Price',
			'fob_cost_price' => 'FOB Cost Price',
			'sale_price' => 'Sale Price',
			'added_date' => 'Added Date',
			'status' => 'Status',
			'processed_date' => 'Processed Date',
			'operator_id' => 'Operator',
            'custom_color' => 'Custom Color',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($supplier_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array(
			"items",			
		);
		
        $criteria->together = true;
        $criteria->order = 'items.item_name';
        
		$criteria->compare('supplier_order_id',$supplier_id,false);
		if ($this->item_code != NULL){
			$criteria->compare('items.item_code', $this->item_code, true);
		}
		
		if ($this->color != NULL){
			$criteria->compare('items.color', $this->color, true);
		}

		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_order_id',$this->supplier_order_id,true);
		$criteria->compare('item_id',$this->item_id,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('exw_cost_price',$this->exw_cost_price);
		$criteria->compare('fob_cost_price',$this->fob_cost_price);
		$criteria->compare('sale_price',$this->sale_price);
		$criteria->compare('added_date',$this->added_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('processed_date',$this->processed_date,true);
		$criteria->compare('operator_id',$this->operator_id,true);
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchNotDenied($supplier_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
	
		$criteria=new CDbCriteria;
	
		$criteria->with = array(
				"items",
		);
	
		$criteria->together = true;
		$criteria->order = 'items.item_name';
	
		$criteria->compare('supplier_order_id',$supplier_id,false);
		if ($this->item_code != NULL){
			$criteria->compare('items.item_code', $this->item_code, true);
		}
	
		if ($this->color != NULL){
			$criteria->compare('items.color', $this->color, true);
		}
	
		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplier_order_id',$this->supplier_order_id,true);
		$criteria->compare('item_id',$this->item_id,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('exw_cost_price',$this->exw_cost_price);
		$criteria->compare('fob_cost_price',$this->fob_cost_price);
		$criteria->compare('sale_price',$this->sale_price);
		$criteria->compare('added_date',$this->added_date,true);
// 		$criteria->compare('status',$this->status,true);
		$criteria->compare('processed_date',$this->processed_date,true);
		$criteria->compare('operator_id',$this->operator_id,true);
	
		$criteria->addCondition('t.status != "'.self::$STATUS_DENIED.'"');
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}
	
	public function acceptAddedItem(){
		$supplierOrder = SupplierOrder::model()->findByPk($this->supplier_order_id);
	
		$criteriaOrderItems = new CDbCriteria;
		$criteriaOrderItems->compare('supplier_order_items_id',$this->id);
		
		$status = Individualitem::$STATUS_ORDERED_WITH_ETL;
		if (($supplierOrder->status == SupplierOrder::$STATUS_ACCEPTED)
				|| ($supplierOrder->status == SupplierOrder::$STATUS_ADVANCED_PAID)){
			$status = Individualitem::$STATUS_ORDERED_WITHOUT_ETL;
		}
		
		if ($eta = $supplierOrder->getETA()){
			$status = Individualitem::$STATUS_ORDERED_WITH_ETA;
		}
		
		/*$attributeItem = array(
			'status'=>$status,
			'supplier_id'=>$supplierOrder->id,
		);
		
		//IndividualItemSupplierOrderItems::model()->updateAll($attributeItem, $criteriaOrderItems);*/
		
		
		$individual_item_supplier_order_items = IndividualItemSupplierOrderItems::model()->findAll($criteriaOrderItems);
		foreach ($individual_item_supplier_order_items as $ind_item){
			$ind_item_info = Individualitem::model()->findByPk($ind_item->individual_item_id);
			$ind_item_info->status = $status;
			$ind_item_info->supplier_order_id = $supplierOrder->id;
			if ($eta){
				$ind_item_info->eta = $eta;
				$ind_item_info->supplier_order_container_id = $supplierOrder->container_id;
				$ind_item_info->cutoff_date = $supplierOrder->supplier_order_container->getWinningBid()->cutoff_date;
				
			}
			$ind_item_info->save();
		}
	}
	
	public function denyAddedItem(){
		$supplierOrder = SupplierOrder::model()->findByPk($this->supplier_order_id);
	
		$criteriaOrderItems = new CDbCriteria;
		$criteriaOrderItems->compare('supplier_order_items_id',$this->id);
		
		$individual_item_supplier_order_items = IndividualItemSupplierOrderItems::model()->findAll($criteriaOrderItems);
		foreach ($individual_item_supplier_order_items as $ind_item){
			$ind_item_info = Individualitem::model()->findByPk($ind_item->individual_item_id);
			$ind_item_info->status = Individualitem::$STATUS_NOT_ORDERED;
			
			$ind_item_info->save();
		}
	}
    
    public function countSoldItems(){
    	$ind_item = $this->ind_items[0];
        
    	// get offset: how many items with the same status we have higher in the queue then current ones
        $criteria = new CDbCriteria();
        $criteria->compare('status', $ind_item->status);
        $criteria->compare('item_id', $this->item_id);
    	$criteria->order = 'eta, supplier_order_id, id';
    	$ind_items = Individualitem::model()->findAll($criteria);
    	$offset = 0;
    	foreach ($ind_items as $i=>$row){
    		if ($row->supplier_order_id == $ind_item->supplier_order_id){
    			$offset = $i;
    			break;
    		}
    	}
    	$item = $this->items;
    	
    	$count_sold = min($item->countItemsInventory($ind_item->status, true) - $offset, $this->quantity);

//         $criteria_ind_items = new CDbCriteria();
//         $criteria_ind_items->addInCondition('id', $ind_items_ids);
//         $criteria_ind_items->compare('customer_order_items_id', '>0');
//         $count_sold_items = Individualitem::model()->count($criteria_ind_items);

        return max($count_sold, 0);
    }
    
    public function deleteFromOrder(){
        $sql = "select individual_item_id from individual_item_supplier_order_items where supplier_order_items_id = ".$this->id;
        // get all item id's to be deleted
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $item_ids = array();
        // check how many of them are sold
        foreach ($result as $row){
            $item_ids[] = $row['individual_item_id'];
        }
        
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $item_ids);
        $items = Individualitem::model()->findAll($criteria);
        foreach($items as $item){
            $item->deleteFromOrder();
        }
        
        return $this->delete();
    }
    
    public function updateQty($new_value){
        $old_value = $this->quantity;
    	if ($new_value < $old_value){
            // need to delete some
            $to_delete_count = $old_value - $new_value;
            $sql = "select individual_item_id from individual_item_supplier_order_items where supplier_order_items_id = ".$this->id;
            // get all item id's to be deleted
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $item_ids = array();
            // check how many of them are sold
            foreach ($result as $row){
                $item_ids[] = $row['individual_item_id'];
            }
            
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $item_ids);
            $criteria->compare('customer_order_items_id', 0);
            // here we have array of individual items that are not sold
            $items = Individualitem::model()->findAll($criteria);

            // enough items - delete
            $model = IndividualItemSupplierOrderItems::model();
            
            $i = 0;
            while ($to_delete_count && $i<count($items)){
                $items[$i]->deleteFromOrder(); 
                $to_delete_count--;
                $i++;
            }
            
            if ($to_delete_count){
                // this means that there are not enough free items and we have to delete some sold
                $criteria = new CDbCriteria();
                $criteria->addInCondition('id', $item_ids);
                $criteria->compare('customer_order_items_id', '<>0');
                // here we have array of individual items that are not sold
                $items = Individualitem::model()->findAll($criteria);
                while ($to_delete_count){
                    $items[$i]->deleteFromOrder();
                    $to_delete_count--;
                    $i++;
                }
            }
            
            $this->quantity = $new_value;
            return $this->save();
        } else {
            // create new items
            $to_create_count = $new_value - $old_value;
            // just a little copy-paste from order model
            // $item = Item::model()->findByPk($orderItemsModel->item_id);
            while ($to_create_count > 0){
                Individualitem::addToOrder($this);
                $to_create_count--;
            }
            $this->quantity = $new_value;
            return $this->save();
        }
    }
	
	public function getLoadedQty(){
		$loaded = 0;
		foreach ($this->ind_items as $item){
			if ($item->loaded == Individualitem::$LS_LOADED){
				$loaded++;
			}
		}
		return $loaded;
	}
	
	public function getTotalCBM(){
		if ($this->is_replacement){
			$cbm = 0;
			foreach ($this->ind_items as $item){
				$cbm += $item->supplier_replacement_cbm;
			}
			return $cbm;
		} else {
			return floatval($this->items->cbm) * intval($this->quantity);
		}
	}
	
	/**
	 * Creates individual items and links them to the items model
	 * Used for regular order when no corrections needed (unlike pending loading or replacement items)
	 */
	public function createIndividualItems(){
		for ($i=0; $i < $this->quantity; $i++){
			$individual_item = new Individualitem();
			$individual_item->attributes = $this->items->attributes;
			$individual_item->item_id = $this->items->id;
			$individual_item->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;
		
			if ($individual_item->save()){
				$individualItemSupplierOrderItems = new IndividualItemSupplierOrderItems();
				$individualItemSupplierOrderItems->individual_item_id = $individual_item->id;
				$individualItemSupplierOrderItems->supplier_order_items_id = $this->id;
					
				if ($individualItemSupplierOrderItems->save() == false){
					var_dump(CHtml::errorSummary($individualItemSupplierOrderItems));
					exit;
				}
			} else {
				var_dump(CHtml::errorSummary($individual_item));
				exit;
			}
		}
	}
	
	/**
	 * Adds not ordered items to the row updating quantity and custom color field if needed
	 */
	public function addNotOrderedItems(Item $item_model){
		$criteria = new CDbCriteria;
		$criteria->compare('t.status', Individualitem::$STATUS_NOT_ORDERED);
		$criteria->addCondition('customer_order_items_id > 0');
		$criteria->compare('t.item_id', $this->item_id);
		$criteria->with = 'customer_order_items';
		$criteria->limit = $item_model->countItemsInventory(Individualitem::$STATUS_NOT_ORDERED, true);
		$criteria->together = true;
		
		$items = Individualitem::model()->findAll($criteria);
		if (!count($items)){
			return true;
		}
		
		foreach ($items as $item){
			$item->supplier_order_id = $supplierOrder->id;
			$item->status = Individualitem::$STATUS_ORDERED_BUT_WAITING;
			$item->exw_cost_price = $item_model->exw_cost_price;
			$item->fob_cost_price = $item_model->fob_cost_price;
			$item->save();
			
			$newIndividualItemSupplierOrderItemsModel = new IndividualItemSupplierOrderItems();
			$newIndividualItemSupplierOrderItemsModel->attributes = array(
					'individual_item_id' => $item->id,
					'supplier_order_items_id' => $this->id,
			);
			
			$newIndividualItemSupplierOrderItemsModel->save();
		}
		
		// get custom colors
		$items = $item_model->getSoldItemsByShortStatus('sold_not_ordered');
		$colors = array();
		
		foreach ($items as $item){
			if ($item->customer_order_items->custom_color){
				$colors[$item->customer_order_items->custom_color]++;
			}
		}
		
		$colors2 = array();
		foreach ($colors as $color=>$count){
			$colors2[] = $count.'x"'.$color.'"';
		}
		
		$custom_color = implode(', ', $colors2);
		
		$this->quantity += count($items);
		$this->custom_color .= ($this->custom_color ? ',' : '').$custom_color;
		return $this->save();
	}

	/**
	 * Tries to find and combine row with existing one with same prices and status
	 * $this row is deleted if combines 
	 * @return row id that combined with
	 */
	public function combine(){
		$this->refresh();
        $criteria = new CDbCriteria();
		$criteria->compare('id', '<>'.$this->id);
		$criteria->compare('item_id', $this->item_id);
		$criteria->compare('supplier_order_id', $this->supplier_order_id);
		$criteria->compare('exw_cost_price', $this->exw_cost_price);
		$criteria->compare('fob_cost_price', $this->fob_cost_price);
		$criteria->compare('sale_price', $this->sale_price);
		$criteria->compare('status', $this->status);
		$criteria->compare('custom_color', $this->custom_color);
		$criteria->compare('is_replacement', $this->is_replacement);
		$criteria->compare('not_loaded_reordered', $this->not_loaded_reordered);
		$criteria->compare('quantity_updated', 0);
		if ($row = SupplierOrderItems::model()->find($criteria)){
			// update quantity
			$row->quantity += $this->quantity;
			$row->processed_date = $this->processed_date;
			if ($row->save()){
				// take care of individual items
				$criteria = new CDbCriteria();
				$criteria->compare('supplier_order_items_id', $this->id);
				IndividualItemSupplierOrderItems::model()->updateAll(array('supplier_order_items_id'=>$row->id), $criteria);
				$this->delete();
				return $row->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

    /**
     * Moves items to back to initial order and mark them as Scanned IN.
     * Used for items that was not loaded first and marked as not_loaded_reordered
     * @param int $quantity
     * @throws CDbException
     */
    public function moveToInitialOrder($quantity)
    {
        $individualItems = Individualitem::model()->findAllByAttributes(array(
            'supplier_order_id' => $this->supplier_order_id,
            'item_id' => $this->item_id,
            'status' => Individualitem::$STATUS_ORDERED_BUT_WAITING,
            'loaded' => null,
        ));
        $movedCount = 0;
        if (empty($this->initial_order_id))
            throw new CDbException('Initial order not found');
        //get corresponding individual items and scan them in
        for ($i = 0; $i < $quantity; $i++) {
            if (!isset($individualItems[$i])) break;
            //scan item as in and move back to previous order
            $individualItems[$i]->saveAttributes(array(
                'supplier_order_id' => $this->initial_order_id,
                'loaded' => Individualitem::$LS_LOADED,
                'status'=>Individualitem::$STATUS_IN_STOCK,
                'scanned_in_date' => date('Y-m-d H:i:s'),
            ));
            $movedCount++;
            //get previous items id
            $supplierOrderItems = self::model()->findByAttributes(array(
                'supplier_order_id' => $this->initial_order_id,
                'item_id' => $individualItems[$i]->item_id,
            ));
            //update pivot table
            if (isset($supplierOrderItems)) {
                IndividualItemSupplierOrderItems::model()->updateAll(array(
                    'supplier_order_items_id' => $supplierOrderItems->id,
                ), 'individual_item_id = :individualItemId', array(
                    ':individualItemId' => $individualItems[$i]->id,
                ));
            }
        }
        //set new quantity for items here or delete that completely
        $this->quantity = $this->quantity - $quantity;
        if ($this->quantity > 0) $this->save(true, array('quantity'));
        else $this->delete();
        return $movedCount;
    }
}