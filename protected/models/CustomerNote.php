<?php

/**
 * This is the model class for table "customer_note".
 *
 * The followings are the available columns in table 'customer_note':
 * @property string $id
 * @property string $sales_person_id
 * @property string $customer_id
 * @property string $content
 * @property string $created_on
 */
class CustomerNote extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerNote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_note';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sales_person_id, customer_id, content, created_on', 'required'),
			array('sales_person_id, customer_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sales_person_id, customer_id, content, created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sales_person_id' => 'Sales Person',
			'customer_id' => 'Customer',
			'content' => 'Content',
			'created_on' => 'Created On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($customer_id = 0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sales_person_id',$this->sales_person_id);
		
		if ($customer_id != 0){
			$this->customer_id = $customer_id;
		}
		
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('content',$this->content);
		$criteria->compare('created_on',$this->created_on);

		$criteria->order = 'created_on DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getContentWithAttachments(){
		return $this->content;
		
		$content = $this->content;
		if ($this->customer_order_id){
			// add order info to the content
			$content .= '<div class="customer_note_attachment">';
			$content .= $this->getAttachedOrderInfo();
			$content .= '</div>';
		} else if ($this->customer_order_shipment_id){
			// add order info to the content
			$content .= '<div class="customer_note_attachment">';
			$content .= $this->getAttachedShipmentInfo();
			$content .= '</div>';
		}
		return $content;
	}
	
	private function getAttachedOrderInfo(){
		$order = CustomerOrder::model()->findByPk($this->customer_order_id);
		
		$actions = array();
		if ($order->shipping_status == CustomerOrder::$SHIPPING_STATUS_PENDING){
			$actions = array(
				array(
						'value'=>'Cancel',
						'class'=>'cancel_order',
				),
				array(
						'value'=>'Chargeback',
						'class'=>'chargeback_order',
				),
// 				array(
// 						'value'=>'Send For Bidding',
// 						'class'=>'send_for_bidding_order',
// 				),
			);
			
			if ($order->verificationRequired()){
				$actions[] = array('value'=>'Verification Request', 'class'=>'order_verification_request');
				$actions[] = array('value'=>'Verified', 'class'=>'order_verify');
			}
		}
		
		$content  = '<strong>Order Shipping Status: </strong>'.$order->shipping_status;
		$content .= '<br /><strong>Order contents</strong>:<br />'.$order->getOrderContentsStr().'<br />';
		if (count($actions)){
			$content .= '<br /><strong>Actions: </strong>';
			foreach ($actions as $action){
				$content .= '<input type="button" rel="'.$order->id.'" value="'.$action['value'].'" class="'.$action['class'].'" />';
			}
		}
		
		return $content;
	}
	
	private function getAttachedShipmentInfo(){
		$shipment = CustomerOrderShipment::model()->findByPk($this->customer_order_shipment_id);
		
		$actions = array();
		$items = nl2br($shipment->getItemsListForEmail());
		if ($shipment->status == CustomerOrderShipment::$STATUS_PENDING){
			$actions = array(
// 					array(
// 							'value'=>'Send For Bidding',
// 							'class'=>'send_for_bidding_shipment',
// 					),
// 					array(
// 							'value'=>'Scan Out',
// 							'class'=>'scan_out_shipment',
// 					)
			);
			
		}
		if ($shipment->status == CustomerOrderShipment::$STATUS_COMPLETED){
			$actions = array(
// 					array(
// 							'value'=>'Mark As Damaged',
// 							'class'=>'mark_as_damaged_shipment',
// 					),
			);
			$items = nl2br($shipment->getItemsListForEmail(true));
		}
		
		$content  = '<strong>Status: </strong>'.$shipment->status;
		$content .= '<br /><strong>Shipment contents</strong>:<br />'.$items.'<br />';
		if (count($actions)){
			$content .= '<strong>Actions: </strong>';
			foreach ($actions as $action){
				$content .= '<input type="button" rel="'.$shipment->id.'" value="'.$action['value'].'" class="'.$action['class'].'" />';
			}
		}
		
		return $content;
	}
	
	private function getAttachedIndividualItemInfo(){
	
	}
}