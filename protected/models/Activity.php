<?php

/**
 * This is the model class for table "activity".
 *
 * The followings are the available columns in table 'activity':
 * @property string $id
 * @property string $date
 * @property string $message
 * @property string $sender_id
 */
class Activity extends CActiveRecord
{
	/********************************************************************/
	/*	Customer Order Activity
	/********************************************************************/
	public static $ACTIVITY_CREATE_NEW_ITEM							= 1;	// Done
	
	/********************************************************************/
	/*	Customer Order Activity
	/********************************************************************/
	public static $ACTIVITY_CREATE_CUSTOMER_ORDER					= 2;	// Done
	public static $ACTIVITY_SEND_INVOICE_FOR_CUSTOMER_ORDER			= 3;	// Done
	public static $ACTIVITY_COMPLETE_PAY_CUSTOMER_ORDER				= 4;	// Done
	public static $ACTIVITY_SELL_ITEMS_CUSTOMER_ORDER				= 5;	// Done
	public static $ACTIVITY_CREATE_NEW_IND_ITEMS_FOR_CUSTOMER_ORDER	= 6;	// Done
	public static $ACTIVITY_CREATE_CUSTOMER_NOTE					= 7;	// Done
	public static $ACTIVITY_CRATE_CUSTOMER_FOLLOWUP					= 8;	// Done	
	
	/********************************************************************/
	/*	Supplier Order Activity
	/********************************************************************/
	public static $ACTIVITY_CREATE_SUPPLIER_ORDER_BY_ADMIN 			= 9;	// Done
	public static $ACTIVITY_CREATE_NEW_IND_ITEMS_FOR_SUPPLIER_ORDER	= 10;	// Done
	public static $ACTIVITY_ADD_ITEMS_TO_SUPPLIER_ORDER 			= 11;	// Done
	public static $ACTIVITY_ACCPET_SUPPLIER_ORDER_BY_SUPPLIER		= 12;	// Done
	public static $ACTIVITY_SET_ETA_SUPPLIER_ORDER_BY_SUPPLIER		= 13;	// Done
	public static $ACTIVITY_COMPLETE_SUPPLIER_ORDER_BY_SUPPLIER		= 14;	// Done
	public static $ACTIVITY_PAY_ADVANCED_SUPPLIER_ORDER_BY_ADMIN	= 15;	// Done
	public static $ACTIVITY_ACCEPT_SUPPLIER_ORDER_ITEMS_BY_SUPPLIER	= 16;	// Done
	public static $ACTIVITY_DENY_SUPPLIER_ORDER_ITEMS_BY_SUPPLIER	= 17;	// Done
	public static $ACTIVITY_PAY_REMAINING_SUPPLIER_ORDER_BY_ADMIN	= 18;	
	
	/********************************************************************/
	/*	Supplier Order Container Activity
	/********************************************************************/
	public static $ACTIVITY_CREATE_SUPPLIER_ORDER_CONTAINER_BY_ADMIN	= 19;
	public static $ACTIVITY_RESET_All_SUPPLIER_ORDER_CONTAINER_BIDS		= 20;
	
	/********************************************************************/
	/*	Supplier Order Shipping Activity
	/********************************************************************/
	public static $ACTIVITY_BID_SUPPLIER_ORDER_CONTAINER_BY_SUPPLIER_SHIPPER	= 21;	
	public static $ACTIVITY_CHOOSE_SUPPLIER_ORDER_CONTAIENR_SHIPPER				= 22;
	
	
	

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Activity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'activity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, message', 'required'),
			array('sender_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, message, sender_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'message' => 'Message',
			'sender_id' => 'Sender',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('sender_id',$this->sender_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'date DESC',
			)
		));
	}
	
	public function search_my_messages()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteriaActivity = new CDbCriteria;
		
		if (YumUser::isSupplier(false) || YumUser::isSupplierShipper(false)){
			$arrActivityId = array();
			
			$criteriaReceiverUser = new CDbCriteria;
			$user_id = Yii::app()->user->id;
			$criteriaReceiverUser->compare('receiver', $user_id);
			$criteriaReceiverUser->compare('receiver_type', 'user');
			$userActivityReceiverList = ActivityReceiver::model()->findAll($criteriaReceiverUser);
			
			foreach ($userActivityReceiverList as $userActivity){
				$arrActivityId[] = $userActivity->activity_id;
			}
			
			$criteriaReceiverUserGroup = new CDbCriteria;
			
			$criteriaReceiverUserGroup->compare('receiver_type', 'user_group');
			if (YumUser::isSupplier(false)){
				$criteriaReceiverUserGroup->compare('receiver', 'supplier');
			} else if (YumUser::isSupplierShipper(false)){		
				$criteriaReceiverUserGroup->compare('receiver', 'supplier_shipper');
			}
			
			$userGroupActivityReceiverList = ActivityReceiver::model()->findAll($criteriaReceiverUserGroup);
			
			foreach ($userGroupActivityReceiverList as $userGroupActivity){
				$arrActivityId[] = $userGroupActivity->activity_id;
			}
			
			$criteriaActivity->addInCondition('id', $arrActivityId);
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteriaActivity,
			'sort'=>array(
				'defaultOrder'=>'date DESC',
			)
		));
	}
	
	public static function addActivityContent($content){
		$activity = new Activity();
		$user_id = Yii::app()->user->id;
		
		$activity->message = $content;
		$activity->sender_id = $user_id;
		$activity->date = date("Y-m-d H:i:s", time());
		if ($activity->save()){
			return $activity->id;
		} else {
			return 0;
		}
	}
	
	/*************************************/
	/* ACTIVITY_CREATE_NEW_ITEM
	/*************************************/
	public static function addActivity_CreateNewItem(){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$content = $currentUserName." created the new item.";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "sales_person", "user_group");
		}
	}
	
	
	/*************************************/
	/* ACTIVITY_CREATE_CUSTOMER_ORDER
	/*************************************/
	public static function addActivity_CreateCustomerOrder($customer_order_id){		
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerOrderInfo = CustomerOrder::model()->findByPk($customer_order_id);
		$customerInfo = Customer::model()->findByPk($customerOrderInfo->customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$content = $currentUserName." created the customer order for ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "admin", "user_group");
			ActivityReceiver::model()->addActivityReceiver($activity_id, "sales_person", "user_group");
			ActivityReceiver::model()->addActivityReceiver($activity_id, $customerOrderInfo->sales_person_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_SEND_INVOICE_FOR_CUSTOMER_ORDER
	/*************************************/
	public static function addActivity_SendInvoiceForCustomerOrder($customer_order_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerOrderInfo = CustomerOrder::model()->findByPk($customer_order_id);
		$customerInfo = Customer::model()->findByPk($customerOrderInfo->customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$content = $currentUserName." sent the invoice for the customer order of ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $customerOrderInfo->sales_person_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_COMPLETE_PAY_CUSTOMER_ORDER
	/*************************************/
	public static function addActivity_CompletePayCustomerOrder($customer_order_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerOrderInfo = CustomerOrder::model()->findByPk($customer_order_id);
		$customerInfo = Customer::model()->findByPk($customerOrderInfo->customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$content = $currentUserName." created new individual item(s) for the customer order of ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $customerOrderInfo->sales_person_id, "user");
		}
	}
		
	/*************************************/
	/* ACTIVITY_SELL_ITEMS_CUSTOMER_ORDER
	/*************************************/
	public static function addActivity_SellItemsCustomerOrder($customer_order_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerOrderInfo = CustomerOrder::model()->findByPk($customer_order_id);
		$customerInfo = Customer::model()->findByPk($customerOrderInfo->customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$content = $currentUserName." sold the items for the customer order of ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $customerOrderInfo->sales_person_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_CREATE_NEW_IND_ITEMS_FOR_CUSTOMER_ORDER
	/*************************************/
	public static function addActivity_CreateNewIndItemsForCustomerOrder($customer_order_id, $item_count = 0){
		if ($item_count == 0)
			return;
			
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerOrderInfo = CustomerOrder::model()->findByPk($customer_order_id);
		$customerInfo = Customer::model()->findByPk($customerOrderInfo->customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$strItemCount = ($item_count == 1)?"1 individual item":$item_count." individual items";
		$content = $currentUserName." created new ".$strItemCount." for the customer order of ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){		
			ActivityReceiver::model()->addActivityReceiver($activity_id, $customerOrderInfo->sales_person_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_CREATE_CUSTOMER_NOTE
	/*************************************/
	public static function addActivity_CreateCustomerNote($customer_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerInfo = Customer::model()->findByPk($customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$content = $currentUserName." created the customer note for ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "sales_person", "user_group");
		}
	}
	
	/*************************************/
	/* ACTIVITY_CRATE_CUSTOMER_FOLLOWUP
	/*************************************/
	public static function addActivity_CreateCustomerFollowup($customer_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$customerInfo = Customer::model()->findByPk($customer_id);
		$customerName = $customerInfo->first_name." ".$customerInfo->last_name;
		
		$content = $currentUserName." created the customer followup for ".$customerName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "sales_person", "user_group");
		}
	}
	
	
	/*************************************/
	/* ACTIVITY_CREATE_SUPPLIER_ORDER_BY_ADMIN
	/*************************************/
	public static function addActivity_CreateSupplierOrderByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." created the supplier order from ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}	
	
	/*************************************/
	/* ACTIVITY_CREATE_NEW_IND_ITEMS_FOR_SUPPLIER_ORDER
	/*************************************/
	public static function addActivity_CreateNewIndItemsForSupplierOrder($supplier_order_id, $item_count = 0){
		if ($item_count == 0)
			return;
			
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplierOrderInfo = SupplierOrder::model()->findByPk($supplier_order_id);
		$supplierName = YumUser::model()->getUserName($supplierOrderInfo->supplier_id);
		
		$strItemCount = ($item_count == 1)?"1 individual item":$item_count." individual items";
		$content = $currentUserName." created new ".$strItemCount." for the supplier order from ".$supplierName.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){		
			ActivityReceiver::model()->addActivityReceiver($activity_id, "operator", "user_group");
		}
	}

	
	/*************************************/
	/* ACTIVITY_ADD_ITEMS_TO_SUPPLIER_ORDER
	/*************************************/
	public static function addActivity_AddItemsToSupplierOrder($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." added the new items to the supplier order from ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}	
	
	/*************************************/
	/* ACTIVITY_ACCPET_SUPPLIER_ORDER_BY_SUPPLIER
	/*************************************/
	public static function addActivity_AcceptSupplierOrderBySupplier($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $supplier_name." accepted the supplier order.";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
	
	/*************************************/
	/* ACTIVITY_SET_ETA_SUPPLIER_ORDER_BY_SUPPLIER
	/*************************************/
	public static function addActivity_SetEtlSupplierOrderBySupplier($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $supplier_name." set E.T.L for the supplier order.";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_PAY_ADVANCED_SUPPLIER_ORDER_BY_ADMIN
	/*************************************/
	public static function addActivity_PayAdvancedSupplierOrderByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." paid advanced for the supplier order from ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
    
    /*************************************/
	/* ACTIVITY_SET_ETL_SUPPLIER_ORDER_BY_ADMIN
	/*************************************/
	public static function addActivity_SetETLSupplierOrderByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." set ETL for the supplier order for ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
    /*************************************/
	/* ACTIVITY_RESET_SUPPLIER_ORDER_TO_MAKING_BY_ADMIN
	/*************************************/
	public static function addActivity_ResetSupplierOrderMakingByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." reset supplier order status to \"Making\" for ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
    /*************************************/
	/* ACTIVITY_RESET_SUPPLIER_ORDER_TO_ADVANCED_PAID_BY_ADMIN
	/*************************************/
	public static function addActivity_ResetSupplierOrderAdvancedPaidByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." reset supplier order status to \"Advanced Paid\" for ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
    
    /*************************************/
	/* ACTIVITY_COMPLETE_SUPPLIER_ORDER_BY_ADMIN
	/*************************************/
	public static function addActivity_CompleteSupplierOrderByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." set supplier order status to \"Completed\" for ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
    
	/*************************************/
	/* ACTIVITY_COMPLETE_SUPPLIER_ORDER_BY_SUPPLIER
	/*************************************/
	public static function addActivity_CompleteSupplierOrderBySupplier($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $supplier_name." completed the supplier order.";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_ACCEPT_SUPPLIER_ORDER_ITEMS_BY_SUPPLIER
	/*************************************/
	public static function addActivity_AcceptSupplierOrderItemsBySupplier($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $supplier_name." accepted the new added items";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_DENY_SUPPLIER_ORDER_ITEMS_BY_SUPPLIER
	/*************************************/
	public static function addActivity_DenySupplierOrderItemsBySupplier($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $supplier_name." denied the new added items";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, $supplier_id, "user");
		}
	}
	
	/*************************************/
	/* ACTIVITY_CREATE_SUPPLIER_ORDER_CONTAINER_BY_ADMIN
	/*************************************/
	public static function addActivity_CreateSupplierOrderContainerByAdmin(){
		$currentUserName = YumUser::model()->getCurrentUserName();		
		$content = $currentUserName." create the new container for supplier orders.";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "supplier_shipper", "user_group");
		}
	}
	
	/*************************************/
	/* ACTIVITY_RESET_All_SUPPLIER_ORDER_CONTAINER_BIDS
	/*************************************/
	public static function addActivity_ResetAllSupplierOrderContainerBids(){
		$currentUserName = YumUser::model()->getCurrentUserName();		
		$content = $currentUserName." reset the supplier order container bids.";
		$activity_id = Activity::model()->addActivityContent($content);
		
		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "supplier_shipper", "user_group");
		}
	}
	
	/*************************************/
	/* ACTIVITY_PAY_REMAINING_SUPPLIER_ORDER_BY_ADMIN
	/*************************************/
	public static function addActivity_PayRemainingSupplierOrderByAdmin($supplier_id){
		$currentUserName = YumUser::model()->getCurrentUserName();
		$supplier_name = YumUser::model()->getUserName($supplier_id);
		$content = $currentUserName." paid remaining for the supplier order from ".$supplier_name.".";
		$activity_id = Activity::model()->addActivityContent($content);

		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "supplier_shipper", "user_group");
		}
	}
	
	/*************************************/
	/* ACTIVITY_BID_SUPPLIER_ORDER_CONTAINER_BY_SUPPLIER_SHIPPER
	/*************************************/
	public static function addActivity_BidSupplierOrderContainerBySupplierShipper($supplier_shipper_id){
		$supplierShipperName = YumUser::model()->getUserName($supplier_shipper_id);
		$content = $supplierShipperName." bade the supplier order container.";
		$activity_id = Activity::model()->addActivityContent($content);

		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "supplier_shipper", "user_group");
		}
	}
	
	/*************************************/
	/* ACTIVITY_CHOOSE_SUPPLIER_ORDER_CONTAIENR_SHIPPER
	/*************************************/
	public static function addActivity_ChooseSupplierOrderContainerShipper(){
		$currentUserName = YumUser::model()->getCurrentUserName();		
		$content = $currentUserName." chose the supplier shipper for the supplier order container.";
		$activity_id = Activity::model()->addActivityContent($content);

		if ($activity_id != 0){
			ActivityReceiver::model()->addActivityReceiver($activity_id, "supplier_shipper", "user_group");
		}
	}
}
