<?php

/**
 * This is the model class for table "supplier_order_note".
 *
 * The followings are the available columns in table 'supplier_order_note':
 * @property string $id
 * @property string $operator_id
 * @property string $supplier_order_id
 * @property string $content
 * @property string $created_on
 *
 * Relations:
 * @property SupplierOrder $supplierOrder
 */
class SupplierOrderNote extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierOrderNote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_order_note';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('operator_id, supplier_order_id, content, created_on, modified', 'required'),
			array('operator_id, supplier_order_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, operator_id, supplier_order_id, content, created_on, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'supplierOrder' => array(self::BELONGS_TO, 'SupplierOrder', 'supplier_order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'operator_id' => 'Operator',
			'supplier_order_id' => 'Supplier Order',
			'content' => 'Content',
			'created_on' => 'Created On',
			'modified' => 'Modified'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($supplier_order_id = 0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('operator_id',$this->operator_id,true);
		
		if ($supplier_order_id != 0){
			$this->supplier_order_id = $supplier_order_id;
		}

		$criteria->compare('supplier_order_id', $this->supplier_order_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('modified',$this->created_on,true);
		
		$criteria->order = 'modified DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterSave()
    {
        $this->sendNotification();
    }

    public function sendNotification()
    {
        //@TODO Change this someone. This one will die soon
        $email = Yii::app()->email;
        $email->to = $this->supplierOrder->supplier->profile->email;
        $email->from = 'auto@midmodfurnishings.com';
        $email->subject = strtr(AutoMailMessage::MS_SUPPLIER_ORDER_NOTE, array(
            '{order_id}' => $this->id,
        ));
        $content = strtr(AutoMailMessage::MC_SUPPLIER_ORDER_NOTE, array(
            '{note}' => $this->content,
        ));
        $email->message = nl2br($content);
        $email->send();
    }
}