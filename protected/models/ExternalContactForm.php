<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ExternalContactForm extends CFormModel
{
	public $name;
	public $email;
    public $phone;
    public $enquiry;
    public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			//array('name, email, subject, body', 'required'),
			array('name', 'required'),
			// email has to be a valid email address
			//array('email', 'email'),
			// verifyCode needs to be entered correctly
			//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'captchaAction' => 'customer/captcha'),
			array('verifyCode', 'application.extensions.recaptcha.EReCaptchaValidator', 'privateKey'=>'6LdW4-0SAAAAAC5R4hLkNX5weEyPEls7du-Sixsg'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
}