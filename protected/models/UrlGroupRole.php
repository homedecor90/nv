<?php

/**
 * This is the model class for table "url_group_role".
 *
 * The followings are the available columns in table 'url_group_role':
 * @property string $url_group_id
 * @property string $role
 */
class UrlGroupRole extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UrlGroupRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'url_group_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url_group_id, role', 'required'),
			array('url_group_id', 'length', 'max'=>10),
			array('role', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('url_group_id, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'url_group_id' => 'Url Group',
			'role' => 'Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('url_group_id',$this->url_group_id,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getGroupRoleString($url_group_id){
		$criteriaUrl = new CDbCriteria;
		$criteriaUrl->compare('url_group_id', $url_group_id);
		$urlGroupRoleList = UrlGroupRole::model()->findAll($criteriaUrl);
		
		$roleStringList = array();
		foreach ($urlGroupRoleList as $urlGroupRole){
			$roleStringList[] = $urlGroupRole->role;
		}
		
		$result = implode(", ", $roleStringList);
		return $result;
	}
	
	public static function updateGroupRole($arrUrlGroupID, $arrGroupRoleID){
		$result = array();
		
		foreach ($arrUrlGroupID as $url_group_id){
			$criteriaUrl = new CDbCriteria;
			$criteriaUrl->compare('url_group_id', $url_group_id);
			$urlGroupRoleList = UrlGroupRole::model()->deleteAll($criteriaUrl);
		
			foreach ($arrGroupRoleID as $role_id){
				$role_string = "";
				if ($role_id == "1"){
					$role_string = UrlGroup::$ROLE_ADMIN;
				} else if ($role_id == "2"){
					$role_string = UrlGroup::$ROLE_SALES_PERSON;
				} else if ($role_id == "3"){
					$role_string = UrlGroup::$ROLE_OPERATOR;
				} else if ($role_id == "4"){
					$role_string = UrlGroup::$ROLE_SUPPLIER;
				} else if ($role_id == "5"){
					$role_string = UrlGroup::$ROLE_SUPPLIER_SHIPPER;
				} else if ($role_id == "6"){
					$role_string = UrlGroup::$ROLE_ALL_AUTHENTICATED_USERS;
				} else if ($role_id == "7"){
					$role_string = UrlGroup::$ROLE_ALL_VISITORS;
				}
				
				$newGroupRole = new UrlGroupRole();
				$newGroupRole->url_group_id = $url_group_id;
				$newGroupRole->role = $role_string;
				
				if (!$newGroupRole->save()){
					$result['message'] = 'fail';
					$result['error'] = CHtml::errorSummary($newGroupRole);
					echo json_encode($result);
					return;
				}
			}
		}
		
		$result['message'] = 'success';
		echo json_encode($result);
	}
}