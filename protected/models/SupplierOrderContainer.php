<?php

/**
 * This is the model class for table "supplier_order_container".
 *
 * The followings are the available columns in table 'supplier_order_container':
 * @property string $id
 * @property string $created_date
 * @property string $status
 * @property string $picked_up_date
 * @property string $shipped_date
 * @property string $supplier_shipper_id
 * @property string $destination_port
 * @property string $original_port
 * @property string $terms
 * @property string $operator_id
 * @property boolean $archived
 */
class SupplierOrderContainer extends CActiveRecord
{
	public static $TERMS_EXW = "EXW";
	public static $TERMS_FOB = "FOB";
    
    private $total_cbm = null;
    private $shipping_price = null;
    private $chosen_bid = null;
    
	/*public static $EXW_FIELD_LIST = array(
		"Custom Clearance China", 
		"Cost from Supplir to China Port", 
		"Misc. Charges", 
	);*/
	
    public static $EXW_FIELD_LIST = array(
		"ORC", 
		"DOC", 
		"AMS", 
		"Clearance customs", 
		"Fumigation fee", 
		"Trucking fee", 
        "Misc. Charges", 
	);
    
	public static $FOB_FIELD_LIST = array(
		/*"FOB Chinese Port to LA Port",
		"FOB LA Custom Clearance",
		"FOB LA Port to Warehouse",*/
	);
	
    /*
	public static $COMMON_FIELD_LIST = array(
		"Chinese Port to LA Port", 
		"LA Custom Clearance", 
		"LA Port to Warehouse", 
		"Est USA Duty", 
		"USA customs entry", 
		"ISF Filing", 
		"Cartage and Services", 
		"Clean Truck Fee", 
		"Pier Pass",
		"Any Other Charges",
	);*/
	
	public static $COMMON_FIELD_LIST = array(
		"Ocean Freight", 
		"ISF Filing", 
		"Import Handling", 
		"Entry Fee", 
		"Duty", 
		"Messenger Fee", 
		"Pier Pass Fee", 
		"Clean Truck Fee",
		"Container Delivery to [city_zip]",
		"Any Other Charges",
	);
    
    
	public static $STATUS_PENDING 			= "Pending";
	public static $STATUS_CHOSEN_SHIPPER 	= "Chosen Shipper";

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierOrderContainer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_order_container';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_date, operator_id', 'required'),
			array('status, destination_port', 'length', 'max'=>100),
			array('original_port', 'length', 'max'=>200),
			array('supplier_shipper_id, operator_id', 'length', 'max'=>10),
			array('terms', 'length', 'max'=>20),
			array('picked_up_date, shipped_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created_date, status, picked_up_date, shipped_date, supplier_shipper_id, destination_port, original_port, terms, operator_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'orders'=>array(self::HAS_MANY, 'SupplierOrder', 'container_id'),
            'shipper'=>array(self::BELONGS_TO, 'YumUser', 'supplier_shipper_id'),
            // 'bid'=>array(self::HAS_ONE, 'SupplierOrder', 'container_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created_date' => 'Created Date',
			'status' => 'Status',
			'picked_up_date' => 'Picked Up Date',
			'shipped_date' => 'Shipped Date',
			'supplier_shipper_id' => 'Supplier Shipper',
			'destination_port' => 'Destination Port',
			'original_port' => 'Original Port',
			'terms' => 'Terms',
			'operator_id' => 'Operator',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('picked_up_date',$this->picked_up_date,true);
		$criteria->compare('shipped_date',$this->shipped_date,true);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id,true);
		$criteria->compare('destination_port',$this->destination_port,true);
		$criteria->compare('original_port',$this->original_port,true);
		// $criteria->compare('terms',$this->terms,true);
		$criteria->compare('operator_id',$this->operator_id,true);
		$criteria->compare('paid',$this->paid,false);
		$criteria->compare('archived', $this->archived, false);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'created_date DESC',
            ),
		));
	}
	
    public function searchChosen()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('picked_up_date',$this->picked_up_date,true);
		$criteria->compare('shipped_date',$this->shipped_date,true);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id,false);
		$criteria->compare('destination_port',$this->destination_port,true);
		$criteria->compare('original_port',$this->original_port,true);
		// $criteria->compare('terms',$this->terms,true);
		$criteria->compare('operator_id',$this->operator_id,true);
		$criteria->compare('paid',$this->paid,false);
        
        if ($_GET['date_from']){
            $criteria->addCondition('created_date>="'.date("Y-m-d H:i:s",strtotime($_GET['date_from'])).'"');
        }
        
        if ($_GET['date_to']){
            $criteria->addCondition('created_date<="'.date("Y-m-d H:i:s",strtotime($_GET['date_to'])).'"');
        }
        
        $criteria->addCondition('NOT ISNULL(chosen_date)');
        
        // echo $criteria->condition;
        // die();
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchAvailable()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('status', SupplierOrderContainer::$STATUS_PENDING,true);
		$criteria->compare('picked_up_date',$this->picked_up_date,true);
		$criteria->compare('shipped_date',$this->shipped_date,true);
		$criteria->compare('supplier_shipper_id',$this->supplier_shipper_id,true);
		$criteria->compare('destination_port',$this->destination_port,true);
		$criteria->compare('original_port',$this->original_port,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('operator_id',$this->operator_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getCommonFields(){
        $fields = self::$COMMON_FIELD_LIST;
        $city_zip = Settings::getVar('company_address_city').' '.Settings::getVar('company_address_zip');
        foreach($fields as $i=>$field){
            $fields[$i] = str_replace('[city_zip]', $city_zip, $field);
        }

        return $fields;
    }

    public static function getEXWFields(){
        return self::$EXW_FIELD_LIST;
    }

    public function getOrderInfo($container_id){
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->compare('container_id',$container_id);
		$supplierOrders = SupplierOrder::model()->findAll($criteriaOrders);
		$strOrders = "";
		
		foreach ($supplierOrders as $supplierOrderInfo){
			$supplier_order_id = $supplierOrderInfo->id;
			$supplier_id = $supplierOrderInfo->supplier_id;
			$supplier_model = YumUser::model()->findByPk($supplier_id);
			$supplier_name = $supplier_model->profile->firstname." ".$supplier_model->profile->lastname;
			$supplier_port_box = $supplier_model->profile->port_box;
		
			$criteriaItems = new CDbCriteria;
			$criteriaItems->compare('supplier_order_id',$supplier_order_id);
            $criteriaItems->compare('status','<>'.SupplierOrder::$STATUS_DENIED);
			$supplierOrder = SupplierOrder::model()->findByPk($supplier_order_id);
			
			$strAction = "";
			if ((YumUser::model()->isSupplier(false))
				&& ($supplierOrder->status != SupplierOrder::$STATUS_PENDING)){
					$strAction = '<th class="action">Action</td>';
			}
			
			$supplier_order_eta = $supplierOrder->etl;
			$admin = YumUser::model()->isOperator(true);
			$strOrders .= '<table class="item_list">'
				.'<thead>'
				.'<th class="supplier">Supplier</td>'
				.'<th class="port">Port</td>'
				.'<th class="eta">E.T.L</td>'
//				.'<th class="eta">E.T.L 2</td>'
				.'<th class="cbm">Total CBM</td>'
				.($admin ? '<th class="docs">Official Docs</th>' : '')
				.'</thead>';
			
			$strOrders .= '<tbody><tr>'
				.'<td class="supplier">'.$supplier_name.'</td>'
				."<td>".$supplier_port_box."</td>"
				."<td>".date("M. d, Y", strtotime($supplier_order_eta))."</td>";
//				."<td>".date("M. d, Y", strtotime($supplier_order_eta) + 24 * 3600 * 7)."</td>";

            $controller = new CController('supplierOrderFancy');
            
			// $strOrders .= $strItems.'</td>';
            $strOrders .= '<td><span href="#order_contents_'.$supplier_order_id.'" id="open_order_contents_'.$supplier_order_id.'" class="total_cbm">'.SupplierOrder::model()->getSupplierOrderTotalCBM($supplier_order_id).'</span>'.SupplierOrder::getItemsPopupCode($supplier_order_id, $controller).'</td>';
            
			if ($admin){
				$docs = array();
				foreach($supplierOrderInfo->official_docs as $j=>$doc){
					$docs[] = '<a target="blank" href="'.Yii::app()->getBaseUrl(true).'/index.php/supplierorder/final_doc/?order_id='.$supplier_order_id.'&doc='.($j+1).'">View</a>';
				}
				$strOrders .= '<td>'.implode('<br />', $docs).'</td>';
			}
			
			$strOrders .= '</tr></tbody></table><br/>';
		}
	
		return $strOrders;
	}
	
	public function getOrderInfoByShipper($container_id){
		$strItems = "";
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->compare('container_id',$container_id);
		$supplierOrders = SupplierOrder::model()->findAll($criteriaOrders);
		$strOrders = "";
		
		foreach ($supplierOrders as $supplierOrderInfo){
			$supplier_order_id = $supplierOrderInfo->id;
			$supplier_id = $supplierOrderInfo->supplier_id;
			$supplier_model = YumUser::model()->findByPk($supplier_id);
			$supplier_name = $supplier_model->profile->firstname." ".$supplier_model->profile->lastname;
			$supplier_port_box = $supplier_model->profile->port_box;
		
			$criteriaItems = new CDbCriteria;
			$criteriaItems->compare('supplier_order_id',$supplier_order_id);
			$items = SupplierOrderItems::model()->findAll($criteriaItems);
			$supplierOrder = SupplierOrder::model()->findByPk($supplier_order_id);
			
			$strAction = "";
			if ((YumUser::model()->isSupplier(false))
				&& ($supplierOrder->status != SupplierOrder::$STATUS_PENDING)){
					$strAction = '<th class="action">Action</td>';
			}
			
			$total_cbm = SupplierOrder::model()->getSupplierOrderTotalCBM($supplier_order_id);
			$supplier_order_eta = $supplierOrder->etl;
			
			$strOrders .= '<table class="item_list">'
				.'<thead>'
				.'<th class="supplier">Supplier</td>'
				.'<th class="total_cbm">Total CBM</td>'
				.'<th class="eta">Loading Date</td>'
				.'</thead>';
			
			$strOrders .= '<tbody><tr>'
				.'<td class="supplier">'.$supplier_name.'</td>'
				."<td>".$total_cbm."</td>"
				."<td>".date("M. d, Y", strtotime($supplier_order_eta))."</td>"
				;
			
			$strItems = '<table class="item_list">'
				.'<thead>'
				.'<th class="item_code">Item Code</td>'
				.'<th class="color">Color</td>'
				.'<th class="price">Price</td>'
				.'<th class="cbm">CBM</td>'
				.'<th class="quantity">Qty</td>'
				.'<th class="added_date">Added Date</td>'
				.'<th class="total">Total</td>'
				.'<th class="status">Status</td>'				
				.'</thead>';
				
			foreach ($items as $item){			
				$item_id = $item->item_id;
				$item_info = Item::model()->findByPk($item_id);
				$item_toal = floatval($item->exw_cost_price) * floatval($item->quantity);
				$strStatus = "<b>".$item->status."</b><br/>".$item->processed_date;
				$strItems .= "<tr>";
				$strItems .= "<td class='item_code'>".$item_info->item_code."</td>";
				$strItems .= "<td class='color'>".$item_info->color."</td>";
				$strItems .= "<td class='price'>".$item->exw_cost_price."</td>";
				$strItems .= "<td class='price'>".$item_info->cbm."</td>";
				$strItems .= "<td class='quantity'>".$item->quantity."</td>";
				$strItems .= "<td class='added_date'>".$item->added_date."</td>";
				$strItems .= "<td class='total'>".$item_toal."</td>";
				$strItems .= "<td class='status'>".$strStatus."</td>";				
				$strItems .= "</tr>";
			}

			$strItems .= "</table>";
			$strOrders .= "</tr></tbody></table><br/>";
		}
	
		return $strOrders;
	}

	public static function getSupplierOrderContainerTotalEXWPriceWithShipping($container_id){
		
		$total_exw_price = SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($container_id);
		//$total_exw_price_shipping = $total_exw_price;
		$total_exw_price_shipping = 0;
		
		$user_id =  Yii::app()->user->id;
		
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_shipper_id', $user_id);
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		$supplierOrderContainerBidModelList = SupplierOrderContainerBidding::model()->findAll($criteriaBid);
		
		if (count($supplierOrderContainerBidModelList) > 0){
			$supplierOrderContainerBidModel = $supplierOrderContainerBidModelList[0];
			$bid_id = $supplierOrderContainerBidModel->id;
			$bid_exw_price = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalEXWPrice($bid_id);
			$total_exw_price_shipping += $bid_exw_price;
		}
		
		$criteriaBid = new CDbCriteria;
		$user_id =  Yii::app()->user->id;
		
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_shipper_id', $user_id);
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		$supplierOrderContainerBiddingModel = SupplierOrderContainerBidding::model()->find($criteriaBid);
		
		$strResult = '<p class="total_exw_price" id="total_exw_price_'.$container_id.'">'.$total_exw_price_shipping.'</p>';
		/*$strResult = '<p class="total_exw_price" id="total_exw_price_'.$container_id.'">Total: '.$total_exw_price_shipping.'</p>';
		if ($supplierOrderContainerBiddingModel != null){
			$bid_id = $supplierOrderContainerBiddingModel->id;
			$strOptions = SupplierOrderContainerBidding::model()->getBiddingEXWOptions($bid_id);
			$strResult .= '<div id="detail_exw_price_'.$container_id.'" class="detail_exw_fob_price">EXW Price:'.$total_exw_price.'<br/>'
					.$strOptions.'</div>';
		}*/
		
		return $strResult;
	}
	
	public static function getSupplierOrderContainerTotalFobPriceWithShipping($container_id){		
		$total_fob_price = SupplierOrderContainer::model()->getSupplierOrderContainerTotalFobPrice($container_id);
		//$total_fob_price_shipping = $total_fob_price;
		$total_fob_price_shipping = 0;
		
		$user_id =  Yii::app()->user->id;
		
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_shipper_id', $user_id);
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		$supplierOrderContainerBidModelList = SupplierOrderContainerBidding::model()->findAll($criteriaBid);
		
		if (count($supplierOrderContainerBidModelList) > 0){
			$supplierOrderContainerBidModel = $supplierOrderContainerBidModelList[0];
			$bid_id = $supplierOrderContainerBidModel->id;
			$bid_fob_price = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalFOBPrice($bid_id);
			$total_fob_price_shipping += $bid_fob_price;
		}
		
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_shipper_id', $user_id);
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		$supplierOrderContainerBiddingModel = SupplierOrderContainerBidding::model()->find($criteriaBid);
		
		$strResult = '<p class="total_fob_price" id="total_fob_price_'.$container_id.'">'.$total_fob_price_shipping.'</p>';		
		/*$strResult = '<p class="total_fob_price" id="total_fob_price_'.$container_id.'"> Total: '.$total_fob_price_shipping.'</p>';		
		if ($supplierOrderContainerBiddingModel != null){
			$bid_id = $supplierOrderContainerBiddingModel->id;
			$strOptions = SupplierOrderContainerBidding::model()->getBiddingFobOptions($bid_id);
			$strResult .= '<div id="detail_fob_price_'.$container_id.'" class="detail_exw_fob_price">FOB Price:'.$total_fob_price.'<br/>'
					.$strOptions.'</div>';
		}*/
		
		return $strResult;
	}
	
	// returns false if no shipper is selected
	public function getEXWTotalWithShipping(){
		if ($this->status == self::$STATUS_PENDING){
			return false;
		}
		$bid = $this->getWinningBid();
		$total_exw_price = SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($this->id);
		
		$exw_shipping = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalEXWPrice($bid->id);
		return $total_exw_price + $exw_shipping;
	}
	
	// returns false if no shipper is selected
	public function getFOBTotalWithShipping(){
		if ($this->status == self::$STATUS_PENDING){
			return false;
		}
		$bid = $this->getWinningBid();
		$total_fob_price = SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($this->id);

		$fob_shipping = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalFOBPrice($bid->id);
		return $total_fob_price + $fob_shipping;
	}
	
	public function getWinningBid(){
		if ($this->status == self::$STATUS_PENDING){
			return false;
		}
		
		$criteria = new CDbCriteria();
		$criteria->compare('supplier_shipper_id', $this->supplier_shipper_id);
		$criteria->compare('supplier_order_container_id', $this->id);
		
		return SupplierOrderContainerBidding::model()->find($criteria);
	}
	
	public static function getSupplierOrderContainerTotalEXWPrice($container_id){		
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->compare('container_id',$container_id);
		$supplierOrders = SupplierOrder::model()->findAll($criteriaOrders);
		
		$total_exw_price = 0;
		foreach ($supplierOrders as $supplierOrderInfo){
			$supplier_order_id = $supplierOrderInfo->id;
			$order_total_exw_price = SupplierOrder::model()->getSupplierOrderTotalExwPrice($supplier_order_id);
			$total_exw_price += $order_total_exw_price;
		}
	
		return $total_exw_price;
	}
	
	public static function getSupplierOrderContainerTotalFobPrice($container_id){		
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->compare('container_id',$container_id);
		$supplierOrders = SupplierOrder::model()->findAll($criteriaOrders);
		
		$total_fob_price = 0;
		foreach ($supplierOrders as $supplierOrderInfo){
			$supplier_order_id = $supplierOrderInfo->id;
			$order_total_fob_price = SupplierOrder::model()->getSupplierOrderTotalFobPrice($supplier_order_id);
			$total_fob_price += $order_total_fob_price;
		}
	
		return $total_fob_price;
	}
	
	public static function getSupplierOrderContainerTotalCBM($container_id){
		$strItems = "";
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->compare('container_id',$container_id);
		$supplierOrders = SupplierOrder::model()->findAll($criteriaOrders);
		
		$total_cbm = 0;
		foreach ($supplierOrders as $supplierOrderInfo){
			$supplier_order_id = $supplierOrderInfo->id;
			$order_total_cbm = SupplierOrder::model()->getSupplierOrderTotalCBM($supplier_order_id);
			$total_cbm += $order_total_cbm;
		}
	
		return $total_cbm;
	}
	
    public function getTotalCBM()
    {
        if ($this->total_cbm === null){
            $this->total_cbm = SupplierOrderContainer::getSupplierOrderContainerTotalCBM($this->id);
        }
        
        return $this->total_cbm;
    }
    
    public function getShippingPrice(){
        if ($this->shipping_price === null){
            $bid = $this->getChosenBid();
            if ($this->terms == SupplierOrderContainer::$TERMS_EXW){
                $this->shipping_price = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalEXWPrice($bid->id);
            } else {
                $this->shipping_price = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalFOBPrice($bid->id);
            }
        }
        return $this->shipping_price;
    }
    
    public function getChosenBid(){
        if (!$this->chosen_date){
            return null;
        } else {
            if (!$this->chosen_bid){
                $criteria = new CDbCriteria();
                $criteria->compare('supplier_shipper_id', $this->supplier_shipper_id);
                $criteria->compare('supplier_order_container_id', $this->id);
                $this->chosen_bid = SupplierOrderContainerBidding::model()->find($criteria);
            }
        }
        
        return $this->chosen_bid;
    }
    
    // returns timestamp
    public function getETL2(){
        $etl = array();
        foreach ($this->orders as $row){
            $etl[] = strtotime($row->etl) + 24 * 3600 * 7;
        }
        
        if (!count($etl)){
        	return 0;
        }
        
        return max($etl);
    }
    
    public function getSupplierAddressStr(){
        $addr = array();
        foreach ($this->orders as $order){
            $user = YumUser::model()->findByPk($order->supplier_id);
            $addr[] = $user->profile->port_box;
        }
        
        return implode('<br /><br />', $addr);
    }
    
    public function getSupplierCount(){
		return count($this->orders);
    }
	
	public static function getActionButtonStringByShipper($supplier_order_container_id){
		$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($supplier_order_container_id);
		$user_id =  Yii::app()->user->id;
		
		$criteriaBidding = new CDbCriteria;
		$criteriaBidding->compare('supplier_shipper_id', $user_id, true);
		$criteriaBidding->compare('supplier_order_container_id', $supplier_order_container_id, true);		
		$result = SupplierOrderContainerBidding::model()->findAll($criteriaBidding);		
		
		$strResult = '';
		if (count($result) > 0){
            // show my bid
            $strResult .= '<strong>Your bid:</strong><br />';
            $strResult .= 'EXW: $'.SupplierOrderContainerBidding::getBiddingOptionsTotalEXWPrice($result[0]->id);
            if ($supplierOrderContainerModel->supplier_shipper_id == $user_id && $supplierOrderContainerModel->terms == 'EXW'){
                $strResult .= ' <span class="lowest_price">WON</span>';
            }
            $strResult .= ' - FOB: $'.SupplierOrderContainerBidding::getBiddingOptionsTotalFOBPrice($result[0]->id);
            if ($supplierOrderContainerModel->supplier_shipper_id == $user_id && $supplierOrderContainerModel->terms == 'FOB'){
                $strResult .= ' <span class="lowest_price">WON</span>';
            }
            
            // if I'm not a winner - shoe winner's bid
            if ($supplierOrderContainerModel->status == SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER
                    && $supplierOrderContainerModel->supplier_shipper_id != $user_id){
                $bid = $supplierOrderContainerModel->getChosenBid();
                
                $strResult .= '<br /><strong>Winner\'s bid:</strong><br />';
                $strResult .= 'EXW: $'.SupplierOrderContainerBidding::getBiddingOptionsTotalEXWPrice($bid->id);
                if ($supplierOrderContainerModel->terms == 'EXW'){
                    $strResult .= ' <span class="lowest_price">WON</span>';
                }
                $strResult .= ' - FOB: $'.SupplierOrderContainerBidding::getBiddingOptionsTotalFOBPrice($bid->id);
                if ($supplierOrderContainerModel->terms == 'FOB'){
                    $strResult .= ' <span class="lowest_price">WON</span>';
                }
            }
            
			if ($strResult != ''){
				$strResult .= '<br/>';
			}
		}

		$strResult .= '<a href="#" class="view_detail" rel="'.$supplier_order_container_id.'">View Detail</a>';
		return $strResult;
	}
	
	public static function getDetailLink($container_id){
		$strLink = '<a href="'.Yii::app()->getBaseUrl(true).'/index.php/supplierorder/container_detail/?container_id='.$container_id.'">Detail</a>';
		return $strLink;
	}
	
	public static function getShipperName($container_id){
		$supplierOrderContainerModel = SupplierOrderContainer::model()->findByPk($container_id);
		$supplier_shipper_id = $supplierOrderContainerModel->supplier_shipper_id;
		
		if ($supplier_shipper_id == 0){
			return "";
		}
		
		$supplierShipper = YumUser::model()->findByPk($supplier_shipper_id);
		$name = $supplierShipper->profile->firstname." ".$supplierShipper->profile->lastname;
		
		return $name;
	}
	
	public static function getSupplierOrderContainerBidCutoffDate($container_id){
		$user_id =  Yii::app()->user->id;
		
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_shipper_id', $user_id);
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		$supplierOrderContainerBidModelList = SupplierOrderContainerBidding::model()->findAll($criteriaBid);
		
		if (count($supplierOrderContainerBidModelList) > 0){
			$supplierOrderContainerBidModel = $supplierOrderContainerBidModelList[0];
			return date("M, d. Y", strtotime($supplierOrderContainerBidModel->cutoff_date));
		}
		
		return "";
	}
	
	public static function getSupplierOrderContainerBidEta($container_id){
		$user_id =  Yii::app()->user->id;
		
		$criteriaBid = new CDbCriteria;
		$criteriaBid->compare('supplier_shipper_id', $user_id);
		$criteriaBid->compare('supplier_order_container_id', $container_id);
		$supplierOrderContainerBidModelList = SupplierOrderContainerBidding::model()->findAll($criteriaBid);

		if (count($supplierOrderContainerBidModelList) > 0){
			$supplierOrderContainerBidModel = $supplierOrderContainerBidModelList[0];
			return date("M, d. Y", strtotime($supplierOrderContainerBidModel->bid_eta));
		}
		
		return "";
	}
	
	public function getETA(){
		if ($bid = $this->getChosenBid()){
			return $bid->bid_eta;
		} else {
			return false;
		}
	}
	
	public function addSupplierOrders($order_id_list){
		$container_id = $this->id;
		$arrOrderList = explode("_", $order_id_list);
		
		$criteriaOrders = new CDbCriteria;
		$criteriaOrders->addInCondition('id', $arrOrderList);
		$attributesOrders = array('container_id'=>$container_id);
		SupplierOrder::model()->updateAll($attributesOrders, $criteriaOrders);
		
		$criteriaIndividualItems = new CDbCriteria;
		$criteriaIndividualItems->addInCondition('supplier_order_id', $arrOrderList);
		$attributesIndividualItems = array('supplier_order_container_id'=>$container_id);
		Individualitem::model()->updateAll($attributesIndividualItems, $criteriaIndividualItems);
	}
    
    public function getBidsCount(){
        $criteria = new CDbCriteria();
        $criteria->compare('supplier_order_container_id', $this->id);
        return SupplierOrderContainerBidding::model()->count($criteria);
    }
	
	// public function getShipperName(){
		// $profile = YumUser::model()->findByPk($this->supplier_shipper_id)->profile;
		// return $profile->firstname." ".$profile->lastname;		
	// }
	
	// returns htmls
	public function getOfficialDocsList(){
		$docs_str = array();
		foreach($this->orders as $order){
			$str = '';
			foreach($order->official_docs as $j=>$doc){
				$str .= '<a target="blank" href="'.Yii::app()->getBaseUrl(true).'/index.php/supplierorder/final_doc/?order_id='.$order->id.'&doc='.($j+1).'">View</a>';
			}
			$docs_str[] = $str;
		}
		return implode('<hr />',$docs_str);
	}
	
	public function getPaymentPDFLink(){
		return Yii::app()->getBaseUrl(true).'/pdf/container/?container_id='.$this->id;
	}
	
	public function scanAllItemsIn(){
		$criteria = new CDbCriteria();
		$criteria->compare('supplier_order_container_id', $this->id);
		Individualitem::model()->updateAll(array('status'=>Individualitem::$STATUS_IN_STOCK), $criteria);
		return Individualitem::model()->count($criteria);
	}
}
