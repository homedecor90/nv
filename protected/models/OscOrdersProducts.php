<?php

/**
 * This is the model class for table "osc_orders_products".
 *
 * The followings are the available columns in table 'osc_orders_products':
 * @property integer $orders_products_id
 * @property integer $orders_id
 * @property integer $products_id
 * @property string $products_model
 * @property string $products_name
 * @property string $products_price
 * @property string $final_price
 * @property string $products_tax
 * @property integer $products_quantity
 */
class OscOrdersProducts extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OscOrdersProducts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'osc_orders_products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orders_id, products_id, products_quantity', 'numerical', 'integerOnly'=>true),
			array('products_model', 'length', 'max'=>25),
			array('products_name', 'length', 'max'=>64),
			array('products_price, final_price', 'length', 'max'=>15),
			array('products_tax', 'length', 'max'=>7),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orders_products_id' => 'Orders Products',
			'orders_id' => 'Orders',
			'products_id' => 'Products',
			'products_model' => 'Products Model',
			'products_name' => 'Products Name',
			'products_price' => 'Products Price',
			'final_price' => 'Final Price',
			'products_tax' => 'Products Tax',
			'products_quantity' => 'Products Quantity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('orders_products_id',$this->orders_products_id);
		$criteria->compare('orders_id',$this->orders_id);
		$criteria->compare('products_id',$this->products_id);
		$criteria->compare('products_model',$this->products_model,true);
		$criteria->compare('products_name',$this->products_name,true);
		$criteria->compare('products_price',$this->products_price,true);
		$criteria->compare('final_price',$this->final_price,true);
		$criteria->compare('products_tax',$this->products_tax,true);
		$criteria->compare('products_quantity',$this->products_quantity);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getProductAttirbutes($product_id){
		$strResult = "";
		
		$criteriaOrderProductsAttributes =new CDbCriteria;
		$criteriaOrderProductsAttributes->compare('orders_products_id',$product_id);
		
		$oscOrdersProductsAttributesModel = OscOrdersProductsAttributes::model()->findAll($criteriaOrderProductsAttributes);
		
		foreach ($oscOrdersProductsAttributesModel as $attributes){
			$strAttribute = $attributes->products_options." : ".$attributes->products_options_values;
			$strResult = ($strResult == "")?"":$strResult."<br/>";
			$strResult .= $strAttribute;
		}
		
		return $strResult;
	}
	
	public static function importOscData($siteId){
        $last_order_id = Yii::app()->db->createCommand()
            ->select('orders_id')
            ->from('osc_orders_products')
            ->where('site_id=:siteId', array(':siteId'=>$siteId))
            ->order('orders_products_id desc')
            ->queryScalar();
		
		$sqlSelect = 'SELECT * FROM orders_products'. ($last_order_id ?
            " WHERE orders_id > $last_order_id" : '');
		$resource = mysql_query($sqlSelect);
		
		while ($row = mysql_fetch_array($resource)){
			$field_count = count(array_keys($row)) / 2;
			
			while ($field_count > 0){
				$field_count--;
				
				if (isset($row[$field_count])){
					unset($row[$field_count]);
				}
			}
			
			$newOscOrderProductsModel = new OscOrdersProducts();
			$newOscOrderProductsModel->orders_products_id = $row['orders_products_id'];
			$newOscOrderProductsModel->site_id = $siteId;
			$newOscOrderProductsModel->attributes = $row;
			$newOscOrderProductsModel->save();
		}
	}
}