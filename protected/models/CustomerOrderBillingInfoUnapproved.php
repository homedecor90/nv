<?php

/**
 * This is the model class for table "customer_order_billing_info".
 *
 * The followings are the available columns in table 'customer_order_billing_info':
 * @property string $id
 * @property string $customer_order_id
 * @property string $billing_name
 * @property string $billing_last_name
 * @property string $billing_street_address
 * @property string $billing_city
 * @property string $billing_state
 * @property string $billing_zip_code
 * @property string $billing_phone_number
 * @property string $billing_email_address
 * @property string $shipping_type
 * @property string $shipping_name
 * @property string $shipping_last_name
 * @property string $shipping_street_address
 * @property string $shipping_city
 * @property string $shipping_state
 * @property string $shipping_zip_code
 * @property string $shipping_phone_number
 * @property string $order_notes
 */
class CustomerOrderBillingInfoUnapproved extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomerOrderBillingInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_order_billing_info_unapproved';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_order_id', 'required'),
			array('customer_order_id', 'length', 'max'=>10),
			array('billing_name, billing_last_name, billing_street_address, billing_city, billing_state, billing_zip_code, billing_phone_number, billing_email_address, shipping_type, shipping_name, shipping_last_name, shipping_street_address, shipping_city, shipping_state, shipping_zip_code, shipping_phone_number, busrestype', 'length', 'max'=>100),
			//~ array('order_notes,shipping_notes', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_order_id, cc_number, cc_expiry_date, cc_name_on_card, cc_type, billing_name, billing_last_name, billing_street_address, billing_city, billing_state, billing_zip_code, billing_phone_number, billing_email_address, shipping_type, shipping_street_address, shipping_city, shipping_state, shipping_zip_code, shipping_phone_number, order_notes,shipping_notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer_order'=>array(self::BELONGS_TO, 'CustomerOrder', 'customer_order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_order_id' => 'Customer Order',
			'billing_name' => 'Billing Name',
			'billing_last_name' => 'Billing Last Name',
			'billing_street_address' => 'Billing Street Address',
			'billing_city' => 'Billing City',
			'billing_state' => 'Billing State',
			'billing_zip_code' => 'Billing Zip Code',
			'billing_phone_number' => 'Billing Phone Number',
			'billing_email_address' => 'Billing Email Address',
			'shipping_type' => 'Shipping Type',
			'shipping_name' => 'Shipping Name',
			'shipping_last' => 'Shipping Last Name',
			'shipping_city' => 'Shipping City',
			'shipping_state' => 'Shipping State',
			'shipping_zip_code' => 'Shipping Zip Code',
			'shipping_phone_number' => 'Shipping Phone Number',
			'order_notes' => 'Order Notes',
			'busrestype'=>'Address Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('customer_order_id',$this->customer_order_id,true);
		$criteria->compare('billing_name',$this->billing_name,true);
		$criteria->compare('billing_last_name',$this->billing_last_name,true);
		$criteria->compare('billing_street_address',$this->billing_street_address,true);
		$criteria->compare('billing_city',$this->billing_city,true);
		$criteria->compare('billing_state',$this->billing_state,true);
		$criteria->compare('billing_zip_code',$this->billing_zip_code,true);
		$criteria->compare('billing_phone_number',$this->billing_phone_number,true);
		$criteria->compare('billing_email_address',$this->billing_email_address,true);
		$criteria->compare('shipping_type',$this->shipping_type,true);
		$criteria->compare('shipping_street_address',$this->shipping_street_address,true);
		$criteria->compare('shipping_city',$this->shipping_city,true);
		$criteria->compare('shipping_state',$this->shipping_state,true);
		$criteria->compare('shipping_zip_code',$this->shipping_zip_code,true);
		$criteria->compare('shipping_phone_number',$this->shipping_phone_number,true);
		
		$criteria->with = array('customer_order', 'customer_order.customer');
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getShippingAddress(){
		$str  = $this->shipping_name.' '.$this->shipping_last_name.'<br /><br />';
		$str .= $this->shipping_street_address.', ';
		$str .= $this->shipping_city.', ';
		$str .= $this->shipping_state.', ';
		$str .= $this->shipping_zip_code;
		$str .= '<br />Phone: '.$this->shipping_phone_number;
		$str .= '<br /><br />'.$this->busrestype;
		
		return $str;
	}
	
	public function getBillingAddress(){
		$str  = $this->billing_name.' '.$this->billing_last_name.'<br /><br />';
		$str .= $this->billing_street_address.', ';
		$str .= $this->billing_city.', ';
		$str .= $this->billing_state.', ';
		$str .= $this->billing_zip_code;
		$str .= '<br />Phone: '.$this->billing_phone_number;
		$str .= '<br /><br />'.$this->busrestype;
		
		return $str;
	}
}
