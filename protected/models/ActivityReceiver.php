<?php

/**
 * This is the model class for table "activity_receiver".
 *
 * The followings are the available columns in table 'activity_receiver':
 * @property string $id
 * @property string $activity_id
 * @property string $receiver
 * @property string $receiver_type
 */
class ActivityReceiver extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ActivityReceiver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'activity_receiver';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('activity_id, receiver, receiver_type', 'required'),
			array('activity_id', 'length', 'max'=>10),
			array('receiver, receiver_type', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, activity_id, receiver, receiver_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'activity_id' => 'Activity',
			'receiver' => 'Receiver',
			'receiver_type' => 'Receiver Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('activity_id',$this->activity_id,true);
		$criteria->compare('receiver',$this->receiver,true);
		$criteria->compare('receiver_type',$this->receiver_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function addActivityReceiver($activity_id, $receiver, $receiver_type){
		$activityReceiverModel = new ActivityReceiver();
		$activityReceiverModel->activity_id = $activity_id;
		$activityReceiverModel->receiver = $receiver;
		$activityReceiverModel->receiver_type = $receiver_type;
		$activityReceiverModel->save();
	}
}