<?php

/**
 * This is the model class for table "url_group".
 *
 * The followings are the available columns in table 'url_group':
 * @property string $id
 * @property string $title
 */
class UrlGroup extends CActiveRecord
{
	public static $ROLE_ADMIN 						= "Admin";
	public static $ROLE_SALES_PERSON				= "Sales Person";
	public static $ROLE_OPERATOR					= "Operator";
	public static $ROLE_SUPPLIER					= "Supplier";
	public static $ROLE_WAREHOUSE					= "Warehouse";
	public static $ROLE_DATA_ENTRY					= "Data Entry";
	public static $ROLE_FREELANCER					= "Freelancer";
	public static $ROLE_SUPPLIER_SHIPPER			= "Supplier Shipper";
	public static $ROLE_CUSTOMER_SHIPPER			= "Customer Shipper";
	public static $ROLE_CUSTOMER_SERVICE			= "Customer Service";
	public static $ROLE_ALL_AUTHENTICATED_USERS		= "All Authenticated Users";
	public static $ROLE_ALL_VISITORS				= "All Visitors";
    public static $ROLE_RETAILER			        = "Retailer";

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UrlGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'url_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));
	}
}