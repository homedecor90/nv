<?php

class ItemBox extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'item_box';
	}

	public function rules()
	{
		return array(
			array('box_1_height, box_1_width, box_1_length, box_1_weight, box_2_height, box_2_width, box_2_length, box_2_weight, box_3_height, box_3_width, box_3_length, box_3_weight, box_4_height, box_4_width, box_4_length, box_4_weight, box_5_height, box_5_width, box_5_length, box_5_weight', 'numerical'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'box_1_height'=>'Box 1 Height',
			'box_1_width'=>'Box 1 Width',
			'box_1_length'=>'Box 1 Length',
			'box_1_weight'=>'Box 1 Weight',
			'box_2_height'=>'Box 2 Height',
			'box_2_width'=>'Box 2 Width',
			'box_2_length'=>'Box 2 Length',
			'box_2_weight'=>'Box 2 Weight',
			'box_3_height'=>'Box 3 Height',
			'box_3_width'=>'Box 3 Width',
			'box_3_length'=>'Box 3 Length',
			'box_3_weight'=>'Box 3 Weight',
			'box_4_height'=>'Box 4 Height',
			'box_4_width'=>'Box 4 Width',
			'box_4_length'=>'Box 4 Length',
			'box_4_weight'=>'Box 4 Weight',
			'box_5_height'=>'Box 5 Height',
			'box_5_width'=>'Box 5 Width',
			'box_5_length'=>'Box 5 Length',
			'box_5_weight'=>'Box 5 Weight',
		);
	}
}