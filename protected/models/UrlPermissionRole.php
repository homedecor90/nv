<?php

/**
 * This is the model class for table "url_permission_role".
 *
 * The followings are the available columns in table 'url_permission_role':
 * @property string $url_permission_id
 * @property string $role
 */
class UrlPermissionRole extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UrlPermissionRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'url_permission_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url_permission_id, role', 'required'),
			array('url_permission_id', 'length', 'max'=>10),
			array('role', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('url_permission_id, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'url_permission_id' => 'Url Permission',
			'role' => 'Role',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('url_permission_id',$this->url_permission_id,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,			
		));
	}
	
	public static function getPermissionRoleString($url_id){
		$criteriaUrl = new CDbCriteria;
		$criteriaUrl->compare('url_permission_id', $url_id);
		$urlPermissionRoleList = UrlPermissionRole::model()->findAll($criteriaUrl);
		
		$roleStringList = array();
		foreach ($urlPermissionRoleList as $urlPermissionRole){
			$roleStringList[] = $urlPermissionRole->role;
		}
		
		$result = implode(", ", $roleStringList);
		return $result;
	}
	
	public static function updatePermissionRole($arrUrlID, $arrPermissionRoleID){
		$result = array();
		
		foreach ($arrUrlID as $url_id){
			$criteriaUrl = new CDbCriteria;
			$criteriaUrl->compare('url_permission_id', $url_id);
			$urlPermissionRoleList = UrlPermissionRole::model()->deleteAll($criteriaUrl);
		
			foreach ($arrPermissionRoleID as $role_id){
				$role_string = "";
				if ($role_id == "1"){
					$role_string = UrlPermission::$ROLE_ADMIN;
				} else if ($role_id == "2"){
					$role_string = UrlPermission::$ROLE_SALES_PERSON;
				} else if ($role_id == "3"){
					$role_string = UrlPermission::$ROLE_OPERATOR;
				} else if ($role_id == "4"){
					$role_string = UrlPermission::$ROLE_SUPPLIER;
				} else if ($role_id == "5"){
					$role_string = UrlPermission::$ROLE_SUPPLIER_SHIPPER;
				} else if ($role_id == "6"){
					$role_string = UrlPermission::$ROLE_ALL_AUTHENTICATED_USERS;
				} else if ($role_id == "7"){
					$role_string = UrlPermission::$ROLE_ALL_VISITORS;
				}
				
				$newPermissionRole = new UrlPermissionRole();
				$newPermissionRole->url_permission_id = $url_id;
				$newPermissionRole->role = $role_string;
				
				if (!$newPermissionRole->save()){
					$result['message'] = 'fail';
					$result['error'] = CHtml::errorSummary($newPermissionRole);
					echo json_encode($result);
					return;
				}
			}
		}
		
		$result['message'] = 'success';
		echo json_encode($result);
	}
	
	static public function checkUrlPermission($data){
		$objAction = $data['action'];
		$controller_name = $objAction->getController()->getId();
		$action_name = $objAction->getId();
		$action_url = $controller_name."/".$action_name;
		
		$criteriaUrl = new CDbCriteria;
		$criteriaUrl->compare('url', $action_url);
		$urlPermissionList = UrlPermission::model()->findAll($criteriaUrl);
		
		if (count($urlPermissionList) == 0){
			return false;
		}
		
		$urlPermission = $urlPermissionList[0];
		$url_permission_id = $urlPermission->id;
		
		$criteriaUrlPermission = new CDbCriteria;
		$criteriaUrlPermission->compare('url_permission_id', $url_permission_id);
		$urlPermissionRoleList = UrlPermissionRole::model()->findAll($criteriaUrlPermission);
		
		$roleList[] = array();
		foreach ($urlPermissionRoleList as $urlPermissionRole){
			$roleList[] = $urlPermissionRole->role;
		}
		
		if (in_array(UrlPermission::$ROLE_ALL_VISITORS, $roleList)){
			return true;
		}
		
		if (in_array(UrlPermission::$ROLE_ALL_AUTHENTICATED_USERS, $roleList)){
			$user_id = Yii::app()->user->id;
			if ($user_id != 0){
				return true;
			}
		}
		
		if (Yii::app()->user->isAdmin()){
			if (in_array(UrlPermission::$ROLE_ADMIN, $roleList)){
				return true;
			}
		} else if (YumUser::isSalesPerson()){
			if (in_array(UrlPermission::$ROLE_SALES_PERSON, $roleList)){
				return true;
			}
		} else if (YumUser::isOperator()){
			if (in_array(UrlPermission::$ROLE_OPERATOR, $roleList)){
				return true;
			}
		} else if (YumUser::isSupplier(false)){
			if (in_array(UrlPermission::$ROLE_SUPPLIER, $roleList)){
				return true;
			}
		} else if (YumUser::isSupplierShipper(false)){
			if (in_array(UrlPermission::$ROLE_SUPPLIER_SHIPPER, $roleList)){
				return true;
			}
		}
		
		return false;
	}
}