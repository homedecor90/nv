<?php
class PendingCustomer extends CActiveRecord
{
    public $verifyCode;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date',
            )
        );
    }
    
    public function tableName()
	{
		return 'pending_customer';
	}
    
    public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
            // array('name, email, phone, enquiry, date', 'required'),
			array('name, phone', 'length', 'max'=>100),
            array('email', 'email'),
            array('enquiry', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, email, phone, enquiry', 'safe', 'on'=>'search'),
            array(
                'verifyCode',
                'application.extensions.recaptcha.EReCaptchaValidator',
                'privateKey'=>'6LdW4-0SAAAAAC5R4hLkNX5weEyPEls7du-Sixsg',
                'on' => 'externalContact'
            ),
		);
	}
    
    public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
    
    public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,false);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('enquiry',$this->enquiry,true);
		$criteria->compare('date',$this->date,true);
        $criteria->order = 'id DESC';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=>100, 
			),
		));
	}
    
    public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'enquiry' => 'Enquiry',
			'date' => 'Date',
		);
	}
}