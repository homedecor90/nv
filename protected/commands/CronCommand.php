<?php
class CronCommand extends CConsoleCommand{
	
	private $time1;
	private $time2;
	private $bought_together;
	private $items;
	
	/*public function run($args){
		$filename = Yii::getPathOfAlias("application")."/timestamp.txt";
		file_put_contents($filename, time());
		echo $filename."\n";
		echo Yii::getVersion();
	}*/
	
	private function log($message){
		$filename = Yii::getPathOfAlias("application")."/cron.txt";
		$file = fopen($filename, 'a+');
		$date = date('[Y-m-d H:i:s] ');
		fwrite($file, $date.$message."\n");
	}
	
	/**
	 * Updates DB table containing most popular items for the last 30 days
	 */
	public function actionUpdate_most_popular(){
		$days 	= 30;
		$limit 	= 10;
		$time 	= time() - $days * 86400;
		
		Yii::app()->db->createCommand()->truncateTable('item_most_popular');
		
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.sold_date > "'.date('Y-m-d H:i:s', $time).'"');
		$criteria->select 	= 't.item_id, COUNT(t.id) as num';
		$criteria->group 	= 'items.item_main_name';
		$criteria->order 	= 'num DESC';
		$criteria->limit 	= $limit;
		$criteria->with 	= 'items';
		$criteria->together = true;
		$rows = Individualitem::model()->findAll($criteria);
		foreach ($rows as $row){
			$popular = new ItemMostPopular();
			$popular->item_id = $row->item_id;
			$popular->count = $row->num;
			$popular->save();
		}
		
		$this->log('"Most popular" updated');
	}
	
	private function timeStart(){
		$this->time1 = microtime(true);
	}
	
	private function timeStop(){
		$this->time2 = microtime(true);
	}
	
	/**
	 * @return string seconds
	 */
	private function getTime(){
		return round($this->time2 - $this->time1, 3).'s';
	}
	
	/**
	 * Fills the table containing items pairs with qty showing how often these items are bought together 
	 */
	public function actionUpdate_bought_together(){	
		$this->timeStart();
		
		// array of item_id pairs
		$pairs 	= array();
		// qty for every pair
		$qty 	= array();
		// level for every pair
		$pair_level = array();
		
		// TODO: all this algorithm together with item_main_name should be refactored for sure
		$rows = ItemBoughtTogether::model()->findAll();
		foreach ($rows as $row){
			$pairs[] = array($row->item1_id, $row->item2_id);
			$qty[] = 0;
			$pair_level[] = 0;
		}
		
		$criteria = new CDbCriteria();
		$criteria->compare('status', CustomerOrder::$STATUS_SOLD);
		$orders = CustomerOrder::model()->with('customer')->findAll($criteria);
		$customer_ids 		= array();
		$customers 			= array();
		$orders_by_customer = array();
		$items_by_customer 	= array();
		
		foreach ($orders as $order) {
			if (!in_array($order->customer_id, $customer_ids)){
				$customer_ids[] = $order->customer_id;
				$customers[] = $order->customer;
				$orders_by_customer[$order->customer_id][] = $order;
			}
		}
		
		foreach ($orders_by_customer as $customer_id=>$orders){
			foreach ($orders as $order){
				foreach ($order->order_items as $items){
					$items_by_customer[$customer_id][] = $items->item_id;
				}
			}
		}
		
		foreach ($items_by_customer as $customer_id=>$item_ids){
			foreach ($item_ids as $item_id_1){
				foreach ($item_ids as $item_id_2){
					if ($item_id_1 != $item_id_2){
						// second id should be always greater then the first
						$id1 = $item_id_1 < $item_id_2 ? $item_id_1 : $item_id_2;
						$id2 = $item_id_1 > $item_id_2 ? $item_id_1 : $item_id_2;
						$pair = array($id1, $id2);
						$key = array_search($pair, $pairs);
						if ($key === false){
							$pairs[] = $pair;
							$qty[] = 1;
							$pair_level[] = 1;
						} else {
							$qty[$key]++;
							$pair_level[$key] = 1;
						}
					}
				}
			}
		}
		
		$item_ids = array();
		foreach ($pairs as $i=>$pair){
			if (!in_array($pair[0], $item_ids)){
				$item_ids[] = $pair[0];
			}
			if (!in_array($pair[1], $item_ids)){
				$item_ids[] = $pair[1];
			}
		}
		
		// on this step we have all first-level siblings should add up to 4 levels
		for ($level = 2; $level <= 4; $level++){
			// for each new level go through all items
			foreach ($item_ids as $i=>$item_id){
				$new_siblings = array();
				$prev_level_siblings = array();
				// collect siblings (related items) from the previous level
				foreach ($pairs as $i=>$pair){
					if ($pair_level[$i] == $level - 1 && ($pair[0] == $item_id || $pair[1] == $item_id)){
						$prev_level_siblings[] = $pair[0] == $item_id ? $pair[1] : $pair[0];
					}
				}
				
				// for prev level siblings collect their first level siblings
				foreach ($prev_level_siblings as $sibling_id){
					foreach ($pairs as $i=>$pair){
						if ($pair_level[$i] == 1 && ($pair[0] == $sibling_id || $pair[1] == $sibling_id)){
							$new_siblings[] = $pair[0] == $sibling_id ? $pair[1] : $pair[0];
						}
					}
				}
//				if ($item_id == 62){
//					echo $level."\n";
//					print_r($prev_level_siblings);
//					print_r($new_siblings);
//				}
				
				// for each new sibling check if we don't already have it on some other level
				// if not - add a pair
				foreach ($new_siblings as $new_sibling_id){
					if ($new_sibling_id == $item_id){
						continue;
					}
					$id1 = $item_id < $new_sibling_id ? $item_id : $new_sibling_id;
					$id2 = $item_id > $new_sibling_id ? $item_id : $new_sibling_id;
					$pair = array($id1, $id2);
					$key = array_search($pair, $pairs);
					if ($key === false){
						$pairs[] = $pair;
						$qty[] = 1;
						$pair_level[] = $level;
					}
				}
			}
		}
		
		Yii::app()->db->createCommand()->truncateTable('item_bought_together');
		foreach ($pairs as $i=>$pair){
			$row = new ItemBoughtTogether();
			$row->item1_id 	= $pair[0];
			$row->item2_id 	= $pair[1];
			$row->qty 		= $qty[$i];
			$row->level		= $pair_level[$i];
			$row->save();
		}

		$this->timeStop();
		
		$this->log('"Bought together" updated, '.($i+1).' pairs stored ('.$this->getTime().')');
	}
	
	private function render($view, $data){
		$path = Yii::getPathOfAlias('application.views').'/'.$view.'.php';
		return $this->renderFile($path, $data, true);
	}
	
	/**
	 * 
	 * @param int $item_id
	 * @param array $except item_id that shouldn't be returned
	 * @return array
	 */
	private function getBoughtTogether($item_id, $except){
		if (!$this->bought_together){
			$this->bought_together = ItemBoughtTogether::model()->findAll();
			$this->items = Item::model()->findAll();
		}
		
		$related = array();
		
		foreach ($this->bought_together as $row){
			$related_id = 0;
			if ($row->item1_id == $item_id){
				$related_id = $row->item2_id;
			} elseif ($row->item2_id == $item_id) {
				$related_id = $row->item1_id;				
			}
			
			if ($related_id && !in_array($related_id, $except)){
				foreach ($this->items as $item){
					if ($item->id == $related_id){
						$related[] = $item;
						break;
					}
				}
			}
		}
		
		return $related;
	}
	
	public function actionGenerate_newsletter_queue(){
        $this->timeStart();
		$per_batch = 250;
        $per_request = 250;
		$criteria = new CDbCriteria();
		$criteria->compare('completed', 0);
        $criteria->addCondition('start_time < NOW()');
        $criteria->addCondition('NOT ISNULL(start_time)');
		$newsletter = CustomerNewsletter::model()->find($criteria);
		if (!$newsletter){
			$this->log('Generate newsletter queue: no pending newsletter');
			return;
		}
		
		// TODO: think about random order for items in emails and better add randomization to the generate table functions rather then to generate email 
		$criteria = new CDbCriteria();
		$criteria->addCondition('email != ""');
		$criteria->addCondition('t.id>'.$newsletter->last_customer_id);
		$criteria->order = 't.id';
		$criteria->compare('newsletter_unsubscribed',0);
		$criteria->limit = $per_batch;
		$customers = Customer::model()->findAll($criteria);
		if (!count($customers)){
			$newsletter->completed = 1;
			$newsletter->date_completed = date('Y-m-d H:i:s');
			$newsletter->save();
			return;
		}

		$recommended_item_ids = explode(',', $newsletter->recommended_items);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $recommended_item_ids);
        $recommended_items = Item::model()->findAll($criteria);


		$rows = ItemMostPopular::model()->with('item')->findAll();

        $most_popular = array();
		foreach ($rows as $row){
			$most_popular[] = $row->item;
		}
		
		$i = 0;
		$no_related = 0;
		$related = 0;
		$related_by_item_id = array();
		$sql = 'INSERT INTO customer_newsletter_letter (id, newsletter_id, customer_id, email, subject, text, sent, date_sent) VALUES ';
		$letters = array();
		
		foreach ($customers as $customer){
 			if ($customer->newsletter_unsubscribed){
 				continue;
 			}
			// TODO: check if it's fine to treat orders with sold and pending orders the same way
			$subject = 'Great Deals '.trim($customer->first_name).' '.trim($customer->last_name).' | '.date('F Y');
			$email = $customer->email;
			$data['unsubscribe_link'] = $this->getNewsletterUnsubscribeLink($email);
            $data['customer_id'] = $customer->id;
			$orders = $customer->orders;

            $order_ids = array();
            foreach ($orders as $order){
				$order_ids[] = $order->id;
			}

            $data['most_popular_items'] = $most_popular;

            $related_item_names = array();
            $related_items = array();

			if (count($orders)){
				$criteria = new CDbCriteria();
				$criteria->addInCondition('customer_order_id', $order_ids);
				$order_items = CustomerOrderItems::model()->findAll($criteria);
				$item_ids = array();
				foreach ($order_items as $items){
					// got items that customer has in his orders 
					$item_ids[] = $items->item_id;
				}

				foreach ($item_ids as $item_id){
					if (!isset($related_by_item_id[$item_id])){
						$related_by_item_id[$item_id] = $this->getBoughtTogether($item_id, $recommended_item_ids);
					}
				}

				foreach ($item_ids as $item_id){
                    foreach ($related_by_item_id[$item_id] as $related_item){
                        if (!in_array($related_item->item_main_name, $related_item_names)){
							$related_item_names[] = $related_item->item_main_name;
							$related_items[] = $related_item;
						}
					}
				}

				$data['related_items'] = $related_items;
            } else {
                $data['related_items'] = array();
            }
            $data['recommended_items'] = $recommended_items;
            $view = 'customer/email/newsletter';
            if (!count($related_items)){
                $no_related++;
//					$data['items'] = $most_popular;
//					$view = 'customer/email/newsletter_no_orders';
            } else {
                $related++;
            }
            $text = $this->render($view, $data);

//            echo $customer->id.': '.count($related_items)."\n";
            $letters[] = '(NULL,'.$newsletter->id.','.$customer->id.',\''.$email.'\','.Yii::app()->db->quoteValue($subject).','.Yii::app()->db->quoteValue($text).',0,NULL)';
            if (count($letters) == $per_request){
                $req = $sql.implode(',', $letters);
                Yii::app()->db->createCommand($req)->execute();
                $letters = array();
            }
			$i++;
		}
		
		if (count($letters)){
			$sql .= implode(',', $letters);
            Yii::app()->db->createCommand($sql)->execute();
		}
		
		$this->timeStop();
		echo $no_related.' '.$related.' '.$i.' ';
		echo $this->getTime()."\n";
		$newsletter->last_customer_id = $customer->id;
		
		$newsletter->save();
		$this->log('Newsletter queue generated, '.$i.' emails added ('.$this->getTime().')');
	}
	
	private function getNewsletterUnsubscribeLink($email){
		$path = 'https://midmodfurnishings.com/customer/unsubscribe?code='.base64_encode($email).'|'.Customer::getNewsletterUnsubscribeCode($email);
		return $path;
	}
	
	/**
	 * Returns true if current time is "off-peak"
	 * "Off peak times qualify as all day Saturday and Sunday, and 1AM - 8AM Eastern Standard Time, Monday through Friday.
	 */
	private function isTimeOffPeak(){
        $date = new DateTime();
		$date->setTimezone(new DateTimeZone('EST'));
		$day = intval($date->format('N'));
		if ($day == 6 || $day == 7){
			return true;
		}
		
		$hour = intval($date->format('G'));
		if ($hour >= 1 and $hour < 8){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Send a portion of emails from newsletter queue
	 */
	public function actionSend_newsletter() {
		// TODO: don't forget to clean up duplicates
		if (!$this->isTimeOffPeak()){
			return;
		}
		$this->timeStart();
		// how many emails are sent every time
		$per_batch = 88;
		$pause = 0.25; // sec
		$criteria = new CDbCriteria();
		$criteria->compare('sent', '0');
		$criteria->limit = $per_batch;
		$model = CustomerNewsletterLetter::model();
		$rows = $model->findAll($criteria);
		
		if (!count($rows)){
			$this->log('Nothing to send');
			return;
		}
		foreach ($rows as $i=>$row){
			$email 			= Yii::app()->email;
			$email->to 		= $row->email;
//			$email->to 		= 'fun4main+c'.$row->customer_id.'@gmail.com';
			// TODO: check if we do not need another email
			$email->from 	= Customer::NOTIFICATION_EMAIL;
			$email->subject = $row->subject;
			$email->message = $row->text;
			$email->send();
// 			$ids[] = $row->id;
			$row->sent = 1;
			$row->date_sent = date('Y-m-d H:i:s');
			$row->save();
			sleep($pause);
		}
// 		$criteria = new CDbCriteria();
// 		$criteria->addInCondition('id', $ids);
// 		CustomerNewsletter
		$this->timeStop();
		$this->log('Sent '.count($rows).' emails in '.$this->getTime());
	}

	public function actionCreate_new_newsletter(){
		$newsletter = new CustomerNewsletter();
		$newsletter->date_added 		= date('Y-m-d H:i:s');
		$newsletter->completed 			= 0;
		$newsletter->last_customer_id 	= 0;
		$newsletter->save();
		$this->log('New newsletter created');
	}
}