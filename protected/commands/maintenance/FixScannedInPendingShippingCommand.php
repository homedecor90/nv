<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Addicted
 * Date: 10/16/13
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */

class FixScannedInPendingShippingCommand extends CConsoleCommand
{
    public function run()
    {
        $orders = CustomerOrder::model()->with('individual_items')->findAll();
        foreach ($orders as $order) {
            if ($order->shipping_status == 'Pending' and $order->sold_date != '0000-00-00 00:00:00') {
                foreach ($order->individual_items as $item) {
                    $this->changeItemScannedInDate($item, $order->sold_date);
                }
            }
        }
    }

    private function changeItemScannedInDate($item, $soldDate, $days = 30)
    {
        $scannedInDate = date('Y-m-d H:i:s',
            strtotime($soldDate) + $days * 60 * 60 * 24
        );
        $item->saveAttributes(array(
            'scanned_in_date' => $scannedInDate,
        ));
    }
}