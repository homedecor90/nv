<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Addicted
 * Date: 10/11/13
 * Time: 4:36 PM
 * To change this template use File | Settings | File Templates.
 */

class FixScanDatesCommand extends CConsoleCommand
{
    public function run()
    {
        $items = Individualitem::model()->findAll();
        foreach ($items as $item) {
            if (($item->scanned_out_date == '0000-00-00 00:00:00') and
                ($item->status == 'Scanned OUT')) {
                $this->setItemScannedOutDate($item);
            }
            if ($item->scanned_in_date == '0000-00-00 00:00:00') {
                if ($item->scanned_out_date != '0000-00-00 00:00:00') {
                    $time = strtotime($item->scanned_out_date);
                } else $time = time();
                $newScannedIn = strtotime('-1 month', $time);
                $item->saveAttributes(array(
                    'scanned_in_date' => date('Y-m-d H:i:s', $newScannedIn),
                ));
            }
        }
    }

    private function setItemScannedOutDate($item) {
        echo "Setting new scanned out date for item #{$item->id}\n";
        $customerOrderItems = CustomerOrderItems::model()->findByPk($item->customer_order_items_id);
        if (empty($customerOrderItems)) {
            echo "CustomerOrderItems model not found\n";
            return false;
        }
        $customerOrder = CustomerOrder::model()->findByPk($customerOrderItems->customer_order_id);
        if (empty($customerOrder)) {
            echo "CustomerOrder model not found\n";
            return false;
        }
        echo "New date: {$customerOrder->created_on}\n";
        $item->saveAttributes(array(
            'scanned_out_date' => $customerOrder->created_on,
        ));
    }
}