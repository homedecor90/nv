<?php
/**
 * Class FixScannedInDatesCommand
 * Set not existing scanned in dates in individual_items table.
 *
 */
class FixScannedInDatesCommand extends CConsoleCommand
{
    /**
     * Date interval construction format
     */
    const DATE_INVTERVAL_FROM_SUPPLIER_SHIPMENT = 'P3W';

    public function run()
    {
        $items = Individualitem::model()->with('supplier_order')->findAllByAttributes(array(
            'scanned_in_date' => '0000-00-00 00:00:00',
        ));

        foreach ($items as $item) {
            echo "Setting new scanned in date for item ({$item->id}): \n";
            if (!isset($item->supplier_order)) {
                echo "Supplier order not found\n";
                /*
                echo "Set scanned in date to: September 7, 2013\n";
                $item->saveAttributes(array(
                    'scanned_in_date' => date('Y-m-d H:i:s')
                ));
                */
                continue;
            }
            $docs = SupplierOrderOfficialDocs::model()->findByAttributes(array(
                'supplier_order_id' => $item->supplier_order->id,
            ));
            if (!isset($docs)) {
                echo "Supplier order documents not found\n";
                continue;
            }
            $docsUploadedDate = new DateTime($docs->created_on);
            $newDate = $docsUploadedDate->add(
                new DateInterval(self::DATE_INVTERVAL_FROM_SUPPLIER_SHIPMENT));
            echo $newDate->format('Y-m-d H:i:s')."\n";
            $item->saveAttributes(array(
                'scanned_in_date' => $newDate->format('Y-m-d H:i:s')
            ));
        }
    }
}