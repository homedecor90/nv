<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Addicted
 * Date: 10/7/13
 * Time: 12:56 PM
 * To change this template use File | Settings | File Templates.
 */

class RetailerCatalogCommand extends CConsoleCommand
{
    const PDF_FILENAME = 'catalog.pdf';
    const RETAILER_ROLE_NAME = 'Retailer';
    const MAIL_FROM = 'support@regencyshop.com';

    public function run()
    {
        echo "Creating inventory catalog\n";
        $this->createInventoryPdf();
        $mailingList = $this->getRetailerMailingList();
        foreach ($mailingList as $retailer) {
            echo "Sending catalog to {$retailer['email']}\n";
            $this->sendInventorySheet($retailer['email'], $retailer['firstname'], $retailer['lastname']);
        }
    }

    private function createInventoryPdf()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'item_name';
        $data = Item::model()->findAll($criteria);

        $html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderFile(
            Yii::getPathOfAlias('application.views.pdf.retailerInventory') . '.php',
            array('data'=>$data,),
        true));

        $file = Yii::getPathOfAlias('application.runtime') . DIRECTORY_SEPARATOR . self::PDF_FILENAME;
        $html2pdf->Output($file, EYiiPdf::OUTPUT_TO_FILE);
    }

    private function getRetailerMailingList()
    {
        $emails = Yii::app()->db->createCommand()
            ->select('firstname, lastname, email')
            ->from('profile')
            ->join('user_role', 'profile.user_id = user_role.user_id')
            ->join('role', 'user_role.role_id = role.id')
            ->where('role.title = :role', array(':role' => self::RETAILER_ROLE_NAME))
            ->queryAll();
        return $emails;
    }

    private function sendInventorySheet($email, $firstname = null, $lastname = null)
    {
        error_reporting(0);
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;
        $mail->IsSendmail();
        $mail->AddAddress($email);
        $mail->SetFrom(self::MAIL_FROM, 'RegencyShop.com');
        #$mail->SetFrom('somemail@mail.com', 'RegencyShop.com');
        $mail->Subject = AutoMailMessage::MS_RETAILER_INVENTORY;
        $mail->Body = strtr(AutoMailMessage::MC_RETAILER_INVENTORY, array(
            '{name}' => $firstname . ' ' . $lastname,
            '{date}' => date('m-d-Y'),
        ));
        $mail->AddAttachment(Yii::getPathOfAlias('application.runtime') . DIRECTORY_SEPARATOR . self::PDF_FILENAME,
            self::PDF_FILENAME);
        if ($mail->Send()){
            echo "Inventory sent to $email\r\n";
        } else {
            echo "Failed to send mail to $email\r\n";
        }
    }
}