<?php

class m131126_185805_order_shipping_method_sales_tax extends CDbMigration
{
	public function up()
	{
        UrlPermission::model()->updateAll(array(
                'url_group' => '5_23'
            ),'url = "customer/set_order_shipping_method_ajax"'
        );
        UrlPermission::model()->updateAll(array(
                'url_group' => '5_23'
            ),'url = "customer/set_order_add_tax_ajax"'
        );
	}

	public function down()
	{
		echo "m131126_185805_order_shipping_method_sales_tax does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}