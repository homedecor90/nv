<?php

class m140505_152742_customer_order_review_image extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable('customer_order_review_image', array(
            'id' => 'pk',
            'review_id' => 'integer',
            'name' => 'string',
            'filename' => 'string',
            'size' => 'integer',
            'mime' => 'string',
            'create_time' => 'timestamp DEFAULT CURRENT_TIMESTAMP',
        ));

//        $this->addForeignKey(
//            'review_image_review_fk', 'customer_order_review_image',
//            'review_id', 'customer_order_review', 'id', 'cascade', 'cascade'
//        );
	}

	public function safeDown()
	{
		$this->dropTable('customer_order_review_image');
	}
}