<?php

class m140109_201440_dashboard_newsletter_link extends CDbMigration
{
	public function up()
	{
        $this->update('dashblock', array(
            'actions' => "settings/index|Settings\ncustomerNewsletter/index|Newsletter Stats",
        ), 'title="Settings"');
	}

	public function down()
	{
		echo "m140109_201440_dashboard_newsletter_link does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}