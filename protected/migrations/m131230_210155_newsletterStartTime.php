<?php

class m131230_210155_newsletterStartTime extends CDbMigration
{
	public function up()
	{
        $this->addColumn('customer_newsletter', 'start_time', 'DATETIME');
	}

	public function down()
	{
		echo "m131230_210155_newsletterStartTime does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}