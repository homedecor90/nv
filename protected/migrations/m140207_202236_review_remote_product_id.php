<?php

class m140207_202236_review_remote_product_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('customer_order_review', 'remote_product_id', 'INT(11) DEFAULT NULL');
	}

	public function down()
	{
		echo "m140207_202236_review_remote_product_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}