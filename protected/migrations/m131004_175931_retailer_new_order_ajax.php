<?php

class m131004_175931_retailer_new_order_ajax extends CDbMigration
{
	public function up()
	{
        $this->insert('url_permission', array(
            'title' => 'Retailer create order ajax',
            'url' => 'customer/retailer_new_order_ajax',
            'url_group' => 23,
        ));
	}

	public function down()
	{
		echo "m131004_175931_retailer_new_order_ajax does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}