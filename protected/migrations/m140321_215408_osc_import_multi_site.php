<?php

class m140321_215408_osc_import_multi_site extends CDbMigration
{
	public function safeUp()
	{
        $this->addColumn('osc_orders', 'site_id', 'INT(11) NOT NULL AFTER orders_id');
        $this->update('osc_orders', ['site_id' => 1]);
        $this->alterColumn('osc_orders', 'orders_id', 'INT(11)');
        $this->execute('ALTER TABLE osc_orders DROP PRIMARY KEY');
        $this->execute('ALTER TABLE osc_orders ADD PRIMARY KEY (orders_id, site_id)');
    }

	public function down()
	{
		echo "m140321_215408_osc_import_multi_site does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}