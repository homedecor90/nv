<?php

class m140109_193146_customer_newsletter_subscribed_count extends CDbMigration
{
	public function up()
	{
        $this->addColumn('customer_newsletter', 'subscribed', 'INT(10) UNSIGNED AFTER recommended_items');
        $this->addColumn('customer_newsletter', 'unsubscribed', 'INT(10) UNSIGNED AFTER recommended_items');
	}

	public function down()
	{
		echo "m140109_193146_customer_newsletter_subscribed_count does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}