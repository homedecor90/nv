<?php

class m140207_135910_drop_customer_newsletter_unsubscribed extends CDbMigration
{
	public function safeUp()
	{
        $this->dropColumn('customer_newsletter', 'unsubscribed');
	}

	public function down()
	{
		echo "m140207_135910_drop_customer_newsletter_unsubscribed does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}