<?php

class m131015_231115_payment_processor_setting extends CDbMigration
{
	public function up()
	{
        $this->addColumn('settings', 'payment_processor', 'VARCHAR(100)');
	}

	public function down()
	{
        $this->dropColumn('settings', 'payment_processor');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}