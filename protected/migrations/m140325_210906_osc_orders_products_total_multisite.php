<?php

class m140325_210906_osc_orders_products_total_multisite extends CDbMigration
{
	public function up()
	{
        $this->addColumn('osc_orders_total', 'site_id',
            'INT(11) NOT NULL AFTER orders_id');
        $this->update('osc_orders_total', ['site_id' => 1]);
        $this->alterColumn('osc_orders_total',
            'orders_total_id', 'INT(11)');
        $this->execute('ALTER TABLE osc_orders_total DROP PRIMARY KEY');
        $this->execute('ALTER TABLE osc_orders_total ADD PRIMARY KEY (orders_total_id, site_id)');
    }

	public function down()
	{
		echo "m140325_210906_osc_orders_products_total_multisite does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}