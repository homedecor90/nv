<?php

class m140109_173451_customer_order_payment_processor extends CDbMigration
{
	public function up()
	{
        $this->addColumn('customer_order', 'payment_processor', 'VARCHAR(150) DEFAULT NULL');
	}

	public function down()
	{
		echo "m140109_173451_customer_order_payment_processor does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}