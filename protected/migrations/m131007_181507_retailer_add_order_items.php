<?php

class m131007_181507_retailer_add_order_items extends CDbMigration
{
	public function up()
	{
        UrlPermission::model()->updateAll(array(
                'url_group' => '5_23'
            ),'url = "customer/add_order_items_ajax"'
        );
	}

	public function down()
	{
		echo "m131007_181507_retailer_add_order_items does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}