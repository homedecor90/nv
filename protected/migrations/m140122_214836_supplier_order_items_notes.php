<?php

class m140122_214836_supplier_order_items_notes extends CDbMigration
{
	public function up()
	{
        $this->addColumn('supplier_order_items', 'notes', 'VARCHAR(255)');
	}

	public function down()
	{
		$this->dropColumn('supplier_order_items', 'notes');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}