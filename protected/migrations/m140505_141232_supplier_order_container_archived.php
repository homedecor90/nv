<?php

class m140505_141232_supplier_order_container_archived extends CDbMigration
{
	public function up()
	{
        $this->addColumn('supplier_order_container', 'archived', 'boolean DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('supplier_order_container', 'archived');
	}

}