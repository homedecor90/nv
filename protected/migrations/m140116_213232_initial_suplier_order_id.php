<?php

class m140116_213232_initial_suplier_order_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('supplier_order_items', 'initial_order_id', 'INT(10) UNSIGNED DEFAULT NULL');
	}

	public function down()
	{
		echo "m140116_213232_initial_suplier_order_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}