<?php

class m140207_134303_customer_unsubscribe extends CDbMigration
{
	public function up()
	{
        $this->createTable('customer_unsubscribe', array(
            'customer_id' => 'INT(10) UNSIGNED PRIMARY KEY',
            'date' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
        ));

        $this->addForeignKey('customer_unsubscribe_customer_fk', 'customer_unsubscribe',
            'customer_id', 'customer', 'id', 'cascade', 'cascade');
	}

	public function down()
	{
		echo "m140207_134303_customer_unsubscribe does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}