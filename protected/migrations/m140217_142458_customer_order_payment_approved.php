<?php

class m140217_142458_customer_order_payment_approved extends CDbMigration
{
	public function up()
	{
        $this->addColumn('customer_order', 'payment_approved', 'TINYINT(1) DEFAULT 0');
	}

	public function down()
	{
		echo "m140217_142458_customer_order_payment_approved does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}