<?php

class m140325_210847_osc_orders_products_attributes_multisite extends CDbMigration
{
	public function up()
	{
        $this->addColumn('osc_orders_products_attributes', 'site_id',
            'INT(11) NOT NULL AFTER orders_products_attributes_id');
        $this->update('osc_orders_products_attributes', ['site_id' => 1]);
        $this->alterColumn('osc_orders_products_attributes',
            'orders_products_attributes_id', 'INT(11)');
        $this->execute('ALTER TABLE osc_orders_products_attributes DROP PRIMARY KEY');
        $this->execute('ALTER TABLE osc_orders_products_attributes ADD PRIMARY KEY (orders_products_attributes_id, site_id)');
    }

	public function down()
	{
		echo "m140325_210847_osc_orders_products_attributes_multisite does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}