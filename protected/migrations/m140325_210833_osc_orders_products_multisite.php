<?php

class m140325_210833_osc_orders_products_multisite extends CDbMigration
{
	public function up()
	{
        $this->addColumn('osc_orders_products', 'site_id', 'INT(11) NOT NULL AFTER orders_products_id');
        $this->update('osc_orders_products', array('site_id' => 1));
        $this->alterColumn('osc_orders_products', 'orders_products_id', 'INT(11)');
        $this->execute('ALTER TABLE osc_orders_products DROP PRIMARY KEY');
        $this->execute('ALTER TABLE osc_orders_products ADD PRIMARY KEY (orders_products_id, site_id)');
    }

	public function down()
	{
		echo "m140325_210833_osc_orders_products_multisite does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}