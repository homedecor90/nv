<?php

class m140603_164005_customer_newsletter_indexes extends CDbMigration
{
	public function safeUp()
	{
        $this->createIndex('sent', 'customer_newsletter_letter', 'sent');
        $this->addForeignKey(
            'customer_newsletter_letter_newsletter_fk',
            'customer_newsletter_letter', 'newsletter_id',
            'customer_newsletter', 'id', 'cascade', 'cascade'
        );
	}

	public function safeDown()
	{
        $this->dropIndex('sent', 'customer_newsletter_letter');
        $this->dropForeignKey(
            'customer_newsletter_letter_newsletter_fk',
            'customer_newsletter_letter'
        );
	}
}