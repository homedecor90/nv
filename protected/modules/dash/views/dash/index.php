<?php
$this->breadcrumbs=array(
	'Dashboard',
);

if (Yii::app()->user->isAdmin()){
	$this->menu=array(
		array('label'=>'Create Block', 'url'=>array('create')),
		array('label'=>'Manage Blocks', 'url'=>array('admin')),
		array('label'=>'Order Blocks', 'url'=>array('order')),
	);
}
?>

<h1>Dashboard</h1>

<?php
if ($loadingPendingItemsCount > 0
	&& YumUser::isOperator())
{
?>
	<div class="error_loading_pending_messages_div">
		<a href="#" id="collapse_loading_pending_error_message">Expand Loading Pending Items Table</a><br/><br/>
		<div class="error_loading_pending_messages">
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'loading-pending-items-grid',
				'dataProvider'=>$individualLoadingPendingItemModel->searchPendingLoadingItemList(),
				'summaryText'=>'{count} item(s) are not loaded',
				'columns'=>array(
					array(
						'header'=>'Item Name',
						'type'=>'raw',						
						'value'=>'"<div id=\"loading_pending_item_name_".$data->item_id."\">".$data->items->item_name."</div>"',
					),
					array(
						'header'=>'Item Code',
						'type'=>'raw',						
						'value'=>'"<div id=\"loading_pending_item_code_".$data->item_id."\">".$data->items->item_code."</div>"',
					),
					array(
						'header'=>'Color',
						'type'=>'raw',
						'value'=>'"<div id=\"loading_pending_item_color_".$data->item_id."\">".$data->items->color."</div>"',
					),
					array(
						'header'=>'Count',
						'type'=>'raw',
						'value'=>'"<div id=\"loading_pending_item_quantity_".$data->item_id."\">".Individualitem::getLoadingPendingCount($data->id)."</div>"',
					)
				),
			)); ?>
		</div>
	</div>
<?php
}
?>

<?php
if ($waitingItemCount > 0 && Yii::app()->user->isAdmin()){
?>
	<div id="waiting_item_div">
		<a href="#" id="collapse_waiting_item">Expand Items Table Waiting Admin's Approval</a><br/><br/>
		<div id="waiting_item_content">
			<?php
				$columns = array(
						'item_name',
						'item_code',
						'color',
						'exw_cost_price',
						'fob_cost_price',
						'sale_price',
						'description',
						array(
							'class'=>'CButtonColumn',
							'template'=>'{view}{update}',
						),
					);
				$columns_checkbox = array(array(
					'visible'=>'Yii::app()->user->isAdmin()',
					'class'=>'CMyCheckBoxColumn',
					'selectableRows' => 2,
				));

				if (Yii::app()->user->isAdmin()){
					$columns = array_merge($columns_checkbox, $columns);
				}

				$this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'item-grid',
					'dataProvider'=>$itemModel->search_waiting(),
					'filter'=>$itemModel,
					'columns'=>$columns,
				)); 

				if (Yii::app()->user->isAdmin()){
			?>
					<input type="button" id="approve_items" value="Approve Items"/>
					<input type="button" id="delete_items" value="Delete Items"/>
			<?php
				}
			?>
		</div>
	</div>
<?php
}
?>

<?php
if ($waitingSupplierOrderCount > 0 && Yii::app()->user->isAdmin()){
?>
	<div id="waiting_supplier_order_div">
		<a href="#" id="collapse_waiting_supplier_order">Expand Supplier Order Table Waiting Admin's Approval</a><br/><br/>
		<div id="waiting_supplier_order_content">
			<?php
				$columns = array(
					array(
						'header'=>'Supplier',
						'type'=>'raw',
						'value'=>'SupplierOrder::model()->getSupplierName($data->id)',
					),
					array(
						'header'=>'Items',
						'type'=>'raw',
						'value'=>'SupplierOrder::model()->getSupplierOrderItems($data->id)',
					),
					array(
						'header'=>'Total EXW Price',
						'type'=>'raw',
						'value'=>'SupplierOrder::model()->getSupplierOrderTotalExwPrice($data->id)',
					),
					array(
						'header'=>'Total Fob Price',
						'type'=>'raw',
						'value'=>'SupplierOrder::model()->getSupplierOrderTotalFobPrice($data->id)',
					),
					array(
						'header'=>'Total CBM',
						'type'=>'raw',
						'value'=>'SupplierOrder::model()->getSupplierOrderTotalCBM($data->id)',
					),
				);

				$columns_checkbox = array(array(
					'visible'=>'Yii::app()->user->isAdmin()',
					'class'=>'CMyCheckBoxColumn',
					'selectableRows' => 2,
				));

				if (Yii::app()->user->isAdmin()){
					$columns = array_merge($columns_checkbox, $columns);
				}

				$this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'supplier-order-grid',
					'dataProvider'=>$supplierOrderModel->search_waiting(),	
					'columns'=>$columns
					)
				);

				if (Yii::app()->user->isAdmin()){
			?>
					<input type="button" id="approve_orders" value="Approve Orders"/>
					<input type="button" id="delete_orders" value="Delete Orders"/>
			<?php
				}
			?>
		</div>
	</div>
<?php
}
?>
	
<?php
if ($bankInfoCount > 0 && Yii::app()->user->isAdmin()){
?>
    <div id="bank_info_div">
		<a href="#" id="collapse_bank_info_table">Expand Bank Info Table Waiting Approval</a><br/><br/>
		<div id="bank_info_content">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'bank_info_grid',
				'dataProvider'=>$bankInfoModel->searchNotApproved(),
				// 'summaryText'=>'{count} item(s) are not loaded',
				'columns'=>array(
					array(
						'header'=>'Supplier',
						'type'=>'raw',
						'value'=>'$data->user->profile->firstname." ".$data->user->profile->lastname',
					),
					'account_name',
                    'account_address',
                    'account_number',
                    'routing_number',
                    'bank_name',
                    'bank_address',
                    array(
                        'header'=>'Action',
                        'type'=>'raw',
                        'value'=>'\'<input type="button" value="Accept" class="accept_bank_info" rel="\'.$data->id.\'" /><br />
                                    <input type="button" value="Deny" class="deny_bank_info" rel="\'.$data->id.\'" />\''
                    )
				),
			)); ?>
        </div>
	</div>
<?php
}

if (YumUser::model()->isSalesPerson(true)){
?>
<h2>Customer follow-ups for today</h2>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'folowup_grid',
	'dataProvider'=>$customerFollowupModel->searchToday(),
	// 'summaryText'=>'{count} item(s) are not loaded',
	'columns'=>array(
		array(
			'header'=>'Customer',
			'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/customer/\'.$data->customer_id.\'">\'.$data->customer->first_name.\' \'.$data->customer->last_name.\'</a>\'',
			'type'=>'raw',
		),
		array(
			'header'=>'Followup time',
			'value'=>'date("M. d, Y H:i:s", strtotime($data->followup_time))',
			'type'=>'raw',
		),
		'status',
	),
));

}
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="sendOrderDiv" class="fancy_box_div">
		<div id="supplier_list_div" class="active">
			<div>
				<?php
					echo CHtml::label("Supplier", "supplier_id");
					echo CHtml::dropDownList('supplier_id', '', $supplierList);
				?>
			</div>
			<div id="supplier_order_items_list_div">
				
			</div>
			<div>
				<input type="button" value="Next" id="select_supplier" disabled="disabled" class="disabled" />
			</div>
		</div>
		<div id="supplier_order_list_div">
			<div id="supplier_order_list_content_div" class="active">
			</div>
			<input type="button" value="Back" id="back_to_select_supplier"/>
			<input type="button" value="Create New Order" id="create_new_order"/>
		</div>
	</div>
</div>

<?php 

// newsletter reminder and create form
if (Yii::app()->user->isAdmin() && $show_newsletter_form){
	echo '<div id="create_newsletter_form">';
	echo '<h2>Create a newsletter</h2>';
	echo '<strong>Select items to be used as "recommended to you"</strong><br />';
	echo CHtml::dropDownList('newsletter_item_id_1', '', CHtml::listData($all_items, 'id', 'item_name'));
	echo '<br />';
	echo CHtml::dropDownList('newsletter_item_id_2', '', CHtml::listData($all_items, 'id', 'item_name'));
	echo '<br />';
	echo CHtml::dropDownList('newsletter_item_id_3', '', CHtml::listData($all_items, 'id', 'item_name'));
	echo '<br /><br />';
	echo '<input type="button" value="Create" id="create_newsletter" />';
	echo '</div>';
}

?>

<?php
if (!YumUser::isSupplierShipper(false) && !YumUser::isCustomerShipper() && !YumUser::isDataEntry() && !YumUser::isRetailer()):
?>
<h3>My Messages</h3>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'activity-grid',
	'dataProvider'=>$activityModel->search_my_messages(),
	'columns'=>array(
		array(
			'header'=>'Date',				
			'value'=>'date("M. d, Y H:i:s", strtotime($data->date))',
		),
		array(
			'header'=>'Sender',
			'value'=>'YumUser::model()->getUserName($data->sender_id)',
		),
		'message',
	),
)); 

elseif(YumUser::isSupplierShipper(false)):
echo '<h3>Order Containers</h3>';
// show container table for supplier shipper instead of messages
$containerModel = new SupplierOrderContainer('search');
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-container-by-shipper-grid',
	// 'dataProvider'=>$containerModel->searchAvailable(),
	'dataProvider'=>$containerModel->search(),
	'columns'=>array(
		'id',
		array(
			'header'=>'Total CBM',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalCBM($data->id)',
			'type'=>'raw',
		),
        array(
            'header'=>'Number of Suppliers',
            'value'=>'$data->getSupplierCount()',
            'type'=>'raw',
        ),
        array(
            'header'=>'Supplier Address',
            'type'=>'raw',
            'value'=>'$data->getSupplierAddressStr()',
        ),
		array(
			'header'=>'Action',
			// 'value'=>'SupplierOrderContainer::model()->getActionButtonStringByShipper($data->id)',
			// 'value'=>'\'<input type="button" class="shipper_bid_button" value="Submit Quote" rel="\'.$data->id.\'" />\'',
			'value'=>'SupplierOrderContainer::model()->getActionButtonStringByShipper($data->id)',
			'type'=>'raw',
		),
	),
));

endif;
?>

<?php $this->widget('zii.widgets.CListView', array(
  'id' => 'dashboard',
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
  'summaryText'=>FALSE,
)); ?>

<?php
if (Yii::app()->getModule('dash')->useMasonry) {
  $js = <<<JS
  jQuery('#dashboard').masonry({
    itemSelector : '.dashboard-block',
    columnWidth : 250
  });
JS;

  Yii::app()->clientScript->registerScriptFile(Yii::app()->getModule('dash')->assetsFolder .'/js/jquery.masonry.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScript('dash-masonry', $js, CClientScript::POS_END);
}

echo CancellationReason::getCancellationReasonCode();
?>

<script>
var selItemList = "";
var selQuantityList = "";
var selCustomColorList = "";
var selStatusList = "";
var selSupplierId = 0;

jQuery(document).ready(function(){

	jQuery('#create_newsletter').click(function(e){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/create_newsletter_ajax/";
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: {
				items:[
					jQuery('#newsletter_item_id_1').val(),
					jQuery('#newsletter_item_id_2').val(),
					jQuery('#newsletter_item_id_3').val(),
				]
			},
			type: 'post',
			success:function(result){
				alert('Newsletter created successfully');
				jQuery('#create_newsletter_form').remove();
			}
		});
	});
	
	jQuery("#collapse_error_message").click(function(){
		if (jQuery(".error_messages").hasClass("active")){
			jQuery(".error_messages").removeClass("active");
			jQuery(this).html("Expand Order Table");
		} else {
			jQuery(".error_messages").addClass("active");
			jQuery(this).html("Collapse Order Table");
		}
		return false;
	});
	
	jQuery("#collapse_waiting_item").click(function(){
		if (jQuery("#waiting_item_content").hasClass("active")){
			jQuery("#waiting_item_content").removeClass("active");
			jQuery(this).html("Expand Items Table Waiting Admin's Approval");
		} else {
			jQuery("#waiting_item_content").addClass("active");
			jQuery(this).html("Collapse Items Table Waiting Admin's Approval");
		}
		return false;
	});
	
	jQuery("#collapse_waiting_supplier_order").click(function(){
		if (jQuery("#waiting_supplier_order_content").hasClass("active")){
			jQuery("#waiting_supplier_order_content").removeClass("active");
			jQuery(this).html("Expand Supplier Order Table Waiting Admin's Approval");
		} else {
			jQuery("#waiting_supplier_order_content").addClass("active");
			jQuery(this).html("Collapse Supplier Order Table Waiting Admin's Approval");
		}
		return false;
	});
	
	jQuery("#collapse_bank_info_table").click(function(){
		if (jQuery("#bank_info_content").hasClass("active")){
			jQuery("#bank_info_content").removeClass("active");
			jQuery(this).html("Expand Bank Info Table Waiting Approval");
		} else {
			jQuery("#bank_info_content").addClass("active");
			jQuery(this).html("Collapse Bank Info Table Waiting Approval");
		}
		return false;
	});
    
	jQuery("#collapse_loading_pending_error_message").click(function(){
		if (jQuery(".error_loading_pending_messages").hasClass("active")){
			jQuery(".error_loading_pending_messages").removeClass("active");
			jQuery(this).html("Expand Loading Pending Items Table");
		} else {
			jQuery(".error_loading_pending_messages").addClass("active");
			jQuery(this).html("Collapse Loading Pending Items Table");
		}
		return false;
	});
	

	jQuery('.search-button').click(function(){
		jQuery('.search-form').toggle();
		return false;
	});

	jQuery("#send_order").click(function(){
		selItemList = "";
		var htmlOrderItems = '<table><thead>'
					+ '<th>Item Name</th>'
					+ '<th>Item Code</th>'
					+ '<th>Item Color</th>'
					+ '<th>Quantity</th>'
					+ '<th>Custom Color</th>'
                    + '<th>Status</th></thead><tbody>';
		var htmlOrderItemsBody = "";
		
        var not_filled = [];
		var combined_ids = [];
		var combined_ids_str = [];
		jQuery("#not-ordered-items-grid td.checkbox-column input:checked").each(function(){
			if (jQuery(this).attr("id") == "individual-item-grid_c0_all"){
				return;
			};
			
			if (selItemList != ""){
				selItemList += "_";
			}
			var sel_item_combined_id = jQuery(this).val();
			combined_ids.push(sel_item_combined_id);
			combined_ids_str.push('cid[]='+sel_item_combined_id);
            var sel_item_id = sel_item_combined_id.split('_')[0];
            // check if item fields are filled
            if (parseInt(jQuery('#filled_'+sel_item_id).val()) == 0){
                not_filled.push(jQuery(".item_name_" + sel_item_id).eq(0).html());
                return;
            }
            var sel_item_status = sel_item_combined_id.split('_')[1];
            // console.log(jQuery("#item_qty_" + sel_item_combined_id).html());
            // console.log("#item_qty_" + sel_item_combined_id);
			selItemList += sel_item_id;
			htmlOrderItemsBody += '<tr>'
				+ '<td>' + jQuery(".item_name_" + sel_item_id).eq(0).html() + '</td>'
				+ '<td>' + jQuery(".item_code_" + sel_item_id).eq(0).html() + '</td>'
				+ '<td>' + jQuery(".item_color_" + sel_item_id).eq(0).html() + '</td>'
				+ '<td><input type="text" class="item_quantity" data-status="'+sel_item_status+'" rel="' + sel_item_id + '" id="item_quantity_' + sel_item_id + '" value="' + jQuery("#item_qty_" + sel_item_combined_id).html() + '"></input></td>'
				+ '<td><input type="text" class="item_custom_color" id="item_custom_color_' + sel_item_id + '_' + sel_item_status + '" value=""></input></td>'
                + '<td>'+(sel_item_status=='S' ? 'Sold' : 'Not Sold')+'</td></tr>'
				;
		});
		
        if (not_filled.length){
            alert('Please fill all the fields in item info before adding it to the order: '+not_filled.join(', '));
            return false;
        }
        
		if (htmlOrderItemsBody == ""){
			htmlOrderItemsBody = '<tr><td colspan="4" style="font-style:italic; text-align:center">No Result</td></tr>'
		}
		htmlOrderItems += htmlOrderItemsBody;
		htmlOrderItems += "</tbody></table>";
		
		if (selItemList == ""){
			alert("Please input the item to send order");
			return false;
		}
		
		jQuery("#sendOrderButton").trigger("click");
		jQuery("#fancybox-content").height(480);
		jQuery("#supplier_order_list_div").removeClass("active");
		jQuery("#supplier_list_div").addClass("active");
		jQuery("#supplier_order_items_list_div").html(htmlOrderItems);
		
		// we send id_status pairs to the server
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/get_custom_color_list_ajax/?";
		url += combined_ids_str.join('&');
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (typeof(result.status)=='undefined' || result.status != 'success'){
					alert('Error getting custom colors list');
					return;
				}
				for (i in result.colors){
					jQuery('#item_custom_color_'+combined_ids[i]).val(result.colors[i]);
					// console.log(combined_ids[i]);
				}
				
				jQuery('#select_supplier').removeAttr('disabled');
				jQuery('#select_supplier').removeClass('disabled');
			}
		});
	});
	
	jQuery("#back_to_select_supplier").live("click", function(){
		//jQuery("#fancybox-content").height(480);
		jQuery("#supplier_order_list_div").removeClass("active");
		jQuery("#supplier_list_div").addClass("active");
	});
	
	jQuery("#select_supplier").live("click", function(){
		var selTempItemList = "";
		var selTempQuantityList = "";
		var selTempCustomColorList = "";
		var selTempStatusList = "";
		
		jQuery(".item_quantity").each(function(){
			var sel_item_id	= jQuery(this).attr("rel");
			var sel_item_qty = jQuery(this).val();
			var sel_item_status = jQuery(this).attr('data-status');
			
			if (selTempItemList != ""){
				selTempItemList += "_";
			}
			selTempItemList += sel_item_id;
			
			if (selTempQuantityList != ""){
				selTempQuantityList += "_";
			}
			selTempQuantityList += sel_item_qty;
			
			if (selTempStatusList != ""){
				selTempStatusList += "_";
			}
			selTempStatusList += sel_item_status;
            
			if (selTempCustomColorList != ""){
				selTempCustomColorList += "_";
			}
			selTempCustomColorList += jQuery('#item_custom_color_'+sel_item_id+'_'+sel_item_status).val();
            
			if (sel_item_qty.trim() == "" || isNaN(sel_item_qty)){
				alert("Please input the exact quantity");
				return false;
			}
		});
		
		selItemList = selTempItemList;
		selQuantityList = selTempQuantityList;
		selCustomColorList = selTempCustomColorList;
		selStatusList = selTempStatusList;
		
		selSupplierId = jQuery("#supplier_id").val();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/get_pending_order_for_supplier_ajax/?"
			+ "supplier_id=" + selSupplierId;

		jQuery.ajax({
			url:url,
			success:function(result){
				jQuery("#fancybox-content").height(500);
				jQuery("#fancybox-content>div").height(480);				
				jQuery("#supplier_order_list_div").addClass("active");
				jQuery("#supplier_list_div").removeClass("active");
				jQuery("#supplier_order_list_content_div").html(result);
			}
		});
	});
	
	jQuery(".choose_order").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		sendOrder(order_id);
	});
	
	jQuery("#create_new_order").live("click", function(){
		sendOrder(0);
	});
	
	jQuery("#approve_items").click(function(){
		if (jQuery("#item-grid td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the item to approve");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("#item-grid td.checkbox-column input[type='checkbox']:checked").each(function(){
			var item_id = jQuery(this).val();
			if (id_list == ""){
				id_list = item_id;
			} else {
				id_list += ("_" + item_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/approve_items/?item_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.waitingItemCount == 0){
					jQuery("#waiting_item_div").remove();
				}
				jQuery.fn.yiiGridView.update('item-grid');
			}
		});
	});
	
    jQuery("#delete_items").click(function(){
		if (jQuery("#item-grid td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the item to delete");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("#item-grid td.checkbox-column input[type='checkbox']:checked").each(function(){
			var item_id = jQuery(this).val();
			if (id_list == ""){
				id_list = item_id;
			} else {
				id_list += ("_" + item_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/delete_waiting_items/?item_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			success:function(result){
                if (result.status == 'fail'){
                    alert(result.message);
                } else if (result.status == 'success'){
                    if (result.waitingItemCount == 0){
                        jQuery("#waiting_item_div").remove();
                    } else {
                        jQuery.fn.yiiGridView.update('item-grid');
                    }
                }
			},
            dataType:"json"
		});
	});
    
	jQuery("#approve_orders").click(function(){
		if (jQuery("#supplier-order-grid td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the order to approve");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("#supplier-order-grid td.checkbox-column input[type='checkbox']:checked").each(function(){
			var order_id = jQuery(this).val();
			if (id_list == ""){
				id_list = order_id;
			} else {
				id_list += ("_" + order_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/approve_orders/?order_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.waitingSupplierOrderCount == 0){
					jQuery("#waiting_supplier_order_div").remove();
				}
				
				jQuery.fn.yiiGridView.update('supplier-order-grid');
			}
		});
	});
    
    jQuery("#delete_orders").click(function(){
		if (jQuery("#supplier-order-grid td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the order to delete");
			return false;
		}
		
		var id_list = "";
		
		jQuery("#supplier-order-grid td.checkbox-column input[type='checkbox']:checked").each(function(){
			var order_id = jQuery(this).val();
			if (id_list == ""){
				id_list = order_id;
			} else {
				id_list += ("_" + order_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/delete_pending_approval_orders/?order_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.status == 'fail'){
                    alert(result.message);
                    return;
                }
                
                if (result.waitingSupplierOrderCount == 0){
					jQuery("#waiting_supplier_order_div").remove();
				}
				
				jQuery.fn.yiiGridView.update('supplier-order-grid');
			}
		});
	});
});

function sendOrder(order_id){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/send_order_items_by_admin_ajax/?"
			+ "supplier_id=" + selSupplierId + "&order_id=" + order_id + "&item_list=" + selItemList + "&item_quantity_list=" + selQuantityList + "&item_status_list=" + selStatusList + '&item_custom_color_list=' + selCustomColorList;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			jQuery.fancybox.close();
			jQuery.fn.yiiGridView.update('not-ordered-items-grid');
			
			if (result.notOrderedItemCount == 0){
				jQuery(".error_messages_div").remove();
			}
		}
	});
}

<?php
if (YumUser::isSupplierShipper(false)):
?>

jQuery('.shipper_bid_button').live('click', function(e){
    var container_id = jQuery(e.target).attr('rel');
    window.location.href = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/container_detail_by_shipper/?container_id='+container_id;
});

<?php
endif;
?>

jQuery('#delete_button').click(function(e){
    
    var arr = [];
    jQuery("#not-ordered-items-grid td.checkbox-column input:checked").each(function(){
        if (jQuery(this).attr("id") == "individual-item-grid_c0_all"){
            return;
        }
        arr.push(jQuery(this).val());        
    });
    
    if (!arr.length){
        alert('Plese select rows to delete');
        return;
    } else if (!confirm('Are you sure?')){
        return false;
    }
    
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/delete_not_ordered_items_by_admin_ajax/?"
			+ "&item_list=" + arr.join('|');
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == 'fail'){
                alert(result.message);
            }
			jQuery.fn.yiiGridView.update('not-ordered-items-grid');
			
			if (result.notOrderedItemCount == 0){
				jQuery(".error_messages_div").remove();
			}
		}
	});
});

jQuery('.view_detail').live("click", function(){
    var container_id = jQuery(this).attr("rel");
    
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/container_detail_by_shipper/?container_id=" + container_id;
    window.location.href = url;
});

jQuery('.accept_bank_info').live('click', function(e){
    var id = jQuery(e.target).attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/approve_supplier_bank_info_ajax/?row_id=" + id;
		
    jQuery.ajax({
        url:url,
        dataType:"json",
        success:function(result){
            jQuery.fn.yiiGridView.update('bank_info_grid');
        }
    });
});

jQuery('.deny_bank_info').live('click', function(e){
    var id = jQuery(e.target).attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_supplier_bank_info_ajax/?row_id=" + id;
		
    jQuery.ajax({
        url:url,
        dataType:"json",
        success:function(result){
            jQuery.fn.yiiGridView.update('bank_info_grid');
        }
    });
});

jQuery('.expand_sold').live('click', function(e){
    var target  = jQuery(e.target);
    var status  = target.attr('data-status');
    var item_id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/sold_items_details_ajax/?item_id=" + item_id + '&status=' + status;
    
    jQuery.ajax({
        url:url,
        dataType:"json",
        success:function(result){
            if (result.status == "fail"){
                alert(result.message);
                return;
            }
            
            if (result.status == "success"){
                jQuery("#sold_items_list_content").html(result.text);
                jQuery("#sold_items_list_button").trigger("click");
                //$.fn.yiiGridView.update('inventory-item-grid');
            }
        }
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('.cancel_item').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/individualitem/cancel_from_customer_order_ajax/?ind_item_id='+id;
	cancelShippingWindow(url, function(){$.fn.yiiGridView.update('not-ordered-items-grid');});
	
	e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

</script>
