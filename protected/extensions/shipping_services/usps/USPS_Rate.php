<?php

//$rate = USPSParcelRate($address,3,array(10,20,30),array("10x15x6","11x11x12","10x10x10"));

function USPSParcelRate($dest_address,$package_count,$weight,$dimension) {

$userName = '722DESIG4331'; // Your USPS Username

$url = "http://production.shippingapis.com/ShippingAPI.dll";
$ch = curl_init();

// set the target url
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

// parameters to post
curl_setopt($ch, CURLOPT_POST, 1);

$packages = "";

for($i =0 ;$i < $package_count ; $i++)
{
 
	$dims = explode("x",$dimension[$i]);
	$girth = 2*($dims[1]+$dims[2]);
	
	$size = "REGULAR";
	
	//LARGE: Any package dimension is larger than 12 - usps docs
	for($j=0;$j<3;$j++)
	{
		if(floatval($dims[$j]) > 12){
			$size = "LARGE";
		}
	}
	
	$packages .= "<Package ID=\"$i\">    
	<Service>PRIORITY COMMERCIAL</Service>
    <ZipOrigination>".ShippingCompany::$sender_address['zip']."</ZipOrigination>
    <ZipDestination>".$dest_address['zip']."</ZipDestination>
    <Pounds>$weight[$i]</Pounds>
    <Ounces>0</Ounces>
    <Container></Container>
    <Size>$size</Size>
    <Width>$dims[1]</Width>
    <Length>$dims[0]</Length>
    <Height>$dims[2]</Height>
    <Girth>$girth</Girth>
	</Package>\n";
}
$data = "API=RateV4&XML=<RateV4Request USERID=\"$userName\"> <Revision/>" . $packages . "</RateV4Request>";

// send the POST values to USPS
curl_setopt($ch, CURLOPT_POSTFIELDS,$data);

//echo $data;

$result=curl_exec ($ch);
curl_close($ch);

$data = strstr($result, '<?');

//echo '<!-- '. $data. ' -->'; // Uncomment to show XML in comments
$xml = new SimpleXMLElement($data);

$rate = 0.0;
$return_array = array();
$msg = "";

for($i =0 ;$i < $package_count ; $i++)
{
	if(isset($xml->Package[$i]->Error) && isset($xml->Package[$i]->Error->Description))
	{
		$msg = $xml->Package[$i]->Error->Description;
		break;
	}
	
	if(isset($xml->Package[$i]->Postage[0]->CommercialRate))
		$rate = $rate + floatval($xml->Package[$i]->Postage[0]->CommercialRate);
	//else
	  //return false;
		
}

if(strlen($msg) > 0)
{
	$return_array = array(
		'rate' => false, 
		'message' => $msg
	);
}
else
{
	$return_array = array(
		'rate' => $rate, 
		'message' => ''
	);
}
return $return_array;

 //echo '<pre>'; print_r($params); echo'</pre>'; // Uncomment to see xml tags
 
}
?>