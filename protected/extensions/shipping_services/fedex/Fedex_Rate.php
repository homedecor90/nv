<?php


require_once('fedex-common.php5');

//$rate =  getFedexShippingRate(2,array(12,10),array("20x20x21","22x50x2"));


function addShipper(){
	$address = ShippingCompany::$sender_address;
	$shipper = array(
		'Contact' => array(
			'PersonName' => $address['name'],
			'CompanyName' => $address['company'],
			'PhoneNumber' => $address['phone']),
		'Address' => array(
			'StreetLines' => $address['street'],
			'City' => $address['city'],
			'StateOrProvinceCode' => $address['state'],
			'PostalCode' => $address['zip'],
			'CountryCode' => 'US')
	);
	return $shipper;
}
function addRecipient($address){
	$recipient = array(
		'Contact' => array(
			'PersonName' => $address['name'],
			'CompanyName' => '',
			'PhoneNumber' => $address['phone']),
		'Address' => array(
			'StreetLines' => $address['street'],
			'City' => $address['city'],
			'StateOrProvinceCode' => $address['state'],
			'PostalCode' => $address['zip'],
			'CountryCode' => 'US',
			'Residential' => $address['residental'])
	);
	
	return $recipient;	                                    
}
function addShippingChargesPayment(){
	$shippingChargesPayment = array(
		'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
		'Payor' => array(
			'ResponsibleParty' => array(
			'AccountNumber' => getProperty('billaccount'),
			'CountryCode' => 'US')
		)
	);
	return $shippingChargesPayment;
}
function addLabelSpecification(){
	$labelSpecification = array(
		'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
		'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
		'LabelStockType' => 'PAPER_7X4.75');
	return $labelSpecification;
}
function addSpecialServices(){
	$specialServices = array(
		'SpecialServiceTypes' => array('COD'),
		'CodDetail' => array(
			'CodCollectionAmount' => array('Currency' => 'USD', 'Amount' => 150),
			'CollectionType' => 'ANY')// ANY, GUARANTEED_FUNDS
	);
	return $specialServices; 
}
function addPackageLineItems($package_count,$weight_array,$dim_array){
	
	$packageLineItem = array();
	
	for($i=0 ; $i < $package_count ; $i++)
	{
		$dims = explode("x",$dim_array[$i]);
		
		$packageLineItem += array(
			'SequenceNumber'=>$i+1,
			'GroupPackageCount'=>$package_count,
			'Weight' => array(
				'Value' => $weight_array[$i],
				'Units' => 'LB'
			),
			'Dimensions' => array(
				'Length' => $dims[0],
				'Width' => $dims[1],
				'Height' => $dims[2],
				'Units' => 'IN'
			)
		);
	}
	return $packageLineItem;
}

function getFedexShippingRate($address, $package_count,$weight_array,$dim_array, $service = 'FEDEX_GROUND')
{
	$newline = "<br />";
	//The WSDL is not included with the sample code.
	//Please include and reference in $path_to_wsdl variable.

	//$path_to_wsdl = Yii::getPathOfAlias('webroot') ."/fedex/RateService_v13.wsdl";
	// TODO: why can't I use relative file path?
	$path_to_wsdl = Yii::app()->getBaseUrl(true)."/fedex/RateService_v13.wsdl";
	
	ini_set("soap.wsdl_cache_enabled", "0");
	
	$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

	$request['WebAuthenticationDetail'] = array(
		'UserCredential' =>array(
			'Key' => getProperty('key'), 
			'Password' => getProperty('password')
		)
	); 
	$request['ClientDetail'] = array(
		'AccountNumber' => getProperty('shipaccount'), 
		'MeterNumber' => getProperty('meter')
	);
	$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request v13 using PHP ***');
	$request['Version'] = array(
		'ServiceId' => 'crs', 
		'Major' => '13', 
		'Intermediate' => '0', 
		'Minor' => '0'
	);
	$request['ReturnTransitAndCommit'] = true;
	$request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
	$request['RequestedShipment']['ShipTimestamp'] = date('c');
	$request['RequestedShipment']['ServiceType'] = $service;// valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	$request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	$request['RequestedShipment']['TotalInsuredValue']=array('Ammount'=>100,'Currency'=>'USD');
	$request['RequestedShipment']['Shipper'] = addShipper();
	$request['RequestedShipment']['Recipient'] = addRecipient($address);
	$request['RequestedShipment']['ShippingChargesPayment'] = addShippingChargesPayment();
	$request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
	$request['RequestedShipment']['RateRequestTypes'] = 'LIST'; 
	$request['RequestedShipment']['PackageCount'] = $package_count;
	$request['RequestedShipment']['RequestedPackageLineItems'] = addPackageLineItems($package_count,$weight_array,$dim_array);

	try 
	{
	
		if(setEndpoint('changeEndpoint'))
		{
			$newLocation = $client->__setLocation(setEndpoint('endpoint'));
		}
		
		$response = $client ->getRates($request);
		$return_array = array();
		
		if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR')
		{  	
			$rateReply = $response -> RateReplyDetails;
			$amount =  number_format($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount,2,".",",");
			
			$return_array = array(
				'rate' => $amount, 
				'message' => ''
			);
		}
		else
		{
			if(count($response -> Notifications) > 1)
				$msg = $response -> Notifications[0] -> Message;
			else
				$msg = $msg = $response -> Notifications -> Message;
			
			$return_array = array(
				'rate' => false, 
				'message' => $msg
			);
		}

		return $return_array;
	} 
	catch (SoapFault $exception) 
	{
	   return $return_array = array(
				'rate' => false, 
				'message' => 'Exception occured'
			);
	}
	
}

?>