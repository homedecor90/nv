<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl(true). '/js/common.js', CClientScript::POS_HEAD); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script>
		var global_baseurl = '<?php echo Yii::app()->getBaseUrl(true); ?>';
	</script>
</head>

<body>

<div class="container" id="page">
	<div id="header">
		<div id="logo"><a href="/"><?php echo CHtml::encode(Yii::app()->name); ?></a></div>
		<?php
			if(YumUser::isSalesPerson() || YumUser::isWarehouse() || YumUser::isOperator()){
				echo '<div class="customer_search">';
				echo '<label for="customer_name">Find Customer</label>&nbsp;';
				$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
					'name'=>'customer_name',
					'value'=>isset($_GET['search_str']) ? trim($_GET['search_str']) : '',
					'source'=>Yii::app()->getBaseUrl(true)."/index.php/customer/get_customers_names_ajax",
					'options'=>array(
						'select'=>"js:function(event, ui) {
										window.location.href = '".Yii::app()->getBaseUrl(true)."/index.php/customer/find_customers_results?search_str=' + ui.item.value;
		                          }",
						'minLength'=>'2',
					)
				));
				echo '</div>';
			}
		?>
	</div><!-- header -->
	
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <!-- script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script-->

	<div id="mainmenu">
		<?php 
			Yii::import('application.modules.dash.models.DashBlock');
			$items = DashBlock::getMenuList();
			$items[] = array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest);
			$this->widget('zii.widgets.CMenu',array(
// 			'items'=>array(
// 				array('label'=>'Dashboard', 'url'=>array('/dash/dash')),
// 				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
// 				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			'items'=>$items
			)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
			echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
		}
		
	?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->
<?php
//$this->widget('dash.components.DashMenuWidget');
?>
<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>
