<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
        
        if (isset($this->stats)){
            // TODO: make it as a normal widget if needed
            echo '<div class="portlet">';
            echo '<div class="portlet-decoration"><div class="portlet-title">Stats</div></div>';
            echo $this->stats;
            echo '</div>';
        }
		
		if (isset($this->profit)){
            // TODO: make it as a normal widget if needed
            echo '<div class="portlet">';
            echo '<div class="portlet-decoration"><div class="portlet-title">Profit</div></div>';
            echo $this->profit;
            echo '</div>';
        }
        
        if (isset($this->to_order)){
        	// TODO: make it as a normal widget if needed
        	echo '<div class="portlet">';
        	echo '<div class="portlet-decoration"><div class="portlet-title">To Order</div></div>';
        	echo $this->to_order;
        	echo '</div>';
        }
        
        if (isset($this->not_ordered)){
        	// TODO: make it as a normal widget if needed
        	echo '<div class="portlet">';
        	echo '<div class="portlet-decoration"><div class="portlet-title">Sold Not Ordered</div></div>';
        	echo $this->not_ordered;
        	echo '</div>';
        }
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>

<?php
if (YumUser::model()->isSalesPerson(true)){
?>

<div id="dialog-confirm" title="Followup" style="display:none">Follow up with <a href="#">Sasha Romanov</a></div>

<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script-->

<script>
var customerFollowupList;
var bDialogOpen = false;
jQuery(document).ready(function(){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/get_today_followup_list_ajax";

	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			customerFollowupList = result;
		}
	});
	
	setInterval(function(){showCustomerFollowup()}, 5000);
});

function showCustomerFollowup(){
	if (bDialogOpen == true)
		return false;
	
	jQuery.each(customerFollowupList, function(index, item){
		var customer_id = item.customer_id;
		var customer_name = item.customer_name;
		var followup_time = new Date(item.followup_time * 1000).getTime() / 1000;
		var followup_status = item.status;
		
		var bAlert = false;
		var bDismiss = false;
		var dialogButtons = {};
		
		var current_time = Math.round(new Date().getTime() / 1000);
		var bDiffTime = followup_time - current_time;
		
		var action = "";
		
		if (followup_status == "Followed"){
			if (bDiffTime < 15 * 60){
				var urlAlerted = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/alert_cusomter_followup/?followup_id=" + index;
				item.status = "Alerted";
				jQuery.ajax({url:urlAlerted});
				
				bAlert = true;
				dialogButtons = {
					"Snooze": function() {
						jQuery(this).dialog('close');
					},
					"Dismiss": function() {				
						bDismiss = true;
						jQuery(this).dialog('close');
					}
				};
			}
		} else if (followup_status == "Alerted"){
			if (bDiffTime < 0){
				bAlert = true;
				
				dialogButtons = {
					"Snooze": function() {
						jQuery(this).dialog('close');
					},
                    "Ok": function() {
						bDismiss = true;
						jQuery(this).dialog('close');
					}
				};
			}
		} else {
			bAlert = false;
		}
		
		if (bAlert){
			var strHtml = 'Followup with <a href="<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/'
				+ customer_id + '">' + customer_name + '</a>';
			
			jQuery("#dialog-confirm").html(strHtml);
			bDialogOpen = true;
			jQuery("#dialog-confirm").dialog({
				resizable: false,
				height:150,
				modal: true,
				buttons: dialogButtons,
				close:function(event, ui){
					if (bDismiss){
						var urlDismiss = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/dismiss_cusomter_followup/?followup_id=" + index;
								
						jQuery.ajax({
							url:urlDismiss,
							success:function(){
								delete customerFollowupList[index];
								jQuery("#dialog-confirm").dialog('close');
							}
						});
					}
								
					bDialogOpen = false;
				}
			});
			
			return false;
		}
	});
}
</script>
<?php
}
?>