<?php
if (!$shipment && !$order):
	echo '<strong>Payment Not Found</strong>';
elseif($shipment->additional_payment_done || $order->shipping_edited_payment_done):
	echo '<strong>Payment successful</strong>';
else:
	if (isset($transaction_success) && $transaction_success == false){
		echo '<strong>ERROR: '.$error_message.'</strong><br /><br />';
	}
?>
	<form id="checkout_form" method="POST">
		<?php if ($se): ?>
		<h4>Payment amount: <?php echo '$'.number_format($amount, 2); ?></h4>
		<?php else: ?>
		<h4>"Beyond Point" Adjustment: <?php echo '$'.number_format($amount, 2); ?></h4>
		<?php endif; ?>
		<h3>Credit Card</h3>
		<div class="field_list active">
			<input type="hidden" readonly="readonly" id="amount" name="amount" value="<?php echo $amount;?>">
			
			<div class="field_row">
				<div class="field_label">Credit Card Number:</div>
				<div class="field_content"><input type="text" name="authorizenet_cc_number"></div>
			</div>
			<div class="field_row">
				<div class="field_label">Credit Card Expiry Date:</div>		
				<div class="field_content">
					<select style="width:125px" name="authorizenet_cc_expires_month">
						<option value="01">January</option><option value="02">February</option>
						<option value="03">March</option><option value="04">April</option>
						<option value="05">May</option><option value="06">June</option>
						<option value="07">July</option><option value="08">August</option>
						<option value="09">September</option><option value="10">October</option>
						<option value="11">November</option><option value="12">December</option>
					</select>&nbsp;
					<select name="authorizenet_cc_expires_year">
						<option value="13">2013</option>
						<option value="14">2014</option><option value="15">2015</option>
						<option value="16">2016</option><option value="17">2017</option>
						<option value="18">2018</option><option value="19">2019</option>
						<option value="20">2020</option><option value="21">2021</option>
					</select>
				</div>
			</div>
			<div class="field_row">
				<div class="field_label">Credit Card Type:</div>
				
				<div class="field_content">
					<select name="credit_card_type">
						<option value="Amex">Amex</option>
						<option value="Discover">Discover</option>
						<option value="Mastercard">Mastercard</option>
						<option value="Visa">Visa</option>
					</select>
				</div>		
			</div>
		</div>
		<?php
		if (!$billing_info->billing_name):
		?>
		<h3>Billing Address</h3>
		<div class="field_list active">
			<div class="field_row">
				<div class="field_label">Name:</div>
				<div class="field_content"><input name="fname" value="<?php echo $customerModel->first_name; ?>" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
            <div class="field_row">
				<div class="field_label">Last Name:</div>
				<div class="field_content"><input name="lname" type="text" value="<?php echo $customerModel->last_name; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Street Address:</div>
				<div class="field_content"><input name="street_address" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">City:</div>
				<div class="field_content"><input name="city" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">State/Province:</div>
				<div class="field_content">
                    <input type="text" name="state" />&nbsp;
					<span class="inputRequirement">*</span>
				</div>
			</div>
			<div class="field_row">
				<div class="field_label">Zip Code:</div>
				<div class="field_content"><input name="postcode" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Telephone Number:</div>
				<div class="field_content"><input name="telephone" type="text" value="<?php echo $customerModel->phone; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">E-Mail Address:</div>
				<div class="field_content"><input name="email_address" type="text" value="<?php echo $customerModel->email; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
		</div>
		<?php
		endif;
		?>
		<input type="submit" id="btnCheckOut" value="Checkout"/>
	</form>
<?php
endif;
?>

<script>
jQuery(document).ready(function(){
	jQuery("#btnCheckOut").click(function(){
		bVerify = true;
		
		jQuery('form#checkout_form .field_list.active .field_content input[type="text"]').each(function(){			
			if (jQuery(this).val().trim() == ""){
				jQuery(this).focus();
				bVerify = false;
				return false;
			}
		});		
		
		if (bVerify == false){
			alert("Please input the required value");
			return false;
		}
	});
});
</script>
