<?php

?>







<h1>Create order</h1>



<?php $this->widget('zii.widgets.CDetailView', array(

	'data'=>$model,

	'attributes'=>array(

		'id',

		'first_name',

		'last_name',

		'phone',

		array(

            'label'=>'email',

            'type'=>'raw',

            'value'=>'<a href="mailto:'.$model->email.'">'.$model->email.'</a>'

        ),

        array(

            'label'=>'Created On',

            'value'=> (strtotime($model->created_on)>0 ? date("M. d, Y h:i:s A", strtotime($model->created_on)) : '')

        ),

		'heard_through',

	),

)); ?>

<br/>

<?php

$viewData = array(

	"customerNoteModel"=>$customerNoteModel,

	"customerFollowupModel"=>$customerFollowupModel,

	"customerOrderModel"=>$customerOrderModel,

	"newCustomerNoteModel"=>$newCustomerNoteModel,

	"newCustomerFollowupModel"=>$newCustomerFollowupModel,

	"newCustomerOrderModel"=>$newCustomerOrderModel,	

	"salesPersonList"=>$salesPersonList,

	"itemCodeList"=>$itemCodeList,

	"itemNameList"=>$itemNameList,	

	"newItemModel"=>$newItemModel,

	"sales_tax_percent"=>$sales_tax_percent,

	"model"=>$model,

);



$Tabs = array(

	'order'=>array('title'=>'Order','view'=>"_retailerOrder"),

//	'note'=>array('title'=>'Note','view'=>"_note"),
//
//	'followup'=>array('title'=>'Followup','view'=>"_followup"),

);



$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));



?>