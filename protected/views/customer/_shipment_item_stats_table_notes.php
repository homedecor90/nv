<table class="shipment_table">
	<tr>
		<th>Item Name</th>
		<th>Item Code</th>
		<th>Qty</th>
		<th>Status</th>
	</tr>
	<?php
	foreach ($items as $rows):
	?>
	<tr>
		<td><?php echo $rows[0]->items->item_name; ?></td>
		<td><?php echo $rows[0]->items->item_code; ?></td>
		<td><?php echo count($rows); ?></td>
		<td><span class="ind_item <?php echo $rows[0]->getShortStatus(); ?>">
					<?php echo $rows[0]->getStatus(); ?>
		</span></td>
	</tr>
	<?php
	endforeach;
	?>
</table>