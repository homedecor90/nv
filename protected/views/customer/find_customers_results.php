<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Create Customer', 'url'=>array('create')),
);

?>

<h1>Results Find Customers</h1>
<?php
	/*if(YumUser::isWarehouse() || YumUser::isOperator()){
		echo '<div class="customer_search">';
		echo '<label for="customer_name">Find Customer</label>&nbsp;';
		$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
			'name'=>'customer_name',
			'value'=>$search_str,
			'source'=>Yii::app()->getBaseUrl(true)."/index.php/customer/get_customers_names_ajax",
			'options'=>array(
				'select'=>"js:function(event, ui) {
								window.location.href = '".Yii::app()->getBaseUrl(true)."/index.php/customer/find_customers_results?search_str=' + ui.item.value;
                          }",
				'minLength'=>'2',
			)
		));
		echo '</div>';
	}*/
?>

<?php 
		

		$strItems = '<div class="custom-grid-view">
			<table class="items" >'
			.'<thead>'
			.'<th >ID</th>'
			.'<th >Customer Name</th>'
			.'<th >Blling Name</th>'
			.'<th >Shipping Name</th>'
			.'<th >Details</th>';
        
        $strItems .= '</thead>';
		$class="even";
		foreach ($find_customers as $customer){
			if ($class=="odd"){
				$class="even";
			} else {
				$class="odd";
			}
			$strItems .= "<tr class=\"$class\">";
			$strItems .= "<td class=\"row_id\">".$customer['id']."</td>";
			$strItems .= "<td >".$customer['customer_name']."</td>";
			$strItems .= "<td >";
				if(!empty($customer['biling_name'])){
					foreach ($customer['biling_name'] as $biling_name){
						$strItems .= $biling_name."; <br />";
					}
				}
			$strItems .= "</td>";
			$strItems .= "<td >";
				if(!empty($customer['shipping_name'])){
					foreach ($customer['shipping_name'] as $shipping_name){
						$strItems .= $shipping_name."; <br />";
					}
				}
			$strItems .= "</td>";
			$strItems .= "<td > email: ".$customer['email']." <br /> phone: ".$customer['phone']." </td>";
			$strItems .= "</tr>";
		}

		$strItems .= "</table></div>";
		echo $strItems;
?>

<script type="text/javascript">
jQuery.fn.highlight = function(pat) {
 function innerHighlight(node, pat) {
  var skip = 0;
  if (node.nodeType == 3) {
   var pos = node.data.toUpperCase().indexOf(pat);
   if (pos >= 0) {
    var spannode = document.createElement('span');
    spannode.className = 'highlight';
    var middlebit = node.splitText(pos);
    var endbit = middlebit.splitText(pat.length);
    var middleclone = middlebit.cloneNode(true);
    spannode.appendChild(middleclone);
    middlebit.parentNode.replaceChild(spannode, middlebit);
    skip = 1;
   }
  }
  else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
   for (var i = 0; i < node.childNodes.length; ++i) {
    i += innerHighlight(node.childNodes[i], pat);
   }
  }
  return skip;
 }
 return this.each(function() {
  innerHighlight(this, pat.toUpperCase());
 });
};

jQuery('.custom-grid-view table tr td').live('click', function(e){
    var target = jQuery(e.target);
    // alert(target[0].tagName);
    if (target[0].tagName.toLowerCase() == 'td' || target[0].tagName.toLowerCase() == 'span'){
        if (target[0].tagName.toLowerCase() == 'span'){
            target = target.parent();
        }
        var id = target.parent().find('.row_id').text();
        window.location.href = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/'+id;
    }
});
jQuery(document).ready(function(){
	$('div.custom-grid-view').highlight('<?php echo addslashes($search_str);?>');
});
</script>

