<h3>Set your shipment pickup date</h3>


<?php
if ($shipment->pickup_date == '0000-00-00 00:00:00'):
	$shipment->pickup_date = '';
?>
<p>
	Select any date and time between 9am and 4pm, Monday-Friday
</p>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($shipment); ?>

	<div class="row">
		<?php echo $form->labelEx($shipment,'pickup_date'); ?>
		<?php
			Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
			$this->widget('CJuiDateTimePicker',array(
				'model'=>$shipment,
				'attribute'=>'pickup_date',
				'mode'=>'datetime', //use "time","date" or "datetime" (default)
				'language'=>'en-GB',
				'options'=>array(
					"dateFormat"=>"M. dd, yy",
					"timeFormat"=>"hh:mm tt",
					"ampm"=>true
				),
				'htmlOptions' => array(
					'id' => 'pickup_date'
				)
			));
		?>
		<?php echo $form->error($shipment,'pickup_date'); ?>
	</div>
	
		<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>
    
<?php $this->endWidget(); ?>

<?php
else:
?>
<p>
	Pickup date has been set
</p>
<?php
endif;
?>