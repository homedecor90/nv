<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Create Customer', 'url'=>array('create')),
	array('label'=>'Import osCommerce Orders', 'url'=>array('import_osc_orders')),
);

if (Yii::app()->user->isAdmin()){
	$this->stats = $this->renderPartial('_stats', array('stats'=>$stats), true);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Customers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
$this->widget('CGridViewExt', array(
	'id'=>'customer-grid',
	'dataProvider'=>$model->search($order_status),
	'filter'=>$model,
	'columns'=>array(
		array(
            'header'=>'ID',
            'name'=>'id',
            'type'=>'raw',
            'value'=>'\'<span class="row_id">\'.$data->id.\'</span>\'',
        ),
		'first_name',
		'last_name',
		'phone',
		array(
            'header'=>'Email',
            'name'=>'email',
			'type'=>'raw',
            'value'=>'\'<a href="mailto:\'.$data->email.\'">\'.$data->email.\'</a>\'',
        ),
		'heard_through',
        array(
			'header'=>'Created On',				
			'value'=>'strtotime($data->created_on)>0 ? date("M. d, Y h:i:s A", strtotime($data->created_on)) : \'\'',
		),
        array(
            'header'=>'Last Order',
            'type'=>'raw',
   	    'filter'=>CHtml::dropDownList("Customer[last_order]",$order_status,CustomerOrder::getOrderStatus()),
            // 'value'=>'$data->getLastOrder()->status'
            'value'=>'isset($data->last_order)? $data->last_order->status : ""'
        ),
        array(
            'header'=>'Last Folowup',
            'type'=>'raw',
            'value'=>'($data->getLastFollowupTime()? \'<span style="color: #\'.($data->getLastFollowupTime()<time()?\'ff0086;\':\'00a86b;\').\'">\'.date(\'M. d, Y h:i:s A\', $data->getLastFollowupTime()).\'</span>\' : \'\')'
        ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
)); ?>
<script type="text/javascript">
jQuery('#customer-grid tr td').live('click', function(e){
    var target = jQuery(e.target);
    // alert(target[0].tagName);
    if (target[0].tagName.toLowerCase() == 'td' || target[0].tagName.toLowerCase() == 'span'){
        if (target[0].tagName.toLowerCase() == 'span'){
            target = target.parent();
        }
        var id = target.parent().find('.row_id').text();
        window.location.href = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/'+id;
    }
});
</script>
