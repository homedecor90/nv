<h2>Add Third Party Shipment</h2>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-order-form',
	'enableAjaxValidation'=>false,
)); ?>
	<strong>Customer Info:</strong><br />
	<div id="third_party_customer_info">
	<?php
		echo CHtml::label('Email', 'customer_email', array('class'=>'left_label'));
		echo CHtml::textField('Customer[email]','', array('id'=>'customer_email'));
		
		echo CHtml::label('Name', 'customer_name');
		echo CHtml::textField('Customer[name]','', array('id'=>'customer_name'));
		
		echo CHtml::label('Phone', 'customer_phone');
		echo CHtml::textField('Customer[phone]','', array('id'=>'customer_phone'));
		echo '<br />';
		
		echo CHtml::label('Street', 'customer_street', array('class'=>'left_label'));
		echo CHtml::textField('Customer[street]','', array('id'=>'customer_street'));
		
		echo CHtml::label('City', 'customer_city');
		echo CHtml::textField('Customer[city]','', array('id'=>'customer_city'));
		
		echo CHtml::label('State', 'customer_state');
		echo CHtml::textField('Customer[state]','', array('id'=>'customer_state'));
		echo '<br />';
		echo CHtml::label('Zip', 'customer_zip', array('class'=>'left_label'));
		echo CHtml::textField('Customer[zip_code]','', array('id'=>'customer_zip'));
	?>
	</div>

	<strong>Order Info:</strong><br />
	<?php
		echo CHtml::label('Sold On', 'sold_on');
	?>
	<select name="sold_on" id="sold_on">
		<option value="Ebay">Ebay</option>
		<option value="Amazon">Amazon</option>
		<option value="Other">Other</option>
	</select>

	<?php
	$this->renderPartial('../customer/_add_order', array(
		'newCustomerOrderModel'=>$newCustomerOrderModel, 
		'form'=>$form,
		'itemNameList'=>$itemNameList,
		'sales_tax_percent'=>$sales_tax_percent
	));
	?>

	<div class="row buttons">
		<?php echo CHtml::Button('Add Items', array('id'=>'add_existing_item')); ?>
		<?php echo CHtml::Button('Submit', array('id'=>'submit_order')); ?>	
		<?php echo CHtml::Button('Reset', array('id'=>'reset_items')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
if (typeof(init_local) == 'undefined' || !init_local){
	jQuery("#submit_order").click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/add_3rd_party_shipment_ajax";
		var custom_discounted = parseFloat(jQuery('#custom_discounted').val());
		if (custom_discounted && jQuery('#discounted').val() == 'Discounted'){
			jQuery('#param_discounted_total').val(custom_discounted);
			jQuery('#CustomerOrder_custom_discount').val(1);
			jQuery('#CustomerOrder_raw_discounted').val(custom_discounted);
		}
		var postData = jQuery("form#customer-order-form").serialize();
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.status != "success"){
					alert(result.message);
					return;
				}
				
				jQuery.fancybox.close();
				if (result.message){
					alert(result.message);
				} else {
					jQuery.fn.yiiGridView.update('customer-sold-order-grid');
				}
				jQuery("table#order_item_list tbody tr").remove();
				item_count = 0;
				item_number = 0;
				changeOrderItemList();
				calculateTotal();
				jQuery('#custom_discounted').val('');
				jQuery('#CustomerOrder_custom_discount').val(0);
				jQuery('#CustomerOrder_raw_discounted').val(0);
			}
		});
	});
	
	init_local = true;
}
</script>
