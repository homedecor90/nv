<div class="portlet-content customer_stats">
    <h5>Last 30 days</h5>
    <?php
        echo '<strong>Customers added</strong>: '.$stats['last_30_days']['customers'].'<br />';
        echo '<strong>Sold</strong>: '.$stats['last_30_days']['sold']['number'].' / $'.number_format($stats['last_30_days']['sold']['total'],2).'<br />';
        echo '<strong>Pending</strong>: '.$stats['last_30_days']['pending']['number'].' / $'.number_format($stats['last_30_days']['pending']['total'],2).'<br />';
        echo '<strong>Lost</strong>: '.$stats['last_30_days']['lost']['number'].' / $'.number_format($stats['last_30_days']['lost']['total'],2).'<br />';
    ?>
    <br />
    <h5>All time</h5>
    <?php
        echo '<strong>Customers added</strong>: '.$stats['all_time']['customers'].'<br />';
        echo '<strong>Sold</strong>: '.$stats['all_time']['sold']['number'].' / $'.number_format($stats['all_time']['sold']['total'],2).'<br />';
        echo '<strong>Pending</strong>: '.$stats['all_time']['pending']['number'].' / $'.number_format($stats['all_time']['pending']['total'],2).'<br />';
        echo '<strong>Lost</strong>: '.$stats['all_time']['lost']['number'].' / $'.number_format($stats['all_time']['lost']['total'],2).'<br />';
    ?>
</div>