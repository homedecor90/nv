<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	"View Customer Order"
);*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Create Customer', 'url'=>array('create')),
	array('label'=>'Update Customer', 'url'=>array('update', 'id'=>$customerOrderModel->customer_id)),
	array('label'=>'View Customer', 'url'=>array('view', 'id'=>$customerOrderModel->customer_id)),
);
?>

<h1>View Customer Order #<?php echo $customerOrderModel->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$customerOrderModel,
	'attributes'=>array(
		'id',
		array(
			'label'=>'Sales Person',
			'value'=>$salesPersonName,
		),
		array(
			'label'=>'Customer',
			'value'=>$customerName,
		),
		'status',
	),
)); 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-items-grid',
	'dataProvider'=>$customerOrderItemsModel->search($customerOrderModel->id),
	'filter'=>$customerOrderItemsModel,
	'columns'=>array(
		array(
			'header'=>'Item Code',
			'value'=>'$data->items->item_code',
			'filter'=>CHtml::activeTextField($customerOrderItemsModel, 'item_code'),
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
			'filter'=>CHtml::activeTextField($customerOrderItemsModel, 'color'),
		),
		'sale_price',
		'quantity',
	),
));

?>
