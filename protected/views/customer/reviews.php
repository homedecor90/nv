<?php
/**
 * @var Controller @controller
 */
?>
<h1>Customer Reviews</h1>
<?php $this->widget('ext.fancybox.EFancyBox', array(
    'target' => '.fancybox',
));?>
<?php
$controller = $this;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-reviews-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'customer_order_id',
		array(
			'header'=>'Customer',
            'type'=>'raw',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_order->customer_id.\'">\'.$data->customer_order->customer->first_name.\' \'.$data->customer_order->customer->last_name.\'</a>\'',
		),
		array(
            'header' => 'Date',
            'type' => 'raw',
            'value' => 'date("M. d, Y", strtotime($data->date))'
        ),
		array(
			'header' => 'Overall',
			'type' => 'raw',
			'value' => '$data->overall.\'/5<br />\'.CustomerOrderReview::$overall_list[$data->overall]'
		),
		array(
			'header' => 'Cost',
			'type' => 'raw',
			'value' => '$data->cost.\'/5<br />\'.CustomerOrderReview::$cost_list[$data->cost]'
		),
		array(
			'header' => 'Future Shopping',
			'type' => 'raw',
			'value' => '$data->future_shopping.\'/5<br />\'.CustomerOrderReview::$future_shopping_list[$data->future_shopping]'
		),
		array(
			'header' => 'Shipping',
			'type' => 'raw',
			'value' => '$data->shipping.\'/5<br />\'.CustomerOrderReview::$shipping_list[$data->shipping]'
		),
		array(
			'header' => 'Customer Service',
			'type' => 'raw',
			'value' => '$data->customer_service.\'/5<br />\'.CustomerOrderReview::$customer_service_list[$data->customer_service]'
		),
		array(
			'header' => 'Return',
			'type' => 'raw',
			'value' => '$data->return.\'/5<br />\'.CustomerOrderReview::$return_list[$data->return]'
		),
		'review_text',
        array(
            'header' => 'Images',
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                return $controller->renderPartial('_reviewImages', array(
                    'data' => $data
                ), true);
            }
        ),
        array(
            'header' => 'Post review',
            'name' => 'Post review',
            'type' => 'raw',
            'value' => function($data, $row) use ($controller) {
                    return $controller->renderPartial('_reviewPostForm', array(
                        'data' => $data
                    ), true);
            },
            //'value' => 'Yii::app()->getController()->renderPartial(\'_reviewPostForm\', array(\'data\' => $data, true)'$this->renderPartial('_reviewPostForm', array('data' => $data), true),
        ),
        array(
            'class'=>'CButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'url' =>'array("/customer/editReview", "id" => $data->id)',
                    'options' => array(
                        'class' => 'editReview',
                    ),
                ),
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("customer/deleteReview", array("id" => $data->id))',
                ),
            ),
        )
	),
)); ?>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'.editReview',
    'config'=>array(
        'width'=>400,
        'height'=>320,
        'autoDimensions'=>false,
    ),
));
?>
<script type="text/javascript">
    $(document).on('submit', '.post-review-form', function(e){
        e.preventDefault();
        if ($(this).find('[name="products_id"]').val() == 0) {
            alert('Please, select product');
            return false;
        }
        $.post($(this).attr('action'), $(this).serialize(), function(resp){
            alert(resp.message);
        }, 'json');
    });
</script>
<script type="text/javascript">
    $(document).on('submit', '.updateReview', function(e){
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(resp){
            jQuery.fancybox.close();
            $.fn.yiiGridView.update('customer-reviews-grid');
            alert(resp.message);
        }, 'json');
    });
</script>