<?
/**
 * @var CustomerController $this
 * @var PendingCustomer $model
 * @var CActiveForm $form
 */
?>
<style>
    .main {
        font-family: Tahoma;
        color: rgb(109, 109, 109);
        font-size: 12px;
    }
</style>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'contact-form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        //'action' => $this->createAbsoluteUrl('/customer/contact'),
        'focus'=>array($model,'name'),
    )); ?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
                <td><?php echo $form->labelEx($model,'name', array('class' => 'main')); ?></td>
                <td>
                    <?php echo $form->textField($model, 'name', array('style' => 'width:200px;')); ?>
                    <?php echo $form->error($model,'name'); ?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'email', array('class' => 'main')); ?></td>
                <td>
                    <?php echo $form->textField($model,'email', array('style' => 'width:200px;')); ?>
                    <?php echo $form->error($model,'email'); ?>
                </td>
            </tr>
            <tr><td><?php echo $form->labelEx($model,'phone', array('class' => 'main')); ?></td>
                <td>
                    <?php echo $form->textField($model,'phone', array('style' => 'width:200px;')); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </td>
            </tr>
            <tr><td><?php echo $form->labelEx($model,'enquiry', array('class' => 'main')); ?></td>
                <td>
                    <?php echo $form->textArea($model,'enquiry', array('rows' => '5', 'cols' => '50')); ?>
                    <?php echo $form->error($model,'enquiry'); ?>
                </td></tr>
            <tr><td colspan="2">
                        <?php $this->widget('application.extensions.recaptcha.EReCaptcha', array(
                            'model'=>$model,
                            'attribute'=>'verifyCode',
                            'theme'=>'red',
                            'language'=>'en',
                            'publicKey'=>'6LdW4-0SAAAAAAG2v33kVhhjz66dO6VzO6M4_PPk',
                        )); ?>
                </td></tr>
            <tr><td>&nbsp;</td>
                <td>
                    <?php echo CHtml::imageButton('/images/button_images/button_continue.gif');?>
                </td>
            </tr>
        </table>
    <?php $this->endWidget();?>
</div>