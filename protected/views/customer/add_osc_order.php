<?php
/**
 * @var $form CActiveForm
*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Create Customer', 'url'=>array('create')),
	array('label'=>'Import osCommerce Orders', 'url'=>array('import_osc_orders')),
);

?>

<h1>Add osCommerce Order</h1>

<h3>Customer Information</h3>

<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$oscOrderModel,
	'attributes'=>array(
		'customers_name',
		'customers_telephone',
		'customers_email_address',
	),
));
?>

<h3>Customer Order Items</h3>

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-grid',
	'dataProvider'=>$oscOrderItemsModel->search(),
	'columns'=>array(
		'products_name',
		'products_price',
		'final_price',
		'products_tax',
		'products_quantity',
		array(
			'header'=>'Product Attributes',
			'type'=>'raw',
			'value'=>'OscOrdersProducts::model()->getProductAttirbutes($data->orders_products_id)',
		),
	),
));
?>

<h4>Total:<?php echo $orderTotal;?></h4>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#add_new_customer',
    'config'=>array(
		'width'=>500,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="add_new_customer_box" class="fancy_box_div">
		<?php $this->renderPartial('add_new_customer',array(
			"oscOrderModel"=>$oscOrderModel,
			"newCustomerModel"=>$newCustomerModel,
			"heardThroughList"=>$heardThroughList,
		)); ?>
	</div>
</div>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#add_new_item',
    'config'=>array(
		'width'=>500,
		'height'=>450,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="add_item_box" class="fancy_box_div">
		<?php $this->renderPartial('add_new_item',array(
			"newItemModel"=>$newItemModel,
		)); ?>
	</div>
</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-order-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<?php echo $form->hiddenField($newCustomerOrderModel, 'sales_person_id', array('value'=>0));?>
	<?php echo $form->hiddenField($newCustomerOrderModel, 'customer_id', array('value'=>0));?>
	<?php echo $form->hiddenField($newCustomerOrderModel, 'discounted', array('value'=>'Not Discounted'));?>
	
	<?php /*
    <div class="row" style="margin:20px 0;">
		<?php echo CHtml::Button('Add New Customer', array('id'=>'add_new_customer', "href"=>"#add_new_customer_box")); ?>
		<?php echo $form->error($newCustomerOrderModel,'customer_id'); ?>
	</div>
	*/?>
	<div class="row" style="margin:20px 0;">
        <span>
		<?php echo $form->labelEx($newCustomerOrderModel,'shipping_method'); ?>
		<?php
			$shippingMethodList = array(
				'Shipping'=>'Shipping',
				'Local Pickup'=>'Local Pickup',
				'Shipped from Los Angeles'=>'Shipped from Los Angeles',
				'Canada Shipping'=>'Canada Shipping',
			);
		?>
		<?php echo $form->dropDownList($newCustomerOrderModel, 'shipping_method', $shippingMethodList, array('id'=>'shipping_method'));?>
		<?php echo $form->error($newCustomerOrderModel,'shipping_method'); ?>
        </span>&nbsp;&nbsp;
        <span>
            <? echo $form->labelEx($newCustomerOrderModel, 'payment_processor');?>
            <?php echo $form->dropDownList(
                $newCustomerOrderModel,
                'payment_processor',
                array_combine(
                    array_keys(Yii::app()->params['paymentProcessors']),
                    array_keys(Yii::app()->params['paymentProcessors'])
                ),
                array('prompt' => 'Select processor used')
            );?>
            <?php echo $form->error($newCustomerOrderModel,'payment_processor'); ?>
        </span>
	</div>
	
	<table id="order_item_list">
		<thead>
			<th class="item_name">Item Name</th>
			<th class="color">Color</th>
			<th class="quantity">Qty</th>
			<th class="item_eta">E.T.A</th>
			<th class="item_price">Price</th>
			<th class="special_shipping_price">Special Shipping</th>
			<th class="custom_color">Custom Color</th>
			<th class="total_price">Total Price</th>
			<th class="action">Action</th>
		</thead>
	</table>

	<div class="customer_order_info">
		<div class="row grand_total_price">Grand Total: $<span>0</span></div>
		<div class="row discounted_total">Discounted Total: $<span>0</span></div>
		<div class="row total_discount">Total Discount: $<span>0</span></div>
		
		<?php echo $form->hiddenField($newCustomerOrderModel, 'grand_total_price', array("id"=>"param_grand_total_price", 'value'=>0));?>
		<?php echo $form->hiddenField($newCustomerOrderModel, 'discounted_total', array("id"=>"param_discounted_total", 'value'=>0));?>
		
		<div id="total_price_same"></div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::Button('Add Items', array('id'=>'add_existing_item')); ?>
		<?php echo CHtml::Button('Add New Items', array('id'=>'add_new_item', "href"=>"#add_item_box")); ?>
		<?php echo CHtml::Button('Calculate Total', array('id'=>'calculate_total')); ?>	
		<?php echo CHtml::Button('Reset', array('id'=>'reset_items')); ?>
	</div>
	
<?php $this->endWidget(); ?>

<script>
var item_count = 0;
var item_number = 0;
var strNameList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_name', '', $itemNameList['itemNameList']));?>';
var strColorList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_color', '', $itemNameList['itemColorList']));?>';
var strQuantity = '<?php echo str_replace("\n", "", CHtml::textField('quantity', '1'));?>';
var strCustomColor = '<?php echo str_replace("\n", "", CHtml::textField('custom_color', ''));?>';
var strSpecialShippingPrice = '<?php echo str_replace("\n", "", CHtml::textField('special_shipping_price', '0'));?>';
var orderTotalPrice = <?php echo $orderTotalPrice;?>;

jQuery(document).ready(function(){	
	/**************************************************
	/* Event when click the add existing item
	/**************************************************/
	jQuery("#add_existing_item").click(function(){
		item_number++;		
		var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'
				+ '<td class="item_name">' + strNameList + '</td>'
				+ '<td class="color">' + strColorList + '</td>'
				+ '<td class="quantity">' + strQuantity + '</td>'
				+ '<td class="item_eta">' + '' + '</td>'
				+ '<td class="item_price"></td>'
				+ '<td class="special_shipping_price">' + strSpecialShippingPrice + '</td>'
				+ '<td class="custom_color">' + strCustomColor + '</td>'
				+ '<td class="total_price"></td>'
				+ '<td class="action"><a href="#" class="delete_item">Delete</a></td>'
				+ '</tr>';

		jQuery("#order_item_list").append(newRowHtml);
		jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");
		jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]");
		jQuery("#item_" + item_number + " .custom_color input").attr("name", "item[custom_color][]");
		jQuery("#item_" + item_number + " .special_shipping_price input").attr("name", "item[special_shipping_price][]");
		
		item_count++;
		changeOrderItemList();
	});
	
	/**************************************************
	/* Event when change the item name
	/**************************************************/
	jQuery(".item_name select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		changeItemName(item_cur_number);
	});
	
	/**************************************************
	/* Event when change the item code
	/**************************************************/
	jQuery(".item_code select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		changeItemCode(item_cur_number);
	});
	
	/**************************************************
	/* Event when change the color
	/**************************************************/
	jQuery(".color select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		var item_id = jQuery(this).val();
		changeItemColor(item_cur_number, item_id);
	});
	
	/**************************************************
	/* Event when change the quantity
	/**************************************************/
	jQuery(".quantity input").live("change",function(){
		calculateTotal();
		
		var item_number = jQuery(this).parent().parent().attr("rel");
		var quantity = jQuery(this).val();
		getItemEta(item_number, quantity);
	});
	
	/**************************************************
	/* Event when change the special shipping price
	/**************************************************/
	jQuery(".special_shipping_price input").live("change",function(){
		calculateTotal();
	});
	
	/**************************************************
	/* Event when click the delete button
	/**************************************************/
	jQuery(".delete_item").live("click",function(){
		jQuery(this).parent().parent().remove();
		item_count--;
		changeOrderItemList();
		calculateTotal();
		return false;
	});
	
	jQuery("#reset_items").click(function(){
		jQuery("tr.item_row").remove();
		item_count = 0;;
		changeOrderItemList();
		calculateTotal();
		return false;
	});
	
	/**************************************************
	/* Event when click the Calculate button
	/**************************************************/
	jQuery("#calculate_total").click(function(e){
		jQuery(e.target).attr('disabled', 'disabled');
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/add_new_osc_order_ajax/"
			+ "?osc_order_id=<?php echo $oscOrderModel->orders_id;?>&osc_site_id=<?php echo $oscOrderModel->site_id;?>";
		var postData = jQuery("form#customer-order-form").serialize();
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}

				window.location.href = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/import_osc_orders";
			}
		});
	});
	
	/**************************************************
	/* Event when click the Submit button on lightbox
	/**************************************************/
	jQuery("#fancybox-content #add_new_customer_submit").live("click", function(){
		var postData = jQuery("#fancybox-content form#customer-form").serialize();
		var url_add = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/add_new_customer_ajax";
		
		jQuery.ajax({
			url:url_add,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}
				jQuery.fancybox.close();
				jQuery("#CustomerOrder_customer_id").val(result.data.id);
			}
		});
	});
	
	/**************************************************
	/* Event when click the Submit button on lightbox
	/**************************************************/
	jQuery("#fancybox-content #add_new_item_submit").live("click", function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/add_new_item_ajax";
		var postData = jQuery("#fancybox-content form#item-form").serialize();
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}
				
				var item_name = result.data.item_name;
				var quantity = result.data.quantity;
				
				var str_new_option = '<option value="' + item_name +'">' + item_name + '</option>';
				if (strNameList.indexOf(str_new_option) == -1){
					strNameList = strNameList.replace('</select>', str_new_option + '</select>');
					jQuery(".item_name select").append(str_new_option);
				}
				
				item_number++;		
				var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'
						+ '<td class="item_name">' + strNameList + '</td>'
						+ '<td class="color">' + strColorList + '</td>'
						+ '<td class="quantity">' + strQuantity + '</td>'
						+ '<td class="item_eta">' + '' + '</td>'
						+ '<td class="item_price"></td>'
						+ '<td class="special_shipping_price">' + strSpecialShippingPrice + '</td>'
						+ '<td class="custom_color">' + strCustomColor + '</td>'
						+ '<td class="total_price"></td>'
						+ '<td class="action"></td>'
						+ '</tr>';

				jQuery("#order_item_list").append(newRowHtml);
				jQuery("#item_" + item_number + " .item_name select").val(item_name);
				jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");
				jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]").val(quantity);
				jQuery("#item_" + item_number + " .custom_color input").attr("name", "item[custom_color][]");
				jQuery("#item_" + item_number + " .special_shipping_price input").attr("name", "item[special_shipping_price][]").val(quantity);
				
				item_count++;
				changeOrderItemList();
				
				jQuery.fancybox.close();
			}
		});
	});
	
	changeOrderItemList();
	jQuery("#CustomerOrder_shipping_method").val("Shipping");
	// checkNameDuplicated();
});

// function checkNameDuplicated(){
	// var url_check = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/check_name_duplicated_ajax";
	// var postData = jQuery("form#customer-form").serialize();
	
	// jQuery.ajax({
		// url:url_check,
		// dataType:"json",
		// data: postData,
		// type: "POST",
		// success:function(result){
			// if (result.message == "duplicated"){
				// if (confirm("Customer name is duplicated. Do you want to add this customer?")){
					// jQuery("#CustomerOrder_customer_id").val(result.id);
				// }
			// }
		// }
	// });
// }

/**************************************************
/* Add the inputed data into the 
/**************************************************/
function calculateTotal(){
	var grand_total_price = 0;
	var item_price_list = new Array();
	var i = 0;
	jQuery(".item_row").each(function(){
		var row_id = jQuery(this).attr("id");
		var sale_price = jQuery("#" + row_id + " .action input.sale_price").val();
		
		var shipping_price = 0;
		
		if (jQuery("#shipping_method").val() == "Shipping"){
			shipping_price = jQuery("#" + row_id + " .action input.shipping_price").val();
		} else if (jQuery("#shipping_method").val() == "Local Pickup") {
			shipping_price = jQuery("#" + row_id + " .action input.local_pickup_price").val();
		} else if (jQuery("#shipping_method").val() == "Shipped from Los Angeles") {
			shipping_price = jQuery("#" + row_id + " .action input.la_oc_shipping_price").val();
		} else if (jQuery("#shipping_method").val() == "Canada Shipping") {
			shipping_price = jQuery("#" + row_id + " .action input.canada_shipping_price").val();
		}
		
		var special_shipping_price = jQuery("#" + row_id + " #special_shipping_price").val();
		special_shipping_price = isNaN(special_shipping_price)?0:special_shipping_price;
		var quantity = jQuery("#" + row_id + " .quantity input").val();
		
		if (isNaN(quantity) || isNaN(sale_price)){
			return 0;
		}
		
		var item_total_price = (parseFloat(sale_price) + parseFloat(shipping_price) + parseFloat(special_shipping_price)) * parseInt(quantity);
		jQuery("#" + row_id + " .item_price").html(sale_price);
		jQuery("#" + row_id + " .total_price").html(item_total_price);
		grand_total_price += item_total_price;
		
		var item_price = new Array();
		item_price['sale_price'] = parseFloat(sale_price);
		item_price['shipping_price'] = parseFloat(shipping_price);
		item_price['special_shipping_price'] = parseFloat(special_shipping_price);
		item_price['quantity'] = quantity;
		
		item_price_list[i] = item_price;
		i++;
	});
	
	grand_total_price = grand_total_price.toFixed(2);
	calculateDiscountedTotal(grand_total_price, item_price_list);
}

function getItemTotalPrice(item_price){
	var total_price = item_price['sale_price'] + item_price['shipping_price'] + item_price['special_shipping_price'];
	return total_price;
}

/**************************************************
/* Calculate the discounted total
/**************************************************/
function calculateDiscountedTotal(grand_total_price, item_price_list){
	item_price_list.sort(function(item_price_first, item_price_second){
		var total_price_first = getItemTotalPrice(item_price_first);
		var total_price_second = getItemTotalPrice(item_price_second);
		return total_price_second - total_price_first;
	});

	var strPrice = "";
	var salePriceDiscount = 97.5;
	var shippingPriceDiscount = 95;
	var discounted_total = 0;
	
	for (var index in item_price_list){
		var item_price = item_price_list[index];
		var quantity = item_price['quantity'];
		
		for (var i = 0; i < quantity; i++){
			salePriceDiscount = (salePriceDiscount < 70.5)?70:salePriceDiscount - 0.5;
			shippingPriceDiscount = (shippingPriceDiscount < 50)?45:shippingPriceDiscount - 5;
			
			var item_discounted_total_price = (salePriceDiscount * item_price['sale_price']) / 100
				+ (shippingPriceDiscount * (item_price['shipping_price'] + item_price['special_shipping_price'])) / 100;
			
			discounted_total += item_discounted_total_price;
		}
	}
	
	discounted_total = discounted_total.toFixed(2);	
	var total_discount = (grand_total_price - discounted_total).toFixed(2);
	
	jQuery(".grand_total_price span").html(grand_total_price);
	jQuery(".discounted_total span").html(discounted_total);
	jQuery(".total_discount span").html(total_discount);
	
	jQuery("#param_grand_total_price").val(grand_total_price);
	jQuery("#param_discounted_total").val(discounted_total);
	
	if (orderTotalPrice == grand_total_price){
		jQuery("#total_price_same").html("Total price is same");
		jQuery("#total_price_same").addClass("same");
	} else {
		jQuery("#total_price_same").html("Total price is not same");
		jQuery("#total_price_same").removeClass("same");
	}
}

/**************************************************
/* Reflect the item change status to the grid view
/**************************************************/
function changeOrderItemList(){
	if (item_count == 0){
		jQuery("#order_item_list").append('<tr class="order_item_empty"><td colspan=8>No items are added</td></tr>');
	} else {
		jQuery(".order_item_empty").remove();
		changeItemName(item_number);
	}
}

/**************************************************
/* Get the item eta when change the item and quantity
/**************************************************/
function getItemEta(item_number, quantity){
	var item_id = jQuery("#item_" + item_number + " .action .item_id").val();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/item_eta_by_id_quantity/?"
			+ "item_id=" + item_id + "&quantity=" + quantity;
	
	jQuery.ajax({
		url:url,
		success:function(result){
			jQuery("#item_" + item_number + " .item_eta").html(result);
		}
	});
}

/**************************************************
/* Get the color when change the item name
/**************************************************/
function changeItemName(item_cur_number){
	var item_name = jQuery("#item_" + item_cur_number + " #item_name").val();
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/item_color_list_by_name/?item_name=" + encodeURIComponent(item_name);
	
	jQuery('#calculate_total').attr('disabled', 'disabled');
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;

			var itemColorList = result.itemColorList;
			var selectText = "";
			var bChangeColor = false;
			
			for (var item_id in itemColorList){
				if (!bChangeColor){
					changeItemColor(item_cur_number, item_id);
					bChangeColor = true;
				}					
				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';
			}
			jQuery("#item_" + item_cur_number + " .color select").html(selectText);
		}
	});
}

/**************************************************
/* Get the color when change the item code
/**************************************************/
function changeItemCode(item_cur_number){
	var item_code = jQuery("#item_" + item_cur_number + " #item_code").val();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/itemcolorlist/?item_code=" + encodeURIComponent(item_code);
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;

			var itemColorList = result.itemColorList;
			var selectText = "";
			var bChangeColor = false;
			
			for (var item_id in itemColorList){
				if (!bChangeColor){
					changeItemColor(item_cur_number, item_id);
					bChangeColor = true;
				}					
				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';
			}
			jQuery("#item_" + item_cur_number + " .color select").html(selectText);
		}
	});
}

/**************************************************
/* Get the item info when change the color
/**************************************************/
function changeItemColor(item_cur_number, item_id){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/iteminfo/?id=" + item_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;
			
			var itemInfo = result.itemInfo;
			var shipping_price = 0;
			
			if (jQuery("#shipping_method").val() == "Shipping"){
				shipping_price = itemInfo.shipping_price;
			} else if (jQuery("#shipping_method").val() == "Local Pickup"){
				shipping_price = itemInfo.local_pickup;
			} else if (jQuery("#shipping_method").val() == "Shipped from Los Angeles"){
				shipping_price = itemInfo.la_oc_shipping;
			} else if (jQuery("#shipping_method").val() == "Canada Shipping") {
				shipping_price = itemInfo.canada_shipping;
			}
			
			var str_action_field = "<input type='hidden' class='sale_price' value='" + itemInfo.sale_price + "'/>" 
				+ "<input type='hidden' class='shipping_price' value='" + itemInfo.shipping_price + "'/>" 
				+ "<input type='hidden' class='local_pickup_price' value='" + itemInfo.local_pickup + "'/>" 
				+ "<input type='hidden' class='la_oc_shipping_price' value='" + itemInfo.la_oc_shipping + "'/>"
				+ "<input type='hidden' class='canada_shipping_price' value='" + itemInfo.canada_shipping + "'/>"
				+ "<input type='hidden' class='item_id' value='" + itemInfo.id + "'/>"
				+ '<a href="#" class="delete_item">Delete</a>';
			
			jQuery("#item_" + item_cur_number + " .action").html(str_action_field);
			calculateTotal();
			
			var quantity = jQuery("#item_" + item_cur_number + " #quantity").val();
			getItemEta(item_cur_number, quantity);
			jQuery('#calculate_total').removeAttr('disabled');
		}
	});
}
</script>