<?php

$salesPerson = YumUser::model()->findByPk($customerOrderModel->sales_person_id);
$salesPersonName = $salesPerson->profile->firstname." ".$salesPerson->profile->lastname;
// $total_price = CustomerOrder::model()->getCustomerOrderTotalPrice($customerOrderModel->id);
$price = CustomerOrder::model()->getCustomerOrderTotalPrice($customerOrderModel->id, true);
$total_price  = number_format($price['total_with_tax'], 2);
$order_tax    = $price['tax'];
$settings = Settings::model()->getSettingVariables();
$tax_percent = number_format($settings->tax_percent, 2);

?>

<?php
/////////////
//TotalTotalTotalTotalTotalTotalTotalTotalTotalTotal
?>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-grid',
	'dataProvider'=>$customerOrderItemsModel->search($customerOrderModel->id),
	'summaryText'=>'',
	'columns'=>array(
		array(
			'header'=>'ITEM NAME',
			'value'=>'$data->items->item_name',
		),
        array(
			'header'=>'Color',
			'value'=>'$data->items->color.($data->custom_color ? \' (\'.$data->custom_color.\')\' : \'\')',
		),	
		array(
			'header'=>'QTY',
			'value'=>'$data->quantity',
		),
		array(
			'header'=>'TOTAL PER ITEM',
			'value'=>'CustomerOrderItems::model()->getItemTotalSalePrice($data->id)',
		),
			
	),
));

if ($order_tax):
?>
<h3 class="sales_tax">Sales Tax at <?php echo $tax_percent; ?>%: $<?php echo number_format($order_tax, 2);?></h3>
<?php
endif;
?>
<h3 class="grand_total">Grand Total: $<?php echo $total_price?></h3>
<?php
	if ($customerOrderModel->discounted == "Discounted"){
?>
		<h3>Discounted Total:$<?php echo number_format($customerOrderModel->discounted_total,2)?></h3>
<?php
        $amount = $customerOrderModel->discounted_total;
	} else {
        $amount = $customerOrderModel->grand_total_price;
    }
?>
<br/><br/><br/>

<?php
	if ($transaction_success == false){
		echo '<br /><strong>ERROR: '.$error_message.'</strong><br /><br />';
	}
?>

<?php
if ($customerOrderModel->status == CustomerOrder::$STATUS_COMPLETED || $customerOrderModel->status == CustomerOrder::$STATUS_SOLD){
?>
	<h3>This order is completed</h3>
<?php
} elseif ($customerOrderModel->status == CustomerOrder::$STATUS_LOST || $customerOrderModel->status == CustomerOrder::$STATUS_DEAD) {
   ?>
    <h3>This order cannot be processed</h3>
<?php   
}

 else {
?>

	<form id="checkout_form" method="POST">
		<h3>Credit Card</h3>
		<div class="field_list active">
<?php /*
			<input type="hidden" readonly="readonly" id="amount" name="amount" value="<?php echo $amount;?>">
*/ ?>			
			<div class="field_row">
				<div class="field_label">Credit Card Number:</div>
				<div class="field_content"><input type="text" name="authorizenet_cc_number">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Credit Card Expiry Date:</div>		
				<div class="field_content">
					<select style="width:125px" name="authorizenet_cc_expires_month">
						<option value="01">January</option><option value="02">February</option>
						<option value="03">March</option><option value="04">April</option>
						<option value="05">May</option><option value="06">June</option>
						<option value="07">July</option><option value="08">August</option>
						<option value="09">September</option><option value="10">October</option>
						<option value="11">November</option><option value="12">December</option>
					</select>&nbsp;
					<select name="authorizenet_cc_expires_year">
						<option value="12">2012</option><option value="13">2013</option>
						<option value="14">2014</option><option value="15">2015</option>
						<option value="16">2016</option><option value="17">2017</option>
						<option value="18">2018</option><option value="19">2019</option>
						<option value="20">2020</option><option value="21">2021</option>
					</select>
				</div>
			</div>
			<?php /*
			<div class="field_row">
				<div class="field_label">Name on the Card</div>		
				<div class="field_content"><input type="text" name="authorizenet_cc_owner"></div>
			</div>
            */ ?>
			<div class="field_row">
				<div class="field_label">Credit Card Type:</div>
				
				<div class="field_content">
					<select name="credit_card_type">
						<option value="Amex">Amex</option>
						<option value="Discover">Discover</option>
						<option value="Mastercard">Mastercard</option>
						<option value="Visa">Visa</option>
					</select>
				</div>		
			</div>
		</div>
			
		<h3>Billing Address</h3>
		<div class="field_list active">
			<div class="field_row">
				<div class="field_label">Name:</div>
				<div class="field_content"><input name="fname" value="<?php echo $customerModel->first_name; ?>" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
            <div class="field_row">
				<div class="field_label">Last Name:</div>
				<div class="field_content"><input name="lname" type="text" value="<?php echo $customerModel->last_name; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Street Address:</div>
				<div class="field_content"><input name="street_address" value="<?php echo $orderBillingInfoModel->billing_street_address; ?>" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">City:</div>
				<div class="field_content"><input name="city" value="<?php echo $orderBillingInfoModel->billing_city; ?>" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">State/Province:</div>
				<div class="field_content">
                    <input type="text" name="state" value="<?php echo $orderBillingInfoModel->billing_state; ?>" />&nbsp;
					<span class="inputRequirement">*</span>
				</div>
			</div>
			<div class="field_row">
				<div class="field_label">Zip Code:</div>
				<div class="field_content"><input name="postcode" value="<?php echo $orderBillingInfoModel->billing_zip_code; ?>" type="text">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Telephone Number:</div>
				<div class="field_content"><input name="telephone" type="text" value="<?php echo $customerModel->phone; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">E-Mail Address:</div>
				<div class="field_content"><input name="email_address" type="text" value="<?php echo $customerModel->email; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
		</div>

		<h3>Shipping Address</h3>
		<div class="field_list active">
			<div class="field_row">
				<div class="field_label"><b>My shipping address is:</b></div>
				<div class="field_content">
					<input type="radio" id="my_billing_address" checked="" value="0" name="sh_radio">My billing address.
					<input type="radio" id="different_address" value="1" name="sh_radio">A different address.
				</div>
			</div>	
			<div class="field_row">
				<div class="field_label"><b>This ship address is:</b></div>
				<div class="field_content">
					<input type="radio" checked="" value="Residental" name="busrestype">Residental
					<input type="radio" value="Business" name="busrestype">Business
				</div>
			</div>
		</div>

		<div class="field_list" id="shipping_address">
			<div class="field_row">
				<div class="field_label">Name:</div>
				<div class="field_content"><input type="text" size="20" value="<?php echo $orderBillingInfoModel->shipping_name.' '.$orderBillingInfoModel->shipping_last_name; ?>" name="ShipFirstName" style="width:180px">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Street Address:</div>
				<div class="field_content"><font size="2"><input size="20" value="<?php echo $orderBillingInfoModel->shipping_street_address; ?>" name="ShipAddress">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">City:</div>
				<div class="field_content"><input type="text" value="<?php echo $orderBillingInfoModel->shipping_city; ?>" name="ShipCity">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">State/Province:</div>
				<div class="field_content">
                    <input type="text" value="<?php echo $orderBillingInfoModel->shipping_state; ?>" name="shipstate" />
					<span class="inputRequirement">*</span>
				</div>
			</div>
			<div class="field_row">
				<div class="field_label">Zip Code:</div>
				<div class="field_content"><input type="text" value="<?php echo $orderBillingInfoModel->shipping_zip_code; ?>" name="shippostcode">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Telephone Number:</div>
				<div class="field_content"><input type="text" value="<?php echo $orderBillingInfoModel->shipping_phone_number; ?>" name="shiptelephone">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
		</div>
		
		<input type="submit" id="btnCheckOut" value="Checkout"/>
	</form>
<?php
}
?>

<script>
var bMyBillingAddress = true;
jQuery(document).ready(function(){
	jQuery("#my_billing_address").click(function(){
		jQuery("#shipping_address").removeClass("active");
		bMyBillingAddress = true;
	});
	
	jQuery("#different_address").click(function(){
		jQuery("#shipping_address").addClass("active");
		bMyBillingAddress = false;
	});
	
	jQuery("#btnCheckOut").click(function(){
		bVerify = true;
		
		jQuery('form#checkout_form .field_list.active .field_content input[type="text"]').each(function(){			
			if (jQuery(this).val().trim() == ""){
				jQuery(this).focus();
				bVerify = false;
				return false;
			}
		});		
		
		if (bVerify == false){
			alert("Please input the required value");
			return false;
		}
	});
});
</script>
