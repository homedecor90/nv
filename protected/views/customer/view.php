<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->id,
);*/

$this->menu=array(
	array('label'=>'Create Customer', 'url'=>array('create')),
	array('label'=>'Update Customer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Customer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Customer', 'url'=>array('admin')),
);


if (Yii::app()->user->isAdmin()){
	$this->profit = $this->renderPartial('_profit', array('profit'=>$profit), true);
}
?>



<h1>View Customer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'first_name',
		'last_name',
		'phone',
		array(
            'label'=>'email',
            'type'=>'raw',
            'value'=>'<a href="mailto:'.$model->email.'">'.$model->email.'</a>'
        ),
        array(
            'label'=>'Created On',
            'value'=> (strtotime($model->created_on)>0 ? date("M. d, Y h:i:s A", strtotime($model->created_on)) : '')
        ),
		'heard_through',
	),
)); ?>
<br/>
<?php
$viewData = array(
	"customerNoteModel"=>$customerNoteModel,
	"customerFollowupModel"=>$customerFollowupModel,
	"customerOrderModel"=>$customerOrderModel,
	"newCustomerNoteModel"=>$newCustomerNoteModel,
	"newCustomerFollowupModel"=>$newCustomerFollowupModel,
	"newCustomerOrderModel"=>$newCustomerOrderModel,	
	"salesPersonList"=>$salesPersonList,
	"itemCodeList"=>$itemCodeList,
	"itemNameList"=>$itemNameList,	
	"newItemModel"=>$newItemModel,
	"sales_tax_percent"=>$sales_tax_percent,
	"model"=>$model,
);

$Tabs = array(
	'order'=>array('title'=>'Order','view'=>"_order"),
	'note'=>array('title'=>'Note','view'=>"_note"),
	'followup'=>array('title'=>'Followup','view'=>"_followup"),
);

$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));

?>