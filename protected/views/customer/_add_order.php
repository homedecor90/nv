<div class="row">

		<?php echo $form->labelEx($newCustomerOrderModel,'shipping_method'); ?>

		<?php

			$shippingMethodList = array(

				'Shipping'=>'Shipping',

				'Local Pickup'=>'Local Pickup',

				'Shipped from Los Angeles'=>'Shipped from Los Angeles',

				'Canada Shipping'=>'Canada Shipping',

			);

		?>

		<?php echo $form->dropDownList($newCustomerOrderModel, 'shipping_method', $shippingMethodList, array('id'=>'shipping_method'));?>

		<?php echo $form->error($newCustomerOrderModel,'shipping_method'); ?>

	</div>

	

	<div class="row">

        <?php if (!YumUser::isRetailer() || Yii::app()->user->isAdmin()) { ?>
        <?php echo $form->labelEx($newCustomerOrderModel,'discounted'); ?>
		<?php

			$shippingMethodList = array(

				'Not Discounted'=>'Not Discounted',

				'Discounted'=>'Discounted',

			);

		?>

		<?php echo $form->dropDownList($newCustomerOrderModel, 'discounted', $shippingMethodList, array('id'=>'discounted'));?>

		<?php echo $form->error($newCustomerOrderModel,'discounted'); ?>

        <?php } else {;?>
            <?php echo CHtml::activeHiddenField($newCustomerOrderModel, 'discounted', array('id'=>'discounted', 'value' => 'Discounted'));?>
        <?php }?>
	</div>

    <?php if (!YumUser::isRetailer() || Yii::app()->user->isAdmin()):?>

    <div class="row">

		<?php echo CHtml::label('Custom Discounted Total', 'custom_discounted'); ?>

        <?php echo CHtml::textField('custom_discounted'); ?>

        <?php echo $form->hiddenField($newCustomerOrderModel, 'custom_discount', array('value'=>0)); ?>

        <?php echo $form->hiddenField($newCustomerOrderModel, 'raw_discounted', array('value'=>0)); ?>

    </div>

    

    <div class="row">

		<?php echo $form->checkbox($newCustomerOrderModel, 'add_sales_tax');?>

		<?php echo $form->labelEx($newCustomerOrderModel,'add_sales_tax', array('class'=>'checkbox_label')); ?>

	</div>
    <?php endif;?>

    

	<table id="order_item_list">

		<thead>

			<th class="item_name">Item Name</th>

			<th class="color">Color</th>

			<th class="quantity">Qty</th>

			<th class="item_eta">E.T.A</th>

			<th class="item_price">Price</th>

			<th class="special_shipping_price">Special Shipping</th>

			<th class="custom_color">Custom Color</th>

			<th class="total_price">Total Price</th>

			<th class="action">Action</th>

		</thead>

	</table>

	

	<div class="customer_order_info">

		<div class="row grand_total_price">Grand Total: $<span>0</span></div>

		<div class="row discounted_total">Discounted Total: $<span>0</span></div>

		<div class="row total_discount">Total Discount: $<span>0</span></div>

<?php if (Yii::app()->user->isAdmin()) : ?>

		<div class="row ant_profit">Anticipated Profit: $<span>0</span></div>

		

<?php endif; ?>

		<?php echo $form->hiddenField($newCustomerOrderModel, 'grand_total_price', array("id"=>"param_grand_total_price", 'value'=>0));?>

		<?php echo $form->hiddenField($newCustomerOrderModel, 'discounted_total', array("id"=>"param_discounted_total", 'value'=>0));?>

	</div>

	

	

<script type="text/javascript">



var item_count = 0;

var item_number = 0;

var strNameList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_name', '', $itemNameList['itemNameList']));?>';

var strColorList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_color', '', $itemNameList['itemColorList']));?>';

var strQuantity = '<?php echo str_replace("\n", "", CHtml::textField('quantity', '1'));?>';

var strSpecialShippingPrice = '<?php echo str_replace("\n", "", CHtml::textField('special_shipping_price', '0'));?>';

var strCustomColor = '<?php echo str_replace("\n", "", CHtml::textField('custom_color', '', array('id'=>'', 'class'=>"custom_color")));?>';

var sales_tax_percent = <?php echo $sales_tax_percent; ?>;



// elements for add order items form

var _strNameList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_name', '', $itemNameList['itemNameList'], array('id'=>'', 'class'=>"add_order_items_item_name")));?>';

var _strColorList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_color', '', $itemNameList['itemColorList'], array('id'=>'', 'class'=>"add_order_items_item_color")));?>';

var _strQuantity = '<?php echo str_replace("\n", "", CHtml::textField('quantity', '1', array('id'=>'', 'class'=>"add_order_items_qty")));?>';

var _strSpecialShippingPrice = '<?php echo str_replace("\n", "", CHtml::textField('special_shipping_price', '0', array('id'=>'', 'class'=>"add_order_items_spec_shipping")));?>';

var _strCustomColor = '<?php echo str_replace("\n", "", CHtml::textField('custom_color', '', array('id'=>'', 'size'=>'10', 'class'=>"custom_color")));?>';

// var init = false;

jQuery(document).ready(function(){

	if (typeof(init) == 'undefined' || !init){

		jQuery('#CustomerOrder_add_sales_tax').change(function(e){

			calculateTotal();

		});

		

		jQuery("#reset_items").click(function(){

			jQuery("tr.item_row").remove();

			item_count = 0;;

			changeOrderItemList();

			calculateTotal();

			return false;

		});

		/**************************************************

		/* Event when click the add existing item

		/**************************************************/

		jQuery("#add_existing_item").click(function(){

			item_number++;		

			var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'

					+ '<td class="item_name">' + strNameList + '</td>'

					+ '<td class="color">' + strColorList + '</td>'

					+ '<td class="quantity">' + strQuantity + '</td>'

					+ '<td class="item_eta">' + '' + '</td>'

					+ '<td class="item_price"></td>'

					+ '<td class="special_shipping_price">' + strSpecialShippingPrice + '</td>'

					+ '<td class="custom_color">' + strCustomColor + '</td>'

					+ '<td class="total_price"></td>'

					+ '<td class="action"><a href="#" class="delete_item">Delete</a></td>'

					+ '</tr>';



			jQuery("#order_item_list").append(newRowHtml);

			jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");

			jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]");

			jQuery("#item_" + item_number + " .custom_color input").attr("name", "item[custom_color][]");

			jQuery("#item_" + item_number + " .special_shipping_price input").attr("name", "item[special_shipping_price][]");

			

			item_count++;

			changeOrderItemList();

		});

		

		/**************************************************

		/* Event when change the item name

		/**************************************************/

		jQuery(".item_name select").live("change",function(){

			var item_cur_number = jQuery(this).parent().parent().attr("rel");

			changeItemName(item_cur_number);

		});

		

		/**************************************************

		/* Event when change the item code

		/**************************************************/

		jQuery(".item_code select").live("change",function(){

			var item_cur_number = jQuery(this).parent().parent().attr("rel");

			changeItemCode(item_cur_number);

		});

		

		/**************************************************

		/* Event when change the color

		/**************************************************/

		jQuery(".color select").live("change",function(){

			var item_cur_number = jQuery(this).parent().parent().attr("rel");

			var item_id = jQuery(this).val();

			changeItemColor(item_cur_number, item_id);

		});

		

		/**************************************************

		/* Event when change the quantity

		/**************************************************/

		jQuery(".quantity input").live("change",function(){

			calculateTotal();

			

			var item_number = jQuery(this).parent().parent().attr("rel");

			var quantity = jQuery(this).val();

			getItemEta(item_number, quantity);

		});

		

		/**************************************************

		/* Event when change the special shipping price

		/**************************************************/

		jQuery(".special_shipping_price input").live("change",function(){

			var special_shipping_price_value = jQuery(this).val()

			if (isNaN(special_shipping_price_value) || special_shipping_price_value == ""){

				jQuery(this).val(0);

			}

			

			calculateTotal();

		});

		

		/**************************************************

		/* Event when click the delete button

		/**************************************************/

		jQuery(".delete_item").live("click",function(){

			jQuery(this).parent().parent().remove();

			item_count--;

			changeOrderItemList();

			calculateTotal();

			return false;

		});

		

		changeOrderItemList();

		jQuery("#CustomerOrder_shipping_method").val("Shipping");

		init = true;

	}

});



/**************************************************

/* Add the inputed data into the 

/**************************************************/

function calculateTotal(){

    var grand_total_price = 0;

	var ant_profit_wod_total = 0;

	var item_price_list = new Array();

	var i = 0;

    var add_sales_tax = (typeof(jQuery('#CustomerOrder_add_sales_tax').attr('checked')) == 'undefined') ? 0 : 1;

    

	jQuery(".item_row").each(function(){

		var row_id = jQuery(this).attr("id");

		var sale_price = jQuery("#" + row_id + " .action input.sale_price").val();

		

		var shipping_price = 0;

		

		if (jQuery("#shipping_method").val() == "Shipping"){

			shipping_price = jQuery("#" + row_id + " .action input.shipping_price").val();

		} else if (jQuery("#shipping_method").val() == "Local Pickup") {

			shipping_price = jQuery("#" + row_id + " .action input.local_pickup_price").val();

		} else if (jQuery("#shipping_method").val() == "Shipped from Los Angeles") {

			shipping_price = jQuery("#" + row_id + " .action input.la_oc_shipping_price").val();

		} else if (jQuery("#shipping_method").val() == "Canada Shipping") {

			shipping_price = jQuery("#" + row_id + " .action input.canada_shipping_price").val();

		}

		

		var special_shipping_price = jQuery("#" + row_id + " #special_shipping_price").val();		

		var quantity = jQuery("#" + row_id + " .quantity input").val();

		

		if (isNaN(quantity) || isNaN(sale_price)){

			return 0;

		}

		

		var item_total_price = (parseFloat(sale_price) + parseFloat(shipping_price)) * parseInt(quantity) + parseFloat(special_shipping_price);

		item_total_price = parseFloat(item_total_price.toFixed(2));

		var ant_profit_wod = parseFloat(jQuery("#" + row_id + " .action input.ant_profit_wod").val())*parseInt(quantity);

		

		jQuery("#" + row_id + " .item_price").html(sale_price);

		jQuery("#" + row_id + " .total_price").html(item_total_price);

		

		grand_total_price += item_total_price;

		ant_profit_wod_total += parseFloat(ant_profit_wod);

		

		var item_price = new Array();

		item_price['sale_price'] = parseFloat(sale_price);

		

		item_price['shipping_price'] = parseFloat(shipping_price);

		item_price['special_shipping_price'] = parseFloat(special_shipping_price);

		item_price['quantity'] = quantity;

		

		item_price_list[i] = item_price;

		i++;

	});

	

    if (add_sales_tax){

        grand_total_price = grand_total_price * (100 + sales_tax_percent) / 100;

    }

    

	grand_total_price = grand_total_price.toFixed(2);

	calculateDiscountedTotal(grand_total_price, item_price_list, ant_profit_wod_total);

}



function getItemTotalPrice(item_price){

	// var total_price = item_price['sale_price'] + item_price['shipping_price'] + item_price['special_shipping_price'];

	var total_price = item_price['sale_price'] + item_price['shipping_price'];

	return total_price;

}



/**************************************************

/* Calculate the discounted total

/**************************************************/



jQuery('#discounted').change(function(){

    calculateTotal();

});



function calculateDiscountedTotal(grand_total_price, item_price_list, ant_profit_wod_total){

	if (jQuery('#discounted').val() == 'Discounted'){

        item_price_list.sort(function(item_price_first, item_price_second){

            var total_price_first = getItemTotalPrice(item_price_first);

            var total_price_second = getItemTotalPrice(item_price_second);

            return total_price_second - total_price_first;

        });



        var strPrice = "";

        var salePriceDiscount = 97.5;

        var shippingPriceDiscount = 95;

        var discounted_total = 0;

        var distract_value = 0;

		var add_sales_tax = (typeof(jQuery('#CustomerOrder_add_sales_tax').attr('checked')) == 'undefined') ? 0 : 1;

        

        for (var index in item_price_list){

            var item_price = item_price_list[index];

            var quantity = parseInt(item_price['quantity']);

            // for (var i = 0; i < quantity; i++){

                // salePriceDiscount = (salePriceDiscount < 70.5)?70:salePriceDiscount - 0.5;

                // shippingPriceDiscount = (shippingPriceDiscount < 50)?45:shippingPriceDiscount - 5;

                

                // var item_discounted_total_price = (salePriceDiscount * item_price['sale_price']) / 100

                    // + (shippingPriceDiscount * (item_price['shipping_price'] + item_price['special_shipping_price'])) / 100;

                

                // discounted_total += item_discounted_total_price;

            // }

        // ====================================

            // calculate how many sale price percents should we distract

            

            if (quantity > 10){

                var percents = 55 + 10*(quantity-10);

            } else {

                var percents = (1+quantity)*quantity/2;

            }

            

            distract_value += item_price['sale_price']*0.01*percents;

            // same thing for shipping percents

            if (quantity > 15){

                percents = 120 + 15*(quantity-15);

            } else {

                percents = (1+quantity)*quantity/2;

            }

            distract_value += item_price['shipping_price']*0.01*percents;

        }

        

        if (add_sales_tax){

            distract_value = distract_value * (100 + sales_tax_percent) / 100;

        }

        

        discounted_total = (grand_total_price - distract_value).toFixed(2);

    } else {

        discounted_total = grand_total_price;

    }

	

	var total_discount = (grand_total_price - discounted_total).toFixed(2);

	var ant_profit = (ant_profit_wod_total - total_discount).toFixed(2); 

	

	

	jQuery(".grand_total_price span").html(grand_total_price);

	jQuery(".discounted_total span").html(discounted_total);

	jQuery(".total_discount span").html(total_discount);

	jQuery(".ant_profit span").html(ant_profit);

	

	jQuery("#param_grand_total_price").val(grand_total_price);

	jQuery("#param_discounted_total").val(discounted_total);

}



/**************************************************

/* Reflect the item change status to the grid view

/**************************************************/

function changeOrderItemList(){

	if (item_count == 0){

		jQuery("#order_item_list").append('<tr class="order_item_empty"><td colspan=8>No items are added</td></tr>');

	} else {

		jQuery(".order_item_empty").remove();

		changeItemName(item_number);

	}

}



/**************************************************

/* Get the item eta when change the item and quantity

/**************************************************/

function getItemEta(item_number, quantity){

	var item_id = jQuery("#item_" + item_number + " .action .item_id").val();

	

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/item_eta_by_id_quantity/?"

			+ "item_id=" + item_id + "&quantity=" + quantity;

	

	jQuery.ajax({

		url:url,

		success:function(result){

			jQuery("#item_" + item_number + " .item_eta").html(result);

		}

	});

}



/**************************************************

/* Get the color when change the item name

/**************************************************/

function changeItemName(item_cur_number){

	var item_name = jQuery("#item_" + item_cur_number + " #item_name").val();

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/item_color_list_by_name/?item_name=" + encodeURIComponent(item_name);

	

	jQuery.ajax({

		url:url,

		dataType:"json",

		success:function(result){

			if (result.message != "success")

				return;



			var itemColorList = result.itemColorList;

			var selectText = "";

			var bChangeColor = false;

			

			for (var item_id in itemColorList){

				if (!bChangeColor){

					changeItemColor(item_cur_number, item_id);

					bChangeColor = true;

				}					

				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';

			}

			jQuery("#item_" + item_cur_number + " .color select").html(selectText);

		}

	});

}



/**************************************************

/* Get the color when change the item code

/**************************************************/

function changeItemCode(item_cur_number){

	var item_code = jQuery("#item_" + item_cur_number + " #item_code").val();

	

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/itemcolorlist/?item_code=" + encodeURIComponent(item_code);

	

	jQuery.ajax({

		url:url,

		dataType:"json",

		success:function(result){

			if (result.message != "success")

				return;



			var itemColorList = result.itemColorList;

			var selectText = "";

			var bChangeColor = false;

			

			for (var item_id in itemColorList){

				if (!bChangeColor){

					changeItemColor(item_cur_number, item_id);

					bChangeColor = true;

				}					

				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';

			}

			jQuery("#item_" + item_cur_number + " .color select").html(selectText);

		}

	});

}



/**************************************************

/* Get the item info when change the color

/**************************************************/

function changeItemColor(item_cur_number, item_id){

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/iteminfo/?id=" + item_id;	

	jQuery.ajax({

		url:url,

		dataType:"json",

		success:function(result){

			if (result.message != "success")

				return;

			

			var itemInfo = result.itemInfo;

			var shipping_price = 0;

			

			var ant_profit_wod = 0; //anticipated profit without Discounts

			//ANT PROFIT = (Sale Price)*.97 - (Cost Price + 100*item CBM) 

			

			if (jQuery("#shipping_method").val() == "Shipping"){

				shipping_price = itemInfo.shipping_price;

			} else if (jQuery("#shipping_method").val() == "Local Pickup"){

				shipping_price = itemInfo.local_pickup;

			} else if (jQuery("#shipping_method").val() == "Shipped from Los Angeles"){

				shipping_price = itemInfo.la_oc_shipping;

			} else if (jQuery("#shipping_method").val() == "Canada Shipping") {

				shipping_price = itemInfo.canada_shipping;

			}

			

			ant_profit_wod = parseFloat(itemInfo.sale_price)*0.97 - parseFloat(itemInfo.fob_cost_price) - 100*parseFloat(itemInfo.cbm);

			

			var str_action_field = "<input type='hidden' class='sale_price' value='" + itemInfo.sale_price + "'/>" 

				+ "<input type='hidden' class='shipping_price' value='" + itemInfo.shipping_price + "'/>" 

				+ "<input type='hidden' class='local_pickup_price' value='" + itemInfo.local_pickup + "'/>" 

				+ "<input type='hidden' class='la_oc_shipping_price' value='" + itemInfo.la_oc_shipping + "'/>"

				+ "<input type='hidden' class='canada_shipping_price' value='" + itemInfo.canada_shipping + "'/>"

				+ "<input type='hidden' class='ant_profit_wod' value='" + ant_profit_wod + "'/>"

				+ "<input type='hidden' class='item_id' value='" + itemInfo.id + "'/>"

				+ '<a href="#" class="delete_item">Delete</a>';

			

			jQuery("#item_" + item_cur_number + " .action").html(str_action_field);

			calculateTotal();

			

			var quantity = jQuery("#item_" + item_cur_number + " #quantity").val();

			getItemEta(item_cur_number, quantity);

		}

	});

}



jQuery("#shipping_method").change(function(){

	calculateTotal();

});



</script>