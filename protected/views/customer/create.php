<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Create',
);*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Import osCommerce Orders', 'url'=>array('import_osc_orders')),
);

?>

<h1>Create Customer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'pending'=>$pending, 'heardThroughList'=>$heardThroughList)); ?>