<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Create Customer', 'url'=>array('create')),
);

?>

<h1>Import osCommerce Orders</h1>

<?php echo CHtml::Button('Import osCommerce Order Info', array('id'=>'import_osc_data')); ?>

<?php 
$oscOrdersModel->status = OscOrders::$STATUS_NOT_IMPORTED;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-grid',
	'dataProvider'=>$oscOrdersModel->search(),
	'filter'=>$oscOrdersModel,
	'columns'=>array(
		'customers_name',
		'customers_email_address',
		'date_purchased',
		array(
			'header'=>'Total',
			'type'=>'raw',
			'value'=>'OscOrders::getOscOrderTotal($data->orders_id, $data->site_id)',
		),
		array(
			'name'=>'status',
			'filter'=>false
		),
		array(
			'header'=>'Action',
			'type'=>'raw',
			'value'=>'OscOrders::getOrdersActionButton($data->orders_id, $data->site_id)',
		),
	),
)); ?>

<script>
jQuery(document).ready(function(){
	jQuery("#import_osc_data").click(function(){
		var urlImportOrder = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/import_osc_data_ajax";
				
		jQuery.ajax({
			url:urlImportOrder,
			success:function(result){
				jQuery.fn.yiiGridView.update('customer-order-grid');
			}
		});
	});

	jQuery(".delete_order").live('click', function(e){
		if (!confirm('Are you sure?')){
			return false;
		}
		var order_id = jQuery(e.target).attr('rel');
		var site_id = jQuery(e.target).data('site-id');
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/delete_osc_order_ajax?order_id="+order_id+"&site_id="+site_id;

		jQuery.ajax({
			url:url,
			success:function(result){
				jQuery.fn.yiiGridView.update('customer-order-grid');
			}
		});
	});
});
</script>