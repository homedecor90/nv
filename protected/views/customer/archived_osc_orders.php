<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Create Customer', 'url'=>array('create')),
);

?>

<h1>Archived osCommerce Orders</h1>

<?php 
$oscOrdersModel->status = OscOrders::$STATUS_IMPORTED;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-grid',
	'dataProvider'=>$oscOrdersModel->search(),
	'filter'=>$oscOrdersModel,
	'columns'=>array(
		'customers_name',
		'customers_email_address',
		'date_purchased',
		array(
			'header'=>'Total',
			'type'=>'raw',
			'value'=>'OscOrders::getOscOrderTotal($data->orders_id)',
		),
		array(
			'name'=>'status',
			'filter'=>false
		),
		array(
			'header'=>'Action',
			'type'=>'raw',
			'value'=>'OscOrders::getOrdersActionButton($data->orders_id)',
		),
	),
)); ?>