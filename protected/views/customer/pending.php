<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);*/

?>

<h1>Pending Customers</h1>


<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pending-customer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'email',
		'phone',
		'enquiry',
		array(
            'header' => 'Date',
            'type' => 'raw',
            'value' => 'date("M. d, Y h:i:s A", strtotime($data->date))'
        ),
        array(
            'header'=>'Add',
            'type'=>'raw',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/create/?pending=\'.$data->id.\'">Add</a>\''
        ),
        array(
            'header'=>'Delete',
            'type'=>'raw',
            'value'=>'\'<a class="delete_link" rel="\'.$data->id.\'" href="#">Delete</a>\''
        ),
	),
)); ?>

<script type="text/javascript">
jQuery('.delete_link').live('click', function(e){
    var target = jQuery(e.target);
    var customer_id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/delete_pending_customer_ajax/?customer_id="+customer_id;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            $.fn.yiiGridView.update('pending-customer-grid');
        },
        dataType: 'json'
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});
</script>