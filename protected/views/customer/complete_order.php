<?php
$salesPerson = YumUser::model()->findByPk($customerOrderModel->sales_person_id);
$salesPersonName = isset($salesPerson->profile) ?
    $salesPerson->profile->firstname." ".$salesPerson->profile->lastname
    : '';
$price = CustomerOrder::model()->getCustomerOrderTotalPrice($customerOrderModel->id, true);
$total_price  = number_format($price['total_with_tax'], 2);
$order_tax    = $price['tax'];
$settings = Settings::model()->getSettingVariables();
$tax_percent = number_format($settings->tax_percent, 2);
?>

<?php
/////////////
//TotalTotalTotalTotalTotalTotalTotalTotalTotalTotal
?>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-grid',
	'dataProvider'=>$customerOrderItemsModel->search($customerOrderModel->id),
	'summaryText'=>'',
	'columns'=>array(
		array(
			'header'=>'Item Code',
			'value'=>'$data->items->item_code',
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
		),		
		'sale_price',
		array(
			'header'=>'Shipping Price',
			'value'=>'CustomerOrderItems::model()->getItemShippingPrice($data->id)',
		),
		'quantity',
	),
));
?>

<div class="checkout_order_info">
<?php
if ($order_tax):
?>
<h3 class="sales_tax">Sales Tax at <?php echo $tax_percent; ?>%: $<?php echo number_format($order_tax, 2);?></h3>
<?php
endif;
?>

<b>Grand Total:$<span id="grand_total_value"><?php echo number_format($customerOrderModel->grand_total_price, 2)?></span></b><br/>
<?php
	if ($customerOrderModel->discounted == "Discounted"){
?>
		<b>Discounted Total:$<span id="discounted_value"><?php echo number_format($customerOrderModel->discounted_total, 2)?></span></b>
<?php
        $amount = $customerOrderModel->discounted_total;
	} else {
        $amount = $customerOrderModel->grand_total_price;
    }
?>
</div>


<?php
if ($customerOrderModel->status == CustomerOrder::$STATUS_COMPLETED || $customerOrderModel->status == CustomerOrder::$STATUS_SOLD){
?>
	<h3>This order is completed</h3>
<?php
} elseif ($customerOrderModel->status == CustomerOrder::$STATUS_LOST || $customerOrderModel->status == CustomerOrder::$STATUS_DEAD) {
   ?>
    <h3>This order cannot be processed</h3>
<?php   
}

 else {
 ?>

    <div class="sales_tax">
        Sales tax: <input type="checkbox" id="sales_tax_check"<?php echo $customerOrderModel->add_sales_tax ? ' checked="checked"' : ''; ?> />
    </div>
    <?php echo CHtml::label('Shipping Method', 'shipping_method'); ?>
    <?php
        $shippingMethodList = array(
            'Shipping'=>'Shipping',
            'Local Pickup'=>'Local Pickup',
            'Shipped from Los Angeles'=>'Shipped from Los Angeles',
            'Canada Shipping'=>'Canada Shipping',
        );
    ?>
    <?php echo CHtml::dropDownList('shipping_method', $customerOrderModel->shipping_method, $shippingMethodList, array('id'=>'shipping_method'));?>

	<form id="checkout_form" method="POST">
		<h3>Shipping Address</h3>

		<div class="field_list" id="">
			<div class="field_row">
				<div class="field_label">Name:</div>
				<div class="field_content"><input type="text" size="20" value="<?php echo $orderBillingInfoModel->shipping_name; ?>" name="ShipFirstName" style="width:180px">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
            <div class="field_row">
				<div class="field_label">Last Name:</div>
				<div class="field_content"><input type="text" size="20" value="<?php echo $orderBillingInfoModel->shipping_last_name; ?>" name="ShipLastName" style="width:180px">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Street Address:</div>
				<div class="field_content"><font size="2"><input size="20" value="<?php echo $orderBillingInfoModel->shipping_street_address; ?>" name="ShipAddress">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">City:</div>
				<div class="field_content"><input type="text" value="<?php echo $orderBillingInfoModel->shipping_city; ?>" name="ShipCity">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">State/Province:</div>
				<div class="field_content">
					<?php /*
                    <select name="shipstate">
						<option value="Alabama">Alabama</option>
						<option value="Alaska">Alaska</option><option value="American Samoa">American Samoa</option>
						<option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option>
						<option value="Armed Forces Africa">Armed Forces Africa</option><option value="Armed Forces Americas">Armed Forces Americas</option>
						<option value="Armed Forces Canada">Armed Forces Canada</option><option value="Armed Forces Europe">Armed Forces Europe</option>
						<option value="Armed Forces Middle East">Armed Forces Middle East</option><option value="Armed Forces Pacific">Armed Forces Pacific</option>
						<option value="California">California</option><option value="Colorado">Colorado</option>
						<option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option>
						<option value="District of Columbia">District of Columbia</option><option value="Federated States Of Micronesia">Federated States Of Micronesia</option>
						<option value="Florida">Florida</option><option value="Georgia">Georgia</option>
						<option value="Guam">Guam</option><option value="Hawaii">Hawaii</option>
						<option value="Idaho">Idaho</option><option value="Illinois">Illinois</option>
						<option value="Indiana">Indiana</option><option value="Iowa">Iowa</option>
						<option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option>
						<option value="Louisiana">Louisiana</option><option value="Maine">Maine</option>
						<option value="Marshall Islands">Marshall Islands</option><option value="Maryland">Maryland</option>
						<option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option>
						<option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option>
						<option value="Missouri">Missouri</option><option value="Montana">Montana</option>
						<option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option>
						<option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option>
						<option value="New Mexico">New Mexico</option><option value="New York">New York</option>
						<option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option>
						<option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Ohio">Ohio</option>
						<option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option>
						<option value="Palau">Palau</option><option value="Pennsylvania">Pennsylvania</option>
						<option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option>
						<option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option>
						<option value="Tennessee">Tennessee</option><option value="Texas">Texas</option>
						<option value="Utah">Utah</option><option value="Vermont">Vermont</option>
						<option value="Virgin Islands">Virgin Islands</option><option value="Virginia">Virginia</option>
						<option value="Washington">Washington</option><option value="West Virginia">West Virginia</option>
						<option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option>
					</select>*/
                    ?>
                    <input type="text" name="shipstate" value="<?php echo $orderBillingInfoModel->shipping_state; ?>" />&nbsp;
					<span class="inputRequirement">*</span>
				</div>
			</div>
			<div class="field_row">
				<div class="field_label">Zip Code:</div>
				<div class="field_content"><input type="text" name="shippostcode" value="<?php echo $orderBillingInfoModel->shipping_zip_code; ?>" />&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label">Telephone Number:</div>
				<div class="field_content"><input type="text" name="shiptelephone" value="<?php echo $orderBillingInfoModel->shipping_phone_number; ?>">&nbsp;<span class="inputRequirement">*</span></div>
			</div>
			<div class="field_row">
				<div class="field_label"><b>This ship address is:</b></div>
				<div class="field_content">
					<input type="radio" checked="" value="Residental" name="busrestype">Residental
					<input type="radio" value="Business" name="busrestype">Business
				</div>
			</div>
		</div>
		<div class="field_list">
            <div class="field_row">
				<div class="field_label"><b>Order Notes:</b></div>
				<div class="field_content">
					<input type="text" value="" name="notes" />
				</div>
			</div>	
                         <div class="field_row">
				<div class="field_label"><b>Shipping Notes:</b></div>
				<div class="field_content">
					<input type="text" value="<?php echo $orderBillingInfoModel->shipping_notes; ?>" name="shipping_notes" />
				</div>
			</div>	
        </div>
        
		<input type="submit" id="btnCheckOut" value="Complete"/>
	</form>
<?php
}
?>

<script>
var order_id = <?php echo $customerOrderModel->id; ?>;
var apply_sales_tax = <?php echo $customerOrderModel->add_sales_tax; ?>;

jQuery(document).ready(function(){
	jQuery("#btnCheckOut").click(function(){
		bVerify = true;
		
		jQuery('form#checkout_form .field_list.active .field_content input[type="text"]').each(function(){			
			if (jQuery(this).val().trim() == ""){
				jQuery(this).focus();
				bVerify = false;
				return false;
			}
		});		
		
		if (bVerify == false){
			alert("Please input the required value");
			return false;
		}
	});
});

jQuery('#sales_tax_check').change(function(e){
    var state = apply_sales_tax ? 0 : 1;
    
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/set_order_add_tax_ajax/?order_id="+order_id+"&state="+state;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            var new_amount = (result.new_total == result.new_discounted) ? result.new_total : result.new_discounted;
            jQuery('#grand_total_value').text(result.new_total);
            jQuery('#discounted_value').text(result.new_discounted);
            jQuery('#amount').val(new_amount.replace(',',''));
            
            if (!state){
                jQuery('h3.sales_tax').hide();
            }
            
            apply_sales_tax = state;
            
            // jQuery('#grand_total_'+order_id).text(result.new_total);
        },
        dataType: 'json'
    });
});

jQuery('#shipping_method').live('change', function (e){
    var method = jQuery(e.target).val();
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/set_order_shipping_method_ajax/?order_id="+order_id+"&method="+method;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            var new_amount = (result.new_total == result.new_discounted) ? result.new_total : result.new_discounted;
            jQuery('#grand_total_value').text(result.new_total);
            jQuery('#discounted_value').text(result.new_discounted);
            jQuery('#amount').val(new_amount.replace(',',''));

            // jQuery('#grand_total_'+order_id).text(result.new_total);
        },
        dataType: 'json'
    });
});
</script>
