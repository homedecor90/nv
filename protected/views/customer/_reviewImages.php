<?php
/**
 * @var CustomerOrderReview $data
 */
if (!empty($data->images)) {
    foreach ($data->images as $key => $image) {
        $link = $key == 0 ? Yii::app()->easyImage->thumbOf(
            Yii::getPathOfAlias('webroot') . $image->filename,
            array(
                'resize' => array('width' => 100, 'height' => 100),
            )
        ) : '';
        echo CHtml::link(
            $link,
            $image->filename, array(
                'class' => 'fancybox',
                'rel' => 'review-'.$data->id,
            )
        );
    }
}
