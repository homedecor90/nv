<?php
echo '<div style="text-align: left;">';
echo '<strong>Order #'.$order->id.'</strong><br />';
echo '<strong>Sold On: </strong>'.date("M. d, Y", strtotime($order->sold_date)).'<br />';
echo '<strong>Added On: </strong>'.date("M. d, Y", strtotime($order->created_on)).'<br />';
echo '<strong>Shipping Method: </strong>'.$order->shipping_method.'<br />';
echo '<input type="button" class="order_expand_shipment_details" rel="'.$order->id.'" value="Expand Details" /><br />';
if ($order->shipping_status == CustomerOrder::$SHIPPING_STATUS_PENDING){
	echo '<input type="button" class="cancel_order" value="Cancel" rel="'.$order->id.'" />';
	echo '<input type="button" class="chargeback_order" value="Chargeback" rel="'.$order->id.'" />';
}
if ($order->verificationRequired()){
	echo '<input type="button" class="order_verification_request" rel="'.$order->id.'" value="Verification Request" />';
	echo '<input type="button" rel="'.$order->id.'" class="order_verify" value="Verified" />';
}

echo '</div>';
echo '<table class="items">';
echo '<tr>';
echo '<th>Item</th>';
echo '<th>Qty</th>';
echo '<th>Status</th>';
echo '<th>Action</th>';
echo '</tr>';
$shipment_index = 0;
$items_arr = array();
foreach ($i_items as $i=>$i_item){
	if ($i == 0 || $i_item->customer_order_shipment_id != $i_items[$i-1]->customer_order_shipment_id){

		if ($i>0){
			foreach ($items_arr as $j=>$row){
				echo '<tr class="'.($j%2 ? 'even' : 'odd').'">';
				echo '<td>';
				echo $row[0]->items->item_name;
				echo '</td>';
				echo '<td>'.count($row).'</td>';
				echo '<td>';
				echo $row[0]->status;
				echo '</td>';
				echo '<td></td>';
				echo '</tr>';
			}
		}
		$items_arr = array();

		$shipment = $shipments[$shipment_index];
		echo '<tr>';
		echo '<td colspan="2"><strong>Shipment #'.$shipment->id.'</strong></td>';
		echo '<td>'.$shipment->status;
		$p_time = strtotime($shipment->pickup_date);
		if ($p_time>0 || $shipment->tracking_number){
			echo '<br />(';
			if ($p_time>0){
				echo 'Pickup Date: '.date('M. d, Y', $p_time).'; ';
			}
			echo 'Tracking Number: '.$shipment->tracking_number.'; ';
			echo 'Tracking Website: '.$shipment->tracking_website;
			if ($shipment->claim){
				echo '; Damaged on: '.date('M. d, Y', strtotime($shipment->claim_date)).')';
			} else {
				echo ')';
			}
		}
		echo '</td>';
		echo '<td>';
		// sow shipment actions
		$buttons = array();
		if (!$s_actions[$shipment->id]){
			$s_actions[$shipment->id] = array();
		}
		foreach ($s_actions[$shipment->id] as $action) {
			switch ($action){
				case 'scan_as_out': $buttons[] = '<input type="button" class="scan_out_shipment" rel="'.$shipment->id.'" value="Scan As Out" />';
				break;
				
				case 'send_for_bidding': $buttons[] = '<input type="button" class="send_for_bidding_shipment" rel="'.$shipment->id.'" value="Send For Bidding" />';
				break;
				
				case 'schedule_pickup': $buttons[] = '<input type="button" class="schedule_pickup" rel="'.$shipment->id.'" value="Schedule Customer Pickup" />';
				break;
				
				case 'mark_as_damaged': $buttons[] = '<input type="button" class="damage" rel="'.$shipment->id.'" value="Mark As Damaged" />';
				break;
				
				case 'requeue': $buttons[] = '<input type="button" class="requeue" rel="'.$shipment->id.'" value="Requeue" />';
				break;
				
				case 'cancel': $buttons[] = '<input type="button" class="cancel_shipment" rel="'.$shipment->id.'" value="Cancel" />';
				break;
			}
		}
		echo implode('<br />', $buttons);
		echo '</td>';
		echo '</tr>';
		$shipment_index++;

	}
	$items_arr[$i_item->item_id.$i_item->status][] = $i_item;
		
}
foreach ($items_arr as $j=>$row){
	echo '<tr class="'.($j%2 ? 'even' : 'odd').'">';
	echo '<td>';
	echo $row[0]->items->item_name;
	echo '</td>';
	echo '<td>'.count($row).'</td>';
	echo '<td>';
	echo $row[0]->status;
	echo '</td>';
	echo '<td></td>';
	echo '</tr>';
}
echo '</table>';