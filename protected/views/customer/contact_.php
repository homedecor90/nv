<?
/**
 * @var CustomerController $this
 * @var PendingCustomer $model
 * @var CActiveForm $form
 */
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'contact-form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        //'action' => $this->createAbsoluteUrl('/customer/contact'),
        'focus'=>array($model,'name'),
    )); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone'); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'enquiry'); ?>
        <?php echo $form->textArea($model,'enquiry'); ?>
        <?php echo $form->error($model,'enquiry'); ?>
    </div>
    <div class="row">
        <?php $this->widget('application.extensions.recaptcha.EReCaptcha', array(
            'model'=>$model,
            'attribute'=>'verifyCode',
            'theme'=>'red',
            'language'=>'en',
            'publicKey'=>'6LdW4-0SAAAAAAG2v33kVhhjz66dO6VzO6M4_PPk',
        )); ?>
    </div>
    <div class="row">
        <?php echo CHtml::imageButton('/images/button_images/button_continue.gif');?>
    </div>
    <?php $this->endWidget();?>
</div>