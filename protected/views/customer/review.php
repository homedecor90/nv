<?php
/**
 * @var CActiveForm $form
 */

if (!$order->review_placed):

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo CHtml::label('Your Review', 'review_text'); ?>
		<?php echo $form->textArea($model, 'review_text', array('rows'=>7, 'cols'=>60));?>
		<?php echo $form->error($model,'review_text'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Rate your overall satisfaction with Regency Shop', 'overall'); ?>
		<?php echo $form->dropDownList($model, 'overall', CustomerOrderReview::$overall_list);?>
		<?php echo $form->error($model,'overall'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Rate the cost of products', 'cost'); ?>
		<?php echo $form->dropDownList($model, 'cost', CustomerOrderReview::$cost_list);?>
		<?php echo $form->error($model,'cost'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::label('How likely are you to shop at Regency Shop in the future?', 'future_shopping'); ?>
		<?php echo $form->dropDownList($model, 'future_shopping', CustomerOrderReview::$future_shopping_list);?>
		<?php echo $form->error($model,'future_shopping'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::label('Rate Shipping Process overall', 'shipping'); ?>
		<?php echo $form->dropDownList($model, 'shipping', CustomerOrderReview::$shipping_list);?>
		<?php echo $form->error($model,'shipping'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::label('Rate Customer Service overall', 'customer_service'); ?>
		<?php echo $form->dropDownList($model, 'customer_service', CustomerOrderReview::$customer_service_list);?>
		<?php echo $form->error($model,'customer_service'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::label('If you returned a product for refund or exchange, rate returns service overall, or choose n/a', 'return'); ?>
		<?php echo $form->dropDownList($model, 'return', CustomerOrderReview::$return_list);?>
		<?php echo $form->error($model,'return'); ?>
	</div>

    <div class="row">
        <p>We love to see your pictures of the "<?php echo implode(', ', $order->itemNames);?>". Please share :)</p>
        <a id="add-images" href="#image-upload">Add Images</a>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#add-images').click(function(){
                    $('#image-upload').dialog({
                        title: 'Add images',
                        width: 560
                    });
                });
            });
        </script>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit', array('id'=>'submitButton')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
<div id="image-upload" style="width: 640px; display: none;">
    <?php
    $this->widget('ext.xupload.XUpload', array(
        'model' => new ReviewImageUploadForm(),
        'attribute' => 'file',
        'url' => $this->createUrl('/customerOrderReview/uploadImage', array(
            'order_id' => $order->id
        )),
        'multiple' => true,
        'showForm' => true,
        'autoUpload' => true,
        'formView' => 'application.views.xupload.form',
        'options' => array(
            'maxNumberOfFiles' => 3,
            'progress' => 'js: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                data.context.find(".progress").progressbar({value: progress});
            }'
        ),
    ));?>
</div>
<?php
else:
if ($post_data){
	echo '<p>Congratulations! Your review has been selected to be shared, so other buyers can make an informed decision :)<br />
'.$order->customer->first_name.' '.$order->customer->last_name.', we really appreciate your business.<br />
<strong>Please check your email '.$order->customer->email.' to verify your review.</strong></p>';
} else {
	echo '<p>Thank you, your feedback has been recorded. It will be forwarded to our managers. '.$order->customer->first_name.' '.$order->customer->last_name.', we really appreciate your business.</p>';
}

endif;
?>
