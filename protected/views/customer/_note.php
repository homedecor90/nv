<h2>Customer Shipments</h2>
<?php 
echo CancellationReason::getCancellationReasonCode();
echo CustomerOrder::getScanOutFormCode();

$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'customer-sold-orders-grid',
		'dataProvider'=>$model->searchShipmentsForNotes(),
		'columns'=>array(
				'id',
				array(
						'header'=>'Customer Order ID',
						'value'=>'$data->customer_order_id
						.($data->return ? \'<br /><br/>[Return]\' : \'\')',
						'type'=>'raw',
				),
				array(
						'header'=>'Status',
						'value'=>'$data->getStatusExt()',
						'type'=>'raw',
				),
				array(
						'header'=>'Shipment Data',
						'value'=>'$data->getItemStatsTableNotes()',
						'type'=>'raw',
				),
				array(
						'header'=>'Action',
						'value'=>'$data->getActionButtonsStr()',
						'type'=>'raw',
				),
		),
));

// foreach ($model->sold_orders as $order){
	
// 	echo $order->getItemStatsTableNotes();
// }
?>
<h2>Customer Notes</h2>
<div class="form">

<input type="button" value="Expand Order Details" href="#order_details_container" id="order_details_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_details_button',
    'config'=>array(
		'width'=>900,
		'height'=>500,
		'autoDimensions'=>false,
    	'onClosed'=>'js:function(){if(order_details_need_update_window){refreshWindow(); order_details_need_update_window = false; }}'
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="order_details_container" class="fancy_box_div">
<div id="order_details_content"></div>
</div>

</div>

<input type="button" value="Switch Order Shipping Method" href="#order_shipping_method_div" id="order_shipping_method_button" style="display:none;"/>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_shipping_method_button',
    'config'=>array(
		'width'=>600,
		'height'=>450,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="order_shipping_method_div" class="fancy_box_div">
		<div id="order_shipping_method_form_container">
		</div>
	</div>
</div>

<input type="button" value="Combine Orders" href="#combine_orders_container" id="combine_orders_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#combine_orders_button',
    'config'=>array(
		'width'=>300,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="combine_orders_container" class="fancy_box_div">
<div id="combine_orders_content"></div>
	<h3>Combine Orders</h3>
	<p>
		<strong>Order ID: </strong><span id="combine_orders_order_id"></span><br />
		<strong>Customer: </strong><span id="combine_orders_customer"></span>
	</p>
	<strong>Shipping Info: </strong><div id="combine_orders_shipping_info"></div>
	<label for="combine_orders_copy_info">Copy shipping info: </label>
	<input type="checkbox" id="combine_orders_copy_info" value="1" />
	<br /><br />
	<input type="button" value="Combine" id="combine_orders_submit" />
</div>

<input type="button" value="Schedule Pickup" href="#schedule_pickup_div" id="schedule_pickup_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#schedule_pickup_button',
    'config'=>array(
		'width'=>320,
		'height'=>200,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="schedule_pickup_div" class="fancy_box_div">
		<form id="schedule_pickup_form" class="fancy_form">
			<h2>Schedule Pickup</h2>
			<div>
				<input type="hidden" name="order_id" id="pickup_shipment_id" value=""/>
				<input type="radio" checked="checked" name="schedule_option" value="admin" id="schedule_by_admin" />
				<label for="schedule_by_admin">Schedule pickup time by admin</label><br />
				<label>Pickup Date</label>
				<?php
					Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
					$this->widget('CJuiDateTimePicker',array(
						'mode'=>'datetime', //use "time","date" or "datetime" (default)
						'language'=>'en-GB',
						'name'=>'pickup_date',
						'options'=>array(
							"dateFormat"=>"M. dd, yy",
							"timeFormat"=>"hh:mm tt",
							"ampm"=>true
						),
						'htmlOptions' => array(
                            'id' => 'pickup_date'
                        )
					));
				?>
				<br /><br />
				<input type="radio" name="schedule_option" value="customer" id="schedule_by_customer" />
				<label for="schedule_by_customer">Schedule pickup time by customer</label><br />
			</div>
			
			<input type="button" value="Save" id="schedule_pickup_submit"/>
		</form>
	</div>
</div>

</div>
<?php 
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'customer-note-form',
		'enableAjaxValidation'=>false,
)); ?>
	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($newCustomerNoteModel); ?>

	<?php echo $form->hiddenField($newCustomerNoteModel, 'customer_id', array('value'=>$model->id)); ?>

	<?php
	if (YumUser::model()->isSalesPerson(true) || YumUser::model()->isOperator(true)) {
		$user_id = Yii::app()->user->id;
		echo $form->hiddenField($newCustomerNoteModel, 'sales_person_id', array('value'=>$user_id));
	} else {

		?>
	<div class="row">
		<?php echo $form->labelEx($newCustomerNoteModel,'sales_person_id'); ?>
		<?php echo $form->dropDownList($newCustomerNoteModel, 'sales_person_id', $salesPersonList);  ?>
		<?php echo $form->error($newCustomerNoteModel,'sales_person_id'); ?>
	</div>
	<?php	
	}
	?>

	<div class="row">
		<?php echo $form->labelEx($newCustomerNoteModel,'content'); ?>
		<?php echo $form->textArea($newCustomerNoteModel,'content',array('cols'=>60, 'rows'=>6, 'id'=>'content_textarea')); ?>
		<?php echo $form->error($newCustomerNoteModel,'content'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::Button('Add New Note', array('id'=>'add_new_note')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
<!-- form -->

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'customer-note-grid',
		'dataProvider'=>$customerNoteModel->search($model->id),
		'columns'=>array(
				array(
						// 'header'=>'Sales Person',
						// TODO: field is still called "sales person", probably we will have to rename it
						'header'=>'User',
						'value'=>'YumUser::model()->findByPk($data->sales_person_id)->profile->firstname." ".YumUser::model()->findByPk($data->sales_person_id)->profile->lastname',
				),
				array(
						'header'=>'Content',
						'value'=>'$data->getContentWithAttachments()',
						'type'=>'raw',
				),
				array(
						'header'=>'Created On',
						'value'=>'date("M. d, Y h:i:s A", strtotime($data->created_on))',
				),
		),
));
?>

<input
	type="button" value="Shipment Damaged"
	href="#shipment_damage_container" id="shipment_damage_button"
	style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
		'target'=>'#shipment_damage_button',
		'config'=>array(
				'width'=>400,
				'height'=>400,
				'autoDimensions'=>false,
		),
));
?>

<div class="fancy_box_div_wrapper" style="display: none;">
	<div id="shipment_damage_container" class="fancy_box_div">
		<div id="shipment_damage_content"></div>
	</div>

</div>

<script>
order_details_need_update_window = false;

jQuery('.cancel_order').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/cancel_order_ajax/?order_id='+id;
	cancelShippingWindow(url, function(){refreshWindow();});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	return false;
});

jQuery('.cancel_shipment').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/cancel_shipment_ajax/?shipment_id='+id;
	cancelShippingWindow(url, function(){refreshWindow();});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	return false;
});

jQuery('.cancel_item').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/individualitem/cancel_from_customer_order_ajax/?ind_item_id='+id;
	cancelShippingWindow(url, function(){refreshWindow();});
	
	e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

jQuery('.scan_out_shipment').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/scan_shipment_out_ajax/?shipment_id='+id;
	scanOutWindow(url, function(){
		refreshWindow();
	});
	
	e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

jQuery('.chargeback_order').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/order_chargeback_ajax/?order_id='+id;
	cancelShippingWindow(url, function(){refreshWindow();});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	return false;
});

jQuery('.send_for_bidding_order').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
    var id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/order_send_for_bidding_ajax/?id="+id;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            refreshWindow();
        },
        dataType: 'json'
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('.send_for_bidding_shipment').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
    var id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/shipment_send_for_bidding_ajax/?id="+id;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            refreshWindow();
        },
        dataType: 'json'
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('.order_verification_request').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/order_shipping_info_verification_request_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert('Failed to send notification');
				return;
			}
			
			if (result.status == "success"){
				alert('Notification sent');
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

function refreshWindow(){
	//window.location.hash = 'note'; 
	//window.location.reload();
	$.fn.yiiGridView.update('customer-sold-orders-grid');
	$.fn.yiiGridView.update('customer-note-grid');
}

jQuery('.order_verify').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/verify_order_shipping_info_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert('Failed to send notification');
				return;
			}
			
			if (result.status == "success"){
				$.fn.yiiGridView.update('customer-note-grid');
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery(document).ready(function(){
	jQuery("#add_new_note").click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/add_new_note_ajax";
		var postData = jQuery("form#customer-note-form").serialize();		
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}
				
				jQuery.fn.yiiGridView.update('customer-note-grid');
				jQuery("#content_textarea").val("");
			}
		});
	});
});

jQuery('.damage').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/shipment_mark_as_damaged_form_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#shipment_damage_content').html(result);
			jQuery('#shipment_damage_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#shipment_damage_submit').live('click', function(e){
	var checked = false;
	var boxes = [];
	var id = jQuery('#shipment_damage_id').val();
	
	jQuery('#shipment_damage_content input:checked').each(function(i){
		checked = true;
		boxes.push(jQuery(this).val());
	});
	
	if (!checked){
		alert('Please select boxes to mark');
		return false;
	}

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/mark_shipment_as_damaged_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {boxes: boxes},
		success:function(result){
			if (typeof(result.status) == 'undefined'){
				alert(result);
				return;
			}
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				jQuery.fancybox.close();
				refreshWindow();
			}
		},
		error:function(result){
			alert(result.responseText);
		}
	});
});

jQuery('.scan_out_customer_pickup').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
	var shipment_id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/scan_shipment_out_ajax/?shipment_id=" + shipment_id;
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}

			refreshWindow();
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

</script>
