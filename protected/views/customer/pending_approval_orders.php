<h1>Pending Approval Orders</h1>

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-pending-approval-order-grid',
	'dataProvider'=>$customerOrderModel->searchPendingApproval(),
	'columns'=>array(
		'id',
        array(
			'header'=>'Added On',				
			'value'=>'date("M. d, Y", strtotime($data->created_on))',
		),
        array(
			'header'=>'Billing Name',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/customer/\'.$data->customer_id.\'">\'.$data->billing_info->billing_name.\' \'.$data->billing_info->billing_last_name.\'</a>\'',
			'type'=>'raw',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
            'value'=>'CustomerOrder::model()->getCustomerOrderItemsShort($data->id)',
		),
		array(
			'header'=>'Shipping Method',
			'type'=>'raw',
            'value'=>'\'<span id="shipping_method_\'.$data->id.\'">\'.$data->shipping_method.\'</span>\'',
		),
		array(
			'header'=>'Price',
			'type'=>'raw',
            'value'=>'\'Total: $\'.$data->grand_total_price.\'<br />Discounted: $\'.$data->discounted_total',
		),
        array(
            'header'=>'Action',
            'type'=>'raw',
            'value'=>'\'<input type="button" value="Approve" class="approve_order" rel="\'.$data->id.\'" /><br />
						<input type="button" value="Deny" class="deny_order" rel="\'.$data->id.\'" />\'',
        ),
	),
));
?>

<script type="text/javascript">
jQuery('.approve_order').live('click', function(e){
	var order_id = jQuery(e.target).attr('rel');
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/approve_order_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				$.fn.yiiGridView.update('customer-pending-approval-order-grid');
			}
		}
	});
});

jQuery('.deny_order').live('click', function(e){
	var order_id = jQuery(e.target).attr('rel');
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/deny_order_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				$.fn.yiiGridView.update('customer-pending-approval-order-grid');
			}
		}
	});
});
</script>
