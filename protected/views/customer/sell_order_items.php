<?php
/*$this->breadcrumbs=array(
	'Customers'=>array('index'),
	"View Customer Order"
);*/

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
	array('label'=>'Create Customer', 'url'=>array('create')),
	array('label'=>'Update Customer', 'url'=>array('update', 'id'=>$customerOrderModel->customer_id)),
	array('label'=>'View Customer', 'url'=>array('view', 'id'=>$customerOrderModel->customer_id)),
);
?>

<h1>View Customer Order #<?php echo $customerOrderModel->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'id'=>'customer-order-detail-grid',
	'data'=>$customerOrderModel,
	'attributes'=>array(
		'id',
		array(
			'label'=>'Sales Person',
			'value'=>$salesPersonName,
		),
		array(
			'label'=>'Customer',
			'value'=>$customerName,
		),
		array(
			'label'=>'Status',
			'value'=>$customerOrderModel->status,
			'cssClass'=>'tr_status',
		),		
	),
)); 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-items-grid',
	'dataProvider'=>$customerOrderItemsModel->search($customerOrderModel->id),
	'columns'=>array(
		array(
			'header'=>'Item Code',
			'value'=>'$data->items->item_code',
			'filter'=>CHtml::activeTextField($customerOrderItemsModel, 'item_code'),
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
			'filter'=>CHtml::activeTextField($customerOrderItemsModel, 'color'),
		),
		'sale_price',
		'quantity',
		array(
			'header'=>'Availability',
			'value'=>'Item::model()->getAvailability($data->id, $data->quantity)',
			'type'=>'raw'
		),
	),
));
?>

<?php echo CHtml::Button('Sell Items', array('id'=>'sell_order_items')); ?>

<script>
var order_id = <?php echo $customerOrderModel->id; ?>;
jQuery(document).ready(function(){
	jQuery("#sell_order_items").click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/sell_order_items_by_ajax/?order_id=" + order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){				
				jQuery.fn.yiiGridView.update('customer-order-items-grid');
				var tr_status = jQuery(".tr_status");
				var table_detail = tr_status.parent();
				tr_status.remove();
				table_detail.append('<tr class="even"><th>Status</th><td>Sold</td></tr>');
				jQuery("#sell_order_items").remove();
				
			}
		});
	});
});
</script>