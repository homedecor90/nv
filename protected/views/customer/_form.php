<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'heard_through'); ?>
		<?php echo $form->dropDownList($model, 'heard_through', $heardThroughList);?>
		<?php echo $form->error($model,'heard_through'); ?>
	</div>
    
    <input type="hidden" name="pending" value="<?php echo $pending; ?>" />
	<div class="row buttons">
		<?php echo CHtml::button($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submitButton')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
jQuery(document).ready(function(){
	jQuery("#submitButton").click(function(){
		if (jQuery(this).val() == "Create"){
			var postData = jQuery("#customer-form").serialize();
			
			var url_check = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/check_name_duplicated_ajax";
			jQuery.ajax({
				url:url_check,
				dataType:"json",
				data: postData,
				type: "POST",
				success:function(result){
					if (result.message == "duplicated"){
						if (confirm("Customer name is duplicated. Do you want to add this customer?") == false){
							return;
						}
					}
					
					jQuery("#customer-form").submit();
				}
			});
		} else {
			jQuery("#customer-form").submit();
		}
	});
});
</script>
