<?
/**
 * @var $data CustomerOrder
 */
?>
<span class="clickable customer_name" rel="<?=$data->id?>">
    <? CustomerOrder::model()->getSalesPersonName($data->id);?>
</span>
<div style="display: none;" id="customer_info_<?=$data->id?>">
    <strong>Shipping Info:</strong><br>
    <? if (isset($data->billing_info)):?>
        <?=$data->billing_info->shipping_name?>
        <?=$data->billing_info->shipping_last_name?><br><br>
        <? $data->getShippingAddressStr();?>
    <? endif;?>
    <br><br>
    <strong>Billing Info:</strong><br>
    <? if (isset($data->billing_info)):?>
        <?=$data->billing_info->billing_name?>
        <?=$data->billing_info->billing_last_name?><br><br>
        <? $data->getBillingAddressStr();?>
    <? endif;?>