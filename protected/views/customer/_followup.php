<h2>Customer Followup</h2>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-followup-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="followup">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($newCustomerFollowupModel); ?>
	
	<?php echo $form->hiddenField($newCustomerFollowupModel, 'customer_id', array('value'=>$model->id));  ?>		

	<?php
		if (YumUser::model()->isSalesPerson(true)) {
			$user_id = Yii::app()->user->id;
			echo $form->hiddenField($newCustomerFollowupModel, 'sales_person_id', array('value'=>$user_id));
		} else {
		
	?>
			<div class="row">
				<?php echo $form->labelEx($newCustomerFollowupModel,'sales_person_id'); ?>
				<?php echo $form->dropDownList($newCustomerFollowupModel, 'sales_person_id', $salesPersonList);  ?>		
				<?php echo $form->error($newCustomerFollowupModel,'sales_person_id'); ?>
			</div>
	<?php	
		}
	?>
	
	<div class="row">
		<?php echo $form->labelEx($newCustomerFollowupModel,'followup_time'); ?>
		<?php // echo $form->textField($newCustomerFollowupModel,'followup_time',array('size'=>60,'maxlength'=>100)); ?>
		
		<?php
			Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
			$this->widget('CJuiDateTimePicker',array(
				'model'=>$newCustomerFollowupModel, //Model object
				'attribute'=>'followup_time', //attribute name
				'mode'=>'datetime', //use "time","date" or "datetime" (default)
				'language'=>'en-GB',
				'options'=>array(
					"dateFormat"=>"M. dd, yy",
					"timeFormat"=>"hh:mm tt",
                    "ampm"=>true
				)
			));
		?>
		<?php echo $form->error($newCustomerFollowupModel,'followup_time'); ?>
	</div>

	<div class="row buttons"><?php echo CHtml::Button('Add New Followup', array('id'=>'add_new_followup')); ?></div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-followup-grid',
	'dataProvider'=>$customerFollowupModel->search($model->id),
	'ajaxUpdate'=>true,
	'columns'=>array(
		array(
			'header'=>'Sales Person',
            'value'=>'YumUser::model()->findByPk($data->sales_person_id)->profile->firstname." ".YumUser::model()->findByPk($data->sales_person_id)->profile->lastname',
		),
		array(
			'header'=>'Followup Time',				
			'value'=>'date("M. d, Y h:i:s A", strtotime($data->followup_time))',
		),
		array(
			'header'=>'Created On',				
			'value'=>'date("M. d, Y h:i:s A", strtotime($data->created_on))',
		),
	),
));
?>

<script>
jQuery(document).ready(function(){
	jQuery("#add_new_followup").click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/add_new_followup_ajax";
		var postData = jQuery("form#customer-followup-form").serialize();		
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}
				
				jQuery.fn.yiiGridView.update('customer-followup-grid');
				jQuery("#followup_time_text").val("");
			}
		});
	});
});
</script>