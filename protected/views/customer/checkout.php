<?php
/**
 * @var $this CustomerController
 * @var $itemsDataProvider CActiveDataProvider
 * @var $order CustomerOrder
 * @var $billingInfo CustomerOrderBillingInfo
 * @var $form CActiveForm
 */
?>
<?php Yii::app()->clientScript->registerScriptFile(
    Yii::app()->baseUrl . '/js/backend.js', CClientScript::POS_END
); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'order-items-grid',
    'dataProvider' => $itemsDataProvider,
    'summaryText' => false,
    'columns'=>array(
        array(
            'header'=>'Item Code',
            'value'=>'$data->items->item_code',
        ),
        array(
            'header'=>'Color',
            'value'=>'$data->items->color.($data->custom_color ? \' (\'.$data->custom_color.\')\' : \'\')',
        ),
        'sale_price',
        array(
            'header'=>'Shipping Price',
            'value'=>'CustomerOrderItems::model()->getItemShippingPrice($data->id)',
        ),
        'quantity',
    ),
));?>

<div class="checkout_order_info">
    <h3 class="sales_tax"
        <?php if (!$order->add_sales_tax):?>style="display: none"<?php endif;?>
        >Sales Tax at <?php echo Settings::getVar('tax_percent'); ?>%:
        $<?php echo number_format($order->grand_total_price * Settings::getVar('tax_percent') / 100, 2);?></h3>
    <b>Grand Total: $<span id="grand_total_value"><?php echo number_format($order->grand_total_price, 2)?></span></b><br/>
    <?php if ($order->discounted == "Discounted"):?>
        <b>Discounted Total:$<span id="discounted_value"><?php echo number_format($order->discounted_total, 2)?></span></b><br/>
    <?php endif;?>
    <?php if (Yii::app()->user->isAdmin()):?>
    <b>Account used: <?php echo Settings::getVar('payment_processor');?></b>
    <?php endif;?>
</div>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'checkout_form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
    //'focus'=>array($model,'firstName'),
)); ?>
<?php echo $form->errorSummary(array($billingInfo, $order));?>
<div class="sales_tax">
    <?php echo $form->labelEx($order,'add_sales_tax'); ?>
    <?php echo $form->checkbox($order,'add_sales_tax', array(
        'class' => 'ajax-value-update',
        'data-ajax-url' => $this->createUrl('customer/set_order_add_tax_ajax', array(
            'order_id' => $order->id,
            'state' => '{value}',
        ))
    )); ?>
</div>
<?php echo $form->labelEx($order,'shipping_method'); ?>
<?php echo $form->dropdownList($order,'shipping_method',
    CustomerOrder::getShippingMethods(), array(
        'class' => 'ajax-value-update',
        'data-ajax-url' => $this->createUrl('customer/set_order_shipping_method_ajax', array(
            'order_id' => $order->id,
            'method' => '{value}',
        ))
    )
); ?>
<div>

    <h3>Credit Card</h3>
    <div class="field_list active">
        <input type="hidden" readonly="readonly" id="amount" name="amount" value="<?php echo $amount;?>">

        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'cc_number'); ?></div>
            <div class="field_content"><?php echo $form->textField($billingInfo,'cc_number', array(
                    'autocomplete' => 'off',
                    'value' => '',
                )); ?></div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo CHtml::activeLabel($billingInfo, 'cc_expiry_date', array('required' => true)); ?></div>
            <div class="field_content">
                <?php echo $form->dropdownList($billingInfo,'cc_exp_month', CustomerOrderBillingInfo::monthList()); ?>
                <?php echo $form->dropdownList($billingInfo,'cc_exp_year', CustomerOrderBillingInfo::yearsList()); ?>
            </div>
        </div>
        <div class="field_row">
            <div class="field_label">Credit Card Type:</div>
            <div class="field_content">
                <?php echo $form->dropdownList($billingInfo,'cc_type', CustomerOrderBillingInfo::cardTypes()); ?>
            </div>
        </div>
        <?php echo $form->hiddenField($billingInfo, 'cc_name_on_card');?>
    </div>
    <h3>Billing Address</h3>
    <div class="field_list active">
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_name'); ?></div>
            <div class="field_content">
                <?php echo $form->textField($billingInfo,'billing_name', array(
                    'value' => isset($billingInfo->billing_name) ?
                            $billingInfo->billing_name : $order->customer->first_name
                )); ?>
            </div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_last_name'); ?></div>
            <div class="field_content">
                <?php echo $form->textField($billingInfo,'billing_last_name', array(
                    'value' => isset($billingInfo->billing_last_name) ?
                            $billingInfo->billing_last_name : $order->customer->last_name
                )); ?>
            </div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_street_address'); ?></div>
            <div class="field_content"><?php echo $form->textField($billingInfo,'billing_street_address'); ?></div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_city'); ?></div>
            <div class="field_content"><?php echo $form->textField($billingInfo,'billing_city'); ?></div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_state'); ?></div>
            <div class="field_content"><?php echo $form->textField($billingInfo,'billing_state'); ?></div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_zip_code'); ?></div>
            <div class="field_content"><?php echo $form->textField($billingInfo,'billing_zip_code'); ?></div>
        </div>
        <div class="field_row">
            <div class="field_label">
                <?php echo $form->labelEx($billingInfo,'billing_phone_number'); ?>
            </div>
            <div class="field_content">
                <?php echo $form->textField($billingInfo,'billing_phone_number', array(
                    'value' => isset($billingInfo->billing_phone_number) ?
                            $billingInfo->billing_phone_number : $order->customer->phone
                )); ?>
            </div>
        </div>
        <div class="field_row">
            <div class="field_label"><?php echo $form->labelEx($billingInfo,'billing_email_address'); ?></div>
            <div class="field_content">
                <?php echo $form->textField($billingInfo,'billing_email_address', array(
                    'value' => isset($billingInfo->billing_email_address) ?
                            $billingInfo->billing_email_address : $order->customer->email
                )); ?>
            </div>
        </div>
    </div>
    <h3>Shipping Address</h3>

    <div class="field_list active">
        <div class="field_row">
            <div class="field_label"><b>My shipping address is:</b></div>
            <div class="field_content">
                <input type="radio" id="my_billing_address" checked="" value="0" name="sh_radio">My billing address.
                <input type="radio" id="different_address" value="1" name="sh_radio">A different address.
            </div>
        </div>
        <div class="field_row">
            <div class="field_label"><b>This ship address is:</b></div>
            <div class="field_content">
                <?php echo $form->radioButtonList($billingInfo,'busrestype',
                    CustomerOrderBillingInfo::ListAddressTypes(),
                    array(
                        'class' => 'radiolist',
                        'template' => '<span class="radio">{input}{label}</span>',
                        'separator' => '&nbsp;'
                    )
                ); ?>
            </div>
        </div>
        <div class="field_list" id="shipping_address">
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_name'); ?></div>
                <div class="field_content">
                    <?php echo $form->textField($billingInfo,'shipping_name'); ?>
                </div>
            </div>
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_last_name'); ?></div>
                <div class="field_content"><?php echo $form->textField($billingInfo,'shipping_last_name'); ?></div>
            </div>
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_street_address'); ?></div>
                <div class="field_content"><?php echo $form->textField($billingInfo,'shipping_street_address'); ?></div>
            </div>
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_city'); ?></div>
                <div class="field_content"><?php echo $form->textField($billingInfo,'shipping_city'); ?></div>
            </div>
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_state'); ?></div>
                <div class="field_content"><?php echo $form->textField($billingInfo,'shipping_state'); ?></div>
            </div>
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_zip_code'); ?></div>
                <div class="field_content"><?php echo $form->textField($billingInfo,'shipping_zip_code'); ?></div>
            </div>
            <div class="field_row">
                <div class="field_label"><?php echo $form->labelEx($billingInfo,'shipping_phone_number'); ?></div>
                <div class="field_content"><?php echo $form->textField($billingInfo,'shipping_phone_number'); ?></div>
            </div>
        </div>
        <div class="field_row">
            <div class="field_label"><b>Order Notes:</b></div>
            <div class="field_content"><?php echo $form->textField($billingInfo,'order_notes'); ?></div>
        </div>
    </div>

    <div class="row">
        <?php echo CHtml::submitButton('Submit');?>
    </div>
<?php $this->endWidget(); ?>
</div>