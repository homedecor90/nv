<h1>Add New Customer</h1>

<?php
$arrName = explode(" ", $oscOrderModel->customers_name);
$first_name = isset($arrName[0])?$arrName[0]:"";
$last_name = isset($arrName[1])?$arrName[1]:"";
$phone = $oscOrderModel->customers_telephone;
$email = $oscOrderModel->customers_email_address;
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($newCustomerModel,'first_name'); ?>
		<?php echo $form->textField($newCustomerModel,'first_name',array('size'=>60,'maxlength'=>100, 'value'=>$first_name)); ?>
		<?php echo $form->error($newCustomerModel,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newCustomerModel,'last_name'); ?>
		<?php echo $form->textField($newCustomerModel,'last_name',array('size'=>60,'maxlength'=>100, 'value'=>$last_name)); ?>
		<?php echo $form->error($newCustomerModel,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newCustomerModel,'phone'); ?>
		<?php echo $form->textField($newCustomerModel,'phone',array('size'=>60,'maxlength'=>100, 'value'=>$phone)); ?>
		<?php echo $form->error($newCustomerModel,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newCustomerModel,'email'); ?>
		<?php echo $form->textField($newCustomerModel,'email',array('size'=>60,'maxlength'=>100, 'value'=>$email)); ?>
		<?php echo $form->error($newCustomerModel,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newCustomerModel,'heard_through'); ?>
		<?php echo $form->dropDownList($newCustomerModel, 'heard_through', $heardThroughList);?>
		<?php echo $form->error($newCustomerModel,'heard_through'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::Button('Create', array('id'=>'add_new_customer_submit')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->