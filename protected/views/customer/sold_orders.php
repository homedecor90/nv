<h1>Sold Orders</h1>

<?php 
$customerOrderModel->status = CustomerOrder::$STATUS_SOLD;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-sold-order-grid',
	'dataProvider'=>$customerOrderModel->searchSold(),
	'columns'=>array(
		'id',
        array(
			'header'=>'Sold On',				
			'value'=>'date("M. d, Y", strtotime($data->sold_date))',
		),
        array(
			'header'=>'Billing Name',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/customer/\'.$data->customer_id.\'">\'.$data->billing_info->billing_name.\' \'.$data->billing_info->billing_last_name.\'</a>\'',
			'type'=>'raw',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
            'value'=>'CustomerOrder::model()->getCustomerOrderItemsShort($data->id)',
		),
		array(
			'header'=>'Shipping Method',
			'type'=>'raw',
            'value'=>'\'<span id="shipping_method_\'.$data->id.\'">\'.$data->shipping_method.\'</span>\'',
		),
        //'discounted',
		array(
			'header'=>'Price',
			'type'=>'raw',
            'value'=>'\'Total: $\'.$data->grand_total_price.\'<br />Discounted: $\'.$data->discounted_total',
		),
		/*array(
			'header'=>'Discounted Total',
			'type'=>'raw',
            'value'=>'\'<span id="dsc_total_\'.$data->id.\'">\'.$data->discounted_total.\'</span>\'',
		),*/
		/*'status',
        array(
            'header' => 'Sales Tax',
            'type'=>'raw',
            'value'=>'CHtml::checkbox("sales_tax", $data->add_sales_tax, array("class"=>"order_sales_tax", "data-orderid"=>$data->id))'
        ),*/
		// array(
			// 'header'=>'Action',
			// 'type'=>'raw',
			// 'value'=>'CustomerOrder::model()->getActionButtonString($data->id)',
		// ),
        // array(
            // 'header'=>'Shipping Address Type',
            // 'type'=>'raw',
            // 'value'=>'$data->billing_info->busrestype',
        // ),
        array(
            'header'=>'Shipping Address',
            'type'=>'raw',
            'value'=>'$data->getShippingAddressStr()',
        ),
        array(
            'header'=>'Order Notes',
            'type'=>'raw',
            'value'=>'\'<div id="order_notes_\'.$data->id.\'">\'.nl2br($data->billing_info?$data->billing_info->order_notes:\'\').\'</div><a href="#" class="edit_order_notes" rel="\'.$data->id.\'">[Edit]</a>\''
        ),
            array(
            'header'=>'Shipping Notes',
            'type'=>'raw',
            'value'=>'nl2br($data->billing_info?$data->billing_info->shipping_notes:\'\')',
        ),
	),
));
?>

<input type="button" value="Edit notes" href="#edit_notes_div" id="edit_notes_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#edit_notes_button',
    'config'=>array(
		'width'=>500,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="edit_notes_div" class="fancy_box_div">
		<form id="schedule_pickup_form" class="fancy_form">
			<h2>Edit order notes</h2>
			<div>
				<input type="hidden" name="order_id" id="edit_notes_order_id" value=""/>
				<textarea style="width: 100%; height: 200px;" id="edit_notes_text"></textarea>
			</div>
			
			<input type="button" value="Save" id="edit_notes_submit"/>
		</form>
	</div>
</div>

<script type="text/javascript">
// jQuery('.cancel_item').live('click', function(e){
	// if (!confirm('Are you sure?')){
		// return false;
	// }
	// var target  = jQuery(e.target);
	// var item_id = target.attr('rel');
	// var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/cancel_from_customer_order_ajax/?ind_item_id=" + item_id;
	
	// jQuery.ajax({
		// url:url,
		// dataType:"json",
		// success:function(result){
			// if (result.status == "fail"){
				// alert(result.message);
				// return;
			// }
			
			// if (result.status == "success"){
				// target.text('Cancelled');
				// $.fn.yiiGridView.update('inventory-item-grid');
			// }
		// }
	// });
	
	// e.preventDefault();
	// e.stopImmediatePropagation();
// });

jQuery('.edit_order_notes').live('click', function(e){
	var target = jQuery(e.target);
	var order_id = target.attr('rel');
	var text = jQuery('#order_notes_'+order_id).html();
	text = text.replace(/\n/g, '').replace(/<br>/g,"\n");
	jQuery('#edit_notes_text').val(text);
	jQuery('#edit_notes_order_id').val(order_id);
	jQuery('#edit_notes_button').trigger('click');
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#edit_notes_submit').live('click', function(e){
	var text = jQuery('#edit_notes_text').val();
	var order_id = jQuery('#edit_notes_order_id').val();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customer/edit_order_notes_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		type: 'post',
		data:{
			text: text
		},
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				$.fn.yiiGridView.update('customer-sold-order-grid');
				jQuery.fancybox.close();
			}
		}
	});
});
</script>