<?php
/**
 * @var $this CustomerController
 * @var $form CActiveForm
 * @var $model CustomerOrderReview
 */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'customer-order-review-form',
    'enableAjaxValidation'=> false,
    'enableClientValidation'=> false,
    'htmlOptions' => array(
        'class' => 'updateReview'
    )
)); ?>
<div class="row">
    <?php echo CHtml::activeTextArea($model, 'review_text', array(
        'rows' => 15,
        'cols' => 45,
    ));?>
</div>
<div class="row">
    <?php //echo CHtml::ajaxSubmitButton('submit', array('/customer/updateReview', 'id' => $model->id));?>
    <?php echo CHtml::submitButton('submit');?>
</div>
<?php $this->endWidget();?>