<h1>Add New Item</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-form',
	'enableAjaxValidation'=>false,
)); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'item_name'); ?>
		<?php echo $form->textField($newItemModel,'item_name',array('size'=>60,'maxlength'=>100, 'name'=>'item_name')); ?>
		<?php echo $form->error($newItemModel,'item_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'item_code'); ?>
		<?php echo $form->textField($newItemModel,'item_code',array('size'=>60,'maxlength'=>100, 'name'=>'item_code')); ?>
		<?php echo $form->error($newItemModel,'item_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'color'); ?>
		<?php echo $form->textField($newItemModel,'color',array('size'=>60,'maxlength'=>100, 'name'=>'color')); ?>
		<?php echo $form->error($newItemModel,'color'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'sale_price'); ?>
		<?php echo $form->textField($newItemModel,'sale_price', array('name'=>'sale_price')); ?>
		<?php echo $form->error($newItemModel,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'shipping_price'); ?>
		<?php echo $form->textField($newItemModel,'shipping_price', array('name'=>'shipping_price')); ?>
		<?php echo $form->error($newItemModel,'shipping_price'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($newItemModel,'local_pickup'); ?>
		<?php echo $form->textField($newItemModel,'local_pickup', array('name'=>'local_pickup')); ?>
		<?php echo $form->error($newItemModel,'local_pickup'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($newItemModel,'la_oc_shipping'); ?>
		<?php echo $form->textField($newItemModel,'la_oc_shipping', array('name'=>'la_oc_shipping')); ?>
		<?php echo $form->error($newItemModel,'la_oc_shipping'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($newItemModel,'canada_shipping'); ?>
		<?php echo $form->textField($newItemModel,'canada_shipping', array('name'=>'canada_shipping')); ?>
		<?php echo $form->error($newItemModel,'canada_shipping'); ?>
	</div>
    <?php /*
    <div class="row">
		<?php echo $form->labelEx($newItemModel,'cbm'); ?>
		<?php echo $form->textField($newItemModel,'cbm', array('name'=>'cbm')); ?>
		<?php echo $form->error($newItemModel,'cbm'); ?>
	</div>
    */?>
	<div class="row">
		<?php echo CHtml::label('quantity', 'quantity'); ?>
		<?php echo CHtml::textField('quantity', '1', array('name'=>'quantity')); ?>		
	</div>

	<div class="row buttons">
		<?php echo CHtml::Button('Create', array('id'=>'add_new_item_submit')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->