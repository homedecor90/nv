<?php
$displayed_items = array();
//echo count($related_items);
//return;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Regency Shop</title>

    <style type="text/css">
        p {margin-bottom:0; margin:0}
    </style>


</head>
<body>

<table width="640px" align="center" style="
	font-size: normal;
	line-height: normal;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	vertical-align: baseline;
	display: block;
	max-width: 900px;
	margin-left: auto;
	margin-right:auto;
	padding: 50px 0 10px;
	border: 0;">

    <tr><td style="text-align:left; vertical-align: baseline; float: left; margin: 0 0 0 40px; padding: 0; border: 0;">

            <img src="http://www.regencyshop.com/templates/regency/images/logo.gif" alt="" style="vertical-align: baseline; float: left; text-align: right; position: relative; margin: 10px 0 0 25px; padding: 0; border: 0;" align="left">


            <img src="http://midmodfurnishings.com/images/newsletter/phone.png" alt="" style="margin: 0 0 0 40px;" align="right">
        </td></tr>
</table>

<table align="center" width="640" style="width:640px;">

    <tr id="products" align="center" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; display: block; width: 600px; -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px; position: relative; background-color: #efefef; margin: 20px auto; padding: 0px 10px 20px 30px; border: 1px solid #d2d2d2;">
        <td class="products">
            <p align="center" style="vertical-align: baseline; color: #83646c; font-style: normal; font-variant: normal; font-weight: bold; font-size: 24px; line-height: normal; font-family: 'Roboto', sans-serif; margin: 0 0 20px; padding: 10px 20px; border: 0;">Recommended for you</p>
            <table align="center"><tr><td class="holder" style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0 auto; font-weight: normal; vertical-align: baseline;">
                        <table align="center" style="display:inline; margin:0 0 0 0px; padding:0; text-align:left; width:170px;height:150Çpx;">
                            <tr>
                                <?php
                                foreach ($recommended_items as $i=>$item):
                                    $displayed_items[] = $item->item_main_name;
                                ?>
                                    <td class="product" style="font-size: 17px; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; width: 170px; display: inline-block; color: #83646c; margin: 0 auto; padding: 0; border: 0;">
                                        <table>
<!--                                            -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; border: 1px solid #e2e2e2;-->
                                            <tr>
                                                <td class="image" style="-webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; text-align:center; vertical-align: middle; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; width: 170px; height: 150px; max-width: 170px; max-height: 170px; background-color: white; margin: 0 0 20px; padding: 0; border: 1px solid #e2e2e2;">
                                                    <a href="<?php echo $item->url; ?>">
                                                        <img style="-webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; border: 1px solid #e2e2e2; max-width: 170px; max-height: 170px; border-style: none;" src="<?php echo $item->image_url; ?>" />
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <table><tr style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; position: relative; background-color: #efefef; margin: -8px auto; padding: 0px 10px 20px 30px; text-align:center">
                                                <td style="text-align: center">
                                                    <a href="<?php echo $item->url; ?>" style="font-size: 13px; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: none; margin: 0; padding: 0; border: 0;"><?php echo $item->item_main_name; ?></a><br />
                                                    <span style="font-size: 14px; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: #83646c; margin: 5px 0 10px; padding: 0; border: 0;">Price: $<?php echo number_format($item->sale_price + $item->shipping_price, 2); ?></span>
                                                </td>
                                            </tr></table>
                                    </td>
                                    <?php
                                    if ($i != count($recommended_items)-1):
                                    ?>
                                        <td class="separator" style="min-width: 15px; display:inline-block;"></td>
                                    <?php
                                    endif;
                                    ?>
                                <?php
                                endforeach;
                                ?>
                            </tr>
                        </table>
                    </td></tr></table>
        </td>
    </tr>
</table>
<?php
if (count($related_items)):
?>
<table class="midsection sub_footer" style="width:640px;" align="center">
    <tr align="center">

    </tr>

    <tr align="center" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; width: 640px; text-align: center; background-color: #ece8e9; margin: -15px auto; padding: 0; border: 0;">
        <td align="center">
            <p align="center" style="vertical-align: baseline; color: #83646c; font-style: normal; font-variant: normal; font-weight: bold; font-size: 20px; line-height: normal; font-family: 'Roboto', sans-serif; margin: 0 0 20px; padding: 10px 20px; border: 0;">People who bought your items, also bought</p>

            <?php
            $index = 0;
            for ($rows = 1; $rows<=2; $rows++):
            ?>
                <table class="holder" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0 0 40px; padding: 0; border: 0;">
                    <tr align="center">
                    <?php
                        for ($items = 1; $items<=3; $items++):
                            while (in_array($related_items[$index]->item_main_name, $displayed_items)){
                                $index++;
                                if (!isset($related_items[$index])){
                                    break(2);
                                }
                            }
                            $item = $related_items[$index];
                            $displayed_items[] = $item->item_main_name;
                    ?>
                        <td class="product" style="font-size: 17px; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; width: 170px; display: inline-block; color: #83646c; margin: 0px; padding: 0; border: 0;">
                            <table>
                                <tr>
                                    <td class="image" style="-webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; text-align:center; vertical-align: middle; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; width: 170px; height: 150px; max-width: 170px; max-height: 170px; background-color: white; margin: 0 0 20px; padding: 0; border: 1px solid #e2e2e2;">
                                        <a href="<?php echo $item->url; ?>">
                                            <img style="-webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; border: 1px solid #e2e2e2; max-width: 170px; max-height: 170px; border-style: none" src="<?php echo $item->image_url; ?>" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr style="text-align:center">
                                    <td style="text-align: center">
                                        <a href="<?php echo $item->url; ?>" style="font-size: 13px; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: none; margin: 0; padding: 0; border: 0;"><?php echo $item->item_main_name; ?></a><br />
                                        <span style="font-size: 14px; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: #83646c; margin: 5px 0 10px; padding: 0; border: 0;">Price: $<?php echo number_format($item->sale_price + $item->shipping_price, 2); ?></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <?php
                        if ($items < 3):
                        ?>
                        <td class="separator" style="min-width: 15px; display:inline-block;"></td>
                        <?php
                        endif;
                        ?>
                    <?php
                    endfor;
                    ?>
                    </tr>
                </table>
            <?php
            endfor;
            ?>
        </td>
    </tr>
</table>
<?php
endif;
?>
<table class="subfooter" style="width: 640px; text-align: center; padding: 20px 0px; background-color: #f2f2f2; margin:0 auto;" align="center">
    <tr>
        <td>
            <p align="center" style="vertical-align: baseline; color: #83646c; font-style: normal; font-variant: normal; font-weight: bold; font-size: 22px; line-height: normal; font-family: 'Roboto', sans-serif; margin: 0 0 20px; padding: 10px 20px; border: 0;">Popular this month</p>
            <?php
            $index = 0;
            for ($rows = 1; $rows<=2; $rows++):
            ?>
            <table class="holder" style="width:640px; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; margin: 0 0 40px; padding: 0; border: 0;">
                <tr align="center">
                <?php
                for ($items = 1; $items<=2; $items++):
                    while (in_array($most_popular_items[$index]->item_main_name, $displayed_items)){
                        $index++;
                    }
                    $item = $most_popular_items[$index];
                    $displayed_items[] = $item->item_main_name;
                ?>
                    <td class="product" style="font-size: 17px; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; width: 170px; display: inline-block; color: #83646c; margin: 0px; padding: 0; border: 0;">
                        <table>
                            <tr>
                                <td class="image" style="-webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; text-align:center; vertical-align: middle; font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; width: 170px; height: 150px; max-width: 170px; max-height: 170px; background-color: white; margin: 0 0 20px; padding: 0; border: 1px solid #e2e2e2;">
                                    <a href="<?php echo $item->url; ?>">
                                        <img style="-webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; border: 1px solid #e2e2e2; max-width: 170px; max-height: 170px; border-style: none" src="<?php echo $item->image_url; ?>" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr style="text-align:center">
                                <td style="text-align: center; vertical-align: top;">
                                    <a href="<?php echo $item->url; ?>" style="font-size: 15px; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: none; margin: 0; padding: 0; border: 0;"><?php echo $item->item_main_name; ?></a><br />
                                    <span style="font-size: 14px; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: #83646c; margin: 5px 0 10px; padding: 0; border: 0;">Price: $<?php echo number_format($item->sale_price + $item->shipping_price, 2); ?></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <?php
                    if ($items < 2):
                    ?>
                    <td class="separator" style="min-width: 15px; display:inline-block;"></td>
                    <?php
                    endif;
                    ?>
                <?php
                endfor;
                ?>
                </tr>
            </table>
            <?php
            endfor;
            ?>
        </td>
    </tr>
</table>

<table align="center" class="footer" style="width: 640px; margin-left:auto; margin-right:auto; border-top: 2px solid grey; text-align: center; padding: 20px 0px; background-color: #f2f2f2">
    <tr align="center">
        <td align="center">
            <p style="font-size: 16px; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; margin: 4px 0 0; padding: 0; border: 0;"><span style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: gray; margin: 0; padding: 0; border: 0;">Sales:</span> 1-866-776-2680</p>
            <p style="font-size: 16px; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; margin: 4px 0 0; padding: 0; border: 0;"><span style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: gray; margin: 0; padding: 0; border: 0;">Customer Service:</span> 1-323-747-5378</p>

        </td></tr><tr class="social" style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: gray; margin: 20px 0 0; padding: 0; border: 0;">
        <td>
<!--            <a href="" style="font-size: normal; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: underline; margin: 0; padding: 0; border: 0;"><img src="http://midmodfurnishings.com/images/newsletter/instagram.png" alt="" style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"></a>-->
            <a href="https://www.facebook.com/RegencyShop.ModernFurniture" style="font-size: normal; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: underline; margin: 0; padding: 0; border: 0;"><img src="http://midmodfurnishings.com/images/newsletter/facebook.png" alt="" style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"></a>
            <a href="https://twitter.com/regencyshop" style="font-size: normal; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: underline; margin: 0; padding: 0; border: 0;"><img src="http://midmodfurnishings.com/images/newsletter/twitter.png" alt="" style="font-size: normal; border: 0; font-style: normal; font-variant: normal; padding: 0; line-height: normal; margin: 0; font-weight: normal; vertical-align: baseline;"></a>
        </td>
    </tr>
    <tr style="font-size: normal; line-height: normal; font-style: normal; font-weight: normal; font-variant: normal; vertical-align: baseline; color: gray; margin: 20px 0 0; padding: 0; border: 0;">
        <td>
            Don't want to receive RegencyShop updates? <a href="<?php echo $unsubscribe_link; ?>" style="font-size: normal; line-height: normal; font-style: normal; font-weight: bold; font-variant: normal; vertical-align: baseline; color: #83646c; text-decoration: underline; margin: 0; padding: 0; border: 0;"> Unsubscribe</a>
        </td>
    </tr>
</table>
</html>