<h1>Container</h1>

<?php
$this->menu=array(
    array(
        'label'=>'Supplier Order Containers',
        'url'=>array('/supplierorder/view_container')
    ),
    array('label'=>'Create Supplier Order', 'url'=>array('create')),
    array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'supplier-order-container-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        'id',
        array(
            'header'=>'Created Date',
            'name' => 'created_date',
            'value'=>'($data->created_date==NULL)?"":date("M. d, Y", strtotime($data->created_date))',
        ),
        array(
            'header'=>'Orders',
            'value'=>'SupplierOrderContainer::model()->getOrderInfo($data->id)',
            'type'=>'raw',
        ),
        array(
            'header'=>'Total EXW Price',
            'value'=>'($data->terms == \'EXW\' ? \'<span class="balance_highlight">\' : \'\').SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($data->id).($data->terms == \'EXW\' ? \'</span>\': \'\')',
            'type'=>'raw',
        ),
        array(
            'header'=>'Total FOB Price',
            'value'=>'($data->terms == \'FOB\' ? \'<span class="balance_highlight">\' : \'\').SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($data->id).($data->terms == \'FOB\' ? \'</span>\' : \'\')',
            'type'=>'raw',
        ),
        array(
            'header'=>'Total EXW With Shipping',
            'value'=>'($data->terms == \'EXW\' ? \'<span class="balance_highlight">\' : \'\').$data->getEXWTotalWithShipping().($data->terms == \'EXW\' ? \'</span>\' : \'\')',
            'type'=>'raw',
        ),
        array(
            'header'=>'Total FOB With Shipping',
            'value'=>'($data->terms == \'FOB\' ? \'<span class="balance_highlight">\' : \'\').$data->getFOBTotalWithShipping().($data->terms == \'FOB\' ? \'</span>\' : \'\')',
            'type'=>'raw',
        ),
        array(
            'header'=>'Number of Bids',
            'value'=>'$data->getBidsCount()',
        ),
        array(
            'header'=>'Shipper Name',
            'type'=>'raw',
            'value'=>'($data->status == SupplierOrderContainer::$STATUS_PENDING ? \'[Pending]\' : SupplierOrderContainer::getShipperName($data->id))',
        ),
        array(
            'header'=>'Detail',
            'type'=>'raw',
            'value'=>'SupplierOrderContainer::model()->getDetailLink($data->id)',
        ),
    ),
)); ?>
