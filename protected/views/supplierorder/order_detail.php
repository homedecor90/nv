<h1>Supplier Order Detail #<?php echo $supplierOrderModel->id;?></h1>

<h2>Order Information</h2>

<input type="button" rel="<?php echo $supplierOrderModel->id; ?>" class="print" value="Print" />
<input type="button" rel="<?php echo $supplierOrderModel->id; ?>" class="pdf" value="Download PDF" />
<!-- input type="button" rel="<?php echo $supplierOrderModel->id; ?>" class="csv" value="Download CSV" /-->
<br /><br />
<strong>Status: <?php echo $supplierOrderModel->status; ?></strong>
<br /><br />

<?php if (strtotime($supplierOrderModel->etl) > 0 && YumUser::isOperator(true)): ?>
	<strong>ETL: </strong>
	<span id="old_etl"><?php echo date('M d, Y', strtotime($supplierOrderModel->etl))?></span>
	<?php if (Yii::app()->user->isAdmin()): ?>
		<span href="#setEtlDiv" id="setEtlButton" class="clickable">edit</span><br /><br />
	<?php endif;?>

	<?php
	$this->widget('application.extensions.fancybox.EFancyBox', array(
	    'target'=>'#setEtlButton',
	    'config'=>array(
			'width'=>250,
			'height'=>120,
			'autoDimensions'=>false,
		),
	));
	?>

	<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="setEtlDiv" class="fancy_box_div">
	<?php
		Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
		$this->widget('CJuiDateTimePicker',array(
			'value'=>date('M d, Y', strtotime($supplierOrderModel->etl)),
			'name'=>'new_etl',
			'mode'=>'date', //use "time","date" or "datetime" (default)
			'language'=>'en-GB',
			'options'=>array(
				"dateFormat"=>"M. dd, yy"
			) // jquery plugin options
		));
	?>
	<input type="button" value="Set E.T.L" id="set_etl_fancy"/>
	</div>
	</div>

<?php endif;?>


<?php
	// var_dump(SupplierOrder::getPendingLoadingItemsBySupplierID(4));
?>

<table class="supplier_order_details">
	<tr>
		<td>
			<b><?php echo $supplierOrderModel->getAttributeLabel('created_date'); ?>:</b>
			<?php echo date("M. d, Y", strtotime($supplierOrderModel->created_date)); ?>
			<br />

			<?php if ($supplierOrderModel->accepted_date):?>
			<b><?php echo $supplierOrderModel->getAttributeLabel('accepted_date'); ?>:</b>
			<?php echo date("M. d, Y", strtotime($supplierOrderModel->accepted_date)); ?>
			<br />
			<?php endif; ?>

			<?php if ($supplierOrderModel->advanced_paid_date):?>
			<b><?php echo $supplierOrderModel->getAttributeLabel('advanced_paid_date'); ?>:</b>
			<?php echo date("M. d, Y", strtotime($supplierOrderModel->advanced_paid_date)); ?>
			<br />
			<?php endif; ?>

			<?php if ($supplierOrderModel->completed_date):?>
			<b><?php echo $supplierOrderModel->getAttributeLabel('completed_date'); ?>:</b>
			<?php echo date("M. d, Y", strtotime($supplierOrderModel->completed_date)); ?>
			<br />
			<?php endif; ?>

			<?php if ($eta = $supplierOrderModel->getETAString()):?>
			<b>ETA: </b>
			<?php echo $eta; ?>
			<br />
			<?php endif; ?>
		</td>
		<td>
			<b>Total CBM:</b>
			<?php echo SupplierOrder::model()->getSupplierOrderTotalCBM($supplierOrderModel->id); ?>
		</td>
		<td>
			<?php echo ($supplierOrderModel->getShippingTerms() == 'EXW' ? '<span class="balance_highlight">' : '')
						.'<strong>Total EXW Price</strong>:<br />$'.SupplierOrder::model()->getSupplierOrderTotalExwPrice($supplierOrderModel->id)
						.($supplierOrderModel->getShippingTerms() == 'EXW' ? '</span>' : ''); ?>
			<br />

			<?php echo ($supplierOrderModel->getShippingTerms() == 'FOB' ? '<span class="balance_highlight">' : '')
						.'<strong>Total FOB Price:</strong><br />$'.SupplierOrder::model()->getSupplierOrderTotalFobPrice($supplierOrderModel->id)
						.($supplierOrderModel->getShippingTerms() == 'FOB' ? '</span>' : ''); ?>
			<br />
		</td>
		<td>

			<?php echo ($supplierOrderModel->getShippingTerms() == 'EXW' ? '<span class="balance_highlight">' : '')
						.'<b>EXW Balance:</b><br />'
						.$supplierOrderModel->getSupplierOrderExwBalanceString()
						.($supplierOrderModel->getShippingTerms() == 'EXW' ? '</span>' : ''); ?>
			<br />

			<?php echo ($supplierOrderModel->getShippingTerms() == 'FOB' ? '<span class="balance_highlight">' : '')
						.'<b>FOB Balance:</b><br />'
						.$supplierOrderModel->getSupplierOrderFobBalanceString()
						.($supplierOrderModel->getShippingTerms() == 'FOB' ? '</span>' : ''); ?>
			<br />
		</td>
		<td>
			<?php echo SupplierOrder::model()->getSupplierOrderStatus($supplierOrderModel->id); ?>
		</td>
	</tr>
</table>

<h2>Items</h2>
<div class="grid-view">
<?php
	echo SupplierOrder::model()->getSupplierOrderItems($supplierOrderModel->id);
?>
</div>

<?php
if ($supplierOrderModel->status == SupplierOrder::$STATUS_FULL_PAID || $supplierOrderModel->status == SupplierOrder::$STATUS_ARCHIVED){
    if (YumUser::model()->isSupplier(false) && $supplierOrderModel->loaded != SupplierOrder::$LS_LOADED){
?>
		<div id="load_supplier_order_items_div" class="grid-view">
			<h3>Load Items</h3>
			<?php
				echo $supplierOrderModel->getLoadingWaitingItems();
			?>
			<input type="button" value="Confirm Loaded Items" id="load_items"/>
		</div>
<?php
	}
    //$supplierOrderModel->loaded == SupplierOrder::$LS_LOADED
    //	&&
  else if (YumUser::isOperator(true)  && $supplierOrderModel->loaded_by_admin != 1 ) {
?>
	<div id="load_supplier_order_items_div" class="grid-view">
		<h3>Confirm Loaded Items (Scan as In Stock)</h3>
		<?php
			echo $supplierOrderModel->getLoadingWaitingItems();
		?>
		<input type="button" value="Confirm Loaded Items ad Scan as IN STOCK" id="load_items"/>
	</div>
	<input type="button" value="Scan loaded items as IN" id="scan_loaded_items" style="display: none;" />
<?php
    }
} /*else if(YumUser::isOperator(true) && $supplierOrderModel->loaded_by_admin) {
	?>
	<input type="button" value="Scan loaded items as IN" id="scan_loaded_items" />
	<?php
}*/
?>

<h2>Official Documents</h2>
<?php if ($terms = $supplierOrderModel->getShippingTerms()):?>
<strong>Selected Term: <?php echo $terms; ?></strong>
<?php endif;?>
<div style="display:none">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-grid',
	'dataProvider'=>$supplierOrderOfficialDocsModel->search(),
	'columns'=>array(
		// array(
			// 'header'=>'File Name',
			// 'type'=>'raw',
            // 'value'=>'$data->file_name',
		// ),
	),
)); ?>
</div>

<div class="grid-view">
	<?php
	echo $supplierOrderOfficialDocsModel->getOfficialDocumentsTable();
	?>
</div>




<h2>Order Note</h2>
<?php
$user_id = Yii::app()->user->id;
$isAdmin = Yii::app()->user->isAdmin();
$baseGridScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-note-grid',
	'dataProvider' => SupplierOrderNote::model()->search($supplierOrderModel->id),
	'columns'=>array(
		array(
		     'headerHtmlOptions' => array('style' => 'display:none'),
		     'htmlOptions' => array('style' => 'display:none'),
			'value'=>'$data->id',
		),
		array(
			'header'=>'User',
		    'value'=>'YumUser::model()->findByPk($data->operator_id)->profile->firstname." ".YumUser::model()->findByPk($data->operator_id)->profile->lastname',
		),
		array(
			'name'=>'content',
            //'value' => nl2br($data->content),//nl2br(CHtml::encode($data->content)),
			'type'=>'ntext',
		),
		array(
			'header'=>'Created On',
			'value'=>'date("M. d, Y h:i:s A", strtotime($data->created_on))',
		),
		array(
			'header'=>'Modified',
			'value'=>'date("M. d, Y h:i:s A", strtotime($data->modified))',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{edit}',
			'buttons'=>array(
				'edit' => array(
					'imageUrl'=>$baseGridScriptUrl.'/update.png',
					'url'=>'"#order_note_form"',
					'visible'=>'($data->operator_id == '.$user_id.') || ('.$isAdmin.' == 1)',
					'click'=>'function(){
						jQuery("form#add-supplier-order-note-form").hide();
						jQuery("form#edit-supplier-order-note-form #SupplierOrderNote_id").val(
									$(this).parent().parent().children(\':nth-child(1)\').text());
						jQuery("form#edit-supplier-order-note-form #content_textarea").val(
									$(this).parent().parent().children(\':nth-child(3)\').text());
						jQuery("form#edit-supplier-order-note-form").show();
					}',
				),
			),
		),
	),
));
?>

<?php if (YumUser::model()->isOperator(true)) : ?>
	<div class="form" id="order_note_form">
<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'add-supplier-order-note-form',
		'enableAjaxValidation'=>false,
	));
?>
	<?php echo $form->errorSummary($newSupplierOrderNoteModel); ?>
	<?php echo $form->hiddenField($newSupplierOrderNoteModel, 'supplier_order_id', array('value'=>$supplierOrderModel->id)); ?>
	<?php echo $form->hiddenField($newSupplierOrderNoteModel, 'operator_id', array('value'=>$user_id));?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<div class="row">
		<?php echo $form->labelEx($newSupplierOrderNoteModel,'content'); ?>
		<?php echo $form->textArea($newSupplierOrderNoteModel,'content',array('cols'=>60, 'rows'=>6, 'id'=>'content_textarea')); ?>
		<?php echo $form->error($newSupplierOrderNoteModel,'content'); ?>
	</div>

	<div class="row buttons"><?php echo CHtml::Button('Add New Note', array('id'=>'add_new_note')); ?></div>

<?php $this->endWidget(); ?>
<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'edit-supplier-order-note-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('style'=>'display:none')
	));
?>
	<?php echo $form->hiddenField($newSupplierOrderNoteModel, 'id', array('value'=>'')); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<div class="row">
		<?php echo $form->labelEx($newSupplierOrderNoteModel,'content'); ?>
		<?php echo $form->textArea($newSupplierOrderNoteModel,'content',array('cols'=>60, 'rows'=>6, 'id'=>'content_textarea')); ?>
		<?php echo $form->error($newSupplierOrderNoteModel,'content'); ?>
	</div>
	<div class="row buttons"><?php echo CHtml::Button('Save Note', array('id'=>'save_note')); ?>
							<?php echo CHtml::Button('Cancel Note', array('id'=>'cancel_save_note')); ?></div>
<?php $this->endWidget(); ?>
	</div><!-- form -->


<?php
// ADD ITEMS FORM
echo $this->renderPartial('_add_items_to_order', array(
		'supplierOrderModel'=>$supplierOrderModel,
		'newItemModel'=>$newItemModel,
		'itemCodeList'=>$itemCodeList,
));

endif;

?>

<script>
var operator = <?php echo YumUser::isOperator(true) ? 'true' : 'false'; ?>;
jQuery(document).ready(function(){
	jQuery("#set_etl_fancy").live("click", function(){
		var id = <?php echo $supplierOrderModel->id; ?>;
		var etl_date = jQuery("#new_etl").val();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/set_etl_by_admin_ajax/?"
			+ "id=" + id + "&etl_date=" + etl_date;
		jQuery.fancybox.close();

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if(result.message){
					alert(result.message);
				}

				if (result.status == "success"){
					jQuery('#old_etl').text(etl_date);
				}
			}
		});
	});

	jQuery("#load_items").click(function(e){
		var bVerify = true;
		var item_id_list = "";
		var quantity_list = "";

		jQuery(".loading_quantity").each(function(){
			var item_id = jQuery(this).attr("rel");
			var item_quantity = parseInt(jQuery(this).val());
			var waiting_quantity = parseInt(jQuery("#waiting_quantity_" + item_id).val());


			if (isNaN(item_quantity)){
				bVerify = false;
			} else {
				if (item_quantity > waiting_quantity || item_quantity < 1){
					bVerify = false;
				}
			}

			if (item_id_list != ""){
				item_id_list += "_";
			}
			item_id_list += item_id;

			if (quantity_list != ""){
				quantity_list += "_";
			}
			quantity_list += item_quantity;
		});

		if (bVerify == false){
			if (!confirm("Are you sure?")){
				jQuery(this).focus();
				return false;
			}
		}

		// if (bVerify == false){
			// return;
		// }

		var action = operator ? 'load_order_items_by_admin_ajax' : 'load_order_items_by_supplier_ajax';

		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/"+action+"/?"
			+ "supplier_order_id=" + <?php echo $supplierOrderModel->id;?> + "&item_id_list=" + item_id_list + "&quantity_list=" + quantity_list;

		jQuery(e.target).attr('disabled', 'disabled');

		jQuery.ajax({
			url:url,
			success:function(result){
				/*jQuery("#load_supplier_order_items_div").remove();
				if (operator){
					jQuery('#scan_loaded_items').show();
				}*/
				//document.location.reload(true);
                alert('Successfully loaded items')
			}
		});
	});

	jQuery('#scan_loaded_items').click(function(e){
		jQuery(e.target).attr('disabled', 'disabled');
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/scan_loaded_items_in_ajax/?order_id="+<?php echo $supplierOrderModel->id;?>;
		jQuery.ajax({
			url:url,
			success:function(result){
				if (result.status == 'fail'){
					alert(result.message);
					return;
				}

				//alert('Total items: '+result.items);
				alert('Items has been scanned as in');
			}
		});
	});

    jQuery('.item_qty_accept').live('click', function(e){
        var items_id = jQuery(e.target).attr('rel');
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_qty_change_ajax/?items_id=" + items_id;

        jQuery.ajax({
            url:url,
            success:function(result){
                if (result.status == 'fail'){
                    alert(result.message);
                    return;
                }
                document.location.reload(true);
            }
        });
    });

    jQuery('.item_qty_deny').live('click', function(e){
        var items_id = jQuery(e.target).attr('rel');
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_qty_change_ajax/?items_id=" + items_id;

        jQuery.ajax({
            url:url,
            success:function(result){
                if (result.status == 'fail'){
                    alert(result.message);
                    return;
                }
                document.location.reload(true);
            }
        });
    });

    jQuery(".item_accept").live("click", function(){
		var target = jQuery(this);
        var item_id = target.attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_item_by_supplier_ajax/?item_id=" + item_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				// target.remove();
                // jQuery('.item_deny[rel="'+item_id+'"]').remove();
                document.location.reload(true);
			}
		});
	});

	jQuery(".item_deny").live("click", function(){
		var target = jQuery(this);
        var item_id = target.attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_item_by_supplier_ajax/?item_id=" + item_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				// target.remove();
                // jQuery('.item_accept[rel="'+item_id+'"]').remove();
                document.location.reload(true);
			}
		});
	});

	jQuery('.print').click(function(e){
		var id = jQuery(e.target).attr('rel');
		window.open('<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/print/?order_id='+id,'_blank');
	});

	jQuery('.pdf').click(function(e){
		var id = jQuery(e.target).attr('rel');
		window.location.href = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/pdf/supplier_order_details/?order_id='+id;
	});

	jQuery('.csv').click(function(e){
		var id = jQuery(e.target).attr('rel');
		window.location.href = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/order_details_csv/?order_id='+id;
	});

<?php if (YumUser::model()->isOperator(true)) : ?>
	jQuery("form#add-supplier-order-note-form #add_new_note").click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/add_new_note_ajax";
		var postData = jQuery("form#add-supplier-order-note-form").serialize();

		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}

				jQuery.fn.yiiGridView.update('supplier-order-note-grid');
				jQuery("form#add-supplier-order-note-form #content_textarea").val("");
			}
		});
	});
	jQuery("form#edit-supplier-order-note-form #save_note").click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/edit_note_ajax";
		var postData = jQuery("form#edit-supplier-order-note-form").serialize();

		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}

				jQuery.fn.yiiGridView.update('supplier-order-note-grid');
				jQuery("form#edit-supplier-order-note-form #cancel_save_note").click();

			}
		});
	});
	jQuery("form#edit-supplier-order-note-form #cancel_save_note").click(function(){
		jQuery("form#edit-supplier-order-note-form").hide();
		jQuery("form#edit-supplier-order-note-form #SupplierOrderNote_id").val('');
		jQuery("form#edit-supplier-order-note-form #content_textarea").val('');
		jQuery("form#add-supplier-order-note-form").show();
	});
<?php endif; ?>
});
</script>