<h1>Pending Payment Containers</h1>

<?php
$this->menu=array(
	array('label'=>'Payment History', 'url'=>array('payment_history')),
	array('label'=>'Pending Payment Containers', 'url'=>array('pending_payment')),
);
?>
<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Select Shipper', 'shipper_id'); ?>
                <?php echo CHtml::dropDownList('shipper_id', $shipper_id, $shipper_list); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date from', 'date_from'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_from', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_from",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date to', 'date_to'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_to', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_to",
                        ),
                    ));
                ?>
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<?php 
$containers = $container_model->searchChosen();

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-grid',
	'dataProvider'=>$containers,
	'columns'=>array(
		array(
            'header'=>'ID',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/supplierorder/container_detail/?container_id=\'.$data->id.\'">\'.$data->id.\'</a>\'',
            'type'=>'raw'
        ),
		array(
			'header'=>'Created Date',				
			'value'=>'($data->created_date==NULL)?"":date("M. d, Y", strtotime($data->created_date))',
		),
        array(
            'header'=>'Company',
            'value'=>'$data->shipper->profile->firstname.\' \'.$data->shipper->profile->lastname',
        ),
		array(
            'header'=>'Number of Suppliers',
            'value'=>'$data->getSupplierCount()',
            'type'=>'raw',
        ),
		array(
			'header'=>'Total CBM',
			'type'=>'raw',
            // 'value'=>'SupplierOrderContainer::getSupplierOrderContainerTotalCBM($data->id)',
            'value'=>'$data->getTotalCBM()',
		),
        array(
			'header'=>'Price per CBM',
			'type'=>'raw',
            // 'value'=>'SupplierOrderContainer::getSupplierOrderContainerTotalCBM($data->id)',
            'value'=>'number_format($data->getTotalCBM() ? $data->getShippingPrice()/$data->getTotalCBM() : 0, 2)',
		),
        array(
			'header'=>'Container Size',
			'type'=>'raw',
            'value'=>'$data->getChosenBid()->container_size',
		),
        array(
			'header'=>'ETA',
			'type'=>'raw',
            'value'=>'date("M, d. Y", strtotime($data->getChosenBid()->bid_eta))',
		),
        array(
			'header'=>'Picked Up Date',
			'type'=>'raw',
            'value'=>'$data->picked_up_date ? date("M, d. Y", strtotime($data->picked_up_date)) : \'\'',
		),
        array(
			'header'=>'Term - Cost',
			'type'=>'raw',
            'value'=>'$data->terms.\' - $\'.$data->getShippingPrice()',
		),
        array(
            'header'=>'Pay',
            'type'=>'raw',
            'value'=>'\'<input type="button" value="Pay" class="pay_button" rel="\'.$data->id.\'" />\'',
        )
	),
)); 
?>

<div id="fancy_triggers" style="display: none;">
<?php
foreach ($containers->data as $row){
    // 1. fancy box trigger element 
    echo '<a href="#fancy_block_'.$row->id.'" id="fancy_trigger_'.$row->id.'"></a>';
    // 2. widget call
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'#fancy_trigger_'.$row->id,
        'config'=>array(
            'width'=>550,
            'height'=>430,
            'autoDimensions'=>false,
        ),
    ));
    // 3. bid info block
    ?>
    <div id="fancy_block_<?php echo $row->id;?>">
        <?php 
            echo '<strong>Selected Term:</strong><br />';
            echo $row->terms.': $'.$row->getShippingPrice().'<br />';
            echo '<label>Check Number: </label> <input type="text" id="check_number_'.$row->id.'" />';
            echo '<input type="button" value="Pay and Print Details" class="send_payment" rel="'.$row->id.'"/><br /><br />';
            echo SupplierOrderContainerBidding::model()->getBiddingEXWOptions($row->getChosenBid()->id);
        ?>
    </div>
<?php
}
?>
</div>

<script type="text/javascript">
jQuery('.pay_button').live('click', function (e){
    var target = jQuery(e.target);
    var id = target.attr('rel');
    jQuery('#fancy_trigger_'+id).trigger('click');
});

jQuery('.send_payment').live('click', function (e){
    var target = jQuery(e.target);
    var id = target.attr('rel');
    var check_number = jQuery('#check_number_'+id).val();
    
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/pay_shipper_invoice_ajax?container_id="+id+'&check_number='+check_number;

		jQuery.ajax({
			url:url,
			dataType:"json",
			type: "POST",
			success:function(result){
				if (result.status != "success"){
					alert(result.message);
					return false;
				}
				
				jQuery.fancybox.close();
				jQuery.fn.yiiGridView.update('supplier-order-container-grid');
                window.location.href = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/pdf/container/?container_id='+id;
			}
		});
});

jQuery('#button_filter').click(function(e){
    var search_condition = jQuery('#filter_form').serialize();
		
    jQuery.fn.yiiGridView.update('supplier-order-container-grid', {
        data: search_condition
    });
    return false;
});
</script>