<h1>Container</h1>

<?php
$this->menu=array(
	array('label'=>'Create Supplier Order', 'url'=>array('create')),
	array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
	array('label'=>'Archived Containers', 'url'=>array('/supplierOrderContainer/archived')),
);
?>

<input type="button" id="resend_requests" value="Resend Supplier Shipper Bid Requests" />

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-grid',
	'dataProvider'=>$supplierOrderContainerModel->search(),
	'columns'=>array(
		'id',
		array(
			'header'=>'Created Date',
            'name' => 'created_date',
			'value'=>'($data->created_date==NULL)?"":date("M. d, Y", strtotime($data->created_date))',
		),
		array(
			'header'=>'Orders',
			'value'=>'SupplierOrderContainer::model()->getOrderInfo($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total EXW Price',
			'value'=>'($data->terms == \'EXW\' ? \'<span class="balance_highlight">\' : \'\').SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($data->id).($data->terms == \'EXW\' ? \'</span>\': \'\')',
			'type'=>'raw',
		),
		array(
			'header'=>'Total FOB Price',
			'value'=>'($data->terms == \'FOB\' ? \'<span class="balance_highlight">\' : \'\').SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($data->id).($data->terms == \'FOB\' ? \'</span>\' : \'\')',
			'type'=>'raw',
		),
		array(
			'header'=>'Total EXW With Shipping',
			'value'=>'($data->terms == \'EXW\' ? \'<span class="balance_highlight">\' : \'\').$data->getEXWTotalWithShipping().($data->terms == \'EXW\' ? \'</span>\' : \'\')',
			'type'=>'raw',
		),
		array(
			'header'=>'Total FOB With Shipping',
			'value'=>'($data->terms == \'FOB\' ? \'<span class="balance_highlight">\' : \'\').$data->getFOBTotalWithShipping().($data->terms == \'FOB\' ? \'</span>\' : \'\')',
			'type'=>'raw',
		),
        array(
            'header'=>'Number of Bids',
            'value'=>'$data->getBidsCount()',
        ),
		array(
			'header'=>'Shipper Name',
			'type'=>'raw',
			'value'=>'($data->status == SupplierOrderContainer::$STATUS_PENDING ? \'[Pending]\' : SupplierOrderContainer::getShipperName($data->id))',
		),
		array(
			'header'=>'Detail',
			'type'=>'raw',
			'value'=>'SupplierOrderContainer::model()->getDetailLink($data->id)',
		),
        array(
            'class' => 'CButtonColumn',
            'template' => '{archive}',
            'buttons' => array(
                'archive' => array(
                    'label' => 'Archive',
                    'imageUrl' => '/images/button_images/archive.gif',
                    'url' =>
                        'CHtml::normalizeUrl(array("/supplierOrderContainer/archive", "id" => $data->id));',
                    'options' => array(
                        'class' => 'ajax-operation',
                    ),
                ),
            ),
        ),
	),
)); ?>

<script>
jQuery(document).ready(function(){
	jQuery(".bid_order").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/shipping_bid/?order_id=" + order_id;
		window.location.href = url;
	});
});

jQuery('#resend_requests').click(function(e){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/resend_shipper_requests_ajax/";

	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == 'success'){
				alert('Successfully sent requests');
			} else {
				alert('Error sending requests');
			}
		}
	});
});

</script>
