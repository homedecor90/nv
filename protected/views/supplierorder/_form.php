<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-order-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'supplier_id'); ?>
		<?php echo $form->textField($model,'supplier_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'supplier_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accepted_date'); ?>
		<?php echo $form->textField($model,'accepted_date'); ?>
		<?php echo $form->error($model,'accepted_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'advanced_paid_date'); ?>
		<?php echo $form->textField($model,'advanced_paid_date'); ?>
		<?php echo $form->error($model,'advanced_paid_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'completed_date'); ?>
		<?php echo $form->textField($model,'completed_date'); ?>
		<?php echo $form->error($model,'completed_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->