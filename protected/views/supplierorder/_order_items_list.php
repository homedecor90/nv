<?php
$strAction = "";
$op  = YumUser::model()->isOperator(true);
$adm = Yii::app()->user->isAdmin();
if ((YumUser::model()->isSupplier(false))
		&& ($order->status != SupplierOrder::$STATUS_PENDING)){
	$strAction = '<th class="action">Action</th>';
}

$strItems = '<table class="items" id="order-items">'
.'<thead>'
// .'<th class="item_code">Item Code</th>'
.'<th class="image">Image</th>'
.($op ? '<th class="item_name">Item Name</th>' : '<th class="item_code">Item Code</th>')
.'<th class="color">Color</th>'
.'<th class="exw_price">EXW Price</th>'
.'<th class="fob_price">FOB Price</th>'
.'<th class="cbm">CBM</th>'
.'<th class="quantity">Qty</th>'
.'<th class="loaded">Loaded</th>'
.'<th class="added_date">Added Date</th>'
.'<th class="total_exw_price">Total EXW Price</th>'
.'<th class="total_fob_price">Total FOB Price</th>'
.'<th class="total_cbm">Total CBM</th>'
.'<th class="custom_color">Custom Color</th>'
.'<th class="notes">Notes</th>'
.'<th class="status">Status</th>';
if ($order->status != SupplierOrder::$STATUS_FULL_PAID && $order->status != SupplierOrder::$STATUS_ARCHIVED && $op){
	$strItems .= '<th class="edit">Edit</th>';
	$strItems .= '<th class="delete">Delete</th>';
}
$strItems .= $strAction
.'</thead>';
	
foreach ($order_items as $i=>$item){
	if ($item->status == SupplierOrderItems::$STATUS_DENIED){
		continue;
	}
	if ($item->is_replacement && !$order_items[$i-1]->is_replacement){
		$strItems .= '<tr><td colspan="15">Replacements</td></tr>';
	}
		
	if ($item->not_loaded_reordered && !$order_items[$i-1]->not_loaded_reordered){
		$strItems .= '<tr class="not_loaded_header"><td colspan="15">Not Loaded Previous Time</td></tr>';
	}
		
	$item_id = $item->item_id;
	// $item_info = Item::model()->findByPk($item_id);
	$item_info = $item->items;
	$item_total_exw_price = floatval($item->exw_cost_price) * floatval($item->quantity);
	$item_total_fob_price = floatval($item->fob_cost_price) * floatval($item->quantity);
	$item_total_cbm = $item->getTotalCBM();
		
	$strAction = "";
	if ((YumUser::model()->isSupplier(false))
			&& ($order->status != SupplierOrder::$STATUS_PENDING)){
		if ($item->status == "Pending"){
			$strAction = "<td class='action'>"
			.'<input class="item_accept" rel="'.$item->id.'" type="button" value="Accept"/><br/>'
			.'<input class="item_deny" rel="'.$item->id.'" type="button" value="Deny"/>'
			."</td>";
		} else if ($item->quantity_updated){
			$strAction = "<td class='action'>"
			.'<input class="item_qty_accept" rel="'.$item->id.'" type="button" value="Accept Changes"/><br/>'
			.'<input class="item_qty_deny" rel="'.$item->id.'" type="button" value="Deny Changes"/>'
			."</td>";
		} else {
			$strAction = "<td class='action'></td>";
		}
	}
		
	if ($item->status == SupplierOrder::$STATUS_PENDING){
		$strStatus = "<b>".$item->status."</b>";
	} else {
		$strStatus = "<b>".$item->status."</b><br/>".date("M. d, Y", strtotime($item->processed_date));
	}
		
	if ($item->is_replacement){
		$strStatus .= '<br /><br />[Replacement]';
	}
		
	$strItems .= '<tr data-id="'.$item->id.'" id="item_row_'.$item->id.'"
	    class="' . $item->status
        . ($item->not_loaded_reordered ? ' not_loaded_reordered' : '') . '">';
    $strItems .= '<td><img width="50" src="'.$item->items->getStoredImageUrl().'"/></td>';
	// $strItems .= "<td class='item_code'>".$item_info->item_code."</td>";
	$strItems .= ($op ? "<td class='item_name'>".($adm?'<a href="'.Yii::app()->getBaseUrl(true).'/index.php/item/update/'.$item_id.'">':'').$item_info->item_name.($adm?'</a>':'')."</td>" : "<td class='item_code'>".$item_info->item_code."</td>");
	$strItems .= "<td class='color'>".$item_info->color."</td>";
	$strItems .= "<td class='exw_price'>".$item->exw_cost_price."</td>";
	$strItems .= "<td class='fob_price'>".$item->fob_cost_price."</td>";
	$strItems .= "<td class='cbm'>".$item_info->cbm."</td>";
	if (($sold = $item->countSoldItems()) && !YumUser::model()->isSupplier(false)){
		$strItems .= '<td class="quantity sold" id="quantity_'.$item->id.'" title="'.$sold.' item(s) sold">'.$item->quantity.' '.($item->quantity_updated ? '&nbsp;<span>('.$item->new_quantity.')</span>' : '')."</td>";
	} else {
		$strItems .= '<td class="quantity" id="quantity_'.$item->id.'">'.$item->quantity.' '.($item->quantity_updated ? '&nbsp;<span>('.$item->new_quantity.')</span>' : '')."</td>";
	}
	$strItems .= '<td class="loaded">'.$item->getLoadedQty().'</td>';
	$strItems .= "<td class='added_date'>".date("M. d, Y", strtotime($item->added_date))."</td>";
	$strItems .= "<td class='total_exw_price'>".$item_total_exw_price."</td>";
	$strItems .= "<td class='total_fob_price'>".$item_total_fob_price."</td>";
	$strItems .= "<td class='total_cbm'>".$item_total_cbm."</td>";
	$strItems .= "<td class='custom_color'>".$item->custom_color."</td>";
	$strItems .= "<td class='notes'><span>" . $item->notes . "</span>"
        . (Yii::app()->user->isAdmin() ? CHtml::link(
            CHtml::image('/images/button_images/note.png'),
            array('/supplierorder/addItemNotes', 'id' => $item->id),
            array('class' => 'add-item-notes')
        ) : '') . "</td>";
	$strItems .= "<td class='status'>".$strStatus."</td>";
		
	if ($order->status != SupplierOrder::$STATUS_FULL_PAID && $order->status != SupplierOrder::$STATUS_ARCHIVED && $op){
        if ($item->not_loaded_reordered) {
            if (isset($item->initial_order_id))
                $strItems .= '<td class="edit">'
                    . CHtml::link('Scan In', array('supplierorder/scanItemsIn', 'itemId' => $item->id), array('class' => 'scan_in_item'))
                    . '</td>';
            else $strItems .= '<td class="edit"></td>';
            $strItems .= '<td class="delete"><a href="#" class="delete_old_item" data-itemid="'.$item->id.'" id="delete_'.$item->id.'">Delete</a></td>';
        } else {
            $strItems .= '<td class="edit"><a href="#" class="edit_old_item" data-itemid="'.$item->id.'" id="edit_'.$item->id.'">Edit</a></td>';
            $strItems .= '<td class="delete"><a href="#" class="delete_old_item" data-itemid="'.$item->id.'" id="delete_'.$item->id.'">Delete</a></td>';
        }
	}

	$strItems .= $strAction;
	$strItems .= "</tr>";
}

$strItems .= "</table>";

echo $strItems;

$this->widget('application.extensions.fancybox.EFancyBox', array(
        'target' => '.add-item-notes',
        'config' => array(),
    )
);
?>

<script type="text/javascript">
/**************************************************/
/* Delete old items from the order
/**************************************************/

// remember not to conflict with existing variables

jQuery('.delete_old_item').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
    var old_item_id = jQuery(e.target).attr('data-itemid');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/delete_existing_items_ajax/?items_id=" + old_item_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                // just delete table row
                // jQuery('#delete_'+old_item_id).parent().parent().remove();
                jQuery('#item_row_'+old_item_id).remove();
            }
		}
	});
    e.preventDefault();
    e.stopImmediatePropagation();
});

/**************************************************/
/* Edit old items
/**************************************************/

jQuery('.edit_old_item').live('click', function(e){
    // show inout boxes instead of numbers in the table
    var old_item_id = jQuery(e.target).attr('data-itemid');
    
    //var old_value = jQuery('#qty_'+old_item_id).text();
    var new_value = prompt('Input new quantity please:');

    if (!parseInt(new_value)){
        return false;
    }
    
    // var row = jQuery(e.target).parent().parent();
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/update_existing_items_ajax/?items_id=" + old_item_id +'&new_value='+new_value;	
    jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                //jQuery.fn.yiiGridView.update('supplier-order-grid');
                jQuery('#quantity_' + old_item_id).html(result.qty);
            }
		}
	});
    
    e.preventDefault();
    e.stopImmediatePropagation();

});
/**************************************************/
/* Scan items in
 /**************************************************/
$(document).on('click', '.scan_in_item', function(e){
    e.preventDefault();
    var row = $(this).closest('tr');
    var curQuantity = parseInt(row.find('.quantity').text());
    var quantity = prompt('Please, enter quantity to scan in');
    $.post($(this).attr('href'), {'quantity': quantity}, function(resp){
        if (curQuantity <= resp.movedCount) row.remove();
        if ($('#order-items tr.not_loaded_reordered').length === 0)
            $('#order-items tr.not_loaded_header').remove();
        alert(resp.message);
    }, 'json');
});

$(document).on('submit', '#supplierOrderItems-notes-form', function(e){
    e.preventDefault();
    var form = $(this);
    $.post(form.attr('action'), form.serialize(), function(resp){
        if (resp.status == 'success') {
            var text = form.find('#SupplierOrderItems_notes').val();
            $('#item_row_' + resp.item_id + ' span').html(text);
            parent.jQuery.fancybox.close();
        } else {
            alert(resp.message);
        }
    }, 'json');
});
</script>