<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_id')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accepted_date')); ?>:</b>
	<?php echo CHtml::encode($data->accepted_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advanced_paid_date')); ?>:</b>
	<?php echo CHtml::encode($data->advanced_paid_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('completed_date')); ?>:</b>
	<?php echo CHtml::encode($data->completed_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>