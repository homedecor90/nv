<h1>Containers</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-by-shipper-grid',
	'dataProvider'=>$supplierOrderContainerModel->searchAvailable(),
	'columns'=>array(
		'id',
		array(
			'header'=>'Orders',
			'value'=>'SupplierOrderContainer::model()->getOrderInfoByShipper($data->id)',
			'type'=>'raw',
		),
		'destination_port',
		'original_port',
		array(
			'header'=>'Total CBM',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalCBM($data->id)',
			'type'=>'raw',
		),
		/*array(
			'header'=>'Total EXW Shipping Price',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPriceWithShipping($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total FOB Shipping Price',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPriceWithShipping($data->id)',
			'type'=>'raw',
		),*/
		array(
			'header'=>'Cut-Off Date',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerBidCutoffDate($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Bid E.T.A',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerBidEta($data->id)',
			'type'=>'raw',
		),
		'status',
		array(
			'header'=>'Action',
			'value'=>'SupplierOrderContainer::model()->getActionButtonStringByShipper($data->id)',
			'type'=>'raw',
		),
	),
)); ?>

<script>
var option_index = 0;

var conatiner_exw_price_data = new Array();
var conatiner_fob_price_data = new Array();

jQuery(document).ready(function(){
	jQuery('.view_detail').live("click", function(){
		var container_id = jQuery(this).attr("rel");
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/container_detail_by_shipper/?container_id=" + container_id;
		window.location.href = url;
	});
});
</script>