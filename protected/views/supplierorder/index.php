<?php
$this->menu=array(
	array('label'=>'Archived Supplier Orders', 'url'=>array('index_archived')),
);
?>

<h1>My Orders</h1>

<input type="button" value="Set E.T.L" href="#setEtlDiv" id="setEtaButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#setEtaButton',
    'config'=>array(
		'width'=>240,
		'height'=>120,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="setEtlDiv" class="fancy_box_div">
<?php
	Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
	$this->widget('CJuiDateTimePicker',array(
		'model'=>$model, //Model object
		'attribute'=>'etl', //attribute name
		'mode'=>'date', //use "time","date" or "datetime" (default)
		'language'=>'en-GB',
		'options'=>array(
			"dateFormat"=>"M. dd, yy"
		) // jquery plugin options
	));
?>
<input type="button" value="Set E.T.L" id="set_etl_fancy"/>
</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'self-supplier-order-grid',
	'dataProvider'=>$model->search(Yii::app()->user->id),	
    'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'header'=>'Created Date',
			'value'=>'date("M. d, Y", strtotime($data->created_date))',
		),
		/*array(
			'header'=>'Items',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderItems($data->id)',
		),*/
		array(
			'header'=>'Total Price',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderTotalPrice($data->id)',
		),
		array(
			'header'=>'Total CBM',
			'type'=>'raw',
            // 'value'=>'SupplierOrder::model()->getSupplierOrderTotalCBM($data->id)',
            'value'=>'\'<span href="#order_contents_\'.$data->id.\'" id="open_order_contents_\'.$data->id.\'" class="total_cbm">\'.SupplierOrder::model()->getSupplierOrderTotalCBM($data->id).\'</span>\'.SupplierOrder::getItemsPopupCodeSupplier($data->id)',
		),
		array(
			'header'=>'E.T.L',
			'type'=>'raw',
            'value'=>'($data->etl==NULL)?"":date("M. d, Y", strtotime($data->etl))',
		),
		array(
			'header'=>'Status',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderStatus($data->id)',
		),
		array(
			'header'=>'Action',
			'type'=>'raw',
			'value'=>'SupplierOrder::model()->getActionButtonString($data->id)',
		),
	),
)); ?>

<input type="button" value="Upload Order Attach" href="#uploadOfficialDocsDiv" id="uploadOfficialDocsButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#uploadOfficialDocsButton',
    'config'=>array(
		'width'=>400,
		'height'=>250,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
<div id="uploadOfficialDocsDiv" class="fancy_box_div">
<br/><br/>
<?php
// Ajax Upload View Part
$this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
	'id'=>'uploadOrderAttach',
	'config'=>array(
		'action'=>Yii::app()->createUrl('supplierorder/upload'),
		'allowedExtensions'=>array("pdf","jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
		'sizeLimit'=>10*1024*1024,// maximum file size in bytes
		'multiple'=>true,
		'minSizeLimit'=>0,// minimum file size in bytes
		'onComplete'=>"js:function(id, file_name, responseJSON){ onCompleteUploadOfficialDocs(file_name); }",
		'onProgress'=>"js:function(id, file_name, loaded, total){ onProgessOfficialDocs(file_name, loaded, total); }"
	)
)); 
?>
<div id="progress"></div>
</div>
</div>

<input type="button" value="Bank Info" href="#bank_info_block" id="enter_bank_info" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#enter_bank_info',
    'config'=>array(
		'width'=>400,
		'height'=>480,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
<div id="bank_info_block" class="fancy_box_div">
    <?php if ($bank_info->id): ?>
    <h3>Bank Info</h3>
    
    <div id="old_bank_info" class="view">
        <strong>Account Name: </strong><?php echo $bank_info->account_name; ?><br />
        <strong>Account Address: </strong><?php echo $bank_info->account_address; ?><br />
        <strong>Account Number: </strong><?php echo $bank_info->account_number; ?><br />
        <strong>Routing Number: </strong><?php echo $bank_info->routing_number; ?><br />
        <strong>Bank Name: </strong><?php echo $bank_info->bank_name; ?><br />
        <strong>Bank Address: </strong><?php echo $bank_info->bank_address; ?><br />
        <input type="button" value="OK" id="bank_info_correct" />
        <input type="button" value="Edit" id="bank_info_edit" />
    </div>
    
    <div id="bank_info_form_container" style="display: none">
    <?php else: ?>
    <h3>Please enter your bank info</h3>
    <div id="bank_info_form_container">
    
    <?php endif; ?>
        <form id="bank_info_form">
            
            <br />
            <label>Account Name</label><br />
            <input type="text" id="account_name" name="account_name" value="<?php echo $bank_info->account_name; ?>" />
            
            <br />
            <label>Account Address</label><br />
            <input type="text" id="account_address" name="account_address" value="<?php echo $bank_info->account_address; ?>" />
            
            <br />
            <label>Account Number</label><br />
            <input type="text" id="account_number" name="account_number" value="<?php echo $bank_info->account_number; ?>" />
            
            <br />
            <label>Rounting Number</label><br />
            <input type="text" id="routing_number" name="routing_number" value="<?php echo $bank_info->routing_number; ?>" />
            
            <br />
            <label>Bank Name</label><br />
            <input type="text" id="bank_name" name="bank_name" value="<?php echo $bank_info->bank_name; ?>" />
            
            <br />
            <label>Bank Address</label><br />
            <input type="text" id="bank_address" name="bank_address" value="<?php echo $bank_info->bank_address; ?>" />
            <br />
            <input type="button" value="Save" id="save_bank_info" />
        </form>
    </div>
</div>
</div>

<script>
var current_order_id = 0;
var arr_upload_progress = new Array();

jQuery(document).ready(function(){
	jQuery(".order_detail").live("click", function(){
		var order_id = jQuery(this).attr("rel");		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/order_detail/?order_id=" + order_id;
		window.location.href = url;
	});
	
	jQuery(".accept_order").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		current_order_id = order_id;
        
		// var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_order_by_supplier_ajax/?order_id=" + order_id;
        
        jQuery('#enter_bank_info').trigger('click');
        
        return;
		// jQuery.ajax({
			// url:url,
			// dataType:"json",		
			// success:function(result){
				// jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			// }
		// });
	});
	
    jQuery('#save_bank_info').live('click', function(e){
        jQuery(e.target).attr('disabled', 'disabled');
        var data = jQuery('#bank_info_form').serialize();
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/save_bank_info_by_supplier_ajax";

		jQuery.ajax({
			url:url,
            type: 'post',
            data: data,
			dataType:"json",		
			success:function(result){
                jQuery.fancybox.close();
				//jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});

        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_order_by_supplier_ajax/?order_id=" + current_order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",		
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
    });
    
    jQuery('#bank_info_correct').live('click', function(e){
        jQuery.fancybox.close();
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_order_by_supplier_ajax/?order_id=" + current_order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",		
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
    });
    
    jQuery('#bank_info_edit').live('click', function(e){
        jQuery('#bank_info_form_container').show();
    });
    
	jQuery(".deny_order").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_order_by_supplier_ajax/?order_id=" + order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",		
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".set_eta").live("click",function(){
		var order_id = jQuery(this).attr("rel");
		current_order_id = order_id;
		jQuery("#setEtaButton").trigger("click");
	});
	
	jQuery(".complete").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/complete_order_by_supplier_ajax/?order_id=" + order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery("#set_etl_fancy").live("click", function(){
		var order_id = current_order_id;
		var etl_date = jQuery("#SupplierOrder_etl").val();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/set_etl_by_supplier_ajax/?"
			+ "order_id=" + order_id + "&etl_date=" + etl_date;		
		jQuery.fancybox.close();

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".item_accept").live("click", function(){
		var item_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_item_by_supplier_ajax/?item_id=" + item_id;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".item_deny").live("click", function(){
		var item_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_item_by_supplier_ajax/?item_id=" + item_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".upload_documents").live("click", function(){
		alert("This order will now be archived.");
		var order_id = jQuery(this).attr("rel");
		current_order_id = order_id;
		
		jQuery(".upload_button").html("Upload Official Documents");
		jQuery("#uploadOfficialDocsButton").trigger("click");
	});
});

function onCompleteUploadOfficialDocs(file_name){
	var order_id = current_order_id;
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/upload_official_docs_by_supplier_ajax/?"
			+ "order_id=" + order_id + "&file_name=" + file_name;

	jQuery.ajax({
		url:url,
		success:function(result){
			jQuery.fn.yiiGridView.update('self-supplier-order-grid');
		}
	});
}

function onProgessOfficialDocs(file_name, loaded, total){
	var strFileProgress = "<div>" + file_name + " : " + loaded + " of "+ total + " Bytes uploaded</div>";
	arr_upload_progress[file_name] = strFileProgress;
	
	var strProgress = "";
	for (key in arr_upload_progress){
		strProgress += arr_upload_progress[key];
	}
	
	jQuery("#progress").html(strProgress);
}

jQuery('.item_qty_accept').live('click', function(e){
    var items_id = jQuery(e.target).attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_qty_change_ajax/?items_id=" + items_id;

	jQuery.ajax({
		url:url,
		success:function(result){
			if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            jQuery.fancybox.close();
            jQuery.fn.yiiGridView.update('self-supplier-order-grid');
		}
	});
});

jQuery('.item_qty_deny').live('click', function(e){
    var items_id = jQuery(e.target).attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_qty_change_ajax/?items_id=" + items_id;

	jQuery.ajax({
		url:url,
		success:function(result){
			if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            jQuery.fancybox.close();
            jQuery.fn.yiiGridView.update('self-supplier-order-grid');
		}
	});
});
</script>