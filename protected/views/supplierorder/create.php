<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	'Create',
);*/

$this->menu=array(
	array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
);
?>

<h1>Create Supplier Order</h1>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#add_new_item',
    'config'=>array(
		'width'=>500,
		'height'=>500,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="add_item_box" class="fancy_box_div">
<?php $this->renderPartial('add_new_item',array(
	"newItemModel"=>$newItemModel,
)); ?>
</div>
</div>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-order-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="order">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>	

	<?php
		if (YumUser::model()->isSupplier(false)) {
			$user_id = Yii::app()->user->id;
			echo $form->hiddenField($model, 'supplier_id', array('value'=>$user_id));
		} else {
		
	?>
			<div class="row">
				<?php echo $form->labelEx($model,'supplier_id'); ?>
				<?php echo $form->dropDownList($model, 'supplier_id', $supplierList);  ?>		
				<?php echo $form->error($model,'supplier_id'); ?>
			</div>
	<?php	
		}
	?>
	<div class="row">
        <?php echo $form->labelEx($model,'discount_amount'); ?>
        <?php echo $form->textField($model,'discount_amount'); ?>
        <?php echo $form->error($model,'discount_amount'); ?>
    </div>
	<h3 id="pending_loading_items_h">Pending Loading Items</h3>
	<div id="pending_loading_items_div" class="grid-view">
	</div>
	
	<div class="row total_price">
		<div class="total_fob_price">
			Total FOB Price:<span>0</span>
		</div>
		<div class="total_exw_price">
			Total EXW Price:<span>0</span>
		</div>
	</div>
	
	<table id="order_item_list">
		<thead>
			<th class="item_code">Item Name</th>
			<th class="color">Color</th>
			<th class="quantity">Quantity</th>
			<th class="total_exw_price">Total EXW Price</th>
			<th class="total_fob_price">Total FOB Price</th>
            <th class="custom_color">Custom Color</th>
            <th class="custom_color">Notes</th>
			<th class="action">Action</th>
		</thead>		
	</table>

	<div class="row buttons">
		<?php echo CHtml::Button('Add Items', array('id'=>'add_existing_item')); ?>
		<?php echo CHtml::Button('Add New Items', array('id'=>'add_new_item', "href"=>"#add_item_box")); ?>
		<?php echo CHtml::submitButton('Create Supplier Order', array('id'=>'calculate_total')); ?>		
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
var item_count = 0;
var item_number = 0;
var strCodeList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_code', '', $itemCodeList['itemCodeList']));?>';
var strColorList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_color', '', $itemCodeList['itemColorList']));?>';
var strQuantity = '<?php echo str_replace("\n", "", CHtml::textField('quantity', '1'));?>';
var strCustomColor = '<?php echo str_replace("\n", "", CHtml::textField('custom_color', ''));?>';
var strNotes = '<?php echo str_replace("\n", "", CHtml::textField('notes', ''));?>';

jQuery(document).ready(function(){
	/**************************************************
	/* Event when click the create new order
	/**************************************************/
	jQuery("#calculate_total").click(function(){
		if (item_count == 0){
			alert("Please input the item");
			return false;
		}
        
        // Check if items have prices before adding them to the order
        var failed = false;
        jQuery(".item_row").each(function(){
            var row_id = jQuery(this).attr("id");
            var exw_cost_price = parseInt(jQuery("#" + row_id + " .action input.exw_cost_price").val());
            var fob_cost_price = parseInt(jQuery("#" + row_id + " .action input.fob_cost_price").val());
            var quantity = jQuery("#" + row_id + " .quantity input").val();
            
            if (isNaN(exw_cost_price) || fob_cost_price == 0 || exw_cost_price == 0){
                failed = true;
            }
            
        });
        
        if (failed){
            alert('Some items have no price specified. Please specify valid FOB/EXW price and CBM before adding them to the order');
            return false;
        }
        
		var bNumber = true;
		
		jQuery(".quantity input").each(function(){
			var quantity = jQuery(this).val();
			if (isNaN(quantity)){
				alert("Pleas input the number as quantity");
				jQuery(this).focus();
				bNumber = false;
				return false;
			}
		});
		
		jQuery('#pending_loading_items_div input[type="text"].new_quantity').each(function(){
			var quantity = jQuery(this).val();
			if (isNaN(quantity)){
				alert("Pleas input the number as quantity");
				jQuery(this).focus();
				bNumber = false;
				return false;
			}
			
			var item_id = jQuery(this).attr("rel");
			var pending_quantity = jQuery("#pending_quantity_" + item_id).val();
			if (quantity < 0 || quantity > pending_quantity){
				alert("Pleas input the exact number as quantity");
				jQuery(this).focus();
				bNumber = false;
				return false;
			}
		});
		
		if (bNumber == false){
			return false;
		}
	});
	
	/**************************************************
	/* Event when click the add new item
	/**************************************************/
	jQuery("#add_existing_item").click(function(){
		item_number++;		
		var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'
				+ '<td class="item_code">' + strCodeList + '</td>'
				+ '<td class="color">' + strColorList + '</td>'
				+ '<td class="quantity">' + strQuantity + '</td>'
				+ '<td class="total_exw_price"></td>'
				+ '<td class="total_fob_price"></td>'
                + '<td class="custom_color">' + strCustomColor + '</td>'
                + '<td class="notes">' + strNotes + '</td>'
				+ '<td class="action"><a href="#" class="delete_item">Delete</a></td>'
				+ '</tr>';

		jQuery("#order_item_list").append(newRowHtml);
		jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");
		jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]");
		jQuery("#item_" + item_number + " .custom_color input").attr("name", "item[custom_color][]");
		jQuery("#item_" + item_number + " .notes input").attr("name", "item[notes][]");
		item_count++;
		changeOrderItemList();
	});
	
	
	/**************************************************
	/* Event when change the item code
	/**************************************************/
	jQuery(".item_code select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		changeItemCode(item_cur_number);
	});
	
	/**************************************************
	/* Event when change the color
	/**************************************************/
	jQuery(".color select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		var item_id = jQuery(this).val();
		changeItemColor(item_cur_number, item_id);
	});
	
	/**************************************************
	/* Event when change the quantity
	/**************************************************/
	jQuery(".quantity input").live("change",function(){
		calculateTotal();
	});
	
	/**************************************************
	/* Event when click the delete button
	/**************************************************/
	jQuery(".delete_item").live("click",function(){
		jQuery(this).parent().parent().remove();
		item_count--;
		changeOrderItemList()
		return false;
	});
	
	/**************************************************
	/* Event when click the Submit button on lightbox
	/**************************************************/
	jQuery("#fancybox-content #add_new_item_submit").live("click", function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/add_new_item_on_supplier_order_ajax";
		var postData = jQuery("#fancybox-content form#item-form").serialize();
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					alert(result.error);
					return;
				}
				
				var item_code = result.data.item_code;
				var quantity = result.data.quantity;
				
				var str_new_option = '<option value="' + item_code +'">' + item_code + '</option>';
				if (strCodeList.indexOf(str_new_option) == -1){
					strCodeList = strCodeList.replace('</select>', str_new_option + '</select>');
					jQuery(".item_code select").append(str_new_option);
				}
				
				item_number++;		
				var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'
						+ '<td class="item_code">' + strCodeList + '</td>'
						+ '<td class="color">' + strColorList + '</td>'
						+ '<td class="quantity">' + strQuantity + '</td>'
						+ '<td class="total_exw_price"></td>'
						+ '<td class="total_fob_price"></td>'
						+ '<td class="action"></td>'
						+ '</tr>';

				jQuery("#order_item_list").append(newRowHtml);
				jQuery("#item_" + item_number + " .item_code select").val(item_code);
				jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");
				jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]").val(quantity);
				item_count++;
				changeOrderItemList();
				
				jQuery.fancybox.close();
			}
		});
	});
	
	jQuery("#SupplierOrder_supplier_id").change(function(){
		changePendingLoadingItemsBySupplierId();
	});
	
	changeOrderItemList();
	changePendingLoadingItemsBySupplierId();
});

/**************************************************
/* Add the inputed data into the 
/**************************************************/
function calculateTotal(){
	var exw_price = 0;
	var fob_price = 0;
	
	jQuery(".item_row").each(function(){
		var row_id = jQuery(this).attr("id");
		var exw_cost_price = jQuery("#" + row_id + " .action input.exw_cost_price").val();
		var fob_cost_price = jQuery("#" + row_id + " .action input.fob_cost_price").val();
		var quantity = jQuery("#" + row_id + " .quantity input").val();
		
		if (isNaN(quantity) || isNaN(exw_cost_price)){
			return 0;
		}
		
		var item_total_exw_price = (parseFloat(exw_cost_price)) * parseInt(quantity);
		var item_total_fob_price = (parseFloat(fob_cost_price)) * parseInt(quantity);
		item_total_exw_price = parseFloat(item_total_exw_price.toFixed(2));
		item_total_fob_price = parseFloat(item_total_fob_price.toFixed(2));
		
		jQuery("#" + row_id + " .total_exw_price").html(item_total_exw_price);
		jQuery("#" + row_id + " .total_fob_price").html(item_total_fob_price);
		exw_price += item_total_exw_price;
		fob_price += item_total_fob_price;
	});
	
	jQuery(".total_exw_price span").html(exw_price);
	jQuery(".total_fob_price span").html(fob_price);
}

/**************************************************
/* Reflect the item change status to the grid view
/**************************************************/
function changeOrderItemList(){
	if (item_count == 0){
		jQuery("#order_item_list").append('<tr class="order_item_empty"><td colspan=6>No items are added</td></tr>');
	} else {
		jQuery(".order_item_empty").remove();
		changeItemCode(item_number);
	}
}

/**************************************************
/* Get the color when change the item code
/**************************************************/
function changeItemCode(item_cur_number){
	var item_code = jQuery("#item_" + item_cur_number + " #item_code").val();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/itemcolorlist/?item_code=" + encodeURIComponent(item_code);
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;

			var itemColorList = result.itemColorList;
			var selectText = "";
			var bChangeColor = false;
			
			for (var item_id in itemColorList){
				if (!bChangeColor){
					changeItemColor(item_cur_number, item_id);
					bChangeColor = true;
				}					
				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';
			}
			jQuery("#item_" + item_cur_number + " .color select").html(selectText);
		}
	});
}

/**************************************************
/* Get the item info when change the color
/**************************************************/
function changeItemColor(item_cur_number, item_id){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/iteminfo/?id=" + item_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;
			
			var itemInfo = result.itemInfo;			
			var str_action_field = "<input type='hidden' class='exw_cost_price' value='" + itemInfo.exw_cost_price + "'/>"
						+ "<input type='hidden' class='fob_cost_price' value='" + itemInfo.fob_cost_price + "'/>"
						+ '<a href="#" class="delete_item">Delete</a>';
			
			jQuery("#item_" + item_cur_number + " .action").html(str_action_field);
			calculateTotal();
		}
	});
}

function changePendingLoadingItemsBySupplierId(){
	var supplier_id = jQuery("#SupplierOrder_supplier_id").val();	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/get_pending_loading_items_by_supplier_id/?supplier_id=" + supplier_id;
	
	jQuery.ajax({
		url:url,
		success:function(result){
			if (result){
                jQuery("#pending_loading_items_h").show();
            }
            jQuery("#pending_loading_items_div").html(result);
		}
	});
}
</script>