<h1>Shipment Details</h1>

<?php
if (YumUser::model()->isSupplierShipper(false)){
	$this->menu=array(
		array('label'=>'Available Supplier Orders', 'url'=>array('shipping')),
	);
} else {
	$this->menu=array(
		array('label'=>'Create Supplier Order', 'url'=>array('create')),
		array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
	);
}
?>

<?php 

$columns = array(
		'id',
        /*array(
			'header'=>'Created On',				
			'value'=>'date("M. d, Y", strtotime($data->created_date))',
		),*/
		/*array(
			'header'=>'Orders',
			'value'=>'SupplierOrderContainer::model()->getOrderInfoByShipper($data->id)',
			'type'=>'raw',
		),*/
        array(
            'header'=>'Commodity',
            'value'=>'\'Living room furniture (duty free)\'',
            'type'=>'raw'
        ),
		array(
			'header'=>'Total EXW Price',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total FOB Price',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total CBM',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalCBM($data->id)',
			'type'=>'raw',
		),
		//'status',
//		array(
//            'header'=>'Loading Date',
//			'value'=>'date("M. d, Y", $data->getETL2())',
//			'type'=>'raw',
//        ),
        array(
            'header'=>'Supplier Address',
            'type'=>'raw',
            'value'=>'$data->getSupplierAddressStr()',
        ),
        array(
            'header'=>'Arrival Port',
			'value'=>'$data->destination_port',
			'type'=>'raw',
        ),
	);

if ($supplierOrderContainerModel->supplier_shipper_id == Yii::app()->user->id){
	$columns[] = array(
		'header'=>'Official Docs',
		'type'=>'raw',
		'value'=>'$data->getOfficialDocsList()'
	);
}
	
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'supplier-order-container-grid',
		'dataProvider'=>$supplierOrderContainerModel->search(),
		'columns'=>$columns,
	)
); ?>

<?php
if ($supplierOrderContainerModel->status != SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER){
	if (!$bAlreadyBid){
?>
		<input type="button" value="Submit your quote" href="#bidPriceDiv" id="editPriceButton"/>
<?php
	}
} 
?>
<br/><br/><br/>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#editPriceButton',
    'config'=>array(
		'width'=>550,
		'height'=>430,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="bidPriceDiv" class="fancy_box_div">
        <form id="frmBidPrice">
            <div id="editPriceDiv_1" class="active editPriceDiv">
                <h3>EXW China Charges</h3>
                    
                    <?php
                        $i = 0;
                        foreach (SupplierOrderContainer::getEXWFields() as $exw_field){
                            $i++;
                    ?>
                            <div class="row">
                                <label><?php echo $exw_field; ?></label>
                                <input type="hidden" id="option_name_<?php echo $i;?>" name="option_name[<?php echo $i;?>]" value="<?php echo $exw_field; ?>"/>
                                <input type="text" class="option_price" id="option_price_<?php echo $i;?>" name="option_price[<?php echo $i;?>]" value="0"/>
                            </div>
                    <?php
                        }
                    ?>
                    
                    <input type="button" value="Submit China Charges, Enter USA Charges" id="switch_price_form_button"/>
            </div>
            <div id="editPriceDiv_2" class="editPriceDiv">
                    <input type="hidden" name="supplier_shipper_id" value="<?php echo Yii::app()->user->id; ?>"/>
                    <input type="hidden" id="supplier_order_container_id" name="supplier_order_container_id" value="<?php echo $supplierOrderContainerModel->id;?>"/>
                    
                    <h3>FOB Shipment Charges</h3>
                    
                    <?php			
                        foreach (SupplierOrderContainer::getCommonFields() as $common_field){
                            $i++;
                    ?>
                            <div class="row">
                                <label><?php echo $common_field; ?></label>
                                <input type="hidden" id="option_name_<?php echo $i;?>" name="option_name[<?php echo $i;?>]" value="<?php echo $common_field; ?>"/>
                                <input type="text" class="option_price" id="option_price_<?php echo $i;?>" name="option_price[<?php echo $i;?>]" value="0"/>
                            </div>
                    <?php
                        }
                    ?>
            
                <div>
                    <!--input type="button" value="Reset Price" id="reset_price_button"/-->
                    <input type="button" value="Edit China Quote" id="back_price_form_button"/>
                    <input type="button" value="Preview" id="preview_quote_button"/>
                </div>
            </div>
        </form>
		<div id="bidPreviewDiv" class="editPriceDiv fancy_box_div">
            <h3>EXW China Charges</h3>
                    
            <?php
                $i = 0;
                foreach (SupplierOrderContainer::getEXWFields() as $exw_field){
                    $i++;
            ?>
                    <div class="row">
                        <label><?php echo $exw_field; ?></label>
                        <span id="option_price_value_<?php echo $i;?>"></span>
                    </div>
            <?php
                }
            ?>
            <h3>FOB Shipment Charges</h3>
                    
            <?php			
                foreach (SupplierOrderContainer::getCommonFields() as $common_field){
                    $i++;
            ?>
                    <div class="row">
                        <label><?php echo $common_field; ?></label>
                        <span id="option_price_value_<?php echo $i;?>"></span>
                    </div>
            <?php
                }
            ?>
            <br />
            <strong>EXW Term: <span id="exw_total_value"></span></strong><br />
            <strong>FOB Term: <span id="fob_total_value"></span></strong>
            <div>
                <!--input type="button" value="Reset Price" id="reset_price_button"/-->
                <input type="button" value="Edit Quote" id="edit_quote_button"/>
                <input type="button" value="Submit Quote" id="set_price_button"/>
            </div>
        </div>
		<div id="bidDateDiv" class="fancy_box_div">
			<form id="frmBidDate">
				<h3>Date</h3>
				
                <div class="row">
                    <label>Container size</label>
                    <select name="container_size">
                        <option value="25HQ">25HQ</option>
                        <option value="40HF">40HF</option>
                        <option value="40HQ">40HQ</option>
                        <option value="45HQ">45HQ</option>
                    </select>
                </div>
				<div class="row">
					<label>Cut-Off Date</label>
					<?php
						Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
						$this->widget('CJuiDateTimePicker',array(
							'model'=>$supplierOrderContainerBiddingModel, //Model object
							'attribute'=>'cutoff_date', //attribute name
							'mode'=>'date', //use "time","date" or "datetime" (default)
							'language'=>'en-GB',
							'options'=>array(
								"dateFormat"=>"M. dd, yy"
							), // jquery plugin options,
							'htmlOptions'=>array(
								'name'=>"cutoff_date",
							),
						));
					?>
				</div>
				
				<div class="row">
					<label>E.T.A to Los Angeles/ Long Beach</label>
					<?php
						Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
						$this->widget('CJuiDateTimePicker',array(
							'model'=>$supplierOrderContainerBiddingModel, //Model object
							'attribute'=>'bid_eta', //attribute name
							'mode'=>'date', //use "time","date" or "datetime" (default)
							'language'=>'en-GB',
							'options'=>array(
								"dateFormat"=>"M. dd, yy"
							), // jquery plugin options,
							'htmlOptions'=>array(
								'name'=>"bid_eta",
							),
						));
					?>
				</div>
			</form>
		
			<div>
				<input type="button" value="Back" id="preivous_button"/>
				<input type="button" value="Bid" id="bid_container_button"/>
			</div>
		</div>
	</div>
    
</div>

<h3>Quotes from Other Custom / Freight Brokers</h3>
<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-bidding-grid',
	'dataProvider'=>$supplierOrderContainerBiddingModel->search($supplierOrderContainerModel->id),
	'columns'=>array(
		array(
			'header'=>'Company Name',
			'value'=>'SupplierOrderContainerBidding::model()->getShipperName($data->id)',
			'type'=>'raw',
		),
        array(
			'header'=>'ETA in Long Beach - Los Angeles Port',
			'value'=>'date("M, d. Y", strtotime($data->bid_eta))',
			'type'=>'raw',
		),
        array(
            'type'=>'raw',
            'header'=>'EXW Quote',
            'value'=>'\'<span class="fob_total">\'.($exw = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalEXWPrice($data->id)).\'</span>\'.($exw == '.$lowest_price.'? \'&nbsp;<span class="lowest_price">Lowest Quote</span>\': \'\')'
        ),
        array(
            'type'=>'raw',
            'header'=>'FOB Quote',
            'value'=>'\'<span class="exw_total">\'.($fob = SupplierOrderContainerBidding::model()->getBiddingOptionsTotalFOBPrice($data->id)).\'</span>\'.($fob == '.$lowest_price.'? \'&nbsp;<span class="lowest_price">Lowest Quote</span>\': \'\')'
        ),
		
	),
)); ?>

<script>
var current_container_id = <?php echo $supplierOrderContainerModel->id?>;
var bid_price_data;

jQuery(document).ready(function(){
	jQuery("#reset_price_button").live("click",function(){
		jQuery(".option_price").val(0);
	});
	
	jQuery("#switch_price_form_button").live("click",function(){
        jQuery("#editPriceDiv_1").removeClass("active");
		jQuery("#editPriceDiv_2").addClass("active");
		// jQuery("#fancybox-content").height(180);
    });
    
    jQuery("#back_price_form_button").live("click",function(){
        jQuery("#editPriceDiv_2").removeClass("active");
		jQuery("#editPriceDiv_1").addClass("active");
		// jQuery("#fancybox-content").height(180);
    });
    
    jQuery('#edit_quote_button').live('click', function(){
        jQuery("#bidPreviewDiv").removeClass("active");
		jQuery("#editPriceDiv_1").addClass("active");
    });
    
	jQuery("#preview_quote_button").live("click",function(){
        var bValidate = true;
		jQuery("input.option_price").each(function(){			
			var option_price = jQuery(this).val();
            if (option_price == "" || isNaN(option_price)){
				alert("Please input exact shipping price");
				bValidate = false;
				jQuery(this).focus();
				return false;
			}
		});
		
        // fill preview fields and show preview page
        
        bid_price_data = jQuery("form#frmBidPrice").serializeArray();
        var total_exw_price = 0;
		var total_fob_price = 0;
		
		for (var key in bid_price_data){
			var array_item_name = bid_price_data[key]["name"];
			
			if (array_item_name.indexOf("option_price") != -1){
                var option_price_index = array_item_name.replace("option_price[", "");
				option_price_index = parseInt(option_price_index.replace("]", ""));
				option_price_value = parseFloat(bid_price_data[key]["value"]);
                // console.log(option_price_value);
                jQuery('#option_price_value_'+option_price_index).text('$'+option_price_value);
                
                total_exw_price += option_price_value;
				if (option_price_index > 7){
					total_fob_price += option_price_value;
				}
			}
		}
        
        jQuery('#exw_total_value').text('$'+total_exw_price);
        jQuery('#fob_total_value').text('$'+total_fob_price);
        
        jQuery("#editPriceDiv_2").removeClass("active");
        jQuery("#bidPreviewDiv").addClass("active");
        // jQuery("#fancybox-content").height(180);         
    });
    
	jQuery("#set_price_button").live("click",function(){
		
		
		//jQuery.fancybox.close();
		// if (bid_price_data == null){
			// conatiner_exw_price_data = parseFloat(jQuery("p#total_exw_price_" + current_container_id).html());
			// conatiner_fob_price_data = parseFloat(jQuery("p#total_fob_price_" + current_container_id).html());
		// }
		
		bid_price_data = jQuery("form#frmBidPrice").serializeArray();

		var total_exw_price = 0;
		var total_fob_price = 0;
		
		for (var key in bid_price_data){
			var array_item_name = bid_price_data[key]["name"];
			
			if (array_item_name.indexOf("option_price") != -1){
				var option_price_index = array_item_name.replace("option_price[", "");
				option_price_index = parseInt(option_price_index.replace("]", ""));
				option_price_value = parseFloat(bid_price_data[key]["value"]);
				
				total_exw_price += option_price_value;
				if (option_price_index > 7){
					total_fob_price += option_price_value;
				}
			}
		}
		
		// jQuery("p#total_exw_price_" + current_container_id).html(total_exw_price);
		// jQuery("p#total_fob_price_" + current_container_id).html(total_fob_price);
        
        // check if bid is lower than others
        var not_lower = false;
        jQuery('.fob_total').each(function(e){
            if (total_fob_price > parseFloat(jQuery(this).text())){
                not_lower = true;
            }
        });
        
        if (not_lower){
            var msg = 'Your bid is not the lowest one, place anyway?';
            if (confirm(msg)){
                jQuery("#bidPreviewDiv").removeClass("active");
                jQuery("#bidDateDiv").addClass("active");
                jQuery("#fancybox-content").height(180);    
            }
        } else {
            jQuery("#bidPreviewDiv").removeClass("active");
            jQuery("#bidDateDiv").addClass("active");
            jQuery("#fancybox-content").height(180);            
        }
        
	});
	
	jQuery("#preivous_button").live("click", function(){
		jQuery("#bidPreviewDiv").addClass("active");
		jQuery("#bidDateDiv").removeClass("active");
		jQuery("#fancybox-content").height(430);
	});
	
	jQuery("#bid_container_button").live("click",function(){
		var bValidate = true;
		jQuery('#bidDateDiv input[type="text"]').each(function(){			
			var text_value = jQuery(this).val();
			if (text_value == ""){
				alert("Please input the date.");
				bValidate = false;
				jQuery(this).focus();
				return false;
			}
		});
		
		if (bValidate == false){
			return;
		}
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/bid_container_by_shipper_ajax";
		var postDataPrice = jQuery("form#frmBidPrice").serialize();
		var postDataDate = jQuery("form#frmBidDate").serialize();
		var postData = postDataPrice + "&" + postDataDate;

		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert(result.message);
					return false;
				}
				
				jQuery.fancybox.close();
				jQuery.fn.yiiGridView.update('supplier-order-container-grid');
				jQuery.fn.yiiGridView.update('supplier-order-container-bidding-grid');
				jQuery("#editPriceButton").remove();
			}
		});
	});
});
</script>