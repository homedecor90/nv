<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);*/

$this->menu=array(
	array('label'=>'Create Supplier Order', 'url'=>array('create')),
	array('label'=>'View Supplier Order', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
);
?>

<h1>Update Supplier Order <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>