<?php
$this->menu=array(
	array('label'=>'Manage Supplier Orders', 'url'=>array('index')),
);
?>

<h1>My Orders</h1>

<input type="button" value="Set E.T.L" href="#setEtlDiv" id="setEtaButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#setEtaButton',
    'config'=>array(
		'width'=>200,
		'height'=>120,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="setEtlDiv" class="fancy_box_div">
<?php
	Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
	$this->widget('CJuiDateTimePicker',array(
		'model'=>$model, //Model object
		'attribute'=>'etl', //attribute name
		'mode'=>'date', //use "time","date" or "datetime" (default)
		'language'=>'en-GB',
		'options'=>array(
			"dateFormat"=>"M. dd, yy"
		) // jquery plugin options
	));
?>
<input type="button" value="Set E.T.L" id="set_etl_fancy"/>
</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'self-supplier-order-grid',
	'dataProvider'=>$model->search_archived(Yii::app()->user->id),	
	'columns'=>array(
		array(
			'header'=>'Created Date',
			'value'=>'date("M. d, Y", strtotime($data->created_date))',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderItems($data->id)',
		),
		array(
			'header'=>'Total Price',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderTotalPrice($data->id)',
		),
		array(
			'header'=>'Total CBM',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderTotalCBM($data->id)',
		),
		array(
			'header'=>'E.T.L',
			'type'=>'raw',
            'value'=>'($data->etl==NULL)?"":date("M. d, Y", strtotime($data->etl))',
		),
		array(
			'header'=>'Status',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderStatus($data->id)',
		),
		array(
			'header'=>'Action',
			'type'=>'raw',
			'value'=>'SupplierOrder::model()->getActionButtonString($data->id)',
		),
	),
)); ?>

<input type="button" value="Upload Order Attach" href="#uploadOfficialDocsDiv" id="uploadOfficialDocsButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#uploadOfficialDocsButton',
    'config'=>array(
		'width'=>400,
		'height'=>250,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
<div id="uploadOfficialDocsDiv" class="fancy_box_div">
<br/><br/>
<?php
// Ajax Upload View Part
$this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
	'id'=>'uploadOrderAttach',
	'config'=>array(
		'action'=>Yii::app()->createUrl('supplierorder/upload'),
		'allowedExtensions'=>array("*"),//array("jpg","jpeg","gif","exe","mov" and etc...
		'sizeLimit'=>10*1024*1024,// maximum file size in bytes
		'multiple'=>true,
		'minSizeLimit'=>0,// minimum file size in bytes
		'onComplete'=>"js:function(id, file_name, responseJSON){ onCompleteUploadOfficialDocs(file_name); }",
		'onProgress'=>"js:function(id, file_name, loaded, total){ onProgessOfficialDocs(file_name, loaded, total); }"
	)
)); 
?>
<div id="progress"></div>
</div>
</div>

<script>
var current_order_id = 0;
var arr_upload_progress = new Array();

jQuery(document).ready(function(){
	jQuery(".order_detail").live("click", function(){
		var order_id = jQuery(this).attr("rel");		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/order_detail/?order_id=" + order_id;
		window.location.href = url;
	});
	
	jQuery(".accept_order").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_order_by_supplier_ajax/?order_id=" + order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",		
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".deny_order").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_order_by_supplier_ajax/?order_id=" + order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",		
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".set_eta").live("click",function(){
		var order_id = jQuery(this).attr("rel");
		current_order_id = order_id;
		jQuery("#setEtaButton").trigger("click");
	});
	
	jQuery(".complete").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/complete_order_by_supplier_ajax/?order_id=" + order_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery("#set_etl_fancy").live("click", function(){
		var order_id = current_order_id;
		var etl_date = jQuery("#SupplierOrder_etl").val();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/set_etl_by_supplier_ajax/?"
			+ "order_id=" + order_id + "&etl_date=" + etl_date;		
		jQuery.fancybox.close();

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".item_accept").live("click", function(){
		var item_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/accept_item_by_supplier_ajax/?item_id=" + item_id;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".item_deny").live("click", function(){
		var item_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/deny_item_by_supplier_ajax/?item_id=" + item_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('self-supplier-order-grid');
			}
		});
	});
	
	jQuery(".upload_documents").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		current_order_id = order_id;
		
		jQuery("#uploadOfficialDocsButton").trigger("click");
	});
});

function onCompleteUploadOfficialDocs(file_name){
	var order_id = current_order_id;
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/upload_official_docs_by_supplier_ajax/?"
			+ "order_id=" + order_id + "&file_name=" + file_name;

	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			jQuery.fn.yiiGridView.update('supplier-order-grid');
		}
	});
}

function onProgessOfficialDocs(file_name, loaded, total){
	var strFileProgress = "<div>" + file_name + " : " + loaded + " of "+ total + " Bytes uploaded</div>";
	arr_upload_progress[file_name] = strFileProgress;
	
	var strProgress = "";
	for (key in arr_upload_progress){
		strProgress += arr_upload_progress[key];
	}
	
	jQuery("#progress").html(strProgress);
}
</script>