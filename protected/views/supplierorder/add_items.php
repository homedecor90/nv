<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	'Create',
);*/

$this->menu=array(
	array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
);
?>

<h1>Edit Supplier Order</h1>
<?php if (strtotime($supplierOrderModel->etl) > 0): ?>
	<strong>ETL: </strong>
	<span id="old_etl"><?php echo date('M d, Y', strtotime($supplierOrderModel->etl))?></span>
	<?php if (Yii::app()->user->isAdmin()): ?>
		<span href="#setEtlDiv" id="setEtaButton" class="clickable">edit</span>
	<?php endif;?>
	
	<?php
	$this->widget('application.extensions.fancybox.EFancyBox', array(
	    'target'=>'#setEtaButton',
	    'config'=>array(
			'width'=>250,
			'height'=>120,
			'autoDimensions'=>false,
		),
	));
	?>
	
	<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="setEtlDiv" class="fancy_box_div">
	<?php
		Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
		$this->widget('CJuiDateTimePicker',array(
			//'model'=>$supplierOrderModel, //Model object
			//'attribute'=>'etl', //attribute name
			'value'=>date('M d, Y', strtotime($supplierOrderModel->etl)),
			'name'=>'new_etl',
			'mode'=>'date', //use "time","date" or "datetime" (default)
			'language'=>'en-GB',
			'options'=>array(
				"dateFormat"=>"M. dd, yy"
			) // jquery plugin options
		));
	?>
	<input type="button" value="Set E.T.L" id="set_etl_fancy"/>
	</div>
	</div>

<?php endif;?>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#add_new_item',
    'config'=>array(
		'width'=>500,
		'height'=>500,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="add_item_box" class="fancy_box_div">
<?php $this->renderPartial('add_new_item',array(
	"newItemModel"=>$newItemModel,
)); ?>
</div>
</div>

<?php 
// we need to show edit/delete controls in the table if order is pending
// so we use two different grid widgets
if ($supplierOrderModel->status != SupplierOrder::$STATUS_FULL_PAID && $supplierOrderModel->status != SupplierOrder::$STATUS_ARCHIVED):

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-grid',
	'dataProvider'=>$supplierOrderItemsModel->searchNotDenied($supplierOrderModel->id),
	'summaryText'=>'',
	'columns'=>array(
		array(
			'header'=>'Item Name',
			'value'=>'$data->items->item_name',
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
		),
		'exw_cost_price',
		'fob_cost_price',
        'custom_color',
		array(
			'header'=>'Quantity',
            'type'=>'raw',
            'value'=>'\'<span id="qty_\'.$data->id.\'">\'.$data->quantity.\'</span><input type="text" id="new_qty_\'.$data->id.\'" class="new_qty_input" value="\'.$data->quantity.\'" />\'
                .($data->quantity_updated ? \'&nbsp;(\'.$data->new_quantity.\')\' : \'\')',
        ),
        'added_date',
		'status',
        array(
			'header'=>'Edit',
            'type'=>'raw',
			'value'=>'\'<a href="#" id="edit_\'.$data->id.\'" class="edit_old_item" data-itemid="\'.$data->id.\'">Edit</a>
                        <a href="#" id="edit_save_\'.$data->id.\'" class="edit_save_old_item" data-itemid="\'.$data->id.\'">Save</a>
                        <a href="#" id="edit_cancel_\'.$data->id.\'" class="edit_cancel_old_item" data-itemid="\'.$data->id.\'">Cancel</a>
                        \'',
		),
        array(
			'header'=>'Delete',
            'type'=>'raw',
			'value'=>'\'<a href="#" id="delete_\'.$data->id.\'" class="delete_old_item" data-itemid="\'.$data->id.\'">Delete</a>\'',
		),
	),
));

else:

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-grid',
	'dataProvider'=>$supplierOrderItemsModel->search($supplierOrderModel->id),
	'summaryText'=>'',
	'columns'=>array(
		array(
			'header'=>'Item Name',
			'value'=>'$data->items->item_name',
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
		),
		'exw_cost_price',
		'fob_cost_price',
		'quantity',
		'added_date',
	),
));

endif;
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-order-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="order">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($supplierOrderModel); ?>	

	<?php
		echo $form->hiddenField($supplierOrderModel, 'supplier_id', array('value'=>$supplierOrderModel->supplier_id));		
        
    ?>
    
	<div class="row total_price">
		<div class="total_fob_price">
			Total FOB Price:<span>0</span>
		</div>
		<div class="total_exw_price">
			Total EXW Price:<span>0</span>
		</div>
	</div>
	
	<table id="order_item_list">
		<thead>
			<th class="item_code">Item Code</td>
			<th class="color">Color</td>
			<th class="quantity">Quantity</td>
			<th class="total_exw_price">Total EXW Price</td>
			<th class="total_fob_price">Total FOB Price</td>
			<th class="custom_color">Custom Color</td>
			<th class="action">Action</td>
		</thead>		
	</table>

	<div class="row">
        <?php echo $form->labelEx($supplierOrderModel,'discount_amount'); ?>
        <?php echo $form->textField($supplierOrderModel,'discount_amount'); ?>
        <?php echo $form->error($supplierOrderModel,'discount_amount'); ?>
    </div>
	<div class="row buttons">
		<?php echo CHtml::Button('Add Items', array('id'=>'add_existing_item')); ?>
		<?php echo CHtml::Button('Add New Items', array('id'=>'add_new_item', "href"=>"#add_item_box")); ?>
		<?php echo CHtml::submitButton('Update Supplier Order', array('id'=>'calculate_total')); ?>		
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
var item_count = 0;
var item_number = 0;
var strCodeList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_code', '', $itemCodeList['itemCodeList']));?>';
var strColorList = '<?php echo str_replace("\n", "", CHtml::dropDownList('item_color', '', $itemCodeList['itemColorList']));?>';
var strQuantity = '<?php echo str_replace("\n", "", CHtml::textField('quantity', '1'));?>';
var strCustomColor = '<?php echo str_replace("\n", "", CHtml::textField('custom_color', ''));?>';

jQuery(document).ready(function(){		

	jQuery("#set_etl_fancy").live("click", function(){
		var order_id = <?php echo $supplierOrderModel->id; ?>;
		var etl_date = jQuery("#new_etl").val();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/set_etl_by_admin_ajax/?"
			+ "order_id=" + order_id + "&etl_date=" + etl_date;		
		jQuery.fancybox.close();

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if(result.message){
					alert(result.message);
				}
				
				if (result.status == "success"){
					jQuery('#old_etl').text(etl_date);
				}
			}
		});
	});
	
	/**************************************************
	/* Event when click the create new order
	/**************************************************/
	jQuery("#calculate_total").click(function(){
		// if (item_count == 0){
			// alert("Please input the item");
			// return false;
		// }		
		var bNumber = true;
		
        // Check if items have prices before adding them to the order
        var failed = false;
        jQuery(".item_row").each(function(){
            var row_id = jQuery(this).attr("id");
            var exw_cost_price = parseInt(jQuery("#" + row_id + " .action input.exw_cost_price").val());
            var fob_cost_price = parseInt(jQuery("#" + row_id + " .action input.fob_cost_price").val());
            var quantity = jQuery("#" + row_id + " .quantity input").val();
            
            if (isNaN(exw_cost_price) || fob_cost_price == 0 || exw_cost_price == 0){
                failed = true;
            }
            
        });
        
        if (failed){
            alert('Some items have no price specified. Please specify valid FOB/EXW price and CBM before adding them to the order');
            return false;
        }
        
		jQuery(".quantity input").each(function(){
			var quantity = jQuery(this).val();
			if (isNaN(quantity)){
				alert("Pleas input the number as quantity");
				jQuery(this).focus();
				bNumber = false;
				return false;
			}
		});
		
		if (bNumber == false){
			return false;
		}
	});
	
	/**************************************************
	/* Event when click the add new item
	/**************************************************/
	jQuery("#add_existing_item").click(function(){
		item_number++;		
		var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'
				+ '<td class="item_code">' + strCodeList + '</td>'
				+ '<td class="color">' + strColorList + '</td>'
				+ '<td class="quantity">' + strQuantity + '</td>'
				+ '<td class="total_exw_price"></td>'
				+ '<td class="total_fob_price"></td>'
				+ '<td class="custom_color">' + strCustomColor + '</td>'
				+ '<td class="action"><a href="#" class="delete_item">Delete</a></td>'
				+ '</tr>';

		jQuery("#order_item_list").append(newRowHtml);
		jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");
		jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]");
		jQuery("#item_" + item_number + " .custom_color input").attr("name", "item[custom_color][]");
		item_count++;
		changeOrderItemList();
	});
	
	
	/**************************************************
	/* Event when change the item code
	/**************************************************/
	jQuery(".item_code select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		changeItemCode(item_cur_number);
	});
	
	/**************************************************
	/* Event when change the color
	/**************************************************/
	jQuery(".color select").live("change",function(){
		var item_cur_number = jQuery(this).parent().parent().attr("rel");
		var item_id = jQuery(this).val();
		changeItemColor(item_cur_number, item_id);
	});
	
	/**************************************************
	/* Event when change the quantity
	/**************************************************/
	jQuery(".quantity input").live("change",function(){
		calculateTotal();
	});
	
	/**************************************************
	/* Event when click the delete button
	/**************************************************/
	jQuery(".delete_item").live("click",function(){
		jQuery(this).parent().parent().remove();
		item_count--;
		changeOrderItemList()
		return false;
	});
	
	/**************************************************
	/* Event when click the Submit button on lightbox
	/**************************************************/
	jQuery("#fancybox-content #add_new_item_submit").live("click", function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/add_new_item_on_supplier_order_ajax";
		var postData = jQuery("#fancybox-content form#item-form").serialize();
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert("Please input the exact values");
					return;
				}
				
				var item_code = result.data.item_code;
				var quantity = result.data.quantity;
				
				var str_new_option = '<option value="' + item_code +'">' + item_code + '</option>';
				if (strCodeList.indexOf(str_new_option) == -1){
					strCodeList = strCodeList.replace('</select>', str_new_option + '</select>');
					jQuery(".item_code select").append(str_new_option);
				}
				
				item_number++;		
				var newRowHtml = '<tr rel="' + item_number + '" id="item_' + item_number + '" class="item_row">'
						+ '<td class="item_code">' + strCodeList + '</td>'
						+ '<td class="color">' + strColorList + '</td>'
						+ '<td class="quantity">' + strQuantity + '</td>'
						+ '<td class="total_exw_price"></td>'
						+ '<td class="total_fob_price"></td>'
						+ '<td class="action"></td>'
						+ '</tr>';

				jQuery("#order_item_list").append(newRowHtml);
				jQuery("#item_" + item_number + " .item_code select").val(item_code);
				jQuery("#item_" + item_number + " .color select").attr("name", "item[item_id][]");
				jQuery("#item_" + item_number + " .quantity input").attr("name", "item[quantity][]").val(quantity);
				item_count++;
				changeOrderItemList();
				
				jQuery.fancybox.close();
			}
		});
	});
	
	changeOrderItemList();
});

/**************************************************
/* Add the inputed data into the 
/**************************************************/
function calculateTotal(){
	var exw_price = 0;
	var fob_price = 0;
	
	jQuery(".item_row").each(function(){
		var row_id = jQuery(this).attr("id");
		var exw_cost_price = jQuery("#" + row_id + " .action input.exw_cost_price").val();
		var fob_cost_price = jQuery("#" + row_id + " .action input.fob_cost_price").val();
		var quantity = jQuery("#" + row_id + " .quantity input").val();
		
		if (isNaN(quantity) || isNaN(exw_cost_price)){
			return 0;
		}
		
		var item_total_exw_price = (parseFloat(exw_cost_price)) * parseInt(quantity);
		var item_total_fob_price = (parseFloat(fob_cost_price)) * parseInt(quantity);
		item_total_exw_price = parseFloat(item_total_exw_price.toFixed(2));
		item_total_fob_price = parseFloat(item_total_fob_price.toFixed(2));
		
		jQuery("#" + row_id + " .total_exw_price").html(item_total_exw_price);
		jQuery("#" + row_id + " .total_fob_price").html(item_total_fob_price);
		exw_price += item_total_exw_price;
		fob_price += item_total_fob_price;
	});
	
	jQuery(".total_exw_price span").html(exw_price);
	jQuery(".total_fob_price span").html(fob_price);
}

/**************************************************
/* Reflect the item change status to the grid view
/**************************************************/
function changeOrderItemList(){
	if (item_count == 0){
		jQuery("#order_item_list").append('<tr class="order_item_empty"><td colspan=6>No items are added</td></tr>');
	} else {
		jQuery(".order_item_empty").remove();
		changeItemCode(item_number);
	}
}

/**************************************************
/* Get the color when change the item code
/**************************************************/
function changeItemCode(item_cur_number){
	var item_code = jQuery("#item_" + item_cur_number + " #item_code").val();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/itemcolorlist/?item_code=" + encodeURIComponent(item_code);
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;

			var itemColorList = result.itemColorList;
			var selectText = "";
			var bChangeColor = false;
			
			for (var item_id in itemColorList){
				if (!bChangeColor){
					changeItemColor(item_cur_number, item_id);
					bChangeColor = true;
				}					
				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';
			}
			jQuery("#item_" + item_cur_number + " .color select").html(selectText);
		}
	});
}

/**************************************************
/* Get the item info when change the color
/**************************************************/
function changeItemColor(item_cur_number, item_id){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/iteminfo/?id=" + item_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;
			
			var itemInfo = result.itemInfo;			
			var str_action_field = "<input type='hidden' class='exw_cost_price' value='" + itemInfo.exw_cost_price + "'/>"
						+ "<input type='hidden' class='fob_cost_price' value='" + itemInfo.fob_cost_price + "'/>"
						+ '<a href="#" class="delete_item">Delete</a>';
			
			jQuery("#item_" + item_cur_number + " .action").html(str_action_field);
			calculateTotal();
		}
	});
}

/**************************************************/
/* Delete old items from the order
/**************************************************/

// remember not to conflict with existing variables

jQuery('.delete_old_item').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
    var old_item_id = jQuery(e.target).attr('data-itemid');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/delete_existing_items_ajax/?items_id=" + old_item_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                // just delete table row
                // jQuery('#delete_'+old_item_id).parent().parent().remove();
                jQuery.fn.yiiGridView.update('supplier-order-grid');
            }
		}
	});
    e.preventDefault();
    e.stopImmediatePropagation();
});

/**************************************************/
/* Edit old items in the order
/**************************************************/

jQuery('.edit_old_item').live('click', function(e){
    // show inout boxes instead of numbers in the table
    var old_item_id = jQuery(e.target).attr('data-itemid');
    // var row = jQuery(e.target).parent().parent();
    jQuery('#qty_'+old_item_id).hide();
    jQuery('#new_qty_'+old_item_id).show();
    jQuery(e.target).hide();
    jQuery('#edit_save_'+old_item_id).show();
    jQuery('#edit_cancel_'+old_item_id).show();
    
    e.preventDefault();
    e.stopImmediatePropagation();

});

jQuery('.edit_cancel_old_item').live('click', function(e){
    // show inout boxes instead of numbers in the table
    var old_item_id = jQuery(e.target).attr('data-itemid');
    // var row = jQuery(e.target).parent().parent();
    jQuery('#qty_'+old_item_id).show();
    jQuery('#new_qty_'+old_item_id).hide();
    jQuery(e.target).hide();
    jQuery('#edit_save_'+old_item_id).hide();
    jQuery('#edit_'+old_item_id).show();
    
    e.preventDefault();
    e.stopImmediatePropagation();

});

jQuery('.edit_save_old_item').live('click', function(e){
    // show inout boxes instead of numbers in the table
    var old_item_id = jQuery(e.target).attr('data-itemid');
    
    var old_value = jQuery('#qty_'+old_item_id).text();
    var new_value = jQuery('#new_qty_'+old_item_id).val();
    
    // var row = jQuery(e.target).parent().parent();
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/update_existing_items_ajax/?items_id=" + old_item_id +'&old_value='+old_value+'&new_value='+new_value;	
    jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                jQuery.fn.yiiGridView.update('supplier-order-grid');
                // jQuery('#qty_'+old_item_id).text(new_value);
                // jQuery('#edit_cancel_'+old_item_id).trigger('click');
                // just delete table row
                // jQuery('#delete_'+old_item_id).parent().parent().remove();
            }
		}
	});
    
    e.preventDefault();
    e.stopImmediatePropagation();

});

</script>