<?php
echo '<table class="items">
	<thead>
	<th class="items">Items</th>
	<th class="total_exw_price">Total EXW Price</th>
	<th class="total_fob_price">Total Fob Price</th>
	<th class="total_cbm">Total CBM</th>
	<th class="action">Action</th>
</thead>';
	
$i = 0;
foreach ($orders as $order){
	$order_id = $order->id;
	$strItems = SupplierOrder::getSupplierOrderItems($order_id);
	$total_exw_price = SupplierOrder::getSupplierOrderTotalExwPrice($order_id);
	$total_fob_price = SupplierOrder::getSupplierOrderTotalFobPrice($order_id);
	$total_cbm= SupplierOrder::getSupplierOrderTotalCBM($order_id);
	$strAction = '<button class="choose_order" rel="'.$order_id.'">Choose</a>';
		
	echo '<tr>'
	.'<td class="items">'
	.'<strong>Status: '.$order->status.'</strong>'
	.$strItems
	.'</td>'
	.'<td class="total_exw_price">'.$total_exw_price.'</td>'
	.'<td class="total_fob_price">'.$total_fob_price.'</td>'
	.'<td class="total_cbm">'.$total_cbm.'</td>'
	.'<td class="action">'.$strAction.'</td>'
	.'</tr>';
	$i++;
}

if ($i == 0){
	echo '<tr>'
	.'<td colspan="5" style="font-style:italic; text-align:center">No Result</td>'
	.'</tr>';
}

echo '</table>';