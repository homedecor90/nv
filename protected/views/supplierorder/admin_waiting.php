<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Manage Supplier Orders', 'url'=>array('admin')),
	array('label'=>'Create Supplier Order', 'url'=>array('create')),
	array('label'=>'View Supplier Order Container', 'url'=>array('view_container')),
	array('label'=>'View Supplier Order Payment Track', 'url'=>array('view_supplier_order_track')),
	array('label'=>'Archived Supplier Orders', 'url'=>array('admin_archived')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('supplier-order-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<input type="button" value="Upload Order Attach" href="#uploadOrderAttachDiv" id="uploadOrderAttchButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#uploadOrderAttchButton',
    'config'=>array(
		'width'=>400,
		'height'=>130,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="uploadOrderAttachDiv" class="fancy_box_div">
<?php
// Ajax Upload View Part
$this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
	'id'=>'uploadOrderAttach',
	'config'=>array(
		'action'=>Yii::app()->createUrl('supplierorder/upload'),
		'allowedExtensions'=>array("*"),//array("jpg","jpeg","gif","exe","mov" and etc...
		'sizeLimit'=>10*1024*1024,// maximum file size in bytes
		'minSizeLimit'=>0,// minimum file size in bytes
		'onComplete'=>"js:function(id, file_name, responseJSON){ onCompleteUploadAttach(file_name); }",
		'onProgress'=>"js:function(id, file_name, loaded, total){ onProgessUploadAttach(file_name, loaded, total); }"
	)
)); 
?>
<input type="button" value="No Upload Attach File" id="no_upload_attch_file"/>
<div id="progress"></div>
</div>
</div>

<input type="button" value="Create Container" href="#createCotainerDiv" id="createContainerButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#createContainerButton',
    'config'=>array(
		'width'=>320,
		'height'=>140,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="createCotainerDiv" class="fancy_box_div">
		<form id="frmCrateContainer">
			<input type="hidden" name="order_id_list" id="order_id_list" value=""/>
			<div>
				<label>Destination Port</label>
				<input type="text" name="destination_port" id="destination_port" readonly="true" value="L.A."/>
			</div>
			<div>
				<label>Original Port</label>
				<select type="text" name="original_port" id="original_port"></select>
			</div>
			<div>
				<input type="button" value="Create Container" id="create_container"/>
			</div>
		</form>
	</div>
</div>

<h1>Manage Supplier Orders</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
$columns = array(
	array(
		'header'=>'Supplier',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getSupplierName($data->id)',
	),
	array(
		'header'=>'Items',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getSupplierOrderItems($data->id)',
	),
	array(
		'header'=>'Total EXW Price',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getSupplierOrderTotalExwPrice($data->id)',
	),
	array(
		'header'=>'Total Fob Price',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getSupplierOrderTotalFobPrice($data->id)',
	),
	array(
		'header'=>'Total CBM',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getSupplierOrderTotalCBM($data->id)',
	),
	/*array(
		'header'=>'E.T.L',				
		'value'=>'($data->etl==NULL)?"":date("M. d, Y", strtotime($data->etl))',
	),
	array(
		'header'=>'Status',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getSupplierOrderStatus($data->id)',
	),
	array(
		'header'=>'Container',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getContainerDetailLink($data->id)',
	),
	array(
		'header'=>'Action',
		'type'=>'raw',
		'value'=>'SupplierOrder::model()->getActionButtonString($data->id)',
	),*/
);

$columns_checkbox = array(array(
	'visible'=>'Yii::app()->user->isAdmin()',
	'class'=>'CMyCheckBoxColumn',
	'selectableRows' => 2,
));

if (Yii::app()->user->isAdmin()){
	$columns = array_merge($columns_checkbox, $columns);
}

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-grid',
	'dataProvider'=>$model->search_waiting(),	
	'columns'=>$columns
	)
);

if (Yii::app()->user->isAdmin()){
?>
	<input type="button" id="approve_orders" value="Approve Orders"/>
<?php
}
?>

<script>
jQuery(document).ready(function(){
	jQuery("#approve_orders").click(function(){
		if (jQuery("td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the order to approve");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("td.checkbox-column input[type='checkbox']:checked").each(function(){
			var order_id = jQuery(this).val();
			if (id_list == ""){
				id_list = order_id;
			} else {
				id_list += ("_" + order_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/approve_orders/?order_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			success:function(result){
				jQuery.fn.yiiGridView.update('supplier-order-grid');
			}
		});
	});
});
</script>