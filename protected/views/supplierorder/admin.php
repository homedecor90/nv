<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	'Manage',
);*/
// print_r(SupplierOrder::getPendingSupplierOrderItemsSupplierIdList());
$this->menu=array(
	array('label'=>'Waiting Supplier Orders', 'url'=>array('admin_waiting')),
	array('label'=>'Create Supplier Order', 'url'=>array('create')),
	array('label'=>'View Supplier Order Container', 'url'=>array('view_container')),
	array('label'=>'View Supplier Order Payment Track', 'url'=>array('view_supplier_order_track')),
	array('label'=>'Archived Supplier Orders', 'url'=>array('admin_archived')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('supplier-order-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<input type="button" value="Upload Order Attach" href="#uploadOrderAttachDiv" id="uploadOrderAttchButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#uploadOrderAttchButton',
    'config'=>array(
		'width'=>400,
		'height'=>130,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="uploadOrderAttachDiv" class="fancy_box_div">
<?php
// Ajax Upload View Part
$this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
	'id'=>'uploadOrderAttach',
	'config'=>array(
		'action'=>Yii::app()->createUrl('supplierorder/upload'),
		'allowedExtensions'=>array("pdf","jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
		'sizeLimit'=>10*1024*1024,// maximum file size in bytes
		'minSizeLimit'=>0,// minimum file size in bytes
        'multiple'=>true,
		'onComplete'=>"js:function(id, file_name, responseJSON){ onCompleteUploadAttach(file_name); }",
		'onProgress'=>"js:function(id, file_name, loaded, total){ onProgessUploadAttach(file_name, loaded, total); }"
	)
)); 
?>
<input type="button" value="No Upload Attach File" id="no_upload_attch_file"/>
<div id="progress"></div>
</div>
</div>

<input type="button" value="Create Container" href="#createCotainerDiv" id="createContainerButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#createContainerButton',
    'config'=>array(
		'width'=>320,
		'height'=>140,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="createCotainerDiv" class="fancy_box_div">
		<form id="frmCrateContainer">
			<input type="hidden" name="order_id_list" id="order_id_list" value=""/>
			<div>
				<label>Destination Port</label>
				<input type="text" name="destination_port" id="destination_port" readonly="true" value="L.A."/>
			</div>
			<div>
				<label>Original Port</label>
				<select type="text" name="original_port" id="original_port"></select>
			</div>
			<div>
				<input type="button" value="Create Container" id="create_container"/>
			</div>
		</form>
	</div>
</div>

<input type="button" value="Change Order Status" href="#orderStatusDiv" id="orderStatusButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#orderStatusButton',
    'config'=>array(
		'width'=>320,
		'height'=>170,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="orderStatusDiv" class="fancy_box_div">
		<form id="frmOrderStatus">
			<input type="hidden" name="order_id" id="status_order_id" value=""/>
			<input type="hidden" name="order_id" id="old_status" value=""/>
			<div>
				<label>New Status</label>
				<select name="status" id="new_status">
                    <option value="<?php echo SupplierOrder::$STATUS_ADVANCED_PAID; ?>"><?php echo SupplierOrder::$STATUS_ADVANCED_PAID; ?></option>
                    <option value="<?php echo SupplierOrder::$STATUS_MAKING; ?>"><?php echo SupplierOrder::$STATUS_MAKING; ?></option>
                    <option value="<?php echo SupplierOrder::$STATUS_COMPLETED; ?>"><?php echo SupplierOrder::$STATUS_COMPLETED; ?></option>
                </select>
			</div>
            <div id="etl_block" style="display: none;">
                <label>E.T.L.</label>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        // 'model'=>$model, //Model object
                        // 'attribute'=>'etl', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'name'=> 'new_etl',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options
                        'htmlOptions' => array(
                            'id' => 'new_etl'
                        )
                    ));
                ?>
            </div>
			<div>
				<input type="button" value="Change Status" id="change_status_submit"/>
			</div>
		</form>
	</div>
</div>

<h1>Manage Supplier Orders</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="view">
    20'ft container 26-28 CBM<br />
    40'ft container 56-58 CBM<br />
    40'ft HQ container 60-68 CBM<br />
    45'ft HQ container about 78 CBM<br />
</div>

<?php $this->widget('CGridViewExt', array(
	'id'=>'supplier-order-grid',
	'dataProvider'=>$model->search(),
    'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'visible'=>'SupplierOrder::model()->isCombinable($data->id)',
			'class'=>'CMyCheckBoxColumn',
			'selectableRows' => 2,
		),
        'id',
		array(
			'header'=>'Supplier',
			'type'=>'raw',
            //'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/user/user/view/id/\'.$data->supplier_id.\'">\'.SupplierOrder::model()->getSupplierName($data->id).\'</a>\'',
				'value'=>'\'<span class="clickable supplier_name" rel="\'.$data->id.\'">\'.SupplierOrder::model()->getSupplierName($data->id).\'</span>\'
				.\'<div style="display: none;" id="supplier_info_\'.$data->id.\'">\'.$data->getSupplierInfoHtml().\'</div>\'',
		),
		/*
        array(
			'header'=>'Items',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderItems($data->id, true)',
		),*/
        array(
            'header'=>'Totals',
			'type'=>'raw',
            'value'=>'\'<strong>FOB:</strong><br />$\'.number_format(SupplierOrder::model()->getSupplierOrderTotalFobPrice($data->id),2)
                        .\'<br /><br /><strong>EXW:</strong><br />$\'.number_format(SupplierOrder::model()->getSupplierOrderTotalExwPrice($data->id),2)',
        ),
        array(
            'header'=>'Discount',
            'type'=>'raw',
            'value'=>'$data->discount_amount',
        ),
        array(
            'header'=>'Current 30% Amount (EXW)',
            'type'=>'raw',
            'value'=>'$data->getCurrentAdvancePaymentAmount()',
        ),
        array(
            'header'=>'Balance',
            'type'=>'raw',
            'value'=>'$data->getSupplierOrderBalanceString()',
        ),
		array(
			'header'=>'Total CBM',
			'type'=>'raw',
            // 'value'=>'SupplierOrder::model()->getSupplierOrderTotalCBM($data->id)',
            'value'=>'\'<span href="#order_contents_\'.$data->id.\'" id="open_order_contents_\'.$data->id.\'" class="total_cbm">\'.SupplierOrder::model()->getSupplierOrderTotalCBM($data->id).\'</span>\'.SupplierOrder::getItemsPopupCodeAdmin($data->id)',
		),
		array(
			'header'=>'E.T.L',
			'value'=>'($data->etl==NULL)?"":date("M. d, Y", strtotime($data->etl))',
		),
		array(
			'header'=>'Status',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderStatus($data->id)',
		),
		array(
			'header'=>'Container',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getContainerDetailLink($data->id)',
		),
		array(
			'header'=>'Action',
			'type'=>'raw',
			'value'=>'SupplierOrder::model()->getActionButtonString($data->id)',
		),
	),
	
)); ?>

<input type="button" id="create_container_for_orders" value="Create Container for Selected Orders"/>

<input type="button" href="#supplier_info_block" id="supplier_info_button" style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'#supplier_info_button',
	'config'=>array(
		'width'=>350,
		'height'=>250,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="supplier_info_block" class="fancy_box_div fancy_form">
		<h2>Supplier Info</h2>
		<div>
			
		</div>
	</div>
</div>


<input type="button" value="Supplier Order List" href="#order_list_container" id="order_list_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_list_button',
    'config'=>array(
		'width'=>1000,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="order_list_container" class="fancy_box_div">
<div id="order_list_content"></div>
</div>
</div>


<script>
var current_order_id = 0;
var advanced_paid_fancy = true;
var combined_order = 0;

jQuery(document).ready(function(){
	jQuery(".order_detail").live("click", function(){
		var order_id = jQuery(this).attr("rel");		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/order_detail/?order_id=" + order_id;
		window.location.href = url;
	});

	jQuery('.order_combine').live('click', function(e){
		// get supplier order list
		var target = jQuery(e.target);
		combined_order = target.attr('rel');
		var supplier_id = target.attr('data-supplier');
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/get_orders_for_supplier_ajax/?supplier_id=" + supplier_id + '&no_container=1&&exclude[]=' + combined_order;
		jQuery.ajax({
			url:url,
			type: 'post',
			dataType:"html",
			success:function(result){
				jQuery('#order_list_content').html(result);
				jQuery('#order_list_button').trigger('click');
			}
		});
		return true;
	});
	
	jQuery(".add_items").live("click", function(){
		var order_id = jQuery(this).attr("rel");		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/add_items/?order_id=" + order_id;
		window.location.href = url;
	});
	
	jQuery(".pay_advanced").live("click", function(){
		var order_id = jQuery(this).attr("rel");
		advanced_paid_fancy = true;
		
		current_order_id = order_id;
		jQuery(".upload_button").html("Upload for Advanced Payment");
		jQuery("#uploadOrderAttchButton").trigger("click");
	});
	
	jQuery("#no_upload_attch_file").live("click",function(){
		jQuery.fancybox.close();
		
		if (advanced_paid_fancy == true){
			updateSupplyOrderAdvancedPaid("");
		} else {
			updateSupplyOrderFullPaid("");
		}
	});
	
	jQuery(".pay_remaining").live("click", function(){		
		var order_id = jQuery(this).attr("rel");
		advanced_paid_fancy = false;
		
		current_order_id = order_id;
		jQuery(".upload_button").html("Upload for Final Payment");
		jQuery("#uploadOrderAttchButton").trigger("click");
		
	});
	
	jQuery("#create_container_for_orders").click(function(){
		if (jQuery("td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the order to combine");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("td.checkbox-column input[type='checkbox']:checked").each(function(){
			var order_id = jQuery(this).val();
			if (id_list == ""){
				id_list = order_id;
			} else {
				id_list += ("_" + order_id);
			}
			
			var oringal_port = jQuery("#supplier_original_port_" + order_id).val();			
			if (original_port_list.indexOf(oringal_port) == -1){
				original_port_list.push(oringal_port);
				str_original_port_list += '<option value="' + oringal_port + '">' + oringal_port + '</option>';
			}
		});
		
		jQuery("#createContainerButton").trigger("click");
		jQuery("#original_port").html(str_original_port_list);
		jQuery("#order_id_list").val(id_list);
	});
	
	jQuery('.choose_order').live('click', function(e){
		var recipient_id = jQuery(e.target).attr('rel');

		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/combine_supplier_orders_ajax/?order_id=" + combined_order + '&recipient_order_id=' + recipient_id;
		jQuery.ajax({
			url:url,
			type: 'post',
			dataType:"json",
			success:function(result){
				if (result.status == 'fail'){
					alert(result.status);
					return false;
				}

				jQuery.fn.yiiGridView.update('supplier-order-grid');
			}
		});
		e.preventDefault();
	    e.stopImmediatePropagation();
		return false;
	});
	
	jQuery("#create_container").live("click", function(){
		jQuery.fancybox.close();
		var postData = jQuery("form#frmCrateContainer").serialize();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/create_container_for_orders_by_admin_ajax";

		jQuery.ajax({
			url:url,
			dataType:"json",
			data: postData,
			type: "POST",
			success:function(result){
				if (result.message != "success"){
					alert(result.error);
					return false;
				}
				
				jQuery.fn.yiiGridView.update('supplier-order-grid');
			}
		});
	});
});

function onCompleteUploadAttach(file_name){
	jQuery.fancybox.close();
	jQuery("#progress").html("");
	
	if (advanced_paid_fancy == true){
		updateSupplyOrderAdvancedPaid(file_name);
	} else {
		updateSupplyOrderFullPaid(file_name);
	}
}

function onProgessUploadAttach(file_name, loaded, total){
	var strProgress = file_name + " : " + loaded + " of "+ total + " Bytes uploaded";
	jQuery("#progress").html(strProgress);
}

function updateSupplyOrderAdvancedPaid(file_name){
	var order_id = current_order_id;
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/pay_advanced_by_admin_ajax/?"
			+ "order_id=" + order_id + "&file_name=" + file_name;

	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			jQuery.fn.yiiGridView.update('supplier-order-grid');
		}
	});
}

function updateSupplyOrderFullPaid(file_name){
	var order_id = current_order_id;
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/pay_remaining_by_admin_ajax/?"
			+ "order_id=" + order_id + "&file_name=" + file_name;

	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			jQuery.fn.yiiGridView.update('supplier-order-grid');
		}
	});
}

/**************************************************/
/* Archive order
/**************************************************/
$(document).on('click', '.order_archive', function(e){
    e.preventDefault();
    if (!confirm('Are you sure?')){
        return false;
    }
    $(e.target).attr('disabled', 'disabled');
    $.post(
        '<?php echo CHtml::normalizeUrl('/supplierorder/archive');?>',
        { id: $(this).attr('rel') }, function(){
            $.fn.yiiGridView.update('supplier-order-grid');
        }
    );
});

/**************************************************/
/* Delete order
/**************************************************/

jQuery('.order_delete').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
    var target = jQuery(e.target);
    target.attr('disabled', 'disabled');
    var order_id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/delete_order_by_admin_ajax/?order_id=" + order_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                // just delete table row
                // jQuery('#delete_'+old_item_id).parent().parent().remove();
                // target.parent().parent().remove();
                $.fn.yiiGridView.update('supplier-order-grid');
            }
		}
	});
    e.preventDefault();
    e.stopImmediatePropagation();
});

/**************************************************/
/* Resend denied order
/**************************************************/

jQuery('.order_resend').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
    var target = jQuery(e.target);
    var order_id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/resend_order_by_admin_ajax/?order_id=" + order_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                $.fn.yiiGridView.update('supplier-order-grid');
            }
		}
	});
    e.preventDefault();
    e.stopImmediatePropagation();
});

/**************************************************/
/* Delete pending order
/**************************************************/

jQuery('.delete_pending_order').live('click', function(e){
    var target = jQuery(e.target);
    var order_id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/delete_pending_order_by_admin_ajax/?order_id=" + order_id;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                $.fn.yiiGridView.update('supplier-order-grid');
            }
		}
	});
    e.preventDefault();
    e.stopImmediatePropagation();
});

/**************************************************/
/* Change Order Status
/**************************************************/

jQuery('.change_status').live('click', function(e){
    var target = jQuery(e.target);
    var order_id = target.attr('rel');
    var old_status = target.attr('data-status');
    
    jQuery('#orderStatusDiv #status_order_id').val(order_id);
    jQuery('#orderStatusDiv #old_status').val(old_status);
    jQuery('#new_status').trigger('change');
    jQuery('#orderStatusButton').trigger('click');
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('#new_status').change(function(e){
    var val = jQuery(e.target).val();
    var old_status = jQuery('#old_status').val();
    if ((val == '<?php echo SupplierOrder::$STATUS_MAKING ?>' || val == '<?php echo SupplierOrder::$STATUS_COMPLETED ?>')
        && old_status == 'advanced_paid'){
        jQuery('#etl_block').show();
    } else {
        jQuery('#etl_block').hide();
    }
});

jQuery('#change_status_submit').click(function(e){
    var order_id = jQuery('#orderStatusDiv #status_order_id').val();
    
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/change_order_status_ajax/?order_id=" + order_id;
	jQuery.ajax({
		url:url,
        data:   {
                    status: jQuery('#new_status').val(),
                    etl: jQuery('#new_etl').val()
                },
        type: 'post',
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
                $.fn.yiiGridView.update('supplier-order-grid');
            }
		}
	});
    
    jQuery.fancybox.close();
});

jQuery('.supplier_name').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	jQuery('#supplier_info_block > div').html(jQuery('#supplier_info_'+id).html());
	jQuery('#supplier_info_button').trigger('click');
	e.preventDefault();
    e.stopImmediatePropagation();
});

</script>
