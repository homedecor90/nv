<?php
/**
 * @var $this SupplierorderController
 * @var $item SupplierOrderItems
 * @var $form CActiveForm
 */
?>
<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'supplierOrderItems-notes-form',
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'focus'=>array($item,'notes'),
)); ?>
    <div class="row">
        <?php echo $form->textArea($item, 'notes');?>
    </div>
    <div class="row">
        <?php echo CHtml::submitButton('Submit');?>
    </div>
<?php $this->endWidget();?>
</div>