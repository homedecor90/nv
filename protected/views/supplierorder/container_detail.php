<h1>Container Detail</h1>

<?php
if (YumUser::model()->isSupplierShipper(false)){
	$this->menu=array(
		array('label'=>'Available Supplier Orders', 'url'=>array('shipping')),
	);
} else {
	$this->menu=array(
		array('label'=>'Create Supplier Order', 'url'=>array('create')),
		array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
	);
}
?>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#setEtlButton',
    'config'=>array(
        'width'=>250,
        'height'=>120,
        'autoDimensions'=>false,
    ),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
    <div id="setEtlDiv" class="fancy_box_div">
        <?php
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',array(
            'value'=>'',
            'name'=>'new_etl',
            'mode'=>'date', //use "time","date" or "datetime" (default)
            'language'=>'en-GB',
            'options'=>array(
                "dateFormat"=>"M. dd, yy"
            ) // jquery plugin options
        ));
        ?>
        <input type="button" value="Set E.T.L" id="set_etl_fancy"/>
    </div>
</div>

<span class="clickable" href="#setEtlDiv" id="setEtlButton">Set ETL</span>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-grid',
	'dataProvider'=>$supplierOrderContainerModel->search(),
	'columns'=>array(
		array(
			'header'=>'Created On',
			'value'=>'date("M. d, Y", strtotime($data->created_date))',
		),
		array(
			'header'=>'Orders',
			'value'=>'SupplierOrderContainer::getOrderInfo($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total EXW Price',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalEXWPrice($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total FOB Price',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalFOBPrice($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total CBM',
			'value'=>'SupplierOrderContainer::model()->getSupplierOrderContainerTotalCBM($data->id)',
			'type'=>'raw',
		),
		'status',
		array(
			'header'=>'Shipper',
			'value'=>'SupplierOrderContainer::model()->getShipperName($data->id)',
			'type'=>'raw',
		),
	),
)); ?>

<?php
if ($supplierOrderContainerModel->status != SupplierOrderContainer::$STATUS_CHOSEN_SHIPPER){	
?>
		<input type="button" id="reset_all_bids" value="Reset All Bids"/>
<?php	
} 
?>
<input type="button" id="print_qr_code" value="Print QR code"/>
<?php
/*
<input type="button" id="scan_in_stock" value="Scan Items As IN"/>
*/?>
<br/><br/><br/>

<h3>Bid List</h3>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-bidding-grid',
	'dataProvider'=>$supplierOrderContainerBiddingModel->search($supplierOrderContainerModel->id),
	'columns'=>array(
		array(
			'header'=>'Shipper',
			'value'=>'SupplierOrderContainerBidding::model()->getShipperName($data->id).( $data->supplier_shipper_id == '.intval($supplierOrderContainerModel->supplier_shipper_id).' ? \'\' : \'<input type="button" class="delete_bid" rel="\'.$data->id.\'" value="Delete" />\')',
			'type'=>'raw',
		),		
		array(
			'header'=>'EXW Options',
			'value'=>'SupplierOrderContainerBidding::model()->getBiddingEXWOptions($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'FOB Options',
			'value'=>'SupplierOrderContainerBidding::model()->getBiddingFOBOptions($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Cut-Off Date',
			'value'=>'date("M, d. Y", strtotime($data->cutoff_date))',
			'type'=>'raw',
		),
		array(
			'header'=>'Bid E.T.A',
			'value'=>'date("M, d. Y", strtotime($data->bid_eta))',
			'type'=>'raw',
		),
		array(
			'header'=>'Total EXW Price With Shipping',
			'value'=>'SupplierOrderContainerBidding::model()->getBiddingOptionsEXWTotalPriceWithShipping($data->id)',
			'type'=>'raw',
		),
		array(
			'header'=>'Total FOB Price With Shipping',
			'value'=>'SupplierOrderContainerBidding::model()->getBiddingOptionsFOBTotalPriceWithShipping($data->id)',
			'type'=>'raw',
		),
	),
)); ?>

<script>

var current_container_id = <?php echo $supplierOrderContainerModel->id; ?>;

jQuery(document).ready(function(){
	jQuery("#reset_all_bids").click(function(){
		var bid_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/reset_all_bids/"
			+ "?container_id=" + current_container_id;
			
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('supplier-order-container-grid');
				jQuery.fn.yiiGridView.update('supplier-order-container-bidding-grid');
			}
		});
	});

	jQuery(".choose_exw").live("click", function(){
        if (confirm("Are you sure you want to select this bid?")) {
            var bid_id = jQuery(this).attr("rel");
            var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/choose_bid_by_admin_ajax/"
                + "?bid_id=" + bid_id + "&terms=<?php echo SupplierOrderContainer::$TERMS_EXW;?>";

            jQuery.ajax({
                url:url,
                dataType:"json",
                success:function(result){
                    jQuery.fn.yiiGridView.update('supplier-order-container-grid');
                    jQuery.fn.yiiGridView.update('supplier-order-container-bidding-grid');
                    jQuery("#reset_all_bids").remove();
                }
            });
        }
	});

	jQuery(".choose_fob").live("click", function(){
        if (confirm("Are you sure you want to select this bid?")) {
            var bid_id = jQuery(this).attr("rel");
            var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/choose_bid_by_admin_ajax/"
                + "?bid_id=" + bid_id + "&terms=<?php echo SupplierOrderContainer::$TERMS_FOB;?>";

            jQuery.ajax({
                url: url,
                dataType: "json",
                success: function (result) {
                    jQuery.fn.yiiGridView.update('supplier-order-container-grid');
                    jQuery.fn.yiiGridView.update('supplier-order-container-bidding-grid');
                    jQuery("#reset_all_bids").remove();
                }
            });
        }
	});

    jQuery(".delete_bid").live("click", function(){
		var bid_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/delete_container_bid_ajax/" + "?bid_id=" + bid_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.status == 'fail'){
                    alert(result.message);
                } else {
                    jQuery.fn.yiiGridView.update('supplier-order-container-bidding-grid');
                }
			}
		});
	});

    jQuery('#print_qr_code').click(function(e){
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/get_container_ind_items_ids_ajax/" + "?container_id=" + current_container_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.status == 'fail'){
                    alert(result.message);
                } else {
                    window.open("<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/print_qrcode/?id=" + result.ids.join('_'), '','left=10');
                }
			}
		});
    });

	jQuery('#scan_in_stock').click(function(){
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/scan_container_items_in_stock_ajax/" + "?container_id=" + current_container_id;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.status == 'fail'){
                    alert(result.message);
                } else {
                    alert('Total items: '+result['items']);
                }
			}
		});
	});

    jQuery("#set_etl_fancy").live("click", function(){
        var id = <?php echo $supplierOrderContainerModel->id; ?>;
        var etl_date = jQuery("#new_etl").val();
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/set_etl_by_admin_ajax/?"
            + "id=" + id + "&etl_date=" + etl_date + '&container=1';
        jQuery.fancybox.close();

        jQuery.ajax({
            url:url,
            dataType:"json",
            success:function(result){
                if(result.message){
                    alert(result.message);
                }

                if (result.status == "success"){
                    jQuery.fn.yiiGridView.update('supplier-order-container-grid');
                }
            }
        });
    });
});
</script>