<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('supplier-order-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<h1>View Supplier Order Payment Track</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'header'=>'Supplier Name',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierName($data->id)',
		),
		array(
			'header'=>'Date of Advance',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getAdvancedDate($data->id)',
		),		
		array(
			'header'=>'Date of Final Payment',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getFullPaidDate($data->id)',
		),
		array(
			'header'=>'Total Paid for Shipment',
			'type'=>'raw',
            'value'=>'SupplierOrder::model()->getTotalPaidForShipDate($data->id)',
		),
	),
)); ?>
