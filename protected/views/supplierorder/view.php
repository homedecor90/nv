<?php
/*$this->breadcrumbs=array(
	'Supplier Orders'=>array('index'),
	$model->id,
);*/

$this->menu=array(
	array('label'=>'Update Supplier Order', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Supplier Order', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Supplier Order', 'url'=>array('admin')),
);
?>

<h1>View Supplier Order #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'supplier_id',
		'created_date',
		'accepted_date',
		'advanced_paid_date',
		'completed_date',
		'status',
	),
)); ?>
