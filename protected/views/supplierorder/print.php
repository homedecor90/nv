<h3>Order detail</h3>
Supplier: <?php echo $order->supplier->profile->firstname.' '.$order->supplier->profile->lastname ;?>

<table class="items">
	<tr>
		<th></th>
		<th>Item Code</th>
		<th>Item Name</th>
		<th>Color</th>
		<th>Qty</th>
	</tr>
<?php
foreach ($order->order_items as $i=>$items){
	echo '<tr>';
	echo '<td>'.($i+1).'</td>';
	echo '<td>'.$items->items->item_code.'</td>';
	echo '<td>'.$items->items->item_name.'</td>';
	echo '<td>'.$items->items->color.($items->custom_color ? ' ('.$items->custom_color.')' : '').'</td>';
	echo '<td>'.$items->quantity.'</td>';
	echo '</tr>';
}

?>
</table>