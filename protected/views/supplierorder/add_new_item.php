<h1>Add New Item</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'item_name'); ?>
		<?php echo $form->textField($newItemModel,'item_name',array('size'=>60,'maxlength'=>100, 'name'=>'item_name')); ?>
		<?php echo $form->error($newItemModel,'item_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'item_code'); ?>
		<?php echo $form->textField($newItemModel,'item_code',array('size'=>60,'maxlength'=>100, 'name'=>'item_code')); ?>
		<?php echo $form->error($newItemModel,'item_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'color'); ?>
		<?php echo $form->textField($newItemModel,'color',array('size'=>60,'maxlength'=>100, 'name'=>'color')); ?>
		<?php echo $form->error($newItemModel,'color'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'sale_price'); ?>
		<?php echo $form->textField($newItemModel,'sale_price', array('name'=>'sale_price')); ?>
		<?php echo $form->error($newItemModel,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'exw_cost_price'); ?>
		<?php echo $form->textField($newItemModel,'exw_cost_price', array('name'=>'exw_cost_price')); ?>
		<?php echo $form->error($newItemModel,'exw_cost_price'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($newItemModel,'fob_cost_price'); ?>
		<?php echo $form->textField($newItemModel,'fob_cost_price', array('name'=>'fob_cost_price')); ?>
		<?php echo $form->error($newItemModel,'fob_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'shipping_price'); ?>
		<?php echo $form->textField($newItemModel,'shipping_price', array('name'=>'shipping_price')); ?>
		<?php echo $form->error($newItemModel,'shipping_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'local_pickup'); ?>
		<?php echo $form->textField($newItemModel,'local_pickup', array('name'=>'local_pickup')); ?>
		<?php echo $form->error($newItemModel,'local_pickup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'la_oc_shipping'); ?>
		<?php echo $form->textField($newItemModel,'la_oc_shipping', array('name'=>'la_oc_shipping')); ?>
		<?php echo $form->error($newItemModel,'la_oc_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'canada_shipping'); ?>
		<?php echo $form->textField($newItemModel,'canada_shipping', array('name'=>'canada_shipping')); ?>
		<?php echo $form->error($newItemModel,'canada_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'cbm'); ?>
		<?php echo $form->textField($newItemModel,'cbm', array('name'=>'cbm')); ?>
		<?php echo $form->error($newItemModel,'cbm'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::label('quantity', 'quantity'); ?>
		<?php echo CHtml::textField('quantity', '1', array('name'=>'quantity')); ?>		
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'dimensions_length'); ?>
		<?php echo $form->textField($newItemModel,'dimensions_length',array('size'=>60,'maxlength'=>100, 'name'=>'dimensions_length')); ?>
		<?php echo $form->error($newItemModel,'dimensions_length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'dimensions_width'); ?>
		<?php echo $form->textField($newItemModel,'dimensions_width',array('size'=>60,'maxlength'=>100, 'name'=>'dimensions_width')); ?>
		<?php echo $form->error($newItemModel,'dimensions_width'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'dimensions_height'); ?>
		<?php echo $form->textField($newItemModel,'dimensions_height',array('size'=>60,'maxlength'=>100, 'name'=>'dimensions_height')); ?>
		<?php echo $form->error($newItemModel,'dimensions_height'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'weight'); ?>
		<?php echo $form->textField($newItemModel,'weight', array('name'=>'weight')); ?>
		<?php echo $form->error($newItemModel,'weight'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'description'); ?>
		<?php echo $form->textArea($newItemModel,'description',array('rows'=>6, 'cols'=>50, 'name'=>'description')); ?>
		<?php echo $form->error($newItemModel,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($newItemModel,'condition'); ?>
		<?php $condition_list = array('New'=>'New', 'Damaged'=>'Damaged', 'Returned New'=>'Returned New');?>
		<?php echo $form->dropDownList($newItemModel,'condition', $condition_list, array('name'=>'condition')); ?>
		<?php echo $form->error($newItemModel,'condition'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::Button('Create', array('id'=>'add_new_item_submit')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->