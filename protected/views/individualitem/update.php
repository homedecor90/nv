<?php
/*$this->breadcrumbs=array(
	'Individual Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);*/

$this->menu=array(
	array('label'=>'Create Individual Items', 'url'=>array('create')),
	array('label'=>'View Individual Item', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Individual Item', 'url'=>array('admin')),
);
?>

<h1>Update Individual Item <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'itemCodeList'=>$itemCodeList,)); ?>