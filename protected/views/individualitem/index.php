<?php
/*$this->breadcrumbs=array(
	'Individual Items',
);*/

$this->menu=array(
	array('label'=>'Create Individual Items', 'url'=>array('create')),
	array('label'=>'Manage Individual Item', 'url'=>array('admin')),
);
?>

<h1>Individual Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
