<?php
$item_model = Item::model()->findByPK($data->item_id);
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_code')); ?>:</b>
	<?php echo CHtml::encode($item_model->item_code); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('color')); ?>:</b>
	<?php echo CHtml::encode($item_model->color); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exw_cost_price')); ?>:</b>
	<?php echo CHtml::encode($data->exw_cost_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fob_cost_price')); ?>:</b>
	<?php echo CHtml::encode($data->fob_cost_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sale_price')); ?>:</b>
	<?php echo CHtml::encode($data->sale_price); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('shipping_price')); ?>:</b>
	<?php echo CHtml::encode($data->shipping_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('local_pickup')); ?>:</b>
	<?php echo CHtml::encode($data->local_pickup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('la_oc_shipping')); ?>:</b>
	<?php echo CHtml::encode($data->la_oc_shipping); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('canada_shipping')); ?>:</b>
	<?php echo CHtml::encode($data->canada_shipping); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cbm')); ?>:</b>
	<?php echo CHtml::encode($data->cbm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensions_length')); ?>:</b>
	<?php echo CHtml::encode($data->dimensions_length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensions_width')); ?>:</b>
	<?php echo CHtml::encode($data->dimensions_width); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensions_height')); ?>:</b>
	<?php echo CHtml::encode($data->dimensions_height); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weight')); ?>:</b>
	<?php echo CHtml::encode($data->weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condition')); ?>:</b>
	<?php echo CHtml::encode($data->condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_order_items_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_order_items_id); ?>
	<br />


</div>