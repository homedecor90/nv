<?php
/*$this->breadcrumbs=array(
	'Individual Items'=>array('index'),
	'Queue',
);*/

$this->menu=array(
	array('label'=>'Manage Individual Item', 'url'=>array('admin')),
	array('label'=>'Create Individual Items', 'url'=>array('create')),
);
?>

<h1>Individual Item Queue</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search_queue',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="grid_header">
	<?php echo CHtml::Button('Send Order', array('id'=>'send_order')); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'individual-item-grid',
	'dataProvider'=>$model->searchQueue(),
	'columns'=>array(
		array(
			'class'=>'CCheckBoxColumn',
			'selectableRows' => 2,
			'value'=>'$data->id'
		),
		'id',
		array(
			'header'=>'Item Code',
			'value'=>'$data->items->item_code',
			'filter'=>CHtml::activeTextField($model, 'item_code'),
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
			'filter'=>CHtml::activeTextField($model, 'color'),
		),
		'exw_cost_price',
		'fob_cost_price',
		'sale_price',
		'status',
		array(
			'header'=>'Customer Ordered',
			'value'=>'Individualitem::model()->customerOrdered($data->id)',
			'filter'=>CHtml::activeTextField($model, 'color'),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>

<input type="button" value="Set Order" href="#sendOrderDiv" id="sendOrderButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#sendOrderButton',
    'config'=>array(
		'width'=>200,
		'height'=>120,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="sendOrderDiv" class="fancy_box_div">
<div class="supplier_list_div">
<?php
	echo CHtml::label("Supplier", "supplier_id");
	echo CHtml::dropDownList('supplier_id', '', $supplierList);
?>	
</div>
<input type="button" value="Send Order" id="select_supplier"/>
</div>
</div>

<script>
var selItemList = "";
jQuery(document).ready(function(){
	jQuery('.search-button').click(function(){
		jQuery('.search-form').toggle();
		return false;
	});

	jQuery("#send_order").click(function(){
		selItemList = "";
		jQuery("table.items td.checkbox-column input:checked").each(function(){
			if (jQuery(this).attr("id") == "individual-item-grid_c0_all"){
				return;
			}
			
			if (selItemList != ""){
				selItemList += "_";
			}
			
			selItemList += jQuery(this).val();
		});
		
		if (selItemList == ""){
			alert("Please input the item to send order");
			return false;
		}
		
		jQuery("#sendOrderButton").trigger("click");
	});
	
	jQuery("#select_supplier").live("click", function(){
		var supplier_id = jQuery("#supplier_id").val();
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/send_order_items_by_admin_ajax/?"
			+ "supplier_id=" + supplier_id + "&item_list=" + selItemList;

		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fancybox.close();
				jQuery.fn.yiiGridView.update('individual-item-grid');
			}
		});
	});
});
</script>