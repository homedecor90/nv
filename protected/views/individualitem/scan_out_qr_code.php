<h1>Scan Out Individual Item</h1>
<strong>Item name: </strong><?php echo $item->item_name; ?><br />
<strong>Item Code: </strong><?php echo $item->item_code; ?><br />
Box #<?php echo $box.': '.$item->getSingleBoxDimsStr($box); ?>
<br /><br />
<div id="scan_block">
<strong>Please select shipment you are scanning out</strong>
<table class="border_table">
	<tr>
		<th>ID</th>
		<th>Status</th>
		<th>Shipping Name</th>
		<th>Boxes scanned</th>
		<th></th>
	</tr>
<?php 
foreach ($shipments as $shipment){
	echo '<tr>';
	echo '<td>'.$shipment->id.'</td>';
	echo '<td>'.$shipment->status.'</td>';
	echo '<td>'.$shipment->customer_order->getShippingName().'</td>';
	$box_total = $shipment->getBoxQty();
	$pending_scan = $shipment->getPendingScanOutBoxQty();
	$local_pickup = $shipment->customer_order->shipping_method == CustomerOrder::SHIPPING_METHOD_LOCAL_PICKUP;
	echo '<td>'.($box_total - $pending_scan).'/'.$box_total.'</td>';
	echo '<td><input type="button" class="select_shipment" value="Select" rel="'.$shipment->id.'" data-tracking-number="'.($pending_scan == 1 && !$local_pickup ? 1 : 0).'" /></td>';
	echo '</tr>';
}
?>
</table>
</div>
<h2 id="status_block" style="display: none;"></h2>
<?php
// echo '<pre>';
// print_r(Yii::app()->user->getState('boxes_to_scan_out'));
// echo '</pre>';
?>

<script type="text/javascript">
	jQuery('.select_shipment').live('click', function(e){
		if (!confirm('Are you sure?')){
			return false;
		}
		var target = jQuery(e.target);
	    var id = target.attr('rel');
		var tracking_number = null;
		var tracking_website = null;tracking_number
		
	    if (target.attr('data-tracking-number') == 1){
			tracking_number = prompt('Please enter tracking number','');
			tracking_website = prompt('Please enter tracking website','');
			if (!tracking_number || !tracking_website){
				alert('Incorrect data');
				return false;
			}
		}
	    
	    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/select_shipment_to_scan_box_ajax/?shipment_id="+id;

		var data = {
			tracking_number: tracking_number,
			tracking_website: tracking_website,
			item_id: <?php echo $item->id; ?>,
			box: <?php echo $box; ?>
		};
	    
	    jQuery.ajax({
	        type: 'post',
	        data: data,
	        url: url,
	        success: function(result){
	            if (result.status == 'fail'){
	                alert(result.message);
	                return;
	            }

	            if (result.status == 'success'){
	            	jQuery('#scan_block').remove();
					jQuery('#status_block').text(result.message).show(500);
		        }
	        },
	        dataType: 'json'
	    });
	    
	    e.preventDefault();
	    e.stopImmediatePropagation();
	});
</script>