<?php
/*$this->breadcrumbs=array(
	'Individual Items'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Create Individual Items', 'url'=>array('create')),
	array('label'=>'Individual Item Queue', 'url'=>array('queue')),
);
?>

<h1>Manage Individual Items</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
    'item_list'=>$item_list
)); ?>
</div><!-- search-form -->

<div class="grid_header">
	<?php echo CHtml::Button('Print QR Code', array('id'=>'print_qr_code')); ?>
</div>

<?php $this->widget('CGridViewExt', array(
	'id'=>'individual-item-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'class'=>'CCheckBoxColumn',
			'selectableRows' => 2,
			'value'=>'$data->id'
		),
		'id',
		array(
			'header'=>'Item Code',
			'value'=>'$data->items->item_code',
			'filter'=>CHtml::activeTextField($model, 'item_code'),
		),
		array(
			'header'=>'Color',
			'value'=>'$data->items->color',
			'filter'=>CHtml::activeTextField($model, 'color'),
		),
		'exw_cost_price',
		'fob_cost_price',
		'sale_price',
		'status',
		array(
			'header'=>'Customer Ordered',
			'value'=>'Individualitem::model()->customerOrdered($data->id)',
			'filter'=>CHtml::activeTextField($model, 'color'),
		),
        array(
            'header'=>'ETA',
            'value'=>'$data->eta && $data->supplier_order_id ? date("M. d, Y", strtotime($data->eta)) : \'\'',
        ),
        array(
            'header'=>'Supplier',
            'type'=>'raw',
            'value'=>'$data->supplier_order ? $data->supplier_order->supplier->profile->firstname.\' \'.$data->supplier_order->supplier->profile->lastname : \'\'',
        ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view_detail}{check_in_out}',
			'buttons'=>array(
				'view_detail'=>array(
					'label'=>'View Detail<br/>',
					'url'=>'Yii::app()->createUrl("individualitem/view_detail", array("id"=>$data->id))',
				),
				'check_in_out'=>array(
					'label'=>'Check In/Out',
					'url'=>'Yii::app()->createUrl("individualitem/view", array("id"=>$data->id))',
				),
			),
		),
	),
)); ?>

<script>
jQuery(document).ready(function(){
	jQuery('.search-button').click(function(){
		jQuery('.search-form').toggle();
		return false;
	});

	jQuery("#print_qr_code").click(function(){
		var selItemList = "";
		jQuery("table.items td.checkbox-column input:checked").each(function(){
			if (jQuery(this).attr("id") == "individual-item-grid_c0_all"){
				return;
			}
			
			if (selItemList != ""){
				selItemList += "_";
			}
			
			selItemList += jQuery(this).val();
		});
		
		if (selItemList == ""){
			alert("Please input the item to print QR Code");
			return false;
		}
		
		window.open("<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/print_qrcode/?id=" + selItemList, '','left=10');
	});
});
</script>