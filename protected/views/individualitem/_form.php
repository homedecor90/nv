<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'individual-item-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo CHtml::hiddenField('Individualitem[item_id]', 0); ?>

	<?php 
		echo CHtml::label('Item Code', 'item_code');
		echo CHtml::dropDownList('item_code', '', $itemCodeList['itemCodeList']);
	?>

	<?php 
		echo CHtml::label('Color', 'item_color');
		echo CHtml::dropDownList('item_color', '', $itemCodeList['itemColorList']);
	?>

	<div class="row">
		<?php echo $form->labelEx($model,'exw_cost_price'); ?>
		<?php echo $form->textField($model,'exw_cost_price'); ?>
		<?php echo $form->error($model,'exw_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fob_cost_price'); ?>
		<?php echo $form->textField($model,'fob_cost_price'); ?>
		<?php echo $form->error($model,'fob_cost_price'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price'); ?>
		<?php echo $form->error($model,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shipping_price'); ?>
		<?php echo $form->textField($model,'shipping_price'); ?>
		<?php echo $form->error($model,'shipping_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'local_pickup'); ?>
		<?php echo $form->textField($model,'local_pickup'); ?>
		<?php echo $form->error($model,'local_pickup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'la_oc_shipping'); ?>
		<?php echo $form->textField($model,'la_oc_shipping'); ?>
		<?php echo $form->error($model,'la_oc_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'canada_shipping'); ?>
		<?php echo $form->textField($model,'canada_shipping'); ?>
		<?php echo $form->error($model,'canada_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cbm'); ?>
		<?php echo $form->textField($model,'cbm'); ?>
		<?php echo $form->error($model,'cbm'); ?>
	</div>
	
	<?php
		if ($model->isNewRecord){
	?>
			<div class="row">
				<label class="required" for="quanitty">Quantity <span class="required">*</span><label>
				<?php echo CHtml::textField('quantity', 1, array("id"=>"txtQuantity")); ?>				
			</div>
	<?php
		}
	?>

	<div class="row">
		<?php echo $form->labelEx($model,'dimensions_length'); ?>
		<?php echo $form->textField($model,'dimensions_length',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dimensions_length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dimensions_width'); ?>
		<?php echo $form->textField($model,'dimensions_width',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dimensions_width'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dimensions_height'); ?>
		<?php echo $form->textField($model,'dimensions_height',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dimensions_height'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'weight'); ?>
		<?php echo $form->textField($model,'weight'); ?>
		<?php echo $form->error($model,'weight'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condition'); ?>
		<?php $condition_list = array('New'=>'New', 'Damaged'=>'Damaged', 'Returned New'=>'Returned New');?>
		<?php echo $form->dropDownList($model,'condition', $condition_list); ?>
		<?php echo $form->error($model,'condition'); ?>
	</div>
	
	<?php
		if ($model->isNewRecord){
	?>
			<?php echo CHtml::checkBox('print_qrcode');?> <b>Print QR Code</b>
	<?php
		}
	?>

	<?php echo CHtml::hiddenField('Individualitem[status]', "In Stock"); ?>
	<?php echo CHtml::hiddenField('Individualitem[customer_order_items_id]', 0); ?>

	<div class="row buttons">
		<?php
			if ($model->isNewRecord){
				echo CHtml::submitButton('Create', array("id"=>"create_individual_items"));
			} else {
				echo CHtml::submitButton('Save');
			}
		?>
	</div>

<?php $this->endWidget(); ?>

<script>
jQuery(document).ready(function(){
	jQuery("#item_code").change(function(){
		changeItemCode();
	});
	
	jQuery("#item_color").change(function(){
		var item_id = jQuery(this).val();
		changeItemColor(item_id);
	});
	
	jQuery("#create_individual_items").click(function(){
		var quantity = jQuery("#txtQuantity").val();
		if (isNaN(quantity)){
			alert("Please input the exact quantity");
			jQuery("#txtQuantity").focus();
			return false;
		}
	});
	
	<?php
		if (isset($save_success)){
			if ($save_success == true){
	?>
				changeItemCode();
	<?php
			}
		}
	?>
});

function changeItemCode(){
	var item_code = jQuery("#item_code").val();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/itemcolorlist/?item_code=" + encodeURIComponent(item_code);
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.message != "success")
				return;
				
			var itemColorList = result.itemColorList;
			var selectText = "";
			var bChangeColor = false;
			for (var item_id in itemColorList){
				if (!bChangeColor){
					changeItemColor(item_id);
					bChangeColor = true;
				}					
				selectText += '<option value="' + item_id + '">' + itemColorList[item_id] + '</option>';
			}
			jQuery("#item_color").html(selectText);
		}
	});
}

function changeItemColor(item_id){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/iteminfo/?id=" + item_id;
	jQuery("#Individualitem_item_id").val(item_id);
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){				
			if (result.message != "success")
				return;
			
			var itemInfo = result.itemInfo;
			
			jQuery("#Individualitem_exw_cost_price").val(itemInfo.exw_cost_price);
			jQuery("#Individualitem_fob_cost_price").val(itemInfo.fob_cost_price);
			jQuery("#Individualitem_sale_price").val(itemInfo.sale_price);
			jQuery("#Individualitem_shipping_price").val(itemInfo.shipping_price);
			jQuery("#Individualitem_local_pickup").val(itemInfo.local_pickup);
			jQuery("#Individualitem_la_oc_shipping").val(itemInfo.la_oc_shipping);
			jQuery("#Individualitem_canada_shipping").val(itemInfo.canada_shipping);
			jQuery("#Individualitem_cbm").val(itemInfo.cbm);
			jQuery("#Individualitem_dimensions_length").val(itemInfo.dimensions_length);
			jQuery("#Individualitem_dimensions_width").val(itemInfo.dimensions_width);
			jQuery("#Individualitem_dimensions_height").val(itemInfo.dimensions_height);
			jQuery("#Individualitem_weight").val(itemInfo.weight);
			jQuery("#Individualitem_description").val(itemInfo.description);
			jQuery("#Individualitem_condition").val("New");
		}
	});
    
    // get current count in stock
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/itemcountinstock/?id=" + item_id;
    jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
            if (result.message != "success")
				return;
            jQuery('#txtQuantity').val(result.count_in_stock);
        }
    });
}
</script>

</div><!-- form -->