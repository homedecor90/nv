<?php
/*$this->breadcrumbs=array(
	'Individual Items'=>array('index'),
	$model->id,
);*/

$item_model = Item::model()->findByPK($model->item_id);

$this->menu=array(
	array('label'=>'Create Individual Items', 'url'=>array('create')),
	array('label'=>'Update Individual Item', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Individual Item', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Individual Item', 'url'=>array('admin')),
);
?>

<h1>View Individual Item #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'label'=>'Item Code',			
			'value'=>$item_model->item_code,
		),
		array(
			'label'=>'Color',			
			'value'=>$item_model->color,
		),
		'exw_cost_price',
		'fob_cost_price',
		'sale_price',
		'shipping_price',
		'local_pickup',
		'la_oc_shipping',
		'canada_shipping',
		'cbm',
		'dimensions_length',
		'dimensions_width',
		'dimensions_height',
		'weight',
		'description',
		'condition',
		'status',
		'customer_order_items_id',
		array(
			'label'=>'QR Code',
			'type'=>'raw',
			'value'=>'<img src="'.Yii::app()->getBaseUrl(true).'/images/qrcodes/item_'.$model->id.'.png"></img>'
		)
	),
)); ?>
