<?php
/*$this->breadcrumbs=array(
	'Individual Items'=>array('index'),
	'Create',
);*/

$this->menu=array(
	array('label'=>'Manage Individual Item', 'url'=>array('admin')),
	array('label'=>'Individual Item Queue', 'url'=>array('queue')),
);
?>

<?php
if ($print_qrcode){
?>
	<script>
		jQuery(document).ready(function(){
			window.open('<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/print_qrcode/?id=<?php echo $str_new_id_list;?>', '','left=10');
		});
	</script>
<?php
}
?>

<h1>Create Individual Items</h1>

<?php
	if ($save_success == true && $crated_quantity > 0){
?>
		<h4><?php echo $crated_quantity;?> Items are added.</h4>
<?php
	}
?>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'itemCodeList'=>$itemCodeList, 'save_success'=>$save_success)); ?>