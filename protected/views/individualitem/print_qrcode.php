<style>
	@media print{
		#print_button {display : none}
	}
	
	body{
		margin: 0;
		padding: 0;
	}
	
	img{margin:10px 10px 10px 0; width:150px;}
	.item_qrcode_div{
		display: inline-block;
	    overflow: hidden;
	    width: 32%;
	}
	
	.item_qrcode_info{
		padding-right: 5px;
	}
	
</style>

<input type='button' value='Print' onclick="window.print()" id="print_button"><br/>

<?php
if (trim($id_list) != "" || $type=='instock'){
	if ($type != 'instock'){
		$id_array = explode("_", $id_list);
	}
	foreach ($id_array as $item_id){
		$individual_item = Individualitem::model()->findByPk($item_id);
		if (!$individual_item->hasQrCodes()){
			$individual_item->generateQrCodes();
		}
		if ($individual_item == null) continue;
		
		$item = Item::model()->findByPk($individual_item->item_id);
		if ($item == null) continue;
		for ($i = 1; $i<=$item->box_qty; $i++):
?>
			<div class="item_qrcode_div">
				<img src="<?php echo Yii::app()->getBaseUrl(true);?>/images/qrcodes/item_<?php echo $item_id.'_'.$i;?>.png"/>
				<div class="item_qrcode_info">
					<b>Item Code:<?php echo $item->item_code;?><br/>
					Color:<?php echo $item->color;?><br />
					Individual Item ID:<?php echo $individual_item->id;?><br />
					Box:<?php echo $i.'/'.$item->box_qty;?><br /></b>
					<?php echo '('.str_replace(' x ','x',$item->getSingleBoxDimsStr($i)).')'; ?>
				</div>
			</div>
<?php
		endfor;
	}
}
?>
