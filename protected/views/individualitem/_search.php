<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_id'); ?>
		<?php //echo $form->textField($model,'item_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->dropDownList($model, 'item_id', $item_list); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exw_cost_price'); ?>
		<?php echo $form->textField($model,'exw_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fob_cost_price'); ?>
		<?php echo $form->textField($model,'fob_cost_price'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php //echo $form->textField($model,'status',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>0,'value'=>Individualitem::$STATUS_SOLD)); ?>
		<?php echo Individualitem::$STATUS_SOLD; ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>1, 'value'=>Individualitem::$STATUS_NOT_ORDERED)); ?>
		<?php echo Individualitem::$STATUS_NOT_ORDERED; ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>2, 'value'=>Individualitem::$STATUS_ORDERED_WITHOUT_ETL)); ?>
		<?php echo Individualitem::$STATUS_ORDERED_WITHOUT_ETL; ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>3, 'value'=>Individualitem::$STATUS_ORDERED_WITH_ETL)); ?>
		<?php echo Individualitem::$STATUS_ORDERED_WITH_ETL; ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>4, 'value'=>Individualitem::$STATUS_IN_STOCK)); ?>
		<?php echo Individualitem::$STATUS_IN_STOCK; ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>5, 'value'=>Individualitem::$STATUS_DENIED)); ?>
		<?php echo Individualitem::$STATUS_DENIED; ?>
		<?php echo CHtml::checkBox('', true, array('class'=>'chkIndItemStatus', 'rel'=>6, 'value'=>Individualitem::$STATUS_ORDERED_BUT_WAITING)); ?>
		<?php echo Individualitem::$STATUS_ORDERED_BUT_WAITING; ?>
		<?php echo $form->hiddenField($model,'status_list',array('id'=>'status_list')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script>
var arrStatusList = new Array(1, 1, 1, 1, 1, 1, 1);
jQuery(document).ready(function(){
	jQuery(".chkIndItemStatus").live("change",function(){
		var status_id = jQuery(this).attr("rel");
		var status_val = 0;
		
		if (jQuery(this).attr("checked") == "checked"){
			status_val = 1;
		}
		arrStatusList[status_id] = status_val;
	});
	
	jQuery('.search-form form').submit(function(){
		var strStatusVal = "";
		var bFirst = true;
		for (status_id in arrStatusList){
			if (bFirst){
				strStatusVal = arrStatusList[status_id];
			} else {
				strStatusVal += ("_" + arrStatusList[status_id]);
			}
			bFirst = false;
		}
		
		jQuery("#status_list").val(strStatusVal);
		var search_condition = jQuery(this).serialize();
		
		jQuery.fn.yiiGridView.update('individual-item-grid', {
			data: search_condition
		});
		
		return false;
	});
});
</script>