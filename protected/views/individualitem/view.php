<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'individual-item-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div id="check_in_out_div" class="row buttons">
		<?php echo CHtml::hiddenField('check_state', 'Checked In', array("id"=>"check_state")); ?>
		<?php echo CHtml::submitButton('Check In', array("id"=>"checkin_item"));?>
		<br/>
		<?php echo CHtml::submitButton('Check Out', array("id"=>"checkout_item"));?>
	</div>

<?php $this->endWidget(); ?>
</div>

<script>
jQuery(document).ready(function(){
	jQuery("#checkin_item").click(function(){
		jQuery("#check_state").val('<?php echo Individualitem::$STATUS_IN_STOCK; ?>');
	});
	
	jQuery("#checkout_item").click(function(){
		window.location.href = '<?php echo Yii::app()->getBaseUrl(true).'/index.php/individualitem/scan_out_qr_code/?i_item_id='.$i_item_id.'&box='.$box; ?>';
		//jQuery("#check_state").val('<?php echo Individualitem::$STATUS_SOLD; ?>');
		return false;
	});
});
</script>
