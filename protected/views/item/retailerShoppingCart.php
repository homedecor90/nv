<?php
/**
 * @var $this ItemController
 * @var $item Item
 * @var $customerOrder CustomerOrder
 */
Yii::app()->clientScript->registerScript('item','var item = '.CJSON::encode($itemInfo));
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'retailer-shopping-cart-form',
    'action' => $this->createUrl('customer/retailer_new_order_ajax'),
    'enableAjaxValidation'=>false,
));

$this->widget('zii.widgets.CDetailView', array(
    'data'=>$item,
    'attributes'=>array(
        'item_name',             // title attribute (in plain text)
        array(
            'name' => 'priceWithShipping',
            'cssClass' => 'sale_price'
        ),
        array(
            'label' => 'Select quantity',
            'type'=>'raw',
            'value'=>CHtml::TextField('item[quantity][]', '1', array('class' => 'quantity')),
        ),
        array(
            'label' => 'Shipping method',
            'type'=>'raw',
            'value'=>$form->dropDownList($customerOrder, 'shipping_method', CustomerOrder::getShippingMethods(), array('class'=>'shipping_method')),
        ),
    ),
));
echo CHtml::hiddenField('item_shipping_price', $item->shipping_price);
echo CHtml::hiddenField('item_price', $item->sale_price);
echo CHtml::hiddenField('item[item_id][]', $item->id);
echo CHtml::activeHiddenField($customerOrder, 'customer_id');

echo CHtml::activeHiddenField($customerOrder, 'sales_person_id');
echo CHtml::activeHiddenField($customerOrder, 'grand_total_price');
echo CHtml::activeHiddenField($customerOrder, 'discounted_total');

echo $form->hiddenField($customerOrder, 'discounted', array('value' => 'Discounted'));
echo CHtml::button('Buy and Continue Shopping', array('class' => 'ajax-order close'));
echo CHtml::button('Buy and Checkout', array(
    'class' => 'ajax-order checkout',
    'data-href' => $this->createUrl('customer/createRetailerOrder'),
));

$this->endWidget();
?>