<?php
/*$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Create Item', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Waiting Items</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
$columns = array(
		'item_name',
		'item_code',
		'color',
		'exw_cost_price',
		'fob_cost_price',
		'sale_price',
		'description',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	);
$columns_checkbox = array(array(
	'visible'=>'Yii::app()->user->isAdmin()',
	'class'=>'CMyCheckBoxColumn',
	'selectableRows' => 2,
));

if (Yii::app()->user->isAdmin()){
	$columns = array_merge($columns_checkbox, $columns);
}

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'item-grid',
	'dataProvider'=>$model->search_waiting(),
	'filter'=>$model,
	'columns'=>$columns,
)); 

if (Yii::app()->user->isAdmin()){
?>
	<input type="button" id="approve_items" value="Approve Items"/>
	<input type="button" id="delete_items" value="Delete Items"/>
<?php
}
?>

<script>
jQuery(document).ready(function(){
	jQuery("#approve_items").click(function(){
		if (jQuery("td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the item to approve");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("td.checkbox-column input[type='checkbox']:checked").each(function(){
			var item_id = jQuery(this).val();
			if (id_list == ""){
				id_list = item_id;
			} else {
				id_list += ("_" + item_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/approve_items/?item_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			success:function(result){
				jQuery.fn.yiiGridView.update('item-grid');
			}
		});
	});
    
    jQuery("#delete_items").click(function(){
		if (jQuery("td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the item to delete");
			return false;
		}
		
		var id_list = "";
		var str_original_port_list = "";
		var original_port_list = new Array();
		
		jQuery("td.checkbox-column input[type='checkbox']:checked").each(function(){
			var item_id = jQuery(this).val();
			if (id_list == ""){
				id_list = item_id;
			} else {
				id_list += ("_" + item_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/delete_waiting_items/?item_id_list=" + id_list;
		
		jQuery.ajax({
			url:url,
			success:function(result){
				if (result.status == 'fail'){
                    alert(result.message);
                } else if (result.status == 'success'){
                    jQuery.fn.yiiGridView.update('item-grid');
                }
			},
            dataType:"json"
		});
	});
});
</script>