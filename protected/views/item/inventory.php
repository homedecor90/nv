<?php
/*$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Create Item', 'url'=>array('create')),
);

// $item = Item::model()->findByPK(18);
// print_r($item->fetchStats());
// die();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('inventory-item-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->to_order = Item::getToOrderTable();
$this->not_ordered = Item::getToOrderTable(true);

echo CancellationReason::getCancellationReasonCode();
echo CustomerOrder::getScanOutFormCode();

?>

<h1>Inventory</h1>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
		<?php echo CHtml::label('Select item', 'Item_id'); ?>
		<?php //echo $form->textField($model,'item_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->dropDownList($model, 'id', $item_list); ?>
	</div>
    
    <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>
    
<?php $this->endWidget(); ?>

</div><!-- search-form -->

<input type="button" value="Print" onclick="window.location.href='<?php echo Yii::app()->getBaseUrl(true);?>/index.php/pdf/inventory';" />
<input type="button" value="Print In Stock QR Codes" onclick="window.open('<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/print_qrcode/?id=&type=instock', '','left=10');" />
<?php

$this->widget('application.extensions.fancybox.EFancyBox', array(

    'target'=>'img.fancy',
    'config'=>array(
//        'width'=>380,
//        'height'=>200,
        'autoDimensions'=>true,
    ),

));

?>

<?php $this->widget('CGridViewExt', array(
	'id'=>'inventory-item-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
        array(
            'header' => false,
            'type' => 'raw',
            'value' => 'CHtml::image($data->thumbnailUrl,
                        $data->item_name,
                        empty($data->image)?
                        array():array(
                            "href"=>Yii::app()->baseUrl.Item::IMAGE_PATH.$data->image,
                            "class"=>"fancy")
                        )',
            'htmlOptions' => array('width' => '120'),
        ),
		array(
			'header'=>'Item Name',
			'type'=>'raw',
			'value'=>'$data->item_name'
		),
		array(
			'header'=>'Code',
			'type'=>'raw',
			'value'=>'$data->item_code'
		),
		array(
			'header'=>'Color',
			'type'=>'raw',
			'value'=>'$data->color'
		),
		array(
			'header'=>'In Stock',
			'type'=>'raw',
			'value'=>'(Yii::app()->user->isAdmin()?\'<div class="inv_buttons"><input type="button" class="dec_qty" data-itemid="\'.$data->id.\'" value="-" />\':\'\').\'<span class="inner"><span class="instock_qty" id="instock_qty_\'.$data->id.\'">\'.$data->countItemsInventory(Individualitem::$STATUS_IN_STOCK).\'</span> <span class="expand_sold" rel="\'.$data->id.\'" data-status="in_stock">(\'.$data->countItemsInventory(Individualitem::$STATUS_IN_STOCK, true).\')</span></span></span>\'.(Yii::app()->user->isAdmin()?\'<input type="button" class="inc_qty" data-itemid="\'.$data->id.\'" value="+" /></div>\':\'\')'
		),
        array(
			'header'=>'Sold Not Ordered',
			'type'=>'raw',
			'value'=>'\'<span class="expand_sold" rel="\'.$data->id.\'" data-status="sold_not_ordered">\'.$data->countItemsInventory(Individualitem::$STATUS_NOT_ORDERED, true).\'</span>\''
		),
        array(
			'header'=>'Without E.T.A.',
			'type'=>'raw',
			'value'=>'($data->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL)+$data->countItemsInventory(Individualitem::$STATUS_ORDERED_WITHOUT_ETL)).\' <span class="expand_sold" rel="\'.$data->id.\'" data-status="without_eta">(\'.($data->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL, true)+$data->countItemsInventory(Individualitem::$STATUS_ORDERED_WITHOUT_ETL, true)).\'</span>)\''
		),
        array(
			'header'=>'With E.T.A.',
			'type'=>'raw',
			'value'=>'$data->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETA).\' <span class="expand_sold" rel="\'.$data->id.\'" data-status="with_eta">(\'.$data->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETA, true).\')</span>\''
		),
        array(
			'header'=>'Waiting',
			'type'=>'raw',
			// 'value'=>'$data->countItemsInventory(Individualitem::$STATUS_ORDERED_BUT_WAITING)'
			'value'=>'$data->countItemsInventory(Individualitem::$STATUS_ORDERED_BUT_WAITING).\' <span class="expand_sold" rel="\'.$data->id.\'" data-status="waiting">(\'.$data->countItemsInventory(Individualitem::$STATUS_ORDERED_BUT_WAITING, true).\')</span>\''
		),
		array(
			'header'=>'Final Count',
			'type'=>'raw',
			'value'=>'$data->finalCount()'
		),
        array(
			'header'=>'To Order',
			'type'=>'raw',
			'value'=>'$data->toOrder()'
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
			// 'template'=>'{view}{update}{delete}',
			// 'deleteButtonUrl'=>'Yii::app()->getBaseUrl(true).\'/index.php/item/delete_individual_items_ajax/?item_id=\'.$data->id',
		),
	),
)); ?>

<input type="button" value="Pending Scan Item Count" href="#pendingScanItemCount" id="pendingScanItemCountButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#pendingScanItemCountButton',
    'config'=>array(
		'width'=>400,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="pendingScanItemCount" class="fancy_box_div">
<div id="pendingScanItemCountContent"></div>
<input type="button" value="Close" id="close_pending_scan_div"/>
</div>

</div>

<input type="button" value="Expand Sold Items List" href="#sold_items_list" id="sold_items_list_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#sold_items_list_button',
    'config'=>array(
		'width'=>700,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="sold_items_list" class="fancy_box_div">
<div id="sold_items_list_content"></div>
</div>

</div>

<input type="button" value="Select order to replace item" href="#select_order_replace_list" id="select_order_replace_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#select_order_replace_button',
    'config'=>array(
		'width'=>600,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="select_order_replace_list" class="fancy_box_div">
<div id="select_order_replace_list_content"></div>
</div>

</div>

<script>
jQuery(document).ready(function(){
	jQuery(".pending_scan_count").live("click", function(){
		var item_id = jQuery(this).attr("rel");
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/count_pending_scan_string/?item_id=" + item_id;
		
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery("#pendingScanItemCountContent").html(result);
				jQuery("#pendingScanItemCountButton").trigger("click");
			}
		});
		
		return false;
	});
	
	jQuery("#close_pending_scan_div").live("click", function(){
		jQuery.fancybox.close();
	});
    
    jQuery('.inc_qty').live('click', function(e){
        var itemid = jQuery(e.target).attr('data-itemid');
        adjust_instock(itemid, 'add');
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    
    jQuery('.dec_qty').live('click', function(e){
        var itemid = jQuery(e.target).attr('data-itemid');
        adjust_instock(itemid, 'remove');
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    
    function adjust_instock(item_id, action){
        var cur_qty = parseInt(jQuery('#instock_qty_'+item_id).text());
        
        if (action == 'remove' && cur_qty == 0){
            return;
        }
        
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/adjust_instock_qty_ajax/?item_id=" + item_id + '&act=' + action;
        
        jQuery.ajax({
            url:url,
            dataType:"json",
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                
                if (result.status == "success"){
                    /*if (typeof(result['select_order']) != 'undefined'){
						jQuery('#select_order_replace_list_content').html(result['select_order']);
						jQuery('#select_order_replace_button').trigger('click');
					}*/
                    $.fn.yiiGridView.update('inventory-item-grid');
                }
            }
        });
    }
    
	jQuery('.select_order_to_replace_item').live('click', function(e){
		var target = jQuery(e.target);
		var order_id = target.attr('data-orderid');
		var item_id = target.attr('data-itemid');
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/replace_order_item_ajax/?ind_item_id=" + item_id + '&order_id=' + order_id;
		jQuery.ajax({
            url:url,
            dataType:"json",
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                
                if (result.status == "success"){
					$.fn.yiiGridView.update('inventory-item-grid');
                    jQuery.fancybox.close();
                }
            }
        });
        
        e.preventDefault();
        e.stopImmediatePropagation();
	});
	
    jQuery('.expand_sold').live('click', function(e){
        var target  = jQuery(e.target);
        var status  = target.attr('data-status');
        var item_id = target.attr('rel');
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/sold_items_details_ajax/?item_id=" + item_id + '&status=' + status;
        
        jQuery.ajax({
            url:url,
            dataType:"json",
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                
                if (result.status == "success"){
                    jQuery("#sold_items_list_content").html(result.text);
                    jQuery("#sold_items_list_button").trigger("click");
                    //$.fn.yiiGridView.update('inventory-item-grid');
                }
            }
        });
        
        e.preventDefault();
        e.stopImmediatePropagation();
    });

    jQuery('.scan_item').live('click', function(e){
    	var id = jQuery(e.target).attr('rel');
    	var url = global_baseurl + '/index.php/individualitem/scan_as_out_ajax/?ind_item_id='+id;
    	scanOutWindow(url, function(){$.fn.yiiGridView.update('inventory-item-grid');});
    	
    	e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });

    jQuery('.cancel_item').live('click', function(e){
    	var id = jQuery(e.target).attr('rel');
    	var url = global_baseurl + '/index.php/individualitem/cancel_from_customer_order_ajax/?ind_item_id='+id;
    	cancelShippingWindow(url, function(){$.fn.yiiGridView.update('inventory-item-grid');});
    	
    	e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });

    /*
    jQuery('.cancel_item').live('click', function(e){
        if (!confirm('Are you sure?')){
            return false;
        }
        var target  = jQuery(e.target);
        var item_id = target.attr('rel');
        var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/individualitem/cancel_from_customer_order_ajax/?ind_item_id=" + item_id;
        
        jQuery.ajax({
            url:url,
            dataType:"json",
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                
                if (result.status == "success"){
                    target.text('Cancelled');
                    $.fn.yiiGridView.update('inventory-item-grid');
                }
            }
        });
        
        e.preventDefault();
        e.stopImmediatePropagation();
    });*/

    jQuery('.inventory_order').click(function(e){
		var items = [];
		var suppliers = [];
		var type = jQuery(e.target).attr('rel');
        jQuery('#'+ type +'_table select').each(function(){
			var el = jQuery(this);
			var val = parseInt(el.val());
            if (val){
                items.push(el.attr('rel'));
            	suppliers.push(val);
            }
        });
        
		if (items.length == 0){
			alert('Please select at least one item to order');
			return false;
		}

		if (!confirm('Are you sure?')){
			return false;
		}

		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/order_items_from_inventory_ajax?type="+type;
		jQuery(e.target).attr('disabled','disabled');
		
        jQuery.ajax({
            url:url,
            dataType:"json",
           	type: 'post',
            data: {
                items: items,
                suppliers: suppliers
            },
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    jQuery(e.target).removeAttr('disabled');
                    return false;
                }
                location.reload();
            }
        });
    }); 
});
</script>
