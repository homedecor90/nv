<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_name'); ?>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_code'); ?>
		<?php echo $form->textField($model,'item_code',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'color'); ?>
		<?php echo $form->textField($model,'color',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exw_cost_price'); ?>
		<?php echo $form->textField($model,'exw_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fob_cost_price'); ?>
		<?php echo $form->textField($model,'fob_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shipping_price'); ?>
		<?php echo $form->textField($model,'shipping_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'local_pickup'); ?>
		<?php echo $form->textField($model,'local_pickup'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'la_oc_shipping'); ?>
		<?php echo $form->textField($model,'la_oc_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'canada_shipping'); ?>
		<?php echo $form->textField($model,'canada_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cbm'); ?>
		<?php echo $form->textField($model,'cbm'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dimensions_length'); ?>
		<?php echo $form->textField($model,'dimensions_length',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dimensions_width'); ?>
		<?php echo $form->textField($model,'dimensions_width',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dimensions_height'); ?>
		<?php echo $form->textField($model,'dimensions_height',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'weight'); ?>
		<?php echo $form->textField($model,'weight'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'condition'); ?>
		<?php echo $form->textField($model,'condition',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->