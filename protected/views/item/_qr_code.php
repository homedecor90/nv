<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('individualitem/view_detail', 'id'=>$data->id)); ?>
	<br />

	<img src="<?php echo Yii::app()->getBaseUrl(true).'/images/qrcodes/item_'.$data->id; ?>.png" />
	<br />

</div>