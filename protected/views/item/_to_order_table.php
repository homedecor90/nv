<table class="items" id="<?php echo $not_ordered ? 'not_ordered' : 'to_order'; ?>_table">
	<tr>
		<th>Item</th>
		<th><?php echo $not_ordered ? 'Not Ordered' : 'To Order'; ?></th>
		<?php if(Yii::app()->user->isAdmin()): ?>
		<th>Supplier</th>
		<?php endif; ?>
	</tr>
<?php
$count = 0;
foreach ($items as $item){
	$qty = $not_ordered ?  $item->countItemsInventory(Individualitem::$STATUS_NOT_ORDERED, true) 
			: $item->toOrder(true);
	//$not_ordered = ;
	if ($qty>0){
		echo '<tr>';
		echo '<td>'.$item->item_name.'</td>';
		echo '<td>'.$qty.'</td>';
		if(Yii::app()->user->isAdmin()){
			echo '<td>'.CHtml::dropDownList('supplier_id', '', $supplierList,array('rel'=>$item->id)).'</td>';
		}
		echo '</tr>';
		$count++;
	} 
}
?>
</table>
<?php if(Yii::app()->user->isAdmin() && $count): ?>
<input type="button" class="inventory_order" rel="<?php echo $not_ordered ? 'not_ordered' : 'to_order'; ?>" value="Order" />
<br /><br />
<?php endif; ?>