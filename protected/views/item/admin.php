<?php
/*$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Manage',
);*/

$this->menu=array(
	array('label'=>'Create Item', 'url'=>array('create')),
	array('label'=>'Waiting Items', 'url'=>array('admin_waiting')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Items</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'item_name',
		'item_code',
		'color',
		'exw_cost_price',
		'fob_cost_price',
		'sale_price',
		//'description',
		array(
			'header'=>'Total',
			'value'=>'$data->sale_price + $data->shipping_price'
		),
		array(
			'class'=>'CButtonColumn',
			// 'template'=>'{view}{update}',
            'template'=>Yii::app()->user->isAdmin()?'{view}{update}{delete}':'{view}{update}',
			'deleteButtonUrl'=>'Yii::app()->getBaseUrl(true).\'/index.php/item/delete_item_full_ajax/?item_id=\'.$data->id',
		),
	),
)); ?>
