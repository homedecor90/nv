<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'item_name'); ?>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'item_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_main_name'); ?>
		<?php echo $form->textField($model,'item_main_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'item_main_name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'item_code'); ?>
		<?php echo $form->textField($model,'item_code',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'item_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'color'); ?>
		<?php echo $form->textField($model,'color',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'color'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price'); ?>
		<?php echo $form->error($model,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exw_cost_price'); ?>
		<?php echo $form->textField($model,'exw_cost_price'); ?>
		<?php echo $form->error($model,'exw_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fob_cost_price'); ?>
		<?php echo $form->textField($model,'fob_cost_price'); ?>
		<?php echo $form->error($model,'fob_cost_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shipping_price'); ?>
		<?php echo $form->textField($model,'shipping_price'); ?>
		<?php echo $form->error($model,'shipping_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'local_pickup'); ?>
		<?php echo $form->textField($model,'local_pickup'); ?>
		<?php echo $form->error($model,'local_pickup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'la_oc_shipping'); ?>
		<?php echo $form->textField($model,'la_oc_shipping'); ?>
		<?php echo $form->error($model,'la_oc_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'canada_shipping'); ?>
		<?php echo $form->textField($model,'canada_shipping'); ?>
		<?php echo $form->error($model,'canada_shipping'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cbm'); ?>
		<?php echo $form->textField($model,'cbm'); ?>
		<?php echo $form->error($model,'cbm'); ?>
	</div>
<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'dimensions_length'); ?>
		<?php echo $form->textField($model,'dimensions_length',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dimensions_length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dimensions_width'); ?>
		<?php echo $form->textField($model,'dimensions_width',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dimensions_width'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dimensions_height'); ?>
		<?php echo $form->textField($model,'dimensions_height',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dimensions_height'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'weight'); ?>
		<?php echo $form->textField($model,'weight'); ?>
		<?php echo $form->error($model,'weight'); ?>
	</div>
*/?>	
	<div class="row">
		<?php echo $form->labelEx($model,'box_qty'); ?>
		<?php echo $form->dropDownList($model,'box_qty', array(1=>1, 2=>2, 3=>3, 4=>4, 5=>5), array('id'=>'item_box_qty')); ?>
		<?php echo $form->error($model,'box_qty'); ?>
	</div>
	
	<div id="box_dims">
		<div id="box_dims_1">
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_1_height'); ?>
				<?php echo $form->textField($model->box,'box_1_height'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_1_width'); ?>
				<?php echo $form->textField($model->box,'box_1_width'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_1_length'); ?>
				<?php echo $form->textField($model->box,'box_1_length'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_1_weight'); ?>
				<?php echo $form->textField($model->box,'box_1_weight'); ?>
			</div>
			<div class="clear">
			</div>
		</div>
		<div id="box_dims_2">
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_2_height'); ?>
				<?php echo $form->textField($model->box,'box_2_height'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_2_width'); ?>
				<?php echo $form->textField($model->box,'box_2_width'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_2_length'); ?>
				<?php echo $form->textField($model->box,'box_2_length'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_2_weight'); ?>
				<?php echo $form->textField($model->box,'box_2_weight'); ?>
			</div>
			<div class="clear">
			</div>
		</div>
		<div id="box_dims_3">
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_3_height'); ?>
				<?php echo $form->textField($model->box,'box_3_height'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_3_width'); ?>
				<?php echo $form->textField($model->box,'box_3_width'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_3_length'); ?>
				<?php echo $form->textField($model->box,'box_3_length'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_3_weight'); ?>
				<?php echo $form->textField($model->box,'box_3_weight'); ?>
			</div>
			<div class="clear">
			</div>
		</div>
		<div id="box_dims_4">
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_4_height'); ?>
				<?php echo $form->textField($model->box,'box_4_height'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_4_width'); ?>
				<?php echo $form->textField($model->box,'box_4_width'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_4_length'); ?>
				<?php echo $form->textField($model->box,'box_4_length'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_4_weight'); ?>
				<?php echo $form->textField($model->box,'box_4_weight'); ?>
			</div>
			<div class="clear">
			</div>
		</div>
		<div id="box_dims_5">
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_5_height'); ?>
				<?php echo $form->textField($model->box,'box_5_height'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_5_width'); ?>
				<?php echo $form->textField($model->box,'box_5_width'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_5_length'); ?>
				<?php echo $form->textField($model->box,'box_5_length'); ?>
			</div>
			<div class="col">
				<?php echo $form->labelEx($model->box,'box_5_weight'); ?>
				<?php echo $form->textField($model->box,'box_5_weight'); ?>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model, 'image_file'); ?>
		<?php echo $form->fileField($model, 'image_file'); ?>
		<?php echo $form->error($model, 'image_file'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'image_url'); ?>
		<?php echo $form->textField($model,'image_url',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'image_url'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

<?php if (!$model->isNewRecord && YumUser::isOperator(true)): ?>
<div id="item_related_items_form" style="width: 700px;">
	<hr />
	<h4>Auto generated related items</h4>
	<?php 
	foreach($related_items_auto as $item){
		echo $item->item_name.'<br />';
	}  
	?>
	<br /><br />
	<h4>Manually selected related items</h4>
	<?php 
	
	echo '<strong style="float: left;">Related items</strong>';
	echo '<strong style="float: right;">All items</strong>';
	echo '<div class="clear"></div>';
	echo CHtml::dropDownList('related_items', '', CHtml::listData($related_items_manual, 'id', 'item_name'), array('multiple'=>'multiple','size'=>10, 'style'=>'width: 50%;'));
	echo CHtml::dropDownList('all_items', '', CHtml::listData($all_items, 'id', 'item_name'), array('multiple'=>'multiple','size'=>10, 'style'=>'width: 50%;'));
	echo CHtml::button('Remove >', array('id'=>'remove_related'));
	echo CHtml::button('< Add', array('id'=>'add_related','style'=>"float: right;"));
	echo '<div class="clear"></div>';
	?>
</div>
<?php endif; ?>
</div><!-- form -->

<script type="text/javascript">
<?php if (!$model->isNewRecord && YumUser::isOperator(true)): ?>

var current_id = <?php echo $model->id; ?>;

jQuery('#add_related').click(function(e){
	var ids = [];
	var options = jQuery('#all_items option:selected');
	options.each(function(){
		ids.push(jQuery(this).val());
	});
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/add_related_items_ajax/";
	jQuery.ajax({
		url:url,
		dataType:"json",
		data: {
			item_id: current_id,
			related_id: ids
		},
		type: 'post',
		success:function(result){
			options.each(function(){
				jQuery(this).clone().appendTo(jQuery('#related_items'));
			});
		}
	});
});

jQuery('#remove_related').click(function(e){
	var ids = [];
	var options = jQuery('#related_items option:selected');
	options.each(function(){
		ids.push(jQuery(this).val());
	});
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/item/remove_related_items_ajax/";
	jQuery.ajax({
		url:url,
		dataType:"json",
		data: {
			item_id: current_id,
			related_id: ids
		},
		type: 'post',
		success:function(result){
			options.remove();
		}
	});
});

<?php endif; ?>

function box_qty_change(){
	var qty = jQuery('#item_box_qty').val();
	jQuery('#box_dims>div').hide();
	var selector = '';
	for (var i=1; i<=qty; i++){
		selector += '#box_dims_' + i + (i==qty ? '' : ',');
	}
	jQuery(selector).show(500);
}

box_qty_change();

jQuery(document).ready(function(){
	jQuery('#item_box_qty').change(box_qty_change);
});
</script>