<?php
/*$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->id,
);*/

$this->menu=array(
	array('label'=>'Create Item', 'url'=>array('create')),
	array('label'=>'Update Item', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Manage Item', 'url'=>array('admin')),
	// array('label'=>'Print QR Codes', 'url'=>array('qrcodes', 'id'=>$model->id)),
);
?>

<h1>View Item #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_name',
		'item_main_name',
		'item_code',
		'color',
		'sale_price',
		'exw_cost_price',
		'fob_cost_price',
		'shipping_price',
		'local_pickup',
		'la_oc_shipping',
		'canada_shipping',
		'cbm',
        array(
            'label'=>'Boxes',
            'type'=>'raw',
            'value'=>$model->getBoxDimsStr()
        ),
		'description',
		'condition',
        array(
            'label'=>'In Stock',
            'type'=>'number',
            'value'=>Item::model()->getCountInStock($model->id)
        ),
        array(
            'label'=>'Ordered With ETA',
            'type'=>'number',
            'value'=>Item::model()->getCountIncoming($model->id)
        ),
        array(
            'label'=>'Not Ordered',
            'type'=>'number',
            'value'=>Item::model()->getCountNotOrdered($model->id)
        ),
		array(
			'label'=>'Image',
			'type'=>'raw',
			'value'=>'<img width="300px" src="'.Yii::app()->getBaseUrl(true).'/'.$model->image.'"></img>'
		),
		array(
				'label'=>'Image URL',
				'type'=>'raw',
				'value'=>'<img width="300px" src="'.$model->image_url.'"></img>'
		),
		array(
				'label'=>'URL',
				'type'=>'raw',
				'value'=>'<a href="'.$model->url.'">'.$model->url.'</a>'
		),
	),
)); ?>
