<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<?php
	$order_id = $customerOrderModel->id;

	$strCustomerCompanyInfo = $customerOrderModel->getBillingInfo();
		
	$price = CustomerOrder::model()->getCustomerOrderTotalPrice($order_id, true);
	$order_total_price  = number_format($price['total_with_tax'], 2);
	$order_tax          = number_format($price['tax'], 2);
	$item_list_rows = CustomerOrder::model()->getCustomerOrderItemsForInvoicePdf($order_id);
    $settings = Settings::model()->getSettingVariables();
    $tax_percent = number_format($settings->tax_percent, 2);
?>

<html>
	<style>
		p{margin:3px 0;}
		h2{margin:3px 0;}
		div.invoice_content{margin:50px 0}
		td{padding:0;}
		
		table.item_list table td{padding:10px;}
		/*table.item_list table td.item_name{width:70%; border-right:1px solid #888;}
		table.item_list table td.item_price{width:30%;}*/
	</style>
    
	<body>
		<div class="invoice_content">
			<br/><br/><br/>
			<table cellspacing="0" valign="middle" width="100%">
				<tr>
					<td style="width:8%;"></td>
					<td style="width:40%;"><h2><?php echo Settings::getVar('company_name'); ?></h2></td>
					<td style="width:40%; "><p><b>Date of Invoice:</b> <?php echo date("F d, Y", strtotime($customerOrderModel->created_on));?></p></td>
					<td style="width:12%"></td>
				</tr>
				
				<tr>
					<td style="width:8%;"></td>
					<td style="width:40%;"><p><?php echo Settings::getVar('company_address_street'); ?></p></td>
					<td style="width:52%; "></td>
				</tr>
				
				<tr>
					<td style="width:8%;"></td>
					<td style="width:40%;"><p><?php echo Settings::getVar('company_address_city').', '.Settings::getVar('company_address_state').' '.Settings::getVar('company_address_zip'); ?></p></td>
					<td style="width:40%; "><p><b>Invoice No.</b>  <?php printf("%010s", $order_id);?></p></td>
					<td style="width:12%"></td>
				</tr>
				
				<tr>
					<td style="width:8%;"></td>
					<td style="width:40%;"><p><?php echo Settings::getVar('sales_phone_number'); ?></p></td>
					<td style="width:52%; "></td>
				</tr>	
			</table>
			<br/><br/>
			<table cellspacing="0" valign="middle" width="100%">
				<tr>
					<td style="width:7%;"></td>
					<td style="width:35%;"><table valign="middle" style="margin:0; width:100%;border:1px solid #888;padding:5% 10px">
						<tr>
							<td style="width:5%;"></td>
							<td style="width:90%; padding:10px 0;">
								<?php echo $strCustomerCompanyInfo;?>
							</td>
							<td style="width:5%;"></td>
						</tr>
					</table></td>
					<td style="width:58%;"></td>
				</tr>
				<tr>
					<td style="width:7%;"></td>
					<td style="width:35%;"><table valign="middle" style="margin:0; width:100%;border-left:1px solid #888; border-right:1px solid #888;padding:5% 10px">
						<tr>
							<td style="width:100%;"><br/><br/><br/></td>
						</tr>
					</table></td>
					<td style="width:58%;"></td>
				</tr>
			</table>
			<table cellspacing="0" valign="middle" width="100%" class="item_list">
				<tr>
					<td style="width:7%;"></td>
					<td style="width:80%;">
                        <table valign="middle" style="margin:0; width:100%;border:1px solid #888;" cellpadding=0 cellspacing=0>
                            <tr>
                                <td style="border-bottom:1px solid #888" colspan="3"><b>Invoice</b></td>
                            </tr>
                            <tr>
                                <td class="item_name" style="border-bottom:1px solid #888">Item</td>
                                <td style="border-bottom:1px solid #888">Special Shipping</td>
                                <td class="item_price" style="border-bottom:1px solid #888">Total</td>
                            </tr>
                            <?php echo $item_list_rows;?>
                            <?php
                            if ($order_tax):
                            ?>
                            <tr>
                                <td class="item_name" style="border-top:1px solid #888">Sales Tax at <?php echo $tax_percent; ?>%</td>
                                <td style="border-top:1px solid #888"></td>
                                <td class="item_price" style="border-top:1px solid #888">$<?php echo $order_tax; ?></td>
                            </tr>
                            <?php
                            endif;
                            ?>
                            <tr>
                                <td class="item_name" style="border-top:1px solid #888">Total</td>
                                <td style="border-top:1px solid #888"></td>
                                <td class="item_price" style="border-top:1px solid #888">$<?php echo $order_total_price; ?></td>
                            </tr>
                            <?php
                            if ($customerOrderModel->discounted == 'Discounted'):
                            ?>
                            <tr>
                                <td class="item_name" style="border-top:1px solid #888">Discounted total</td>
                                <td style="border-top:1px solid #888"></td>
                                <td class="item_price" style="border-top:1px solid #888">$<?php echo number_format($customerOrderModel->discounted_total, 2); ?></td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </table>
                    </td>
					<td style="width:13%;"></td>
				</tr>
			</table>
			<br/>
			<table cellspacing="0" valign="middle" width="100%" align="center" class="item_list">
				<tr>
					<td style="width:20%;"></td>
					<td style="width:60%;"><h4>*Prepayment is required for all orders to be processed.</h4></td>
					<td style="width:20%;"></td>
				</tr>
			</table>
			<br/>
			<br/>
			<table cellspacing="0" valign="middle" width="100%" align="center" class="item_list">
				<tr>
					<td style="width:20%;"></td>
					<td style="width:60%;">
						<ul>
							<li><b>California residents add <?php echo $tax_percent;?>% sales tax.</b></li>
							<li><b>Resellers are not required to pay sales tax.</b></li>
							<li><b>Price valid for 7 days from the invoice date.</b></li>
						</ul>
					</td>
					<td style="width:20%;"></td>
				</tr>
			</table>
		</div>
    </body>
</html>