<style>
	#items{
		display: block;
		width: 50%;
		margin: 10px auto;
		text-align: center;
	}
	
	#items td{
		padding: 2px 5px;
	}
	
	h4{
		text-align: center;
	}
	
	div{
		font-family: times;
	}
</style>

<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
	<div>
		<h4><?php echo $company_name; ?>, Payment Authorization</h4>
		<p>
		I hereby authorize Authorize.net (the "payment processor") to process a payment of $<?php echo number_format($shipment->calculateTotalPrice(), 2); ?> 
		behalf of <?php echo $company_name; ?>, for the following items:
		</p>
		<table id="items" cellpadding="10" align="center" border="0">
			<tr>
				<td>Quantity</td>
				<td>Item Name</td>
			</tr>
			<?php
				foreach ($items as $item){
					echo '<tr>';
					echo '<td>'.$item['qty'].'</td>';
					echo '<td>'.$item['name'].'</td>';
					echo '</tr>';
				}
			?>
		</table>
		
		<p>
		I certify that I am the owner of any credit card and bank account associated with my accounts at 
		these payment processors. I assume responsibility for all payments made by me. I understand 
		that all payments are considered final. I will not cause reversals of any payments through these 
		payment processors, my credit card issuer, bank or by any other means, for any reason, 
		including, but not limited to, courtesy charges. I agree to reimburse <?php echo $company_name; ?> for any losses
		incurred due to such a payment reversal. 
		</p>
		<p>
		Credit card: <br />
		I do state that I am the cardholder. My name is <u><?php echo $order->getBillingName(); ?></u><br />
		(print name as it appears on your card) <br />
		I accept this agreement and certify the authenticity of the information provided. 
		</p>
		<br />
		<br />
		<br />
		____________<br />
		Signature 
		<br />
		<br />
		<br />
		<?php echo date('M d, Y'); ?>
	</div>
</page>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
	<div>
		<h4>LOCAL PICK UP AGREEMENT</h4>
		<p>
		I, <?php echo $order->getBillingName(); ?>, agree to purchase the furniture items listed under LIST A, for a 
		total price of $<?php echo number_format($shipment->calculateTotalPrice(), 2); ?> from <?php echo $company_name; ?>.
		</p>
		<h4>LIST A</h4>
		<table id="items" cellpadding="10" align="center" border="0">
			<tr>
				<td>Quantity</td>
				<td>Item Name</td>
			</tr>
			<?php
				foreach ($items as $item){
					echo '<tr>';
					echo '<td>'.$item['qty'].'</td>';
					echo '<td>'.$item['name'].'</td>';
					echo '</tr>';
				}
			?>
		</table>
		<p>
		I acknowledge and agree that I am picking up the items listed above from a warehouse, and not a 
		showroom. 
		</p>
		<p>
		I acknowledge and agree that <strong>ALL SALES ARE FINAL, NO REFUNDS</strong> are issued once the 
		item has been paid for.
		</p>
		<p>
		I acknowledge and agree that it is my responsibility to inspect the items after payment, but prior 
		to loading. 
		</p>
		<p>
		I acknowledge and agree that if the item is found damaged during this inspection, it can be 
		exchanged for a similar item.
		</p>
		<p>
		I acknowledge and agree that exchanges will <strong>only</strong> be made if the item is damaged or has a 
		manufacturing defect. 
		</p>
		<p>
		<strong>I acknowledge and agree that warehouse service charge ($<?php echo number_format($shipment->total_shipping_price, 2); ?>) is non-refundable, even 
		if I refuse loading/purchase for any reason whatsoever.</strong>
		</p>
		<p>
		I acknowledge and agree that no exchanges, refunds or credits will be issued at any time, on any 
		item, after I have initially inspected and loaded the items as stated above. 
		</p>
		<br />
		<br />
		<br />
		____________<br />
		Signature 
		<br />
		<br />
		<br />
		<?php echo date('M d, Y'); ?>
	</div>
</page>