<style>
	div.label{
		font-family: times;
		border-bottom: 1px solid #000;
		height: 205pt;
		position: relative;
	}
	
	div.top_left{
		position: absolute;
		left: 0;
		top: 5pt;
	}
	
	div.bottom_left{
		position: absolute;
		left: 0;
		bottom: 5pt;
	}
	
	div.top_right{
		position: absolute;
		right: 0;
		top: 5pt;
	}
	
	div.name{
		font-size: 20pt;
		margin-top: 40pt;
		text-align: center;
	}
	
	div.address{
		font-size: 20pt;
		line-height: 120%;
		margin-top: 20pt;
		text-align: center;
	}
	
	div.phone{
		margin-top: 10pt;
		fond-size: 15pt;
		text-align: center;
	}
</style>

<?php

$box_i = 0;
?>
<page backtop="0" backbottom="0" backleft="0" backright="0">
<?php
foreach ($shipments as $shipment):
foreach ($shipment->active_ind_items as $item):
	for ($i = 1; $i<=$item->items->box_qty; $i++){
		echo '<div class="label">';
		echo '<div class="top_left">';
			echo 	'Furniture<br />
					'.Settings::getVar('company_address_street').'<br />
					'.Settings::getVar('company_address_city').', '.Settings::getVar('company_address_state').' '.Settings::getVar('company_address_zip').'<br />
					PHONE: '.Settings::getVar('customer_service_phone_number').'<br />
					<br />
					Insurance/Value: $600';
					
		echo '</div>';
		echo '<div class="bottom_left">';
			echo 'Box '.$i.' of '.$item->items->box_qty.' for item #'.$item->id.' ('.$item->items->item_code;
			if ($item->customer_order_items->custom_color){
				echo ', '.$item->customer_order_items->custom_color;
			}
			echo ')';
		echo '</div>';
		echo '<div class="top_right">';
			echo 'Shipper: ';
			echo $shipment->selected_user_bid->shipper->profile->company;
			echo '<br /><br />';
			echo 'QUOTE NUMBER: '.$shipment->selected_user_bid->rate_quote_number.'<br />';
			echo 'Pre-Paid';
		echo '</div>';
		echo '<div class="name">';
		echo $shipment->customer_order->billing_info->shipping_name.' '.$shipment->customer_order->billing_info->shipping_last_name;
		echo '</div>';
		echo '<div class="address">';
			echo $shipment->customer_order->billing_info->shipping_street_address;
            echo '<br />'.$shipment->customer_order->billing_info->shipping_city.', ';
            echo $shipment->customer_order->billing_info->shipping_state.', ';
            echo $shipment->customer_order->billing_info->shipping_zip_code;
		echo '</div>';
		echo '<div class="phone">';
            echo 'Phone: '.$shipment->customer_order->billing_info->shipping_phone_number;
		echo '</div>';
		echo '</div>';
		//~ $box_i++;
		//~ if ($box_i%4 == 0){
			//~ echo '</page>';
			//~ echo '<page>';
		//~ }
	}
endforeach;
endforeach;

?>
</page>
