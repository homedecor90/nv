<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<style>
		#order_table
        {
            font-size: 10px;
        }
        
        #order_table td, #order_table th
        {
            padding: 3px;
            text-align: center;
        }
	</style>
    
	<body>
        <h1>Supplier Order Details</h1>
        
        <strong>Date: </strong> <?php echo date("M. d, Y H:i:s", time()); ?><br />
        <strong>Supplier: </strong> <?php echo $order->supplier->profile->firstname.' '.$order->supplier->profile->lastname; ?><br />
        <strong>Total EXW Price: </strong>$<?php echo SupplierOrder::model()->getSupplierOrderTotalExwPrice($order->id); ?><br />
        <strong>Total FOB Price: </strong>$<?php echo SupplierOrder::model()->getSupplierOrderTotalFobPrice($order->id); ?><br />
        <?php if ($order->status != SupplierOrder::$STATUS_PENDING && $order->status != SupplierOrder::$STATUS_ACCEPTED): ?>
        <strong>Advanced Paid: </strong>$<?php echo $order->advanced_paid_amount; ?><br /><br />
		<?php endif; ?>
        
        <table id="order_table" border="1" style="border-collapse: collapse;">
            <tr>
                <?php if (YumUser::isOperator(true)): 
                $cells = 11;
                ?>
                <th>
                    Item name
                </th>
                <?php else: 
                $cells = 10;
                endif;
                ?>
                <th>
                    Item code
                </th>
                <th>
                    Item color
                </th>
                <th>
                    Custom color
                </th>
                <th>
                    EXW Price
                </th>
                <th>
                    FOB Price
                </th>
                <th>
                    CBM
                </th>
                <th>
                    Qty
                </th>
                <th>
                    Total EXW Price
                </th>
                <th>
                    Total FOB Price
                </th>
                <th>
                    Total CBM
                </th>
            </tr>
            <?php
            foreach ($order_items as $i=>$items){
            	if ($items->is_replacement && (i==0 || !$order_items[$i-1]->is_replacement)){
            		echo '<tr><td colspan="'.$cells.'">Replacements</td></tr>';
            	}
            		
            	if ($items->not_loaded_reordered && (!$order_items[$i-1]->not_loaded_reordered || i==0)){
            		echo '<tr><td colspan="'.$cells.'">Not Loaded Previous Time</td></tr>';
            	}
            	
            	echo '<tr>';
            	if (YumUser::isOperator(true)){
                	echo '<td>'.$items->items->item_name.'</td>';
            	}
                echo '<td>'.$items->items->item_code.'</td>';
                echo '<td>'.$items->items->color.'</td>';
                echo '<td>'.$items->custom_color.'</td>';
                echo '<td>'.$items->exw_cost_price.'</td>';
                echo '<td>'.$items->fob_cost_price.'</td>';
                echo '<td>'.$items->items->cbm.'</td>';
                echo '<td>'.$items->quantity.'</td>';
                echo '<td>'.(floatval($items->exw_cost_price) * floatval($items->quantity)).'</td>';
                echo '<td>'.(floatval($items->fob_cost_price) * floatval($items->quantity)).'</td>';
                echo '<td>'.$items->getTotalCBM().'</td>';
                echo '</tr>';
            }
            ?>
        </table>
    </body>
</html>