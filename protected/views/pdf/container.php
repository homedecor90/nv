<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <style>
        td.label
        {
            padding-right: 20px;
            font-weight: bold;
        }
    </style>
	<body>
        <h1>Supplier Order Container Info</h1>
        
        <strong>Date: </strong> <?php echo date("M. d, Y H:i:s", time()); ?><br /><br />
        <table border="0">
            <tr>
                <td class="label">Container ID: </td>
                <td><?php echo $model->id; ?></td>
            </tr>
            <tr>
                <td class="label">Company: </td>
                <td><?php echo $model->shipper->profile->firstname.' '.$model->shipper->profile->lastname; ?></td>
            </tr>
            <tr>
                <td class="label">Total Suppliers: </td>
                <td><?php echo $model->getSupplierCount(); ?></td>
            </tr>
            <tr>
                <td class="label">Total CBM: </td>
                <td><?php echo $model->getTotalCBM(); ?></td>
            </tr>
            <tr>
                <td class="label">Price per CBM: </td>
                <td><?php echo number_format($model->getTotalCBM() ? $model->getShippingPrice()/$model->getTotalCBM() : 0, 2); ?></td>
            </tr>
            <tr>
                <td class="label">Container Size: </td>
                <td><?php echo $model->getChosenBid()->container_size; ?></td>
            </tr>
            <tr>
                <td class="label">ETA: </td>
                <td><?php echo date("M, d. Y", strtotime($model->getChosenBid()->bid_eta)); ?></td>
            </tr>
            <tr>
                <td class="label">Picked Up: </td>
                <td><?php echo $model->picked_up_date ? date("M, d. Y", strtotime($model->picked_up_date)) : ''; ?></td>
            </tr>
            <tr>
                <td class="label">Term - Cost: </td>
                <td><?php echo $model->terms.' - $'.$model->getShippingPrice(); ?></td>
            </tr>
        </table>
    </body>
</html>