<style>
	.items{
		_border-collapse: collapse;
		text-aling: left;
	}
	
	.items td, .items th{
		padding: 5px 6px;
	}
	
	h2,h3,h4{
		text-align: center;
		margin-top: 0;
	}
	
	.head{
		margin-bottom: 0;
	}
	
	div{
		font-family: times;
	}
	
	table {
		width: 100%;
	}
</style>

<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 11pt">
<table align="center" border="1" class="items">
		<tr>
			<th>Shipping Name</th>
			<th>Qty</th>
			<th>Item Code</th>
			<th>Color</th>
		</tr>
<?php
	foreach ($shipments as $sh_i=>$shipment){
?>
	<!-- strong>Total Number of Items:</strong> <?php echo count($shipment->active_ind_items); ?><br /><br /-->
	

	<?php
		$by_id = array();
		foreach ($shipment->active_ind_items as $j=>$item){
			$by_id[$item->item_id.$item->customer_order_items->custom_color][] = $item;
		}

		foreach ($by_id as $id=>$items){
			$custom_color = $items[0]->customer_order_items->custom_color;
			echo '<tr>';
			
			echo '<td>';
			echo $shipment->customer_order->billing_info->shipping_name.' '.$shipment->customer_order->billing_info->shipping_last_name;
			echo '</td>';
			
			echo '<td>';
			echo count($items);
			echo '</td>';
			
			echo '<td>';
			echo $items[0]->items->item_code;
			echo '</td>';
			
			echo '<td>';
			echo $items[0]->items->color;
			if ($custom_color){
				echo ' ('.$custom_color.')';
			}
			echo '</td>';
			
			echo '</tr>';
		}
	?>
	
<?php
	}
?>
</table>
</page>
