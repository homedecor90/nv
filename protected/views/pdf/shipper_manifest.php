<style>
	.items{
		_border-collapse: collapse;
		text-aling: left;
	}
	
	.items td, .items th{
		padding: 5px 6px;
	}
	
	h2,h3,h4{
		text-align: center;
		margin-top: 0;
	}
	
	.head{
		margin-bottom: 0;
	}
	
	div{
		font-family: times;
	}
</style>

<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 11pt">
	<h2>Manifest for <?php echo $shipper->profile->company;?></h2>
	<strong>Pickup Date:</strong> <?php echo $date; ?><br />
	<strong>Total Number of Shipments:</strong> <?php echo count($shipments); ?><br />
	<?php
	$box_qty = 0;
	foreach ($shipments as $shipment){
		$box_qty += $shipment->getBoxQty();
	}
	?>
	<strong>Total Number of Boxes:</strong> <?php echo $box_qty; ?><br /><br />
<?php
	foreach ($shipments as $sh_i=>$shipment){
?>
		<?php
		if ($sh_i != 0){
			echo '<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 11pt">';
		}
		?>
				
		<table align="center" border="1" class="items">
			<?php
			foreach ($shipment->active_ind_items as $j=>$item){
				$box_qty = $shipment->getBoxQty();
				for($i = 1; $i<=$item->items->box_qty; $i++){
					echo '<tr>';
					if ($j == 0 && $i==1){
						echo '<td rowspan="'.$box_qty.'">';
						echo $shipment->customer_order->billing_info->shipping_name.' '.$shipment->customer_order->billing_info->shipping_last_name;
						echo '<br /><br />';
						echo $shipment->customer_order->getShippingAddressStr();
						echo '<br /><br />';
						echo 'Total number of boxes: '.$box_qty;
						echo '</td>';
						
						echo '<td rowspan="'.$box_qty.'">';
						echo $shipment->selected_user_bid->rate_quote_number.' - $'.$shipment->selected_user_bid->rate_quote;
						echo '</td>';
					}
					$width = 'box_'.$i.'_width';
					$height = 'box_'.$i.'_height';
					$length = 'box_'.$i.'_length';
					$weight = 'box_'.$i.'_weight';
					
					echo '<td>'.$item->items->item_code.'</td>';
					echo '<td>'.$item->items->box->$width.' x '.$item->items->box->$height.' x '.$item->items->box->$length.'</td>';
					echo '<td>'.$item->items->box->$weight.' lbs</td>';
					echo '</tr>';
				}
			}
			?>
		</table>
		</page>
<?php
	}
?>
