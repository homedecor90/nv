<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<style>
		#inventory_table
        {
            font-size: 10px;
        }
        
        #inventory_table td, #inventory_table th
        {
            padding: 3px;
            text-align: center;
        }
        
        #inventory_table td.name
        {
            text-align: left;
            width: 140px;
        }
        
        #inventory_table td.code
        {
            width: 80px;
        }
	</style>
    
	<body>
        <h1>Inventory</h1>
        
        <strong>Date: </strong> <?php echo date("M. d, Y H:i:s", time()); ?><br /><br />
        
        <table id="inventory_table" border="1" style="border-collapse: collapse;">
            <tr>
                <th>
                    Item name
                </th>
                <th>
                    Item code
                </th>
                <th>
                    Item color
                </th>
                <th>
                    In<br />Stock
                </th>
                <th>
                    Sold <br />Not Ordered
                </th>
                <th>
                    Without<br />E.T.A.
                </th>
                <th>
                    With<br />E.T.A
                </th>
                <th>
                    Waiting
                </th>
                <th>
                    Final<br />Count
                </th>
                <th>
                    To<br />Order
                </th>
            </tr>
            <?php
            foreach ($data as $row){
                echo '<tr>';
                echo '<td class="name">'.$row->item_name.'</td>';
                echo '<td class="code">'.$row->item_code.'</td>';
                echo '<td>'.$row->color.'</td>';
                echo '<td>'.$row->countItemsInventory(Individualitem::$STATUS_IN_STOCK).' ('.$row->countItemsInventory(Individualitem::$STATUS_IN_STOCK, true).')</td>';
                echo '<td>'.$row->countItemsInventory(Individualitem::$STATUS_NOT_ORDERED).'</td>';
                echo '<td>'.($row->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL)+$row->countItemsInventory(Individualitem::$STATUS_ORDERED_WITHOUT_ETL)).' ('.($row->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETL, true)+$row->countItemsInventory(Individualitem::$STATUS_ORDERED_WITHOUT_ETL, true)).')</td>';
                echo '<td>'.$row->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETA).' ('.$row->countItemsInventory(Individualitem::$STATUS_ORDERED_WITH_ETA, true).')</td>';
                echo '<td>'.$row->countItemsInventory(Individualitem::$STATUS_ORDERED_BUT_WAITING).'</td>';
                echo '<td>'.$row->finalCount().'</td>';
                echo '<td>'.$row->toOrder($row->id).'</td>';
                echo '</tr>';
            }
            ?>
        </table>
    </body>
</html>