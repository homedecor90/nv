<?php
$this->breadcrumbs=array(
	'Url Groups'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UrlGroup', 'url'=>array('index')),
	array('label'=>'Create UrlGroup', 'url'=>array('create')),
	array('label'=>'View UrlGroup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UrlGroup', 'url'=>array('admin')),
);
?>

<h1>Update UrlGroup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>