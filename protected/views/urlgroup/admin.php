<?php
$this->breadcrumbs=array(
	'Url Groups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UrlGroup', 'url'=>array('index')),
	array('label'=>'Create UrlGroup', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('url-group-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Url Groups</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'url-group-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'visible'=>'true',
			'class'=>'CMyCheckBoxColumn',
			'selectableRows' => 2,
		),
		'title',
		array(
			'header'=>'Roles',
			'type'=>'raw',
			'value'=>'UrlGroupRole::model()->getGroupRoleString($data->id)'
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}'
		)
	),
)); ?>

<div class="row">
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>1, 'value'=>UrlGroup::$ROLE_ADMIN)); ?>
	<?php echo UrlGroup::$ROLE_ADMIN; ?>
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>2, 'value'=>UrlGroup::$ROLE_SALES_PERSON)); ?>
	<?php echo UrlGroup::$ROLE_SALES_PERSON; ?>
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>3, 'value'=>UrlGroup::$ROLE_OPERATOR)); ?>
	<?php echo UrlGroup::$ROLE_OPERATOR; ?>
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>4, 'value'=>UrlGroup::$ROLE_SUPPLIER)); ?>
	<?php echo UrlGroup::$ROLE_SUPPLIER; ?>
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>5, 'value'=>UrlGroup::$ROLE_SUPPLIER_SHIPPER)); ?>
	<?php echo UrlGroup::$ROLE_SUPPLIER_SHIPPER; ?>
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>6, 'value'=>UrlGroup::$ROLE_ALL_AUTHENTICATED_USERS)); ?>
	<?php echo UrlGroup::$ROLE_ALL_AUTHENTICATED_USERS; ?>
	<?php echo CHtml::checkBox('', true, array('class'=>'chkUrlGroupRole', 'rel'=>7, 'value'=>UrlGroup::$ROLE_ALL_VISITORS)); ?>
	<?php echo UrlGroup::$ROLE_ALL_VISITORS; ?>
</div>
<br/>
<input type="button" id="update_group" value="Update Group"/>

<script>
jQuery(document).ready(function(){
	jQuery("#update_group").click(function(){
		if (jQuery("td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the url to update group");
			return false;
		}
		
		var url_id_list = "";
		jQuery("td.checkbox-column input[type='checkbox']:checked").each(function(){
			var url_id = jQuery(this).val();
			if (url_id_list == ""){
				url_id_list = url_id;
			} else {
				url_id_list += ("_" + url_id);
			}
		});
		
		var group_role_id_list = "";
		jQuery("input[type='checkbox'].chkUrlGroupRole:checked").each(function(){
			var group_role_id = jQuery(this).attr("rel");
			if (group_role_id_list == ""){
				group_role_id_list = group_role_id;
			} else {
				group_role_id_list += ("_" + group_role_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/urlgroup/update_url_group/?"
			+ "url_id_list=" + url_id_list + "&group_role_id_list=" + group_role_id_list;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				if (result.message != "success"){
					alert(result.error);
					return false;
				}
				
				jQuery.fn.yiiGridView.update('url-group-grid');
			}
		});
	});
});
</script>