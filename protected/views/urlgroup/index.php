<?php
$this->breadcrumbs=array(
	'Url Groups',
);

$this->menu=array(
	array('label'=>'Create UrlGroup', 'url'=>array('create')),
	array('label'=>'Manage UrlGroup', 'url'=>array('admin')),
);
?>

<h1>Url Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
