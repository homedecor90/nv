<?php
$this->breadcrumbs=array(
	'Url Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UrlGroup', 'url'=>array('index')),
	array('label'=>'Manage UrlGroup', 'url'=>array('admin')),
);
?>

<h1>Create UrlGroup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>