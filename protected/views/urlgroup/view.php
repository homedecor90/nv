<?php
$this->breadcrumbs=array(
	'Url Groups'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List UrlGroup', 'url'=>array('index')),
	array('label'=>'Create UrlGroup', 'url'=>array('create')),
	array('label'=>'Update UrlGroup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UrlGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UrlGroup', 'url'=>array('admin')),
);
?>

<h1>View UrlGroup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
	),
)); ?>
