<?php
/**
 * @var $this CustomerNewsletterController
 * @var $model CustomerNewsletter
 */
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
   'id' => 'customerNewsletter-grid',
   'dataProvider' => $model->search(),
   'columns' => array(
       'id',
       'date_added',
       'pending',
       'sent',
       'unsubscribed',
   )
));