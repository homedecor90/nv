<?php
$this->breadcrumbs=array(
	'Url Permissions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create UrlPermission', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('url-permission-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Url Permissions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'url-permission-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'visible'=>'true',
			'class'=>'CMyCheckBoxColumn',
			'selectableRows' => 2,
		),
		'id',
		'title',
		'url',
		array(
			'header'=>'Roles',
			'type'=>'raw',
			'value'=>'UrlPermission::model()->getPermissionRoleString($data->id)'
		),
		array(
			'header'=>'Url Group',
			'type'=>'raw',
			'value'=>'UrlPermission::model()->getPermissionGroupString($data->id)'
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}'
		)
	),
)); ?>

<div class="row">
	<?php 
		foreach ($urlGroupList as $url_group){
	?>
			<?php echo CHtml::checkBox('', false, array('class'=>'chkUrlGroup', 'rel'=>$url_group->id, 'value'=>$url_group->id)); ?>
			<?php echo $url_group->title; ?>
			<br/>
	<?php
		}
	?>
</div>
<br/>
<input type="button" id="update_permission" value="Update Permission"/>

<script>
jQuery(document).ready(function(){
	jQuery("#update_permission").click(function(){
		if (jQuery("td.checkbox-column input[type='checkbox']:checked").length == 0){
			alert("Please select the url to update permission");
			return false;
		}
		
		var url_id_list = "";
		jQuery("td.checkbox-column input[type='checkbox']:checked").each(function(){
			var url_id = jQuery(this).val();
			if (url_id_list == ""){
				url_id_list = url_id;
			} else {
				url_id_list += ("_" + url_id);
			}
		});
		
		var permission_group_id_list = "";
		jQuery("input[type='checkbox'].chkUrlGroup:checked").each(function(){
			var permission_group_id = jQuery(this).attr("rel");
			if (permission_group_id_list == ""){
				permission_group_id_list = permission_group_id;
			} else {
				permission_group_id_list += ("_" + permission_group_id);
			}
		});
		
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/urlpermission/update_url_permission/?"
			+ "url_id_list=" + url_id_list + "&permission_group_id_list=" + permission_group_id_list;
		
		jQuery.ajax({
			url:url,
			dataType:"json",
			success:function(result){
				jQuery.fn.yiiGridView.update('url-permission-grid');
			}
		});
	});
});
</script>