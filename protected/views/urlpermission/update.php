<?php
$this->breadcrumbs=array(
	'Url Permissions'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create UrlPermission', 'url'=>array('create')),
	array('label'=>'Manage UrlPermission', 'url'=>array('admin')),
);
?>

<h1>Update UrlPermission <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>