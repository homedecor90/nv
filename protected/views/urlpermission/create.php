<?php
$this->breadcrumbs=array(
	'Url Permissions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage UrlPermission', 'url'=>array('admin')),
);
?>

<h1>Create UrlPermission</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>