<?php
$this->breadcrumbs=array(
	'Url Permissions',
);

$this->menu=array(
	array('label'=>'Create UrlPermission', 'url'=>array('create')),
	array('label'=>'Manage UrlPermission', 'url'=>array('admin')),
);
?>

<h1>Url Permissions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
