<?php
$this->breadcrumbs=array(
	'Url Permissions'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Create UrlPermission', 'url'=>array('create')),
	array('label'=>'Update UrlPermission', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UrlPermission', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UrlPermission', 'url'=>array('admin')),
);
?>

<h1>View UrlPermission #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'url',
	),
)); ?>
