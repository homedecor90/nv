<h1>Lost Sale</h1>

<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'item-grid',
	'dataProvider'=>$model->searchLostSale(),
	'columns'=>array(
		array(
			'header'=>'ITEM NAME',
			'type'=>'raw',
			'value'=>'$data->items->item_name'
		),
		array(
			'header'=>'ITEM CODE',
			'type'=>'raw',
			'value'=>'$data->items->item_code'
		),
		array(
			'header'=>'ITEM COLOR',
			'type'=>'raw',
			'value'=>'$data->items->color'
		),
        array(
			'header'=>'Qty',
			'type'=>'raw',
			'value'=>'$data->quantity'
		),
        array(
			'header'=>'Date',
			'type'=>'raw',
			'value'=>'date(\'M. d, Y h:i:s A\', strtotime($data->order->lost_sale_date))'
		),
	),
)); 

?>