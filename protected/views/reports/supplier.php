<h1>Sales/Profit Report</h1>

<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Date from', 'date_from'); ?>
                <?php
                Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                $this->widget('CJuiDateTimePicker',array(
                    //'model'=>$supplierOrderContainerBiddingModel, //Model object
                    // 'attribute'=>'date_from', //attribute name
                    'mode'=>'date', //use "time","date" or "datetime" (default)
                    'language'=>'en-GB',
                    'options'=>array(
                        "dateFormat"=>"M. dd, yy"
                    ), // jquery plugin options,
                    'htmlOptions'=>array(
                        'name'=>"date_from",
                    ),
                ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date to', 'date_to'); ?>
                <?php
                Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                $this->widget('CJuiDateTimePicker',array(
                    //'model'=>$supplierOrderContainerBiddingModel, //Model object
                    // 'attribute'=>'date_to', //attribute name
                    'mode'=>'date', //use "time","date" or "datetime" (default)
                    'language'=>'en-GB',
                    'options'=>array(
                        "dateFormat"=>"M. dd, yy"
                    ), // jquery plugin options,
                    'htmlOptions'=>array(
                        'name'=>"date_to",
                    ),
                ));
                ?>
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<strong>Total FOB: </strong><span id="total_fob"><?php echo '$'.number_format($total_fob, 2); ?></span><br />
<strong>Total EXW: </strong><span id="total_exw"><?php echo '$'.number_format($total_exw, 2); ?></span>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'supplier-order-container-grid',
    'dataProvider'=>$model->searchByDates($date_from, $date_to),
    'columns'=>array(
        'id',
        array(
            'header'=>'Supplier',
            'type'=>'raw',
            //'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/user/user/view/id/\'.$data->supplier_id.\'">\'.SupplierOrder::model()->getSupplierName($data->id).\'</a>\'',
            'value'=>'\'<span class="clickable supplier_name" rel="\'.$data->id.\'">\'.SupplierOrder::model()->getSupplierName($data->id).\'</span>\'
				.\'<div style="display: none;" id="supplier_info_\'.$data->id.\'">\'.$data->getSupplierInfoHtml().\'</div>\'',
        ),
        array(
            'header'=>'Created On',
            'value'=>'strtotime($data->created_date)>0 ? date("M. d, Y", strtotime($data->created_date)) : \'\'',
        ),
        array(
            'header'=>'Totals',
            'type'=>'raw',
            'value'=>'\'<strong>FOB:</strong><br />$\'.number_format(SupplierOrder::model()->getSupplierOrderTotalFobPrice($data->id),2)
                        .\'<br /><br /><strong>EXW:</strong><br />$\'.number_format(SupplierOrder::model()->getSupplierOrderTotalExwPrice($data->id),2)',
        ),
        array(
            'header'=>'Discount',
            'type'=>'raw',
            'value'=>'$data->discount_amount',
        ),
//        array(
//            'header'=>'Current 30% Amount (EXW)',
//            'type'=>'raw',
//            'value'=>'$data->getCurrentAdvancePaymentAmount()',
//        ),
//        array(
//            'header'=>'Balance',
//            'type'=>'raw',
//            'value'=>'$data->getSupplierOrderBalanceString()',
//        ),
        array(
            'header'=>'Total CBM',
            'type'=>'raw',
            // 'value'=>'SupplierOrder::model()->getSupplierOrderTotalCBM($data->id)',
            'value'=>'\'<span href="#order_contents_\'.$data->id.\'" id="open_order_contents_\'.$data->id.\'" class="total_cbm">\'.SupplierOrder::model()->getSupplierOrderTotalCBM($data->id).\'</span>\'.SupplierOrder::getItemsPopupCodeAdmin($data->id)',
        ),
//        array(
//            'header'=>'E.T.L',
//            'value'=>'($data->etl==NULL)?"":date("M. d, Y", strtotime($data->etl))',
//        ),
        array(
            'header'=>'Status',
            'type'=>'raw',
            'value'=>'SupplierOrder::model()->getSupplierOrderStatus($data->id)',
        ),
//        array(
//            'header'=>'Container',
//            'type'=>'raw',
//            'value'=>'SupplierOrder::model()->getContainerDetailLink($data->id)',
//        ),
    ),
));

?>

<script type="text/javascript">
    jQuery('#button_filter').click(function(e){
        var search_condition = jQuery('#filter_form').serialize();

        jQuery.fn.yiiGridView.update('supplier-order-container-grid', {
            data: search_condition,
            complete: function(result){
                jQuery('#total_fob').text(jQuery(result.responseText).find('#total_fob').text());
                jQuery('#total_exw').text(jQuery(result.responseText).find('#total_exw').text());
            }
        });
        return false;
    });
</script>