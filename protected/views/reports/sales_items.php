<h1>Sales/Items Report</h1>

<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Date from', 'date_from'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_from', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_from",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date to', 'date_to'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_to', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_to",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <span>Show per page:</span><br />
                <span class="clickable per_page" rel="25">25</span>&nbsp;&nbsp;|&nbsp;
                <span class="clickable per_page" rel="50">50</span>&nbsp;&nbsp;|&nbsp;
                <span class="clickable per_page" rel="0">All</span>

                <input type="hidden" id="per_page" name="per_page" value="25" />
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-grid',
	'dataProvider'=>$model->searchByDates($date_from, $date_to),
	'columns'=>array(
		'item_id',
		array(
			'name'=> 'items.item_name',
			'htmlOptions'=>array(
				'style'=>'text-align: left;'
			),
		),
		'qty',
	),
)); 

?>

<script type="text/javascript">
jQuery('#button_filter').click(function(e){
    var search_condition = jQuery('#filter_form').serialize();
		
    jQuery.fn.yiiGridView.update('supplier-order-container-grid', {
        data: search_condition,
    });
    return false;
});
jQuery('.per_page').click(function(e){
    jQuery('#per_page').val(jQuery(e.target).attr('rel'));
    jQuery('#button_filter').trigger('click');
});
</script>