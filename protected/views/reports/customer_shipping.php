<h1>Customer Shipping Report</h1>

<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Shipper', 'shipper_id'); ?>
                <?php echo CHtml::dropDownList('shipper_id', $shipper_id, $shippers); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date from', 'date_from'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_from', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_from",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date to', 'date_to'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_to', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_to",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Pickup Date (Local pickup)', 'by_pickup'); ?>
				<input type="radio" id="by_pickup" name="filter_by" value="pickup" /><br /><br />
				<?php echo CHtml::label('Payment Date', 'by_payment'); ?>
				<input type="radio" id="by_payment" name="filter_by" value="payment" />
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<strong>Total spent: </strong><span id="total_paid"><?php echo '$'.number_format($total_paid, 2); ?></span>

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-grid',
	'dataProvider'=>$model->searchCSReport(),
	'columns'=>array(
		'id',
		array(
			'header'=>'Pickup date',
			'value'=>'strtotime($data->pickup_date)>0 ? date(\'M. d, Y\', strtotime($data->pickup_date)) : \'\''
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
			'value'=>'$data->getItemTable(true)',
		),
		array(
            'header'=>'Selected Quote',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid ? "$".number_format($data->selected_user_bid->rate_quote, 2) : ""',
        ),
		array(
            'header'=>'Rate Quote Number',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid->rate_quote_number',
        ),
		array(
            'header'=>'Customer Shipping Name',
            'type'=>'raw',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_order->customer_id.\'#note">\'.$data->getCustomerShippingName().\'</a>\'',
            'name'=>'_filter_customer_name'
        ),
		array(
            'header'=>'Company',
            'type'=>'raw',
            'value'=>'$data->getShippingCompany()',
        ),
        array(
        	'header'=>'Paid',
        	'value'=>'$data->paid ? "Yes" : "No"'
       	),
		array(
			'header'=>'Payment date',
			'value'=>'strtotime($data->paid_date)>0 ? date(\'M. d, Y\', strtotime($data->paid_date)) : \'\''
		),
	),
)); 

?>

<script type="text/javascript">
jQuery('#button_filter').click(function(e){
    var search_condition = jQuery('#filter_form').serialize();
		
    jQuery.fn.yiiGridView.update('supplier-order-container-grid', {
        data: search_condition,
        complete: function(result){
			jQuery('#total_paid').text(jQuery(result.responseText).find('#total_paid').text());
        }
    });
    return false;
});
</script>