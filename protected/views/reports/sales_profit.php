<h1>Sales/Profit Report</h1>

<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Date from', 'date_from'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_from', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_from",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::label('Date to', 'date_to'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        //'model'=>$supplierOrderContainerBiddingModel, //Model object
                        // 'attribute'=>'date_to', //attribute name
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date_to",
                        ),
                    ));
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::activeLabel($model, 'state');?>
                <?php echo CHtml::activeDropDownList($model, 'state', States::getList(), array('prompt' => 'Select'));?>
            </div>
            <div class="row">
                <span>Show per page:</span><br />
                <span class="clickable per_page" rel="25">25</span>&nbsp;&nbsp;|&nbsp;
                <span class="clickable per_page" rel="50">50</span>&nbsp;&nbsp;|&nbsp;
                <span class="clickable per_page" rel="0">All</span>

                <input type="hidden" id="per_page" name="per_page" value="25" />
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<strong>Total profit: </strong><span id="total_profit"><?php echo '$'.number_format($total_profit, 2); ?></span><br />
<strong>Total paid: </strong><span id="total_paid"><?php echo '$'.number_format($total_paid, 2); ?></span><br />
<strong>Percentage: </strong><span id="percentage"><?php echo $total_paid ? number_format(100*$total_profit/$total_paid, 2).'%' : '0%'; ?></span>

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-order-container-grid',
	'dataProvider'=>$model->searchByDates($date_from, $date_to),
	'columns'=>array(
		'id',
		array(
			'header'=>'Customer',
			'type'=>'raw',
			'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_id.\'">\'.$data->billing_info->shipping_name.\' \'.$data->billing_info->shipping_last_name.\'</a>\'',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
			'value'=>'CustomerOrder::getCustomerOrderItemsShort($data->id)',
			'htmlOptions'=>array(
				'width'=>'40%'
			)
		),
		array(
			'header'=>'Sold On',
			'value'=>'strtotime($data->sold_date)>0 ? date("M. d, Y", strtotime($data->sold_date)) : \'\'',
		),
		array(
			'header'=>'Profit',
			'value'=>'"$".number_format($data->profit,2)',
		),
        array(
            'header'=>'Paid By Customer',
            'value'=>'"$".number_format($data->getTotalPaidAmount(), 2)'
        ),
        array(
            'header'=>'Percentage',
            'value'=>'$data->getTotalPaidAmount() ? number_format(100*$data->profit/$data->getTotalPaidAmount(), 2)."%" : "0%"'
        ),
	),
)); 

?>

<script type="text/javascript">
jQuery('#button_filter').click(function(e){
    var search_condition = jQuery('#filter_form').serialize();
		
    jQuery.fn.yiiGridView.update('supplier-order-container-grid', {
        data: search_condition,
        complete: function(result){
			jQuery('#total_profit').text(jQuery(result.responseText).find('#total_profit').text());
			jQuery('#total_paid').text(jQuery(result.responseText).find('#total_paid').text());
			jQuery('#percentage').text(jQuery(result.responseText).find('#percentage').text());
        }
    });
    return false;
});

jQuery('.per_page').click(function(e){
    jQuery('#per_page').val(jQuery(e.target).attr('rel'));
    jQuery('#button_filter').trigger('click');
});
</script>