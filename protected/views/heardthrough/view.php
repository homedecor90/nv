<?php
/*$this->breadcrumbs=array(
	'Heard Throughs'=>array('index'),
	$model->id,
);*/

$this->menu=array(
	array('label'=>'List HeardThrough', 'url'=>array('index')),
	array('label'=>'Create HeardThrough', 'url'=>array('create')),
	array('label'=>'Update HeardThrough', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete HeardThrough', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage HeardThrough', 'url'=>array('admin')),
);
?>

<h1>View HeardThrough #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'heard_through',
	),
)); ?>
