<?php
/*$this->breadcrumbs=array(
	'Heard Throughs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);*/

$this->menu=array(
	array('label'=>'List HeardThrough', 'url'=>array('index')),
	array('label'=>'Create HeardThrough', 'url'=>array('create')),
	array('label'=>'View HeardThrough', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage HeardThrough', 'url'=>array('admin')),
);
?>

<h1>Update HeardThrough <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>