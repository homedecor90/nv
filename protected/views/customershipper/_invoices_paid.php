<?php
$this->widget('CGridViewExt', array(
		'id'=>'customer-shipper-invoices-paid-grid',
		'dataProvider'=>$shipment->searchInvoices(true),
		'columns'=>array(
				array(
						'name'=>'id',
						'filter'=>false
				),
				array(
						'header'=>'Destination Zip Code',
						'type'=>'raw',
						'value'=>'$data->customer_order->billing_info->shipping_zip_code',
				),
				array(
						'header'=>'Company',
						'type'=>'raw',
						'value'=>'$data->selected_user_bid->shipper->profile->company',
				),
				array(
						'header'=>'Total Boxes',
						'value'=>'$data->getBoxQty()'
				),
				array(
						'header'=>'Customer Shipping Name',
						'type'=>'raw',
						'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_order->customer_id.\'#note">\'.$data->getCustomerShippingName().\'</a>\'',
						'name'=>'_filter_customer_name'
				),
				array(
						'header'=>'Rate Quote',
						'type'=>'raw',
						'value'=>'\'$\'.number_format($data->selected_user_bid->rate_quote, 2)',
				),
				array(
						'header'=>'Rate Quote Number',
						'type'=>'raw',
						'value'=>'$data->selected_user_bid->rate_quote_number',
				),
				array(
						'header'=>'Pickup date',
						'type'=>'raw',
						'value'=>'(strtotime($data->pickup_date)>0 ? date(\'M. d, Y\', strtotime($data->pickup_date)) : \'\').\'<br />\'.round((time() - strtotime($data->pickup_date))/86400).\'&nbsp;days\''
				),
				array(
						'header'=>'Tracking Number',
						'type'=>'raw',
						'value'=>'$data->tracking_number'
				),
                array(
                    'header'=>'Damages',
                    'type'=>'raw',
                    'value'=>'$data->getClaimsListStr()'
                ),
				array(
						'header'=>'Paid On',
						'value'=>'date(\'M. d, Y\', strtotime($data->paid_date))',
				),
				array(
						'header'=>'Check #',
						'value'=>'$data->paid_checknumber',
				)
		),
));