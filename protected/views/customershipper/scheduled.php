<h1>Scheduled Shipments</h1>

<input type="button" value="Print Manifest" id="print_manifest" />
<br /><br />

<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Date', 'date'); ?>
                <?php
                    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker',array(
                        'mode'=>'date', //use "time","date" or "datetime" (default)
                        'language'=>'en-GB',
                        'options'=>array(
                            "dateFormat"=>"M. dd, yy",
                            "beforeShowDay"=> "js:function(day){ return highlightDays(day); }"
                        ), // jquery plugin options,
                        'htmlOptions'=>array(
                            'name'=>"date",
                        ),
                    ));
                ?>
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<?php

$viewData = array(
	"dataProviderWithBol"=>$dataProviderWithBol,
	"dataProviderWithoutBol"=>$dataProviderWithoutBol,
);

$Tabs = array(
	'with_bol'=>array('title'=>'With BOL','view'=>"_scheduled_with_bol"),
	'without_bol'=>array('title'=>'Without BOL','view'=>"_scheduled_without_bol"),
);
$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));
?>

<input type="button" href="#tracking_number_block" id="tracking_number_button" style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'#tracking_number_button',
	'config'=>array(
		'width'=>350,
		'height'=>180,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="tracking_number_block" class="fancy_box_div fancy_form">
		<h2>Enter Shipment Tracking Number</h2>
		<div>
			<label>Tracking Number: </label>
			<input type="text" id="tracking_number_input" />
		</div>
		<input type="button" value="Submit" id="submit_tracking_number" />
	</div>
</div>

<input type="button" href="#customer_info_block" id="customer_info_button" style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'#customer_info_button',
	'config'=>array(
		'width'=>350,
		'height'=>230,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="customer_info_block" class="fancy_box_div fancy_form">
		<h2>Customer Shipping Address</h2>
		<div>
			
		</div>
	</div>
</div>

<script>

var current_shipment_id = null;

jQuery('#print_manifest').click(function(e){
	var url = '<?php echo Yii::app()->getBaseUrl(true); ?>/index.php/pdf/shipping_manifest?type=cs';

	var ids = [];
	jQuery('#customer-shipment-grid-with-bol input:checked, #customer-shipment-grid-without-bol input:checked,').each(function(){
		ids.push(jQuery(this).val());
	});

	if (ids.length){
		for (i in ids){
			url += '&id[]='+ids[i];
		}
	}
	
	window.location = url;
});

jQuery('#submit_tracking_number').click(function(e){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/set_tracking_number_ajax/?"
				+ 'id=' + current_shipment_id + '&tracking_number=' + jQuery('#tracking_number_input').val();
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status != "success"){
                alert(result.message);
                return;
            }
			
			jQuery.fancybox.close();
			refreshWindow();
		}
	});
});

jQuery('.tracking_number').live('click', function (e){
	current_shipment_id = jQuery(e.target).attr('rel');
	jQuery('#tracking_number_button').trigger('click');
});
	
jQuery('#button_filter').click(function(e){
	var search_condition = jQuery('#filter_form').serialize();
		
	jQuery.fn.yiiGridView.update('customer-shipment-grid-without-bol', {
		data: search_condition
	});
	
	jQuery.fn.yiiGridView.update('customer-shipment-grid-with-bol', {
		data: search_condition
	});
	return false;
});

jQuery('#download_labels').click(function(e){
	var url = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/download_labels';	
	var date = jQuery('#date').val();
	if (date){
		url += '?date='+date;
	}
	
	window.location = url;
});

function refreshWindow(){
	$.fn.yiiGridView.update('customer-shipment-grid-with-bol');
	$.fn.yiiGridView.update('customer-shipment-grid-without-bol');
}

jQuery('#download_bols').click(function(e){
	var url = '<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/download_bols';
	var date = jQuery('#date').val();
	if (date){
		url += '?date='+date;
	}
	
	window.location = url;
});

	var dates = [<?php echo implode(',', $highlightDates); ?>];
    function highlightDays(date) {
				var month = date.getMonth() + 1
				var day = date.getDate();
				var theday = (month < 10 ? '0' : '') + month +'/'+ 
				(day < 10 ? '0' : '') + day + '/' + 
				date.getFullYear();
				//~ console.log(theday);
				return [true,jQuery.inArray(theday, dates) >=0?"highlighted":''];
			}
	 // }

jQuery('.zip').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	jQuery('#customer_info_block > div').html(jQuery('#customer_info_'+id).html());
	jQuery('#customer_info_button').trigger('click');
	e.preventDefault();
    e.stopImmediatePropagation();
});

</script>
