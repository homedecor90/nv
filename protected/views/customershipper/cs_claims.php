<h1>Claims</h1>
<?php

$this->menu=array(
		array('label'=>'Claims Archive', 'url'=>array('customershipper/cs_claims_archive')),
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-shipment-claims-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
		array(
			'header'=>'Destination Zip Code',
			'type'=>'raw',
			'value'=>'$data->shipment->customer_order->billing_info->shipping_zip_code'
		),
		array(
			'header'=>'Damaged Boxes',
			'type'=>'raw',
			'value'=>'$data->getDamagedBoxesListCS(\'shipper\')'
		),
		array(
			'header'=>'Rate Quote',
			'value'=>'$data->shipment->selected_user_bid->rate_quote',
		),
		array(
			'header'=>'Rate Quote Number',
			'type'=>'raw',
			'value'=>'$data->shipment->selected_user_bid->rate_quote_number',
		),
		array(
			'header'=>'Pickup date',
			'value'=>'date(\'M. d, Y\', strtotime($data->shipment->pickup_date))'
		),
		array(
			'header'=>'Date of Damage',
			'value'=>'date(\'M d, Y\', strtotime($data->claim_date))',
		),
		array(
				'header'=>'Docs',
				'type'=>'raw',
				'value'=>'$data->getClaimDocsListStr()',
		),
		array(
			'header'=>'Claim Amount',
			'value'=>'\'$\'.number_format($data->amount, 2)'
		),
	),
));
?>