<?php
Yii::app()->clientScript->registerScriptFile('/js/customerOrder/pendingOrders.js');
$pa = $pending_approval_count ? ' ('.$pending_approval_count.')' : '';
// TODO: make it visible not only for admin
if (Yii::app()->user->isAdmin()){
	$waiting_approval = CustomerOrderBillingInfoUnapproved::model()->count();
    $this->menu=array(
		array('label'=>'Pending Approval Orders'.$pa, 'url'=>array('customer/pending_approval_orders')),
		array('label'=>'Chargebacks', 'url'=>array('customershipper/chargebacks')),
		array('label'=>'Cancelled Orders', 'url'=>array('customershipper/cancelled')),
		array('label'=>'Completed Shipments', 'url'=>array('customershipper/completed')),
		array('label'=>'Approve Order Info Changes'.($waiting_approval ? ' ('.$waiting_approval.')' : ''), 'url'=>array('customershipper/edited_order_info')),
		array('label'=>'Customer Pickup', 'url'=>array('customershipper/customer_pickup')),
	);
}
?>
<h1>Pending Shipping Orders</h1>
<?php
//if (YumUser::isOperator(true)):
?>
<input type="button" value="Add Third Party Shipment" href="#third_party_div" id="third_party_button"/>
<?php
//endif;

echo CancellationReason::getCancellationReasonCode();
echo CustomerOrder::getScanOutFormCode();
?>
<?php 
$customerOrderModel->status = CustomerOrder::$STATUS_SOLD;
$controller = $this;
$this->widget('CGridViewExt', array(
	'id'=>'customer-sold-order-grid',
	'dataProvider'=>$customerOrderModel->searchSoldPendingShipping(),
	'filter'=>$customerOrderModel,
	'columns'=>array(
		array(
			'name'=>'id',
			'filter'=>false
		),
        array(
			'header'=>'Shipping Address',
            'type'=>'raw',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_id.\'">\'.$data->billing_info->shipping_name.\' \'.$data->billing_info->shipping_last_name.\'</a><br /><br />\'.$data->getShippingAddressStr()',
            'name'=>'_filter_billing_name'
		),
        array(
			'header'=>'Billing Address',
            'type'=>'raw',
            'value'=>'$data->billing_info->billing_name.\' \'.$data->billing_info->billing_last_name.\'<br /><br />\'
						.$data->getBillingAddressStr().\'<br /><br />\'.$data->customer->email
						.\'<br /><br /><a href="#" class="edit_info" rel="\'.$data->id.\'">[Edit customer info]</a>\'',
            //~ 'name'=>'_filter_billing_name'
		),
		array(
			'header'=>'Shipments',
			'type'=>'raw',
            'value'=> '$data->getShipmentTableShort()',
		),
		array(
			'header'=>'Price',
			'type'=>'raw',
            'value'=>'\'Total: $\'.$data->grand_total_price.\'<br />Discounted: $\'.$data->discounted_total',
		),
        array(
			'header'=>'Sold On',
			'value'=>'date("M. d, Y", strtotime($data->sold_date))',
		),
		array(
			'header'=>'Shipping Method',
			'type'=>'raw',
            // 'value'=>'\'<span id="shipping_method_\'.$data->id.\'">\'.$data->shipping_method.\'</span>\'',
            'value'=>'$data->shipping_method',
		),
        array(
            'header'=>'Order Notes',
            'type'=>'raw',
            'value'=>'nl2br($data->billing_info->order_notes)'
        ),
		/*array(
            'header'=>'Shipping Notes',
            'type'=>'raw',
            'value'=>'nl2br($data->billing_info->shipping_notes)'
        ),*/
        array(
            'header'=>'Action',
            'type'=>'raw',
            'value'=> function($data, $row) {
                    Yii::app()->controller->renderPartial('_pending_order_actions', array(
                        'data' => $data,
                    ));
                }
                /*'\'<input type="button" class="order_expand_shipment_details" rel="\'.$data->id.\'" value="Expand Details" /><br />\'.
						(($data->shipping_method == \'Local Pickup\' || $data->verificationRequired()) ? \'\' : \'<input type="button" rel="\'.$data->id.\'" value="Send for Bidding" class="send_for_bidding" data-target="order" /><br />\').
						(YumUser::isOperator(true) ? \'<input type="button" class="order_cancel" rel="\'.$data->id.\'" value="Cancel Order" /><br />
                        <input type="button" class="order_chargeback" rel="\'.$data->id.\'" value="Chargeback" /><br />\'        				
        				:\'\')
		        		.( $data->verificationRequired() ? \'<input type="button" class="order_verification_request" rel="\'.$data->id.\'" value="Verification Request" /><input type="button" rel="\'.$data->id.\'" class="order_verify" value="Verified" />\' : \'\')
        				.\'<br />\'.$data->getEditShippingButtonCode()',*/
        ),
        array(
            'header'=>'Timeline',
            'type'=>'raw',
            'value'=>'\'Pending for \'.round((time() - strtotime($data->sold_date))/86400).\' day(s)\'',
        ),
		// 'shipping_status'
	),
));
?>

<?php
if (YumUser::isOperator(true)):

$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#third_party_button',
    'config'=>array(
		'width'=>950,
		'height'=>600,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="third_party_div" class="fancy_box_div">
	<?php
	$this->renderPartial('../customer/_add_3rd_party_shipment', array(
		'newCustomerOrderModel'=>$customerOrderModel, 
		'form'=> isset($form) ? $form : null,
		'itemNameList'=> $itemNameList,
		'sales_tax_percent'=> $sales_tax_percent
	));
	?>
	</div>
</div>

<?php
endif;
?>

<input type="button" value="Edit Order Info" href="#order_info_div" id="order_info_button" style="display:none;"/>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_info_button',
    'config'=>array(
		'width'=>700,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="order_info_div" class="fancy_box_div">
		<div id="order_info_form_container">
		</div>
	</div>
</div>

<input type="button" value="Switch Order Shipping Method" href="#order_shipping_method_div" id="order_shipping_method_button" style="display:none;"/>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_shipping_method_button',
    'config'=>array(
		'width'=>600,
		'height'=>450,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="order_shipping_method_div" class="fancy_box_div">
		<div id="order_shipping_method_form_container">
		</div>
	</div>
</div>

<input type="button" value="Schedule Pickup" href="#schedule_pickup_div" id="schedule_pickup_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#schedule_pickup_button',
    'config'=>array(
		'width'=>320,
		'height'=>200,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="schedule_pickup_div" class="fancy_box_div">
		<form id="schedule_pickup_form" class="fancy_form">
			<h2>Schedule Pickup</h2>
			<div>
				<input type="hidden" name="order_id" id="pickup_shipment_id" value=""/>
				<input type="radio" checked="checked" name="schedule_option" value="admin" id="schedule_by_admin" />
				<label for="schedule_by_admin">Schedule pickup time by admin</label><br />
				<label>Pickup Date</label>
				<?php
					Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
					$this->widget('CJuiDateTimePicker',array(
						'mode'=>'datetime', //use "time","date" or "datetime" (default)
						'language'=>'en-GB',
						'name'=>'pickup_date',
						'options'=>array(
							"dateFormat"=>"M. dd, yy",
							"timeFormat"=>"hh:mm tt",
							"ampm"=>true
						),
						'htmlOptions' => array(
                            'id' => 'pickup_date'
                        )
					));
				?>
				<br /><br />
				<input type="radio" name="schedule_option" value="customer" id="schedule_by_customer" />
				<label for="schedule_by_customer">Schedule pickup time by customer</label><br />
			</div>
			
			<input type="button" value="Save" id="schedule_pickup_submit"/>
		</form>
	</div>
</div>

<input type="button" value="Expand Order Details" href="#order_details_container" id="order_details_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_details_button',
    'config'=>array(
		'width'=>900,
		'height'=>500,
		'autoDimensions'=>false,
    	'onClosed'=>'js:function(){if(order_details_need_update_window){refreshWindow(); order_details_need_update_window = false; }}'
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="order_details_container" class="fancy_box_div">
<div id="order_details_content"></div>
</div>

</div>

<input type="button" value="Combine Orders" href="#combine_orders_container" id="combine_orders_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#combine_orders_button',
    'config'=>array(
		'width'=>300,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="combine_orders_container" class="fancy_box_div">
<div id="combine_orders_content"></div>
	<h3>Combine Orders</h3>
	<p>
		<strong>Order ID: </strong><span id="combine_orders_order_id"></span><br />
		<strong>Customer: </strong><span id="combine_orders_customer"></span>
	</p>
	<strong>Shipping Info: </strong><div id="combine_orders_shipping_info"></div>
	<label for="combine_orders_copy_info">Copy shipping info: </label>
	<input type="checkbox" id="combine_orders_copy_info" value="1" />
	<br /><br />
	<input type="button" value="Combine" id="combine_orders_submit" />
</div>

</div>

<input type="button" value="Combine Shipments" href="#combine_shipments_container" id="combine_shipments_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#combine_shipments_button',
    'config'=>array(
		'width'=>300,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="combine_shipments_container" class="fancy_box_div">
<div id="combine_shipments_content"></div>
	<h3>Combine Shipments</h3>
	<p>
		<strong>Order ID: </strong><span id="combine_shipments_order_id"></span><br />
		<strong>Customer: </strong><span id="combine_shipments_customer"></span>
	</p>
	<strong>Shipping Info: </strong><div id="combine_shipments_shipping_info"></div>

	<input type="button" value="Confirm" id="combine_shipments_submit" />
</div>

</div>


<script type="text/javascript">
order_details_need_update_window = false;

function refreshWindow(){
	$.fn.yiiGridView.update('customer-sold-order-grid');
}

jQuery('.order_cancel').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/cancel_order_ajax/?order_id='+id;
	cancelShippingWindow(url, function(){$.fn.yiiGridView.update('customer-sold-order-grid');});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	return false;
});

jQuery('.cancel_shipment').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/cancel_shipment_ajax/?shipment_id='+id;
	cancelShippingWindow(url, function(){$.fn.yiiGridView.update('customer-sold-order-grid');});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	return false;
});

jQuery('.order_chargeback').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/order_chargeback_ajax/?order_id='+id;
	cancelShippingWindow(url, function(){$.fn.yiiGridView.update('customer-sold-order-grid');});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	return false;
});

jQuery('.send_for_bidding').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
    var id = target.attr('rel');
	var action = target.attr('data-target') == 'order' ? 'order_send_for_bidding_ajax' : 'shipment_send_for_bidding_ajax';
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/"+action+"/?id="+id;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            $.fn.yiiGridView.update('customer-sold-order-grid');
        },
        dataType: 'json'
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('.cancel_item').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/individualitem/cancel_from_customer_order_ajax/?ind_item_id='+id;
	cancelShippingWindow(url, function(){refreshWindow();});
	
	e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

jQuery('.scan_shipment_out').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/customershipper/scan_shipment_out_ajax/?shipment_id='+id;
	scanOutWindow(url, function(){$.fn.yiiGridView.update('customer-sold-order-grid');});
	
	e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

jQuery('.scan_item_out').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + '/index.php/individualitem/scan_as_out_ajax/?ind_item_id='+id;
	scanOutWindow(url, function(){$.fn.yiiGridView.update('customer-sold-order-grid');});
	
	e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

jQuery('.edit_info').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/get_edit_order_info_form_ajax/?order_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#order_info_form_container').html(result);
			jQuery('#order_info_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#save_order_info').live('click', function(e){
	var data = jQuery('#customer-order-info-form').serialize();
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/edit_order_info_ajax/";
	jQuery.ajax({
		url:url,
		dataType:"json",
		data: data,
		type: 'post',
		success:function(result){
			if(result.message){
				alert(result.message);
			}
			
			if (result.status == "success"){
				$.fn.yiiGridView.update('customer-sold-order-grid');
				jQuery.fancybox.close();
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
	
});


// TODO: wrapper function for AJAX calls (?)
</script>
