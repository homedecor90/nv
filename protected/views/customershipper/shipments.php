<?php
$this->menu=array(
	array('label'=>'Pending Shipping Orders', 'url'=>array('customershipper/pending_orders')),
	array('label'=>'Chargebacks / Claims', 'url'=>array('customershipper/chargebacks_claims')),
);
?>
<h1>Shipments</h1>

<?php

$viewData = array(
		"dataProviderWithBol"=>$dataProviderWithBol,
		"dataProviderWithoutBol"=>$dataProviderWithoutBol,
		"highlightDates"=>$highlightDates,
		"shipmentModel"=>$shipmentModel,
);

$Tabs = array(
		'tab_select_bids'=>array('title'=>'Lowest Bid','view'=>"select_bids"),
		'tab_scheduled_cs'=>array('title'=>'Scheduled CS Pickup','view'=>"scheduled"),
		'tab_scheduled_customer'=>array('title'=>'Scheduled Customer Pickup','view'=>"customer_pickup"),
		'tab_completed'=>array('title'=>'Completed','view'=>"completed"),
);
$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));
?>

<script type="text/javascript">

refreshWindow = function(){
	$.fn.yiiGridView.update('customer-shipment-grid-without-bol');
	$.fn.yiiGridView.update('customer-shipment-grid-with-bol');
	$.fn.yiiGridView.update('customer-shipment-grid');
	$.fn.yiiGridView.update('customer-completed-shipment-grid');
	$.fn.yiiGridView.update('customer-pickup-shipment-grid');
}

</script>