<h1>Shipments Scheduled for Customer Pickup</h1>

<input type="button" value="Print Manifest" onclick="window.location = '<?php echo Yii::app()->getBaseUrl(true); ?>/index.php/pdf/shipping_manifest?type=customer'" />

<?php
$dataProvider = $shipmentModel->searchCustomerPickup();
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'customer-pickup-shipment-grid',
		'dataProvider'=>$dataProvider,
		//'filter'=>$shipmentModel,
		'columns'=>array(
				array(
						'name'=>'id',
						'filter'=>false
				),
				array(
						'header'=>'Pickup date',
						'value'=>'strtotime($data->pickup_date)>0 ? date(\'M. d, Y \a\t H:i A\', strtotime($data->pickup_date)) : \'\''
				),
				array(
						'header'=>'Destination Zip Code',
						'type'=>'raw',
						'value'=>'$data->customer_order->billing_info->shipping_zip_code',
				),
				array(
						'header'=>'Items',
						'type'=>'raw',
						'value'=>'$data->getItemTable()',
				),
				array(
						'header'=>'Customer Shipping Name',
						'type'=>'raw',
						'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_order->customer_id.\'#note">\'.$data->getCustomerShippingName().\'</a>\'',
				),
				array(
						'header'=>'Pickup Docs',
						'type'=>'raw',
						'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customershipper/pickup_docs/?id=\'.$data->id.\'">Download</a>\''
				),
				array(
						'header'=>'Action',
						'type'=>'raw',
						'value'=>'\'<input type="button" class="scan_out" rel="\'.$data->id.\'" value="Scan as Out"/><br />
							<input type="button" value="Requeue" rel="\'.$data->id.\'" class="requeue" />\'',
				)
		),
));

?>

<script type="text/javascript">
jQuery('.scan_out').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
	var shipment_id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/scan_shipment_out_ajax/?shipment_id=" + shipment_id;
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}

			refreshWindow();
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

</script>