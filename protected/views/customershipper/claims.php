<?php
/**
 * @var CustomerOrderShipmentClaim $claimModel
 */
?>
<h1>Claims</h1>
<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Shipper', 'shipper_id'); ?>
                <?php echo CHtml::activeDropDownList(
                    $claimModel,
                    'shipperId',
                    CHtml::listData(YumUser::listShippers(), 'user_id', 'company')
                ); ?>
            </div>
            <?php echo CHtml::submitButton('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<?php

// $this->menu=array(
// 		array('label'=>'Claims Archive', 'url'=>array('customershipper/claims_archive')),
// );

$this->widget('CGridViewExt', array(
	'id'=>'customer-shipment-claims-grid',
	'dataProvider'=> $claimModel->with('shipperProfile')->search(),
	'columns'=>array(
		'id',
		'shipment_id',
		array(
			'header'=>'Destination Zip Code',
			'type'=>'raw',
			'value'=>'$data->shipment->customer_order->billing_info->shipping_zip_code'
		),
		array(
			'header'=>'Company',
			'value'=>'$data->shipment->selected_user_bid->shipper->profile->company'
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
			'value'=>'$data->getDamagedBoxesList()'
		),
		array(
			'header'=>'Customer Shipping Name',
			'type'=>'raw',
			'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->shipment->customer_order->customer_id.\'#note">\'.$data->shipment->getCustomerShippingName().\'</a>\'',
		),
		array(
			'header'=>'Rate Quote',
			'value'=>'$data->shipment->selected_user_bid->rate_quote',
		),
		array(
			'header'=>'Rate Quote Number',
			'type'=>'raw',
            'value'=>'$data->shipment->selected_user_bid->rate_quote_number.($data->shipment->paid ? \'<br /><span style="font-size: 10px">/Paid on \'.date(\'M. d\', strtotime($data->shipment->paid_date)).\'/</span>\' : "")',
		),
		array(
			'header'=>'Pickup date',
			'value'=>'(strtotime($data->shipment->pickup_date) > 0) ? date(\'M. d, Y\', strtotime($data->shipment->pickup_date)) : \'\''
		),
		array(
			'header'=>'Date of Damage',
			'value'=>'(strtotime($data->claim_date) > 0)  ? date(\'M d, Y\', strtotime($data->claim_date)) : \'\'',
		),
        array(
            'header'=>'Timeline',
            'type'=>'raw',
            'value'=>'\'Pending for \'.round((time() - strtotime($data->claim_date))/86400).\' day(s)\'',
        ),
		array(
			'header'=>'Claim Amount',
			'type'=>'raw',
			'value'=>'\'<span class="claim_amount clickable" rel="\'.$data->id.\'">$\'.number_format($data->amount, 2).\'</span>\''
		),
		array(
			'header'=>'Docs',
			'type'=>'raw',
			'value'=>'$data->getClaimDocsListStr()',
		),
		array(
			'header'=>'Action',
			'type'=>'raw',
			'value'=>'$data->getActionsStr()',
		)
	),
));
?>

<input type="button" value="Upload Doc" href="#uploadOfficialDocsDiv" id="uploadOfficialDocsButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#uploadOfficialDocsButton',
    'config'=>array(
		'width'=>400,
		'height'=>250,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
<div id="uploadOfficialDocsDiv" class="fancy_box_div">
<br/><br/>
<?php
// Ajax Upload View Part
$this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
	'id'=>'uploadDoc',
	'config'=>array(
		'action'=>Yii::app()->createUrl('customershipper/upload_claim_doc'),
		'allowedExtensions'=>array("pdf","jpg","png","jpeg","rar","zip","doc","odt"),//array("jpg","jpeg","gif","exe","mov" and etc...
		'sizeLimit'=>10*1024*1024,// maximum file size in bytes
		'multiple'=>true,
		'minSizeLimit'=>0,// minimum file size in bytes
		'onComplete'=>"js:function(id, file_name, responseJSON, in_progress){ onCompleteUploadOfficialDocs(responseJSON, file_name, in_progress); }",
		'onProgress'=>"js:function(id, file_name, loaded, total){ onProgessOfficialDocs(file_name, loaded, total); }"
	)
)); 
?>
<div id="progress"></div>
</div>
</div>

<input type="button" value="Order From Supplier" href="#supplier_order_container" id="supplier_order_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#supplier_order_button',
    'config'=>array(
		'width'=>400,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="supplier_order_container" class="fancy_box_div">
<div id="supplier_order_content"></div>
</div>
</div>

<input type="button" value="Authorize Return Form" href="#authorize_return_form_container" id="authorize_return_form_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#authorize_return_form_button',
    'config'=>array(
		'width'=>700,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="authorize_return_form_container" class="fancy_box_div">
<div id="authorize_return_form_content"></div>
</div>
</div>

<input type="button" value="Supplier Order List" href="#order_list_container" id="order_list_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#order_list_button',
    'config'=>array(
		'width'=>1000,
		'height'=>300,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="order_list_container" class="fancy_box_div">
<div id="order_list_content"></div>
<input type="button" value="Create Order" rel="0" class="choose_order" />
</div>
</div>

<input type="button" value="Claim Notes" href="#claim_notes_container" id="claim_notes_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#claim_notes_button',
    'config'=>array(
		'width'=>800,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="claim_notes_container" class="fancy_box_div">
<div id="claim_notes_content"></div>
</div>
</div>

<script type="text/javascript">

var replacing = false;
var current_shipment_id = 0;
var arr_upload_progress = new Array();
var doc_ids = new Array();

jQuery('.replace').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	jQuery('#claim_'+s_id+' input').each(function(){
		var el = jQuery(this);
		if (!el.hasClass('replaced')){
			el.show();
		}
	});
	jQuery('#replace_save_'+s_id).show();
	jQuery('#replace_cancel_'+s_id).show();
	target.hide();
	jQuery('#supplier_order_'+s_id).hide();
	jQuery('#refund_'+s_id).hide();
	jQuery('#authorize_return_'+s_id).hide();
	
	replacing = true;
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.replace_cancel').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	jQuery('#claim_'+s_id+' input').hide();
	jQuery('#replace_'+s_id).show();
	jQuery('#supplier_order_'+s_id).show();
	jQuery('#replace_save_'+s_id).hide();
	jQuery('#refund_'+s_id).show();
	jQuery('#authorize_return_'+s_id).show();
	
	target.hide();

	jQuery('#order_profit_'+s_id+' span').text(jQuery('#order_profit_value_'+s_id).val());
	jQuery('#claim_'+s_id+' input:checked').attr('checked', false);

	replacing = false;
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.replace_save').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	var ids = [];
	jQuery('#claim_'+s_id+' input:checked').each(function(i){
		ids.push(jQuery(this).val());
	});
	if (ids.length == 0){
		alert('Please select items');
		return false;
	}

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/replace_damaged_items_ajax/?claim_id="+s_id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {item_ids: ids},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				refreshWindow();
			}
		}
	});

	replacing = false;
	
	e.preventDefault();
	e.stopImmediatePropagation();
});


jQuery('.refund').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	jQuery('#claim_'+s_id+' input').each(function(){
		var el = jQuery(this);
		if (!el.hasClass('refunded')){
			el.show();
		}
	});
	jQuery('#refund_save_'+s_id).show();
	jQuery('#refund_cancel_'+s_id).show();
	target.hide();
	jQuery('#supplier_order_'+s_id).hide();
	jQuery('#replace_'+s_id).hide();
	jQuery('#authorize_return_'+s_id).hide();
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.refund_cancel').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	jQuery('#claim_'+s_id+' input').hide();
	jQuery('#refund_'+s_id).show();
	jQuery('#supplier_order_'+s_id).show();
	jQuery('#refund_save_'+s_id).hide();
	jQuery('#replace_'+s_id).show();
	jQuery('#authorize_return_'+s_id).show();
	target.hide();

	jQuery('#order_profit_'+s_id+' span').text(jQuery('#order_profit_value_'+s_id).val());
	jQuery('#claim_'+s_id+' input:checked').attr('checked', false);
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.refund_save').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	var ids = [];
	jQuery('#claim_'+s_id+' input:checked').each(function(i){
		ids.push(jQuery(this).val());
	});
	if (ids.length == 0){
		alert('Please select items');
		return false;
	}
	/*jQuery('#claim_'+s_id+' input').hide();
	jQuery('#replace_'+s_id).show();
	jQuery('#replace_save_'+s_id).hide();*/
	//target.hide();

	var amount = prompt('Please enter refund amount:', 0);
	if (!amount){
		return false;
	}
	if (isNaN(parseFloat(amount)) || parseFloat(amount) <= 0){
		alert('Incorrect amount');
		return false;
	}
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/refund_damaged_items_ajax/?claim_id="+s_id+"&amount="+amount;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {item_ids: ids},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				refreshWindow();
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.authorize_return').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/authorize_return_form_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#authorize_return_form_content').html(result);
			jQuery('#authorize_return_form_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.authorize_return_cancel').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	jQuery('#claim_'+s_id+' input').hide();
	jQuery('#refund_'+s_id).show();
	jQuery('#supplier_order_'+s_id).show();
	jQuery('#authorize_return_save_'+s_id).hide();
	jQuery('#replace_'+s_id).show();
	jQuery('#authorize_return_'+s_id).show();
	target.hide();

	jQuery('#order_profit_'+s_id+' span').text(jQuery('#order_profit_value_'+s_id).val());
	jQuery('#claim_'+s_id+' input:checked').attr('checked', false);
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.authorize_return_save').live('click', function(e){
	var target = jQuery(e.target);
	var s_id = target.attr('rel');
	var ids = [];
	jQuery('#claim_'+s_id+' input:checked').each(function(i){
		ids.push(jQuery(this).val());
	});
	if (ids.length == 0){
		alert('Please select items');
		return false;
	}
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/authorize_return_ajax/?shipment_id=" + s_id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {item_ids: ids},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				$.fn.yiiGridView.update('customer-shipment-claims-grid');
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.supplier_order').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/order_damaged_items_from_supplier_form_ajax/?claim_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#supplier_order_content').html(result);
			jQuery('#supplier_order_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.claim_amount').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var amount = prompt('Enter new claim amount',0);
	if (!amount){
		return false;
	}
	if (isNaN(parseFloat(amount)) || parseFloat(amount) <= 0){
		alert('Incorrect amount');
		return false;
	}
	if (!confirm('Are you sure?')){
		return false;
	}
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/update_claim_amount_ajax/?claim_id=" + id + '&amount=' + amount;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#supplier_order_submit').live('click', function(e){
	var checked = false;
	jQuery('#supplier_order_content input:checked').each(function(i){
		checked = true;
	});
	
	if (!checked){
		alert('Please select boxes');
		return false;
	}
	// get supplier order list
	var supplier_id = jQuery('#supplier_id').val();
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/supplierorder/get_pending_order_for_supplier_ajax/?supplier_id=" + supplier_id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"html",
		success:function(result){
			jQuery('#order_list_content').html(result);
			jQuery('#order_list_button').trigger('click');
		}
	});
	return true;
	
});

jQuery('.claim_notes').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/claim_notes_window_ajax/?claim_id=" + id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"html",
		success:function(result){
			jQuery('#claim_notes_content').html(result);
			jQuery('#claim_notes_button').trigger('click');
		}
	});
	return true;
});

jQuery('.add_new_claim_note').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var text = jQuery('#new_claim_note').val();
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/add_claim_note_ajax/?claim_id=" + id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data:{text: text},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				alert('New note has been added');
				jQuery.fancybox.close();
			}
		}
	});
	return true;
});

jQuery('.choose_order').live('click', function(e){
	var checked = false;
	var boxes = [];
	var id = jQuery('#supplier_order_shipment_claim_id').val();
	
	jQuery('#supplier_order_content input:checked').each(function(i){
		checked = true;
		boxes.push(jQuery(this).val());
	});
	
	if (!checked){
		alert('Please select boxes');
		return false;
	}

	var target = jQuery(e.target);
	var order_id = jQuery(target).attr('rel');
	var supplier_id = jQuery('#supplier_id').val();
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/order_damaged_items_from_supplier_ajax/?claim_id=" + id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {
				boxes: boxes,
				order_id: order_id,
				supplier_id: supplier_id
		},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				jQuery.fancybox.close();
				refreshWindow();
			}
		}
	});

	target.attr('disabled', 'disabled');
});

jQuery('.upload_doc').live('click', function(e){
	current_shipment_id = jQuery(e.target).attr('rel');
	jQuery('#progress').empty();
	jQuery('.qq-upload-list').empty();
	
	jQuery('#uploadOfficialDocsButton').trigger('click');
	e.preventDefault();
	e.stopImmediatePropagation();
});

function calculateReplaceCost(s_id){
	var total = 0;
	var profit = parseFloat(jQuery('#order_profit_value_'+s_id).val());
	jQuery('#claim_'+s_id+' input:checked').each(function(i){
		var el = jQuery(this);
		total += parseFloat(el.attr('data-shipping-price'));
		total += 100 * parseFloat(el.attr('data-cbm'));
		total += parseFloat(el.attr('data-fob'));
	});

	var profit_str = '$' + roundExt(profit) + ' - $' + total + ' = $' + roundExt(profit - total);
	jQuery('#order_profit_'+s_id+' span').text(profit_str);
}

jQuery('.resolve').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}

	var s_id = jQuery(e.target).attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/resolve_claim_ajax/?claim_id=" + s_id;

	jQuery.ajax({
		url:url,
		type: 'post',
		success:function(result){
			if (result.status == 'fail'){
				alert(result.message);
				return false;
			}
			refreshWindow();
		}
	});
});

function onCompleteUploadOfficialDocs(response, file_name, in_progress){
	// in_progress shows how many files are still in the queue
	if (response.status == 'fail'){
		alert(response.message);
		return false;
	}

	doc_ids.push(response.doc_id);
	if (in_progress == 0){
		var s_id = current_shipment_id;
		var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/attach_claim_doc_ajax/?claim_id=" + s_id;
	
		jQuery.ajax({
			url:url,
			type: 'post',
			data: {doc_ids: doc_ids},
			success:function(result){
				if (result.status == 'fail'){
					alert(result.message);
					return false;
				}
				jQuery.fancybox.close();
				doc_ids.length = 0;
				refreshWindow();
				alert('Upload complete');
			}
		});
	}
}

function onProgessOfficialDocs(file_name, loaded, total){
	var strFileProgress = "<div>" + file_name + " : " + loaded + " of "+ total + " Bytes uploaded</div>";
	arr_upload_progress[file_name] = strFileProgress;
	
	var strProgress = "";
	for (key in arr_upload_progress){
		strProgress += arr_upload_progress[key];
	}
	
	jQuery("#progress").html(strProgress);
}

function roundExt(val){
	return Math.round(val * 100)/100;
}
<?php
if(Yii::app()->user->isAdmin()):
?>
jQuery('.damaged_items input[type="checkbox"]').live('change', function(e){
	if (replacing){
		calculateReplaceCost(jQuery(e.target).attr('data-claim'));
	}
});
<?php
endif;
?>
</script>
<script type="text/javascript">
    $('.search-form form').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        $.fn.yiiGridView.update('customer-shipment-claims-grid', {'data': data});
    });
</script>

