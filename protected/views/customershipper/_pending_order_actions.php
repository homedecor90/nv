<?php
/**
 * @var $this CustomershipperController
 * @var $data CustomerOrder
 */
?>
    <input type="button" class="order_expand_shipment_details" rel="<?php echo $data->id?>" value="Expand Details"><br>
<?php if ($data->payment_approved) {?>
    <?php if (!($data->shipping_method == 'Local Pickup' || $data->verificationRequired())):?>
        <input type="button" rel="<?php echo $data->id?>" value="Send for Bidding" class="send_for_bidding" data-target="order"><br>
    <?php endif;?>
<?php } else {?>
    <?php if (Yii::app()->user->isAdmin()) :?>
        <button class="order-actions approve"
                data-url="<?=$this->createUrl('/customerOrder/approvePayment', array('id' => $data->id))?>"
                data-grid-id="customer-sold-order-grid"
                data-confirmation="Are you sure you want to approve order?"
            >
            Approve order
        </button>
    <? else:?>
        <span>Waiting approval</span>
    <? endif;?>
<? }?>
<?php if (YumUser::isOperator(true)):?>
    <input type="button" class="order_cancel" rel="<?php echo $data->id?>" value="Cancel Order"><br>
    <input type="button" class="order_chargeback" rel="\'.$data->id.\'" value="Chargeback"><br>
<?php endif;?>
<?php if ($data->verificationRequired()):?>
    <input type="button" class="order_verification_request" rel="<?php echo $data->id?>" value="Verification Request">
    <input type="button" rel="<?php echo $data->id?>" class="order_verify" value="Verified">
<?php endif;?>
<br><?php $data->getEditShippingButtonCode();?>
<?
/*
'\'\'.


		        		.( $data->verificationRequired() ? \'<input type="button" class="order_verification_request" rel="\'.$data->id.\'" value="Verification Request" /><input type="button" rel="\'.$data->id.\'" class="order_verify" value="Verified" />\' : \'\')
        				.\'<br />\'.$data->getEditShippingButtonCode()'
*/
?>