<h1>Submit Rate Quotes</h1>
<p>
- Include $500.00 per box insurance for all shipment <br />
- Verify each address manually to ensure if it's business or residential. Clicking the zip code will show you the full shipping address.<br />
- All quotes submitted will be final. No additional charges should apply. If you have questions, please contact us before submitting your quotes.<br />
</p>
<div style="display:none;" id="high_bids_message">IMPORTANT: The RED Quotes are way too high. The chances of them being accepted for these shipments are very low. It is highly recommended that you lower your rate quote for these shipments and then click  "Submit Quote" Button below</div>
<?php
$this->pageTitle = 'Submit Rate Quotes';
$dataProvider = $shipmentModel->searchBiddingShipper();
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-sold-order-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
        array(
			'header'=>'Destination Zip Code',
            'type'=>'raw',
            'value'=>'\'<span class="clickable zip" rel="\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_zip_code.\'</span>\'
        			.($data->return ? \'<br /><br/>[Return]\' : \'\')	
        			.\'<div style="display: none;" id="customer_info_\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_name.\' \'.$data->customer_order->billing_info->shipping_last_name.\'<br /><br />\'.$data->customer_order->getShippingAddressStr().\'<br /><br />Email: \'.$data->customer_order->customer->email.\'</div>\'',
		),
		array(
			'header'=>'Address type',
			'value'=>'$data->customer_order->billing_info->busrestype',
		),
		array(
            'header'=>'Total Boxes',
            'type'=>'raw',
            'value'=>'$data->getBoxQty().\'<span class="lowest_quote" id="lowest_quote_\'.$data->id.\'">\'.$data->getLowestQuote().\'</span>
				<span class="quote_limit" id="quote_limit_\'.$data->id.\'">\'.$data->getQuoteLimit().\'</span>\'',
        ),
        array(
            'header'=>'Dimensions - Weight',
            'type'=>'raw',
            'value'=>'$data->getBoxDimsStr()',
        ),
        // array(
			// 'header'=>'Items',
			// 'type'=>'raw',
			// 'value'=>'$data->getItemTable()'
		// ),
        array(
            'header'=>'Order Notes',
            'type'=>'raw',
            'value'=>'nl2br($data->customer_order->billing_info->order_notes)'
        ),
		array(
			'header'=>'Rate Quote in $',
			'type'=>'raw',
			// 'value'=>'($quote = $data->getCurrentUserBidRateQuote()) !== false 
                        // ? ($data->isCurrentBidTooHigh() ? \'<span class="high_quote" id="high_quote_\'.$data->id.\'">\' : \'\').$quote.($data->isCurrentBidTooHigh() ? \'</span>&nbsp;<a href="#" class="edit_quote" rel="\'.$data->id.\'">Edit</a>\' : \'\') 
                        // : \'<span class="rate_quote_text" id="rate_quote_text_\'.$data->id.\'"></span><input type="text" autocomplete="off" class="rate_quote" id="rate_quote_\'.$data->id.\'" rel="\'.$data->id.\'" value="" />\'',
			'value'=>'($quote = $data->getCurrentUserBidRateQuote()) !== false 
                        ? ($data->isCurrentBidTooHigh() ? \'<span class="high_quote" id="high_quote_\'.$data->id.\'">\' : \'\').$quote.($data->isCurrentBidTooHigh() ? \'</span>\' : \'\') 
                        : \'<span class="rate_quote_text" id="rate_quote_text_\'.$data->id.\'"></span><input type="text" autocomplete="off" class="rate_quote" id="rate_quote_\'.$data->id.\'" rel="\'.$data->id.\'" value="" />\'',
		),
		array(
			'header'=>'Rate Quote Number',
			'type'=>'raw',
			'value'=>'($quote_number = $data->getCurrentUserBidRateQuoteNumber()) !== false ? \'<span id="rate_quote_number_span_\'.$data->id.\'">\'.$quote_number.\'</span>\' : \'<span class="rate_quote_number_text" id="rate_quote_number_text_\'.$data->id.\'"></span><input type="text" autocomplete="off" class="rate_quote_number" id="rate_quote_number_\'.$data->id.\'" rel="\'.$data->id.\'" value="" />\'',
		),
	),
));
?>

<?php
	$no_bid = 0;
	foreach ($dataProvider->data as $row){
		if (!$row->getCurrentUserBidRateQuote()){
			$no_bid++;
		}
	}

if ($no_bid):
?>
<input type="button" value="Preview" id="preview_quote" />
<?php
endif;
?>

<input type="button" href="#customer_info_block" id="customer_info_button" style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'#customer_info_button',
	'config'=>array(
		'width'=>350,
		'height'=>230,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="customer_info_block" class="fancy_box_div fancy_form">
		<h2>Customer Shipping Address</h2>
		<div>
			
		</div>
	</div>
</div>

<input type="button" value="Go Back & Edit" id="preview_back" style="display: none" />
<input type="button" value="Submit Quote" id="submit_quote" style="display: none"/>

<script type="text/javascript">
jQuery('#submit_quote').click(function(e){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/place_bids_ajax";
    var ids = [];
    var rate_quotes = [];
    var rate_quote_numbers = [];
    jQuery('.rate_quote').each(function(){
        var id                  = jQuery(this).attr('rel');
        var rate_quote          = jQuery(this).val();
        var rate_quote_number   = jQuery('#rate_quote_number_'+id).val();
        if (rate_quote){
            ids.push(id);
            rate_quotes.push(rate_quote);
            rate_quote_numbers.push(rate_quote_number);
        }
    });
    if (ids.length){
        data = {
            id: ids,
            rate_quote: rate_quotes,
            rate_quote_number: rate_quote_numbers
        };
        jQuery.ajax({
            url:url,
            type: 'post',
            data: data,
            dataType:"json",
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                $.fn.yiiGridView.update('customer-sold-order-grid', {success: function(){
                    window.location.reload();
                    // jQuery('#preview_quote').show();
                    // jQuery('#preview_back').hide();
                    // jQuery('#submit_quote').hide();
                    // jQuery('#preview_back').trigger('click');
                }});
            }
        });
	}
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#preview_quote').click(function(e){
    var high_quotes;
    var i = 0;
    if (high_quotes = validateQuotes()){
        jQuery('.rate_quote').each(function(){
            var id                  = jQuery(this).attr('rel');
            var rate_quote          = jQuery(this).val();
            var rate_quote_number   = jQuery('#rate_quote_number_'+id).val();
            var text_node = jQuery('#rate_quote_text_'+id).text(rate_quote);
            jQuery('#rate_quote_number_text_'+id).text(rate_quote_number);
            if (jQuery.inArray(id, high_quotes) == -1){
				jQuery('#rate_quote_text_'+id+', #rate_quote_number_text_'+id).show();
				jQuery('#rate_quote_'+id+', #rate_quote_number_'+id).hide();
				// text_node.addClass('high_quote');
				jQuery('#rate_quote_'+id).removeClass('high');
            } else {
                jQuery('#rate_quote_'+id).addClass('high');
            }
            i++;
        });
        if (i){
            // jQuery('.rate_quote_text, .rate_quote_number_text').show();
            // jQuery('.rate_quote, .rate_quote_number').hide();
            if (high_quotes.length){
				jQuery('#high_bids_message').show();
			}
            jQuery('#preview_quote').hide();
            jQuery('#preview_back').show();
            jQuery('#submit_quote').show();
        }
    }
	//console.log(high_quotes);
});

jQuery('#preview_back').click(function(e){
    jQuery('.rate_quote, .rate_quote_number').show();
    jQuery('.rate_quote_text, .rate_quote_number_text').hide();
    
    jQuery('#preview_quote').show();
    jQuery('#preview_back').hide();
    jQuery('#submit_quote').hide();
    jQuery('#high_bids_message').hide();
});

// returns false if data is not valid or array of high quotes if it is
function validateQuotes(){
    var high_quotes = [];
	var cancelled = false;
    jQuery('.rate_quote').each(function(){
        var id                  = jQuery(this).attr('rel');
        var rate_quote          = jQuery(this).val();
        var quote_limit 		= parseFloat(jQuery('#quote_limit_'+id).text());
        //console.log(quote_limit);
        var lowest_quote 		= parseFloat(jQuery('#lowest_quote_'+id).text());
        // var rate_quote_number   = jQuery('#rate_quote_number_'+id).val();
        if (rate_quote){
            if(!parseFloat(rate_quote)){
                alert('Please input correct rate quote value for shipment #'+id);
                cancelled = true;
            }
            var rate_quote = parseFloat(rate_quote);
			// quote_limit == -1 means it's a replacement shipment
            if ((rate_quote > quote_limit && quote_limit != -1) || (lowest_quote && rate_quote>lowest_quote)){
                if (confirm('Sorry your quote for shipment #'+id+' is quite too high. Are you sure you want to place this quote?')){
                    high_quotes.push(id);
                } else {
                    cancelled = true;
                }
            }
            
            
/*
            if (lowest_quote){
				lowest_quote = parseFloat(jQuery('#lowest_quote_'+id).text());
				//console.log(lowest_quote);
				if (lowest_quote && rate_quote > lowest_quote){
					if (!confirm('We have a lower quote for shipment #'+id+' of $'+lowest_quote+', for your quote to be accepted, we recommend to submit a quote lower than $'+lowest_quote+'. Are you sure you want to submit your quote anyway?')){
						cancelled = true;
					}
				}
			}
*/
        }
    });
	if (cancelled){
		return false;
	} else {
		return high_quotes;
	}
}

jQuery('.zip').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	jQuery('#customer_info_block > div').html(jQuery('#customer_info_'+id).html());
	jQuery('#customer_info_button').trigger('click');
	e.preventDefault();
    e.stopImmediatePropagation();
});

// jQuery('.edit_quote').live('click', function(e){
    // var target = jQuery(e.target);
    // var id = target.attr('rel');
    // var rate_quote = jQuery('#high_quote_'+id).text();
    // var cell = target.parent();
    // cell.empty();
    // cell.append(jQuery('<span class="rate_quote_text" id="rate_quote_text_'+id+'" />'))
        // .append(jQuery('<input type="text" class="rate_quote" rel="'+id+'" id="rate_quote_'+id+'" />').val(rate_quote));
    
    // var number_span = jQuery('#rate_quote_number_span_'+id);
    // var number = number_span.text();
    // cell = number_span.parent();
    // cell.empty();
    // cell.append(jQuery('<span class="rate_quote_number_text" id="rate_quote_number_text_'+id+'" />'))
        // .append(jQuery('<input type="text" class="rate_quote_number" rel="'+id+'" id="rate_quote_number_'+id+'" />').val(number));
    
    // e.preventDefault();
	// e.stopImmediatePropagation();
// });
</script>
