<?php
foreach ($claim->ind_items as $item){
	if ($item->damaged_boxes){
		$d_boxes = explode(',', $item->damaged_boxes);
		foreach ($d_boxes as $box){
			$boxes[] = $item->items->getSingleBoxDimsStr($box);
		}
	} else {
		$boxes[] = $item->items->getBoxDimsStr();
	}
}

echo implode('<br />', $boxes);