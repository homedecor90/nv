<?php
$provider = $shipment->searchInvoices(false);
if (count($provider->data)){
	$provider->data[0]->_total_unpaid = $total_unpaid;
	$provider->data[0]->_total_claims = $total_claims;
}

$this->widget('CGridViewExt', array(
		'id'=>'customer-shipper-invoices-open-grid',
		'dataProvider'=>$provider,
		'columns'=>array(
				array(
						'class'=>'CCheckBoxColumn',
						'selectableRows' => 2,
						'value'=>'$data->id',
				),
				array(
						'name'=>'id',
						'filter'=>false
				),
				array(
						'header'=>'Destination Zip Code',
						'type'=>'raw',
						'value'=>'$data->customer_order->billing_info->shipping_zip_code',
				),
				array(
						'header'=>'Company',
						'type'=>'raw',
						'value'=>'$data->selected_user_bid->shipper->profile->company.
						($row == 0 ? \'<input type="hidden" id="ajax_total_claims" value="\'.($data->_total_claims!==NULL ? number_format($data->_total_claims,2) : \'\').\'" />\' : \'\')'
				),
				array(
						'header'=>'Total Boxes',
						'type'=>'raw',
						'value'=>'$data->getBoxQty().
						($row == 0 ? \'<input type="hidden" id="ajax_total_unpaid" value="\'.number_format($data->_total_unpaid,2).\'" />\' : \'\')'
				),
				array(
						'header'=>'Customer Shipping Name',
						'type'=>'raw',
						'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_order->customer_id.\'#note">\'.$data->getCustomerShippingName().\'</a>\'',
						'name'=>'_filter_customer_name'
				),
				array(
						'header'=>'Rate Quote',
						'type'=>'raw',
						'value'=>'\'<span id="quote_\'.$data->id.\'" data-quote="\'.$data->selected_user_bid->rate_quote.\'">$\'.number_format($data->selected_user_bid->rate_quote, 2).\'</span><br /><span class="edit_quote clickable" rel="\'.$data->selected_user_bid->id.\'">Edit</span>\'',
				),
				array(
						'header'=>'Rate Quote Number',
						'type'=>'raw',
						'value'=>'$data->selected_user_bid->rate_quote_number',
				),
				array(
						'header'=>'Pickup date',
						'type'=>'raw',
						'value'=>'(strtotime($data->pickup_date)>0 ? date(\'M. d, Y\', strtotime($data->pickup_date)) : \'\').\'<br />\'.round((time() - strtotime($data->pickup_date))/86400).\'&nbsp;days\''
				),
				array(
						'header'=>'Tracking Number',
						'type'=>'raw',
						'value'=>'$data->tracking_number'
				),
				array(
						'header'=>'Damages',
						'type'=>'raw',
						'value'=>'$data->getClaimsListStr()'
				)
		),
));

echo '<p>Total unpaid: $<span id="total_unpaid">'.number_format($total_unpaid, 2).'</span><br />';
echo '<p>Total open claims: <span id="total_claims">-</span><br />';
echo '<p>Total selected: $<span id="payment_total">0</span><br />';
echo '<label for="checknumber">Check Number: </label>';
echo '<input type="text" id="checknumber" />';
echo '</p>';
echo '<input type="button" id="pay_invoices" value="Pay" />';
?>
<script type="text/javascript">

jQuery('.edit_quote').live('click', function(e){
    var target = jQuery(e.target);
    var id = target.attr('rel');
    var new_quote = prompt('Enter new rate quote:', '0');

    if (new_quote == null || typeof(new_quote) == 'undefined'){
        return false;
    }

    new_quote = parseFloat(new_quote);

    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/edit_shipment_bid_ajax/?bid_id=" + id;
    jQuery.ajax({
        url:url,
        data:{
            new_quote: new_quote
        },
        type: 'post',
        dataType: 'json',
        success:function(result){
//            if (result.status == "fail"){
//            alert(result.message);
//                return;
//            }
            $.fn.yiiGridView.update('customer-shipper-invoices-open-grid');
        }
    });

    return false;
});

jQuery('#customer-shipper-invoices-open-grid input[type="checkbox"]').live('change', function (e){
	var total = 0;
	jQuery('#customer-shipper-invoices-open-grid input:checked').each(function(){
		var el = jQuery(this);
		if (el.attr('id') != 'customer-shipper-invoices-open-grid_c0_all'){
			var id = el.val();
			total += parseFloat(jQuery('#quote_'+id).attr('data-quote'));
		}
	});

	//jQuery('#payment_total').text(Math.round(total*100)/100);
	jQuery('#payment_total').text(total);
});

jQuery('#pay_invoices').click(function(){
	var ids = [];
	jQuery('#customer-shipper-invoices-open-grid input:checked').each(function(){
		var el = jQuery(this);
		if (el.attr('id') != 'customer-shipper-invoices-open-grid_c0_all'){
			ids.push(el.val());
		}
	});
	var checknumber = jQuery('#checknumber').val();
	if (ids.length == 0){
		alert('Please select invoices to pay');
		return false;
	}
	if (!checknumber){
		alert('Please enter checknumber');
		return false;
	}

	if (!confirm('Are you sure?')){
		return false;
	}
	var url = global_baseurl+'/index.php/customershipper/pay_shipper_invoices_ajax/';

	jQuery.ajax({
		url:url,
		dataType:"json",
		type: 'post',
		data:{
			shipment_id: ids,
			checknumber: checknumber
		},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			$.fn.yiiGridView.update('customer-shipper-invoices-paid-grid');
			$.fn.yiiGridView.update('customer-shipper-invoices-open-grid');
		}
	});
});

</script>