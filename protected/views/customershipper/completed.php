<h1>Completed Shipments</h1>
<?php
$dataProvider = $shipmentModel->searchCompleted();
// TODO: maybe there's actually some way to make these grids working together on the shipments page but for now we will not use Ext class for the completed tab
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-completed-shipment-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$shipmentModel,
	'columns'=>array(
		array(
			'name'=>'id',
			'filter'=>false
		),
		array(
			'header'=>'Pickup date',
			'value'=>'strtotime($data->pickup_date)>0 ? date(\'M. d, Y \a\t H:i A\', strtotime($data->pickup_date)) : \'\''
		),
        array(
			'header'=>'Destination Zip Code',
            'type'=>'raw',
            'value'=>'$data->customer_order->billing_info->shipping_zip_code
        		.($data->return ? \'<br /><br/>[Return]\' : \'\')',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
			'value'=>'$data->getItemTable(true)',
		),
		array(
            'header'=>'Currently Selected Quote',
            'type'=>'raw',
            'value'=>'isset($data->selected_user_bid)?$data->selected_user_bid->rate_quote:""',
        ),
		array(
            'header'=>'Rate Quote Number',
            'type'=>'raw',
            'value'=>'isset($data->selected_user_bid)?$data->selected_user_bid->rate_quote_number:""',
        ),
		array(
            'header'=>'Customer Shipping Name',
            'type'=>'raw',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->customer_order->customer_id.\'#note">\'.$data->getCustomerShippingName().\'</a>\'',
            'name'=>'_filter_customer_name'
        ),
		array(
            'header'=>'Company',
            'type'=>'raw',
            'value'=>'isset($data->selected_user_bid)?$data->selected_user_bid->shipper->profile->company:""',
        ),
        array(
			'header'=>'Docs',
			'type'=>'raw',
			'value'=>'$data->customer_order->shipping_method == \'Local Pickup\' ? \'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customershipper/pickup_docs/?id=\'.$data->id.\'">Pickup Docs</a>\' : \'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customershipper/view_bol/?id=\'.$data->id.\'">View BOL</a>\''
        ),
        array(
			'header'=>'Tracking Number',
			'type'=>'raw',
			'value'=>'$data->tracking_number'
		),
		array(
			'header'=>'Tracking Website',
			'type'=>'raw',
			'value'=>'$data->tracking_website'
		),
		array(
			'header'=>'Damages',
			'type'=>'raw',
			'value'=>'$data->getCompletedShipmentActionsStr()'
		)
	),
));

?>

<input type="button" value="Authorize Return Form" href="#authorize_return_form_container" id="authorize_return_form_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#authorize_return_form_button',
    'config'=>array(
		'width'=>700,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="authorize_return_form_container" class="fancy_box_div">
<div id="authorize_return_form_content"></div>
</div>
</div>


<input type="button" value="Shipment Damaged" href="#shipment_damage_container" id="shipment_damage_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#shipment_damage_button',
    'config'=>array(
		'width'=>400,
		'height'=>400,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="shipment_damage_container" class="fancy_box_div">
<div id="shipment_damage_content"></div>
</div>

</div>

<script type="text/javascript">
jQuery('.damage').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/shipment_mark_as_damaged_form_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#shipment_damage_content').html(result);
			jQuery('#shipment_damage_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#shipment_damage_submit').live('click', function(e){
	var checked = false;
	var boxes = [];
	var id = jQuery('#shipment_damage_id').val();
	
	jQuery('#shipment_damage_content input:checked').each(function(i){
		checked = true;
		boxes.push(jQuery(this).val());
	});
	
	if (!checked){
		alert('Please select boxes to mark');
		return false;
	}

	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/mark_shipment_as_damaged_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {boxes: boxes},
		success:function(result){
			if (typeof(result.status) == 'undefined'){
				alert(result);
				return;
			}
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				jQuery.fancybox.close();
				refreshWindow();
			}
		},
		error:function(result){
			alert(result.responseText);
		}
	});
});
</script>