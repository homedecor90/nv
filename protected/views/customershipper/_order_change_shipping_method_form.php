<h2>Change Order Shipping Method</h2>
<strong>Current shipping method: </strong><?php echo $order->shipping_method; ?><br /><br />
<p>
<?php
foreach ($shipping_arr as $short_title => $shipping){
	echo '<input type="hidden" id="total_'.$short_title.'" value="'.$shipping['total'].'" />';
	echo '<input type="hidden" id="dsc_total_'.$short_title.'" value="'.$shipping['dsc_total'].'" />';
}

echo '<label for="select_shipping_method">Select Shipping Method: </label>';
echo '<select id="select_shipping_method">';
foreach ($shipping_arr as $value => $shipping){
	echo '<option '.($order->shipping_method == $shipping['title'] ? 'selected="selected" ' : '').'value="'.$value.'">'.$shipping['title'].'</option>';
}
echo '</select>';
?>
</p>

<h3>Order Items</h3>
<table class="items">
	<tr>
		<th>Item</th>
		<th>Qty</th>
		<th>Special Shipping</th>
		<th>New Special Shipping</th>
	</tr>
	<?php 
	foreach ($order->order_items as $items){
		echo '<tr>';
		echo '<td>'.$items->items->item_name.'</td>';
		echo '<td>'.$items->quantity.'</td>';
		echo '<td>$'.number_format($items->special_shipping_price,2).'</td>';
		echo '<td>';
		echo '<input type="hidden" class="current_special" id="current_special_'.$items->id.'" value="'.$items->special_shipping_price.'" />';
		echo '$<input type="text" style="width: 40px;" class="new_special" rel="'.$items->id.'" id="new_special_'.$items->id.'" value="'.$items->special_shipping_price.'" />';
		echo '</td>';
		echo '</tr>';
	}
	?>
</table>

<table>
	<tr>
		<td>
			Current Grand Total: <strong>$<span id="current_total"><?php echo round($current_total, 2); ?></span></strong><br />
			Current Discounted Total: <strong>$<span id="current_dsc_total"><?php echo round($current_dsc_total, 2); ?></span></strong><br />
			Current Profit: <strong>$<?php echo round($order->profit, 2); ?></strong>
		</td>
		<td>
			New Grand Total: <strong>$<span id="new_total"></span></strong><br />
			New Discounted Total: <strong>$<span id="new_dsc_total"></span></strong><br />
			<?php /*Additional Payment: <strong>$<span id="additional_payment"></span></strong>*/?>
		</td>
	</tr>
</table>
<!--label for="custom_dsc_total">Custom Discounted Total: </label-->
<input type="hidden" id="custom_dsc_total" value="" />
<!--label for="additional_payment">Additional Payment Amount: </label-->
<input type="hidden" id="additional_payment" value="" />
<label for="email_notification">Send email notification: </label>
<input type="checkbox" id="email_notification" /><br /><br />

<input type="hidden" id="shipping_method_order_id" value="<?php echo $order->id; ?>" />

<input type="button" id="save_new_shipping_method" value="Save" />