<h1>Customer Shipper Invoices</h1>
<div class="search-form">
    <div class="wide form">
        <form id="filter_form">
            <div class="row">
                <?php echo CHtml::label('Shipper', 'shipper_id'); ?>
                <?php echo CHtml::dropDownList('shipper_id', $shipper_id, $shippers); ?>
            </div>
            <?php echo CHtml::Button('Filter', array('id'=>'button_filter')); ?>
        </form>
    </div>
</div>

<?php
$viewData = array(
		'shipment'=>$shipment,
		'total_unpaid'=>$total_unpaid,
		'total_claims'=>$total_claims
);

$Tabs = array(
		'invoices_open'=>array('title'=>'Open','view'=>"_invoices_open"),
		'invoices_paid'=>array('title'=>'Paid','view'=>"_invoices_paid"),
);
$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));
?>
<script type="text/javascript">
jQuery('#button_filter').click(function(e){
	var search_condition = jQuery('#filter_form').serialize();

	jQuery.fn.yiiGridView.update('customer-shipper-invoices-open-grid', {
		data: search_condition,
		complete: function(){
			var unpaid = jQuery('#ajax_total_unpaid');
			var claims = jQuery('#ajax_total_claims');
			if (unpaid.length){
				jQuery('#total_unpaid').text(unpaid.val());
			} else {
				jQuery('#total_unpaid').text(0);
			}

			if (claims.length && claims.val()){
				jQuery('#total_claims').text('$'+claims.val());
			} else {
				jQuery('#total_claims').text('-');
			}
			
		},
	});
	jQuery.fn.yiiGridView.update('customer-shipper-invoices-paid-grid', {
		data: search_condition
	});
	return false;
});
</script>