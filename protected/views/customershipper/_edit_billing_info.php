<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-order-info-form',
	'enableAjaxValidation'=>false,
)); ?>

	<h2>Billing Info</h2>
	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_name'); ?>
		<?php echo $form->textField($billing_info,'billing_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_last_name'); ?>
		<?php echo $form->textField($billing_info,'billing_last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_street_address'); ?>
		<?php echo $form->textField($billing_info,'billing_street_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_city'); ?>
		<?php echo $form->textField($billing_info,'billing_city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_state'); ?>
		<?php echo $form->textField($billing_info,'billing_state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_zip_code'); ?>
		<?php echo $form->textField($billing_info,'billing_zip_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_phone_number'); ?>
		<?php echo $form->textField($billing_info,'billing_phone_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'billing_email_address'); ?>
		<?php echo $form->textField($billing_info,'billing_email_address'); ?>
	</div>

	<div class="clear"></div>

	<h2>Shipping Info</h2>
	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_name'); ?>
		<?php echo $form->textField($billing_info,'shipping_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_last_name'); ?>
		<?php echo $form->textField($billing_info,'shipping_last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_street_address'); ?>
		<?php echo $form->textField($billing_info,'shipping_street_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_city'); ?>
		<?php echo $form->textField($billing_info,'shipping_city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_state'); ?>
		<?php echo $form->textField($billing_info,'shipping_state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_zip_code'); ?>
		<?php echo $form->textField($billing_info,'shipping_zip_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'shipping_phone_number'); ?>
		<?php echo $form->textField($billing_info,'shipping_phone_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($billing_info,'busrestype'); ?>
		<?php echo $form->dropDownList($billing_info,'busrestype', array('Business'=>'Business', 'Residental'=>'Residental')); ?>
	</div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($billing_info,'order_notes'); ?>
        <?php echo $form->textArea($billing_info,'order_notes', array('cols'=>40, 'rows'=>4)); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($billing_info,'shipping_notes'); ?>
        <?php echo $form->textArea($billing_info,'shipping_notes', array('cols'=>40, 'rows'=>4)); ?>
    </div>
	<?php echo $form->hiddenField($billing_info, 'customer_order_id'); ?>

	<div class="clear"></div>

	<input type="button" value="Save" rel="<?php echo $billing_info->customer_order_id; ?>" id="save_order_info" />

<?php $this->endWidget(); ?>

</div>
