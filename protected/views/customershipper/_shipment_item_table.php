<table>
	<tr>
		<th>Item</th>
		<th>Qty</th>
		<th>Boxes</th>
	</tr>
	<?php
	foreach($items as $item_id=>$i_items):
	?>
	<tr>
		<td><?php echo $i_items[0]->items->item_name; ?></td>
		<td><?php echo count($i_items); ?></td>
		<td><?php 
			//echo $item->items->box_qty * $item->qty;
			$qty = 0;
			foreach ($i_items as $i_item){
				if ($shipment->return){
					if ($i_item->returned_boxes){
						$qty += count(explode(',',$i_item->returned_boxes));
					} else {
						$qty += $i_item->items->box_qty;
					}
				} elseif($i_item->is_replacement && $i_item->replacement_boxes){
					$qty += count(explode(',',$i_item->replacement_boxes));
				} else {
					$qty += $i_item->items->box_qty;
				}
			}
			
			echo $qty;
		?></td>
	</tr>
	<?php
	endforeach;
	?>
</table>
