<h1>Completed Shipments</h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'customer-completed-shipment-grid',
		'dataProvider'=>$dataProvider,
		'columns'=>array(
				array(
						'name'=>'id',
						'filter'=>false
				),
				array(
					'header'=>'Destination Zip Code',
		            'type'=>'raw',
		            'value'=>'$data->customer_order->billing_info->shipping_zip_code
						.($data->return ? \'<br /><br/>[Return]\' : \'\')',
				),
				array(
						'header'=>'Total Boxes',
						'type'=>'raw',
						'value'=>'$data->getBoxQty()',
				),
		        array(
		            'header'=>'Dimensions - Weight',
		            'type'=>'raw',
		            'value'=>'$data->getBoxDimsStr()',
		        ),
				array(
					'header'=>'Rate Quote in $',
					'type'=>'raw',
					'value'=>'$data->getCurrentUserBidRateQuote()',
				),
				array(
					'header'=>'Rate Quote Number',
					'type'=>'raw',
					'value'=>'$data->getCurrentUserBidRateQuoteNumber()',
				),
				array(
					'header'=>'Pickup Date',
					'value'=>'date(\'M. d, Y \a\t H:i A\', strtotime($data->pickup_date))'
				),
				array(
					'header'=>'BOL',
					'type'=>'raw',
					'value'=>'($data->bol_path ? \'<a href="\'.Yii::app()->getBaseUrl(true).\'/customershipper/view_bol/?id=\'.$data->id.\'">Download</a><br />\' : \'\' )'
				),
				array(
						'header'=>'Tracking Number',
						'type'=>'raw',
						'value'=>'$data->tracking_number'
				),
				array(
						'header'=>'Tracking Website',
						'type'=>'raw',
						'value'=>'$data->tracking_website'
				),
				array(
						'header'=>'Damages',
						'type'=>'raw',
						'value'=>'$data->claim ? \'<span class="shipment_damaged">Damaged on \'.date(\'M. d, Y\', strtotime($data->claim_date)).\'</span>\'	: \'\'',
				)
		),
));