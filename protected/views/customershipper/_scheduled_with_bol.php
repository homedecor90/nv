<input type="button" id="download_labels" value="Download Shipping Labels" />
<input type="button" id="download_bols" value="Download BOL files" />

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-shipment-grid-with-bol',
	'dataProvider'=>$dataProviderWithBol,
	'columns'=>array(
		array(
				'class'=>'CCheckBoxColumn',
				'selectableRows' => 2,
				'value'=>'$data->id',
		),
		'id',
		array(
			'header'=>'Pickup date',
			'value'=>'date(\'M. d, Y \a\t H:i A\', strtotime($data->pickup_date))'
		),
        array(
			'header'=>'Destination Zip Code',
            'type'=>'raw',
            'value'=>'\'<span class="clickable zip" rel="\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_zip_code.\'</span>\'
        		.($data->return ? \'<br /><br/>[Return]\' : \'\')	
        		.\'<div style="display: none;" id="customer_info_\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_name.\' \'.$data->customer_order->billing_info->shipping_last_name.\'<br /><br />\'.$data->customer_order->getShippingAddressStr().\'<br /><br />Email: \'.$data->customer_order->customer->email.\'</div>\'',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
			'value'=>'$data->getItemTable()',
		),
		array(
            'header'=>'Currently Selected Quote',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid->rate_quote',
        ),
		array(
            'header'=>'Rate Quote Number',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid->rate_quote_number',
        ),
		array(
            'header'=>'Customer name',
            'type'=>'raw',
            'value'=>'$data->getCustomerBillingName()',
        ),
		array(
            'header'=>'Company',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid->shipper->profile->company',
        ),
        array(
			'header'=>'BOL',
			'type'=>'raw',
			'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/customershipper/view_bol/?id=\'.$data->id.\'">View BOL</a>\''
        ),
        array(
			'header'=>'Tracking Number',
			'type'=>'raw',
			'value'=>'\'<input type="button" class="tracking_number" value="Enter Tracking Number" rel="\'.$data->id.\'" />\''
		),
		array(
			'header'=>'Requeue',
			'type'=>'raw',
			'value'=>'$data->return == 0 ? \'<input type="button" value="Requeue" rel="\'.$data->id.\'" class="requeue" />\' 
				:  \'<input type="button" value="Cancel Return" rel="\'.$data->id.\'" class="cancel_return" />\''
        ),
	),
));

?>
