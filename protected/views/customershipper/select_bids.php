<h1>Select Shipment Bids</h1>
<input type="button" value="Send Shipper Notifications" id="send_notifications" />
<input type="button" value="Generate Quotes" id="generate_quotes" />
<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-shipment-grid',
	'dataProvider'=>$shipmentModel->searchBidding(),
	'columns'=> array(
        array(
            'class'=>'ECheckBoxColumn',
            'selectableRows' => 20,
            'id' => 'selectedIds',
            'disabled' => '!$data->readyForPickupScheduling()'
        ),
        'id',
        array(
            'header'=>'Destination Zip Code',
            'type'=>'raw',
            'value'=>'$data->customer_order->billing_info->shipping_zip_code.($data->readyForPickupScheduling() ? \'<input type="hidden" class="shipment_ready" value="\'.$data->id.\'" />\' : \'\')
        		.($data->return ? \'<br /><br/>[Return]\' : \'\')',
        ),
        array(
            'header'=>'Items',
            'type'=>'raw',
            'value'=>'$data->getItemTable()',
        ),
        array(
            'header'=>'Currently Selected Quote',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid ? \'<a href="#" class="quotes_list" rel="\'.$data->id.\'">\'.($data->selected_user_bid->needsAdditionalPayment() && $data->additionalPaymentAmountToPay()>0 && !$data->additional_payment_disabled ? \'<span rel="\'.$data->id.\'" class="high_quote">\' : \'\').\'$\'.number_format($data->selected_user_bid->rate_quote,2).($data->selected_user_bid->needsAdditionalPayment() ? \'</span>\' : \'\').\'</a>\' : \'\'',
        ),
        array(
            'header'=>'Rate Quote Number',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid->rate_quote_number',
        ),
        array(
            'header'=>'Total Quotes',
            'type'=>'raw',
            'value'=>'$data->total_bids',
        ),
        array(
            'header'=>'Customer name',
            'type'=>'raw',
            //'value'=>'$data->getCustomerShippingName()',
            'value'=>'\'<span class="clickable customer_name" rel="\'.$data->id.\'">\'.$data->getCustomerShippingName().\'</span>\'
				.\'<div style="display: none;" id="customer_info_\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_name.\' \'.$data->customer_order->billing_info->shipping_last_name.\'<br /><br />\'.$data->customer_order->getShippingAddressStr().\'<br /><br />Email: \'.$data->customer_order->customer->email.\'</div>\'',
        ),
        array(
            'header'=>'Company',
            'type'=>'raw',
            'value'=>'$data->selected_user_bid ? $data->selected_user_bid->getCompany() : ""',
        ),
        array(
            'header'=>'Additional Payment',
            'type'=>'raw',
            'value'=>'$data->getAdditionalPaymentColumn()',
        ),
        array(
            'header'=>'Requeue',
            'type'=>'raw',
            'value'=>'$data->return == 0 ? \'<input type="button" value="Requeue" rel="\'.$data->id.\'" class="requeue" />\'
        		: \'<input type="button" value="Cancel Return" rel="\'.$data->id.\'" class="cancel_return" />\''
        ),
    )
));
?>

<input type="button" value="Select and Send for Pickup" href="#select_pickup_date_div" id="pickup_date_button" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#pickup_date_button',
    'config'=>array(
		'width'=>320,
		'height'=>200,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="select_pickup_date_div" class="fancy_box_div">
		<form id="select_pickup_date_form" class="fancy_form">
			<h2>Select Pickup Date and Time</h2>
			<div>
				<label>Pickup Date</label>
				<?php
					Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
					$this->widget('CJuiDateTimePicker',array(
						'mode'=>'datetime', //use "time","date" or "datetime" (default)
						'language'=>'en-GB',
						'name'=>'pickup_date',
						'options'=>array(
							"dateFormat"=>"M. dd, yy",
							"timeFormat"=>"hh:mm tt",
							"ampm"=>true
						),
						'htmlOptions' => array(
							'id' => 'pickup_date'
						)
					));
				?>
			</div>
			<input type="button" value="Submit" id="pickup_date_submit"/>
		</form>
	</div>
</div>

<input type="button" value="Expand Qoutes List" href="#qoutes_list_container" id="qoutes_list_button" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#qoutes_list_button',
    'config'=>array(
		'width'=>400,
		'height'=>200,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="qoutes_list_container" class="fancy_box_div">
<div id="qoutes_list_content"></div>
</div>

</div>

<input type="button" href="#customer_info_block" id="customer_info_button" style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'#customer_info_button',
	'config'=>array(
		'width'=>350,
		'height'=>230,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="customer_info_block" class="fancy_box_div fancy_form">
		<h2>Customer Shipping Address</h2>
		<div>
			
		</div>
	</div>
</div>


<script type="text/javascript">
jQuery('.quotes_list').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/shipment_quotes_list_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		success:function(result){
			// if (result.status == "fail"){
				// alert(result.message);
				// return;
			// }
			jQuery('#qoutes_list_content').html(result);
			jQuery('#qoutes_list_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

function refreshWindow(){
	$.fn.yiiGridView.update('customer-shipment-grid');
}

jQuery('.select_quote').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/select_shipment_bid_ajax/?bid_id=" + id;
	jQuery.fancybox.close();
	jQuery.ajax({
		url:url,
        dataType: 'json',
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
});

jQuery('.cancel_quote').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/cancel_shipment_bid_ajax/?bid_id=" + id;
	jQuery.fancybox.close();
	jQuery.ajax({
		url:url,
        dataType: 'json',
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
});

jQuery('#send_notifications').click(function(e){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/send_shipper_notifications_ajax";
	
	jQuery.ajax({
		url:url,
		dataType: 'json',
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			alert('Notification sent');
		}
	});
});

jQuery('#generate_quotes').click(function(e){
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/generate_shipping_services_bids_ajax";
	
	jQuery.ajax({
		url:url,
		dataType: 'json',
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			alert('Quotes generated');
			refreshWindow();
		}
	});
});

jQuery('#pickup_date_submit').click(function(e){
	var date = jQuery('#pickup_date').val();
	var id = [];
    var selected = [];
    $('[name="selectedIds[]"]:checked').each(function(){
        selected.push($(this).val());
    });
    if (selected.length <= 0) {
        alert('Please, select some shipments first');
        jQuery.fancybox.close();
        return false;
    }
    for (var shipmentId in selected)
        id.push('shipment_id[]=' + selected[shipmentId]);
//    console.debug(id);
//    return false;
//	jQuery('.shipment_ready').each(function(){
//		id.push('shipment_id[]='+jQuery(this).val());
//	});
	
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/send_shipments_for_shippers_pickup_ajax/?"+id.join('&')+'&date='+date;
	jQuery.fancybox.close();
	jQuery.ajax({
		url:url,
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
});

jQuery('.send_payment_notification').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/send_payment_notification_ajax/?shipment_id=" + id;
	jQuery.fancybox.close();
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
});

jQuery('.disable_additional_payment').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/disable_additional_payment_ajax/?id=" + id;
	jQuery.fancybox.close();
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
});

jQuery('.customer_name').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	jQuery('#customer_info_block > div').html(jQuery('#customer_info_'+id).html());
	jQuery('#customer_info_button').trigger('click');
	e.preventDefault();
    e.stopImmediatePropagation();
});

</script>
