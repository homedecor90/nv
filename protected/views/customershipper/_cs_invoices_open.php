<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'customer-shipper-invoices-open-grid',
		'dataProvider'=>$shipment->searchInvoices(false, Yii::app()->user->id),
		'columns'=>array(
				array(
						'class'=>'CCheckBoxColumn',
						'selectableRows' => 2,
						'value'=>'$data->id',
				),
				array(
						'name'=>'id',
						'filter'=>false
				),
				array(
						'header'=>'Destination Zip Code',
						'type'=>'raw',
						'value'=>'$data->customer_order->billing_info->shipping_zip_code',
				),
				array(
						'header'=>'Company',
						'type'=>'raw',
						'value'=>'$data->selected_user_bid->shipper->profile->company',
				),
				array(
						'header'=>'Total Boxes',
						'value'=>'$data->getBoxQty()'
				),
				array(
						'header'=>'Customer Shipping Name',
						'type'=>'raw',
						'value'=>'$data->getCustomerShippingName()',
				),
				array(
						'header'=>'Rate Quote',
						'type'=>'raw',
						'value'=>'\'<span id="quote_\'.$data->id.\'" data-quote="\'.$data->selected_user_bid->rate_quote.\'">$\'.number_format($data->selected_user_bid->rate_quote, 2).\'</span>\'',
				),
				array(
						'header'=>'Rate Quote Number',
						'type'=>'raw',
						'value'=>'$data->selected_user_bid->rate_quote_number',
				),
				array(
						'header'=>'Pickup date',
						'type'=>'raw',
						'value'=>'(strtotime($data->pickup_date)>0 ? date(\'M. d, Y\', strtotime($data->pickup_date)) : \'\').\'<br />\'.round((time() - strtotime($data->pickup_date))/86400).\'&nbsp;days\''
				),
				array(
						'header'=>'Tracking Number',
						'type'=>'raw',
						'value'=>'$data->tracking_number'
				),
                array(
                    'header'=>'Damages',
                    'type'=>'raw',
                    'value'=>'$data->getClaimsListStr()'
                )
		),
));

?>
