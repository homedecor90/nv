<h2>Select boxes to return</h2>

<table class="items">
<tr>
	<th>Item</th>
	<th>Boxes</th>
</tr>
<?php
foreach ($items as $item):
// if (!$item->customer_return_id):
?>

<tr>
	<td width="25%"><?php echo $item->items->item_name; ?></td>
	<td>
	<?php
	if ($item->customer_return_id){
		echo '[Returned]';
		continue;
	}
	$damaged_boxes = $item->getDamagedBoxes();
// 	for ($i=1; $i<=$item->items->box_qty; $i++){
// 		$boxes[] = $i; 
// 	}
	$lines = array();
	$boxes = array();
	if ($item->is_replacement){
		$boxes = explode(',', $item->replacement_boxes);
	} else {
		for($box=1; $box<=$item->items->box_qty; $box++){
			$boxes[] = $box;
		}
	}
	foreach ($boxes as $box){
		$val = $item->id.'_'.$box;
		$size = $item->items->getSingleBoxDimsStr($box);
		
		$line = '';
		$line .= '<input type="checkbox" value="'.$val.'" id="box_'.$val.'" />';
		$line .= '<label for="box_'.$val.'">'.$size.'</label>';		
		if (in_array($box, $damaged_boxes)){
			$line .= ' [Damaged]';
		}
		$lines[] = $line;
	}
	echo implode('<br />', $lines);
	?>
	</td>
</tr>

<?php
// endif;
endforeach;
?>
</table>
<input type="hidden" value="<?php echo $shipment->id; ?>" id="authorize_return_shipment_id" />
<input type="button" value="Submit" id="authorize_return_submit" />