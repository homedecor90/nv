<h1>Approve/Delete Order Info Changes</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-order-info-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'customer_order_id',
		array(
			'header' => 'Customer',
			'value' => '$data->customer_order->customer->first_name.\' \'.$data->customer_order->customer->last_name',
		),
		array(
			'header' => 'Billing Address',
			'type' => 'raw',
			'value' => '$data->getBillingAddress()',
		),
		array(
			'header' => 'Shipping Address',
			'type' => 'raw',
			'value' => '$data->getShippingAddress()',
		),
		array(
			'header' => 'Approve',
			'type'=>'raw',
			'value'=>'\'<input type="button" class="approve" rel="\'.$data->id.\'" value="Approve" />\''
		),
		array(
			'header' => 'Delete',
			'type'=>'raw',
			'value'=>'\'<input type="button" class="delete" rel="\'.$data->id.\'" value="Delete" />\''
		)
	)
));
?>

<script type="text/javascript">

jQuery('.approve').live('click', function(e){
    if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
    var id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/approve_order_info_changes_ajax/?id="+id;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            $.fn.yiiGridView.update('customer-order-info-grid');
        },
        dataType: 'json'
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('.delete').live('click', function(e){
    if (!confirm('Are you sure?')){
		return false;
	}
	var target = jQuery(e.target);
    var id = target.attr('rel');
    var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/delete_order_info_changes_ajax/?id="+id;
    
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            $.fn.yiiGridView.update('customer-order-info-grid');
        },
        dataType: 'json'
    });
    
    e.preventDefault();
    e.stopImmediatePropagation();
});

</script>
