<table class="damaged_items" id="claim_<?php echo $claim->id; ?>" rel="<?php echo $claim->id; ?>">
<tr>
	<th>Item</th>
	<th>Boxes</th>
	<th>Status</th>
</tr>
<?php
foreach ($claim->ind_items as $item){
	echo '<tr>';
	echo '<td>';
	$class = array();
	if ($item->replaced) {
		$class[] = 'replaced';
	}
	if ($item->customer_return_id) {
		$class[] = 'returned';
	}
	if ($item->claim_refund_id){
		$class[] = 'refunded';		
	}
	echo '<input data-cbm="'.$item->getDamagedBoxesCBM().'" data-shipping-price="'.$item->items->$shipping_field.'" data-claim="'.$claim->id.'" data-fob="'.$item->items->fob_cost_price.'" class="'.implode(' ', $class).'" type="checkbox" value="'.$item->id.'" />';
	echo $item->items->item_name;
	echo '</td>';
	echo '<td>';
	if ($item->damaged_boxes){
		$boxes = explode(',', $item->damaged_boxes);
	} else {
		// empty fiels means all boxes are damaged
		$boxes = array();
		for($i=1; $i<=$item->items->box_qty; $i++){
			$boxes[] = $i;
		}
	}
	foreach ($boxes as $box){
		echo $item->items->getSingleBoxDimsStr($box).'<br />';
	}
	echo '</td>';
	echo '<td>';

	$status = array();
	
	if ($item->replaced){
		$status[] = 'Replaced';
	}
	if ($item->reordered_from_supplier){
		$status[] = 'Reordered from Supplier';
	}
	if ($item->claim_refund_id){
		$status[] = 'Partial Refund';
	}
	if ($item->customer_return_id){
		$status[] = 'Returned';
	}
	echo implode('<br /><br />',$status);
	echo '</td>';
	echo '</tr>';
}
?>
</table>
<?php
if(Yii::app()->user->isAdmin()):
?>
<div class="order_ant_profit" id="order_profit_<?php echo $claim->id; ?>">
Order Profit: <span>$<?php echo number_format($shipment->customer_order->getProfitWithReplacementsAndRefunds(),2); ?></span>
</div>
<input type="hidden" id="order_profit_value_<?php echo $claim->id; ?>" value="<?php echo round($shipment->customer_order->getProfitWithReplacementsAndRefunds(),2); ?>" />
<?php
endif;
?>