<h1>Customer Shipper Invoices</h1>
<?php
$viewData = array(
		"shipment"=>$shipment,
);

$Tabs = array(
		'with_bol'=>array('title'=>'Open','view'=>"_cs_invoices_open"),
		'without_bol'=>array('title'=>'Paid','view'=>"_cs_invoices_paid"),
);
$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));
?>
