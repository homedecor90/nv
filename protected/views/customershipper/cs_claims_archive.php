<h1>Claims Archive</h1>
<?php

$this->menu=array(
		array('label'=>'Open Claims', 'url'=>array('customershipper/cs_claims')),
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-shipment-claims-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
		array(
			'header'=>'Destination Zip Code',
			'type'=>'raw',
			'value'=>'$data->shipment->customer_order->billing_info->shipping_zip_code'
		),
		array(
			'header'=>'Damaged Boxes',
			'type'=>'raw',
			'value'=>'$data->getDamagedBoxesListCS(\'shipper\')'
		),
		array(
			'header'=>'Rate Quote',
			'value'=>'$data->shipment->selected_user_bid->rate_quote',
		),
		array(
			'header'=>'Rate Quote Number',
			'type'=>'raw',
			'value'=>'$data->shipment->selected_user_bid->rate_quote_number',
		),
		array(
			'header'=>'Pickup date',
			'value'=>'date(\'M. d, Y\', strtotime($data->shipment->pickup_date))'
		),
		array(
			'header'=>'Damaged On',
			'value'=>'date(\'M d, Y\', strtotime($data->claim_date))',
		),
		array(
			'header'=>'Resolved On',
			'value'=>'date(\'M d, Y\', strtotime($data->claim_resolved_date))',
		),
		array(
			'header'=>'Claim Amount',
			'value'=>'\'$\'.number_format($data->amount, 2)'
		),
	),
));
?>