<h1>Cancelled Shipping Orders</h1>

<?php 
$customerOrderModel->status = CustomerOrder::$STATUS_SOLD;
$this->widget('CGridViewExt', array(
	'id'=>'customer-cancelled-order-grid',
	'dataProvider'=>$customerOrderModel->searchCancelled(),
	'columns'=>array(
		'id',
        array(
			'header'=>'Sold On',				
			'value'=>'date("M. d, Y", strtotime($data->sold_date))',
		),
        array(
			'header'=>'Billing Name',
            'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/customer/\'.$data->customer_id.\'">\'.$data->billing_info->billing_name.\' \'.$data->billing_info->billing_last_name.\'</a>\'',
			'type'=>'raw',
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
            'value'=>'CustomerOrder::model()->getCustomerOrderItemsShort($data->id)',
		),
		// array(
			// 'header'=>'Shipping Method',
			// 'type'=>'raw',
            // 'value'=>'\'<span id="shipping_method_\'.$data->id.\'">\'.$data->shipping_method.\'</span>\'',
		// ),
        //'discounted',
		array(
			'header'=>'Price',
			'type'=>'raw',
            'value'=>'\'Total: $\'.$data->grand_total_price.\'<br />Discounted: $\'.$data->discounted_total',
		),
        array(
            'header'=>'Order Notes',
            'type'=>'raw',
            'value'=>'nl2br($data->billing_info?$data->billing_info->order_notes:\'\')'
        ),
		array(
			'header'=>'Cancellation Date',
			'value'=>'strtotime($data->cancellation_date)>0 ? date("M. d, Y", strtotime($data->cancellation_date)) : \'\'',
		)
	),
));
?>