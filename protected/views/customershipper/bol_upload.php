<?php
$shipmentModel->shipper_id = Yii::app()->user->id;
$dataProvider = $shipmentModel->searchShipperPickup();
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-sold-order-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
        array(
			'header'=>'Destination Zip Code',
            'type'=>'raw',
            'value'=>'\'<span class="clickable zip" rel="\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_zip_code.\'</span>\'
        		.($data->return ? \'<br /><br/>[Return]\' : \'\')
        		.\'<div style="display: none;" id="customer_info_\'.$data->id.\'">\'.$data->customer_order->billing_info->shipping_name.\' \'.$data->customer_order->billing_info->shipping_last_name.\'<br /><br />\'.$data->customer_order->getShippingAddressStr().\'<br /><br />Email: \'.$data->customer_order->customer->email.\'</div>\'',
		),
        array(
            'header'=>'Dimensions - Weight',
            'type'=>'raw',
            'value'=>'$data->getBoxDimsStr()',
        ),
		array(
			'header'=>'Rate Quote in $',
			'type'=>'raw',
			'value'=>'$data->getCurrentUserBidRateQuote()',
		),
		array(
			'header'=>'Rate Quote Number',
			'type'=>'raw',
			'value'=>'$data->getCurrentUserBidRateQuoteNumber()',
		),
		array(
			'header'=>'Pickup Date',
			'value'=>'date(\'M. d, Y \a\t H:i A\', strtotime($data->pickup_date))'
		),
		array(
			'header'=>'BOL',
			'type'=>'raw',
			'value'=>'($data->bol_path ? \'<a href="\'.Yii::app()->getBaseUrl(true).\'/customershipper/view_bol/?id=\'.$data->id.\'">[Uploaded]</a><br />\' : \'\' ).\'<input type="button" class="upload_button" value="\'.($data->bol_path ? \'Reupload\' : \'Upload BOL\').\'" rel="\'.$data->id.\'" />\''
		),
	),
));
?>

<input type="button" value="Upload BOL" href="#uploadBolDiv" id="uploadBolButton" style="display:none;"/>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>'#uploadBolButton',
    'config'=>array(
		'width'=>400,
		'height'=>180,
		'autoDimensions'=>false,
	),
));
?>

<div class="fancy_box_div_wrapper" style="display:none;">
<div id="uploadBolDiv" class="fancy_box_div">
<?php
$site = nl2br(YumUser::model()->findByPk(Yii::app()->user->id)->profile->website);
$sites = explode("<br />", $site);

foreach ($sites as $site){
	$options[base64_encode(trim($site))] = $site;
}

echo '<label for="shipper_website">Select website: </label>';

echo CHtml::dropDownList('shipper_website', '', $options);

echo '<br /><br />';
echo '<div id="progress"></div>';
// Ajax Upload View Part
$this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
	'id'=>'uploadBol',
	'config'=>array(
		'action'=>Yii::app()->createUrl('customershipper/upload_bol_ajax'),
		'allowedExtensions'=>array("pdf","jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
		'sizeLimit'=>10*1024*1024,// maximum file size in bytes
		'minSizeLimit'=>0,// minimum file size in bytes
        'multiple'=>false,
		'onComplete'=>"js:function(id, file_name, responseJSON){ onCompleteUploadBol(file_name); }",
		'onSubmit'=>"js:function(id, file_name){ jQuery('#shipper_website').attr('disabled', 'disabled'); }",
		//'onProgress'=>"js:function(id, file_name, loaded, total){ onProgessUploadBol(file_name, loaded, total); }"
	)
)); 
?>

</div>
</div>

<input type="button" href="#customer_info_block" id="customer_info_button" style="display: none;" />
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
	'target'=>'#customer_info_button',
	'config'=>array(
		'width'=>350,
		'height'=>230,
		'autoDimensions'=>false,
	),
));
?>
<div class="fancy_box_div_wrapper" style="display:none;">
	<div id="customer_info_block" class="fancy_box_div fancy_form">
		<h2>Customer Shipping Address</h2>
		<div>
			
		</div>
	</div>
</div>

<script type="text/javascript">

var currently_uploading = null;

jQuery('.upload_button').live('click', function(e){
	currently_uploading = jQuery(e.target).attr('rel');
	jQuery('#shipper_website').removeAttr('disabled');
	jQuery('#uploadBolButton').trigger('click');
});

jQuery('.zip').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	jQuery('#customer_info_block > div').html(jQuery('#customer_info_'+id).html());
	jQuery('#customer_info_button').trigger('click');
	e.preventDefault();
    e.stopImmediatePropagation();
});

function onCompleteUploadBol(file_name){
	jQuery.fancybox.close();
	var website = encodeURIComponent(jQuery('#shipper_website').val());
	var url = "<?php echo Yii::app()->getBaseUrl(true);?>/index.php/customershipper/set_bol_filename_ajax/?shipment_id=" + currently_uploading + '&filename=' + file_name + '&website=' + website;	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
                alert(result.message);
                return;
            }
			
            if (result.status == "success"){
				$.fn.yiiGridView.update('customer-sold-order-grid');
			}
		}
	});

	jQuery("#progress").html("");
}

</script>
