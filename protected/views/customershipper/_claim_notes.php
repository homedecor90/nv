<h2>Notes for the claim #<?php echo $claim->id; ?></h2>
<textarea cols="60" rows="3" id="new_claim_note"></textarea><br />
<input type="button" rel="<?php echo $claim->id; ?>" value="Add New Note" class="add_new_claim_note" /><br /><br />

<table class="items">
	<tr>
		<th>Date</th>
		<th>Text</th>
	</tr>
<?php
	foreach ($notes as $note){
		echo '<tr>';
		echo '<td style="text-align: left;">'.date('M. d', strtotime($note->created_on)).'</td>';
		echo '<td style="text-align: left;">'.$note->content.'</td>';
		echo '</tr>';
	} 
?>
</table>
 