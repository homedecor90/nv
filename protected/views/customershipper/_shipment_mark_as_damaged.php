<h2>Mark Shipment #<?php echo $shipment->id; ?> as Damaged</h2>
<strong>Select damaged boxes:</strong>
<table class="items">
<tr>
	<th>Item</th>
	<th>Boxes</th>
</tr>
<?php
foreach ($items as $item):
if ($item->damaged){
	continue;
}
?>

<tr>
	<td><?php echo $item->items->item_name; ?></td>
	<td>
	<?php
	for ($i = 1; $i<=$item->items->box_qty; $i++){
		$val = $item->id.'_'.$i;
		$size = $item->items->getSingleBoxDimsStr($i);
		echo '<input type="checkbox" value="'.$val.'" id="box_'.$val.'" />';
		echo '<label for="box_'.$val.'">'.$size.'</label><br />';
	}
	?>
	</td>
</tr>

<?php
endforeach;
?>
</table>
<input type="hidden" value="<?php echo $shipment->id; ?>" id="shipment_damage_id" />
<input type="button" value="submit" id="shipment_damage_submit" />