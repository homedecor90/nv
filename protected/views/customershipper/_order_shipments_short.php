<?
/**
 * @var $this CustomershipperController
 * @var $shipments CustomerOrderShipment[]
 * @var $order CustomerOrder
 */
?>
<table class="shipment_table">
<?php
foreach ($shipments as $shipment):
	if ($shipment->status == CustomerOrderShipment::$STATUS_PENDING):
?>
	<tr class="shipment">
		<?php /*
		<td><?php echo $shipment->id; ?></td>
		
		<td><?php 
			echo $shipment->status;
		?></td>
		*/?>
		<?php
		/*
		<td><?php echo $shipment->countTotal(); ?></td>
		<td><?php echo $shipment->countInStock(); ?></td>
		<td><?php echo $shipment->countIncoming(); ?></td>
		<td><?php echo $shipment->countNotOrdered(); ?></td>
		<td><?php echo $shipment->countCancelled(); ?></td>
		*/?>
		<td>
			<table>
				<?php
				foreach ($shipment->active_ind_items as $item):
// 				if($item->status != Individualitem::$STATUS_CANCELLED_SHIPPING 
// 							&& $item->status != Individualitem::$STATUS_SCANNED_OUT):
				?>
				<tr>
					<td>
					<?php 
						if ($item->prev_shipment_id){
							echo '<span class="imported_item" title="combined shipments">';
						}
						echo $item->items->item_name;
						if ($item->prev_shipment_id){
							echo '</span>';
						} 
					?>
					</td>
					<td><?php echo $item->items->item_code; ?></td>
					<td>
						<?php 
						if ($item->_available_for_shipping){
							echo '<strong>';
						}
						?>
						<span class="ind_item <?php echo $item->getShortStatus(); ?>">
						<?php 
							if ($item->getStatus() == Individualitem::$STATUS_ORDERED_WITH_ETA){
								echo date('M. d, Y', strtotime($item->getEta()));
							} else {
								echo $item->getStatus();
							}
						?></span>
						<?php 
						if ($item->_available_for_shipping){
							echo '</strong>';
						}
						?>
					</td>
					<td>
						<?php
						if (YumUser::isOperator(true)):
						?>
						<a href="#" class="cancel_item" rel="<?php echo $item->id; ?>">Cancel</a>
						<?php
						endif;
						if ($order->payment_approved && $item->_available_for_shipping):
                        ?>
                        &nbsp;/&nbsp;<a href="#" class="scan_item_out" rel="<?php echo $item->id; ?>">Scan Out</a>
                        <?php
                        endif;
                        ?>
                    </td>
				</tr>
				<?php
// 				endif;
				endforeach;
				?>
			</table>
		</td>
		<td>
			<?php
			//if ($shipment->status == CustomerOrderShipment::$STATUS_PENDING && !$shipment->customer_order->verificationRequired()) {
			if ($shipment->status == CustomerOrderShipment::$STATUS_PENDING && $shipment->readyForShipping() && $order->payment_approved) {?>
                <? if ($order->shipping_method == 'Local Pickup'):?>
                    <a href="#" class="schedule_pickup" rel="<?php echo $shipment->id?>">Schedule Pickup</a>
                <? else:?>
                    <a href="#" class="send_for_bidding" data-target="shipment" rel="<?php echo $shipment->id?>">Send for Bidding</a>
                <? endif;?>
                <br><br>
			<? }?>
			<?php if ($shipment->status != CustomerOrderShipment::$STATUS_SCANNED_OUT 
					&& $shipment->status != CustomerOrderShipment::$STATUS_CANCELLED
					&& $shipment->readyForShipping()){?>
                <? if ($order->payment_approved):?>
				    <a href="#" rel="<?php echo $shipment->id; ?>" class="scan_shipment_out">
                        Scan As OUT
                    </a>
                <? endif;?>
				<?php
				if (YumUser::isOperator(true)):
				?>
				<br /><br />
				<a href="#" rel="<?php echo $shipment->id; ?>" class="cancel_shipment">Cancel</a>
				<br /><br />			
				<?php
				endif;
			} ?> 
			<a href="#" rel="<?php echo $shipment->id; ?>" class="combine_shipment">Combine</a>
		</td>
	</tr>
<?php
	endif;
endforeach;
?>
</table>
<input type="button" class="order_expand_shipment_details" rel="<?php echo $order->id;?>" value="Split Or Combine Order" />
