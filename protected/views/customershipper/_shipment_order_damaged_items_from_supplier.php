<h2>Select boxes to order from supplier</h2>
<?php echo CHtml::label('Supplier', 'supplier_id'); ?>:&nbsp;
<?php echo CHtml::dropDownList('supplier_id', 0, $supplierList);  ?><br /><br />
<table class="items">
<tr>
	<th>Item</th>
	<th>Boxes</th>
</tr>
<?php
foreach ($items as $item):
if (!$item->reordered_from_supplier):
?>

<tr>
	<td><?php echo $item->items->item_name; ?></td>
	<td>
	<?php
	for ($i = 1; $i<=$item->items->box_qty; $i++){
		$val = $item->id.'_'.$i;
		$size = $item->items->getSingleBoxDimsStr($i);
		echo '<input type="checkbox" value="'.$val.'" id="box_'.$val.'" />';
		echo '<label for="box_'.$val.'">'.$size.'</label><br />';
	}
	?>
	</td>
</tr>

<?php
endif;
endforeach;
?>
</table>
<input type="hidden" value="<?php echo $claim->id; ?>" id="supplier_order_shipment_claim_id" />
<input type="button" value="Submit" id="supplier_order_submit" />