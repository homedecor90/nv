<h1>Claims Archive</h1>
<?php

// $this->menu=array(
// 		array('label'=>'Open Claims', 'url'=>array('customershipper/claims')),
// );

$dataProvider = $claimModel->search(true);
$this->widget('CGridViewExt', array(
	'id'=>'customer-shipment-claims-archive-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
		array(
			'header'=>'Destination Zip Code',
			'type'=>'raw',
			'value'=>'$data->shipment->customer_order->billing_info->shipping_zip_code'
		),
		array(
			'header'=>'Company',
			'value'=>'$data->shipment->selected_user_bid->shipper->profile->company'
		),
		array(
			'header'=>'Items',
			'type'=>'raw',
			'value'=>'$data->getDamagedBoxesList()'
		),
		array(
			'header'=>'Customer Shipping Name',
			'type'=>'raw',
			'value'=>'\'<a href="\'.Yii::app()->getBaseUrl(true).\'/index.php/customer/\'.$data->shipment->customer_order->customer_id.\'#note">\'.$data->shipment->getCustomerShippingName().\'</a>\'',
		),
		array(
			'header'=>'Rate Quote',
			'value'=>'"$".number_format($data->shipment->selected_user_bid->rate_quote, 2)',
		),
		array(
			'header'=>'Rate Quote Number',
			'type'=>'raw',
            'value'=>'$data->shipment->selected_user_bid->rate_quote_number.($data->shipment->paid ? \'<br /><span style="font-size: 10px">/Paid on \'.date(\'M. d\', strtotime($data->shipment->paid_date)).\'/</span>\' : "")',
		),
		array(
			'header'=>'Pickup date',
			'value'=>'(strtotime($data->shipment->pickup_date) > 0) ? date(\'M. d, Y\', strtotime($data->shipment->pickup_date)) : \'\''
		),
		array(
			'header'=>'Damaged On',
			'value'=>'date(\'M d, Y\', strtotime($data->claim_date))',
		),
		array(
			'header'=>'Resolved On',
			'value'=>'date(\'M d, Y\', strtotime($data->claim_resolved_date))',
		),
		array(
			'header'=>'Claim Amount',
			'type'=>'raw',
			'value'=>'"$".number_format($data->amount, 2)'
		),
	),
));
?>