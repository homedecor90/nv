<h2>Select Different Quote</h2>
<table>
	<tr>
		<th>
			Shipper
		</th>
		<th>
			Rate Quote
		</th>
	</tr>
<?php
	foreach ($bids as $bid){
		echo '<tr>';
		echo '<td>'.$bid->getCompany().'</td>';
		echo '<td><a href="#" class="select_quote" rel="'.$bid->id.'">';
		if ($bid->needsAdditionalPayment()){
			echo '<span class="high_quote" rel="'.$bid->id.'">';
		}
			echo '$'.number_format($bid->rate_quote, 2);
		if ($bid->needsAdditionalPayment()){
			echo '</span>';
		}
		echo '</a></td>';
		echo '<td>';
		echo '<a href="#" class="cancel_quote" rel="'.$bid->id.'">Cancel</a>';
		echo '</td>';
		echo '</tr>';
	}
?>
</table>
