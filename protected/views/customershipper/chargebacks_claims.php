<?php
$this->menu=array(
	array('label'=>'Pending Shipping Orders', 'url'=>array('customershipper/pending_orders')),
	array('label'=>'Shipments', 'url'=>array('customershipper/shipments')),
);
?>
<h1>Chargebacks / Claims</h1>
<?php

$viewData = array(
		"customerOrderModel"=>$customerOrderModel,
		"shipmentModel"=>$shipmentModel,
		"claimModel"=>$claimModel
);

$Tabs = array();

if (YumUser::isOperator(true)){
	$Tabs['tab_claim'] = array('title'=>'Claims','view'=>"claims");
	$Tabs['tab_claim_archive'] = array('title'=>'Claims Archive','view'=>"claims_archive");
}

$Tabs['tab_cancelled'] = array('title'=>'Cancelled Orders','view'=>"cancelled");
$Tabs['tab_chargebacks'] = array('title'=>'Chargebacks','view'=>"chargebacks");

$this->widget('CTabView', array('tabs'=>$Tabs, 'viewData'=>$viewData));
?>

<script type="text/javascript">

refreshWindow = function(){
	$.fn.yiiGridView.update('customer-shipment-claims-archive-grid');
	$.fn.yiiGridView.update('customer-shipment-claims-grid');
}

</script>