<h1>Settings</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'tax_percent'); ?>
		<?php echo $form->textField($model,'tax_percent'); ?>
		<?php echo $form->error($model,'tax_percent'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'company_name'); ?>
        <?php echo $form->textField($model,'company_name'); ?>
        <?php echo $form->error($model,'company_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'company_name_2'); ?>
        <?php echo $form->textField($model,'company_name_2'); ?>
        <?php echo $form->error($model,'company_name_2'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'sales_phone_number'); ?>
        <?php echo $form->textField($model,'sales_phone_number'); ?>
        <?php echo $form->error($model,'sales_phone_number'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'customer_service_phone_number'); ?>
        <?php echo $form->textField($model,'customer_service_phone_number'); ?>
        <?php echo $form->error($model,'customer_service_phone_number'); ?>
    </div>
    <fieldset>
    <legend>Company address:</legend>
        <div class="row">
            <?php echo $form->labelEx($model,'company_address_street'); ?>
            <?php echo $form->textField($model,'company_address_street'); ?>
            <?php echo $form->error($model,'company_address_street'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'company_address_city'); ?>
            <?php echo $form->textField($model,'company_address_city'); ?>
            <?php echo $form->error($model,'company_address_city'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'company_address_state'); ?>
            <?php echo $form->textField($model,'company_address_state'); ?>
            <?php echo $form->error($model,'company_address_state'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'company_address_zip'); ?>
            <?php echo $form->textField($model,'company_address_zip'); ?>
            <?php echo $form->error($model,'company_address_zip'); ?>
        </div>
    </fieldset>
    <fieldset>
        <legend>Payment Processor</legend>
        <?php echo $form->labelEx($model,'payment_processor'); ?>
        <?php
            $processors = array_combine(
                array_keys(Yii::app()->params['paymentProcessors']),
                array_keys(Yii::app()->params['paymentProcessors'])
            );
            echo $form->dropdownList($model,'payment_processor',$processors);
        ?>
        <?php echo $form->error($model,'payment_processor'); ?>
    </fieldset>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->