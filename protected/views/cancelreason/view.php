<?php
$this->breadcrumbs=array(
	'Cancellation Reasons'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Cancellation Reasons', 'url'=>array('index')),
	array('label'=>'Create Cancellation Reason', 'url'=>array('create')),
	array('label'=>'Update Cancellation Reason', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Cancellation Reason', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cancellation Reasons', 'url'=>array('admin')),
);
?>

<h1>View CancellationReason #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reason',
	),
)); ?>
