<?php
$this->breadcrumbs=array(
	'Cancellation Reasons'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cancellation Reasons', 'url'=>array('index')),
	array('label'=>'Create Cancellation Reason', 'url'=>array('create')),
	array('label'=>'View Cancellation Reason', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Cancellation Reasons', 'url'=>array('admin')),
);
?>

<h1>Update CancellationReason <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>