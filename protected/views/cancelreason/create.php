<?php
$this->breadcrumbs=array(
	'Cancellation Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cancellation Reasons', 'url'=>array('index')),
	array('label'=>'Manage Cancellation Reasons', 'url'=>array('admin')),
);
?>

<h1>Create CancellationReason</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>