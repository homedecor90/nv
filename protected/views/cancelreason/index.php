<?php
$this->breadcrumbs=array(
	'Cancellation Reasons',
);

$this->menu=array(
	array('label'=>'Create Cancellation Reason', 'url'=>array('create')),
	array('label'=>'Manage Cancellation Reasons', 'url'=>array('admin')),
);
?>

<h1>Cancellation Reasons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
