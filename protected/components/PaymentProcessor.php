<?php
/**
 * Created by JetBrains PhpStorm.
 */

class PaymentProcessor extends CApplicationComponent {
    public $processor;
    private $settings;

    public function init()
    {
        $this->processor = Settings::getVar('payment_processor');
        $this->settings = Yii::app()->params['paymentProcessors'][$this->processor];
        parent::init();
    }

    public function processPayment($order, $billingInfo)
    {
        switch ($this->settings['processor']) {
            case 'braintree':
                $result = $this->processPaymentBraintree($order, $billingInfo);
                break;
            case 'authorizeNet':
                $result = $this->processPaymentAuthorize($order, $billingInfo);
                break;

        }
        return isset($result) ? $result : false;
    }

    private function processPaymentBraintree($order, $billingInfo)
    {
        require_once(Yii::getPathOfAlias('application.vendors.braintree.lib').
            DIRECTORY_SEPARATOR . 'Braintree.php');

        Braintree_Configuration::environment($this->settings['environment']);
        Braintree_Configuration::merchantId($this->settings['merchantId']);
        Braintree_Configuration::publicKey($this->settings['publicKey']);
        Braintree_Configuration::privateKey($this->settings['privateKey']);
        $result = Braintree_Transaction::sale(array(
            'amount' => $order->discounted_total,
            'orderId' => $order->id,
            'creditCard' => array(
                'number' => $billingInfo->cc_number,
                'expirationMonth' => $billingInfo->cc_exp_month,
                'expirationYear' => $billingInfo->cc_exp_year,
                'cardholderName' => $billingInfo->cc_name_on_card,
            ),
            'customer' => array(
                'firstName' => $order->customer->first_name,
                'lastName' => $order->customer->last_name,
                'phone' => $order->customer->phone,
                'email' => $order->customer->email
            ),
            'billing' => array(
                'firstName' => $billingInfo->billing_name,
                'lastName' => $billingInfo->billing_last_name,
                'streetAddress' => $billingInfo->billing_street_address,
                'locality' => $billingInfo->billing_city,
                'region' => $billingInfo->billing_state,
                'postalCode' => $billingInfo->billing_zip_code,
                //'countryCodeAlpha2' => 'US'
            ),
            'shipping' => array(
                'firstName' => $billingInfo->shipping_name,
                'lastName' => $billingInfo->shipping_last_name,
                'streetAddress' => $billingInfo->shipping_street_address,
                'locality' => $billingInfo->shipping_city,
                'region' => $billingInfo->shipping_state,
                'postalCode' => $billingInfo->shipping_zip_code,
                //'countryCodeAlpha2' => 'US'
            ),
            'options' => array(
                'submitForSettlement' => true
            ),
        ));

        return array(
            'success' => $result->success,
            'message' => $result->success ? '' : $result->message,
            'transactionId' => $result->transaction->id,
        );
    }

    private function processPaymentAuthorize($order, $billingInfo)
    {
        require_once Yii::getPathOfAlias('application.vendors.authorizeNet').
            DIRECTORY_SEPARATOR. 'AuthorizeNet.php';
        $transaction = new AuthorizeNetAIM(
            $this->settings['apiLoginId'], $this->settings['transactionKey']
        );
        $transaction->setSandbox(($this->settings['environment'] == 'sandbox') ? true : false);
        $transaction->setFields(array(
            'amount' => $order->discounted_total,
            'card_num' => $billingInfo->cc_number,
            'description' => $order->description,
            'exp_date' => $billingInfo->cc_exp_month . $billingInfo->cc_exp_year,
            'first_name' => $billingInfo->billing_name,
            'last_name' => $billingInfo->billing_last_name,
            'address' => $billingInfo->billing_street_address,
            'city' => $billingInfo->billing_city,
            'state' => $billingInfo->billing_state,
            'zip' => $billingInfo->billing_zip_code,
            //'phone' => $billingInfo->billing_phone,
            'email' => $order->customer->email,
            'ship_to_first_name' => $billingInfo->shipping_name,
            'ship_to_last_name' => $billingInfo->shipping_last_name,
            'ship_to_address' => $billingInfo->shipping_street_address,
            'ship_to_city' => $billingInfo->shipping_city,
            'ship_to_state' => $billingInfo->shipping_state,
            'ship_to_zip' => $billingInfo->shipping_zip_code
        ));
        $transaction->setCustomFields(array(
            'order_id' => $order->id,
            'ship_to_phone' => $billingInfo->shipping_phone_number,
        ));
        $result = $transaction->authorizeAndCapture();

        return array(
            'success' => $result->approved,
            'message' => $result->approved ? '' : $result->response_reason_text,
            'transactionId' => $result->transaction_id,
        );
    }
}