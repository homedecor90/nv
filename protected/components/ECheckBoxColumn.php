<?php
/**
 * Created by PhpStorm.
 * User: Addicted
 * Date: 4/1/14
 * Time: 12:14 AM
 */

class ECheckBoxColumn extends CCheckBoxColumn
{
    public $disabled;

    protected function renderDataCellContent($row,$data)
    {
        if (isset($this->disabled))
            $disabled =
                $this->evaluateExpression($this->disabled, array(
                        'data'=>$data,
                        'row'=>$row
                    )
                );
        if (empty($disabled))
            parent::renderDataCellContent($row,$data);
    }
} 