<?php

Yii::import('zii.widgets.grid.CGridView');

/**
 * Extended version of CGridView created to add save page feature
 * Current page will be saved to the session vars and restored next time the page is viewed 
 * @author yefrem
 *
 */
class CGridViewExt extends CGridView {
	/**
	 * 
	 * @var boolean whether to remember selected page, ignored if $ajaxUpdate == false
	 */
	public $rememberPage = true;
	
	public function init(){
		if ($this->ajaxUpdate !== false && $this->rememberPage){
			$pagevar = $this->dataProvider->getPagination()->pageVar;
	 		$state = 'gridpage_'.$this->getId().'_'.$pagevar;
	
			if (isset($_GET[$this->ajaxVar]) && $_GET[$this->ajaxVar] == $this->getId()){
				$page = isset($_GET[$pagevar]) ? $_GET[$pagevar] : 1;
				Yii::app()->user->setState($state, $page);
			} elseif($page = Yii::app()->user->getState($state)) {
				$this->dataProvider->getPagination()->setCurrentPage(intval($page) - 1);
			}
		}
		parent::init();
	}
}