$(document).on('click', '.order-actions.approve', function(e){
    e.preventDefault();
    if (!confirm($(this).data('confirmation')))
        return(false);
    var gridId = $(this).data('grid-id');
    $.get($(this).data('url'), function(resp){
        $.fn.yiiGridView.update(gridId);
        alert(resp.message);
    }, 'json');
    return true;
});