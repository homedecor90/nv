/**
 * addicted additions
 * Retailers section
 */

// Add to cart buttons

$(document).on('click', '#retailer-shopping-cart-form input.ajax-order', function(){
    var form = $(this).closest('form');
    $.ajax({
        type: 'POST',
        context: this,
        url: $(form).attr('action'),
        data: $(form).serialize(),
        success: function(data) {
            if ($(this).hasClass('checkout'))
                document.location = $(this).data('href');
            else jQuery.fancybox.close();
        }
    });
});

var calculateTotalPrice = function(quantity) {
    var shippingCost = item.shippingCost[$('#retailer-shopping-cart-form .shipping_method').val()];
    var totalPrice = (parseFloat(item.price) + parseFloat(shippingCost)) * quantity;
    $('tr.sale_price td').html(totalPrice);
}

$(document).on('keyup', '#retailer-shopping-cart-form input.quantity', function(e){
    var quantity = $('#item_quantity').val();
    if (/^[0-9]+$/.test(quantity)) {
        calculateTotalPrice(quantity);
    }
    else e.preventDefault();
});

$(document).on('change', '#retailer-shopping-cart-form .shipping_method', function(e){
    calculateTotalPrice($('#item_quantity').val());
});

$(document).on('keydown', '.quantity', function(event) {
    // Allow: backspace, delete, tab, escape, and enter
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        // let it happen, don't do anything
        return true;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
    }
});

/**
 * Checkout form
 */
$('#checkout_form').submit(function(){
    if ($('#my_billing_address').attr('checked')) {
        $('input[id^="CustomerOrderBillingInfo_billing"]').each(function(){
            $('#' + $(this).attr('id').replace('_billing_', '_shipping_')).
                val($(this).val());
        });
    }
    $('#CustomerOrderBillingInfo_cc_name_on_card').val(
        $('#CustomerOrderBillingInfo_billing_name').val() + ' ' + $('#CustomerOrderBillingInfo_billing_last_name').val()
    );
});
$('#CustomerOrderBillingInfo_ship_to_billing').click(function(){
    $('input[id^="CustomerOrderBillingInfo_billing"]').each(function(){
        $('#' + $(this).attr('id').replace('_billing_', '_shipping_')).
            val($(this).val());
    });
});

$('#CustomerOrder_add_sales_tax').click(function(){
    var value = $(this).attr('checked') ? 1 : 0;
    $.ajax({
        url: $(this).data('ajax-url').replace('%7Bvalue%7D', value),
        success: function(data) {
            if (data.status == 'success') {
                $('#grand_total_value').html(data.new_total);
                $('#discounted_value').html(data.new_discounted);
                if (value == 1) {
                    $('h3.sales_tax').show();
                } else {
                    $('h3.sales_tax').hide();
                }
            }
        },
        dataType: 'json'
    })
});

$('#CustomerOrder_shipping_method').change(function(){
    var value = $(this).val();
    $.ajax({
        url: $(this).data('ajax-url').replace('%7Bvalue%7D', value),
        success: function(data) {
            if (data.status == 'success') {
                $('#grand_total_value').html(data.new_total)
                $('#discounted_value').html(data.new_discounted)
            }
        },
        dataType: 'json'
    })
});

$('#checkout_form #different_address').click(function(){$('#shipping_address').show();})
$('#checkout_form #my_billing_address').click(function(){$('#shipping_address').hide();})