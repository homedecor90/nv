$(document).on('click', '.grid-view .button-column .ajax-operation', function(e) {
    e.preventDefault();
    var gridId = $(this).closest('.grid-view').attr('id');
    $.get($(this).attr('href'), function(){
        $.fn.yiiGridView.update(gridId);
    });
});