//Items/orders/shipments cancellation
function cancelShippingWindow(url, callback){
	jQuery('#cancellation_reason_button').trigger('click');
	jQuery('#save_cancellation_reason').unbind('click');
	jQuery('#save_cancellation_reason').click(function(e){
		jQuery.ajax({
            url:url,
            dataType:"json",
            type: 'post',
            data: {reason: jQuery("input:radio[name='cancel_reason']:checked").val()},
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                
                if (result.status == "success"){
                    jQuery.fancybox.close();
                	callback();
                }
            }
        });
	});
}

jQuery('.authorize_return').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = global_baseurl + "/index.php/customershipper/authorize_return_form_ajax/?shipment_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#authorize_return_form_content').html(result);
			jQuery('#authorize_return_form_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#authorize_return_submit').live('click', function(e){
	var checked = false;
	var boxes = [];
	var id = jQuery('#authorize_return_shipment_id').val();
	
	jQuery('#authorize_return_form_content input:checked').each(function(i){
		checked = true;
		boxes.push(jQuery(this).val());
	});
	
	if (!checked){
		alert('Please select boxes to return');
		return false;
	}

	var url = global_baseurl + "/index.php/customershipper/authorize_return_ajax/?shipment_id=" + id;

	jQuery.ajax({
		url:url,
		type: 'post',
		dataType:"json",
		data: {
				boxes: boxes,
		},
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				jQuery.fancybox.close();
				refreshWindow();
			}
		}
	});

	jQuery(e.target).attr('disabled', 'disabled');
});

jQuery('.switch_shipping_method').live('click', function(e){
	var id = jQuery(e.target).attr('rel');
	var url = global_baseurl + "/index.php/customershipper/order_change_shipping_method_form_ajax/?order_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"html",
		success:function(result){
			jQuery('#order_shipping_method_form_container').html(result);
			jQuery('#order_shipping_method_button').trigger('click');
			calculateTotalsWithShipping();
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});


// scanning out
function scanOutWindow(url, callback){
	jQuery('#scan_out_form_button').trigger('click');
	jQuery('#scan_out_form_submit').unbind('click');
	jQuery('#scan_out_form_submit').click(function(e){
		jQuery.ajax({
            url:url,
            dataType:"json",
            type: 'post',
            data: {
            	tracking_number: jQuery('#scan_out_tracking_number').val(),
            	tracking_website: jQuery('#scan_out_website').val(),
            	shipping_company: jQuery('#shipping_company').val(),
                shipment_cost: jQuery('#shipment_cost').val(),
            	phone_number: jQuery('#phone_number').val()
            },
            success:function(result){
                if (result.status == "fail"){
                    alert(result.message);
                    return;
                }
                
                if (result.status == "success"){
                    jQuery.fancybox.close();
                	callback();
                }
            }
        });
	});
}

var opened_order_id = 0;
var opened_shipment_id = 0;

// some duplicated functionality from pending orders page
jQuery('.order_expand_shipment_details').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	opened_order_id = id;
	var url = global_baseurl+"/index.php/customershipper/shipment_details_ajax/?order_id=" + id;
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			jQuery('#order_details_content').html(result.data);
			jQuery('#order_details_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

function refreshOrderDetailsWindow(){
	order_details_need_update_window = true;
	var url = global_baseurl+"/index.php/customershipper/shipment_details_ajax/?order_id=" + opened_order_id;
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			jQuery('#order_details_content').html(result.data);
		// jQuery('#order_details_button').trigger('click');
		}
	});
}

jQuery('.combine_orders').live('click', function(e){
	var order_id = parseInt(prompt('Enter order ID:', '0'));
	if (!order_id){
		return false;
	}

	var url = global_baseurl + '/index.php/customer/order_details_json_ajax/?order_id='+order_id;
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}

			jQuery('#combine_orders_order_id').text(result['order_id']);
			jQuery('#combine_orders_customer').text(result['customer_name']);
			jQuery('#combine_orders_shipping_info').html(result['shipping_name']+'<br /><br />'+result['shipping_address']);
			jQuery('#combine_orders_submit').attr('rel', result['order_id']);
			jQuery('#combine_orders_button').trigger('click');
		}
	});
});

jQuery('#combine_orders_submit').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var data = {};
	data.order_id 	= jQuery(e.target).attr('rel');
	data.copy_info 	= jQuery('#combine_orders_copy_info:checked').length ? 1 : 0;
	data.main_order_id = opened_order_id;
	
	var url = global_baseurl + '/index.php/customershipper/combine_customer_orders_ajax/';
	jQuery.ajax({
		url:url,
		dataType:"json",
		type: 'post',
		data: data,
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			jQuery.fancybox.close();
			refreshWindow();
		}
	});
});

jQuery('.combine_shipment').live('click', function(e){
	var shipment_id = parseInt(prompt('Please enter shipment ID you want to attach items to:', '0'));
	if (!shipment_id){
		return false;
	}
	
	opened_shipment_id = jQuery(e.target).attr('rel');
	
	var url = global_baseurl + '/index.php/customershipper/shipment_details_json_ajax/?shipment_id='+shipment_id;
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.shipment.status != 'Pending'){
				alert('Shipment is not pending');
				return;
			}
			
			jQuery('#combine_shipments_order_id').text(result.shipment.order_id);
			jQuery('#combine_shipments_customer').text(result.shipment.customer_name);
			jQuery('#combine_shipments_shipping_info').html(result.shipment.shipping_name+'<br /><br />'+result.shipment.shipping_address);
			jQuery('#combine_shipments_submit').attr('rel', result.shipment.id);
			jQuery('#combine_shipments_button').trigger('click');
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('#combine_shipments_submit').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var data = {};
	data.attached_shipment_id 	= opened_shipment_id;
	data.main_shipment_id = jQuery(e.target).attr('rel');
	
	var url = global_baseurl + '/index.php/customershipper/combine_shipments_ajax/';
	jQuery.ajax({
		url:url,
		dataType:"json",
		type: 'post',
		data: data,
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			jQuery.fancybox.close();
			refreshWindow();
		}
	});
});

jQuery('.new_shipment').live('click', function(e){
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');
	jQuery('.shipment_item_checkbox.order_'+order_id).show();
	target.hide();
	jQuery('#save_shipment_'+order_id).show();
	jQuery('#cancel_new_shipment_'+order_id).show();
});

jQuery('.save_shipment').live('click', function(e){
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');

	var url = global_baseurl + "/index.php/customershipper/new_shipment_ajax/?order_id=" + order_id;
	var checked = false;
	jQuery('.shipment_item_checkbox.order_'+order_id+':checked').each(function(i){
		checked = true;
		url += '&item_id[]='+jQuery(this).val();
	});
	
	if (!checked){
		alert('Please check items to add');
		return false;
	}
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				refreshOrderDetailsWindow();
			}
		}
	});
});

jQuery('.cancel_new_shipment').live('click', function(e){
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');
	jQuery('.shipment_item_checkbox.order_'+order_id).hide();
	target.hide();
	jQuery('#save_shipment_'+order_id).hide();
	jQuery('#new_shipment_'+order_id).show();
});

jQuery('.select_shipment_for_ind_item').live('change', function(e){
	var target  = jQuery(e.target);
	var item_id = target.attr('rel');
	var shipment_id = target.val();
	
	var url = global_baseurl + "/index.php/individualitem/move_item_to_shipment_ajax/?item_id=" + item_id + '&shipment_id=' + shipment_id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				refreshOrderDetailsWindow();
			}
		}
	});
});


jQuery('.order_verification_request').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');
	var url = global_baseurl + "/index.php/customer/order_shipping_info_verification_request_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert('Failed to send notification');
				return;
			}
			
			if (result.status == "success"){
				alert('Notification sent');
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.order_verify').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target  = jQuery(e.target);
	var order_id = target.attr('rel');
	var url = global_baseurl + "/index.php/customer/verify_order_shipping_info_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert('Failed to save order');
				return;
			}
			
			if (result.status == "success"){
				refreshWindow();
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.cancel_return').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	var target  = jQuery(e.target);
	var id = target.attr('rel');
	var url = global_baseurl + "/index.php/customershipper/cancel_shipment_ajax/?shipment_id=" + id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			
			if (result.status == "success"){
				refreshWindow();
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.schedule_pickup').live('click', function(e){
	var target = jQuery(e.target);
    var id = target.attr('rel');
	jQuery('#pickup_shipment_id').val(id);
	jQuery('#schedule_pickup_button').trigger('click');
	jQuery('#pickup_date').removeAttr('disabled');
	jQuery('#schedule_by_admin').attr('checked', 'checked');
	jQuery('#pickup_date').val('');
	
	e.preventDefault();
    e.stopImmediatePropagation();
});

jQuery('#schedule_by_customer').change(function(e){
	jQuery('#pickup_date').attr('disabled', 'disabled');
});

jQuery('#schedule_by_admin').change(function(e){
	jQuery('#pickup_date').removeAttr('disabled');
});

jQuery('#schedule_pickup_submit').live('click', function(e){
	var id = jQuery('#pickup_shipment_id').val();
	var date = jQuery('#pickup_date').val();
	var option = jQuery('input:radio[name=schedule_option]:checked').val();
	
	var url = global_baseurl + "/index.php/customershipper/order_schedule_pickup_ajax/?shipment_id="+id+"&option="+option;
    if (option == 'admin'){
		url += "&date="+date;
	}
	
    jQuery.ajax({
        type: 'GET',
        url: url,
        success: function(result){
            if (result.status == 'fail'){
                alert(result.message);
                return;
            }
            
            refreshWindow();
			jQuery.fancybox.close();
        },
        dataType: 'json'
    });
});

jQuery('.toggle_shipment_table').live('click', function(e){
	var target = jQuery(e.target);
	var id = target.attr('rel');
	jQuery('#shipment_table_'+id).toggle(300, 'linear');
	if (target.text() == 'Expand'){
		target.text('Collapse');
	} else {
		target.text('Expand');
	}
	
	e.preventDefault();
	e.stopImmediatePropagation();
});


jQuery('#select_shipping_method').live('change', function(e){
	calculateTotalsWithShipping();
});

jQuery('#custom_dsc_total').live('change', function(e){
	calculateTotalsWithShipping();
});

jQuery('#custom_dsc_total').live('keyup', function(e){
	calculateTotalsWithShipping();
});

jQuery('#fancybox-content .new_special').live('keyup', function(e){
	calculateTotalsWithShipping();
});

function calculateTotalsWithShipping(){
	var method = jQuery('#select_shipping_method').val();
	var new_total = parseFloat(jQuery('#total_'+method).val());
	var current_total = parseFloat(jQuery('#current_total').text());
	var current_dsc_total = parseFloat(jQuery('#current_dsc_total').text());
	var new_dsc_total = parseFloat(jQuery('#dsc_total_'+method).val());
	// var custom_dsc_total = parseFloat(jQuery('#custom_dsc_total').val());
	
	// TODO: make it in hidden fields, not in spans
	var old_dsc_total = parseFloat(jQuery('#current_dsc_total').text());

	// special shipping difference
	var new_special = 0;
	var current_special = 0;
	jQuery('#fancybox-content .current_special').each(function(){
		var val = jQuery(this).val();
		current_special += val ? parseFloat(val) : 0;
	});
	jQuery('#fancybox-content .new_special').each(function(){
		var val = jQuery(this).val();
		new_special += val ? parseFloat(val) : 0;
	});

	new_total += (new_special - current_special);
	new_dsc_total += (new_special - current_special);

	var total_diff = new_total - current_total;

	custom_dsc_total = current_dsc_total + total_diff;
	if (new_dsc_total != new_total){
		jQuery('#custom_dsc_total').val(custom_dsc_total);
	}
	
	if (custom_dsc_total){
		new_dsc_total = custom_dsc_total;
	}
	additional_payment = total_diff;
	
	// TODO: rounding numbers
	
	
	jQuery('#new_total').text(new_total 
			+ ' (' + (total_diff>=0 ? '+' : '-') + '$' + Math.abs(total_diff) + ')');
	jQuery('#new_dsc_total').text(new_dsc_total);
	jQuery('#additional_payment').val(additional_payment);
}

jQuery('#save_new_shipping_method').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}

	var order_id = jQuery('#shipping_method_order_id').val();
	var order_items = [];
	var special_shipping = [];

	jQuery('.new_special').each(function(){
		var el = jQuery(this);
		order_items.push(el.attr('rel'));
		special_shipping.push(el.val()); 
	});
	
	var data = {
		shipping_method: jQuery('#select_shipping_method option:selected').text(),
		custom_discounted_total: jQuery('#custom_dsc_total').val(),
		additional_payment: jQuery('#additional_payment').val(),
		send_notification: jQuery('#email_notification:checked').length ? 1 : 0,
		order_items: order_items,
		special_shipping: special_shipping
	};

	
	var url = global_baseurl + "/index.php/customershipper/order_change_shipping_method_ajax/?order_id=" + order_id;
	
	jQuery.ajax({
		url:url,
		dataType:"json",
		type: 'post',
		data: data,
		success:function(result){
			if (result.status == "fail"){
				alert(result['message']);
				return;
			}
			
			if (result.status == "success"){
				jQuery.fancybox.close();
				$.fn.yiiGridView.update('customer-sold-order-grid');
			}
		}
	});
	
	e.preventDefault();
	e.stopImmediatePropagation();
});

jQuery('.requeue').live('click', function(e){
	if (!confirm('Are you sure?')){
		return false;
	}
	
	var target = jQuery(e.target);
	var id = target.attr('rel');
	var url = global_baseurl + "/index.php/customershipper/requeue_shipment_ajax/?id=" + id;
	jQuery.fancybox.close();
	jQuery.ajax({
		url:url,
		dataType:"json",
		success:function(result){
			if (result.status == "fail"){
				alert(result.message);
				return;
			}
			refreshWindow();
		}
	});
});