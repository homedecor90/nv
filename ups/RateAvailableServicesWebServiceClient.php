

<?php

require_once('fedex-common.php');

//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = "RateService_v10.wsdl";

ini_set("soap.wsdl_cache_enabled", "0");
 
$client = new soapclient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

$request['WebAuthenticationDetail'] = array('UserCredential' =>
                                      array('Key' => getProperty('key'), 'Password' => getProperty('password'))); 
$request['ClientDetail'] = array('AccountNumber' => getProperty('shipaccount'), 'MeterNumber' => getProperty('meter'));
$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Available Services Request v10 using PHP ***');
$request['Version'] = array('ServiceId' => 'crs', 'Major' => '10', 'Intermediate' => '0', 'Minor' => '0');
$request['ReturnTransitAndCommit'] = true;
$request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
$request['RequestedShipment']['ShipTimestamp'] = date('c');
// Service Type and Packaging Type are not passed in the request
$request['RequestedShipment']['Shipper'] = array('Address'=>getProperty('address1'));
$request['RequestedShipment']['Recipient'] = array('Address'=>getProperty('address2'));
$request['RequestedShipment']['ShippingChargesPayment'] = array('PaymentType' => 'SENDER',
                                                        'Payor' => array('AccountNumber' => getProperty('billaccount'), // Replace 'XXX' with payor's account number
                                                                     'CountryCode' => 'US'));
$request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
$request['RequestedShipment']['RateRequestTypes'] = 'LIST'; 
$request['RequestedShipment']['PackageCount'] = '2';
$request['RequestedShipment']['RequestedPackageLineItems'] = array(
'0' => array(
	'SequenceNumber' => 1,
	'GroupPackageCount' => 1,
	'Weight' => array('Value' => $weight,
    'Units' => 'LB'),
    'Dimensions' => array('Length' => $length,
       'Width' => $width,
       'Height' => $height,
       'Units' => 'IN')));

try 
{
	if(setEndpoint('changeEndpoint'))
	{
		$newLocation = $client->__setLocation(setEndpoint('https://ws.fedex.com:443/web-services'));
	}
	
	$response = $client ->getRates($request);
        
    if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR')
    {
        //print_r($response);
        
        echo '<table border="1" align="center" cellpadding="5" cellspacing="1" width="100%">';
        
        $reversrespons=array_reverse($response->RateReplyDetails);             
      foreach ( $reversrespons as $rateReply)
        { 
           	echo '<tr>';
        	$serviceType = $rateReply -> ServiceType ;
        	$amount =  number_format($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount,2,".",",") ;
        	
        	echo '<td class="content"><input name="shipmethod" type="radio" /></td><td><b>'.$serviceType.'</b></td><td>$'. $amount.'</td>';
            
        	echo '</tr>';
		
        }
        echo '</table>'. Newline;
    	//printSuccess($client, $response);
    }
    else
    {
        printError($client, $response); 
    } 
    
    writeToLog($client);    // Write to log file   

} catch (SoapFault $exception) {
   printFault($exception, $client);        
}

?>