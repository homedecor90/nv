INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Scheduled For Customer Pickup', 'customershipper/customer_pickup', '16'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'View Pickup Docs', 'customershipper/pickup_docs', '16'
);

DROP TABLE IF EXISTS `dashblock`;
CREATE TABLE IF NOT EXISTS `dashblock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `actions` text,
  `weight` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп даних таблиці `dashblock`
--

INSERT INTO `dashblock` (`id`, `title`, `actions`, `weight`, `status`) VALUES
(1, 'Dashboard', 'dash/dash/index|Block List\r\ndash/dash/admin|Manage Block\r\ndash/dash/create|Create New Block\r\ndash/dash/order|Order Blocks', 0, 1),
(3, 'User', 'user/user/admin|Manage Users\r\nuser/user/create|Create New User', 2, 1),
(4, 'Item', 'item/admin|Manage Items\r\nitem/create|Create New Item\r\nitem/inventory|Inventory\r\nindividualitem/admin|Manage Individual Items\r\nindividualitem/create|Create New Individual Item\r\ncustomer/import_osc_orders|Import osCommerce Orders\r\ncustomer/archived_osc_orders|Archived osCommerce Orders', 3, 1),
(5, 'Customer', 'customer/admin|Manage Customers\r\ncustomer/create|Create New Customer\r\ncustomer/pending|Pending Customers\r\ncustomer/sold_orders|All Sold Orders\r\ncustomer/pending_approval_orders|Pending Approval Orders', 7, 1),
(6, 'Heard Through', 'heardthrough/admin|Manage Heard Through\r\nheardthrough/create|Create New Herad Through', 4, 1),
(7, 'Supplier Order', 'supplierorder/admin|Manage Supplier Orders\r\nsupplierorder/admin_archived|Archived Supplier Orders\r\nsupplierorder/create|Create New Supplier Order', 5, 1),
(8, 'Supplier', 'supplierorder/index|My Orders\r\nsupplierorder/index_archived|Archived Orders', 8, 1),
(9, 'Supplier Shipper', 'supplierorder/shipping|Available Orders', 9, 1),
(10, 'Profile', 'profile/yumprofile/update|My Profile', 10, 1),
(12, 'Url Permission', 'urlgroup/admin|Manage URL Group\r\nurlgroup/create|Create New URL Group\r\nurlpermission/admin|Manage URL Permission\r\nurlpermission/create|Create New URL Permission', 11, 1),
(13, 'Settings', 'settings/index|Settings', 13, 1),
(14, 'Reports', 'reports/lostsale|Lost Sale', 12, 1),
(15, 'Supplier Order Shipper', 'supplierorder/view_container|Manage Supplier Containers\r\nsupplierorder/pending_payment|Pending Payment Containers\r\nsupplierorder/payment_history|Payment History', 6, 1),
(16, 'Warehouse', 'item/inventory|Inventory\r\ncustomershipper/pending_orders|Pending Orders\r\ncustomershipper/cancelled|Cancelled Orders\r\ncustomershipper/chargebacks|Chargebacks', 1, 1),
(17, 'Customer Shipping', 'customershipper/pending_orders|Pending Orders\r\ncustomershipper/select_bids|Lowest Bid\r\ncustomershipper/scheduled|Scheduled for Shipper Pickup\r\ncustomershipper/customer_pickup|Scheduled for Customer Pickup\r\ncustomershipper/completed|Completed Shipments\r\ncustomershipper/cancelled|Cancelled Orders\r\ncustomershipper/chargebacks|Chargebacks', 14, 1),
(18, 'Customer Shipper', 'customershipper/bidding|Submit Quote\r\ncustomershipper/bol_upload|Upload BOL', 0, 1),
(19, 'View Customers', 'customer/admin|Manage Customers', 0, 1);
