ALTER TABLE `customer_order_shipment` ADD `paid` INT( 1 ) NOT NULL ,
ADD `paid_date` DATETIME NOT NULL ,
ADD `paid_checknumber` VARCHAR( 30 ) NOT NULl;

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Customer Shipper Invoices', 'customershipper/invoices', '6'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Pay Shipper Invoices (AJAX)', 'customershipper/pay_shipper_invoices_ajax', '6'
);
