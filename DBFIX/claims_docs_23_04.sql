CREATE TABLE `official_doc` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `type` varchar(20) NOT NULL,
 `item_id` int(11) NOT NULL,
 `path` varchar(500) NOT NULL,
 `date` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='May be used for different kinds of official docs';

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Upload Claim Doc', 'customershipper/upload_claim_doc', '16'
), (
NULL , 'Attach claim doc to the actual shipment', 'customershipper/attach_claim_doc_ajax', '16'
);

