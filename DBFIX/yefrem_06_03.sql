INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'CS Completed Shipments', 'customershipper/cs_completed', '17'
), (
NULL , 'CS Claims', 'customershipper/cs_claims', '17'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Update Claim Amount (AJAX)', 'customershipper/update_claim_amount_ajax', '1'
);
