ALTER TABLE `individual_item` ADD `replaced` INT( 1 ) NOT NULL ,
ADD `replacement_order_id` INT UNSIGNED NOT NULL;

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Replace damaged items (AJAX)', 'customershipper/replace_damaged_items_ajax', '1'
);

ALTER TABLE `individual_item` ADD `is_replacement` INT( 1 ) NOT NULL COMMENT 'Indicates if item is sent to the customer as replacement for damaged item',
ADD `replacement_item` INT UNSIGNED NOT NULL COMMENT 'Shows what item is replaced by current one',
ADD `replacement_boxes` VARCHAR( 20 ) NOT NULL COMMENT 'Shows what boxes are actually replaced';

ALTER TABLE `customer_order` ADD `is_replacement` INT( 1 ) NOT NULL;
ALTER TABLE `customer_order` ADD `replacement_order` INT UNSIGNED NOT NULL;

===


ALTER TABLE `individual_item` ADD `reordered_from_supplier` INT( 1 ) NOT NULL;
INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Order Damaged Items From Supplier Form (AJAX)', 'customershipper/order_damaged_items_from_supplier_form_ajax', '1'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Order Damaged Items From Supplier (AJAX)', 'customershipper/order_damaged_items_from_supplier_ajax', '1'
);

ALTER TABLE `supplier_order_items` ADD `is_replacement` INT( 1 ) NOT NULL;

ALTER TABLE `individual_item` ADD `is_supplier_replacement` INT( 1 ) NOT NULL ,
ADD `supplier_replacement_boxes` VARCHAR( 20 ) NOT NULL ,
ADD `supplier_replacement_cbm` FLOAT NOT NULL;
