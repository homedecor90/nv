ALTER TABLE `customer_order` ADD `shipping_info_verified` INT( 1 ) NOT NULL DEFAULT '0';

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Order Verification Request (AJAX)', 'customer/order_shipping_info_verification_request_ajax', '16'
), (
NULL , 'Verify Order Shipping Info (AJAX)', 'customer/verify_order_shipping_info_ajax', '1'
);

