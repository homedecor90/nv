ALTER TABLE `customer_order_shipment` ADD `claim` INT( 1 ) NOT NULL DEFAULT '0',
ADD `claim_resolved` INT( 1 ) NOT NULL DEFAULT '0',
ADD `claim_date` DATETIME NULL DEFAULT NULL ,
ADD `claim_resolved_date` DATETIME NULL DEFAULT NULL;

ALTER TABLE `individual_item` ADD `damaged` INT( 1 ) NOT NULL DEFAULT '0';

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Mark Shipment As Damaged', 'customershipper/mark_shipment_as_damaged_ajax', '1'
), (
NULL , 'Claims', 'customershipper/claims', '1'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Shipment Mark as Damaged Form', 'customershipper/shipment_mark_as_damaged_form_ajax', '1'
);

ALTER TABLE `individual_item` ADD `damaged_boxes` VARCHAR( 20 ) NOT NULL COMMENT 'Damaged box numbers, comma separated';
ALTER TABLE `customer_order_shipment` ADD `claim_amount` FLOAT NOT NULL;
