ALTER TABLE `customer_order_shipment` ADD `return` INT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `individual_item` ADD `customer_return_id` INT( 11 ) NOT NULL COMMENT 'refers to individual item return table';
ALTER TABLE `customer_order_shipment` ADD `return_date` DATETIME NOT NULL;

CREATE TABLE IF NOT EXISTS `individual_item_return` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) NOT NULL,
  `claim_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `returned_boxes` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Authorize return for damaged items (AJAX)', 'customershipper/authorize_return_ajax', '1'
);

INSERT INTO `url_permission` (`id`, `title`, `url`, `url_group`) VALUES (NULL, 'Authorize return form (AJAX)', 'customershipper/authorize_return_form_ajax', '6');
