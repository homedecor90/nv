CREATE TABLE IF NOT EXISTS `customer_order_refund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_order_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `individual_item` ADD `claim_refund_id` INT NOT NULL;
INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Make Refund For Damaged Items (AJAX)', 'customershipper/refund_damaged_items_ajax', '1'
);

