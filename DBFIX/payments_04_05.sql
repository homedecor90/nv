CREATE TABLE IF NOT EXISTS `customer_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `item_id` int(11) NOT NULL COMMENT 'Order, shipment or other model id depending on the payment type',
  `amount` float NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
