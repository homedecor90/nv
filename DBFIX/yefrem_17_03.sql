ALTER TABLE `customer_order` ADD `cancellation_reason` INT NOT NULL;
ALTER TABLE `customer_order_shipment` ADD `cancellation_reason` INT NOT NULL;
ALTER TABLE `individual_item` ADD `cancellation_reason` INT NOT NULL;

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Delete Cancellation Reason', 'cancelreason/delete', '8'
), (
NULL , 'Update Cancellation Reason', 'cancelreason/update', '8'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'Cancellation Reason List', 'cancelreason/index', '8'
), (
NULL , 'Create Cancellation Reason', 'cancelreason/create', '8'
);

INSERT INTO `url_permission` (
`id` ,
`title` ,
`url` ,
`url_group`
)
VALUES (
NULL , 'View Cancellation Reason', 'cancelreason/view', '8'
), (
NULL , 'Admin Cancellation Reasons', 'cancelreason/admin', '8'
);

INSERT INTO `url_permission` (`id`, `title`, `url`, `url_group`) VALUES (NULL, 'Select Cancellation Reason Form (AJAX)', 'cancelreason/select_reason_form_ajax', '20');
