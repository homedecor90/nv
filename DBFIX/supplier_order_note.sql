-
CREATE TABLE IF NOT EXISTS `supplier_order_note` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `operator_id` int(10) unsigned NOT NULL,
  `supplier_order_id` int(10) unsigned NOT NULL,
  `content` text CHARACTER SET latin1 NOT NULL,
  `created_on` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

